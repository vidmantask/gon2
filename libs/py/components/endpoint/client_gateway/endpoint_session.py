"""
This module contains the client Gateway Client side of a endpoint session
"""
from __future__ import with_statement

import components.endpoint
import lib.checkpoint as checkpoint 
import components.communication.tunnel_endpoint_base as tunnel_endpoint_base 

import components.plugin.client_gateway.plugin_socket_endpoint as plugin_socket_endpoint 
import components.plugin.client_gateway.plugin_socket_token as plugin_socket_token
import components.traffic.client_gateway.client_launch_internal

from plugin_types.client_gateway.plugin_type_token import Token 

from components.presentation.update import UpdateController
import hashlib
import datetime
import time    
import uuid
from plugin_types.client_gateway import plugin_type_token


COMPUTER_TOKEN = 'computer_token'
COMPUTER_TOKEN_ID = '1'


class EndpointSession(tunnel_endpoint_base.TunnelendpointSession, 
                      plugin_socket_endpoint.IPluginSocketCallback, 
                      components.traffic.client_gateway.client_launch_internal.LaunchSession):
    
    ENROLL_STATE_NONE = "None"
    ENROLL_STATE_STARTED = "Started"
    ENROLL_STATE_INITIALIZED = "Initialized"
    ENROLL_STATE_DEPLOYING = "Deploying"
    ENROLL_STATE_FINISHED = "Finished"
    
    MAX_WAIT_FOR_DEVICE_STATUS_SECONDS = 120

    #NOTE: duplicate on server
    ERROR_CODE_UNKNOWN = -1
    ERROR_CODE_TIMEOUT = 1
    ERROR_CODE_NOT_ADMINISTRATOR = 2
    ERROR_CODE_ALREADY_REGISTERED = 3
    ERROR_CODE_NOT_INSTALLED = 4
    ERROR_CODE_ALREADY_ENROLLED = 5
#    ERROR_CODE_TIMEOUT = 1
#    ERROR_CODE_TIMEOUT = 1
    
    def __init__(self, async_service, checkpoint_handler, new_tunnel, user_interface, dictionary, plugin_manager, plugins, client_knownsecret, servers, cb_close, cb_close_with_error, client_runtime_env):
        tunnel_endpoint_base.TunnelendpointSession.__init__(self, async_service, checkpoint_handler, new_tunnel)
        self.endpoint_plugin_socket_endpoint = plugin_socket_endpoint.PluginSocket(plugin_manager, plugins, self) 
        self.endpoint_plugin_socket_token = plugin_socket_token.PluginSocket(plugin_manager, plugins)
        self._user_interface = user_interface
        self.dictionary = dictionary
        self.client_known_secret = client_knownsecret 
        self.servers = servers
        self.cb_close = cb_close
        self.cb_close_with_error = cb_close_with_error
        self.client_runtime_env = client_runtime_env
        self._enroll_state = self.ENROLL_STATE_NONE 
        self._undeployed_tokens = []
        self._deployed_tokens = []
        self.selected_token = None
        self.selected_index = -1
        
        self.finished_with_error = None
        
        self.has_set_enrolled = False
        
        self.seconds_waited = 0
        self.phases = [
                      {'enroll_state': self.ENROLL_STATE_STARTED, 'name': self.dictionary._('Detect'), 'state': UpdateController.ID_PHASE_ACTIVE}, 
                      {'enroll_state': self.ENROLL_STATE_INITIALIZED, 'name': self.dictionary._('Select'), 'state': UpdateController.ID_PHASE_PENDING}, 
                      {'enroll_state': self.ENROLL_STATE_DEPLOYING, 'name': self.dictionary._('Enroll'), 'state': UpdateController.ID_PHASE_PENDING},
                      {'enroll_state': self.ENROLL_STATE_FINISHED, 'name': self.dictionary._('Finished'), 'state': UpdateController.ID_PHASE_PENDING},
                     ]
        
    def session_start_and_connected(self):
        """
        This is called when tunnelendpoint_connected and session_start both has been called
        """ 
        with self.checkpoint_handler.CheckpointScope("session_start_and_connected", components.endpoint.module_id, checkpoint.DEBUG):
            self.endpoint_plugin_socket_endpoint.run_all()

    def session_close(self):
        with self.checkpoint_handler.CheckpointScope("session_close", components.endpoint.module_id, checkpoint.DEBUG):
            pass
        
    def init_gui(self):
        self._user_interface.update.set_selection_method(UpdateController.ID_SELECTION_SINGLE)
        self._user_interface.update.set_banner(UpdateController.ID_BANNER_ENROLL)
        self._user_interface.update.set_phase_list(self.phases)
        self._user_interface.update.subscribe(self._gui_on_update)
        
    def exit_gui(self):
        self._set_state(self.ENROLL_STATE_NONE)
        self._user_interface.update.unsubscribe(self._gui_on_update)
        self._user_interface.update.view.hide()

    def show_messagebox(self, msg):
        self._user_interface.modalmessage.set_text(msg)
        self._user_interface.modalmessage.set_cancel_visible(False)
        self._user_interface.modalmessage.set_cancel_allowed(False)
        self._user_interface.modalmessage.reset_next_clicked()
        self._user_interface.modalmessage.set_next_visible(True)
        self._user_interface.modalmessage.set_next_allowed(True)
        self._user_interface.modalmessage.display()

    def report(self, plugin_name, endpoint_info):
        """
        Callback from endpoint plugin socket
        """
        endpoint_info_attributes = [(endpoint_info.attribute_name, endpoint_info.attribute_value)]
        self.tunnelendpoint_remote('remote_report_some', plugin_name=plugin_name, endpoint_info_attributes=endpoint_info_attributes)
        
    def report_some(self, plugin_name, endpoint_infos):
        """
        Callback from endpoint plugin socket
        """
        endpoint_info_attributes = [(x.attribute_name, x.attribute_value) for x in endpoint_infos]
        self.tunnelendpoint_remote('remote_report_some', plugin_name=plugin_name, endpoint_info_attributes=endpoint_info_attributes)

    def all_done(self):
        self.tunnelendpoint_remote('remote_all_done')

    def all_done_hash(self):
        self.tunnelendpoint_remote('remote_all_done_hash')

    def calc_hash_key(self, token):
        return str(uuid.uuid1())
#        time_str = datetime.datetime.today().isoformat()
#        hash_key_data = u'%s%s' % (time_str, token.get("token_type"))
#        hash_key = hashlib.sha1(hash_key_data.encode('utf8'))
#        return hash_key.hexdigest()
    
    
    def deploy_token(self, token):
        self._set_state(self.ENROLL_STATE_DEPLOYING)
        self.selected_token = token
        token["check_status"] = True
        # do rest in a thread..?
#        plugin_name = token.get("token_type")
#        token_id = token.get("token_id")
#        self.endpoint_plugin_socket_token.set_serial(plugin_name, token_id, self.calc_hash_key(token))
#        self.endpoint_plugin_socket_token.deploy_token(plugin_name, token_id, self.client_known_secret, self.servers)
#        self.endpoint_plugin_socket_token.generate_keypair(plugin_name, token_id)
#        token["token_serial"] = self.endpoint_plugin_socket_token.get_serial(plugin_name, token_id) 
#        token["token_public_key"] = self.endpoint_plugin_socket_token.get_public_key(plugin_name, token_id)
        
        self.tunnelendpoint_remote('enroll_token', token=token)

    def deploy_token_cb(self, token):
        plugin_name = token.get("token_type")
        self._user_interface.update.set_info_progress_complete(1)
        token_id = token.get("token_id")
        self.endpoint_plugin_socket_token.set_serial(plugin_name, token_id, self.calc_hash_key(token))
        self._user_interface.update.set_info_progress_complete(5)
        self.endpoint_plugin_socket_token.deploy_token(plugin_name, token_id, self.client_known_secret, self.servers)
        self._user_interface.update.set_info_progress_complete(25)
        self.endpoint_plugin_socket_token.generate_keypair(plugin_name, token_id)
        self._user_interface.update.set_info_progress_complete(40)
        token["token_serial"] = self.endpoint_plugin_socket_token.get_serial(plugin_name, token_id) 
        token["token_public_key"] = self.endpoint_plugin_socket_token.get_public_key(plugin_name, token_id)
        self.endpoint_plugin_socket_token.set_enrolled(plugin_name, token_id)
        self.selected_token = token
        self._user_interface.update.set_info_progress_complete(50)

        self.tunnelendpoint_remote('enroll_deployed_token', token=token)


    
    def launch_enrollment(self, licensed):
        if self._enroll_state != self.ENROLL_STATE_NONE:
            self._user_interface.update.view.display()
            return
        
        self.init_gui()
        
        if licensed:
            self._set_state(self.ENROLL_STATE_STARTED)
            self._user_interface.update.view.display()
            self._get_token_status()
        else:
            self.finished_with_error = True
            self._user_interface.update.set_info_headline(self.dictionary._("Missing license for Field Enrollment"))
            self._set_state(self.ENROLL_STATE_FINISHED)
            self._user_interface.update.view.display()
            
        

    def _get_token_status(self):
        tokens_socket = self.endpoint_plugin_socket_token.get_tokens_by_distance(self.client_runtime_env)
        tokens = []
        for token_socket in tokens_socket:
            token = token_socket.to_dict()
            token['image_filename'] = token_socket.get_image_filename(image_path=self._user_interface.get_image_path())
            token_status = token.get("token_status") 
            if token_status == Token.TOKEN_STATUS_INITIALIZED:
                token["status_text"] = self.dictionary._("This Token is ready for enrollment")
                if token_socket.token_serial:
                    token["check_status"] = True
                else:
                    token["check_status"] = False
                token["can_deploy"] = True
            elif token_status == Token.TOKEN_STATUS_BURNED:
                token["status_text"] = self.dictionary._("This Token is broken")
                token["check_status"] = False
                token["can_deploy"] = False
            else:
                token["check_status"] = True
                
            if token['token_type'] not in [COMPUTER_TOKEN]:
                tokens.append(token)

        self.tunnelendpoint_remote('get_token_status', tokens=tokens)
        
    WAIT_FOR_STATUS_SECONDS = 30
    WAIT_PROGRESS_PER_SECOND = 50.0/WAIT_FOR_STATUS_SECONDS
        
    def get_token_status_reply(self, tokens, errormsg):
        if self._enroll_state == self.ENROLL_STATE_DEPLOYING:
            if errormsg:
                enroll_result = errormsg
                headline = self.dictionary._("Enrollment Failed")
                self.finished_with_error = True
            else:
                enroll_result = tokens[0].get("status_text")
                if enroll_result is None:
                    if self.seconds_waited > self.WAIT_FOR_STATUS_SECONDS:
                        enroll_result = self.dictionary.gettext_id('ENROLLMENT::NO_RESULT_FROM_MGMT')
                    else:
                        self._user_interface.update.set_info_progress_complete(50 + self.seconds_waited * self.WAIT_PROGRESS_PER_SECOND)
                        time.sleep(1)
                        self.seconds_waited += 1
                        self.tunnelendpoint_remote('get_token_status', tokens=[self.selected_token])
                        return
                headline = self.dictionary._("Token Enrollment Status: %s") % tokens[0].get("token_title") 
                
            self._user_interface.update.set_info_progress_complete(100)
            self._user_interface.update.set_info_headline(headline)
            self._user_interface.update.set_info_free_text(enroll_result)
            self._set_state(self.ENROLL_STATE_FINISHED)
        else:
            if errormsg:
                self._user_interface.update.set_info_headline(errormsg)
                self.finished_with_error = True
                self._set_state(self.ENROLL_STATE_FINISHED)
            else:
                self._init_enrollment(tokens)
        
        
    def _add_to_selection_list(self, token, selection_list, index):
        token['id'] = index
        token['selected'] = index == 0
#        token['name'] = "%s (%s)" % (token.get("token_id") if token.get("token_id") else "Desktop", token.get("token_title"))
        token['name'] = "%s" % (token.get("token_title"))
        token['details'] = token.get("status_text")
        token['icon'] = token.get("image_filename")
        selection_list.append(token)
        
        
    def _init_enrollment(self, tokens):
        self._undeployed_tokens = []
        self._deployed_tokens = []
        for token in tokens:
            if token.get("can_deploy"):
                self._undeployed_tokens.append(token)
            else:
                self._deployed_tokens.append(token)
            if not token.get("status_text"):
                token["status_text"] = self.dictionary._("This Token is ready for enrollment")
                
        selection_list = []
        index = 0
        
        for token in self._undeployed_tokens:
            self._add_to_selection_list(token, selection_list, index)
            index += 1
            
        for token in self._deployed_tokens:
            self._add_to_selection_list(token, selection_list, index)
            index += 1

        self._user_interface.update.set_selection_list([])
        self._user_interface.update.set_selection_list(selection_list, autosort=False)
        if index:
            self._user_interface.update.set_selection_highlighted_by_id(-1)
            self._user_interface.update.set_selection_highlighted_by_id(0)
                
        self._set_state(self.ENROLL_STATE_INITIALIZED)

        
    def enroll_reply(self, rc, msg):
        if rc:
            self._user_interface.update.set_info_progress_subtext(msg)            
            time.sleep(1)
            self.seconds_waited = 1
            self.selected_token["check_status"] = True
            self.tunnelendpoint_remote('get_token_status', tokens=[self.selected_token])
        else:
            self._user_interface.update.set_info_headline(self.dictionary._('Enrollment Failed'))
            self._user_interface.update.set_info_free_text(msg)
            self.finished_with_error = True
            self._set_state(self.ENROLL_STATE_FINISHED)
            
    def _set_phase(self, state):
        found_state = False
        for phase in self.phases:
            if phase.get("enroll_state")==state:
                phase['state'] = UpdateController.ID_PHASE_ACTIVE
                found_state = True
            else:
                if found_state:
                    phase['state'] = UpdateController.ID_PHASE_PENDING
                else:
                    phase['state'] = UpdateController.ID_PHASE_COMPLETE
        self._user_interface.update.set_phase_list(self.phases)            

    def _set_state(self, state):
        self._enroll_state = state
        self._set_phase(state)
        if state == self.ENROLL_STATE_STARTED:
            self._user_interface.update.set_info_mode(UpdateController.ID_MODE_INFORMATION_PROGRESS)
            self._user_interface.update.set_info_progress_mode(UpdateController.ID_MODE_PROGRESS_UNKNOWN)
            self._user_interface.update.set_info_headline(self.dictionary._("Detecting token status"))
            self._user_interface.update.set_info_progress_subtext('')
            self._user_interface.update.set_info_free_text('')
            self._user_interface.update.set_cancel_allowed(True)
            self._user_interface.update.set_next_allowed(False)
            self._user_interface.update.set_cancel_description('')
            self._user_interface.update.set_next_description('')
            self._user_interface.update.set_show_selection_list(False)
        elif state == self.ENROLL_STATE_INITIALIZED:
            self._user_interface.update.set_info_mode(UpdateController.ID_MODE_INFORMATION_FREE_TEXT)
            selection_list = self._user_interface.update.get_selection_list() 
            if selection_list:
                self.show_selected_token_info()
                if len(selection_list) == 1:
                    self._user_interface.update.set_show_selection_list(False)
                else:
                    self._user_interface.update.set_show_selection_list(True)
                
                self._user_interface.update.set_cancel_allowed(True)
                self._user_interface.update.set_cancel_description('Cancel')
                self._user_interface.update.set_next_description('Enroll')
            else:
                self._user_interface.update.set_info_headline(self.dictionary._('No tokens found'))
                self._user_interface.update.set_info_free_text('')
                self._user_interface.update.set_cancel_allowed(True)
                self._user_interface.update.set_next_allowed(False)
                self._user_interface.update.set_cancel_description('')
                self._user_interface.update.set_next_description('')
        elif state == self.ENROLL_STATE_DEPLOYING:
            self._user_interface.update.set_show_selection_list(False)
            self._user_interface.update.set_info_progress_complete(0)            
            self._user_interface.update.set_info_headline('Enrolling...')
            self._user_interface.update.set_info_mode(UpdateController.ID_MODE_INFORMATION_PROGRESS)
            self._user_interface.update.set_info_progress_mode(UpdateController.ID_MODE_PROGRESS_KNOWN)
            self._user_interface.update.set_info_progress_subtext(self.dictionary._('Updating token'))
            self._user_interface.update.set_cancel_allowed(False)
            self._user_interface.update.set_next_allowed(False)
                
        elif state == self.ENROLL_STATE_FINISHED:
            self._user_interface.update.set_info_mode(UpdateController.ID_MODE_INFORMATION_FREE_TEXT)                
            self._user_interface.update.set_show_selection_list(False)
            self._user_interface.update.set_cancel_allowed(True)
            self._user_interface.update.set_next_allowed(True)
        elif state == self.ENROLL_STATE_NONE:
            self._user_interface.update.set_info_mode(UpdateController.ID_MODE_INFORMATION_FREE_TEXT)
            self._user_interface.update.set_info_headline("")
            self._user_interface.update.set_info_free_text('')
            self._user_interface.update.set_cancel_allowed(True)
            self._user_interface.update.set_next_allowed(False)
            self.selected_index = -1
            
            
        

    def show_selected_token_info(self):
        index = self._user_interface.update.get_selection_highlighted_id()
        if index != self.selected_index:
            self.selected_index = index
            selection_list = self._user_interface.update.get_selection_list()
            if index is not None and index < len(selection_list):
                token = selection_list[index]
                self._user_interface.update.set_next_allowed(token.get("can_deploy"))
                self._user_interface.update.set_info_headline(token.get("token_title"))
                self._user_interface.update.set_info_free_text(token.get("status_text"))
            else:
                self._user_interface.update.set_info_headline(self.dictionary._("Enroll Token"))
                self._user_interface.update.set_info_free_text("")

        
    def _gui_on_update(self):
        if self._user_interface.update.get_cancel_clicked():
            self._user_interface.update.reset_cancel_clicked()
            self.exit_gui()
        if self._enroll_state == self.ENROLL_STATE_INITIALIZED:
            if self._user_interface.update.get_next_clicked():
                self._user_interface.update.reset_next_clicked()
                index = self._user_interface.update.get_selection_highlighted_id()
                if not index is None:
                    if index < len(self._undeployed_tokens):
                        token = self._undeployed_tokens[index]
                        self.deploy_token(token)
                    else:
                        self._user_interface.update.set_info_headline(self.dictionary._('Request denied'))
                        self._user_interface.update.set_info_free_text(self.dictionary._('Enrollment not allowed for this token'))
                        self._set_state(self.ENROLL_STATE_FINISHED)
            else:
                self.show_selected_token_info()
                    
        elif self._enroll_state == self.ENROLL_STATE_FINISHED:
            if self._user_interface.update.get_next_clicked():
                self._user_interface.update.reset_next_clicked()
                self.exit_gui()
                
                # This call will close down the client
                if not self.finished_with_error:
                    self.cb_close()


    def launch_device_enrollment(self, device_session_id):
        try:
            with self.checkpoint_handler.CheckpointScope("launch_device_enrollment", components.endpoint.module_id, checkpoint.DEBUG):
                serial = None
                try:
                    serial = self.endpoint_plugin_socket_token.get_serial(COMPUTER_TOKEN, COMPUTER_TOKEN_ID)
                except plugin_type_token.Error:
                    self.checkpoint_handler.CheckpointExceptionCurrent("register_device_failed", components.endpoint.module_id, checkpoint.DEBUG)
                if serial:
                    if not self.endpoint_plugin_socket_token.is_enrolled(COMPUTER_TOKEN, COMPUTER_TOKEN_ID):
                        public_key = self.endpoint_plugin_socket_token.get_public_key(COMPUTER_TOKEN, COMPUTER_TOKEN_ID)
                        self.tunnelendpoint_remote('register_device', device_session_id=device_session_id, serial=serial, public_key=public_key, token_type=COMPUTER_TOKEN)
                    else:
                        self.register_device_failed("Already enrolled", self.ERROR_CODE_ALREADY_ENROLLED)
                else:
                    self.register_device_failed("Device service not installed", self.ERROR_CODE_NOT_INSTALLED)
        except Exception, e:
            self.register_device_failed(e)
        
    def deploy_device(self, device_session_id):    
        try:
            with self.checkpoint_handler.CheckpointScope("deploy_device", components.endpoint.module_id, checkpoint.DEBUG):
                self.endpoint_plugin_socket_token.initialize_token(COMPUTER_TOKEN, COMPUTER_TOKEN_ID)
                self.endpoint_plugin_socket_token.generate_keypair(COMPUTER_TOKEN, COMPUTER_TOKEN_ID)
                serial = self.endpoint_plugin_socket_token.get_serial(COMPUTER_TOKEN, COMPUTER_TOKEN_ID)
                public_key = self.endpoint_plugin_socket_token.get_public_key(COMPUTER_TOKEN, COMPUTER_TOKEN_ID)
                
                self.tunnelendpoint_remote('register_device', device_session_id=device_session_id, serial=serial, public_key=public_key, token_type=COMPUTER_TOKEN)
        except Exception, e:
            self.register_device_failed(e)
        
    def register_device_failed(self, msg, error_code=-1):
        self.checkpoint_handler.Checkpoint("register_device_failed", components.endpoint.module_id, checkpoint.DEBUG, msg=msg, error_code=error_code)
        if self.has_set_enrolled:
            self.endpoint_plugin_socket_token.reset_enrolled(COMPUTER_TOKEN, COMPUTER_TOKEN_ID)
        self.show_messagebox(self.dictionary._("Device enrollment failed: %s") % msg)
        self.cb_close_with_error(msg)

    def register_device_requested(self, token):
        self.endpoint_plugin_socket_token.set_enrolled(COMPUTER_TOKEN, COMPUTER_TOKEN_ID)
        self.has_set_enrolled = True
        seconds_waited = token.get("seconds_waited", 0)
        print "wait for it...%d" % seconds_waited
        if seconds_waited > self.MAX_WAIT_FOR_DEVICE_STATUS_SECONDS:
            self.register_device_failed("Timeout", self.ERROR_CODE_TIMEOUT)
        time.sleep(1)
        seconds_waited += 1
        token["seconds_waited"] = seconds_waited
        self.tunnelendpoint_remote('get_device_status', token=token)
        
    def register_device_succeded(self):
        self.show_messagebox(self.dictionary._("Device has been enrolled successfully"))
        self.cb_close()
        
    def launch_internal_ready(self):
        """
        Callback from launch-internal framework to examine if the session is ready, if False is return the launch will fail
        """
        with self.checkpoint_handler.CheckpointScope("launch_internal_ready", components.endpoint.module_id, checkpoint.DEBUG):
            return True

    def launch_internal_close(self):
        """
        Callback from launch-internal framework to inform that the tunnelendpoint controlling the launch has been asked to close down
        """
        with self.checkpoint_handler.CheckpointScope("launch_internal_close", components.endpoint.module_id, checkpoint.DEBUG):
            pass

    def launch_internal_remote_closed(self):
        """
        Callback from launch-internal framework to inform that the tunnelendpoint controlling the launch has been closed by remote
        """
        with self.checkpoint_handler.CheckpointScope("launch_internal_remote_closed", components.endpoint.module_id, checkpoint.DEBUG):
            pass
