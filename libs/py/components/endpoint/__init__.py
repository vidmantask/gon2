"""
Endpoint component 
"""
module_id = 'endpoint'
component_id = 'endpoint'

import lib.dictionary

lib.dictionary.deftext_id('ENROLLMENT::NO_RESULT_FROM_MGMT', "Token enrollment has been initiated but could not be completed as expected. " + 
                                                             "Please contact your G/On Administrator for additional information. Please press start G/On again when token enrollment is completed.")
lib.dictionary.deftext_id('ENROLLMENT::ENROLLMENT_REQUESTED_FOR_TOKEN', "Enrollment requested for token '%s'", ('Token name',))
lib.dictionary.deftext_id('ENROLLMENT::ENROLLMENT_NOT_ALLOWED', 'Enrollment is not allowed. Token enrollment status : %s', ('Enrollment status'))
lib.dictionary.deftext_id('ENROLLMENT::TOKEN_ALREADY_REGISTERED', "%s is already enrolled. Please contact your G/On Administrator for other options. Please press 'CANCEL' to cancel this enrollment", ('Token name',))
lib.dictionary.deftext_id('ENROLLMENT::TOKEN_ASSIGNED_TO_YOU_BUT_NOT_ACTIVATED', "Token is registered and is now awaiting assignment to you. Please wait for your G/On Administrator to complete the enrollment. Please exit and start G/On again when token enrollment is completed. The token is enrolled as '%s'.")
lib.dictionary.deftext_id('ENROLLMENT::TOKEN_ASSIGNED_TO_YOU_AND_ACTIVATED', "Token has been assigned to you and is ready for use. The token is enrolled as '%s'. Please exit and start G/On again.")
lib.dictionary.deftext_id('ENROLLMENT::ENROLLED_TOKEN_NOT_KNOWN', "%s is either already enrolled on another G/On server or has previously been enrolled or attempted enrolled on this system. Enrolling this token will disable the existing enrollment. Press 'NEXT' if you want to continue with enrollment or 'CANCEL' to stop.")

