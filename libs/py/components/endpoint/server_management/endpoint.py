"""
Module holding Management Server functionality for the endpoint component
"""
from __future__ import with_statement
import lib.checkpoint

from components.database.server_common import schema_api
from components.database.server_common import database_api

import components.management_message.server_common

import components.endpoint
import components.endpoint.server_common.database_schema as dbs

import components.auth.server_management.rule_api as rule_api

import components.admin_ws.ws.admin_ws_services
from components.rest.rest_api import RestApi
import components.user.server_common.database_schema as user_db

import components.plugin.server_management.plugin_socket_endpoint

from components.plugin.server_common.config_api_lib import ConfigTemplateSpec
import plugin_types.server_management.plugin_type_token as plugin_type_token
import plugin_types.common.plugin_type_token
from components.auth.server_management import OperationNotAllowedException, ElementType, RuleType

from components.admin_ws.config_api import ComponentConfigAPI


import components.management_message.server_management.session_recieve


import datetime
import sys

class EndpointElement(ElementType):

    def __init__(self, endpoint, label):
        self.element_id = endpoint.rule_id
        self.label = label
        self.info = ""
        self.template = None

    def get_id(self):
        return self.element_id

    def get_entity_type(self):
        return "Endpoint"

    def get_label(self):
        return self.label

    def get_info(self):
        return self.info

    def get_template(self):
        return self.template

class DeviceElement(ElementType):

    def __init__(self, endpoint, label):
        self.element_id = endpoint.id
        self.label = label
        self.info = ""
        self.template = None

    def get_id(self):
        return self.element_id

    def get_entity_type(self):
        return "Device"

    def get_label(self):
        return self.label

    def get_info(self):
        return self.info

    def get_template(self):
        return self.template

class TokenElement(ElementType):

    def __init__(self, device_id, device_plugin, device_name, device_entity_type):
        self.element_id = "%s.%s" % (device_plugin, device_id)
        self.label = device_name
        self.device_entity_type = "%s.%s" % (device_entity_type, device_entity_type)

    def get_id(self):
        return self.element_id

    def get_entity_type(self):
        return self.device_entity_type

    def get_label(self):
        return self.label



class HardwareCheck(ElementType):

    def __init__(self, endpoint, device_name, entity_type):
        self.element_id = "%s" % (endpoint.id)
        self.label = "%s" % (device_name)
        self.entity_type = "%s.%s" % (entity_type, entity_type)


    def get_id(self):
        return self.element_id

    def get_entity_type(self):
        return self.entity_type

    def get_label(self):
        return self.label



class EndpointRule(RuleType):

    def __init__(self, endpoint, device_id, device_plugin, device_name, device_entity_type):
        self.result_element = EndpointElement(endpoint, device_name)
        self.conditions = [TokenElement(device_id, device_plugin, device_name, device_entity_type),
                           HardwareCheck(endpoint, device_name, "HardwareCheck")]


    def get_id(self):
        return self.result_element.get_id()

    def get_result_element(self):
        return self.result_element

    def get_condition_elements(self):
        return self.conditions

    def is_active(self):
        return True




class EndpointAdmin(components.management_message.server_management.session_recieve.ManagementMessageSessionRecieverBase, ComponentConfigAPI):

    default_hardware_plugins = ['endpoint_mac']

    def __init__(self, environment, server_config, license_handler, activity_log_handler, internal_ws_connector=None):
        components.management_message.server_management.session_recieve.ManagementMessageSessionRecieverBase.__init__(self, environment.checkpoint_handler, components.endpoint.component_id)
        self.plugin_socket_endpoint = components.plugin.server_management.plugin_socket_endpoint.PluginSocket(environment.plugin_manager)
        self.activity_log_handler = activity_log_handler
        self.environment = environment
        self.internal_ws_connector = internal_ws_connector
        self._license_handler = license_handler
        self._server_config = server_config

    def get_attribute_filter(self):
        filter_dict = dict()
        for plugin in self.plugin_socket_endpoint.get_plugins():
            filter = plugin.get_attribute_filter()
            if filter:
                filter_dict.update(filter)
        return filter_dict



    def create_endpoint(self, device_id, device_plugin, device_entity_type, unique_session_id, transaction):
        new_endpoint = transaction.add(dbs.RegisteredEndpoint())
        new_endpoint.token_serial = device_id
        new_endpoint.token_plugin = device_plugin
        now = datetime.datetime.today()
        new_endpoint.deployed = now
        new_endpoint.last_seen = now
        new_endpoint.create_attributes(unique_session_id, self.get_attribute_filter(), transaction)
        token = plugin_types.common.plugin_type_token.lookup_token(transaction, device_plugin, device_id)

        if device_entity_type != "DeviceToken":
            device_name = token.name
            endpoint_rule = EndpointRule(new_endpoint, device_id, device_plugin, device_name, device_entity_type)
            if self._server_config.enrollment_device_hardware_check:
                for plugin_name in self.default_hardware_plugins:
                    hardware_check = transaction.add(dbs.HardwareCheckPlugin())
                    hardware_check.endpoint = new_endpoint
                    hardware_check.plugin_name = plugin_name

            new_endpoint.rule_id = rule_api.create_simple_rule(transaction, endpoint_rule)
            new_endpoint_id = new_endpoint.rule_id
            entity_type = endpoint_rule.get_result_element().get_entity_type()
        else:
            attributes = new_endpoint.get_attribute_dict()
            machine_names = attributes.get("machine_name")
            machine_name = machine_names[0] if machine_names else ""
            if machine_name:
                token.name = plugin_types.common.plugin_type_token.get_unique_token_name(transaction, machine_name)
                device_name = token.name
            else:
                device_name = token.name

            new_endpoint_id = new_endpoint.id
            entity_type = "Device"

        # activity log
        entity_type_title = rule_api.get_entity_type_title(device_entity_type)
        lines = []
        lines.append("name : %s" % device_name)
        lines.append("serial : %s" % device_id)
        lines.append("plugin : %s" % device_plugin)
        lines.append("unique_session_id : %s" % unique_session_id)
        details_string = "%s (%s):\n\n%s" % (entity_type_title, device_id, "\n".join(lines))

        self.activity_log_handler.copyUpdateAndAdd(transaction,
                                                   type="Create",
                                                 summary="Create %s" % entity_type_title,
                                                  details = details_string,
                                                  access_right = device_entity_type,
                                                  )


        return new_endpoint_id, device_name, entity_type


    def call_enroll_token(self, session_id, template, user, enroll_as_endpoint, activate_rule, unique_session_id):
        request = RestApi.Admin.EnrollDeviceToUser()
        request.set_session_id(session_id)
        request.set_external_user_id(user.external_user_id)
        request.set_user_plugin(user.user_plugin)
        request.set_device_template(template)
        request.set_activate_rule(activate_rule)
        request.set_enroll_as_endpoint(enroll_as_endpoint)
        request.set_unique_session_id(unique_session_id)
        response = self.internal_ws_connector.get_admin_ws_server().EnrollDeviceToUser(request)
        return response

    def call_enroll_device(self, session_id, template, unique_session_id, group_name, deactivate_rule):
        request = RestApi.Admin.EnrollDevice()
        request.set_session_id(session_id)
        request.set_group_name(group_name)
        request.set_device_template(template)
        request.set_unique_session_id(unique_session_id)
        activate_rule = False if deactivate_rule else True
        request.set_activate_rule(activate_rule)
        response = self.internal_ws_connector.get_admin_ws_server().EnrollDevice(request)
        return response



    def get_template(self, session_id, entity_type):
        request = RestApi.Admin.GetConfigTemplate()
        request.set_session_id(session_id)
        request.set_entity_type(entity_type)
        response = self.internal_ws_connector.get_admin_ws_server().GetConfigTemplate(request)
        return response.get_config_template

    def test(self, session_id, entity_type):
        request = RestApi.Admin.GetModuleElements()
        request.set_session_id(session_id)
        request.set_element_type(entity_type)
        request.set_refresh_cache(False)
        response = self.internal_ws_connector.get_admin_ws_server().GetModuleElements(request)
        print len(response.get_elements)

    def test1(self, session_id, template, entity_type):
        request = RestApi.Admin.CreateConfigTemplateRow()
        request.set_session_id(session_id)
        request.set_entity_type(entity_type)
        request.set_template(template)
        response = self.internal_ws_connector.get_admin_ws_server().CreateConfigTemplateRow(request)

    def log_failed_enrollment(self, template, unique_session_id, message):
        config_spec = ConfigTemplateSpec(template)
        token_serial = config_spec.get_value(plugin_type_token.IDENT_SERIAL)
        plugin_name = config_spec.get_value(plugin_type_token.IDENT_TYPE)
        dbs.log_failed_enrollment(plugin_name, token_serial, unique_session_id, message)


    def enroll_user_tokens(self, user_ids, token_plugin, unique_session_id, server_sid):
       import uuid
       count = 0
       for user_id in user_ids:
            print "enroll for user %s" % user_id
            token = dict()
            token["token_type"] = token_plugin
            token["token_serial"] = str(uuid.uuid1())
            token["token_type_title"] = token_plugin
            token["token_public_key"] = "%s" % count
            count += 1
            self.enroll_token(token, user_id, False, unique_session_id)

    def enroll_token(self, token, user_id, enroll_as_endpoint, unique_session_id, server_sid):
        try:
            license = self._license_handler.get_license()
            field_enrollment_enabled = license.has('Feature', 'Field Enrollment')
            if not field_enrollment_enabled:
                plugin_name = token.get("token_type")
                token_serial = token.get("token_serial")
                message = "Missing license for Field Enrollment"
                dbs.log_failed_enrollment(plugin_name, token_serial, unique_session_id, message)
                return

            session_id = self.internal_ws_connector.create_session()
            try:
                with database_api.ReadonlySession() as db_session:
                    token_plugin = token.get("token_type")
                    entity_type = rule_api.get_element_type_for_plugin_type(token_plugin, token.get("token_internal_type"))

                    user = user_db.get_user_by_internal_id(db_session, user_id)
                    if not user:
                        raise Exception("User '%s' not found" % user_id)

                    template = self.get_template(session_id, entity_type)
                    template = ConfigTemplateSpec.copy_template(template)

    #                template = self._get_config_spec().get_template()
    #                template.set_attribute_entity_type(entity_type)

                    activate_rule = self._server_config.enrollment_activate_assignment_rule

                    config_spec = ConfigTemplateSpec(template)
                    config_spec.set_value(plugin_type_token.IDENT_NAME, token.get("token_type_title"))
                    config_spec.set_value(plugin_type_token.IDENT_SERIAL, token.get("token_serial"))
                    config_spec.set_value(plugin_type_token.IDENT_TYPE, token.get("token_type"))
                    config_spec.set_value(plugin_type_token.IDENT_INTERNAL_TYPE, token.get("token_internal_type"))
                    config_spec.set_value(plugin_type_token.IDENT_PUBLIC_KEY, token.get("token_public_key"))
                    config_spec.set_value(plugin_type_token.IDENT_DESCRIPTION, "Enrolled by %s" % user.user_login)
                    config_spec.set_value(plugin_type_token.IDENT_ENDPOINT, enroll_as_endpoint)
                    config_spec.add_field("unique_session_id", "unique_session_id", ConfigTemplateSpec.VALUE_TYPE_STRING, field_type=ConfigTemplateSpec.FIELD_TYPE_HIDDEN, value=unique_session_id)

                    response = self.call_enroll_token(session_id, config_spec.get_template(), user, enroll_as_endpoint, activate_rule, unique_session_id)

                    if response._content.get_element_rc():
                        try:
                            with database_api.Transaction() as dbt:
                                dbs.clear_failed_enrollment(token.get("token_type"), token.get("token_serial"), dbt)
                        except Exception, e:
                            (etype, evalue, etrace) = sys.exc_info()
                            self.checkpoint_handler.CheckpointException("Endpoint.error", "clear_failed_enrollment", lib.checkpoint.ERROR, etype, evalue, etrace)

            finally:
                self.internal_ws_connector.close_session(session_id)


        except Exception, e:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("Endpoint.error", "enroll_token", lib.checkpoint.ERROR, etype, evalue, etrace)
            dbs.log_failed_enrollment(token.get("token_type"), token.get("token_serial"), unique_session_id, "Internal Error during enrollment")


    def enroll_device(self, token, unique_session_id, user_login, group_name, deactivate_rule, server_sid):
        try:

            session_id = self.internal_ws_connector.create_session()
            try:
                template = self.get_template(session_id, "DeviceToken")
                template = ConfigTemplateSpec.copy_template(template)


                config_spec = ConfigTemplateSpec(template)
                config_spec.set_value(plugin_type_token.IDENT_NAME, token.get("token_type_title"))
                config_spec.set_value(plugin_type_token.IDENT_SERIAL, token.get("token_serial"))
                config_spec.set_value(plugin_type_token.IDENT_TYPE, token.get("token_type"))
                config_spec.set_value(plugin_type_token.IDENT_INTERNAL_TYPE, token.get("token_internal_type"))
                config_spec.set_value(plugin_type_token.IDENT_PUBLIC_KEY, token.get("token_public_key"))
                config_spec.set_value(plugin_type_token.IDENT_DESCRIPTION, "Enrolled by %s" % user_login)
                config_spec.set_value(plugin_type_token.IDENT_ENDPOINT, True)
                config_spec.set_value(plugin_type_token.IDENT_LICENSED, False)
                config_spec.add_field("unique_session_id", "unique_session_id", ConfigTemplateSpec.VALUE_TYPE_STRING, field_type=ConfigTemplateSpec.FIELD_TYPE_HIDDEN, value=unique_session_id)

                response = self.call_enroll_device(session_id, config_spec.get_template(), unique_session_id, group_name, deactivate_rule)

            finally:
                self.internal_ws_connector.close_session(session_id)


        except Exception, e:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("Endpoint.error", "enroll_device", lib.checkpoint.ERROR, etype, evalue, etrace)


    def get_specific_elements(self, internal_element_type, element_ids):

        with database_api.ReadonlySession() as db_session:
            if internal_element_type == "endpoint":
                endpoints = self._fetch_registered_endpoints(element_ids=element_ids)
                elements = []
                for endpoint_obj in endpoints:
                    element = self._create_element(endpoint_obj)
                    elements.append(element)
                return elements
            elif internal_element_type == "device":
                devices = self._fetch_registered_devices(element_ids=element_ids)
                elements = []
                for device_obj in devices:
                    element = self._create_device_element(device_obj)
                    elements.append(element)
                return elements
            else:
                raise Exception("Unknown element type '%s'" % element_type)





    # CONFIG API


    def _set_element_info(self, endpoint_obj, element):
        attributes = endpoint_obj.get_attribute_dict()
        machine_names = attributes.get("machine_name")
        machine_name = machine_names[0] if machine_names else ""
        account_names = attributes.get("machine_local_account")
        account_name = account_names[0] if account_names else ""
        if machine_name and account_name:
            element.info = "%s : %s" % (machine_name, account_name)
        elif machine_name:
            element.info = machine_name
        else:
            element.info = account_name
    #        config_spec = self._get_config_template(endpoint_obj, token)
    #        element.template = config_spec.get_template()

    def _create_element(self, endpoint_obj):
#        token = self._get_endpoint_token(endpoint_obj, db_session)
#        token_name = token.name
        element = EndpointElement(endpoint_obj, endpoint_obj.token.name)
        self._set_element_info(endpoint_obj, element)

        return element

    def _create_device_element(self, endpoint_obj):
        element = DeviceElement(endpoint_obj, endpoint_obj.token.name)
        attributes = endpoint_obj.get_attribute_dict()
        machine_domains = attributes.get("machine_domain")
        machine_domain = machine_domains[0] if machine_domains else ""
        os_platforms = attributes.get("os_platform")
        os_platform = os_platforms[0] if os_platforms else ""
        element.info = "%s %s" % (os_platform, machine_domain)

        return element

    def _fetch_registered_endpoints(self, search_filter=None, element_ids=None):
        if element_ids:
            filter = database_api.in_(dbs.RegisteredEndpoint.rule_id, element_ids)
        else:
            filter = database_api.not_(dbs.RegisteredEndpoint.rule_id == None)

        return self._fetch_endpoint_data(filter)


    def _fetch_endpoint_data(self, filter):
        db_session = database_api.QuerySession()
        endpoint_select = db_session.select_expression(dbs.RegisteredEndpoint.id, filter=filter)
        endpoints = db_session.select(dbs.RegisteredEndpoint, filter=filter, result_class=dbs.RegisteredEndpointBase)
        endpoint_dict = dict([(e.id, e) for e in endpoints])
        filter = database_api.in_(dbs.RegisteredEndpointAttribute.endpoint_id, endpoint_select)
        attributes = db_session.select(dbs.RegisteredEndpointAttribute, filter=filter)
        for a in attributes:
            endpoint = endpoint_dict.get(a.endpoint_id)
            if endpoint:
                endpoint.attributes.append(a)
    #        if element_ids:
    #            filter = database_api.in_(dbs.HardwareCheckPlugin.endpoint_id, [e.id for e in endpoints])
    #        else:
    #            filter=None

        filter = database_api.in_(dbs.HardwareCheckPlugin.endpoint_id, endpoint_select)
        hw_plugins = db_session.select(dbs.HardwareCheckPlugin, filter=filter)
        for a in hw_plugins:
            endpoint = endpoint_dict.get(a.endpoint_id)
            if endpoint:
                endpoint.hardware_check_plugins.append(a)

        for endpoint_obj in endpoints:
            endpoint_obj.token = plugin_types.common.plugin_type_token.lookup_token(db_session, endpoint_obj.token_plugin, endpoint_obj.token_serial)

        return endpoints

    def _fetch_registered_devices(self, search_filter=None, element_ids=None):
        if element_ids:
            filter = database_api.in_(dbs.RegisteredEndpoint.id, element_ids)
        else:
            filter = dbs.RegisteredEndpoint.rule_id == None

        return self._fetch_endpoint_data(filter)


    def get_elements(self, element_type, search_filter=None):
        if element_type == "endpoint":
            with self.checkpoint_handler.CheckpointScope("_fetch_registered_endpoints", components.endpoint.module_id, lib.checkpoint.DEBUG):
#                endpoints = db_session.select(dbs.RegisteredEndpoint)
                endpoints = self._fetch_registered_endpoints(search_filter=search_filter)
            elements = []
            for endpoint_obj in endpoints:
                element = self._create_element(endpoint_obj)
                yield element
        elif element_type == "device":
            with self.checkpoint_handler.CheckpointScope("_fetch_registered_devices", components.endpoint.module_id, lib.checkpoint.DEBUG):
#                endpoints = db_session.select(dbs.RegisteredEndpoint)
                devices = self._fetch_registered_devices(search_filter=search_filter)
            elements = []
            for device_obj in devices:
                element = self._create_device_element(device_obj)
                yield element
        else:
            raise Exception("Unknown element type '%s'" % element_type)


    # return editEnabled, deleteEnabled
    def get_config_enabling(self, internal_element_type):
        return True, True

    # return templates for creating elements
    def get_create_templates(self, internal_element_type):
        return []


    def _get_endpoint_name(self, endpoint_obj, db_session):
        token = self._get_endpoint_token(endpoint_obj, db_session)
        if token:
            return token.name
        else:
            return "N/A"

    def _get_endpoint_token(self, endpoint_obj, db_session):
        token_serial = endpoint_obj.token_serial
        token_plugin = endpoint_obj.token_plugin
        return plugin_types.common.plugin_type_token.lookup_token(db_session, token_plugin, token_serial)


    def _get_config_template(self, internal_element_type, endpoint_obj, token):
        token_name = token.name
        token_public_key = token.public_key
        attributes = endpoint_obj.get_attribute_dict()
        config_spec = ConfigTemplateSpec()
        config_spec.init("DeviceTemplate", "Edit Device", "")
        field = config_spec.add_field("token_name", 'Name', ConfigTemplateSpec.VALUE_TYPE_STRING, value=token_name)
        field.set_max_length(200)
        machine_name = attributes.get("machine_name", [""])[0]
        config_spec.add_field("machine_name", 'Machine Name', ConfigTemplateSpec.VALUE_TYPE_STRING, read_only=True, value=machine_name)
        local_account = attributes.get("machine_local_account", [""])[0]

        if internal_element_type == "endpoint":
            config_spec.add_field("machine_local_account", 'Local Account', ConfigTemplateSpec.VALUE_TYPE_STRING, read_only=True, value=local_account)

        machine_domain = attributes.get("machine_domain", [""])[0]
        config_spec.add_field("machine_domain", 'Domain or Workgroup', ConfigTemplateSpec.VALUE_TYPE_STRING, read_only=True, value=machine_domain)
        device_os = attributes.get("os_platform", [""])[0]
        config_spec.add_field("device_os", 'Operating System', ConfigTemplateSpec.VALUE_TYPE_STRING, read_only=True, value=device_os)
        os_version = attributes.get("os_version", [""])[0]
        config_spec.add_field("os_version", 'OS version', ConfigTemplateSpec.VALUE_TYPE_STRING, read_only=True, value=os_version)
        config_spec.add_field("created", 'Created', ConfigTemplateSpec.VALUE_TYPE_DATETIME, read_only=True, value=token.created)
        config_spec.add_field("last_seen", 'Last seen', ConfigTemplateSpec.VALUE_TYPE_DATETIME, read_only=True, value=token.last_seen)

        config_spec.add_field("token_public_key", 'Public Key', ConfigTemplateSpec.VALUE_TYPE_STRING, read_only=True, value=token_public_key)


        if internal_element_type == "endpoint":
            mac_adresses = ",".join(attributes.get("net_mac", []))
            config_spec.add_field("mac_adresses", 'MAC addresses', ConfigTemplateSpec.VALUE_TYPE_TEXT, read_only=True, value=mac_adresses)
            enabled_plugins = [hw_check.plugin_name for hw_check in endpoint_obj.hardware_check_plugins]
            for plugin in self.plugin_socket_endpoint.get_endpoint_authentication_plugins():
                enabled = plugin.plugin_name in enabled_plugins
                plugin.add_config(config_spec, endpoint_obj.id, enabled)

        return config_spec


    def get_registered_endpoint(self, internal_element_type, element_id, db_session):
        if internal_element_type == "endpoint":
            endpoint_obj = db_session.select_one(dbs.RegisteredEndpoint, filter=dbs.RegisteredEndpoint.rule_id == element_id)
        elif internal_element_type == "device":
            endpoint_obj = db_session.select_one(dbs.RegisteredEndpoint, filter=dbs.RegisteredEndpoint.id == element_id)
        else:
            raise Exception("Unknown element type '%s' for element '%s'" % (internal_element_type, element_id))
        return endpoint_obj

    def config_get_template(self, internal_element_type, element_id, custom_template, db_session):
        if not element_id:
            return None
        with database_api.ReadonlySession() as db_session:

            endpoint_obj = self.get_registered_endpoint(internal_element_type, element_id, db_session)

            token = self._get_endpoint_token(endpoint_obj, db_session)
            config_spec = self._get_config_template(internal_element_type, endpoint_obj, token)

            return config_spec.get_template()


    # update element
    def config_update_row(self, internal_element_type, element_id, template, dbt):
        with database_api.SessionWrapper(dbt, read_only=False) as transaction:

            endpoint_obj = self.get_registered_endpoint(internal_element_type, element_id, transaction)
            endpoint_token = self._get_endpoint_token(endpoint_obj, transaction)

            config_spec = ConfigTemplateSpec(template)
            new_name = config_spec.get_value("token_name")
            endpoint_token.name = new_name

            if internal_element_type == "endpoint":
                prev_enabled_plugins = dict()
                for hw_check in endpoint_obj.hardware_check_plugins:
                    prev_enabled_plugins[hw_check.plugin_name] = hw_check

                for plugin_name in self.default_hardware_plugins:
                    plugin = self.plugin_socket_endpoint.get_plugin(plugin_name)
                    if not plugin:
                        raise Exception("Default plugin '%s' for device check not available" % plugin_name)
                    enabled = config_spec.get_value(plugin_name)
                    if enabled:
                        if not plugin_name in prev_enabled_plugins.keys():
                            # create new
                            hardware_check = transaction.add(dbs.HardwareCheckPlugin())
                            hardware_check.endpoint = endpoint_obj
                            hardware_check.plugin_name = plugin_name
                    else:
                        if plugin_name in prev_enabled_plugins.keys():
                            # delete
                            hw_check = prev_enabled_plugins.get(plugin_name)
                            transaction.delete(hw_check)

                rule_api.update_simple_rule(transaction, endpoint_obj.rule_id, new_name)




    # create element
    def config_create_row(self, internal_element_type, template, dbt):
        raise NotImplementedError()

    # delete element
    def config_delete_row(self, internal_element_type, element_id, transaction):
        if internal_element_type == "endpoint":
            rule_api.delete_rule(element_id, transaction)
        endpoint_obj = self.get_registered_endpoint(internal_element_type, element_id, transaction)
        endpoint_token = self._get_endpoint_token(endpoint_obj, transaction)

        transaction.delete(endpoint_token)
        endpoint_obj.delete(transaction)

        return True

    def report_all(self, server_sid, endpoint_seen, unique_session_id, all_endpoint_info):
        with self.checkpoint_handler.CheckpointScope("report_all", components.endpoint.module_id, lib.checkpoint.DEBUG, endpoint_seen=str(endpoint_seen), unique_session_id=unique_session_id, all_endpoint_info=str(all_endpoint_info)):
            endpoint_seen_ts = components.management_message.server_common.from_sink_to_timestamp(endpoint_seen)
            hash_key = dbs.calc_hash_key(self.checkpoint_handler, all_endpoint_info)

            with database_api.Transaction() as dbt:
                endpoint_info = dbt.select_first(dbs.EndpointInfo, filter=(dbs.EndpointInfo.hash_key==hash_key))
                if endpoint_info is not None:
                    self.checkpoint_handler.Checkpoint("report_all.endpoint_known", components.endpoint.module_id, lib.checkpoint.DEBUG)
                    endpoint_info.last_seen = endpoint_seen_ts
                else:
                    self.checkpoint_handler.Checkpoint("report_all.endpoint_new", components.endpoint.module_id, lib.checkpoint.DEBUG)
                    endpoint_info = dbs.EndpointInfo()
                    endpoint_info.hash_key = hash_key
                    endpoint_info.first_seen = endpoint_seen_ts
                    endpoint_info.last_seen = endpoint_seen_ts
                    endpoint_info.create_session_id = unique_session_id
                    dbt.add(endpoint_info)
                    dbt.flush()
                    dbs.create_attributes(dbt, endpoint_info.id, all_endpoint_info)

                endpoint_access_log = dbs.EndpointAccessLog()
                endpoint_access_log.hash_key = hash_key
                endpoint_access_log.unique_session_id = unique_session_id
                dbt.add(endpoint_access_log)

