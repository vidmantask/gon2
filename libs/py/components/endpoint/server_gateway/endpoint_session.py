"""
This module contains the Gateway Server side of a endpoint session
"""
from __future__ import with_statement
import weakref

import datetime
import sys

import lib.checkpoint as checkpoint
import components.communication.tunnel_endpoint_base as tunnel_endpoint_base

from components.management_message.server_common import from_timestamp_to_sink
import components.management_message.server_gateway.session_send

import components.endpoint
import components.endpoint.server_common.database_schema as database_schema 

import plugin_types.server_gateway.plugin_type_endpoint
import components.plugin.server_gateway.plugin_socket_endpoint


from components.database.server_common import database_api
import plugin_types.common.plugin_type_token as token_db

import components.traffic.server_gateway.server_launch_internal


class EndpointManagementMessageSender(object):
    """
    This class represent the interface to the Management Server
    """
    def __init__(self, management_message_session, unique_session_id):
        self._management_message_sender = components.management_message.server_gateway.session_send.ManagementMessageSessionSender(components.endpoint.component_id, management_message_session)
        self.unique_session_id = unique_session_id
        
    def report_all(self, all_endpoint_info):
        """
        Notify the Management Server about the seen endpoint
        """
        endpoint_seen = from_timestamp_to_sink(datetime.datetime.now())
        self._management_message_sender.message_remote('report_all', endpoint_seen=endpoint_seen, unique_session_id=self.unique_session_id, all_endpoint_info=all_endpoint_info)
        
    def enroll_token(self, token, user_id, enroll_as_endpoint):
        self._management_message_sender.message_remote('enroll_token', token=token, user_id=user_id, enroll_as_endpoint=enroll_as_endpoint, unique_session_id=self.unique_session_id)

    def enroll_user_tokens(self, user_ids, token_plugin):
        self._management_message_sender.message_remote('enroll_user_tokens', user_ids=user_ids, token_plugin=token_plugin, unique_session_id=self.unique_session_id)

    def enroll_device(self, token, user_login, group_name, deactivate_rule):
        self._management_message_sender.message_remote('enroll_device', token=token, unique_session_id=self.unique_session_id, user_login=user_login, group_name=group_name, deactivate_rule=deactivate_rule)

#test_create_personal_tokens = False

class EndpointSession(tunnel_endpoint_base.TunnelendpointSession, 
                      components.traffic.server_gateway.server_launch_internal.LaunchSession,
                      plugin_types.server_gateway.plugin_type_endpoint.EndpointSessionPluginCB):
    def __init__(self, async_service, checkpoint_handler, tunnel_endpoint_tunnel, management_message_session, session_info, user_session, plugin_manager, plugins, dictionary, license_handler):
        tunnel_endpoint_base.TunnelendpointSession.__init__(self, async_service, checkpoint_handler, tunnel_endpoint_tunnel)
        self.checkpoint_handler = checkpoint_handler
        self._all_endpoint_info = []
        self.endpoint_info_all_done = False
        self.token_serials = []
        self.plugin_socket_endpoint = components.plugin.server_gateway.plugin_socket_endpoint.PluginSocket(plugin_manager, plugins)
        self.plugin_socket_endpoint.set_endpoint_session(self)
        self._management_message_sender = EndpointManagementMessageSender(management_message_session, session_info.unique_session_id)
        self.user_session = user_session
        self.dictionary = dictionary
        self.auth_session_callback = None
        self.registered_endpoint_cache = dict()
        self.license_handler = license_handler

        self._device_session = dict()
        self._device_session_count = 0
        self.machine_name = "Unknown machine name"
        self.authenticated_device_ids = []

#        if test_create_personal_tokens:
#            user_ids = user_session.get_all_user_ids()
#            self._management_message_sender.enroll_user_tokens(user_ids, "soft_token")
 

    #NOTE: duplicate on client
    ERROR_CODE_UNKNOWN = -1
    ERROR_CODE_TIMEOUT = 1
    ERROR_CODE_NOT_ADMINISTRATOR = 2
    ERROR_CODE_ALREADY_REGISTERED = 3

    def session_start_and_connected(self):
        """
        This is called when tunnelendpoint_connected and session_start both has been called
        """ 
        with self.checkpoint_handler.CheckpointScope("session_start_and_connected", components.endpoint.module_id, checkpoint.DEBUG):
            pass

    def session_close(self):
        """Hard close of session. Called from main session when communication is terminated"""
        with self.checkpoint_handler.CheckpointScope("session_close", components.endpoint.module_id, checkpoint.DEBUG):
            self.user_session = None
            self.auth_session_callback = None
            self.plugin_socket_endpoint.reset_all()
            self.reset_tunnelendpoint()

            self._all_endpoint_info = []
            self.endpoint_info_all_done = False
            self.token_serials = []
            self.plugin_socket_endpoint = None
            self._management_message_sender =  None
            self.dictionary  = None
            self.registered_endpoint_cache = None
            self.license_handler  = None
            self._device_session = dict()
            self.authenticated_device_ids = []
            
    def set_auth_callback(self, auth_session_callback):
        self.auth_session_callback = weakref.ref(auth_session_callback)

    def reset_auth_callback(self):
        self.auth_session_callback = None

    def start_auth(self):
#    Disabled computer token:
#        return ["computer_token"]
        return []

    def remote_report_some(self, plugin_name, endpoint_info_attributes):
        """
        Called from client when a plugin has som infor to report
        """
        with self.checkpoint_handler.CheckpointScope("remote_report_some", components.endpoint.module_id, checkpoint.DEBUG, plugin_name=plugin_name, endpoint_info_attributes=str(endpoint_info_attributes)):
            self._all_endpoint_info.extend((plugin_name, attribute_name, attribute_value) for (attribute_name, attribute_value) in endpoint_info_attributes)
            self.plugin_socket_endpoint.endpoint_info_report_some(plugin_name, endpoint_info_attributes)
            
        for (name, value) in endpoint_info_attributes:
            if name=="machine_name":
                self.machine_name = value
            
        
    def remote_all_done(self):
        """
        Called from client when all plugin has finished reporting infomation
        """
        with self.checkpoint_handler.CheckpointScope("remote_all_done", components.endpoint.module_id, checkpoint.DEBUG):
            self._management_message_sender.report_all(self._all_endpoint_info)
            self.endpoint_info_all_done = True
            self.plugin_socket_endpoint.endpoint_info_all_done()
            self.check_endpoints()
            self.auth_session_callback().component_ready(self)

    def remote_all_done_hash(self):
        """
        Called from client when all plugin has finished reporting infomation used for calculating endpoint hash value
        """
        with self.checkpoint_handler.CheckpointScope("remote_all_done_hash", components.endpoint.module_id, checkpoint.DEBUG):
            self.endpoint_info_all_done = True
            self.check_endpoints()
            self.auth_session_callback().component_ready(self)
            self._management_message_sender.report_all(self._all_endpoint_info)


    def launch_internal_ready(self):
        """
        Callback from launch-internal framework to examine if the session is ready, if False is return the launch will fail
        """
        with self.checkpoint_handler.CheckpointScope("launch_internal_ready", components.endpoint.module_id, checkpoint.DEBUG):
            return True

   
    def enroll_deployed_token(self, token):
        user_id = self.user_session.get_user_id()
        if user_id:
            try:
                # Hack for old ios clients with type "mobile"
                if token.get("token_type")=="mobile":
                    token_internal_type = token.get("token_internal_type")
                    if not token_internal_type or token_internal_type=="mobile":
                        token["token_internal_type"] = "ios" 
                        
                self.checkpoint_handler.Checkpoint("call enroll_token", components.endpoint.module_id, checkpoint.DEBUG, token_type=token.get("token_type"), enroll_as_endpoint=token.get("token_is_endpoint"))
                self._management_message_sender.enroll_token(token, user_id, token.get("token_is_endpoint"))
                self.tunnelendpoint_remote("enroll_reply", rc=True, msg=self.dictionary.gettext_id('ENROLLMENT::ENROLLMENT_REQUESTED_FOR_TOKEN', token.get("token_title")) + " - " + self.dictionary._("Waiting for reply"))
            except Exception, e:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("EndpointSession.error", components.endpoint.module_id, checkpoint.ERROR, etype, evalue, etrace)
                self.tunnelendpoint_remote("enroll_reply", rc=False, msg=repr(evalue))
        else:
            self.tunnelendpoint_remote("enroll_reply", rc=False, msg=self.dictionary._("Error : No user information found"))

    def enroll_token(self, token):
        user_id = self.user_session.get_user_id()
        if user_id:
            token["check_status"] = True
            [token], errormsg = self._get_token_status([token])
            if errormsg:
                self.tunnelendpoint_remote("enroll_reply", rc=False, msg=errormsg)
            elif not token['can_deploy']:
                self.tunnelendpoint_remote("enroll_reply", rc=False, msg=self.dictionary.gettext_id("ENROLLMENT::ENROLLMENT_NOT_ALLOWED" , token.get("status_text")))
            else:
                self.tunnelendpoint_remote("deploy_token_cb", token=token)
        else:
            self.tunnelendpoint_remote("enroll_reply", rc=False, msg=self.dictionary._("Error : No user information found"))



    def get_token_status(self, tokens):
        tokens, errormsg = self._get_token_status(tokens)
        self.tunnelendpoint_remote("get_token_status_reply", tokens=tokens, errormsg=errormsg)
            
    def _get_token_status(self, tokens):
        try:
            with database_api.QuerySession() as dbs:
                for token in tokens:
                    if token.get("check_status"):
                        # find token in db
                        plugin_name = token.get("token_type")
                        token_serial = token.get("token_serial")
                        db_token = token_db.lookup_token(dbs, plugin_name, token_serial)
                        if db_token:
                            token["can_deploy"] = False
                            title = db_token.name
                            token["token_title"] += " (Enrolled as %s)" % title
                            if self.auth_session_callback():
                                is_endpoint = token.get("token_is_endpoint")
                                activated, assigned_to_current_user = self.auth_session_callback().get_enrollment_status(plugin_name, token_serial, is_endpoint, dbs)
                                token["auth_activated"] = activated
                                token["assigned_to_current_user"] = assigned_to_current_user
                                if assigned_to_current_user:
                                    if activated:
                                        token["status_text"] = self.dictionary.gettext_id('ENROLLMENT::TOKEN_ASSIGNED_TO_YOU_AND_ACTIVATED', title)
                                    else:
                                        token["status_text"] = self.dictionary.gettext_id('ENROLLMENT::TOKEN_ASSIGNED_TO_YOU_BUT_NOT_ACTIVATED', title)
                                else:
                                    token["status_text"] = self.dictionary.gettext_id('ENROLLMENT::TOKEN_ALREADY_REGISTERED', token.get("token_title"))
                                    
                                    
                            else:
                                token["status_text"] = self.dictionary.gettext_id('ENROLLMENT::TOKEN_ALREADY_REGISTERED', token.get("token_title"))
                                
                        else:
                            failed_enrollement = database_schema.get_failed_enrollment(plugin_name, token_serial, dbs)
                            if failed_enrollement:
                                if failed_enrollement.error_message:
                                    
                                    token["status_text"] = self.dictionary._("Enrollment of this token failed. Message from server: '%s'") % failed_enrollement.error_message
                                else:
                                    token["status_text"] = self.dictionary._("Enrollment of this token failed. No error message available from server")
                                token["can_deploy"] = True
                            else:
                                token["status_text"] = None
                                token["can_deploy"] = True
                                
                return tokens, None
        except Exception, e:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("_get_token_status", components.endpoint.module_id, checkpoint.ERROR, etype, evalue, etrace)
            return tokens, repr(e)
            
        
    def is_licensed_for_enrollment(self):
        license = self.license_handler.get_license()
        return license.has('Feature', 'Field Enrollment')
    
    def get_hardware_check_plugins(self, endpoint_id):
        with database_api.QuerySession() as db_session:
            return db_session.select(database_schema.HardwareCheckPlugin, database_schema.HardwareCheckPlugin.endpoint_id==endpoint_id)
    
    def check_condition(self, internal_type_name, value_id, value_title):
        with self.checkpoint_handler.CheckpointScope("check_condition", components.endpoint.module_id, checkpoint.DEBUG, internal_type_name=internal_type_name, value_id=value_id, value_title=value_title):
            if internal_type_name=="hardware_check":
                with database_api.QuerySession() as db_session:
                    endpoint = database_schema.get_registered_endpoint(int(value_id), db_session)
                    result = True
                    hardware_check_plugins = self.get_hardware_check_plugins(endpoint.id)
                    for rec in hardware_check_plugins:
                        plugin_name = rec.plugin_name
                        result = self.plugin_socket_endpoint.check(plugin_name, endpoint.id)
                        if not result:
                            break
                    return result
            elif internal_type_name=="device":
                return int(value_id) in self.authenticated_device_ids
            else:
                self.checkpoint_handler.Checkpoint("check_condition", components.endpoint.module_id, checkpoint.ERROR, message="Unknown condition type '%s'" % internal_type_name)
                return False
                
    
    def check_endpoints(self):
        with database_api.QuerySession() as db_session:
            endpoints = []
            for token_serial in self.token_serials:
                endpoints.extend(database_schema.get_registered_endpoints_by_token_serial(token_serial, db_session))
            self.token_serials = []
            for endpoint in endpoints:
                self.auth_session_callback().component_result_ready("endpoint", "hardware_check", result_dicts=[dict(value=str(endpoint.id))])
            
        

    def alert(self, **kwargs):
        token_serial = kwargs.get("token_serial")
        if token_serial:
            plugin = kwargs.get("plugin")
            if plugin=="endpoint_token":
                self.token_serials.append(token_serial)
                if self.endpoint_info_all_done:
                    self.check_endpoints()
                    self.auth_session_callback().component_ready(self)
            elif plugin=="computer_token":
                with database_api.QuerySession() as db_session:
                    device_obj = database_schema.get_registered_endpoints_by_token(token_serial, plugin, db_session)
                    if device_obj:
                        self.authenticated_device_ids.append(device_obj.id)
                        self.auth_session_callback().component_result_ready("endpoint", "device", result_dicts=[dict(value=str(device_obj.id))])
                if self.endpoint_info_all_done:
                    self.auth_session_callback().component_ready(self)
                
        elif self.endpoint_info_all_done:
            self.auth_session_callback().component_ready(self)
   
    def launch_internal_enroll_start(self):
        """
        Callback from launch-internal framework that the enrollment should start
        """
        with self.checkpoint_handler.CheckpointScope("launch_internal_enroll_start", components.endpoint.module_id, checkpoint.DEBUG):
            self.tunnelendpoint_remote('launch_enrollment', licensed=self.is_licensed_for_enrollment())
                
            
    def launch_internal_close(self):
        """
        Callback from launch-internal framework to inform that the tunnelendpoint controlling the launch has been asked to close down
        """
        with self.checkpoint_handler.CheckpointScope("launch_internal_close", components.endpoint.module_id, checkpoint.DEBUG):
            pass

    def launch_internal_remote_closed(self):
        """
        Callback from launch-internal framework to inform that the tunnelendpoint controlling the launch has been closed by remote
        """
        with self.checkpoint_handler.CheckpointScope("launch_internal_remote_closed", components.endpoint.module_id, checkpoint.DEBUG):
            pass


    def launch_device_enroll_start(self, launch_element):
        """
        Callback from launch-internal framework that device enrollment should start
        """
        with self.checkpoint_handler.CheckpointScope("launch_device_enroll_start", components.endpoint.module_id, checkpoint.DEBUG):
            group_name = launch_element.param_file_template
            deactivate_rule = launch_element.citrix_https

            device_session = dict()
            device_session["group_name"] = group_name
            device_session["deactivate_rule"] = deactivate_rule
            self._device_session[self._device_session_count] = device_session
            self.tunnelendpoint_remote('launch_device_enrollment', device_session_id=self._device_session_count)
            self._device_session_count += 1
                
    def register_device(self, device_session_id, serial, public_key, token_type):
        try:
            device_session = self._device_session.get(device_session_id)
            if device_session:
                group_name = device_session.get("group_name")
                deactivate_rule = device_session.get("deactivate_rule")
            else:
                group_name = None
                deactivate_rule = False
            
            token = dict()
            token["token_type_title"] = self.machine_name
            token["token_serial"] = serial
            token["token_type"] = token_type
            token["token_internal_type"] = token_type
            token["token_public_key"] = public_key
    
            user_login = self.user_session.get_attribute("login")
            if self.endpoint_info_all_done:
                self.call_enroll_device(token, user_login, group_name, deactivate_rule)
            else:
                kwargs=dict()
                kwargs['token'] = token
                kwargs['user_login'] = user_login
                kwargs['group_name'] = group_name
                self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 1, 0, self, 'async_call_enroll_device', kwargs)
                
                
        except Exception, e:
            self.tunnelendpoint_remote('register_device_failed', msg=e)

    def call_enroll_device(self, token, user_login, group_name, deactivate_rule):
        self._management_message_sender.enroll_device(token, user_login, group_name, deactivate_rule)
        self.tunnelendpoint_remote('register_device_requested', token=token)
            
    def async_call_enroll_device(self, kwargs):
        if self.endpoint_info_all_done:
            token = kwargs['token']
            user_login = kwargs['user_login']
            group_name = kwargs['group_name']
            self._management_message_sender.enroll_device(token, user_login, group_name)
            self.tunnelendpoint_remote('register_device_requested', token=token)
        else:
            self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 1, 0, self, 'async_call_enroll_device', kwargs)

    def _get_device_status(self, token):
        try:
            with database_api.QuerySession() as dbs:
                plugin_name = token.get("token_type")
                token_serial = token.get("token_serial")
                db_token = token_db.lookup_token(dbs, plugin_name, token_serial)
                if db_token:
                    return 1, "OK"

                return 0, "Device has previously been enrolled, but was not found on this server"
        except Exception, e:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("_get_token_status", components.endpoint.module_id, checkpoint.ERROR, etype, evalue, etrace)
            return -1, e
    
    def get_device_status(self, token):
        result_code, msg = self._get_device_status(token)
        if result_code==0:
            self.tunnelendpoint_remote('register_device_requested', token=token)
        elif result_code < 0:
            self.tunnelendpoint_remote('register_device_failed', msg=msg)
        else:
            self.tunnelendpoint_remote('register_device_succeded')
        
    def check_device_status(self, device_session_id, serial, token_type):
        token = dict()
        token["token_serial"] = serial
        token["token_type"] = token_type
        token["token_internal_type"] = token_type
        result_code, msg = self._get_device_status(token)
        if result_code==1:
            self.tunnelendpoint_remote('register_device_failed', msg="Device already registered", error_code=self.ERROR_CODE_ALREADY_REGISTERED)
        elif result_code==0:
            self.tunnelendpoint_remote('register_device_failed', msg=msg)
        else:
            self.tunnelendpoint_remote('deploy_device', device_session_id=device_session_id)
        
     
    def get_registered_endpoint_info(self, endpoint_id, attr_name):
        """
        Expected to return information about a registered endpoint
        """
        endpoint = self.registered_endpoint_cache.get(endpoint_id)
        if not endpoint:
            with database_api.ReadonlySession() as db_session:
                endpoint = database_schema.get_registered_endpoint(endpoint_id, db_session)
            if not endpoint:
                raise Exception("Unable to find endpoint with id='%s'" % endpoint_id)
            else:
                self.registered_endpoint_cache[endpoint_id] = endpoint
        return endpoint.get_attribute_dict().get(attr_name, [])
