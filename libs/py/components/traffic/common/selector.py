"""
Module for a singleton thread "select"ing among selectable objects.
Selectable object is anything that implements fileno() returning file descriptor.
When selected the method selected_for_recv is called.
"""

import threading
import time
import asyncore
import select
import socket
import errno

async_map = {} # FIXME: Don't use global

# Module-global variables
_stop = False

def loop(loop_interval):
    """
    The actual blocking selector loop.
    _stop and new selectable are picked up at least each loop_interval.
    Can be started as thread from start() or called directly.
    """
    while not _stop:
        # Thread must loop regularly in order to pick up new selectables and detect stop signal
        if async_map:
            try:
                selector_loop(timeout=loop_interval, count=1)
            except select.error, err:
                print 'selector loop select error: %r' % err # FIXME
                if err[0] in [errno.EBADF, errno.ENOTSOCK]:
                    # A file descriptor suddenly went bad - probably timing issues between asyncore and c++ core
                    # The fd has probably been closed and removed from socket_map while we were processing
                    continue
                print async_map
                raise
            except socket.error, err:
                # FIXME: temporarily debugging
                print 'selector loop socket error: %r' % err # FIXME
                print async_map
                raise
        else: # avoid busy wait on empty map
            time.sleep(loop_interval)
    # loop has terminated - if it _was_ an thread then it can be joined

def stop():
    """
    Ask the the selector thread to stop.
    Must obviously be called from another thread.
    """
    global _stop
    _stop = True


# Module-global variable
_thread = None

def start(loop_interval):
    """
    Start the selector thread.
    """
    global _thread
    assert not _thread, 'thread already started'
    _thread = threading.Thread(target=loop, args=(loop_interval,), name="Selector")
    #_thread.setDaemon(True)
    _thread.start()

def join():
    """
    Wait for the selector thread to stop.
    Start and stop must be called first.
    """
    global _thread
    global _stop
    assert _thread, 'thread not started'
    assert _stop, 'thread not stopped'
    _thread.join()
    _thread = None
    _stop = False


class dispatcher(asyncore.dispatcher):
    def __init__(self, sock=None):
        asyncore.dispatcher.__init__(self, sock, map=async_map)

def selector_loop(timeout=30.0, use_poll=False, count=None):
    asyncore.loop(timeout=timeout, use_poll=use_poll, map=async_map, count=count)


def test():
    import socket

    (the_host, the_port) = '127.0.0.1', 7913
    loop_interval = 1.0

    class Listen(dispatcher):
        def __init__(self):
            dispatcher.__init__(self)
            print 'Listen()'
            self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
            self.bind((the_host, the_port))
            self.listen(1) # allow queue of one

        def handle_accept(self):
            print 'Listen was selected_for_recv'
            channel, addr = self.accept()
            Connected(channel)

    class Connected(dispatcher):
        def __init__(self, channel):
            dispatcher.__init__(self, channel)
            print 'Connected()'
            self.done = False
            self.next_step = 1

        def writable(self):
            #print 'Connected', self, 'writable', self.next_step
            return self.next_step == 1

        def handle_write(self):
            print 'Sending welcome'
            if self.next_step == 1:
                self.send('welcome')
                self.next_step += 1
            else:
                assert False

        def readable(self):
            #print 'Connected', self, 'readable', self.next_step
            return self.next_step in [2]

        def handle_read(self):
            print 'Connected was selected_for_recv'
            if self.next_step == 2:
                chunk = self.recv(117)
                assert chunk == 'cheers' # FIXME: could be partial read...
                self.next_step += 1 # but done
            else:
                assert False

    class Connector(dispatcher):
        def __init__(self):
            dispatcher.__init__(self)
            print 'Connector()'
            self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
            self.connect((the_host, the_port))
            # self._tcp_socket.settimeout(0) # async - shouldn't matter for now ...
            self.next_step = 1
            self.connect_handled = False

        def handle_connect(self):
            print 'Connector connected'
            self.connect_handled = True

        def make_selectable(self):
            assert self.next_step == 1, self.next_step
            self.next_step += 1

        def readable(self):
            #print 'Connector', self, 'readable', self.next_step
            return self.next_step in [2, 3]

        def handle_read(self):
            print 'Connector was selected_for_recv'
            assert self.connect_handled
            if self.next_step == 2:
                chunk = self.recv(3)
                assert chunk == 'wel'
                self.next_step += 1
            elif self.next_step == 3:
                chunk = self.recv(4)
                assert chunk == 'come'
                self.next_step += 1
            else:
                assert False

        def writable(self):
            #print 'Connector', self, 'writable', self.next_step
            return self.next_step in [4]

        def handle_write(self):
            if self.next_step == 4:
                self.send('cheers')
                self.next_step += 1
            else:
                assert False

    Listen() # adds itself to recv_selectable
    c = Connector() # connects to Listen'er, making it selectable
    print 'Starting'
    start(loop_interval) # Start thread WITH selectable
    # l.selected_for_recv new creates Connected which sends 'welcome' back to Connector
    time.sleep(loop_interval/2)
    print 'Stopping'
    stop()
    print 'Joining'
    join()
    print 'Starting'
    start(loop_interval) # Start thread WITHOUT selectable
    time.sleep(loop_interval/2)
    print 'Making Connector selectable'
    c.make_selectable() # now comes a selectable - should be picked up within 1 second
    time.sleep(loop_interval*2)
    print 'Stopping'
    stop()
    print 'Joining'
    join()
    print 'Test completed as expected'

if __name__ == '__main__':
    test()
