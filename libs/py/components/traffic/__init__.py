"""
One of the famous 3.
Responsible for connecting auth engine with user menu and acting based on user choice.
Contains execution of users choice combined with authorization.
"""
module_id = 'traffic'
