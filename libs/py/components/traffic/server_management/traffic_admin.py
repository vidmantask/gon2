from __future__ import with_statement
import lib.checkpoint
import datetime

from components.database.server_common.database_api import *

import components.traffic.server_common.database_schema as database_schema
import components.traffic

from plugin_types.server_management import plugin_type_config

from components.auth.server_management import rule_api
from components.traffic.server_management import traffic_api
from components.dialog.server_management import dialog_api as dialog_api
from components.dialog.server_common import dialog_api as dialog_api1

from components.auth.server_management import OperationNotAllowedException, ElementType

from components.plugin.server_common.config_api_lib import ConfigTemplateSpec

from components.traffic.server_common.database_schema import LaunchElement
from components.templates.server_common.template_api import TemplateHandler
from components.traffic.server_common.database_schema import license_count_name2launch_type_category, launch_type2launch_category, launch_type_categories, launch_type_category2category_number, launch_type_category_license_count_name, launch_type_category_license_name

import components.database.server_common.database_api as database_api

import components.management_message.server_management.session_recieve


def get_tag_generators_string(launch_element):
    return '\n'.join(launch_element.get_tag_generators())


#licenses2launch_types = dict([(value, key) for key, value in launch_type_licenses.items()])

def_traffic_log_settings = { 'log_http_connections' : 'Log HTTP Connections',
                            'log_tcp_connections' : 'Log TCP Connections',
                            }


class TrafficAdmin():

    def __init__(self, environment, template_handler, dialog_admin, license_handler, user_admin, activity_log_manager):
        self._template_handler = template_handler
        self._dialog_admin = dialog_admin
        self.license_handler = license_handler
        self._user_admin = user_admin
        dialog_admin.set_traffic_admin(self)
        user_admin.set_traffic_admin(self)
        self.checkpoint_handler = environment.checkpoint_handler


    def config_get_template(self, element_type, element_id, custom_template, db_session=None):
        if element_type == "_action":
            return self._create_template(element_id, custom_template=custom_template, db_session=db_session)
        elif element_type == "menu_action":
            return self._create_template(element_id, custom_template=custom_template, db_session=db_session)

    def get_create_templates(self, internal_element_type):
        with self.checkpoint_handler.CheckpointScope("get_create_templates", "traffic_admin", lib.checkpoint.DEBUG):
            if internal_element_type == "_action":
                element_templates = [self._create_template(None)]
                element_templates.extend(self._template_handler.get_templates())
                return element_templates
            elif internal_element_type == "menu_action":
                return []

    _launch_element_fields = {

            'action_id' : dict( name='action_id', title = 'action_id', value_type=ConfigTemplateSpec.VALUE_TYPE_INTEGER, field_type=ConfigTemplateSpec.FIELD_TYPE_HIDDEN),
            'command'   : dict( name='command', title = 'Command', value_type=ConfigTemplateSpec.VALUE_TYPE_TEXT),


        }

    def _create_template(self, element_id, custom_template=None, db_session=None):

        element_id = element_id if element_id else -1
        config_spec = ConfigTemplateSpec()
        config_spec.init("action.%s" % (element_id), "(default)", "")

        config_spec.add_field('action_id', 'action_id', ConfigTemplateSpec.VALUE_TYPE_INTEGER, field_type=ConfigTemplateSpec.FIELD_TYPE_HIDDEN)
        field = config_spec.add_field('label', 'Name', ConfigTemplateSpec.VALUE_TYPE_STRING)
        field.set_max_length(40)

        field = config_spec.add_field('menu_title', 'Menu Title', ConfigTemplateSpec.VALUE_TYPE_STRING)
        field.set_max_length(256)

        field = config_spec.add_field('image_id', 'Menu Image ID', ConfigTemplateSpec.VALUE_TYPE_STRING)
        field.set_max_length(256)

        field = config_spec.add_field('launch_type', 'Launch Type', ConfigTemplateSpec.VALUE_TYPE_INTEGER)
#        config_spec.add_selection_choice(field, "Standard", 0)
#        config_spec.add_selection_choice(field, "Citrix", 1)
#        config_spec.add_selection_choice(field, "Internal CPM", 2)
#        config_spec.add_selection_choice(field, "Wake on LAN", 3)

        config_spec.add_field('command', 'Command', ConfigTemplateSpec.VALUE_TYPE_TEXT)
        config_spec.add_field('working_directory', 'Working directory', ConfigTemplateSpec.VALUE_TYPE_TEXT)
        config_spec.add_field('close_command', 'Close Command', ConfigTemplateSpec.VALUE_TYPE_TEXT)

        config_spec.add_field('close_with_process', 'Close with process', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN)
        config_spec.add_field('kill_on_close', 'Kill process on close', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN)
        config_spec.add_field('citrix_https', 'Citrix server https', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN)
        config_spec.add_field('citrix_metaframe_path', 'Citrix MetaFrame path', ConfigTemplateSpec.VALUE_TYPE_STRING)
        config_spec.add_field('citrix_command', 'Citrix wfica command', ConfigTemplateSpec.VALUE_TYPE_TEXT)
        config_spec.add_field('param_file_name', 'param_file name', ConfigTemplateSpec.VALUE_TYPE_STRING)
        config_spec.add_field('param_file_lifetime', 'param_file lifetime', ConfigTemplateSpec.VALUE_TYPE_INTEGER)
        config_spec.add_field('param_file_template', 'param_file content', ConfigTemplateSpec.VALUE_TYPE_TEXT)

        config_spec.add_field('sso_login', 'SSO login', ConfigTemplateSpec.VALUE_TYPE_STRING)
        config_spec.add_field('sso_password', 'SSO password', ConfigTemplateSpec.VALUE_TYPE_STRING)
        config_spec.add_field('sso_domain', 'SSO domain', ConfigTemplateSpec.VALUE_TYPE_STRING)
        config_spec.add_field('dialog_tags', 'Tags', ConfigTemplateSpec.VALUE_TYPE_STRING)
        config_spec.add_field('dialog_tag_generators', 'Tag Generators', ConfigTemplateSpec.VALUE_TYPE_TEXT)

        self.add_logging_fields(config_spec)

        self.add_portforward_fields(config_spec, 0)

        max_portforward_added = 0

        if element_id < 0:
            # set default values
            config_spec.set_value("working_directory", "")
            config_spec.set_value("param_file_template", "")
            config_spec.set_value("citrix_command", "")
            config_spec.set_value("citrix_metaframe_path", "")
            config_spec.set_value("sso_login", "")
            config_spec.set_value("sso_password", "")
            config_spec.set_value("sso_domain", "")
            config_spec.set_value("param_file_name", "")

            config_spec.set_value("close_with_process", False)
            config_spec.set_value("kill_on_close", False)
            config_spec.set_value("citrix_https", False)

            config_spec.set_value("param_file_lifetime", 5)



        else:
            #self.update_config_with_row(config_spec, element_id)
            with SessionWrapper(db_session) as dbs:
                db_row = self._lookup_key(dbs, element_id)
                if db_row:
                    config_spec.set_value('label', db_row.name)
                    config_spec.set_values_from_object(db_row)

                    portforwards = dict()
                    count = 0
                    # We assume that portforward line numnbers are correct
                    for portforward in db_row.portforwards:
                        count += 1
                        self.add_portforward_fields(config_spec, count)
                        portforwards[portforward.line_no] = portforward
                    for (line_no, portforward) in portforwards.items():
                        prefix = "portforward.%s." % line_no
                        config_spec.set_values_from_object(portforward, attr_name_prefix=prefix)

                    max_portforward_added = len(db_row.portforwards)

                    config_spec.set_value('dialog_tags', dialog_api1.get_tag_string(dbs, db_row.id))
                    config_spec.set_value('dialog_tag_generators', get_tag_generators_string(db_row))

                    for log_setting in db_row.log_settings:
                        value = "1" if log_setting.log_setting_enabled else "-1"
                        config_spec.set_value(log_setting.log_setting_name, value)

        if custom_template:
            max_portforward_in_template = 0
            for field in custom_template.get_field:
                field_name = field.get_name
                if field_name.startswith("portforward."):
                    field_parts = field_name.split(".")
                    if len(field_parts)==3:
                        try:
                            line_no = int(field_parts[1])
                            if line_no > max_portforward_in_template:
                                max_portforward_in_template = line_no
                        except ValueError:
                            pass
            for count in range(max_portforward_added+1, max_portforward_in_template+1):
                self.add_portforward_fields(config_spec, count)


        return config_spec.get_template()

    def add_logging_fields(self, config_spec):
        for name, title in def_traffic_log_settings.items():
            field = config_spec.add_field(name, title, ConfigTemplateSpec.VALUE_TYPE_STRING, value="")
            config_spec.add_selection_choice(field, "System Default", "")
            config_spec.add_selection_choice(field, "Enabled", "1")
            config_spec.add_selection_choice(field, "Disabled", "-1")

    def add_portforward_fields(self, config_spec, line_no):
        title_postfix = line_no if line_no else ""
        field = config_spec.add_field('portforward.%s.server_host' % line_no, 'Server Host %s' % title_postfix, ConfigTemplateSpec.VALUE_TYPE_STRING)
        config_spec.add_portscan_action(field, 'portforward.%s.server_port' % line_no)
        config_spec.add_field('portforward.%s.server_port' % line_no, 'Server Port %s' % title_postfix, ConfigTemplateSpec.VALUE_TYPE_INTEGER)
        config_spec.add_field('portforward.%s.client_host' % line_no, 'Client Host %s' % title_postfix, ConfigTemplateSpec.VALUE_TYPE_STRING)
        config_spec.add_field('portforward.%s.client_port' % line_no, 'Client Port %s' % title_postfix, ConfigTemplateSpec.VALUE_TYPE_INTEGER)
        config_spec.add_field('portforward.%s.lock_to_process_pid' % line_no, 'Lock to process PID %s' % title_postfix, ConfigTemplateSpec.VALUE_TYPE_BOOLEAN)
        config_spec.add_field('portforward.%s.sub_processes' % line_no, '- or its sub processes %s' % title_postfix, ConfigTemplateSpec.VALUE_TYPE_BOOLEAN)
        config_spec.add_field('portforward.%s.lock_to_process_name' % line_no, 'Lock to process name %s' % title_postfix, ConfigTemplateSpec.VALUE_TYPE_STRING)


#    def config_get_row(self, element):
#        """
#        The key used in this interface is the action_id
#        """
#        with ReadonlySession() as dbs:
#            return self._config_get_row(dbs, element)
#
#    def _config_get_row(self, dbs, element):
#        """
#        The key used in this interface is the action_id
#        """
#        db_row = self._lookup_key(dbs, element.get_id())
#
#        row = self.config_handler.get_values(db_row)
#        row["label"] = element.get_label()
#        row["dialog_tags"] = dialog_api1.get_tag_string(dbs, db_row.id)
#        row["dialog_tag_generators"] = dialog_api1.get_tag_generators_string(dbs, db_row.id)
#        return row
    def get_number_of_menu_actions(self):
        return QuerySession().select_count(database_schema.LaunchElement, database_schema.LaunchElement.launch_type!=2)

    def get_launch_type_launches(self, license_count_name):
        launch_type_category = license_count_name2launch_type_category.get(license_count_name)
        if launch_type_category is None:
            self.checkpoint_handler.Checkpoint("get_launch_type_launches", "traffic_admin", lib.checkpoint.ERROR, msg="Unknown launch type category", launch_type_category=launch_type_category, license_count_name=license_count_name)
            return -1
        else:
            return QuerySession().select_count(database_schema.LaunchTypeCategoryLaunched, database_schema.LaunchTypeCategoryLaunched.launch_type_category==launch_type_category)


    def get_user_ids_for_launch_type_category(self, launch_type_category):
        category_number = launch_type_category2category_number.get(launch_type_category)
        return dict([ (e.user_id, e.last_launch) for e in QuerySession().select(database_schema.LaunchTypeCategoryLaunched, database_schema.LaunchTypeCategoryLaunched.launch_type_category==category_number)])

    def delete_launch_type_launched_for_user(self, user_id, launch_type_category):
        category_number = launch_type_category2category_number.get(launch_type_category)
        if category_number is None:
            self.checkpoint_handler.Checkpoint("delete_launch_type_launched_for_user", "traffic_admin", lib.checkpoint.ERROR, msg="Unknown launch type category", launch_type_category=launch_type_category, user_id=user_id)
            return False
        with database_api.Transaction() as dbt:
            record = database_schema.lookup_launch_type_category_launched(category_number, user_id, dbt)
            if record:
                dbt.delete(record)
        return True



    def check_license_for_launch_type(self, license, launch_type):
        launch_type_int = int(launch_type)
        launch_type_category = launch_type2launch_category.get(launch_type_int)
        license_feature = launch_type_category_license_name.get(launch_type_category)
        if license_feature:
            if not license.has('Feature', license_feature):
                raise OperationNotAllowedException("Missing license feature '%s'" % license_feature)



    def config_create_row(self, element_type, template, dbt):

        license = self.license_handler.get_license()
        if not license.valid:
            print license.errors
            raise OperationNotAllowedException("Invalid license")

        max_number_of_menu_actions = license.get_int('Number of Menu Items')
        no_of_menu_actions = self.get_number_of_menu_actions()
        if no_of_menu_actions >= max_number_of_menu_actions:
            raise OperationNotAllowedException("Maximum number of licensed Menu Actions (%d) reached" % max_number_of_menu_actions)

        config_spec = ConfigTemplateSpec(template)
        row = config_spec.get_field_value_dict()

        launch_type = row.get("launch_type")
        if launch_type is None:
            raise OperationNotAllowedException("Launch Type must be specified")
        self.check_license_for_launch_type(license, launch_type)

        label = row.get("label")
        launch_element = self.create_launch_element(dbt, label)

        menu_title = row.get("menu_title")
        if not menu_title:
            row["menu_title"] = label
        self._update_db_with_row(dbt, launch_element, row)

        return launch_element.action_id

    def config_get_id(self, row):
        return row.get("action_id")


    def create_launch_element(self, transaction, label):

        auth_element = rule_api.create_action_criteria(label, transaction)

        launch_element = transaction.add(LaunchElement(action_id=auth_element.get_id()))
        launch_element.authentication = auth_element.criteria

        """ Flush in order to obtain id from database """
        transaction.flush()
        return launch_element


    def config_delete_row(self, internal_element_type, element_id, dbt):
        if internal_element_type == "_action":
            return self.delete_launch_element(dbt, element_id)
        return False

    def delete_launch_element(self, transaction, action_id):
        launch_element = transaction.select_first(LaunchElement, filter=LaunchElement.action_id == action_id)
        if not launch_element:
            return False

        # delete authorisation criteria
        rule_api.delete_criteria(launch_element.action_id, db_transaction=transaction)

        """delete template use"""
        self._template_handler.delete_template_use(transaction, action_id)
        #delete portforwards
        for portforward in launch_element.portforwards:
            transaction.delete(portforward)

        for tag_generator in launch_element.tag_generators:
            transaction.delete(tag_generator)

        for log_setting in launch_element.log_settings:
            transaction.delete(log_setting)

        dialog_api.delete_dialog_element(transaction, "%s" % launch_element.id)

        """delete element"""
        transaction.delete(launch_element)

        return True

    def _update_obj_with_row(self, obj, row, transaction):
        for (name,value) in row.items():
            try:
                setattr(obj, name, value)
            except AttributeError, e:
                pass

    def _update_db_with_row(self, transaction, db_row, row):
        """
        Notice that the action_id NOT is updated, this is not a bug
        """
        portforward_dict = dict()
        launch_spec_row = dict()
        log_setting_dict = dict()
        for (name,value) in row.items():
            if name.startswith("portforward."):
                (dummy, line_no_str, field_name) = name.split(".")
                line_no = int(line_no_str)
                portforward_row = portforward_dict.get(line_no, dict())
                # Translate port nulls to 0
                if (field_name == "client_port" or field_name == "server_port") and value is None:
                    value = 0
                portforward_row[field_name] = value
                portforward_dict[line_no] = portforward_row
            elif name in def_traffic_log_settings.keys():
                log_setting_dict[name] = value
            else:
                launch_spec_row[name] = value

        # Don't ever overwrite action_id or id!!
        launch_spec_row.pop("action_id", None)
        launch_spec_row.pop("id", None)
        db_row.name = launch_spec_row.get("label")
        self._update_obj_with_row(db_row, launch_spec_row, transaction)

        # Update existing portforwards
        for portforward in db_row.portforwards:
            try:
                portforward_row = portforward_dict.pop(portforward.line_no)
                self._update_obj_with_row(portforward, portforward_row, transaction)
            except KeyError:
                pass

        # create new portforwards
        for (line_no, portforward_row) in portforward_dict.items():
            if self._check_portforward_row(portforward_row):
                portforward = database_schema.Portforward()
                portforward.line_no = line_no
                transaction.add(portforward)
                self._update_obj_with_row(portforward, portforward_row, transaction)
                portforward.launch_element = db_row
#                portforward.launch_element_id = db_row.id

        if row.has_key("dialog_tags") and not row.get("dialog_tags") is None:
            dialog_api.update_tag_string(transaction, db_row.id, row.get("dialog_tags"))

        if row.has_key("dialog_tag_generators") and not row.get("dialog_tag_generators") is None:
            tag_generators_set = set([tag.strip() for tag in row.get("dialog_tag_generators").splitlines()])
            current_generators = [tag_generator for tag_generator in db_row.tag_generators]
            for tag_generator in current_generators:
                if tag_generators_set:
                    tag_generator_name = tag_generators_set.pop()
                    tag_generator.tag_generator = tag_generator_name
                else:
                    db_row.tag_generators.remove(tag_generator)
                    transaction.delete(tag_generator)
            if tag_generators_set:
                for tag_generator_name in tag_generators_set:
                    tag_generator = database_schema.LaunchTagGenerator()
                    tag_generator.launch_element = db_row
                    tag_generator.tag_generator = tag_generator_name


        # Update log settings
        for log_setting in db_row.log_settings:
            try:
                new_log_setting = log_setting_dict.pop(log_setting.log_setting_name)
                if new_log_setting=="1":
                    log_setting.log_setting_enabled = True
                elif new_log_setting=="-1":
                    log_setting.log_setting_enabled = False
                else:
                    transaction.delete(log_setting)

            except KeyError:
                pass

        # create new log settings
        for name, value in log_setting_dict.items():
            if value:
                log_setting = database_schema.LaunchElementLog()
                log_setting.log_setting_name = name
                log_setting.log_setting_enabled = True if value=="1" else False
                log_setting.launch_element = db_row


        self.check_launch_spec(db_row)

        return row

    def _check_portforward_row(self, row):
        for (name, value) in row.items():
            if value:
                return True
        return False

    def check_launch_spec(self, launch_spec):
        if launch_spec.launch_type == 2: # internal
            if launch_spec.command == "device_enrollment":
                if not launch_spec.param_file_template:
                    raise OperationNotAllowedException("No device group specified")
                if not rule_api.device_group_exists(launch_spec.param_file_template):
                    raise OperationNotAllowedException("No device group named '%s' exists" % launch_spec.param_file_template)



    def config_update_row(self, element_type, element_id, template, dbt):
        """
        Notice that the action_id NOT is updated, this is not a bug
        """
        config_spec = ConfigTemplateSpec(template)
        row = config_spec.get_field_value_dict()
        db_row = self._lookup_key(dbt, element_id)
        if db_row is None:
            raise OperationNotAllowedException("The launch specification does not exist in the database")

        try:
            print database_schema.launch_element2dict(db_row)
        except:
            pass

        self._update_db_with_row(dbt, db_row, row)
        rule_api.update_action_criteria(db_row.action_id, row.get("label"), dbt)
        dialog_api.update_dialog_element(dbt, db_row.id, row.get("label"))




    def config_create_enabled(self, element_type):
        return True

    def config_get_default_create_title(self, element_type):
        return "(default)"

    def _lookup_key(self, dbs, id):
        key = dbs.select_first(database_schema.LaunchElement, filter=database_schema.LaunchElement.action_id==id)
        return key


    def get_elements(self, element_type, search_filter=None):
        with database_api.QuerySession() as dbs:
            if element_type == "menu_action":
                actions = dbs.select(database_schema.LaunchElement)
                return [LaunchMenuElement(action) for action in actions]
        return []

    def get_config_enabling(self, element_type):
        if element_type == "menu_action":
            return False, False


class TrafficAdminSink(components.management_message.server_management.session_recieve.ManagementMessageSessionRecieverBase):

    def __init__(self, environment):
        components.management_message.server_management.session_recieve.ManagementMessageSessionRecieverBase.__init__(self, environment.checkpoint_handler, components.traffic.module_id)

    def sink_traffic_item_launched(self, server_sid, launch_id, launch_type, user_id):
        if user_id:
            with database_api.Transaction() as dbt:
                launch_type = int(launch_type)
                launch_type_category = launch_type2launch_category.get(launch_type)
                if launch_type_category is None:
                    return
                existing_entry = database_schema.lookup_launch_type_category_launched(launch_type_category, user_id, dbt)
                if not existing_entry:
                    dbt.add(database_schema.LaunchTypeCategoryLaunched(user_id=user_id, launch_type_category=launch_type_category))
                else:
                    existing_entry.last_launch = datetime.datetime.now()


class LaunchMenuElement(ElementType):

    def __init__(self, launch_element):
        self.launch_element = launch_element

    def get_id(self):
        return str("menu_action.%s" % self.launch_element.id)

    def get_entity_type(self):
        return "LaunchSpec"

    def get_label(self):
        return self.launch_element.menu_title

    def get_info(self):
        return self.launch_element.name
