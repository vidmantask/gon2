from components.traffic.server_common.database_schema import LaunchElement
from components.dialog.server_management import dialog_api as dialog_api
from components.templates.server_common.template_api import TemplateHandler


def create_launch_element(transaction, action_id, label):
    launch_element = LaunchElement(action_id=action_id)
    transaction.add(launch_element)
    """ TODO solve transactional problem - see also rule_api """
    transaction.flush()
    """create corresponding dialog element"""
    dialog_api.create_dialog_element(transaction, launch_element.id, label)
    return launch_element

def update_launch_element(transaction, action_id, label):
    elements = transaction.select(LaunchElement, LaunchElement.action_id==action_id)
    assert(len(elements)==1)
    launch_id = elements[0].id
    """update corresponding dialog element"""
    dialog_api.update_dialog_element(transaction, launch_id, label)


def delete_launch_element(transaction, action_id):
    template_handler = TemplateHandler(None)
    elements = transaction.select(LaunchElement, LaunchElement.action_id==action_id)
    for e in elements:
        """delete corresponding dialog element"""
        dialog_api.delete_dialog_element(transaction, e.id)
        """delete template use"""
        template_handler.delete_template_use(transaction, action_id)
        #delete portforwards
        for portforward in e.portforwards:
            transaction.delete(portforward)
        """delete element"""
        transaction.delete(e)
