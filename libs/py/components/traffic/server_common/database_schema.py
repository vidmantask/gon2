"""
This file contains the database schema for the traffic component
"""
from __future__ import with_statement
import datetime
from components.database.server_common import schema_api
from components.database.server_common import database_api



launch_type_categories = {
                           -1 : "Internal",
                            0 : "TCP",
                            1 : "Citrix",
                            2 : "Wake on LAN",
                            3 : "Web Apps",
                            4 : "RDP",
                        }
launch_type_category2category_number = dict([(value, key) for key, value in launch_type_categories.items()])

launch_type_category_license_name = {
                                        0 : "Transparent TCP Connector",
                                        1 : "Citrix Connector",
                                        2 : "Wake on LAN",
                                        3 : "Proxy Connector for HTTP & SOCKS",
                                        4 : "RDP Connector",
                                     }

launch_type_category_license_count_name = {
                                        0 : "Max users of TCP",
                                        1 : "Max users of Citrix",
                                        3 : "Max users of Web Apps",
                                        4 : "Max users of RDP",
                                     }

license_count_name2launch_type_category = dict([(value, key) for key, value in launch_type_category_license_count_name.items()])

launch_type2launch_category = {
                                0 : 0,
                                1 : 1,
                                2 : -1,
                                3 : 2,
                                4 : 1,
                                5 : 4,
                                6 : 4,
                                7 : 3,
                                8 : 3,
                                9 : 4,
                                10 : 4,
                                11 : 3,
                                }


dbapi = schema_api.SchemaFactory.get_creator("traffic_component")


def connect_to_database(environment):
    dbapi.bind(environment.get_default_database())


table_launch_element = dbapi.create_table("launch_element",
                                          schema_api.Column('action_id', schema_api.Integer), # reference to auth engine
                                          # launch_types
                                          # 0=launch_portforward
                                          # 1=launch_citrix
                                          # 2=internal
                                          # 3=wol
                                          # 4=citrix xml
                                          # 5=async
                                          # 6=rdp
                                          # 7=proxy(http/socks)
                                          # 8=  CLient[launch_portforward], Server[proxy(http/socks)]
                                          # 9=  CLient[launch_portforward], Server[async]
                                          # 10= CLient[launch_portforward], Server[rdp]
                                          # 11= CLient[http_direct],        Server[proxy(http/socks]
                                          schema_api.Column('launch_type', schema_api.Integer),
                                          # Card pane:
                                          schema_api.Column('command', schema_api.Text, default=""),
                                          schema_api.Column('close_command', schema_api.Text, default=""),
                                          schema_api.Column('working_directory', schema_api.Text, default=""),
                                          schema_api.Column('param_file_name', schema_api.Text, default=""),
                                          schema_api.Column('param_file_lifetime', schema_api.Integer, default=5),

                                          schema_api.Column('param_file_template', schema_api.Text, nullable=False, default=""),
                                          schema_api.Column('close_with_process', schema_api.Boolean, nullable=False, default=False),
                                          schema_api.Column('kill_on_close', schema_api.Boolean, nullable=False, default=False),
                                          # For launch_type launch_citrix
                                          schema_api.Column('citrix_command', schema_api.Text, nullable=False, default=""),
                                          schema_api.Column('citrix_metaframe_path', schema_api.Text, nullable=False, default=""),
                                          schema_api.Column('citrix_https', schema_api.Boolean, default=False),
                                          schema_api.Column('sso_login', schema_api.Text(), nullable=False, default=""),
                                          schema_api.Column('sso_password', schema_api.Text(), nullable=False, default=""),
                                          schema_api.Column('sso_domain', schema_api.Text(), nullable=False, default=""),

                                          schema_api.Column('name', schema_api.String(40)),
                                          schema_api.Column('menu_title', schema_api.String(256)),
                                          schema_api.Column('image_id', schema_api.String(256)),
                                          )


table_portforward = dbapi.create_table("portforward",
                                          schema_api.Column('launch_element_id', schema_api.Integer(), nullable=False),
                                          schema_api.Column('line_no', schema_api.Integer(), nullable=False),
                                          schema_api.Column('server_host', schema_api.Text()),
                                          schema_api.Column('server_port', schema_api.Integer),
                                          schema_api.Column('client_host', schema_api.Text(), nullable=False, default="127.0.0.1"),
                                          schema_api.Column('client_port', schema_api.Integer, nullable=False, default=0),
                                          schema_api.Column('lock_to_process_pid', schema_api.Boolean, nullable=False, default=False),
                                          schema_api.Column('sub_processes', schema_api.Boolean, nullable=False, default=False),
                                          schema_api.Column('lock_to_process_name', schema_api.Text(), nullable=False, default=""),
                                          )

table_launch_tag_generators = dbapi.create_table("launch_tag_generator",
                                                 schema_api.Column('launch_id', schema_api.Integer, nullable=False),
                                                 schema_api.Column('tag_generator', schema_api.String(1000))
                                                )

table_launch_type_launched = dbapi.create_table("launch_type_category_launched", 
                                                schema_api.Column('launch_type_category', schema_api.Integer),
                                                schema_api.Column('user_id', schema_api.String(256)),
                                                schema_api.Column('last_launch', schema_api.DateTime, default=datetime.datetime.now()),
                                                )

table_launch_element_log = dbapi.create_table("launch_element_log",
                                         schema_api.Column('launch_id', schema_api.Integer, nullable=False),
                                         schema_api.Column('log_setting_name', schema_api.String(64)),
                                         schema_api.Column('log_setting_enabled', schema_api.Boolean),
                                          )

                                                


dbapi.add_foreign_key_constraint(table_portforward, "launch_element_id", table_launch_element)
dbapi.add_unique_constraint(table_portforward, "line_no", "launch_element_id")

dbapi.add_unique_constraint(table_launch_type_launched, "launch_type_category", "user_id")

dbapi.add_foreign_key_constraint(table_launch_tag_generators, "launch_id", table_launch_element)

dbapi.add_foreign_key_constraint(table_launch_element_log, "launch_id", table_launch_element)

schema_api.SchemaFactory.register_creator(dbapi)

class Portforward(database_api.PersistentObject):

    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls, table_portforward)



class LaunchTagGenerator(database_api.PersistentObject):

    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls, table_launch_tag_generators)

class LaunchElementLog(database_api.PersistentObject):

    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls, table_launch_element_log)



class LaunchElement(database_api.PersistentObject):

    @classmethod
    def lookup(cls, launch_id):
        return LaunchElement.get(None, launch_id)


    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls,
                          table_launch_element,
                          relations=[database_api.Relation(Portforward, 'portforwards', backref_property_name='launch_element'),
                                    database_api.Relation(LaunchTagGenerator, 'tag_generators', backref_property_name='launch_element'),
                                    database_api.Relation(LaunchElementLog, 'log_settings', backref_property_name='launch_element'),
                                     ]
                          )
    def get_tag_generators(self):
        return [tag_generator.tag_generator for tag_generator in sorted(self.tag_generators)]

    def add_tag_generator(self, tag_generator_name):
        tag_generator = LaunchTagGenerator(tag_generator=tag_generator_name)
        self.tag_generators.append(tag_generator)

class LaunchTypeCategoryLaunched(database_api.PersistentObject):
    
    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls, table_launch_type_launched)


Portforward.map_to_table()
LaunchTagGenerator.map_to_table()
LaunchElementLog.map_to_table()
LaunchElement.map_to_table()
LaunchTypeCategoryLaunched.map_to_table()

def get_action_id(dbs, launch_id):
    launch_spec = dbs.get(LaunchElement, launch_id)
    assert launch_spec, "Error getting action id for launch spec '%s'" % launch_id
    return launch_spec.action_id

def get_launch_id(dbs, action_id):
    launch_spec = dbs.select_first(LaunchElement, LaunchElement.action_id==action_id)
    assert launch_spec, "Error getting launch id for launch spec with action_id='%s'" % action_id
    return launch_spec.id

def get_launch_title_and_category(dbs, action_id):
    launch_spec = dbs.select_first(LaunchElement, LaunchElement.action_id==action_id)
    assert launch_spec, "Error getting title for menu action with action_id='%s'" % action_id
    category = launch_type2launch_category.get(launch_spec.launch_type)
    return launch_spec.menu_title, launch_type_categories.get(category, "Unknown")

def get_launch_title_and_categories(dbs, action_ids):
    start_index = 0
    launch_specs = []
    while start_index < len(action_ids):
        launch_specs.extend(dbs.select(LaunchElement, database_api.in_(LaunchElement.action_id, action_ids[start_index : start_index + database_api.IN_CLAUSE_LIMIT])))
        start_index += database_api.IN_CLAUSE_LIMIT
        
    launch_spec_dict = dict()
    for launch_spec in launch_specs:
        category = launch_type2launch_category.get(launch_spec.launch_type)
        launch_spec_dict[launch_spec.action_id] = (launch_spec.menu_title, launch_type_categories.get(category, "Unknown"))
    return launch_spec_dict

def lookup_launch_type_category_launched(launch_type_category, user_id, dbs):
    launch_type_category = int(launch_type_category)
    return dbs.select_first(LaunchTypeCategoryLaunched, 
                            filter=database_api.and_(LaunchTypeCategoryLaunched.launch_type_category==launch_type_category,
                                                     LaunchTypeCategoryLaunched.user_id==user_id))
    
def delete_launch_type_launched_for_user(user_id, dbt):
    entries = dbt.select(LaunchTypeCategoryLaunched, LaunchTypeCategoryLaunched.user_id==user_id)
    for e in entries:
        dbt.delete(e)
        
def launch_element2dict(launch_element):
    launch_properties =  dict((col.name, getattr(launch_element, col.name))
                              for col in table_launch_element.c)
    count = 0
    for portforward in launch_element.portforwards:
        count += 1
        portforward_props = dict(("portforward.%d.%s" % (count, col.name), getattr(portforward, col.name))
                              for col in table_portforward.c)
        launch_properties.update(portforward_props)
    return launch_properties

    
    
    