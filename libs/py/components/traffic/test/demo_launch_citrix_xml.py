#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Test of Citrix XML Service Launch
"""
# PYTHONPATH=py python py/components/traffic/test/demo_launch_citrix_xml.py
# pkill -9 -f 'python py/components/traffic/test/'

import unittest

from lib import giri_unittest

import lib.checkpoint

import components.database.server_common.database_api

import components.dialog.server_common.database_schema
import components.traffic.server_common.database_schema

import components.environment
import components.dictionary.common
import components.presentation.user_interface as gui
import components.communication.sim.sim_tunnel_endpoint

import plugin_modules.endpoint.client_gateway

from components.traffic.client_gateway import client_launch_citrix_xml
from components.traffic.server_gateway import server_launch_citrix_xml

checkpoint_handler_null = lib.checkpoint.CheckpointHandler.get_NULL()
checkpoint_handler_show = lib.checkpoint.CheckpointHandler.get_cout_all()
component_env = components.environment.Environment(checkpoint_handler_show, None, 'unittest_launch_portforward')
components.dialog.server_common.database_schema.connect_to_database(component_env)


def cb_client_closed():
    print "cb_client_closed"

def cb_server_closed():
    print "cb_server_closed"

def cb_report_error_to_server(error_source, error_code, error_message):
    print "cb_report_error_to_server", error_source, error_code, error_message

def cb_expand_variables(template_string):
    return template_string


class DummyAccessLogServerSession(object):
    def report_access_log_traffic_start(self, who, client_host, client_port, server_host, server_port):
        print "report_access_log_traffic_start", who, client_host, client_port, server_host, server_port

    def report_access_log_traffic_close(self, who):
        print "report_access_log_traffic_close", who

class LaunchCitrix(unittest.TestCase):

    def setUp(self):
        self.temp_folder = giri_unittest.mkdtemp()
        self.temp_database_name = giri_unittest.generate_database_connect_string(self.temp_folder)

    def tearDown(self):
        pass

    def test_normal_operation(self):
        environment = components.environment.Environment(checkpoint_handler_null, self.temp_database_name, 'management_message_unittest')
        components.traffic.server_common.database_schema.connect_to_database(environment)

        with components.database.server_common.database_api.Transaction() as t:
            launch_spec = components.traffic.server_common.database_schema.LaunchElement()
            launch_spec.launch_type = 4
            portforward_spec = components.traffic.server_common.database_schema.Portforward()
            portforward_spec.line_no = 0
            portforward_spec.server_host = u'dev-test-citrix'
            portforward_spec.server_port = 80
            launch_spec.sso_login = u"user01"
            launch_spec.sso_password = u"GiriGiri007"
            launch_spec.sso_domain = u"devtest2008"
            launch_spec.command = u'start Notepad'
            if 0:
                launch_spec.sso_login = u"æøå ÆØÅ"
                launch_spec.sso_password = u"GiriGiri007å"
                launch_spec.sso_domain = u"dev-test-citrix"
                launch_spec.command = u'start Notepad Å'
            if 0:
                portforward_spec.server_host = '#timeout=20 ctx01'
                launch_spec.sso_login = "demo"
                launch_spec.sso_password = "Giritech"
                launch_spec.sso_domain = "demo"
                launch_spec.command = 'Notepad on CTX01'
            if 1:
                portforward_spec.server_host = '#failover #timeout=2 ctx01 192.168.45.6'
                launch_spec.sso_login = "xxx"
                launch_spec.sso_password = "xxx"
                launch_spec.sso_domain = "giritech"
                launch_spec.command = 'outlook'
            launch_spec.citrix_command = 'sh -c "LANG=en /usr/lib/ICAClient/wfica.sh %(launch.param_file)"'
            launch_spec.close_with_process = False
            launch_spec.close_with_process = True
            launch_spec.close_command = "zenity --info --text \"That's it folks!\""
            launch_spec.kill_on_close = False
            launch_spec.working_directory = ''
            launch_spec.param_file_name = ''
            launch_spec.param_file_lifetime = 100
            launch_spec.param_file_template = '''
[Encoding]
InputEncoding=%(ica.inputencoding)

[WFClient]
CPMAllowed=On
ClientName=%(ica.clientname)
;ProxyFavorIEConnectionSetting=No
;ProxyTimeout=30000
ProxyType=None
;ProxyUseFQDN=Off
RemoveICAFile=no
TransparentKeyPassthrough=Local
TransportReconnectEnabled=On
VSLAllowed=On
Version=2
VirtualCOMPortEmulation=Off

[ApplicationServers]
%(ica.appname)=

[%(ica.appname)]
Address=%(portforward.host):%(portforward.port)
AutologonAllowed=ON
BrowserProtocol=HTTPonTCP
;CGPAddress=*:%(ica.cgpaddress)
ClearPassword=%(ica.clearpassword)
Username=%(ica.username)
ClientAudio=On
DesiredColor=8
DesiredHRES=1024
DesiredVRES=768
DoNotUseDefaultCSL=On
Domain=%(ica.domain)
FontSmoothingType=0
InitialProgram=#%(ica.appname)
;LPWD=
;LaunchReference=
Launcher=WI
LocHttpBrowserAddress=!
LogonTicket=%(ica.ticket)
LogonTicketType=CTXS1
LongCommandLine=
;NRWD=
;ProxyTimeout=30000
ProxyType=None
SFRAllowed=Off
SSLEnable=Off
SessionsharingKey=%(ica.sessionsharingkey)
;StartIFDCD=
;StartSCD=
;TRWD=
TWIMode=On
Title=%(ica.appname)
TransportDriver=TCP/IP
UILocale=en
WinStationDriver=ICA 3.0

[Compress]
DriverNameWin16=pdcompw.dll
DriverNameWin32=pdcompn.dll

[EncRC5-0]
DriverNameWin16=pdc0w.dll
DriverNameWin32=pdc0n.dll

[EncRC5-128]
DriverNameWin16=pdc128w.dll
DriverNameWin32=pdc128n.dll

[EncRC5-40]
DriverNameWin16=pdc40w.dll
DriverNameWin32=pdc40n.dll

[EncRC5-56]
DriverNameWin16=pdc56w.dll
DriverNameWin32=pdc56n.dll
'''

            t.add(launch_spec)
            portforward_spec.launch_element = launch_spec
            t.add(portforward_spec)


        user_interface = giri_unittest.get_user_interface_simulate(giri_unittest.get_checkpoint_handler())
        tunnel_endpoint_client = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_client()
        tunnel_endpoint_server = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_server()


        launch_id = 1
        own_id = 'xxxx'
        launch_element = components.traffic.server_common.database_schema.LaunchElement.lookup(launch_id)
        try:
            xml_service = server_launch_citrix_xml.FailoverService(launch_id, cb_expand_variables)
            print 'apps', xml_service.apps
        except Exception, e:
            print 'exception type:', type(e)
            print str(e)
            return
        access_log_server_session = DummyAccessLogServerSession()
        client_runtime_env = plugin_modules.endpoint.client_gateway.PluginClientRuntimeEnvInstance(checkpoint_handler_null, self.temp_folder)

        _client_launched_citrix_xml = client_launch_citrix_xml.LaunchXmlIca(
            giri_unittest.get_async_service(),
            checkpoint_handler_show,
            user_interface,
            tunnel_endpoint_client,
            client_runtime_env,
            cb_client_closed,
            cb_report_error_to_server,
            )

        appname = launch_element.command
        #print xml_service.apps[0]
        #appname = xml_service.apps[0].InName
        print 'launching app:', appname
        _server_launched_citrix_xml = server_launch_citrix_xml.LaunchXmlIca(giri_unittest.get_async_service(),
            checkpoint_handler_show,
            tunnel_endpoint_server,
            launch_id,
            own_id,
            cb_expand_variables,
            cb_server_closed,
            access_log_server_session,
            None, # cb_report_error
            xml_service,
            appname)
        _server_launched_citrix_xml.tunnelendpoint_connected()


        def wait_for_install_done():
            return False
        giri_unittest.wait_until_with_timeout(wait_for_install_done, 10000000)


if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
