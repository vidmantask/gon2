"""
Unittest of traffic admin
"""
from __future__ import with_statement
import time
import os
import unittest

from lib import giri_unittest

from components.traffic.server_management import traffic_admin
import components.environment
from components.plugin.server_common.config_api_lib import ConfigTemplateSpec

import components.database.server_common.database_api as database_api
from components.auth.server_management import OperationNotAllowedException

import components.auth.server_management.rule_api as rule_api
import components.dialog.server_common.dialog_api as dialog_api

class ActivityLogCreatorStub():

    def __init__(self):
        pass
        
    def save(self, **kwargs):
        pass
        
    def addValues(self, **kwargs):
        pass
    
    def copy(self):
        return self
    
    def copyUpdateAndAdd(self, dbt, **kwargs):
        pass


class UserAdminStub():

    def __init__(self):
        pass
    
    def set_traffic_admin(self, traffic_admin):
        pass

class TemplateHandlerStub(object):

    def __init__(self):
        pass

    def get_templates(self):
        return []

    def delete_template_use(self, transaction, action_id):
        pass

class DialogAdminStub(object):

    def __init__(self):
        pass

    def set_traffic_admin(self, traffic_admin):
        pass




ELEMENT_TYPE = "_action"


temp_root = giri_unittest.mkdtemp()
DB_STR = 'sqlite:///%s/unittest_test.sqlite' % (temp_root)

class TrafficAdminTest(unittest.TestCase):

    def setUp(self):
        self.dev_env_installation = giri_unittest.UnittestDevEnvInstallation.create()

    def _get_launch_id(self, admin, action_id):
        with database_api.ReadonlySession() as db_session:
            launch_element = admin._lookup_key(db_session, action_id)
            return launch_element.id

    def test0(self):
        environment = components.environment.Environment(giri_unittest.get_checkpoint_handler(), DB_STR, 'traffic_adminunittest')
#        environment = components.environment.Environment(checkpoint_handler, 'sqlite:///./traffic_unittest.sqlite', 'traffic_adminunittest')

        admin = traffic_admin.TrafficAdmin(environment, TemplateHandlerStub(), DialogAdminStub(), self.dev_env_installation.get_license_handler(), UserAdminStub(), ActivityLogCreatorStub())
        templates = admin.get_create_templates(ELEMENT_TYPE)
        self.assertEquals(len(templates),1 )

        config_spec = ConfigTemplateSpec(templates[0])
        config_spec.set_value("label", "ALL 0 portforward")
        config_spec.set_value("launch_type", 1)
        config_spec.set_value("command", "command")
        config_spec.set_value("close_with_process", True)
        config_spec.set_value("kill_on_close", False)
        config_spec.set_value("citrix_https", True)
        config_spec.set_value("citrix_metaframe_path", "citrix_metaframe_path")
        config_spec.set_value("citrix_command", "citrix_command")
        config_spec.set_value("param_file_name", "param_file_name")
        config_spec.set_value("param_file_lifetime", 1000)
        config_spec.set_value("param_file_template", "param_file_template")
        config_spec.set_value("sso_login", "sso_login")
        config_spec.set_value("sso_password", "sso_password")
        config_spec.set_value("sso_domain", "sso_domain")
        config_spec.set_value("dialog_tags", "tag1,  tag2 ")
        config_spec.set_value("dialog_tag_generators", "generator1(a,b)\n generator2(c)")

        with database_api.Transaction() as dbt:
            action_id = admin.config_create_row(ELEMENT_TYPE, config_spec.get_template(), dbt)

        with database_api.ReadonlySession() as db_session:
            launch_spec_template = admin.config_get_template(ELEMENT_TYPE, self._get_launch_id(admin, action_id), custom_template=None, db_session=db_session)
            config_spec = ConfigTemplateSpec(launch_spec_template)

            self.assertFalse(config_spec.get_value("action_id") is None)
            self.assertEquals(config_spec.get_value("label"), "ALL 0 portforward")
            self.assertEquals(config_spec.get_value("launch_type"), 1)
            self.assertEquals(config_spec.get_value("command"), "command")
            self.assertEquals(config_spec.get_value("close_with_process"), True)
            self.assertEquals(config_spec.get_value("kill_on_close"), False)
            self.assertEquals(config_spec.get_value("citrix_https"), True)
            self.assertEquals(config_spec.get_value("citrix_metaframe_path"), "citrix_metaframe_path")
            self.assertEquals(config_spec.get_value("citrix_command"), "citrix_command")
            self.assertEquals(config_spec.get_value("param_file_name"), "param_file_name")
            self.assertEquals(config_spec.get_value("param_file_lifetime"), 1000)
            self.assertEquals(config_spec.get_value("param_file_template"), "param_file_template")
            self.assertEquals(config_spec.get_value("sso_login"), "sso_login")
            self.assertEquals(config_spec.get_value("sso_password"), "sso_password")
            self.assertEquals(config_spec.get_value("sso_domain"), "sso_domain")

            self.failUnless(config_spec.get_value("dialog_tags") == "TAG1,TAG2" or
                            config_spec.get_value("dialog_tags") == "TAG2,TAG1")
            self.failUnless(config_spec.get_value("dialog_tag_generators") == "generator1(a,b)\ngenerator2(c)" or
                            config_spec.get_value("dialog_tag_generators") == "generator2(c)\ngenerator1(a,b)")



    def test0a(self):
        environment = components.environment.Environment(giri_unittest.get_checkpoint_handler(), DB_STR, 'traffic_adminunittest')
#        environment = components.environment.Environment(giri_unittest.get_checkpoint_handler(), 'sqlite:///./traffic_unittest.sqlite', 'traffic_adminunittest')

        admin = traffic_admin.TrafficAdmin(environment, TemplateHandlerStub(), DialogAdminStub(), self.dev_env_installation.get_license_handler(), UserAdminStub(), ActivityLogCreatorStub())
        templates = admin.get_create_templates(ELEMENT_TYPE)
        self.assertEquals(len(templates),1 )

        config_spec = ConfigTemplateSpec(templates[0])
        config_spec.set_value("label", "ALL 0 portforward for update")
        config_spec.set_value("launch_type", 1)
        config_spec.set_value("command", "command")
        config_spec.set_value("close_with_process", True)
        config_spec.set_value("kill_on_close", False)
        config_spec.set_value("citrix_https", True)
        config_spec.set_value("citrix_metaframe_path", "citrix_metaframe_path")
        config_spec.set_value("citrix_command", "citrix_command")
        config_spec.set_value("param_file_name", "param_file_name")
        config_spec.set_value("param_file_lifetime", 1000)
        config_spec.set_value("param_file_template", "param_file_template")
        config_spec.set_value("sso_login", "sso_login")
        config_spec.set_value("sso_password", "sso_password")
        config_spec.set_value("sso_domain", "sso_domain")
        config_spec.set_value("dialog_tags", "tag1,  tag2 ")
        config_spec.set_value("dialog_tag_generators", "generator1(a,b)\n generator2(c)")

        with database_api.Transaction() as dbt:
            action_id = admin.config_create_row(ELEMENT_TYPE, config_spec.get_template(), dbt)

        with database_api.Transaction() as dbt:
            launch_spec_template = admin.config_get_template(ELEMENT_TYPE, self._get_launch_id(admin, action_id), custom_template=None, db_session=dbt)
            config_spec = ConfigTemplateSpec(launch_spec_template)

            config_spec.set_value("label", "ALL 0 portforward updated")
            config_spec.set_value("launch_type", 2)
            config_spec.set_value("command", "command1")
            config_spec.set_value("close_with_process", False)
            config_spec.set_value("kill_on_close", True)
            config_spec.set_value("citrix_https", False)
            config_spec.set_value("citrix_metaframe_path", "citrix_metaframe_path1")
            config_spec.set_value("citrix_command", "citrix_command1")
            config_spec.set_value("param_file_name", "param_file_name1")
            config_spec.set_value("param_file_lifetime", 1001)
            config_spec.set_value("param_file_template", "param_file_template1")
            config_spec.set_value("sso_login", "sso_login1")
            config_spec.set_value("sso_password", "sso_password1")
            config_spec.set_value("sso_domain", "sso_domain1")
            config_spec.set_value("dialog_tags", "tag1,  tag3 ")
            config_spec.set_value("dialog_tag_generators", "generator2(a,b)\n generator3(c)")

            admin.config_update_row(ELEMENT_TYPE, action_id, config_spec.get_template(), dbt)

        with database_api.ReadonlySession() as db_session:
            launch_spec_template = admin.config_get_template(ELEMENT_TYPE, self._get_launch_id(admin, action_id), custom_template=None, db_session=db_session)
            config_spec = ConfigTemplateSpec(launch_spec_template)

            self.assertFalse(config_spec.get_value("action_id") is None)
            self.assertEquals(config_spec.get_value("label"), "ALL 0 portforward updated")
            self.assertEquals(config_spec.get_value("launch_type"), 2)
            self.assertEquals(config_spec.get_value("command"), "command1")
            self.assertEquals(config_spec.get_value("close_with_process"), False)
            self.assertEquals(config_spec.get_value("kill_on_close"), True)
            self.assertEquals(config_spec.get_value("citrix_https"), False)
            self.assertEquals(config_spec.get_value("citrix_metaframe_path"), "citrix_metaframe_path1")
            self.assertEquals(config_spec.get_value("citrix_command"), "citrix_command1")
            self.assertEquals(config_spec.get_value("param_file_name"), "param_file_name1")
            self.assertEquals(config_spec.get_value("param_file_lifetime"), 1001)
            self.assertEquals(config_spec.get_value("param_file_template"), "param_file_template1")
            self.assertEquals(config_spec.get_value("sso_login"), "sso_login1")
            self.assertEquals(config_spec.get_value("sso_password"), "sso_password1")
            self.assertEquals(config_spec.get_value("sso_domain"), "sso_domain1")

            self.failUnless(config_spec.get_value("dialog_tags") == "TAG1,TAG3" or
                            config_spec.get_value("dialog_tags") == "TAG3,TAG1")
            self.failUnless(config_spec.get_value("dialog_tag_generators") == "generator2(a,b)\ngenerator3(c)" or
                            config_spec.get_value("dialog_tag_generators") == "generator3(c)\ngenerator2(a,b)")




    def test1(self):
        environment = components.environment.Environment(giri_unittest.get_checkpoint_handler(), DB_STR, 'traffic_adminunittest')
#        environment = components.environment.Environment(giri_unittest.get_checkpoint_handler(), 'sqlite:///./traffic_unittest.sqlite', 'traffic_adminunittest')

        admin = traffic_admin.TrafficAdmin(environment, TemplateHandlerStub(), DialogAdminStub(), self.dev_env_installation.get_license_handler(), UserAdminStub(), ActivityLogCreatorStub())
        templates = admin.get_create_templates(ELEMENT_TYPE)
        self.assertEquals(len(templates),1 )

        config_spec = ConfigTemplateSpec(templates[0])
        config_spec.set_value("label", "Only mandatory set")
        config_spec.set_value("launch_type", 0)

        with database_api.Transaction() as dbt:
            action_id = admin.config_create_row(ELEMENT_TYPE, config_spec.get_template(), dbt)

        with database_api.ReadonlySession() as db_session:
            launch_spec_template = admin.config_get_template(ELEMENT_TYPE, self._get_launch_id(admin, action_id), custom_template=None, db_session=db_session)
            config_spec = ConfigTemplateSpec(launch_spec_template)

            self.assertFalse(config_spec.get_value("action_id") is None)
            self.assertEquals(config_spec.get_value("label"), "Only mandatory set")
            self.assertTrue(config_spec.get_value("launch_type") is 0)
            self.assertTrue(config_spec.get_value("command") is None)
            self.assertEquals(config_spec.get_value("close_with_process"), False)
            self.assertEquals(config_spec.get_value("kill_on_close"), False)
            self.assertEquals(config_spec.get_value("citrix_https"), 0)
            self.assertEquals(config_spec.get_value("citrix_metaframe_path"), "")
            self.assertEquals(config_spec.get_value("citrix_command"), "")
            self.assertEquals(config_spec.get_value("param_file_name"), "")
            self.assertEquals(config_spec.get_value("param_file_lifetime"), 5)
            self.assertEquals(config_spec.get_value("param_file_template"), "")
            self.assertEquals(config_spec.get_value("sso_login"), "")
            self.assertEquals(config_spec.get_value("sso_password"), "")
            self.assertEquals(config_spec.get_value("sso_domain"), "")

            self.assertEquals(config_spec.get_value("dialog_tags"), "")
            self.assertEquals(config_spec.get_value("dialog_tag_generators"), "")


    def test2(self):
        environment = components.environment.Environment(giri_unittest.get_checkpoint_handler(), DB_STR, 'traffic_adminunittest')
#        environment = components.environment.Environment(giri_unittest.get_checkpoint_handler(), 'sqlite:///./traffic_unittest.sqlite', 'traffic_adminunittest')

        admin = traffic_admin.TrafficAdmin(environment, TemplateHandlerStub(), DialogAdminStub(), self.dev_env_installation.get_license_handler(), UserAdminStub(), ActivityLogCreatorStub())
        templates = admin.get_create_templates(ELEMENT_TYPE)
        self.assertEquals(len(templates),1 )

        config_spec = ConfigTemplateSpec(templates[0])
        config_spec.set_value("label", "Portforward All")
        config_spec.set_value("launch_type", 0)

        config_spec.set_value("portforward.0.server_host", "server_host")
        config_spec.set_value("portforward.0.server_port", 42)
        config_spec.set_value("portforward.0.client_host", "client_host")
        config_spec.set_value("portforward.0.client_port", 1886)
        config_spec.set_value("portforward.0.lock_to_process_pid", True)
        config_spec.set_value("portforward.0.sub_processes", True)
        config_spec.set_value("portforward.0.lock_to_process_name", "lock_to_process_name")


        with database_api.Transaction() as dbt:
            action_id = admin.config_create_row(ELEMENT_TYPE, config_spec.get_template(), dbt)

        with database_api.ReadonlySession() as db_session:
            launch_spec_template = admin.config_get_template(ELEMENT_TYPE, self._get_launch_id(admin, action_id), custom_template=None, db_session=db_session)
            config_spec = ConfigTemplateSpec(launch_spec_template)

            self.assertEquals(config_spec.get_value("portforward.0.server_host"), "server_host")
            self.assertEquals(config_spec.get_value("portforward.0.server_port"), 42)
            self.assertEquals(config_spec.get_value("portforward.0.client_host"), "client_host")
            self.assertEquals(config_spec.get_value("portforward.0.client_port"), 1886)
            self.assertEquals(config_spec.get_value("portforward.0.lock_to_process_pid"), True)
            self.assertEquals(config_spec.get_value("portforward.0.sub_processes"), True)
            self.assertEquals(config_spec.get_value("portforward.0.lock_to_process_name"), "lock_to_process_name")


    def test2a(self):
        environment = components.environment.Environment(giri_unittest.get_checkpoint_handler(), DB_STR, 'traffic_adminunittest')
#        environment = components.environment.Environment(giri_unittest.get_checkpoint_handler(), 'sqlite:///./traffic_unittest.sqlite', 'traffic_adminunittest')

        admin = traffic_admin.TrafficAdmin(environment, TemplateHandlerStub(), DialogAdminStub(), self.dev_env_installation.get_license_handler(), UserAdminStub(), ActivityLogCreatorStub())
        templates = admin.get_create_templates(ELEMENT_TYPE)
        self.assertEquals(len(templates),1 )

        config_spec = ConfigTemplateSpec(templates[0])
        self.assertRaises(ConfigTemplateSpec.FieldNotFoundException, config_spec.set_value, "portforward.1.client_host", "client_host")



    def test2b(self):
        environment = components.environment.Environment(giri_unittest.get_checkpoint_handler(), DB_STR, 'traffic_adminunittest')
#        environment = components.environment.Environment(giri_unittest.get_checkpoint_handler(), 'sqlite:///./traffic_unittest.sqlite', 'traffic_adminunittest')

        admin = traffic_admin.TrafficAdmin(environment, TemplateHandlerStub(), DialogAdminStub(), self.dev_env_installation.get_license_handler(), UserAdminStub(), ActivityLogCreatorStub())
        templates = admin.get_create_templates(ELEMENT_TYPE)
        self.assertEquals(len(templates),1 )

        config_spec = ConfigTemplateSpec(templates[0])
        config_spec.set_value("label", "Portforward one")
        config_spec.set_value("launch_type", 0)

        config_spec.set_value("portforward.0.server_host", "server_host")
        config_spec.set_value("portforward.0.server_port", 42)
        config_spec.set_value("portforward.0.client_host", "client_host")
        config_spec.set_value("portforward.0.client_port", 1886)
        config_spec.set_value("portforward.0.lock_to_process_pid", True)
        config_spec.set_value("portforward.0.sub_processes", True)
        config_spec.set_value("portforward.0.lock_to_process_name", "lock_to_process_name")


        with database_api.Transaction() as dbt:
            action_id = admin.config_create_row(ELEMENT_TYPE, config_spec.get_template(), dbt)

        with database_api.Transaction() as dbt:
            launch_spec_template = admin.config_get_template(ELEMENT_TYPE, self._get_launch_id(admin, action_id), custom_template=None, db_session=dbt)
            config_spec = ConfigTemplateSpec(launch_spec_template)

            config_spec = ConfigTemplateSpec(launch_spec_template)
            config_spec.set_value("label", "Portforward two")

            config_spec.set_value("portforward.1.server_host", "server_host1")
            config_spec.set_value("portforward.1.server_port", 1892)

            admin.config_update_row(ELEMENT_TYPE, action_id, config_spec.get_template(), dbt)

        with database_api.ReadonlySession() as db_session:
            launch_spec_template = admin.config_get_template(ELEMENT_TYPE, self._get_launch_id(admin, action_id), custom_template=None, db_session=db_session)
            config_spec = ConfigTemplateSpec(launch_spec_template)

            self.assertEquals(config_spec.get_value("portforward.1.server_host"), "server_host1")
            self.assertEquals(config_spec.get_value("portforward.1.server_port"), 1892)

            config_spec.set_value("portforward.2.server_host", "server_host2")

    def test3(self):
        environment = components.environment.Environment(giri_unittest.get_checkpoint_handler(), DB_STR, 'traffic_adminunittest')
#        environment = components.environment.Environment(giri_unittest.get_checkpoint_handler(), 'sqlite:///./traffic_unittest.sqlite', 'traffic_adminunittest')

        admin = traffic_admin.TrafficAdmin(environment, TemplateHandlerStub(), DialogAdminStub(), self.dev_env_installation.get_license_handler(), UserAdminStub(), ActivityLogCreatorStub())
        templates = admin.get_create_templates(ELEMENT_TYPE)
        self.assertEquals(len(templates),1 )

        config_spec = ConfigTemplateSpec(templates[0])

        with database_api.Transaction() as dbt:
            self.assertRaises(OperationNotAllowedException, admin.config_create_row, ELEMENT_TYPE, config_spec.get_template(), dbt)

        config_spec.set_value("label", "Yo")
        
        with database_api.Transaction() as dbt:
            self.assertRaises(OperationNotAllowedException, admin.config_create_row, ELEMENT_TYPE, config_spec.get_template(), dbt)
        
        config_spec.set_value("launch_type", 0)

        with database_api.Transaction() as dbt:
            admin.config_create_row(ELEMENT_TYPE, config_spec.get_template(), dbt)


        with database_api.Transaction() as dbt:
            self.assertRaises(OperationNotAllowedException, admin.config_create_row, ELEMENT_TYPE, config_spec.get_template(), dbt)


    def test4(self):


        environment = components.environment.Environment(giri_unittest.get_checkpoint_handler(), DB_STR, 'traffic_adminunittest')
#        environment = components.environment.Environment(giri_unittest.get_checkpoint_handler(), 'sqlite:///./traffic_unittest.sqlite', 'traffic_adminunittest')

        admin = traffic_admin.TrafficAdmin(environment, TemplateHandlerStub(), DialogAdminStub(), self.dev_env_installation.get_license_handler(), UserAdminStub(), ActivityLogCreatorStub())
        templates = admin.get_create_templates(ELEMENT_TYPE)
        self.assertEquals(len(templates),1 )

        config_spec = ConfigTemplateSpec(templates[0])
        config_spec.set_value("label", "test delete")
        config_spec.set_value("launch_type", 0)

        with database_api.Transaction() as dbt:
            action_id = admin.config_create_row(ELEMENT_TYPE, config_spec.get_template(), dbt)

        launch_id = self._get_launch_id(admin, action_id)

        with database_api.Transaction() as dbt:
            launch_spec_template = admin.config_get_template(ELEMENT_TYPE, launch_id, custom_template=None, db_session=dbt)
            config_spec = ConfigTemplateSpec(launch_spec_template)

            self.assertFalse(config_spec.get_value("action_id") is None)
            self.assertEquals(config_spec.get_value("label"), "test delete")

            admin.config_delete_row(ELEMENT_TYPE, action_id, dbt)


        with database_api.ReadonlySession() as db_session:
            launch_element = admin._lookup_key(db_session, action_id)
            self.assertTrue(launch_element is None)

            self.failIf(dialog_api.get_tag_set(db_session, launch_id))

            self.failIf(rule_api.get_class_criteria(db_session, action_id))


#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'pwl'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
