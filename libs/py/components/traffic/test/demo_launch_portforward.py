# -*- coding: utf-8 -*-

"""
Unittest of launch_portforward functionality
"""
import unittest

from lib import giri_unittest
GIRITECH_UNITTEST_OWNER = 'mk'
GIRITECH_UNITTEST_IGNORE = True

import lib.checkpoint

import components.database.server_common.database_api

import components.dialog.server_common.database_schema
import components.traffic.server_common.database_schema

import components.environment
import components.dictionary.common
import components.presentation.user_interface as gui
import components.communication.sim.sim_tunnel_endpoint

import plugin_modules.endpoint.client_gateway

from components.traffic.client_gateway import client_launch_portforward
from components.traffic.server_gateway import server_launch_portforward


checkpoint_handler_null = lib.checkpoint.CheckpointHandler.get_NULL()
checkpoint_handler_show = lib.checkpoint.CheckpointHandler.get_cout_all()
component_env = components.environment.Environment(checkpoint_handler_show, None, 'unittest_launch_portforward')
components.dialog.server_common.database_schema.connect_to_database(component_env)


def cb_client_closed():
    print "cb_client_closed"

def cb_server_closed():
    print "cb_server_closed"

def cb_cb_report_error_to_server(error_source, error_code, error_message):
    print "cb_cb_report_error_to_server", error_source, error_code, error_message

def cb_expand_variables(template_string):
    return lib.variable_expansion.expand(template_string, lambda * a: None)


class DummyAccessLogServerSession(object):
    def report_access_log_traffic_start(self, who, client_host, client_port, server_host, server_port):
        print "report_access_log_traffic_start", who, client_host, client_port, server_host, server_port

    def report_access_log_traffic_close(self, who):
        print "report_access_log_traffic_close", who


class LaunchPortforwardTest(unittest.TestCase):

    def setUp(self):
        self.temp_folder = giri_unittest.mkdtemp()
        self.temp_database_name = giri_unittest.generate_database_connect_string(self.temp_folder)

    def tearDown(self):
        pass

    def test_normal_operation(self):
        environment = components.environment.Environment(checkpoint_handler_null, self.temp_database_name, 'management_message_unittest')
        components.traffic.server_common.database_schema.connect_to_database(environment)

        with components.database.server_common.database_api.Transaction() as t:
            launch_spec = components.traffic.server_common.database_schema.LaunchElement()
            launch_spec.id = 1
            launch_spec.launch_type = 0
            launch_spec.command = u"firefox 'http://%%(portforward.host):%%(portforward.port)/'"
            launch_spec.command = u"zenity --info --text 'hello http://%%(portforward.host):%%(portforward.port)/'"
            launch_spec.close_command = "zenity --info --text \"That's it folks!\""
            launch_spec.close_with_process = True
            launch_spec.kill_on_close = False
            launch_spec.working_directory = u''
            t.add(launch_spec)

            launch_portforward = components.traffic.server_common.database_schema.Portforward()
            launch_portforward.launch_element_id = 1
            launch_portforward.line_no = 1
            launch_portforward.server_host = u'www.dr.dk'
            launch_portforward.server_port = 80
            launch_portforward.client_host = u''
            launch_portforward.client_port = 0
            launch_portforward.lock_to_process_pid = False
            launch_portforward.sub_processes = False
            launch_portforward.lock_to_process_name = u''
            t.add(launch_portforward)

            launch_portforward = components.traffic.server_common.database_schema.Portforward()
            launch_portforward.launch_element_id = 1
            launch_portforward.line_no = 2
            launch_portforward.server_host = u'www.tv2.dk'
            launch_portforward.server_port = 80
            launch_portforward.client_host = u''
            launch_portforward.client_port = 0
            launch_portforward.lock_to_process_pid = False
            launch_portforward.sub_processes = False
            launch_portforward.lock_to_process_name = u''
            t.add(launch_portforward)

#            launch_spec.citrix_command = ''
#            launch_spec.param_file_template = ''
#            launch_spec.sso_login = sso_lo
#            launch_spec.sso_password = sso_password


        user_interface = giri_unittest.get_user_interface_simulate(giri_unittest.get_checkpoint_handler())
        tunnel_endpoint_client = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_client()
        tunnel_endpoint_server = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_server()

        launch_id = 1
        own_id = 'xxxx'
        access_log_server_session = DummyAccessLogServerSession()
        client_runtime_env = plugin_modules.endpoint.client_gateway.PluginClientRuntimeEnvInstance(checkpoint_handler_null, self.temp_folder)

        clp = client_launch_portforward.LaunchPortForward(giri_unittest.get_async_service(),
                                                          checkpoint_handler_show,
                                                          user_interface,
                                                          tunnel_endpoint_client,
                                                          client_runtime_env,
                                                          cb_client_closed,
                                                          cb_cb_report_error_to_server)

        slp = server_launch_portforward.LaunchPortForward(giri_unittest.get_async_service(),
                                                          checkpoint_handler_show,
                                                          tunnel_endpoint_server,
                                                          launch_id, own_id,
                                                          cb_expand_variables,
                                                          cb_server_closed,
                                                          access_log_server_session,
                                                          cb_cb_report_error_to_server)
        slp.tunnelendpoint_connected()


        def wait_for_install_done():
            return False
        giri_unittest.wait_until_with_timeout(wait_for_install_done, 10000000)

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
