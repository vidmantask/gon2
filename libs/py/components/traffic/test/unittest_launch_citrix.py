"""
Unittest of Cirtix Launch
"""
import unittest
from lib import giri_unittest


from lib import checkpoint

import components.environment

from components.database.server_common.database_api import Transaction

from components.traffic.client_gateway import client_launch_citrix
from components.traffic.server_gateway import server_launch_citrix
from components.traffic.server_common import database_schema



client_knownsecret = "0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E000400D7997B3756260EEE8D25B9F98256115CDB487F2CA598FD94D7A361E04C01E90B42A6124E64CD4994C01C5108899F64839E46E34A2C5BF4BF753A6E3081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D2AD5BB92B2200E1AF062FA9F29CB43E9969AC7D1F190F7791E52F8D34F"
server_knownsecret = "0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E000400872A7507237DEA288BCDEFC0BEF38A1A239CCF96E9FDC22F2F62336A3F013DD6A903639422F8F74E11CC4B725B72BF3FDB88E666690BFB7676B0F03081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D241304C4665A621272CE151248A2B3D2BBA7C8986219366F10790F01D2"

import components.traffic.common.selector


from plugin_types.client_gateway import plugin_type_client_runtime_env

class UnittestClientRuntimeEnvInstance(plugin_type_client_runtime_env.PluginTypeClientRuntimeEnvInstanceRWFileSystemBase):
    def __init__(self, async_service, temp_foldername):
        plugin_type_client_runtime_env.PluginTypeClientRuntimeEnvInstanceRWFileSystemBase.__init__(self, async_service, giri_unittest.get_checkpoint_handler(), temp_foldername, 'unittest_runtime_env', [])



class LaunchCitrix(unittest.TestCase):

    def setUp(self):
        components.traffic.common.selector.start(1.0)
        db_connect_string = 'sqlite:///:memory:'
        environment = components.environment.Environment(giri_unittest.get_checkpoint_handler(), db_connect_string, "unittest_launch_citrix")
        database_schema.connect_to_database(environment)

        self.temp_foldername = giri_unittest.mkdtemp()

        with Transaction() as t:
            launch_spec = database_schema.LaunchElement()
            launch_spec.launch_type = 1
#            launch_spec.command = '"firefox "http://%(portforward.host):%(portforward.port)/Citrix/MetaFrame1/site/default.aspx"'
            launch_spec.command = '?NFuse_Application=Citrix.MPS.App.G-MPS40.Excel+2003+on+CTX01&NFuse_AppFriendlyNameURLENcoded=Excel+2003+on+CTX01'
            launch_spec.close_with_process = False
            launch_spec.kill_on_close = False
            launch_spec.citrix_metaframe_path = '/Citrix/MetaFrame1/'
            launch_spec.citrix_command = '"C:/utility/icaweb32/wfica32.exe" "%(launch.param_file)"'
            launch_spec.sso_login = "demo"
            launch_spec.sso_password = "Giritech"
            t.add(launch_spec)

            portforward_spec = database_schema.Portforward()
            portforward_spec.line_no = 0
            portforward_spec.server_host = 'ctx01.demo.giritech.com'
            portforward_spec.server_port = 80
            portforward_spec.launch_element = launch_spec
            t.add(portforward_spec)

    def expand_variables(self, template_string):
        return template_string

    def tearDown(self):
        components.traffic.common.selector.stop()

    def server_cb_child_closed(self):
        pass

    def cb_report_error_to_server(self, error_source, error_code, error_message):
        print "ERROR :", error_source, error_code, error_message

    def test_session_start(self):
        tunnel_endpoint_client = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_client()
        tunnel_endpoint_server = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_server()

        user_interface = giri_unittest.get_user_interface_simulate(giri_unittest.get_checkpoint_handler())

        client_runtime_env = UnittestClientRuntimeEnvInstance(giri_unittest.get_async_service(), self.temp_foldername)
        client_launched_citrix = client_launch_citrix.LaunchCitrix(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), user_interface, client_runtime_env, tunnel_endpoint_client, self.cb_report_error_to_server)

        access_log_server_session = None # ???
        launch_id = 1
        server_launched_citrix = server_launch_citrix.LaunchCitrix(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), tunnel_endpoint_server, launch_id, self.expand_variables, self.server_cb_child_closed, access_log_server_session, self.cb_report_error_to_server)


        def wait_for_install_done():
            return False
        giri_unittest.wait_until_with_timeout(wait_for_install_done, 10000000)




#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'mk'
GIRI_UNITTEST_IGNORE = True

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
