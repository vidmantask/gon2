# -*- coding: utf-8 -*-

"""
Demo of launch_portforward functionality
"""

# pkill -9 -f 'python py/components/traffic/test/' ; PYTHONPATH=py python py/components/traffic/test/demo_launch_async.py

import unittest

from lib import giri_unittest

import components.database.server_common.database_api

import components.dialog.server_common.database_schema
import components.traffic.server_common.database_schema

import components.environment
import components.dictionary.common
import components.presentation.user_interface as gui

import plugin_modules.endpoint.client_gateway

from components.traffic.client_gateway import client_launch_async
from components.traffic.server_gateway import server_launch_async
from components.traffic.client_gateway import client_launch_portforward


component_env = components.environment.Environment(giri_unittest.get_checkpoint_handler(), None, 'unittest_launch_portforward')
components.dialog.server_common.database_schema.connect_to_database(component_env)


    

def cb_client_closed():
    print "cb_client_closed"

def cb_server_closed():
    print "cb_server_closed"

def cb_cb_report_error_to_server(error_source, error_code, error_message):
    print "cb_cb_report_error_to_server", error_source, error_code, error_message

def cb_expand_variables(template_string):
    return template_string or ''


class DummyAccessLogServerSession(object):
    def report_access_log_traffic_start(self, who, client_host, client_port, server_host, server_port):
        print "report_access_log_traffic_start", who, client_host, client_port, server_host, server_port

    def report_access_log_traffic_close(self, who):
        print "report_access_log_traffic_close", who

    def report_access_log_menu_action_details(self, *a, **b):
        print 'report_access_log_menu_action_details', a, b

class LaunchPortforwardTest(unittest.TestCase):

    def setUp(self):
        self.temp_folder = giri_unittest.mkdtemp()
        self.temp_database_name = giri_unittest.generate_database_connect_string(self.temp_folder)

    def tearDown(self):
        pass

    def test_normal_operation(self):
        environment = components.environment.Environment(giri_unittest.get_checkpoint_handler(), self.temp_database_name, 'management_message_unittest')
        components.traffic.server_common.database_schema.connect_to_database(environment)

        use_client_portforward = False
        with components.database.server_common.database_api.Transaction() as t:
            launch_spec = components.traffic.server_common.database_schema.LaunchElement()
            t.add(launch_spec)
            launch_spec.id = 1
            launch_spec.close_with_process = True
            launch_spec.kill_on_close = False
#            launch_spec.close_command = "zenity --info --text \"That's it folks!\""
            launch_spec.working_directory = u''
#            launch_spec.citrix_command = ''
#            launch_spec.param_file_template = ''
#            launch_spec.sso_login = sso_lo
#            launch_spec.sso_password = sso_password

            launch_portforward = components.traffic.server_common.database_schema.Portforward(launch_element_id=1, line_no=0)
            t.add(launch_portforward)
            launch_portforward.client_host = u''
            launch_portforward.client_port = 0
            launch_portforward.lock_to_process_pid = False
            launch_portforward.sub_processes = False
            launch_portforward.lock_to_process_name = u''

            if 0: # plain
                launch_spec.launch_type = 5
                launch_spec.command = u"rdesktop '%%(portforward.host):%%(portforward.port)'"
                #launch_portforward.server_host = u'192.168.45.6' # x-men
                launch_portforward.server_host = u'#failover #timeout=2 127.0.0.1 sos5# unknowndomain.foo sos4 1.1.1.1 1.1.1.2 vtst-sos5 '
                launch_portforward.server_port = 3389
            elif 0: # rdp
                launch_spec.launch_type = 6
                launch_spec.command = u"rdesktop '%%(portforward.host):%%(portforward.port)'"
                #launch_spec.command = u"gedit '%%(launch.param_file)'"
                #launch_spec.param_file_template = 'hello %%(launch.pid) %%(portforward.host):%%(portforward.port)'
                #launch_spec.param_file_lifetime = 7
                launch_portforward.server_port = 3389

                launch_spec.sso_login = u"admin01"
                launch_spec.sso_password = u"GiriGiri007"
                launch_spec.sso_domain = u"devtest2008"
                launch_portforward.server_host = u'#failover #timeout=2 127.0.0.1 sos5# unknowndomain.foo sos4 1.1.1.1 1.1.1.2 vtst-sos5 '

#                launch_spec.sso_login = u"superuser"
#                launch_spec.sso_password = u"superuser"
#                launch_portforward.server_host = u'192.168.42.112'

            elif 1:
                launch_spec.launch_type = 7 # proxy, http and socks
#                launch_spec.command = u"curl -vk --socks4  '%%(portforward.host):%%(portforward.port)' http://giritech.com/extension/giritech/design/giritech/images/site.js"
#                launch_spec.command = u"curl -vk --socks4a '%%(portforward.host):%%(portforward.port)' http://giritech.com/extension/giritech/design/giritech/images/site.js"
#                launch_spec.command = u"curl -vk --socks5  '%%(portforward.host):%%(portforward.port)' http://giritech.com/extension/giritech/design/giritech/images/site.js"
#                launch_spec.command = u"curl -vk --proxy   '%%(portforward.host):%%(portforward.port)' http://giritech.com/extension/giritech/design/giritech/images/site.js"
#                launch_spec.command = u"curl -vk --proxy   '%%(portforward.host):%%(portforward.port)' https://hg:443/"
                #launch_spec.sso_login = u"nn" # for basic auth injection
                #launch_spec.sso_password = u"xx"
                #launch_portforward.server_host = u'hg' # for reverse proxy
                #launch_portforward.server_port = 80
                launch_portforward.client_host = u'0.0.0.0'
                launch_portforward.client_port = 1080

                # plain http
#                launch_portforward = components.traffic.server_common.database_schema.Portforward(launch_element_id=1, line_no=2)
#                t.add(launch_portforward)
#                #launch_portforward.server_host = u'giritech.com'
#                launch_portforward.server_host = u'192.168.42/24' #auth=basic
#                launch_portforward.server_host = u'192.168.42/24 #httpproxy=localhost:3128' #auth=basic
#                launch_portforward.server_host = u'192.168.42/24 #deny' #auth=basic
#                launch_portforward.server_port = 0

#                launch_portforward = components.traffic.server_common.database_schema.Portforward(launch_element_id=1, line_no=3)
#                launch_portforward.launch_element_id = 1
#                launch_portforward.server_host = u'hg'
#                launch_portforward.server_port = 443

#                # http://sos5/ user01  ntlm or basic
#                launch_portforward = components.traffic.server_common.database_schema.Portforward(launch_element_id=1, line_no=4)
#                t.add(launch_portforward)
#                launch_portforward.server_host = u'sos5' #auth=basic
#                launch_portforward.server_host = u'#auth 172.16/16' #auth=basic
#                launch_portforward.server_port = 80
#                launch_spec.sso_login = u"user01"
#                launch_spec.sso_password = u"GiriGiri007"

#                # http://dudley/exchange mk xxx
#                launch_portforward = components.traffic.server_common.database_schema.Portforward(launch_element_id=1, line_no=5)
#                t.add(launch_portforward)
#                launch_portforward.server_host = u'dudley #auth=ntlm' # #auth=basic
#                launch_portforward.server_port = 80
#                launch_spec.sso_login = u"mk"
#                launch_spec.sso_password = u"xxx"

                # everything
                launch_portforward = components.traffic.server_common.database_schema.Portforward(launch_element_id=1, line_no=100)
                t.add(launch_portforward)
                launch_portforward.launch_element_id = 1
                launch_portforward.server_host = u'0/0'
                launch_portforward.server_port = 0

            if launch_spec.launch_type == 8:
                use_client_portforward = True

        tunnel_endpoint_client = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_client()
        tunnel_endpoint_server = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_server()

        user_interface = giri_unittest.get_user_interface_simulate(giri_unittest.get_checkpoint_handler())


        launch_id = 1
        own_id = 'xxxx'
        access_log_server_session = DummyAccessLogServerSession()
        client_runtime_env = plugin_modules.endpoint.client_gateway.PluginClientRuntimeEnvInstance(giri_unittest.get_checkpoint_handler(), self.temp_folder)


        if use_client_portforward:
            _clp = client_launch_portforward.LaunchPortForward(
                 giri_unittest.get_async_service(),
                 giri_unittest.get_checkpoint_handler(),
                 user_interface,
                 tunnel_endpoint_client,
                 client_runtime_env,
                 cb_client_closed,
                 cb_cb_report_error_to_server
                 )
        else:
            _clp = client_launch_async.LaunchAsync(
                giri_unittest.get_async_service(),
                giri_unittest.get_checkpoint_handler(),
                user_interface,
                tunnel_endpoint_client,
                client_runtime_env,
                cb_cb_report_error_to_server,
                cb_client_closed,
                )

        slp = server_launch_async.LaunchAsync(
            giri_unittest.get_async_service(),
            giri_unittest.get_checkpoint_handler(),
            tunnel_endpoint_server,
            access_log_server_session,
            cb_expand_variables,
            own_id,
            launch_id,
            cb_server_closed,
            None, # server_config
            )

        slp.tunnelendpoint_connected()
        giri_unittest.wait_until_with_timeout(lambda:False, 500000)


GIRI_UNITTEST_OWNER = 'twa'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
