#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Unittest of Citrix Launch
"""
# PYTHONPATH=py python py/components/traffic/test/demo_launch_citrix_linux.py
# pkill -9 -f 'python py/components/traffic/test/'

import unittest

from lib import giri_unittest
GIRITECH_UNITTEST_OWNER = 'mk'
GIRITECH_UNITTEST_IGNORE = True


import lib.checkpoint

import components.database.server_common.database_api

import components.dialog.server_common.database_schema
import components.traffic.server_common.database_schema

import components.environment
import components.dictionary.common
import components.presentation.user_interface as gui

import plugin_modules.endpoint.client_gateway

from components.traffic.client_gateway import client_launch_citrix
from components.traffic.server_gateway import server_launch_citrix

checkpoint_handler_null = lib.checkpoint.CheckpointHandler.get_NULL()
checkpoint_handler_show = lib.checkpoint.CheckpointHandler.get_cout_all()
component_env = components.environment.Environment(checkpoint_handler_show, None, 'unittest_launch_portforward')
components.dialog.server_common.database_schema.connect_to_database(component_env)

import components.traffic.common.selector


def cb_client_closed():
    print "cb_client_closed"

def cb_server_closed():
    print "cb_server_closed"

def cb_report_error_to_server(error_source, error_code, error_message):
    print "cb_report_error_to_server", error_source, error_code, error_message

def cb_expand_variables(template_string):
    return lib.variable_expansion.expand(template_string, lambda * a: None)


class DummyAccessLogServerSession(object):
    def report_access_log_traffic_start(self, who, client_host, client_port, server_host, server_port):
        print "report_access_log_traffic_start", who, client_host, client_port, server_host, server_port

    def report_access_log_traffic_close(self, who):
        print "report_access_log_traffic_close", who

class LaunchCitrix(unittest.TestCase):

    def setUp(self):
        components.traffic.common.selector.start(1.0)
        self.temp_folder = giri_unittest.mkdtemp()
        self.temp_database_name = giri_unittest.generate_database_connect_string(self.temp_folder)

    def tearDown(self):
        components.traffic.common.selector.stop()

    def test_normal_operation(self):
        environment = components.environment.Environment(checkpoint_handler_null, self.temp_database_name, 'management_message_unittest')
        components.traffic.server_common.database_schema.connect_to_database(environment)

        with components.database.server_common.database_api.Transaction() as t:
            launch_spec = components.traffic.server_common.database_schema.LaunchElement()
            launch_spec.launch_type = 1
            launch_spec.close_with_process = False
            launch_spec.close_with_process = True
            launch_spec.close_command = "zenity --info --text \"That's it folks!\""
            launch_spec.kill_on_close = False
            # LANG=en /usr/lib/ICAClient/wfica.sh launch.ica
            launch_spec.citrix_command = u'sh -c "LANG=en /usr/lib/ICAClient/wfica.sh %(launch.param_file)"'

            portforward_spec = components.traffic.server_common.database_schema.Portforward()
            portforward_spec.line_no = 0
            portforward_spec.server_port = 80

            if 1:
                launch_spec.command = u'firefox "http://%(portforward.host):%(portforward.port)/Citrix/MetaFrame4/site/default.aspx"'
                #launch_spec.command = u'?NFuse_Application=Citrix.MPS.App.farm1.Excel+2003+on+CTX01'
                launch_spec.citrix_metaframe_path = u'/Citrix/MetaFrame4/'
                launch_spec.sso_login = u"demo"
                launch_spec.sso_password = u"Giritech"
                launch_spec.sso_domain = u"demo"
                portforward_spec.server_host = u'ctx01.demo.giritech.com'

            if 0:
                launch_spec.command = u'firefox "http://%(portforward.host):%(portforward.port)/Citrix/XenApp/site/default.aspx"'
                launch_spec.command = u'?NFuse_Application=Citrix.MPS.App.NewFarmName.start+notepad'
                launch_spec.citrix_metaframe_path = u'/Citrix/XenApp/'
                launch_spec.sso_login = u"user01"
                launch_spec.sso_password = u"xxx"
                launch_spec.sso_domain = u"devtest2008"
                portforward_spec.server_host = u'dev-test-citrix'

            if 0:
                launch_spec.command = u'firefox "http://%(portforward.host):%(portforward.port)/Citrix/XenApp/site/default.aspx"'
                launch_spec.command = u'?NFuse_Application=Citrix.MPS.App.NewFarmName.start%20notepad%20%c3%85'
                launch_spec.citrix_metaframe_path = u'/Citrix/XenApp/'
                launch_spec.sso_login = u"æøå ÆØÅ"
                launch_spec.sso_password = u"GiriGiri007å"
                launch_spec.sso_domain = u"dev-test-citrix"
                portforward_spec.server_host = u'dev-test-citrix'

            if 0:
                # Requires hole in the firewall to x-men ...
                launch_spec.command = u'firefox "http://%(portforward.host):%(portforward.port)/Citrix/XenApp/site/default.aspx"'
                launch_spec.command = u'?CTX_Application=Citrix.MPS.App.GiritechFarm.Outlook&CTX_Token=D2AFAFF14D8888D19C7310BC7770B26C&LaunchId=1262618303158'
                launch_spec.citrix_metaframe_path = u'/Citrix/XenApp/'
                launch_spec.sso_login = u"xxx"
                launch_spec.sso_password = u"xxx"
                launch_spec.sso_domain = u"giritech"
                portforward_spec.server_host = u'192.168.45.6'

            t.add(launch_spec)
            portforward_spec.launch_element = launch_spec
            t.add(portforward_spec)


        user_interface = giri_unittest.get_user_interface_simulate(giri_unittest.get_checkpoint_handler())
        tunnel_endpoint_client = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_client()
        tunnel_endpoint_server = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_server()

        launch_id = 1
        own_id = 'xxxx'
        launch_element = components.traffic.server_common.database_schema.LaunchElement.lookup(launch_id)

        access_log_server_session = DummyAccessLogServerSession()
        client_runtime_env = plugin_modules.endpoint.client_gateway.PluginClientRuntimeEnvInstance(checkpoint_handler_null, self.temp_folder)

        _client_launched_citrix = client_launch_citrix.LaunchCitrix(
            giri_unittest.get_async_service(), checkpoint_handler_show, user_interface, client_runtime_env, tunnel_endpoint_client, None)

        _server_launched_citrix = server_launch_citrix.LaunchCitrix(
            giri_unittest.get_async_service(), checkpoint_handler_show, tunnel_endpoint_server, launch_id,
            cb_expand_variables, cb_server_closed, access_log_server_session, None)
        _server_launched_citrix.tunnelendpoint_connected()

        def wait_for_install_done():
            return False
        giri_unittest.wait_until_with_timeout(wait_for_install_done, 10000000)



if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
