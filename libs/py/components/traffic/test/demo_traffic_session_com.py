"""
Demo of session manager
"""
import threading
import time
import sys

import lib_cpp.communication
import lib.config
import unittest
from lib import giri_unittest
from lib import checkpoint
from components.communication import session_manager
from components.communication import session
from components.communication import tunnel_endpoint_base

import components.traffic.server_gateway.traffic_session as traffic_session_server
import components.traffic.client_gateway.traffic_session as traffic_session_client
from components.traffic.common import selector

import components.environment
import lib.checkpoint
checkpoint_handler_demo = lib.checkpoint.CheckpointHandler.get_cout_all()
component_env = components.environment.Environment(checkpoint_handler_demo, None)

import components.traffic.server_common.database_schema
components.traffic.server_common.database_schema.connect_to_database(component_env)
components.traffic.server_common.database_schema.create_demo_data()



class ServerSession(session.APISessionEventhandlerReady):
    """
    Demo server session with one tunnelendpoint having twa child tunnelendpoints
    """
    def __init__(self, new_session):
        session.APISessionEventhandlerReady.__init__(self)
        self.session_ = new_session
        self.session_.set_eventhandler_ready(self)
        self.tunnelendpoint_ = tunnel_endpoint_base.create_tunnelendpoint_tunnel(checkpoint_handler_demo)
        self.session_.add_tunnelendpoint_tunnel(1, self.tunnelendpoint_)
        self.traffic_session = traffic_session_server.TrafficSession(component_env, self.tunnelendpoint_)
#        self.tunnelendpoint_.set_eventhandler(self.traffic_session)

    def session_state_ready(self):
        print 'python ServerSession::session_state_ready'
        self.session_.read_start_state_ready()
        self.traffic_session.traffic_launch(2)

    def session_state_closed(self):
        print 'python ServerSession::session_state_closed'

    def session_state_key_exchange(self):
        print 'python ServerSession::session_state_key_exchange'

    def session_read_continue_state_ready(self):
        print 'python ServerSession::session_read_continue_state_ready'
        return True
#

class ClientSession(session.APISessionEventhandlerReady):
    """
    Demo client session with one tunnelendpoint having twa child tunnelendpoints
    """
    def __init__(self, new_session):
        session.APISessionEventhandlerReady.__init__(self)
        self.session_ = new_session
        self.session_.set_eventhandler_ready(self)
        self.tunnelendpoint_ = tunnel_endpoint_base.create_tunnelendpoint_tunnel(checkpoint_handler_demo)
        self.session_.add_tunnelendpoint_tunnel(1, self.tunnelendpoint_)
        self.traffic_session = traffic_session_client.TrafficSession(component_env, self.tunnelendpoint_)
#        self.tunnelendpoint_.set_eventhandler(self.traffic_session)

    def session_state_ready(self):
        print 'python ClientSession::session_state_ready'
        self.session_.read_start_state_ready()

    def session_state_closed(self):
        print 'python ClientSession::session_state_closed'

    def session_state_key_exchange(self):
        print 'python ClientSession::session_state_key_exchange'

    def session_read_continue_state_ready(self):
        return True



class SessionManagerServerEventhandler(session_manager.APISessionManagerEventhandler):
    def __init__(self):
        session_manager.APISessionManagerEventhandler.__init__(self)
        self.sessions_ = {}

    def session_manager_resolve_connection(self, resolve_info):
        return resolve_info;

    def session_manager_session_created(self, connection_id, new_session):
        print 'python SessionManagerServerEventhandler::session_manager_session_created'
        print 'session_id:' , new_session.get_session_id()
        self.sessions_[new_session.get_session_id()] = ServerSession(new_session)

    def session_manager_session_closed(self, session_id):
        print 'python SessionManagerServerEventhandler::session_manager_session_closed'
        print 'session_id:' , session_id

    def check(self, testcase):
        return True


class SessionManagerClientEventhandler(session_manager.APISessionManagerEventhandler):
    def __init__(self):
        session_manager.APISessionManagerEventhandler.__init__(self)

    def session_manager_resolve_connection(self, resolve_info):
        return resolve_info;

    def session_manager_session_created(self, connection_id, new_session):
        print 'python SessionManagerClientEventhandler::session_manager_session_created'
        print 'session_id:' , new_session.get_session_id()
        self.session_ = ClientSession(new_session)

    def session_manager_session_closed(self, session_id):
        print 'python SessionManagerClientEventhandler::session_manager_session_closed'
        print 'session_id:' , session_id

    def check(self, testcase):
        return True



def server_session_manager_thread_start(checkpoint_handler, communication_session_manager):
    try:
        communication_session_manager.start()

    except RuntimeError, value:
            checkpoint_handler.Checkpoint("main", "unittest_session_manager", checkpoint.ERROR, message= '%s' % value)


def client_session_manager_thread_start(checkpoint_handler, communication_session_manager):
    try:
        communication_session_manager.start()

    except RuntimeError, value:
        checkpoint_handler.Checkpoint("main", "unittest_session_manager", checkpoint.ERROR, message= '%s' % value)




class SessionManagerTester(unittest.TestCase):

    def test_client_server_connect(self):
        checkpoint_handler = checkpoint.CheckpointHandler.get_cout_all()
        session_manager.init(checkpoint_handler)

        known_secret_server = '0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E0004002E3741F0BE067C63BEF553E21C73CF928EB70123CF6817975254252385010AEF60D5894E8734B955689C929FEBB396F9269BA648132F24DB0AD6183081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D1A875AF53854318F52DEC8CD21CAF2DE7BD5187CAB39DA9C757F887747'
        known_secret_client = '0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E0004018BA68CE7B267CD97ED181BD294EC8860399A0E7D2B2356A356C3D5A4A6001E4D1ED593DB5BA8AE8F79CE6C943F2C7BF87BD6642CBBE6934F76E5363081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D4ACDE35A55D995A65C050975890AEDF825C6ABA4DC67D31741D1B71B96'

        session_manager_server_eventhandler = SessionManagerServerEventhandler()
        session_manager_client_eventhandler = SessionManagerClientEventhandler()

        session_manager_server = session_manager.APISessionManagerServer_create(session_manager_server_eventhandler,
                                                                                checkpoint_handler.checkpoint_handler,
                                                                                known_secret_server,
                                                                                "127.0.0.1",
                                                                                8012)
        session_manager_server.set_option_reuse_address(True)
        session_manager_client = session_manager.APISessionManagerClient_create(session_manager_client_eventhandler,
                                                                                checkpoint_handler.checkpoint_handler,
                                                                                known_secret_client,
                                                                                lib_cpp.communication.ApplProtocolType.PYTHON);

        session_manager_server_thread = threading.Thread(target=server_session_manager_thread_start, args=(checkpoint_handler, session_manager_server))
        session_manager_server_thread.start()

        session_manager_client.set_servers(lib.config.parse_servers(checkpoint_handler, "127.0.0.1, 8012"))
        session_manager_client_thread = threading.Thread(target=client_session_manager_thread_start, args=(checkpoint_handler, session_manager_client))
        session_manager_client_thread.start()

        selector.start(1.0)

        time.sleep(20)
        selector.stop()
        session_manager_server.close()
        time.sleep(4)


if __name__ == '__main__':
    gut_py = giri_unittest.CMakeUnittest()

    if gut_py.do_run():
        unittest.main()

