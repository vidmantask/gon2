"""
Portforwardung using comunication component
"""
from __future__ import with_statement

from lib import checkpoint
import components.traffic
from components.communication import tunnel_endpoint_tcp


def icb_connection_checker( (remote_host, remote_port), (local_host, local_port) ):
    """
    Check the connection, should return true/false
    """
    pass

def icb_portforward_ready():
    """
    Notify that the portforward is ready
    """
    pass

def icb_portforward_closed():
    """
    Notify that the portforward has been closed
    """
    pass

def icb_portforward_error(message):
    """
    Notify that an error has occurred and the portforward now is closed
    """
    pass

class PortForwardClient(tunnel_endpoint_tcp.TCPPortforwardClient):
    def __init__(self, checkpoint_handler, async_service, client_host, client_port, tcp_options, cb_connection_checker, cb_portforward_ready, cb_portforward_closed, cb_portforward_error):
        tunnel_endpoint_tcp.TCPPortforwardClient.__init__(self, checkpoint_handler, async_service, client_host, client_port, tcp_options)
        self.client_host = client_host
        self.client_port = client_port
        self.cb_connection_checker = cb_connection_checker
        self.cb_portforward_ready = cb_portforward_ready
        self.cb_portforward_closed = cb_portforward_closed
        self.cb_portforward_error = cb_portforward_error

    def ready(self):
        """
        Callback that indicate that the portforward has been connected to the server-side and is ready to read/write data.
        """
        with self.checkpoint_handler.CheckpointScope("PortForwardClient::ready", components.traffic.module_id, checkpoint.DEBUG, client_host=self.client_host, client_port=self.client_port) as cps:
            tunnel_endpoint_tcp.TCPPortforwardClient.ready(self)
            if not self.is_closed():
                self.cb_portforward_ready()

    def accepted(self, connection):
        """
        Callback that indicate that connection on the listen port has been accepted. If true is returned the connection is accepted,
        else is the connection ignored.
        """
        (remote_host, remote_port) = connection.get_ip_remote()
        (local_host, local_port) = connection.get_ip_local()
        with self.checkpoint_handler.CheckpointScope("PortForwardClient::accepted", components.traffic.module_id, checkpoint.DEBUG, remote_host=remote_host, remote_port=remote_port, local_host=local_host, local_port=local_port) as cps:
            if(self.cb_connection_checker((remote_host, remote_port), (local_host, local_port))):
                self.portforward_connections.append(tunnel_endpoint_tcp.PortforwardTCPConnection(self.checkpoint_handler, connection))
                acces_granted = True
            else:
                acces_granted = False
            cps.add_complete_attr(access="%s" % acces_granted)
            return acces_granted

    def closed(self):
        """
        Callback that indicate that the portforward has been closed.
        """
        with self.checkpoint_handler.CheckpointScope("PortForwardClient::closed", components.traffic.module_id, checkpoint.DEBUG, client_host=self.client_host, client_port=self.client_port) as cps:
            self.cb_portforward_closed()

    def error(self, message):
        """
        Callback that indicate that an error has occurred
        """
        with self.checkpoint_handler.CheckpointScope("PortForwardClient::error", components.traffic.module_id, checkpoint.DEBUG, message=message) as cps:
            self.cb_portforward_error(message)
