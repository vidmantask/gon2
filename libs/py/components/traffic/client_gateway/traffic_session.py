"""
This file contains the launch/traffic functionality related to a session on the client
It is instantiated for each login and is factory for all launches
"""

from lib import checkpoint

from components.communication import tunnel_endpoint_base

import components.traffic
from components.traffic.client_gateway import client_launch_portforward
from components.traffic.client_gateway import client_launch_citrix
from components.traffic.client_gateway import client_launch_internal
from components.traffic.client_gateway import client_launch_citrix_xml
from components.traffic.client_gateway import client_launch_async


class TrafficSession(tunnel_endpoint_base.TunnelendpointSession):
    def __init__(self, async_service, checkpoint_handler, new_tunnel, user_interface, client_runtime_env, launch_sessions):
        tunnel_endpoint_base.TunnelendpointSession.__init__(self, async_service, checkpoint_handler, new_tunnel)
        self._user_interface = user_interface
        self._client_runtime_env = client_runtime_env
        self.launch_sessions = launch_sessions
        self._children = {}

    def session_start(self, gateway_session_id):
        self._client_runtime_env.gateway_session_id = gateway_session_id # FIXME
        tunnel_endpoint_base.TunnelendpointSession.session_start(self)

    def session_start_and_connected(self):
        """
        This is called when tunnelendpoint_connected and session_start both has been called
        """
        pass

    def session_close(self):
        """
        Hard close of session. Called from main session when communication is terminated
        """
        with self.checkpoint_handler.CheckpointScope("session_close", components.traffic.module_id, checkpoint.DEBUG):
            for child_id in self._children.keys():
                child = self._children.pop(child_id)
                child.close(force=True, close_and_forget=True)
                self.remove_tunnelendpoint_tunnel(child_id)

    def traffic_launch_child_with_launch_id(self, child_id, launch_type, launch_id):
        """
        Launch client side, called from server
        """
        with self.checkpoint_handler.CheckpointScope("traffic_launch_child", components.traffic.module_id, checkpoint.DEBUG, child_id=child_id, launch_type=launch_type):
            new_tunnelendpoint = self.create_and_add_tunnelendpoint_tunnel(self.checkpoint_handler, child_id)
            if launch_type in [0, 8, 9, 10]:
                launched = client_launch_portforward.LaunchPortForward(self.async_service,
                                                                       self.checkpoint_handler,
                                                                       self._user_interface,
                                                                       new_tunnelendpoint,
                                                                       self._client_runtime_env,
                                                                       lambda child_id=child_id: self._cb_child_closed(child_id),
                                                                       self._cb_report_error_to_server)
            elif launch_type == 1:
                launched = client_launch_citrix.LaunchCitrix(self.async_service,
                                                             self.checkpoint_handler,
                                                             self._user_interface,
                                                             self._client_runtime_env,
                                                             new_tunnelendpoint,
                                                             self._cb_report_error_to_server)
            elif launch_type == 2:
                launched = client_launch_internal.LaunchInternal(self.async_service,
                                                                 self.checkpoint_handler,
                                                                 new_tunnelendpoint,
                                                                 self.launch_sessions,
                                                                 lambda child_id=child_id: self._cb_child_closed(child_id))
            #elif launch_type == 3: # WOL has no client-side
            elif launch_type == 4:
                launched = client_launch_citrix_xml.LaunchXmlIca(
                    self.async_service,
                    self.checkpoint_handler,
                    self._user_interface,
                    new_tunnelendpoint,
                    self._client_runtime_env,
                    lambda child_id=child_id: self._cb_child_closed(child_id),
                    self._cb_report_error_to_server)
            elif launch_type in [5, 6, 7]:
                launched = client_launch_async.LaunchAsync(
                    self.async_service,
                    self.checkpoint_handler,
                    self._user_interface,
                    new_tunnelendpoint,
                    self._client_runtime_env,
                    self._cb_report_error_to_server,
                    lambda child_id=child_id: self._cb_child_closed(child_id))
            else: raise RuntimeError("launch_type %r" % launch_type)
            self._children[child_id] = launched

    def _cb_child_closed(self, child_id):
        """
        Child tell that it has closed down
        """
        with self.checkpoint_handler.CheckpointScope("_cb_child_closed", components.traffic.module_id, checkpoint.DEBUG, child_id=child_id):
            if self._children.has_key(child_id):
                self._children.pop(child_id)
                self.remove_tunnelendpoint_tunnel(child_id)

    def _cb_report_error_to_server(self, error_source, error_code, error_message):
        self.tunnelendpoint_remote('remote_report_error', error_source=error_source, error_code=error_code, error_message=error_message)

    def show_message(self, title='', titleparams=None, text='', textparams=None):
        """Message from server"""
        local_title = self._user_interface.dictionary._(title) % (titleparams or ())
        local_text = self._user_interface.dictionary._(text) % (textparams or ())
        self.checkpoint_handler.Checkpoint("show_message", "traffic",
            checkpoint.INFO, local_title=local_title, local_text=local_text)
        self._user_interface.message.set_message(local_title, local_text)
        self._user_interface.message.display()
