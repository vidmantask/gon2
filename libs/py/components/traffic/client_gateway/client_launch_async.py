"""
"A launch", instantiated on launch and does whatever it designates of launch and traffic and stuff
"""
from lib import checkpoint
from lib import encode

import components.traffic
from components.communication import tunnel_endpoint_base, async_socket
import client_launch


class LaunchAsync(tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch):
    """
    Represent the client side of the proxy.

    Listen on a ip:port and for each connection a new sub tunnelendpoint is created and a message is send to the server proxy.
    """
    def __init__(self, async_service, checkpoint_handler, user_interface, new_tunnel, client_runtime_env, cb_report_error_to_server, cb_closed):
        tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch.__init__(self, async_service, checkpoint_handler, new_tunnel)
        self.user_interface = user_interface
        self.dictionary = self.user_interface.dictionary
        self.cb_report_error_to_server = cb_report_error_to_server
        self._cb_closed = cb_closed

        self.launched = client_launch.Launch(self.checkpoint_handler)
        self._connecting_portforwards = set()
        self._portforwards = {}
        self._line_no_portforwards = {}
        self._client_runtime_env = client_runtime_env
        self._close_command = None
        self._working_directory = None

    def tunnelendpoint_connected(self):
        """
        This signal is ignored, because 'remote_call_launch' is always called after this signal (ensured by server).
        """
        pass

    def remote_launch(self, launch_command, close_command, working_directory,
            param_file_name, param_file_lifetime, param_file_template,
            close_with_process, kill_on_close,
            client_portforwards,
            tcp_options):
        """
        Request a launch, called from server.
        """
        with self.checkpoint_handler.CheckpointScope("LaunchAsync::remote_launch", components.traffic.module_id, checkpoint.DEBUG) as cps:
            for child_id, portforward_dict in client_portforwards.items():
                portforward = AsyncConnector(
                    self.async_service,
                    self.checkpoint_handler,
                    self.create_and_add_tunnelendpoint_tunnel(self.checkpoint_handler, child_id),
                    encode.preferred(portforward_dict['client_host'].strip()),
                    portforward_dict['client_port'],
                    #tcp_options,
                    lambda remote, local, child_id=child_id: self._check_connection(child_id, remote, local),
                    #lambda child_id=child_id: self._portforward_ready(child_id),
                    #lambda child_id=child_id: self._portforward_closed(child_id),
                    self._portforward_error,
                    )
                self._portforwards[child_id] = (portforward_dict, portforward)
                self._line_no_portforwards[portforward_dict['line_no']] = portforward.get_listen_info()
                self._connecting_portforwards.add(child_id)

            # All portforwards (if any) are ready - now do the launch
            ok = self.launched.launch(
                    launch_command, working_directory,
                    param_file_name, param_file_lifetime, param_file_template,
                    kill_on_close,
                    close_with_process and (lambda: self.mutex_method('_cb_application_terminated')),
                    self.cb_report_error_to_server, self._line_no_portforwards.get, self._client_runtime_env)
            cps.add_complete_attr(ok=str(ok))
            if ok:
                self._close_command = close_command
                self._working_directory = working_directory
            else:
                self.user_interface.message.set_message(self.dictionary._("Launch failed"), self.dictionary._("Unable to start process"))
                self.user_interface.message.display()
                self.close()

    def _portforward_error(self, message):
        """
        Notification from the portforward that something has gone wrong
        """
        self.checkpoint_handler.Checkpoint("LaunchAsync::_portforward_error", components.traffic.module_id, checkpoint.DEBUG, message=message)
        self.cb_report_error_to_server('client_portforward', 0, message)
        self.user_interface.message.set_message(self.dictionary._("Launch failed"), self.dictionary._("Error creating portforward: %s") % message)
        self.user_interface.message.display()
        self.tunnelendpoint_remote('remote_close', message=message)

    def _cb_application_terminated(self):
        """
        The launched application has terminated - and it was close_with_process.
        The port forward should (probably, we assume) be closed now and forgotten.
        """
        self.checkpoint_handler.Checkpoint("LaunchAsync::_cb_application_terminated", components.traffic.module_id, checkpoint.DEBUG)
        self.tunnelendpoint_remote('remote_close', message='Application terminated')
        self._close_helper()
        self._cb_closed()

    def _check_connection(self, child_id, (remote_host, remote_port), (local_host, local_port)):
        with self.checkpoint_handler.CheckpointScope("LaunchAsync::_check_connection", components.traffic.module_id, checkpoint.DEBUG,
                                                     child_id=child_id,
                                                     remote="%s:%i" % (remote_host, remote_port), local="%s:%i" % (local_host, local_port)) as cps:
            if child_id not in self._portforwards:
                cps.add_complete_attr(message="unknown child_id")
                return False
            (portforward_dict, _portforward) = self._portforwards[child_id]
            permit = self.launched.check_connection(portforward_dict['lock_to_process_pid'],
                                                    portforward_dict['sub_processes'],
                                                    portforward_dict['lock_to_process_name'],
                                                    (remote_host, remote_port), (local_host, local_port))
            cps.add_complete_attr(permit=str(permit))
            return permit

    def close(self, force=False, close_and_forget=False):
        """
        Close down the launched portforward without callback
        """
        self.checkpoint_handler.Checkpoint("LaunchAsync::close", components.traffic.module_id, checkpoint.DEBUG)
        self._close_helper()

    def remote_close(self):
        """
        Server is telling that it has closed down
        """
        self.checkpoint_handler.Checkpoint("LaunchAsync::remote_close", components.traffic.module_id, checkpoint.DEBUG)
        self._close_helper()
        self._cb_closed()

    def _close_helper(self):
        self.launched.close()

        close_command, self._close_command = self._close_command, None
        if close_command:
            with self.checkpoint_handler.CheckpointScope("LaunchAsync::close_command", components.traffic.module_id, checkpoint.DEBUG, close_command=close_command) as cps:
                launcher = client_launch.Launch(self.checkpoint_handler)
                ok = launcher.launch(
                        close_command, self._working_directory,
                        param_file_name=None, param_file_lifetime=0, param_file_template=None,
                        kill_on_close=False,
                        cb_terminated=None,
                        cb_error=None, portforward_addr=None, client_runtime_env=self._client_runtime_env)
                cps.add_complete_attr(close_command_ok=ok)

        for (_portforward_spec, portforward) in self._portforwards.values():
            portforward.close()


class AsyncConnector(tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch, async_socket.AsyncSocketTCPAcceptor):
    """
    Represent the client side of the proxy.

    Listen on a ip:port and for each connection a new sub tunnelendpoint is created and a message is send to the server proxy.
    """
    def __init__(self, async_service, checkpoint_handler, tunnel_endpoint, listen_ip, listen_port,
            check_connection,
            portforward_error,
            ):
        tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch.__init__(self, async_service, checkpoint_handler, tunnel_endpoint)
        async_socket.AsyncSocketTCPAcceptor.__init__(self, async_service, checkpoint_handler, (listen_ip, listen_port))
        async_socket.AsyncSocketTCPAcceptor.set_mutex(self, tunnel_endpoint.get_mutex())
        self._check_connection = check_connection
        self._portforward_error = portforward_error
        self._connection_ids_in_use = []
        self._connection_ids_not_in_use = range(1, 255)
        self._connections = {}
        self._running = True
        self.local_host, self.local_port = None, None

        self.accept()

    def accept_failed(self, error_message):
        """Fatal error happened"""
        self._running = False
        self._portforward_error(error_message)

    def accepted(self, raw_connection):
        (remote_host, remote_port) = raw_connection.get_ip_remote()
        (local_host, local_port) = raw_connection.get_ip_local()
        with self.checkpoint_handler.CheckpointScope("AsyncConnector::accepted", components.traffic.module_id, checkpoint.DEBUG, remote_host=remote_host, remote_port=remote_port, local_host=local_host, local_port=local_port) as cps:
            acces_granted = False
            if(self._check_connection((remote_host, remote_port), (local_host, local_port))):
                acces_granted = True
                connection_id = self._connection_ids_not_in_use[0]
                self._connection_ids_in_use.append(connection_id)
                self._connection_ids_not_in_use.remove(connection_id)
                connection_tunnel_endpoint = self.create_and_add_tunnelendpoint_tunnel(self.checkpoint_handler, connection_id)
                self._connections[connection_id] = AsyncConnection(self.async_service, self.checkpoint_handler, raw_connection, connection_tunnel_endpoint, connection_id, self, self.get_mutex())
                self.tunnelendpoint_remote('remote_accepted', connection_id=connection_id)
            cps.add_complete_attr(access="%s" % acces_granted)
            self.accept()
            return acces_granted

    def async_connection_with_remote_closed(self, connection_id):
        self.remove_tunnelendpoint_tunnel(connection_id)
        del self._connections[connection_id]
        self._connection_ids_in_use.remove(connection_id)
        self._connection_ids_not_in_use.append(connection_id)

    def close(self):
        for connection_id in self._connections.keys():
            self._connections[connection_id].tcp.close()

    def is_done(self):
        return not self._running and len(self._connection_ids_in_use) == 0


class AsyncConnection(tunnel_endpoint_base.TunnelendpointBaseWithMessageDispatch):
    """
    Represent a socket connection, connected to a remote tunnelendpoint.
    """
    def __init__(self, async_service, checkpoint_handler, raw_connection, tunnel_endpoint, id_, callback, mutex):
        tunnel_endpoint_base.TunnelendpointBaseWithMessageDispatch.__init__(self, async_service, checkpoint_handler, tunnel_endpoint)
        self.tcp = AsyncConnectionTcp(checkpoint_handler, async_service, raw_connection, self)
        self.tcp.set_mutex(mutex)
        self.id = id_
        self.callback = callback
        self._remote_is_ready = False

    def tunnelendpoint_connected(self):
        """
        Callback from TunnelendpointBaseWithMessageDispatch when connection between the two tunnel_endpoints has been established.
        """
        self.tcp.read_start()
        self._remote_is_ready = True

    def tcp_read(self, data_chunk):
        self.tunnelendpoint_remote('remote_read', data_chunk=data_chunk)

    def remote_read(self, data_chunk):
        if data_chunk:
            self.tcp.write(data_chunk)
        else:
            self.tcp.close_eof()

    def tcp_closed(self):
        if self._remote_is_ready:
            self.tunnelendpoint_remote('remote_closed')
        self.callback.async_connection_with_remote_closed(self.id)

    def remote_closed(self):
        self.tcp.close()

    def tcp_closed_eof(self):
        if self._remote_is_ready:
            self.tunnelendpoint_remote('remote_read', data_chunk='')


class AsyncConnectionTcp(async_socket.AsyncSocketTCPConnection):
    """
    Wrap async tcp connection and make callback to handler
    """
    def __init__(self, checkpoint_handler, async_service, raw_connection, handler):
        async_socket.AsyncSocketTCPConnection.__init__(self, checkpoint_handler, raw_connection)
        self.async_service = async_service
        self.handler = handler

    def read(self, data_chunk):
        self.handler.tcp_read(data_chunk)
        self.read_start()

    def write_buffer_empty(self):
        pass

    def closed_eof(self):
        self.handler.tcp_closed_eof()

    def closed(self):
        self.handler.tcp_closed()
