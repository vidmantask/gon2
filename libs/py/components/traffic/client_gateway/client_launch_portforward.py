"""
"A launch", instantiated on launch and does whatever it designates of launch and traffic and stuff
"""

import os

from lib import checkpoint
from lib import encode

import components.traffic
from components.communication import tunnel_endpoint_base

import client_portforward
import client_launch


def icb_closed():
    """
    Notify that the portforward has been closed
    """
    pass

def icb_cb_report_error_to_server(error_source, error_code, error_message):
    """
    Notify about an error
    """
    pass


class LaunchPortForward(tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch):

    def __init__(self, async_service, checkpoint_handler, user_interface, new_tunnel, client_runtime_env, cb_closed, cb_report_error_to_server):
        tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch.__init__(self, async_service, checkpoint_handler, new_tunnel)
        self.user_interface = user_interface
        self.dictionary = self.user_interface.dictionary
        self.cb_closed = cb_closed
        self.cb_report_error_to_server = cb_report_error_to_server
        self._close_and_forget = False
        self._close_from_server = False

        self.launched = client_launch.Launch(self.checkpoint_handler)
        self.launch_command = None
        self._close_command = None
        self._working_directory = None
        self.param_file_name = None
        self.param_file_lifetime = None
        self.param_file_template = None
        self.close_with_process = False
        self.kill_on_close = False
        self.tcp_options = []
        self._connecting_portforwards = set()
        self._portforwards = {}
        self._line_no_portforwards = {}
        self._client_runtime_env = client_runtime_env

    def tunnelendpoint_connected(self):
        """
        This signal is ignored, because 'remote_call_launch' is always called after this signal (ensured by server).
        """
        pass

    def remote_launch(self, launch_command, close_command, working_directory,
            param_file_name, param_file_lifetime, param_file_template,
            close_with_process, kill_on_close,
            client_portforwards,
            tcp_options):
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::remote_launch", components.traffic.module_id, checkpoint.DEBUG):
            self.remote_call_launch_with_tcp_options(launch_command, close_command, working_directory,
                                                     param_file_name, param_file_lifetime, param_file_template,
                                                     close_with_process, kill_on_close,
                                                     client_portforwards,
                                                     tcp_options)

    # PROTOCOL_CHANGE: This must be cleaned up when backward compatibility can be broken. 5.3.0
    # PROTOCOL_CHANGE: 5.3.0
    def remote_call_launch(self, launch_command, close_command, working_directory,
                           param_file_name, param_file_lifetime, param_file_template,
                           close_with_process, kill_on_close,
                           client_portforwards):
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::remote_call_launch", components.traffic.module_id, checkpoint.DEBUG):
            self.remote_call_launch_with_tcp_options(launch_command, close_command, working_directory,
                                                     param_file_name, param_file_lifetime, param_file_template,
                                                     close_with_process, kill_on_close,
                                                     client_portforwards,
                                                     [])

    # PROTOCOL_CHANGE: This must be cleaned up when backward compatibility can be broken
    # PROTOCOL_CHANGE: 5.3.1
    def remote_call_launch_with_tcp_options(self, launch_command, close_command, working_directory,
                                        param_file_name, param_file_lifetime, param_file_template,
                                        close_with_process, kill_on_close,
                                        client_portforwards,
                                        tcp_options):
        """
        Request a launch, called from server.
        """
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::remote_call_launch_with_tcp_options", components.traffic.module_id, checkpoint.DEBUG):
            self.launch_command = launch_command
            self._close_command = close_command
            self._working_directory = working_directory
            self.param_file_name = param_file_name
            self.param_file_lifetime = param_file_lifetime
            self.param_file_template = param_file_template
            self.close_with_process = close_with_process
            self.kill_on_close = kill_on_close
            self.tcp_options = tcp_options

            if client_portforwards:
                for child_id, portforward_dict in client_portforwards.items():
                    client_host = encode.preferred(portforward_dict['client_host'].strip())
                    client_port = portforward_dict['client_port']
                    portforward = client_portforward.PortForwardClient(self.checkpoint_handler, self.async_service, client_host, client_port, tcp_options,
                                                                          lambda remote, local, child_id=child_id: self._check_connection(child_id, remote, local),
                                                                          lambda child_id=child_id: self._portforward_ready(child_id),
                                                                          lambda child_id=child_id: self._portforward_closed(child_id),
                                                                          self._portforward_error,
                                                                          )
                    self._portforwards[child_id] = (portforward_dict, portforward)
                    self._line_no_portforwards[portforward_dict['line_no']] = portforward
                    self._connecting_portforwards.add(child_id)
                    self.add_tunnelendpoint(child_id, portforward.as_tunnelendpoint())
            else:
                self._do_launch()

    def _check_connection(self, child_id, (remote_host, remote_port), (local_host, local_port)):
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::_check_connection", components.traffic.module_id, checkpoint.DEBUG,
                                                     child_id=child_id,
                                                     remote="%s:%i" % (remote_host, remote_port), local="%s:%i" % (local_host, local_port)) as cps:
            if child_id not in self._portforwards:
                cps.add_complete_attr(message="unknown child_id")
                return False
            (portforward_dict, _portforward) = self._portforwards[child_id]
            permit = self.launched.check_connection(portforward_dict['lock_to_process_pid'],
                                                    portforward_dict['sub_processes'],
                                                    portforward_dict['lock_to_process_name'],
                                                    (remote_host, remote_port), (local_host, local_port))
            cps.add_complete_attr(permit=str(permit))
            return permit

    def _portforward_ready(self, child_id):
        """
        Callback from portforward that it is ready
        """
        self.checkpoint_handler.Checkpoint("LaunchPortForward::_portforward_ready", components.traffic.module_id, checkpoint.DEBUG,
                                            child_id=child_id, waiting=len(self._connecting_portforwards),
                                           )
        if not child_id in self._connecting_portforwards:
            self.checkpoint_handler.Checkpoint("LaunchPortForward::_portforward_ready:error", components.traffic.module_id, checkpoint.WARNING,
                                                child_id=child_id, msg="wasn't waiting for ready child",)
            return
        self._connecting_portforwards.remove(child_id)
        if not self._connecting_portforwards:
            # All ready
            self._do_launch()

    def _do_launch(self):
        """
        All portforwards (if any) are ready - now do the launch
        """
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::_portforward_ready:launch", components.traffic.module_id, checkpoint.DEBUG) as cps:

            def get_portforward_addr(line_no):
                try:
                    return self._line_no_portforwards[line_no].get_ip_local()
                except (IndexError, KeyError):
                    return None
            ok = self.launched.launch(
                    self.launch_command, self._working_directory,
                    self.param_file_name, self.param_file_lifetime, self.param_file_template,
                    self.kill_on_close,
                    self.close_with_process and (lambda : self.mutex_method('_cb_application_terminated')),
                    self.cb_report_error_to_server, get_portforward_addr, self._client_runtime_env)
            cps.add_complete_attr(ok=str(ok))
            if not ok:
                self.user_interface.message.set_message(self.dictionary._("Launch failed"), self.dictionary._("Unable to start process"))
                self.user_interface.message.display()
                self.close()

    def _portforward_closed(self, child_id):
        """
        Notification from the portforward that it has been closed
        """
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::_portforward_closed", components.traffic.module_id, checkpoint.DEBUG):
            self._portforwards.pop(child_id)
            self.remove_tunnelendpoint_tunnel(child_id)
            if self._portforwards:
                return
            # Last portforward closed
            self._close_helper()

    def _portforward_error(self, message):
        """
        Notification from the portforward that something has gone wrong
        """
        self.cb_report_error_to_server('client_portforward', 0, message)
        self.user_interface.message.set_message(self.dictionary._("Launch failed"), self.dictionary._("Portforward error: %s") % message)
        self.user_interface.message.display()
        self._portforwards.clear()
        self._close_helper()

    def _cb_application_terminated(self):
        """
        The launched application has terminated.
        The port forward should (probably, we assume) be closed now and forgotten.
        """
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::_cb_application_terminated", components.traffic.module_id, checkpoint.DEBUG):
            self._close_helper()

    def close(self, force=False, close_and_forget=False):
        """
        Close down the launched portforward
        """
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::close", components.traffic.module_id, checkpoint.DEBUG,
                                                     force=force, close_and_forget=close_and_forget):
            self._close_and_forget = close_and_forget
            self._close_helper()

    def remote_call_closed(self):
        """
        Server is telling that is has closed down
        """
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::remote_call_closed", components.traffic.module_id, checkpoint.DEBUG):
            self._close_from_server = True
            self._close_helper()

    def _close_helper(self):
        self.launched.close()

        close_command, self._close_command = self._close_command, None
        if close_command:
            with self.checkpoint_handler.CheckpointScope("LaunchPortForward::close_command", components.traffic.module_id, checkpoint.DEBUG, close_command=close_command) as cps:
                launcher = client_launch.Launch(self.checkpoint_handler)
                ok = launcher.launch(
                        close_command, self._working_directory,
                        param_file_name=None, param_file_lifetime=0, param_file_template=None,
                        kill_on_close=False,
                        cb_terminated=None,
                        cb_error=None, portforward_addr=None, client_runtime_env=self._client_runtime_env)
                cps.add_complete_attr(close_command_ok=ok)

        # Close down portforward, via callback
        if self._portforwards and self.close_with_process:
            for (_portforward_spec, portforward) in self._portforwards.values():
                portforward.close(False, False)
            return # each of them will call back to _portforward_closed, the last one will call here

        if not self._portforwards:
            if not self._close_from_server:
                self.tunnelendpoint_remote('remote_call_closed')

            if not self._close_and_forget:
                self.cb_closed()
