"""
Handle launch of internal functionality that can be activated by menu items.
"""
from components.communication import tunnel_endpoint_base


class LaunchSession(object):
    """
    Sessions using the internal launch must have this api
    """
    def launch_internal_close(self):
        """
        Callback from launch-internal framework to inform that the tunnelendpoint controlling the launch has been asked to close down
        """
        pass

    def launch_internal_remote_closed(self):
        """
        Callback from launch-internal framework to inform that the tunnelendpoint controlling the launch has been closed by remote
        """
        pass


class LaunchSessions(object):
    def __init__(self, cpm_session, endpoint_session):
        self.cpm_session = cpm_session
        self.endpoint_session = endpoint_session


class LaunchInternal(tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch):
    def __init__(self, async_service, checkpoint_handler, tunnel_endpoint, launch_sessions, cb_closed):
        tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch.__init__(self, async_service, checkpoint_handler, tunnel_endpoint)
        self.launch_sessions = launch_sessions
        self.cb_closed = cb_closed
        self.launched_session = None

    def remote_launch_cpm(self):
        self.launched_session = self.launch_sessions.cpm_session
        if self.launched_session is not None and self.launched_session.analyze_ready():
            self.tunnelendpoint_remote('remote_launch_cpm_response')
        else:
            self.close()

    def remote_launch_endpoint_enrollment(self):
        self.launched_session = self.launch_sessions.endpoint_session
        if self.launched_session is not None and self.launched_session.launch_internal_ready():
            self.tunnelendpoint_remote('remote_launch_endpoint_enrollment_response')
        else:
            self.close()

    def remote_launch_device_enrollment(self):
        self.launched_session = self.launch_sessions.endpoint_session
        if self.launched_session is not None and self.launched_session.launch_internal_ready():
            self.tunnelendpoint_remote('remote_launch_device_enrollment_response')
        else:
            self.close()

    def remote_closed(self):
        if self.launched_session is not None:
            self.launched_session.launch_internal_remote_closed()
            self.launched_session = None
        self.cb_closed()

    def close(self, force = False, close_and_forget = False):
        self.tunnelendpoint_remote('remote_closed')
        if not close_and_forget:
            if self.launched_session is not None:
                self.launched_session.launch_internal_close()
                self.launched_session = None
            self.cb_closed()
