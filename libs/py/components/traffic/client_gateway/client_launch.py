"""
Abstraction of launch_process
"""
import threading
import os

from lib import checkpoint
from lib import launch_process
from lib import socket_owner
from lib import variable_expansion
from lib import winreg
from lib import encode

import components.traffic


class Launch(object):
    """
    Implement remote_launch & co
    """

    _seq = 0

    def __init__(self, checkpoint_handler):
        self._checkpoint_handler = checkpoint_handler
        self._process = None
        self._kill_on_close = False
        self._param_file_name = 'param_file_UNDEFINED' # default value until it is defined

    def launch(self,
            launch_command, working_directory,
            param_file_name, param_file_lifetime, param_file_template,
            kill_on_close,
            cb_terminated, cb_error, portforward_addr, client_runtime_env):
        """
        If cb_terminated then it will be called on regular termination (no error and no .close()) (should be None unless exit_on_close)
        """
        self.__class__._seq += 1
        self._kill_on_close = kill_on_close
        with self._checkpoint_handler.CheckpointScope("Launch::launch", components.traffic.module_id, checkpoint.DEBUG) as cps:

            def getter(first, second): # FIXME: use plugins?
                """Return looked up value for matchobj - suitable for re.sub parameter"""
                if first.startswith('portforward'):
                    try:
                        line_no = int(first.split('portforward', 1)[1] or '0') # default is line 0
                        listen = portforward_addr and portforward_addr(line_no)
                    except ValueError:
                        listen = None
                    if not listen:
                        return 'unknown_%s' % first
                    listen_host, listen_port = listen
                    if second == 'host':
                        if listen_host == '0.0.0.0':
                            return '127.0.0.1'
                        return listen_host
                    if second == 'port':
                        return listen_port
                if first == 'launch':
                    if second == 'param_file': return self._param_file_name
                    if second == 'cwd': return os.getcwd()
                    if second == 'ie':
                        v = winreg.read_value(r'HKEY_CLASSES_ROOT\Applications\iexplore.exe\shell\open\command')
                        return variable_expansion.strip_trailing_percent_one(v)
                    if second == 'browser': # Gets default browser (TC: this is actually wrong, but works 99% of the time)
                        v = winreg.read_value(r'HKEY_CLASSES_ROOT\HTTP\shell\open\command')
                        return variable_expansion.strip_trailing_percent_one(v)
                    if second == 'pid':
                        return str(os.getpid())
                    if second == 'seq': # unique together with pid
                        return str(self._seq)
                if first == 'reg': # arbitrary lookup in registry
                    return winreg.read_value(second)
                if first == 'env': # get environment variable
                    return os.getenv(second)
                if first == 'cpm':
                    if second == 'ro_root': return client_runtime_env.get_ro_root()
                    if second == 'rw_root': return client_runtime_env.get_rw_root()
                if first == 'run_env':
                    if second == 'temp': return client_runtime_env.get_temp_root()
                if first == 'session':
                    if second == 'gateway_session_id': return client_runtime_env.gateway_session_id
                if first == 'ica':
                    if second == 'inputencoding': # http://support.citrix.com/proddocs/index.jsp?topic=/ica-settings/ica-settings-inputencoding.html
                        if encode.get_current_encoding().lower() == 'utf-8':
                            return 'UTF8' # used on Mac and Linux
                        return '' # ISO8859_1 is default but doesn't work if explicit - and we ignore SJIS and EUC-JP ...
                return None

            # The first expansion is self._param_file_name which can be used in later expansions
            working_directory = variable_expansion.expand(working_directory, getter)
            self._param_file_name = variable_expansion.expand(param_file_name or os.path.join('%(run_env.temp)', 'launch_%(launch.pid)_%(launch.seq).tmp'), getter)
            if working_directory and not os.path.isabs(self._param_file_name):
                self._param_file_name = os.path.join(working_directory, self._param_file_name)

            if param_file_template:
                param_file_content = variable_expansion.expand(param_file_template, getter)
                with file(self._param_file_name, 'w') as f:
                    f.write(encode.preferred(param_file_content))

            if launch_command:
                launch_command = variable_expansion.expand(launch_command, getter)
                #print 'launching', launch_command
                process = launch_process.Process(self._checkpoint_handler)
                ok = process.launch(
                        encode.preferred(launch_command),
                        cwd=encode.preferred(working_directory),
                        callback=(lambda p: self._process and cb_terminated()) if cb_terminated else None,
                        error_callback=cb_error)
                cps.add_complete_attr(ok=ok)

                if ok:
                    self._process = process
                    if param_file_lifetime == -1:
                        self._param_file_name = None # forget it - never remove
                    elif param_file_lifetime:
                        timer = threading.Timer(param_file_lifetime, self._remove_param_file)
                        timer.start()
                else:
                    self._remove_param_file()
                return ok
            else:
                cps.add_complete_attr(launch_command='None')
                return True

    def check_connection(self, lock_to_process_pid, sub_processes, lock_to_process_name, (remote_host, remote_port), (local_host, local_port)):
        """
        Return True if the connection is related to the launched _process
        """
        with self._checkpoint_handler.CheckpointScope("Launch::check_connection", components.traffic.module_id, checkpoint.DEBUG,
                                                      remote="%s:%i" % (remote_host, remote_port),
                                                      local="%s:%i" % (local_host, local_port)) as cps:
            permit = True
            if lock_to_process_pid:
                process = self._process
                if process:
                    permit = process.owns((remote_host, remote_port), (local_host, local_port), sub_processes)
                else:
                    cps.add_complete_attr(message="no process")
                    permit = False
            if lock_to_process_name:
                cps.add_complete_attr(lock_to_process_name="%s" % (lock_to_process_name))
                permit = socket_owner.is_connection_owned_by_named(lock_to_process_name, (remote_host, remote_port), (local_host, local_port))
            cps.add_complete_attr(permit="%s" % permit)
            return permit

    def _remove_param_file(self):
        """
        Remove self._param_file_name ... if it exists. May be called several times.
        """
        if not self._param_file_name or not os.path.exists(self._param_file_name):
            return
        try:
            os.remove(self._param_file_name)
        except OSError, e:
            self._checkpoint_handler.Checkpoint("Launch::_remove_param_file", "traffic",
                checkpoint.ERROR,
                file=self._param_file_name,
                message="Remove failed: %s" % e)
        else:
            self._checkpoint_handler.Checkpoint("Launch::_remove_param_file", "traffic",
                checkpoint.DEBUG,
                file=self._param_file_name,
                message="Removed")

    def close(self):
        """
        Call this to try to clean up after a running _process, killing and removing param files
        """
        with self._checkpoint_handler.CheckpointScope("Launch::close", components.traffic.module_id, checkpoint.DEBUG) as cps:
            process, self._process = self._process, None
            if process and self._kill_on_close:
                process.kill()

            self._remove_param_file() # even if not _kill_on_close!
