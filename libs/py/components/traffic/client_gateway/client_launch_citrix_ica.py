"""
"A launch", instantiated on launch and does whatever it designates of launch and traffic and stuff
"""

from lib import checkpoint
from lib import encode

import components.traffic
from components.communication import tunnel_endpoint_base

import client_portforward
import client_launch


def icb_closed():
    """
    Notify that the portforward has been closed
    """
    pass

class IcaLaunchPortForward(tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch):

    def __init__(self, async_service, checkpoint_handler, user_interface,
                 new_tunnel, client_runtime_env, cb_closed, cb_report_error_to_server):
        tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch.__init__(self, async_service, checkpoint_handler, new_tunnel)
        self.user_interface = user_interface
        self.dictionary = self.user_interface.dictionary
        self.cb_closed = cb_closed
        self.cb_report_error_to_server = cb_report_error_to_server
        self._close_and_forget = False
        self._close_from_server = False

        self.launched = client_launch.Launch(self.checkpoint_handler)
        self.child_id = None
        self.launch_command = None
        self.working_directory = None
        self.param_file_name = None
        self.param_file_lifetime = None
        self.param_file_template = None
        self.close_with_process = False
        self.kill_on_close = False
        self.lock_to_process_pid = None
        self.sub_processes = None
        self.lock_to_process_name = None
        self.tcp_options = []
        self._portforward = None
        self._client_runtime_env = client_runtime_env

    def tunnelendpoint_connected(self):
        """
        This signal is ignored, because 'remote_call_launch' is always called after this signal (ensured by server).
        """
        pass

    def remote_call_launch_with_tcp_options(self,
                                            child_id,
                                            launch_command, working_directory,
                                            param_file_name, param_file_lifetime, param_file_template,
                                            close_with_process, kill_on_close,
                                            client_host,
                                            client_port,
                                            lock_to_process_pid,
                                            sub_processes,
                                            lock_to_process_name,
                                            tcp_options):
        """
        Request a launch, called from server.
        """
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::remote_call_launch_with_tcp_options", components.traffic.module_id, checkpoint.DEBUG):
            self.child_id = child_id
            self.launch_command = launch_command
            self.working_directory = working_directory
            self.param_file_name = param_file_name
            self.param_file_lifetime = param_file_lifetime
            self.param_file_template = param_file_template
            self.close_with_process = close_with_process
            self.kill_on_close = kill_on_close
            self.lock_to_process_pid = lock_to_process_pid
            self.sub_processes = sub_processes
            self.lock_to_process_name = lock_to_process_name
            self.tcp_options = tcp_options

            self._portforward = client_portforward.PortForwardClient(self.checkpoint_handler,
                                                                        self.async_service,
                                                                        encode.preferred(client_host.strip()),
                                                                        client_port,
                                                                        tcp_options,
                                                                        self._check_connection,
                                                                        self._portforward_ready,
                                                                        self._portforward_closed,
                                                                        self._portforward_error,
                                                                        )
            self.add_tunnelendpoint(self.child_id, self._portforward.as_tunnelendpoint())

    def _check_connection(self, (remote_host, remote_port), (local_host, local_port)):
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::_check_connection", components.traffic.module_id, checkpoint.DEBUG,
                                                     remote="%s:%i" % (remote_host, remote_port), local="%s:%i" % (local_host, local_port)) as cps:
            permit = self.launched.check_connection(self.lock_to_process_pid,
                                                    self.sub_processes,
                                                    self.lock_to_process_name,
                                                    (remote_host, remote_port), (local_host, local_port))
            cps.add_complete_attr(permit=str(permit))
            return permit

    def _portforward_ready(self):
        """
        Callback from portforward that it is ready
        """
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::_portforward_ready:launch", components.traffic.module_id, checkpoint.INFO) as cps:

            def get_portforward_addr(line_no):
                if line_no == 0:
                    return self._portforward.get_ip_local()
            ok = self.launched.launch(
                    self.launch_command, self.working_directory,
                    self.param_file_name, self.param_file_lifetime, self.param_file_template,
                    self.kill_on_close,
                    self.close_with_process and (lambda: self.mutex_method('_cb_application_terminated')),
                    self.cb_report_error_to_server, get_portforward_addr, self._client_runtime_env)
            cps.add_complete_attr(ok=str(ok))
            if not ok:
                self.user_interface.message.set_message(self.dictionary._("Launch failed"), self.dictionary._("Unable to start process"))
                self.user_interface.message.display()
                self.close()

    def _portforward_closed(self):
        """
        Notification from the portforward that it has been closed
        """
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::_portforward_closed", components.traffic.module_id, checkpoint.DEBUG):
            self._portforward = None
            self.remove_tunnelendpoint_tunnel(self.child_id)
            # Last portforward closed
            self._close_helper()

    def _portforward_error(self, message):
        """
        Notification from the portforward that something has gone wrong
        """
        self.cb_report_error_to_server('client_portforward', 0, message)
        self.user_interface.message.set_message(self.dictionary._("Launch failed"), self.dictionary._("Portforward error: %s") % message)
        self.user_interface.message.display()
        self._portforward = None
        self._close_helper()

    def _cb_application_terminated(self):
        """
        The launched application has terminated.
        The port forward should (probably, we assume) be closed now and forgotten.
        """
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::_cb_application_terminated", components.traffic.module_id, checkpoint.DEBUG):
            self._close_helper()

    def close(self, force=False, close_and_forget=False):
        """
        Close down the launched portforward
        """
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::close", components.traffic.module_id, checkpoint.DEBUG,
                                                     force=force, close_and_forget=close_and_forget):
            self._close_and_forget = close_and_forget
            self._close_helper()

    def remote_call_closed(self):
        """
        Server is telling that it has closed down
        """
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::remote_call_closed", components.traffic.module_id, checkpoint.DEBUG):
            self._close_from_server = True
            self._close_helper()

    def _close_helper(self):
        # Close down process
        self.launched.close()

        # Close down portforward, via callback
        if self._portforward and self.close_with_process:
            self._portforward.close(False, False)
            return # each of them will call back to _portforward_closed, the last one will call here

        if not self._portforward:
            if not self._close_from_server:
                self.tunnelendpoint_remote('remote_call_closed')

            if not self._close_and_forget:
                self.cb_closed()
