"""
"A citrix launch", instantiated on launch and does whatever it designates of launch and traffic and stuff
"""

import threading
import SocketServer
import BaseHTTPServer

from lib import checkpoint
from components.communication import tunnel_endpoint_base
import components.traffic
import client_launch_citrix_ica
import client_launch


class IcaHttpProxyRequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    """
    Simple RequestHandler, sufficiently capable to serve Citrix web interface
    """

    def do_GET(self):
        # print 'Request', self.path
        (peer_host, peer_port) = self.connection.getpeername()
        (my_host, my_port) = self.connection.getsockname()
        status, data = self.server.handle_http_request((peer_host, peer_port), (my_host, my_port), self.path)
        if status == 200:
            #FIXME: get and transfer headers from server...
            self.send_response(200)
            if data[:47].strip().startswith('<'):
                self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(data)
            # print 'Response 200'
        elif status == 303:
            new_url = data
            self.send_response(303)
            self.send_header("Content-type", "text/html")
            self.send_header("Location", new_url)
            self.end_headers()
            self.wfile.write('<html><head><meta http-equiv="refresh" content="1; URL=%s"></head><body></body></html>' % (new_url,))
            # print 'Response redirect', new_url
        else:
            self.send_response(status, data)
            self.send_header("Content-type", "text/html") # if not set then safari will download and launch the URL, and .aspx will start wfica!
            self.end_headers()
            self.wfile.write('Status code: %s<br>' % status)
            self.wfile.write(data)
            # print 'Response', status

    def log_request(self, code='-', size='-'):
        self.server.checkpoint_handler.Checkpoint("IcaHttpProxyRequestHandler", "traffic", checkpoint.DEBUG, req=self.requestline, code=code, size=size)

class CitrixHttpServer(SocketServer.TCPServer):
    """
    Simple HTTP proxy server, forwarding all requests to server.
    Singlethreaded, must be run in thread, blocks in semaphore until response callback received from server.
    """

    def __init__(self, server_address, handle_http_request, checkpoint_handler):
        SocketServer.TCPServer.__init__(self, server_address, IcaHttpProxyRequestHandler)
        self._handle_http_request = handle_http_request
        self._http_request_waiting = threading.Semaphore(0) # Block each time
        self.checkpoint_handler = checkpoint_handler # used from IcaHttpProxyRequestHandler
        self._http_status_data = None

    # called from request handler, will block until response ready
    def handle_http_request(self, (peer_host, peer_port), (my_host, my_port), path):
        # print 'CitrixHttpServer http_request', path
        if not self._handle_http_request((peer_host, peer_port), (my_host, my_port), path, self._http_response):
            return 401, 'Go away'
        self._http_request_waiting.acquire(True) # wait for http_response, don't handle two requests at once
        assert self._http_status_data
        status, data = self._http_status_data
        self._http_status_data = None
        # print path, '->', status
        return status, data

    # given as callback by handle_http_request and called back later on other thread
    def _http_response(self, status, data):
        self._http_status_data = status, data
        self._http_request_waiting.release() # release blocked http_request


class LaunchCitrix(tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch):

    def __init__(self, async_service, checkpoint_handler, user_interface, client_runtime_env, new_tunnel, cb_report_error_to_server):
        tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch.__init__(self, async_service, checkpoint_handler, new_tunnel)
        self.user_interface = user_interface
        if self.user_interface is not None:
            self.dictionary = self.user_interface.dictionary
        self._client_runtime_env = client_runtime_env
        self.got_close = False # if it's an order from server
        self.http_response_callback = None
        self._children = {} # IcaLaunchPortForward's
        self._http_thread = None
        self.cb_report_error_to_server = cb_report_error_to_server
        self.launched = client_launch.Launch(self.checkpoint_handler)

        if self.user_interface is not None:
            self.user_interface.message.set_message(self.dictionary._("Connecting"), self.dictionary._("Waiting for Citrix server"))
            self.user_interface.message.display()
        # server will call start when ready

    # called from owner
    def close(self, force=False, close_and_forget=False):
        self.checkpoint_handler.Checkpoint("close", "traffic", checkpoint.DEBUG)
        if not self.got_close:
            self.checkpoint_handler.Checkpoint("close", "traffic", checkpoint.ERROR, message="tunnel_close message wasn't received")
            self.tunnel_close() # FIXME HACK because tunnel_close doesn't get through

    # called from peer
    def tunnel_close(self):
        self.checkpoint_handler.Checkpoint("tunnel_close", "traffic", checkpoint.DEBUG)
        self.got_close = True
        for child_id in list(self._children):
            pf = self._children.pop(child_id, None)
            if pf:
                pf.close()
            self.close_tunnelendpoint_tunnel(child_id)

    # called from peer
    def start(self, browser_opener):
        self.checkpoint_handler.Checkpoint("start", "traffic",
                                           checkpoint.DEBUG,
                                           message="Setting up client-side of citrix")
        httpd = CitrixHttpServer(('127.0.0.1', 0), self._handle_http_request, self.checkpoint_handler)
        self._http_thread = threading.Thread(target=httpd.serve_forever, args=(), name="LaunchCitrix")
        self._http_thread.setDaemon(True) # Don't wait for it to exit
        self._http_thread.start()

        with self.checkpoint_handler.CheckpointScope("LaunchCitrix::start", components.traffic.module_id, checkpoint.DEBUG) as cps:

            def get_portforward_addr(line_no):
                if line_no == 0:
                    return httpd.socket.getsockname()
            ok = self.launched.launch(
                    browser_opener, None,
                    None, None, None,
                    None, # no kill_on_close!
                    None,
                    self.cb_report_error_to_server, get_portforward_addr, self._client_runtime_env)
            cps.add_complete_attr(ok=str(ok))
            if not ok:
                self.user_interface.message.set_message(self.dictionary._("Launch failed"), self.dictionary._("Unable to start process"))
                self.user_interface.message.display()
                self.close()

    # callback from httpd
    def _handle_http_request(self, (_peer_host, _peer_port), (_my_host, _my_port), path, callback):
        """
        Handle http request.
        If returning True then callback will be called later on with result.
        If returning False then access failed.
        Note: Lock to process has been disabled; it is too slow for Mac
        """
        assert not self.http_response_callback
        self.http_response_callback = callback
        self.tunnelendpoint_remote('http_request', path=path)
        return True

    # called from peer
    def http_response(self, status, data):
        assert self.http_response_callback
        # callback might cause new _handle_http_request immediately, so self.http_response_callback must be cleared before
        cb = self.http_response_callback
        self.http_response_callback = None
        cb(status, data)

    # called from peer
    def create(self, child_id):
        new_tunnel = self.create_and_add_tunnelendpoint_tunnel(self.checkpoint_handler, child_id)

        self._children[child_id] = client_launch_citrix_ica.IcaLaunchPortForward(
            self.async_service, self.checkpoint_handler, self.user_interface,
            new_tunnel, self._client_runtime_env,
            lambda child_id=child_id: self.please_close(child_id),
            self.cb_report_error_to_server)

    # called from a child
    def please_close(self, child_id):
        self.tunnelendpoint_remote('please_close', child_id=child_id)
