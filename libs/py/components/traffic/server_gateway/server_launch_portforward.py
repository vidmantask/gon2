from lib import checkpoint
from lib import encode

import lib.version
import components.traffic
from components.communication import tunnel_endpoint_base

import server_portforward
from components.traffic.server_common import database_schema


def icb_closed():
    """
    Notify that the portforward has been closed
    """
    pass


class LaunchPortForward(tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch):
    def __init__(self, async_service, checkpoint_handler, new_tunnelendpoint, launch_id, own_id,
            expand_variables, cb_closed, access_log_server_session, cb_report_error):
        tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch.__init__(self, async_service, checkpoint_handler, new_tunnelendpoint)
        self._access_log_server_session = access_log_server_session
        self.expand_variables = expand_variables
        self.cb_closed = cb_closed
        self.cb_report_error = cb_report_error
        self._close_from_client = False
        self._own_id = own_id

        self.launch_element = None
        self._next_child_id = 1 # 0 is special
        self._connecting_portforwards = set()
        self._portforwards = {}
        self.launch_id = launch_id

    def tunnelendpoint_connected(self):
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::_launch", components.traffic.module_id, checkpoint.DEBUG, launch_id=self.launch_id):
            self.launch_element = database_schema.LaunchElement.lookup(self.launch_id)
            if not self.launch_element:
                self.checkpoint_handler.Checkpoint("LaunchPortForward::_launch", components.traffic.module_id, checkpoint.ERROR, message="Launch id not found")
                return

            launch_command = self.expand_variables(self.launch_element.command)
            close_command = self.expand_variables(self.launch_element.close_command)
            working_directory = self.expand_variables(self.launch_element.working_directory)
            param_file_name = self.expand_variables(self.launch_element.param_file_name)
            param_file_lifetime = self.launch_element.param_file_lifetime
            param_file_template = self.expand_variables(self.launch_element.param_file_template)
            close_with_process = self.launch_element.close_with_process
            kill_on_close = self.launch_element.kill_on_close
            tcp_options = [x.encode('ascii', 'ignore').strip().upper() for x in self.launch_element.citrix_command.split(',')]
            client_portforwards = {}
            for line_no, portforward_spec in enumerate(self.launch_element.portforwards):
                server_host = encode.preferred(self.expand_variables(portforward_spec.server_host).strip())
                server_port = portforward_spec.server_port
                if not server_host:
                    self.checkpoint_handler.Checkpoint("LaunchPortForward::_launch", components.traffic.module_id, checkpoint.DEBUG,
                                                        message="Skipping portforward line %s without server_host" % (line_no))
                    continue

                child_id = self._next_child_id
                self._next_child_id += 1

                portforward = server_portforward.PortForwardServer(
                    self.async_service,
                    self.checkpoint_handler,
                    server_host,
                    server_port,
                    lambda child_id=child_id: self._portforward_ready(child_id),
                    lambda child_id=child_id: self._portforward_closed(child_id),
                    self.cb_report_error,
                    )

                self._portforwards[child_id] = (portforward_spec, portforward)
                self._connecting_portforwards.add(child_id)
                self.add_tunnelendpoint(child_id, portforward.as_tunnelendpoint())

                client_portforwards[child_id] = dict(
                    line_no=line_no,
                    client_host=portforward_spec.client_host,
                    client_port=portforward_spec.client_port,
                    lock_to_process_pid=portforward_spec.lock_to_process_pid,
                    sub_processes=portforward_spec.sub_processes,
                    lock_to_process_name=portforward_spec.lock_to_process_name,
                    )

                self._access_log_server_session.report_access_log_traffic_start((self._own_id, child_id),
                                                                                portforward_spec.client_host, portforward_spec.client_port,
                                                                                portforward.server_host, portforward.server_port)

            # PROTOCOL_CHANGE: This must be cleaned up when backward compatibility can be broken
            if lib.version.Version.compare(self.get_version_remote(), lib.version.Version.create(5, 3, 1)) == -1:
                self.tunnelendpoint_remote('remote_call_launch',
                                           launch_command=launch_command,
                                           close_command=close_command,
                                           working_directory=working_directory,
                                           param_file_name=param_file_name,
                                           param_file_lifetime=param_file_lifetime,
                                           param_file_template=param_file_template,
                                           close_with_process=close_with_process,
                                           kill_on_close=kill_on_close,
                                           client_portforwards=client_portforwards
                                           )
            else:
                self.tunnelendpoint_remote('remote_call_launch_with_tcp_options',
                                           launch_command=launch_command,
                                           close_command=close_command,
                                           working_directory=working_directory,
                                           param_file_name=param_file_name,
                                           param_file_lifetime=param_file_lifetime,
                                           param_file_template=param_file_template,
                                           close_with_process=close_with_process,
                                           kill_on_close=kill_on_close,
                                           client_portforwards=client_portforwards,
                                           tcp_options=tcp_options
                                           )



    def _portforward_ready(self, child_id):
        self.checkpoint_handler.Checkpoint("LaunchPortForward::_portforward_ready", components.traffic.module_id, checkpoint.DEBUG,
                                            child_id=child_id, waiting=len(self._connecting_portforwards))

        if not child_id in self._connecting_portforwards:
            self.checkpoint_handler.Checkpoint("LaunchPortForward::_portforward_ready:error", components.traffic.module_id, checkpoint.WARNING,
                                                child_id=child_id, msg="wasn't waiting for ready child",)
            return
        self._connecting_portforwards.remove(child_id)

        if self._connecting_portforwards:
            return # More to go
        # All portforwards ready

    def _portforward_closed(self, child_id):
        """
        Portforward has closed down
        """
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::_cb_closed", components.traffic.module_id, checkpoint.DEBUG,
                                                      child_id=child_id, waiting=len(self._portforwards)):
            self._portforwards.pop(child_id)
            if self._portforwards:
                return
            # Last portforward closed
            if not self._close_from_client:
                self.tunnelendpoint_remote('remote_call_closed')
            self._reset()
            self.cb_closed()

    def remote_call_launch_response(self, is_ok):
        """
        Called from client when the launc is done
        """
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::remote_call_launch_response", components.traffic.module_id, checkpoint.DEBUG,
                                                      is_ok="%s" % is_ok):
            pass

    def close(self, force=False, close_and_forget=False):
        """
        Close down the launched portforward
        """
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::close", components.traffic.module_id, checkpoint.DEBUG):
            for (_portforward_spec, portforward) in self._portforwards.values():
                portforward.close(force, close_and_forget)

            if close_and_forget:
                self._reset()

    def remote_call_closed(self):
        """
        Client is telling that is has closed down
        """
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::remote_call_closed", components.traffic.module_id, checkpoint.DEBUG):
            self._close_from_client = True
            for (_portforward_spec, portforward) in self._portforwards.values():
                portforward.close(True, True)
            self._reset()
            self.cb_closed()

    def _reset(self):
        while self._portforwards:
            (child_id, (_portforward_spec, _portforward)) = self._portforwards.popitem()
            self._access_log_server_session.report_access_log_traffic_close((self._own_id, child_id))
            self.remove_tunnelendpoint_tunnel(child_id)
