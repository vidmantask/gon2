import socket
import IPy

import components.traffic
from lib import checkpoint
from lib import encode

import socks
STATE_ERROR = 'STATE_ERROR'
STATE_NEW = 'STATE_NEW'
STATE_SOCKS5_NEGO = 'STATE_SOCKS5_NEGO'
STATE_CONNECTING = 'STATE_CONNECTING'
STATE_CONNECTED = 'STATE_CONNECTED'


def mk_enginefactory(log_environment, launch_element, expand_variables, server_host, server_port, servers):

    checkpoint_handler = log_environment.checkpoint_handler

    def dnslookup(fqdn, default=None):
        #self._env.log2('Dns lookup for %s', fqdn)
        try:
            infos = socket.getaddrinfo(fqdn, 0, socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP)
            (_family, _socktype, _proto, _canonname, (ip, _port)) = infos[0]
            return ip
        except socket.error, e:
            #self._env.log.log2('Error resolving %r: %s', fqdn, e)
            return default

    def mk_white_ipranges():
        white_ipranges = []
        for server in servers:
            server_host = encode.preferred(server.server_host or '').strip().lower()
            server_port = server.server_port or 0
            hashtags = [x for x in server_host.split() if x.startswith('#')]
            hosts = [x for x in server_host.split() if not x.startswith('#')] # strip and ignore hashtags to remain syntactically equivalent with socks
            if len(hosts) != 1:
                checkpoint_handler.Checkpoint("traffic async socks", components.traffic.module_id, checkpoint.ERROR,
                                              msg='multiple hosts', server_host=server_host)
                continue
            ip_s = hosts[0]
            try:
                iprange = IPy.IP(ip_s)
            except ValueError, e:
                checkpoint_handler.Checkpoint("traffic async socks", components.traffic.module_id, checkpoint.ERROR,
                                              msg='invalid IP range', e=str(e), server=server_host, ip_s=ip_s)
                continue
            white_ipranges.append((iprange, server_host, server_port, hashtags))
            checkpoint_handler.Checkpoint("traffic async socks", components.traffic.module_id, checkpoint.DEBUG,
                                          whitelist_iprange=str(iprange), server=server_host, port=server_port)
        return white_ipranges
    white_ipranges = mk_white_ipranges()

    def check_whitelist(request_ip, request_port, white_ipranges=white_ipranges):
        try:
            ip = IPy.IP(request_ip)
        except ValueError:
            checkpoint_handler.Checkpoint("traffic async socks", components.traffic.module_id, checkpoint.ERROR,
                                          error='bad IP address', request_ip=request_ip)
            return False 
        for (iprange, server_host, white_port, hashtags) in white_ipranges:
            if ip in iprange and (request_port == white_port or not white_port):
                if '#deny' in hashtags:
                    checkpoint_handler.Checkpoint("traffic async socks", components.traffic.module_id, checkpoint.DEBUG,
                                                  access='denied', request_ip=request_ip, request_port=request_port,
                                                  server_host=server_host, hashtags=' '.join(hashtags))
                    return False
                else:
                    checkpoint_handler.Checkpoint("traffic async socks", components.traffic.module_id, checkpoint.DEBUG,
                                                  access='authorized', request_ip=request_ip, request_port=request_port,
                                                  server_host=server_host, hashtags=' '.join(hashtags))
                    return True
        checkpoint_handler.Checkpoint("traffic async socks", components.traffic.module_id, checkpoint.DEBUG,
                                      access='not authorized', request_ip=request_ip, request_port=request_port)
        return False 
    
    def mk_logger(level, formatted=True):
        id_ = "SOCKS::log%s" % level
        if formatted:
            def logger(fmt, *a, **b):
                msg = fmt % a
                checkpoint_handler.Checkpoint(id_, components.traffic.module_id, checkpoint.DEBUG, msg=msg)
        else:
            def logger(fmt, *a, **b):
                if 'dump' in b:
                    b['dump'] = repr(b['dump'])
                d = dict(fmt = fmt)
                d.update((str(i), str(z)) for (i, z) in enumerate(a))
                d.update((k, str(v)) for (k, v) in b.items())
                checkpoint_handler.Checkpoint(id_, components.traffic.module_id, checkpoint.DEBUG, **d)
        return logger

    class Environment:
        log1 = staticmethod(lambda * a, **b: None)
        log2 = staticmethod(lambda * a, **b: None)
        log3 = staticmethod(lambda * a, **b: None)
        log1 = staticmethod(mk_logger(1))
        #log2 = staticmethod(mk_logger(2))
        #log3 = staticmethod(mk_logger(3))

    class SocksEngine(object):

        def __init__(self, cb_connect, cb_to_client, cb_close_client):
            self._cb_connect = cb_connect
            self._cb_to_client = cb_to_client
            self._cb_close_client = cb_close_client
            self._env = Environment
            self._access_dict = None
            self._socks_version = None
            self._state = STATE_NEW
            self._auth_addr = (None, None) # for nice error reporting
            self._cb_to_server = None
            self._cb_close_server = None

        # used for call back from connection to server
        def _server_error(self):
            if self._socks_version in (socks.SOCKS4, socks.SOCKS4a):
                self._socks4_error(socks.REP4_request_rejected_or_failed,
                                   'socket error connecting to %s:%s' % self._auth_addr, addr=self._auth_addr)
            else:
                self._socks5_error(socks.REP5_Connection_refused,
                                   'socket error connecting to %s:%s' % self._auth_addr, addr=self._auth_addr)

        def _connected(self, local, remote, write, close):
            """
            Callback from TCP when actually _connected.
            Sets STATE_CONNECTED and enables the client to do I/O on its connection as if it were directly
            _connected to the application server.
            """
            auth_ip, auth_port = self._auth_addr
            self._cb_to_server = write
            self._cb_close_server = close
            # (bnd_addr, bnd_port) = ('0.0.0.0', 0) # FIXME: reply with actual outgoing ip/port? Or is that an information leak?
            if self._socks_version in (socks.SOCKS4, socks.SOCKS4a):
                bnd_addr, bnd_port = local
                self._env.log2('%s created from %s:%s to %s:%s',
                    self, bnd_addr, bnd_port, auth_ip, auth_port)
                self._state = STATE_CONNECTED
                self._cb_to_client(socks.REP4_version + socks.REP4_request_granted +
                    socks.port_to_octets(bnd_port) + socks.ip4_to_octets(bnd_addr))

            elif self._socks_version == socks.SOCKS5:
                bnd_addr, bnd_port = local
                self._env.log2('%s created from %s:%s to %s:%s',
                    self, bnd_addr, bnd_port, auth_ip, auth_port)
                # 1928#6 reply
                self._state = STATE_CONNECTED
                self._cb_to_client(socks.VER_5 + socks.REP5_succeeded + '\x00' + socks.ATYP_IP_V4_address +
                        socks.ip4_to_octets(bnd_addr) + socks.port_to_octets(bnd_port))
            else: assert False, self._socks_version
            return (self._from_server, self.close)

        def from_client(self, in_buffer):
            """
            Handles data from client
            Returns tuple: excess data or None
            """
            self._env.log3('%s %s in_buffer %r', self, self._state, in_buffer)
            if self._state == STATE_NEW:
                if not in_buffer:
                    self._env.log2('%s received shutdown in %s', self, self._state)
                    return self._self_destruct()

                buffer_len = len(in_buffer)

                # 1928#3: version identifier
                field_ver = in_buffer[0]

                if field_ver == socks.VER_4:
                    self._socks_version = socks.SOCKS4 # http://antinat.sourceforge.net/std/SOCKS4.protocol
                    if buffer_len < 8:
                        return in_buffer

                    field_cmd = in_buffer[1]
                    if field_cmd != socks.CMD_CONNECT: # "should be 1 for CONNECT request" # FIXME: also support BIND
                        return self._socks4_error(socks.REP4_request_rejected_or_failed, 'Unknown cmd %r' % field_cmd)

                    request_port = socks.double_octet_to_port(in_buffer[2:4])
                    request_host = socks.quad_octet_to_ip4(in_buffer[4:8])
                    # Null-terminated user id - has no value, usually zero-length
                    user_id_length = 0
                    while 8 + user_id_length < buffer_len and in_buffer[8 + user_id_length] != '\x00':
                        user_id_length += 1
                    if in_buffer[8 + user_id_length] != '\x00':
                        return in_buffer
                    # user_id = self._buffer[8 : 8 + user_id_length]

                    # SOCKS4a hack
                    if request_host.startswith('0.0.0.') and request_host != '0.0.0.0':
                        self._socks_version = socks.SOCKS4a
                        if buffer_len < 9 + user_id_length:
                            return in_buffer
                        # Null-terminated host name
                        host_length = 0
                        while 9 + user_id_length + host_length < buffer_len and in_buffer[9 + user_id_length + host_length] != '\x00':
                            host_length += 1
                        if in_buffer[9 + user_id_length + host_length] != '\x00':
                            return in_buffer
                        host = in_buffer[9 + user_id_length : 9 + user_id_length + host_length]
                        request_host = dnslookup(host)
                        self._env.log2('%s socks4a lookup for %s gave %s', self, host, request_host)
                        if not request_host:
                            return self._socks4_error(socks.REP4_request_rejected_or_failed, 'DNS error for %s' % (host))
                        in_buffer = in_buffer[10 + user_id_length + host_length :]
                    else:
                        in_buffer = in_buffer[9 + user_id_length :]

                    self._auth_addr = request_host, request_port
                    if not check_whitelist(request_host, request_port):
                        return self._socks4_error(socks.REP4_request_rejected_or_failed, 'Not authorized to %s:%s' % (request_host, request_port), addr=self._auth_addr)

                    self._state = STATE_CONNECTING
                    self._cb_connect(self._auth_addr, cb_tcp_connected=self._connected, cb_tcp_error=None)
                    if in_buffer:
                        self._cb_to_server(in_buffer)
                    return ''

                elif field_ver == socks.VER_5:
                    self._socks_version = socks.SOCKS5
                    if buffer_len < 3:
                        return in_buffer

                    # 1928#3: method selection message:
                    field_nmethods = ord(in_buffer[1])
                    if buffer_len < field_nmethods + 2:
                        return in_buffer
                    field_methods = in_buffer[2 : 2 + field_nmethods]
                    if socks.METHOD_NO_AUTH not in field_methods:
                        # FIXME: Compliant implementations MUST support GSSAPI and SHOULD support USERNAME/PASSWORD authentication methods.
                        return self._socks5_error(socks.REP5_general_SOCKS_server_failure, 'Auth method %r not in %r' % (socks.METHOD_NO_AUTH, field_methods))

                    # 1928#3: METHOD selection message
                    self._state = STATE_SOCKS5_NEGO
                    self._cb_to_client(socks.VER_5 + socks.METHOD_NO_AUTH)
                    return in_buffer[2 + field_nmethods:] # Note: might deadlock - but is error
                else:
                    self._env.log1('%s Unknown socks version 0x%x %r', self, ord(field_ver), field_ver)
                    return self._self_destruct()

            elif self._state == STATE_SOCKS5_NEGO:
                if not in_buffer:
                    self._env.log1('%s received shutdown in %s', self, self._state)
                    return self._self_destruct()

                self._env.log3('%s SOCKS5 negotiation received %r', self, in_buffer)
                buffer_len = len(in_buffer)

                if buffer_len < 4:
                    return in_buffer

                # 1928#4: SOCKS request
                field_ver = in_buffer[0]
                if field_ver != socks.VER_5:
                    return self._socks5_error(socks.REP5_Command_not_supported, 'Illegal reply with version 0x%x %r' % (ord(field_ver), field_ver))

                field_cmd = in_buffer[1]
                if field_cmd != socks.CMD_CONNECT: # FIXME: support CMD_BIND and CMD_UDP_ASSOCIATE
                    return self._socks5_error(socks.REP5_Command_not_supported, 'Unknown cmd %r' % (field_cmd,))

                field_rsv = in_buffer[2]
                if field_rsv != '\x00':
                    return self._socks5_error(socks.REP5_Command_not_supported, 'Unknown rsv %r' % (field_rsv,))

                field_atyp = in_buffer[3]
                if field_atyp == socks.ATYP_IP_V4_address:
                    if buffer_len < 10:
                        return in_buffer
                    field_dst_addr = in_buffer[4 : 8]
                    request_host = socks.quad_octet_to_ip4(field_dst_addr)
                    field_dst_port = in_buffer[8 : 10]
                    in_buffer = in_buffer[11 :]
                elif field_atyp == socks.ATYP_DOMAINNAME:
                    name_length = ord(in_buffer[4])
                    if buffer_len < 7 + name_length:
                        return in_buffer
                    field_dst_addr = in_buffer[4 : 5 + name_length] # fully-qualified domain name
                    host = field_dst_addr[1 : ]
                    request_host = dnslookup(host)
                    self._env.log2('%s socks5 lookup for %s gave %s', self, host, request_host)
                    if not request_host:
                        return self._socks5_error(socks.REP5_Host_unreachable, 'DNS error for %s' % (host))
                    field_dst_port = in_buffer[5 + name_length : 7 + name_length]
                    in_buffer = in_buffer[7 + name_length :]
                elif field_atyp == socks.ATYP_IP_V6_address:
                    if buffer_len < 22:
                        return in_buffer
                    field_dst_addr = in_buffer[4 : 20]
                    request_host = socks.hexdeca_octet_to_ip6(field_dst_addr)
                    field_dst_port = in_buffer[20 : 22]
                    in_buffer = in_buffer[23 :]
                else:
                    return self._socks5_error(socks.REP5_Address_type_not_supported, 'Unknown atyp %r' % (field_atyp,))
                request_port = socks.double_octet_to_port(field_dst_port)

                self._auth_addr = request_host, request_port
                if not check_whitelist(request_host, request_port):
                    return self._socks5_error(socks.REP5_connection_not_allowed_by_ruleset, 'Not authorized to %s:%s' % (request_host, request_port), addr=self._auth_addr)

                self._state = STATE_CONNECTING
                self._cb_connect(self._auth_addr, cb_tcp_connected=self._connected, cb_tcp_error=None)
                if in_buffer:
                    self._cb_to_server(in_buffer)
                return ''

            elif self._state in (STATE_CONNECTING, STATE_CONNECTED):
                self._cb_to_server(in_buffer)
                return ''

            elif self._state == STATE_ERROR:
                if in_buffer:
                    self._env.log1('%s ignoring %r received after error', self, in_buffer)
                return self._self_destruct()

            else:
                self._env.log1('%s received %r in state %s', self, in_buffer, self._state)
                return self._self_destruct()

        def _from_server(self, data):
            """
            Handles data from server
            Returns: excess data or None
            """
            self._cb_to_client(data)
            return ''

        def close(self):
            if self._cb_close_server:
                self._cb_close_server()

        def _socks4_error(self, rep_code, info, addr=None):
            return self._socks_error(socks.REP4_version, rep_code, info, addr)

        def _socks5_error(self, rep_code, info, addr=None):
            return self._socks_error(socks.VER_5, rep_code, info, addr)

        def _socks_error(self, ver_code, rep_code, info, addr=None):
            """Socks protocol error occured - reply and close connection"""
            amsg = (' for %s:%s' % addr) if (addr and addr[0]) else ''
            self._env.log1('%s sending socks error reply %s%s: %s', self, ord(rep_code), amsg, info)
            binip = addr and socks.ip4_to_octets(addr[0])
            binaddr = (binip + chr(addr[1] >> 8) + chr(addr[1] & 0xff)) if binip else '\0\0\0\0\0\0'
            self._state = STATE_ERROR
            self._cb_to_client(ver_code + rep_code + '\x00' + socks.ATYP_IP_V4_address + binaddr)
            self._cb_to_client('')
            return ''

        def _self_destruct(self):
            """
            Close now, the only way to .close, no matter if error handling or nice closedown
            """
            self._env.log2('%s self destructing', self)
            return None

    return SocksEngine
