#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
"Web Service" API for accessing Citrix through XML web service

References:
http://www.sepago.de/e/nicholas/2008/07/17/talking-to-the-xml-service-update
nagios check_ica_program_neigbourhood.pl
WI4_Troubleshooters_Guide.pdf

Note: numeric escaping of non-ascii apparently doesn't work - ie ''.join('&#%s;' % ord(cp) for cp in username)
"""

import urllib2, socket

from xml.dom.minidom import parseString
import xml.parsers.expat

import citrix_ctx1_encoding

def monkey_patch_for_citrix():
    """
    Monkey patch for handling of Citrix using transfer-coding instead of transfer-encoding
    RTFRFC!
    """
    import httplib

    org_addheader = httplib.HTTPMessage.addheader

    def addheader(self, key, value):
        """Replace transfer-coding with transfer-encoding - but only once"""
        key = key.lower()
        if key == 'transfer-coding':
            key = 'transfer-encoding'
        if self.dict.get(key) != value:
            org_addheader(self, key, value)

    httplib.HTTPMessage.addheader = addheader

# Module-singleton initialization
monkey_patch_for_citrix()

class XmlServiceException(Exception): pass
class AuthenticationFailure(XmlServiceException): pass

def xmlesc(s):
    return s and s.replace('&', '&amp;').replace('<', '&lt;')

def get_dom(dedom, tagname, attrname=None, uni=False):
    elements = dedom.getElementsByTagName(tagname)
    if elements and len(elements) == 1 and elements[0]:
        if attrname:
            return str(elements[0].getAttribute(attrname))
        if elements[0].firstChild and hasattr(elements[0].firstChild, "data"):
            data = elements[0].firstChild.data
            #print tagname, '=', data
            if uni:
                return data
            return str(data)
    return None

class AppData(object):
    def __init__(self, appdata):
#       <InName>Outlook</InName>
        self.InName = get_dom(appdata, 'InName', uni=True) # Application name (semantic!)
#       <FName>Outlook</FName>
        self.FName = get_dom(appdata, 'FName', uni=True) # Display name
#       <Details></Details>
        #self.Details = get_dom(appdata, 'Details') # ??? ... PWL: commented out since it is not used anywhere
#       <SeqNo>1261425212</SeqNo>
        self.SeqNo = get_dom(appdata, 'SeqNo')
#       <ServerType>win32</ServerType>
        self.ServerType = get_dom(appdata, 'ServerType')
#       <ClientType>ica30</ClientType>
        self.ClientType = get_dom(appdata, 'ClientType')
#       <Settings appisdisabled="false" appisdesktop="false">
        settingslist = appdata.getElementsByTagName('Settings')
        settings = len(settingslist) == 1 and settingslist[0]
        self.appisdisabled = settings and settings.getAttribute('appisdisabled') == 'true' # Disabled but not hidden
        self.appisdesktop = settings and settings.getAttribute('appisdesktop') == 'true' # Type is Server Desktop - not seamless
#       <Description>Outlook&#32;through&#32;Citrix</Description>
        self.Description = settings and get_dom(settings, 'Description', uni=True) # Application description
#       <Folder/>
        self.Folder = settings and get_dom(settings, 'Folder', uni=True) # Client application folder
#       <WinType>pixels</WinType>
#       <WinWidth>1024</WinWidth>
#       <WinHeight>768</WinHeight>
#       <WinScale>1</WinScale>
        self.WinType = settings and get_dom(settings, 'WinType') # Session window size, 'pixels' or 'fullscreen' or 'percent'
        self.WinTypeFullscreen = settings and self.WinType == 'fullscreen'
        self.WinTypePixels = settings and self.WinType == 'pixels'
        self.WinWidth = settings and get_dom(settings, 'WinWidth') # Session window size, Custom Width
        self.WinHeight = settings and get_dom(settings, 'WinHeight') # Session window size, Custom Height
        self.WinTypePercent = settings and self.WinType == 'percent'
        self.WinScale = settings and get_dom(settings, 'WinScale') # Session window size, Percent of client desktop
#       <WinColor>8</WinColor>
        self.WinColor = settings and get_dom(settings, 'WinColor') # Colors, 8=True color (24-bit), 4=High color (16-bit), 2=256 colors, 1=16 colors
#       <SoundType minimum="false">basic</SoundType>
        self.SoundTypeBasic = settings and get_dom(settings, 'SoundType') == 'basic' # Enable legacy audio
        self.SoundTypeMinimum = settings and get_dom(settings, 'SoundType', 'minimum') == 'true' # Minimum requirement
#       <VideoType minimum="false">none</VideoType>
        self.VideoType = settings and get_dom(settings, 'VideoType')
#       <Encryption minimum="false">basic</Encryption>
        self.Encryption = settings and get_dom(settings, 'Encryption')
#       <AppOnDesktop value="false"/>
#       <AppInStartmenu value="true">æøå</AppInStartmenu>
        self.AppOnDesktop = settings and get_dom(settings, 'AppOnDesktop', 'value') == 'true' # Add shortcut to client's desktop
        self.AppInStartmenu = settings and get_dom(settings, 'AppInStartmenu', 'value') == 'true' # Add to client's Start menu
        self.AppInStartmenuRoot = settings and get_dom(settings, 'AppInStartmenu', 'root') # Set to "program" for Programs folder
        self.AppInStartmenuFolder = settings and get_dom(settings, 'AppInStartmenu', uni=True) # Start menu folder
#       <PublisherName>GiritechFarm</PublisherName>
        self.PublisherName = settings and get_dom(settings, 'PublisherName', uni=True) # Farm name
#       <SSLEnabled>false</SSLEnabled>
        self.SSLEnabled = settings and get_dom(settings, 'SSLEnabled') == 'true' # Enable SSL and TLS protocols
#       <RemoteAccessEnabled>false</RemoteAccessEnabled>
        self.RemoteAccessEnabled = settings and get_dom(settings, 'RemoteAccessEnabled') # ???

    def __repr__(self):
        return '<%s %s>' % (self.__class__.__name__, self.__dict__)

class ResponseAddress(object):

    def __init__(self, dedom):
#       <ServerAddress addresstype="dot">192.168.45.6</ServerAddress>
#       <ServerType>win32</ServerType>
#       <ConnectionType>tcp</ConnectionType>
#       <ClientType>ica30</ClientType>
#       <TicketTag>IMAHostId:12940</TicketTag>
#       <FarmLoadHint>800</FarmLoadHint>
#       <SSLRelayAddress addresstype="dns">x-men.giritech.com</SSLRelayAddress>
#       <CGPAddress addresstype="port">2598</CGPAddress>
        self.ServerAddress = get_dom(dedom, 'ServerAddress') # TODO: addresstype="dot"?
        self.ServerType = get_dom(dedom, 'ServerType')
        self.ConnectionType = get_dom(dedom, 'ConnectionType')
        self.ClientType = get_dom(dedom, 'ClientType')
        self.TicketTag = get_dom(dedom, 'TicketTag')
        self.FarmLoadHint = get_dom(dedom, 'FarmLoadHint')
        self.SSLRelayAddress = get_dom(dedom, 'SSLRelayAddress') # addresstype="dns"
        self.CGPAddress = get_dom(dedom, 'CGPAddress') # addresstype="port"

    def __repr__(self):
        return '<%s %s>' % (self.__class__.__name__, self.__dict__)

class XenAppXMLService(object):

    def __init__(self, server_host, server_port, username, password, domain, verbose=False, timeout=3):
        self.url = 'http://%s:%s/scripts/wpnbr.dll' % (server_host.strip().encode('utf-8'), server_port)
        self.timeout = timeout
        self.username = username
        self.domain = domain
        self._credentials_xml = '''
            <Credentials>
                <UserName>%s</UserName>
                <Password encoding="ctx1">%s</Password>
                <Domain type="NT">%s</Domain>
            </Credentials>''' % (
                xmlesc(username),
                citrix_ctx1_encoding.encoded(password),
                xmlesc(domain))
        self.verbose = verbose

        self.serverFarmName = self.ServerFarmName()
        #print 'ServerFarmName:', self.serverFarmName
        self.capabilities = self.Capabilities()
        #print 'Capabilities:', str(', '.join(self.capabilities))
        #Removed check for integrated-authentication - XenApp 6.5 server does not always report this
	#if 'integrated-authentication' not in self.capabilities:
        #    raise XmlServiceException('Missing capability integrated-authentication')
        if 'separate-credentials-validation' in self.capabilities:
            self.ValidateCredentials()

        self.apps = self.RequestAppData(details=True, verbose=verbose)
        #print 'AppDataList:'
        #for app in self.apps:
        #    print '\t', app

    def launch_info(self, appname, verbose=False):
        #print 'Appname:', appname
        addrs = self.RequestAddress(appname)
        addr = addrs[0]
        #print 'RequestAddress:', addr
        if not addr.TicketTag: # TicketTag is None if application disabled
            raise XmlServiceException('%r not available' % (appname))

        ticket = self.RequestTicket(addr.TicketTag)
        #print 'RequestTicket:', ticket
        if not ticket or len(ticket) != 30:
            raise XmlServiceException('Invalid ticket %r for %r' % (ticket, appname))

        appdatadetails = self.RequestAppData(appname=appname, details=True, verbose=verbose)
        if len(appdatadetails) != 1:
            raise XmlServiceException('Invalid AppData for %r' % (appname))

        return dict(
            appname=appname,
            address=addr.ServerAddress,
            ticket=ticket,
            clearpassword=ticket[:15],
            domain='\\' + ticket[15:],
            cgpaddress=addr.CGPAddress or 2598,
            #clientname = ('%s-%s' % (self.domain, self.username))[:19],
            clientname=self.username, # try to avoid more than 19 bytes encoded
            sessionsharingkey='G-On-%s-%s' % (self.domain, self.username),
            username=self.username,
            # details:
            twimode='Off' if appdatadetails[0].appisdesktop else 'On',
            desiredcolor=appdatadetails[0].WinColor,
            windowlines='; windowlines:\n' + (
                'DesiredHRES=%s\nDesiredVRES=%s' % (appdatadetails[0].WinWidth, appdatadetails[0].WinHeight) if appdatadetails[0].WinTypePixels else
                'UseFullScreen=On' if appdatadetails[0].WinTypeFullscreen else
                'ScreenPercent=%s' % appdatadetails[0].WinScale if appdatadetails[0].WinTypePercent else
                '; None'),
            )

    def _rpc(self, method, req_xml, cred=False, verbose=False):
        data = ('''<?xml version="1.0" encoding="UTF-8"?>
            <!DOCTYPE NFuseProtocol SYSTEM "NFuse.dtd">
            <NFuseProtocol version="5.0">
                <%s>%s%s
                </%s>
            </NFuseProtocol>
            ''' % (method, req_xml, self._credentials_xml if cred else '', method)).encode('utf-8')
        if verbose or self.verbose:
            print
            print 'Request %s: %s' % (self.url, method)
            print data

        try:
            urlreq = urllib2.Request(url=self.url, data=data, headers={"Content-Type": "text/xml"})
            f = urllib2.urlopen(urlreq, timeout=self.timeout)
        except socket.timeout, e:
            raise XmlServiceException('Timeout: %s' % e)
        except urllib2.HTTPError, e:
            raise XmlServiceException('HTTP error: %s' % e.msg)
        except urllib2.URLError, e:
            if isinstance(e.reason, socket.error):
                if len(e.reason.args) == 2:
                    raise XmlServiceException('URL error: %s' % (e.reason.args[1],))
                raise XmlServiceException('URL error: %s' % (e.reason.args[0],))
            raise XmlServiceException('URL error: %s' % (e.reason.args[0],))

        resp = f.read()
        if verbose or self.verbose:
            print 'Response:'
            print f.headers
            print resp
            print

        try:
            return parseString(resp)
        except xml.parsers.expat.ExpatError, e:
            print 'parseString error:', e
            print f.headers
            print resp
            raise XmlServiceException('XML error: %s' % e)

    def ServerFarmName(self):
        resp = self._rpc('RequestServerFarmData', '<Nil />')
        # <ServerFarmName>NewFarmName</ServerFarmName>
        return get_dom(resp, 'ServerFarmName', uni=True)

    def Capabilities(self):
        #Before authenticating a user, Web Interface retrieves the capabilities of the server hosting the XML service (step 1: RequestCapabilities)
        resp = self._rpc('RequestCapabilities', '<Nil />')
        # <CapabilityId>separate-credentials-validation</CapabilityId><CapabilityId>launch-reference</CapabilityId><CapabilityId>integrated-authentication</CapabilityId>
        # <CapabilityId>rade-session-proxy</CapabilityId><CapabilityId>multi-image-icons</CapabilityId>
        # * separate-credentials-validation: The XML service is able to validate credentials before actually requesting applications.
        # * multi-image-icons: Web Interface 4.6 allows for icons to be presented in higher resolutions and colour depths. This capability enables an application to request a specific icon format from the XML service.
        return [x.firstChild.data for x in resp.getElementsByTagName('CapabilityId')]

    def ValidateCredentials(self): # requires capability separate-credentials-validation, NOT available on ctx01!
        #... and attempts to logon the user with the presented credentials (step 2: RequestValidateCredentials).
        #There is no kind of session for authenticated users maintained by the XML service.
        #Instead the ResponseValidateCredentials merely indicates whether a user has presented matching credentials to the Web Interface.
        #All requests for user-specific information contain the credentials cached by Web Interface (see the requests in steps 3 and 5).
        #The password is presented to the XML service in a scrambled string.
        resp = self._rpc('RequestValidateCredentials', '', cred=True)
        # silence or <ErrorId>failed-credentials</ErrorId>
        errors = get_dom(resp, 'ErrorId')
        if errors: # often 'failed-credentials'
            raise AuthenticationFailure('Auth error: %s' % errors)

    def RequestAppData(self, appname=None, details=False, verbose=False):
        #Step 3 asks for a list of applications for the user identified by the credentials, the client name and the IP address of the client device.
        #Note that the response contains only a few pieces of information and that Web Interface needs to send a second RequestAppData (step 4) to obtain presentation (e.g. icon data) and launch information.
        #Web Interface needs to send a second RequestAppData (step 4) to obtain presentation (e.g. icon data) and launch information.
        # Note: Disabled applications are included unless they are hidden
        resp = self._rpc('RequestAppData', '''
            <Scope traverse="subtree" />
            <ServerType>all</ServerType>
            <ClientType>ica30</ClientType>
            ''' +
            # <ClientType>content</ClientType>
            ('''<DesiredDetails>defaults</DesiredDetails>
            ''' if details else '') +
            # <DesiredDetails>file-type</DesiredDetails>
            # <DesiredIconData size="32" bpp="4" format="raw"></DesiredIconData>
            ('''<AppName>%s</AppName>
            ''' % (xmlesc(appname),) if appname else ''),
            cred=True, verbose=verbose)
        return [AppData(x) for x in resp.getElementsByTagName('AppData')]

    def RequestAddress(self, appname, verbose=False):
        # As soon as a user launches an application by clicking on the corresponding icon and text (step 6),
        # Web Interface determines the address of a member server able to serve the requested application (RequestAddress).
        #_AppName = '''
        #    <SessionName>
        #        <InName>%s</InName> % appname
        #        <HostId type="ima-host-id">22488</HostId>
        #        <SessionId>3</SessionId>
        #    </SessionName>'''
	# With XenApp 6, it seems necessary to specify more parameters than with XenApp 5
	# We try with ClientName (bogus), ClientAddress (bogus) and ClientType
        resp = self._rpc('RequestAddress', '''
            <Name>
                <AppName>%s</AppName>
            </Name>
            <ClientName>G-On_Server</ClientName>
            <ClientAddress addresstype="dot">127.0.0.1</ClientAddress>
            <ClientType>ica30</ClientType>
	    <ServerAddress addresstype="dot-port" />
            ''' % (xmlesc(appname),),
            cred=True, verbose=verbose)

        # <ServerAddress addresstype="dot">192.168.45.159</ServerAddress><ClientType>ica30</ClientType><TicketTag>IMAHostId:22488</TicketTag>
        # requesting ServerAddress addresstype="dot-port" also gives CGPAddress port
        return [ResponseAddress(x) for x in resp.getElementsByTagName('ResponseAddress')] # Apparently there is only one - the best?


    def RequestTicket(self, TicketTag, verbose=False):
        # After retrieving a ticket for the user session to be initiated (step 7),
        # the web browser is sent the file launch.ica resulting in the launch of the locally installed client.
        resp = self._rpc('RequestTicket', '''
            <TicketType>CtxLogon</TicketType>
            <TicketTag>%s</TicketTag>
            <TimetoLive>200</TimetoLive>
            ''' % xmlesc(TicketTag),
            cred=True, verbose=verbose)
        # Invalid app causes invalid ticket which causes:
        # <MPSError type="IMA">0x80000001</MPSError>
        # <BrowserError>0x00000024</BrowserError>
        #Most information used to build the file launch.ica results from the second RequestAppData during logon (step 4) cached by the Web Interface.
        #print resp.toxml()
        return get_dom(resp, 'Ticket')

def main():
    server, username, password, domain = '192.168.45.6', 'xxx', 'xxx', 'giritech'
    #server, username, password, domain = 'ctx01', 'demo', 'xxx', 'demo'
    #server, username, password, domain = 'dev-test-citrix', u'user01', u'xxx', 'devtest2008'
    #server, username, password, domain = 'dev-test-citrix', u'æøå ÆØÅ', u'xxxå', 'dev-test-citrix'
    xa = XenAppXMLService(server, 80, username, password, domain, verbose=False)

    app = xa.apps[0]
    print 'app', app.InName

    d = xa.launch_info(app.InName)
    print 'launch info dict:'
    print d

    icaname = '/tmp/launch.ica'
    file(icaname, 'w').write(("""
[Encoding]
InputEncoding=UTF8

[WFClient]
CPMAllowed=On
ClientName=%(clientname)s
;ProxyFavorIEConnectionSetting=No
;ProxyTimeout=30000
;ProxyType=Auto
;ProxyUseFQDN=Off
RemoveICAFile=no
TransparentKeyPassthrough=Local
TransportReconnectEnabled=On
VSLAllowed=On
Version=2
VirtualCOMPortEmulation=Off

[ApplicationServers]
%(appname)s=

[%(appname)s]
Address=%(address)s
AutologonAllowed=ON
BrowserProtocol=HTTPonTCP
;CGPAddress=*:%(cgpaddress)s
ClearPassword=%(clearpassword)s
Username=%(username)s
ClientAudio=On
DesiredColor=%(desiredcolor)s
DoNotUseDefaultCSL=On
Domain=%(domain)s
FontSmoothingType=0
InitialProgram=#%(appname)s
;LPWD=
;LaunchReference=d26bIqh+gf0DiekCdBL5ump9oEtCG1Y3DPUifj6Bes4=
Launcher=WI
LocHttpBrowserAddress=!
LogonTicket=%(ticket)s
LogonTicketType=CTXS1
LongCommandLine=
;NRWD=
;ProxyTimeout=30000
;ProxyType=Auto
SFRAllowed=Off
SSLEnable=Off
SessionsharingKey=%(sessionsharingkey)s
;StartIFDCD=1263811530086
;StartSCD=1263811530086
;TRWD=
TWIMode=%(twimode)s
Title=%(appname)s
TransportDriver=TCP/IP
UILocale=en
WinStationDriver=ICA 3.0
%(windowlines)s

[Compress]
DriverNameWin16=pdcompw.dll
DriverNameWin32=pdcompn.dll

[EncRC5-0]
DriverNameWin16=pdc0w.dll
DriverNameWin32=pdc0n.dll

[EncRC5-128]
DriverNameWin16=pdc128w.dll
DriverNameWin32=pdc128n.dll

[EncRC5-40]
DriverNameWin16=pdc40w.dll
DriverNameWin32=pdc40n.dll

[EncRC5-56]
DriverNameWin16=pdc56w.dll
DriverNameWin32=pdc56n.dll
    """ % d).encode('utf-8'))
    #xa.launch(xa.apps[0])

    import os
    os.system('LANG=C /usr/lib/ICAClient/wfica.sh %s' % icaname)

if __name__ == '__main__':
    main()
