'''
Design decisions:
nothing global, each launch get its own state
always sticky, round-robin-next on failure
total failure after two rounds
failure timeout: #timeout=3
space separated, hash options first, :port can be specified and trumps port field
no multi-a handling - will give load balancing but no fail-over (lookup to IP will cause trouble for name-based hosting)
* priority, start from first, #failover
* random, random start, #random (default)
'''
import random

MODE_RANDOM, MODE_FAILOVER = range(2)
MODE = {'#random': MODE_RANDOM,
        '#failover': MODE_FAILOVER,
        }

class Failover(object):
    '''
    Launch-specific state for failover / load balancing

    >>> f = Failover('#timeout=7 #failover #rounds=1 a b:117', port=0)
    >>> f.timeout
    7
    >>> f.get() # first
    ('a', 0)
    >>> f.failure(f.get()) and f.get() # fail over to second
    ('b', 117)
    >>> f.failure(('not', 117)) and f.get() # bad failure is happily accepted but makes no change
    ('b', 117)
    >>> f.failure(('b', 117)) # failed again - that is serious
    False
    >>> f.get() # return something
    ('a', 0)
    >>> f.failure(('not', 117)) and f.get() # bad failure not accepted
    False
    '''

    def __init__(self, spec, port=None, timeout=3, rounds=2):
        '''
        Parse spec and prepare for first get
        '''
        self._port = port
        self.timeout = timeout
        mode = MODE_RANDOM
        l = spec.split()
        if len(l) <= 1:
            rounds = 1 # default if one host and no options
        while l and l[0].startswith('#'):
            x = l[0].lower()
            if x.startswith('#timeout='):
                try:
                    self.timeout = int(x.split('=', 1)[1])
                except ValueError: pass
            elif x.startswith('#rounds='):
                try:
                    rounds = int(x.split('=', 1)[1])
                except ValueError: pass
            elif x in MODE:
                mode = MODE[x]
            else:
                #print 'unknown', x
                break
            del l[0]
        if l:
            self._hosts = l
            self._left = rounds * len(self._hosts)
        else:
            self._hosts = [spec] # if spec is empty or without real hosts then use the full spec even though it won't work.
            self._left = 1
        if mode == MODE_FAILOVER:
            self._current = 0
        else:
            self._current = random.randrange(len(self._hosts))
        #print 'failover hosts', (2*self._hosts)[self._current:self._current+len(self._hosts)]

    def get(self):
        '''
        Retrieve the current/next host
        Is advanced by .failure
        '''
        #import time; time.sleep(1) # evil testing
        host = self._hosts[self._current % len(self._hosts)]
        try:
            h, p = host.split(':', 1)
            return h, int(p)
        except ValueError:
            return host, self._port

    def failure(self, failed_target):
        '''
        Record that a connection failed and advance _current.
        Returns True if there is more to try, False if failure was fatal
        '''
        #print 'failure', self._current, self._left, failed_target
        if failed_target != self.get() and self._left > 0:
            return True # things have changed - just try again
        # Now it gets slightly racy - that shouldn't matter ...
        self._current += 1
        self._left -= 1
        return self._left > 0

if __name__ == '__main__':
    import doctest
    doctest.testmod()
