import server_launch_async_http
import server_launch_async_socks
from lib import checkpoint
import components.traffic

def mk_enginefactory(log_environment, launch_element, expand_variables, server_host, server_port, servers):
    # Note: The actual http or socks mk_enginefactory will only be run after the first data has been received
    
    checkpoint_handler = log_environment.checkpoint_handler
    
    class ProxyEngine(object):
        """Automatic detection of socks and http proxying"""

        def __init__(self, cb_connect, cb_to_client, cb_close_client):
            self._cb_connect = cb_connect
            self._cb_to_client = cb_to_client
            self._cb_close_client = cb_close_client

        # will be overwritten with http or socks method
        def close(self):
            cb_close_client, self._cb_close_client = self._cb_close_client, None
            if cb_close_client:
                cb_close_client()
            self._cb_connect = None
            self._cb_to_client = None

        # will be overwritten with http or socks method
        def from_client(self, data):
            checkpoint_handler.Checkpoint("ProxyEngine::from_client", components.traffic.module_id, checkpoint.DEBUG,
                                          data=len(data), start=str(data and ord(data[0])))
            if data and data[0] in '\x04\x05':
                kindfact = server_launch_async_socks.mk_enginefactory
            else:
                kindfact = server_launch_async_http.mk_enginefactory
            fact = kindfact(log_environment, launch_element, expand_variables, server_host, server_port, servers)
            engine = fact(self._cb_connect, self._cb_to_client, self._cb_close_client)
            self.close = engine.close
            self.from_client = engine.from_client
            self._cb_close_client = None
            self._cb_connect = None
            self._cb_to_client = None
            return self.from_client(data)

    return ProxyEngine
