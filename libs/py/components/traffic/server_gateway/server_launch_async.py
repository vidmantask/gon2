from operator import attrgetter

from lib import checkpoint
from lib import encode

from components.communication import tunnel_endpoint_base, async_socket, tunnel_endpoint_tcp
import components.traffic.server_common
from components.traffic.server_gateway import server_launch_async_rdp
from components.traffic.server_gateway import server_launch_async_plain
from components.traffic.server_gateway import server_launch_async_proxy
import failover


class EngineAccessLogger(object):
    
    def_log_settings = {
                        'http': 'log_http_connections',
                        'tcp' : 'log_tcp_connections'
                        }
    
    def __init__(self, own_id, child_id, checkpoint_handler, access_log_server_session, launch_element_log_settings):
        self.proxy_id = own_id
        self.child_id = child_id
        self.checkpoint_handler = checkpoint_handler
        self.access_log_server_session = access_log_server_session
        self.local_log_settings = dict([(log_setting.log_setting_name, log_setting.log_setting_enabled) for log_setting in launch_element_log_settings])
        
        
    def get_logger(self, log_id="AsyncEngine::log1" , module_name=components.traffic.module_id):
        def logger(fmt, *a, **b):
            if fmt:
                msg = fmt % a
                checkpoint_log_id = b.pop("log_id", log_id)
                checkpoint_module_name = b.pop("module_name", module_name)
                self.checkpoint_handler.Checkpoint(checkpoint_log_id, checkpoint_module_name, checkpoint.DEBUG, msg=msg)
            else:
                msg=None
            log_type = b.get("log_type")
            if log_type:
                log_type_category = log_type.split("_")[0]
                log_setting_name = self.def_log_settings.get(log_type_category)
                force_enabled = False
                if log_setting_name:
                    enabled = self.local_log_settings.get(log_setting_name)
                    if enabled is False:
                        return
                    elif enabled is True:
                        force_enabled = True
                        
                self.access_log_server_session.report_access_log_menu_action_details(log_type, self.proxy_id, self.child_id, msg, b, force_enabled=force_enabled)
                
        
        return logger


class LaunchAsync(tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch):

    def __init__(self, async_service, checkpoint_handler, tunnel_endpoint,
            access_log_server_session, expand_variables, own_id, launch_id, cb_closed, server_config):
        tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch.__init__(self,
            async_service, checkpoint_handler, tunnel_endpoint)
        self._cb_closed = cb_closed
        self._portforwards = {}
        self._connecting_portforwards = set()

        self._access_log_server_session = access_log_server_session
        self._expand_variables = expand_variables
        self._own_id = own_id
        self.launch_id = launch_id
        self._next_child_id = 1
        self._access_log_ids = []
        self._access_log_ids_next_id = 1
        self.server_config = server_config

    def tunnelendpoint_connected(self):
        with self.checkpoint_handler.CheckpointScope("LaunchAsync::tunnelendpoint_connected", components.traffic.module_id, checkpoint.DEBUG, launch_id=self.launch_id):
            launch_element = components.traffic.server_common.database_schema.LaunchElement.lookup(self.launch_id)
            if not launch_element:
                self.checkpoint_handler.Checkpoint("LaunchAsync::tunnelendpoint_connected", components.traffic.module_id, checkpoint.ERROR, message="Launch id not found")
                return

            launch_command = self._expand_variables(launch_element.command)
            close_command = self._expand_variables(launch_element.close_command)
            working_directory = self._expand_variables(launch_element.working_directory)
            param_file_name = self._expand_variables(launch_element.param_file_name)
            param_file_lifetime = launch_element.param_file_lifetime
            param_file_template = self._expand_variables(launch_element.param_file_template)
            close_with_process = launch_element.close_with_process
            kill_on_close = launch_element.kill_on_close
            tcp_options = [x.encode('ascii', 'ignore').strip().upper() for x in launch_element.citrix_command.split(',')]
            client_portforwards = {}

            portforwards = sorted(launch_element.portforwards, key=attrgetter('line_no'))
            if not portforwards:
                self.checkpoint_handler.Checkpoint("LaunchAsync::tunnelendpoint_connected", components.traffic.module_id, checkpoint.ERROR,
                                                   launch_id=self.launch_id, msg="no portforwards defined")
                return
            portforward_spec = launch_element.portforwards[0]

            server_host = encode.preferred(self._expand_variables(portforward_spec.server_host).strip())
            server_port = portforward_spec.server_port

            engine_factory = (
                server_launch_async_rdp.mk_enginefactory if launch_element.launch_type in [6, 10] else
                server_launch_async_proxy.mk_enginefactory if launch_element.launch_type in [7, 8, 11] else
                server_launch_async_plain.mk_enginefactory
                )


            class LogEnvironment():
                
                def __init__(self, checkpoint_handler, access_logger):
                    self.checkpoint_handler = checkpoint_handler
                    self.access_logger = access_logger
                    
            access_logger = EngineAccessLogger(self._own_id, self._next_child_id, self.checkpoint_handler, self._access_log_server_session, launch_element.log_settings)
            log_environment = LogEnvironment(self.checkpoint_handler, access_logger)

            connectionenginefactory = engine_factory(
                log_environment,#self.checkpoint_handler,
                launch_element,
                self._expand_variables,
                server_host=server_host,
                server_port=server_port,
                servers=portforwards[1:])

            child_id = self._next_child_id
            self._next_child_id += 1

            if launch_element.launch_type in [8, 9, 10, 11]:
                self.checkpoint_handler.Checkpoint("LaunchAsync::tunnelendpoint_connected.no_message_wrapping",
                                                   components.traffic.module_id, checkpoint.DEBUG,
                                                   launch_id=self.launch_id)
                portforward = AsyncConnectorPortForwardServerWithConnectionEngine(
                    self.async_service,
                    log_environment,
                    connectionenginefactory
                )
                if('LOG_TO_SERVER_ENABLED' in tcp_options):
                    print "LOG_TO_SERVER_ENABLED log_client_path:", self.server_config.log_client_path
                    portforward.enable_checkpoint_tunnel(self.server_config.log_client_path)
                self.add_tunnelendpoint(child_id, portforward.as_tunnelendpoint())
            else:
                self.checkpoint_handler.Checkpoint("LaunchAsync::tunnelendpoint_connected.message_wrapping",
                                                   components.traffic.module_id, checkpoint.DEBUG,
                                                   launch_id=self.launch_id)
                portforward = AsyncConnector(
                    self.async_service,
                    log_environment,
                    self.create_and_add_tunnelendpoint_tunnel(self.checkpoint_handler, child_id),
                    #self.cb_report_error,
                    connectionenginefactory,
                    )
            self._portforwards[child_id] = (portforward_spec, portforward)
            self._connecting_portforwards.add(child_id)

            client_portforwards[child_id] = dict(
                line_no=portforward_spec.line_no,
                client_host=portforward_spec.client_host,
                client_port=portforward_spec.client_port,
                lock_to_process_pid=portforward_spec.lock_to_process_pid,
                sub_processes=portforward_spec.sub_processes,
                lock_to_process_name=portforward_spec.lock_to_process_name,
                )

            if launch_element.launch_type in [6, 10]:
                self._report_access_log_traffic_start(portforward_spec, portforward_spec)
            elif launch_element.launch_type in [7, 8, 11]:
                for portforward_spec_white_list in launch_element.portforwards[1:]:
                    self._report_access_log_traffic_start(portforward_spec, portforward_spec_white_list)

            self.tunnelendpoint_remote('remote_launch',
                                       launch_command=launch_command,
                                       close_command=close_command,
                                       working_directory=working_directory,
                                       param_file_name=param_file_name,
                                       param_file_lifetime=param_file_lifetime,
                                       param_file_template=param_file_template,
                                       close_with_process=close_with_process,
                                       kill_on_close=kill_on_close,
                                       client_portforwards=client_portforwards,
                                       tcp_options=tcp_options
                                       )

    def _report_access_log_traffic_start(self, portforward_spec_client, portforward_spec_server):
        self._access_log_server_session.report_access_log_traffic_start(
                (self._own_id, self._access_log_ids_next_id),
                portforward_spec_client.client_host, portforward_spec_client.client_port,
                portforward_spec_server.server_host, portforward_spec_server.server_port)
        self._access_log_ids.append( (self._own_id, self._access_log_ids_next_id) )
        self._access_log_ids_next_id += 1

    def remote_call_launch_response(self, is_ok):
        """
        Called from client when the launch is done
        """
        self.checkpoint_handler.Checkpoint("LaunchAsync::remote_call_launch_response", components.traffic.module_id, checkpoint.DEBUG, is_ok="%s" % is_ok)
        if not is_ok:
            self._access_log_server_session.report_access_log_gateway_error('traffic', 'traffic:0001', 'Launch failed on client', 'Please check launch command')

    def remote_call_launch_response_with_message(self, is_ok, message):
        """
        Called from client when the launch is done
        """
        self.checkpoint_handler.Checkpoint("LaunchAsync::remote_call_launch_response_message", components.traffic.module_id, checkpoint.DEBUG, is_ok="%s" % is_ok, message=message)
        if not is_ok:
            self._access_log_server_session.report_access_log_gateway_error('traffic', 'traffic:0001', 'Launch failed on client', message)

    def remote_close(self, message):
        self.checkpoint_handler.Checkpoint("LaunchAsync::remote_close", components.traffic.module_id, checkpoint.DEBUG, message=message)
        self.close()
        self._cb_closed()

    def remote_call_closed(self):
        """
        This method can be called when client side is a portforward (launch_type=8)
        """
        self.checkpoint_handler.Checkpoint("LaunchAsync::remote_call_closed", components.traffic.module_id, checkpoint.DEBUG)
        self.close()
        self._cb_closed()

    def close(self, force=False, close_and_forget=False):
        self._expand_variables = None
        for child_id in self._portforwards:
            (_portforward_spec, portforward) = self._portforwards[child_id]
            portforward.close()
        for (own_id, access_log_id) in self._access_log_ids:
            self._access_log_server_session.report_access_log_traffic_close((own_id, access_log_id))


class AsyncConnector(tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch):
    """
    Represent the server side of the proxy.

    On request from client proxy a connection (created using a connector) is established to ip:port.
    """
    def __init__(self, async_service, log_environment, tunnel_endpoint, enginefactory):
        tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch.__init__(self, async_service, log_environment.checkpoint_handler, tunnel_endpoint)
        self._enginefactory = enginefactory
        self._connections = {}
        self._running = True
        self._log_environment = log_environment

    def remote_accepted(self, connection_id):
        self.checkpoint_handler.Checkpoint("AsyncConnector::remote_accepted", components.traffic.module_id, checkpoint.DEBUG, connection_id=connection_id)
        if self._running:
            connection_tunnel_endpoint = self.create_and_add_tunnelendpoint_tunnel(self.checkpoint_handler, connection_id)
            self._connections[connection_id] = AsyncConnection(self.async_service, self._log_environment, connection_tunnel_endpoint,
                                                               lambda: self.async_connection_with_remote_closed(connection_id), self._enginefactory)

    # callback from connection itself
    def async_connection_with_remote_closed(self, connection_id):
        self.checkpoint_handler.Checkpoint("AsyncConnector::async_connection_with_remote_closed", components.traffic.module_id, checkpoint.DEBUG, connection_id=connection_id)
        self.remove_tunnelendpoint_tunnel(connection_id)
        try:
            del self._connections[connection_id]
            self.checkpoint_handler.Checkpoint("AsyncConnector::async_connection_with_remote_closed", components.traffic.module_id, checkpoint.DEBUG)
        except KeyError:
            self.checkpoint_handler.Checkpoint("AsyncConnector::async_connection_with_remote_closed", components.traffic.module_id, checkpoint.DEBUG, msg='KeyError')

    def close(self):
        self.checkpoint_handler.Checkpoint("AsyncConnector::close", components.traffic.module_id, checkpoint.DEBUG)
        self._running = False
        for connection_id in self._connections.keys():
            self._connections[connection_id].close_all()

    def is_done(self):
        return not self._running and len(self._connections) == 0


class AsyncConnectorPortForwardServerWithConnectionEngine(tunnel_endpoint_tcp.TCPPortforwardServerWithConnectionEngine):
    """
    Represent the server side of the proxy, where the client side of the tunnelendpoint is a plain portforward,
    not using message wrapping.

    On request from client proxy a connection (created using a connector) is established to ip:port.
    """
    def __init__(self, async_service, log_environment, enginefactory):
        tunnel_endpoint_tcp.TCPPortforwardServerWithConnectionEngine.__init__(self, async_service, log_environment.checkpoint_handler)
        self._enginefactory = enginefactory
        self._connections = {}
        self._running = True
        self._log_environment = log_environment

    def remote_accepted(self, connection_id):
        if self._running:
            connection_tunnel_endpoint = self.create_and_add_tunnelendpoint(self.checkpoint_handler, connection_id)
            closed_callback = lambda: self.async_connection_with_remote_closed(connection_id)
            remote_dispatch_remote_closed_callback = lambda: self.remote_dispatch_remote_closed(connection_id)
            self._connections[connection_id] = AsyncConnection(self.async_service, self._log_environment, connection_tunnel_endpoint,
                                                               closed_callback, self._enginefactory, False, remote_dispatch_remote_closed_callback)
            self.remote_recieve_connect_ok(connection_id)
        else:
            self.remote_recieve_connect_error(connection_id)

    # callback from connection itself
    def async_connection_with_remote_closed(self, connection_id):
        self.remove_tunnelendpoint_tunnel(connection_id)
        self.remote_send_closed(connection_id)
        try:
            del self._connections[connection_id]
            self.checkpoint_handler.Checkpoint("AsyncConnectorPortForwardServerWithConnectionEngine::async_connection_with_remote_closed", components.traffic.module_id, checkpoint.DEBUG)
        except KeyError:
            self.checkpoint_handler.Checkpoint("AsyncConnectorPortForwardServerWithConnectionEngine::async_connection_with_remote_closed", components.traffic.module_id, checkpoint.DEBUG, msg='KeyError')

    # callback from connection
    def remote_dispatch_remote_closed(self, connection_id):
        # Ignored, the closed signal is send by async_connection_with_remote_closed
        pass

    def close(self, force=False, close_and_forget=False):
        self.checkpoint_handler.Checkpoint("AsyncConnector::close", components.traffic.module_id, checkpoint.DEBUG)
        self._running = False
        for connection_id in self._connections.keys():
            self._connections[connection_id].close_all()
        tunnel_endpoint_tcp.TCPPortforwardServerWithConnectionEngine.close(self, close_and_forget=True)

    def is_done(self):
        return not self._running and len(self._connections) == 0 and AsyncConnectorPortForwardServerWithConnectionEngine.is_done(self)

    def remote_closed(self, connection_id):
        if self._connections.has_key(connection_id):
            self._connections[connection_id].remote_closed()

    def remote_eof(self, connection_id):
        if self._connections.has_key(connection_id):
            self._connections[connection_id].remote_eof()


class AsyncConnectionBase(tunnel_endpoint_base.TunnelendpointBaseWithMessageDispatch):
    """
    Represent a remote listen socket.

    Two types of remotes are possible toggled by remote_using_message_wrapping.
      True  : communication is using message dispatch (remote is python)
      False : communication is direct, no message dispatch wrapping (remote is C++ portforward)

    """
    def __init__(self, async_service, checkpoint_handler, tunnel_endpoint, remote_using_message_wrapping=True, remote_dispatch_remote_closed_callback=None):
        tunnel_endpoint_base.TunnelendpointBaseWithMessageDispatch.__init__(self, async_service, checkpoint_handler, tunnel_endpoint)
        self.remote_using_message_wrapping = remote_using_message_wrapping
        self.remote_dispatch_remote_closed_callback = remote_dispatch_remote_closed_callback

    def remote_dispatch_remote_read(self, data_chunk):
        if self.remote_using_message_wrapping:
            buffer_size = self.tunnelendpoint_remote('remote_read', data_chunk=data_chunk)
        else:
            buffer_size = tunnel_endpoint_base.TunnelendpointBase.tunnelendpoint_send(self, data_chunk)
        return buffer_size

    def remote_dispatch_remote_closed(self):
        if self.remote_using_message_wrapping:
            self.tunnelendpoint_remote('remote_closed')
        else:
            if self.remote_dispatch_remote_closed_callback:
                self.remote_dispatch_remote_closed_callback()

    def tunnelendpoint_recieve(self, data_chunk):
        if self.remote_using_message_wrapping:
            tunnel_endpoint_base.TunnelendpointBaseWithMessageDispatch.tunnelendpoint_recieve(self, data_chunk)
        else:
            self.remote_read(data_chunk)

    def remote_read(self, data_chunk):
        """
        Should be overwritten, data_chunk received from remote
        """
        raise NotImplementedError

    def remote_closed(self):
        """
        Should be overwritten, remote indicate that it has closed
        """
        raise NotImplementedError


class AsyncConnection(AsyncConnectionBase):
    """
    Represent a remote listen socket.
    """
    def __init__(self, async_service, log_environment, tunnel_endpoint, closed_callback, enginefactory, remote_using_message_wrapping=True, remote_dispatch_remote_closed_callback=None):
        AsyncConnectionBase.__init__(self, async_service, log_environment.checkpoint_handler, tunnel_endpoint, remote_using_message_wrapping, remote_dispatch_remote_closed_callback)
        self._closed_callback = closed_callback # call when listen socket dead - because can't listen or closed because remote told so
        self._remote_is_ready = False
        self._log_environment = log_environment

        self._remote_buffer = '' # data from remote (client) not yet sent to engine
        self.connector = None
        self.engine = enginefactory(self.engine_connect_tcp, self.engine_to_remote, self.close_all)
        self._close_all_called = False

    def tunnelendpoint_connected(self):
        """
        Callback from TunnelendpointBaseWithMessageDispatch when connection between the two tunnel_endpoints has been established.
        """
        self.checkpoint_handler.Checkpoint("AsyncConnector::tunnelendpoint_connected", components.traffic.module_id, checkpoint.DEBUG)
        self._remote_is_ready = True

    # callback from engine
    def engine_connect_tcp(self, addr, cb_tcp_connected, cb_tcp_error, enable_ssl=False, disable_certificate_verification=False):
        """Called back from engine when it wants a tcp connection.
        Should be called before any data_to_server, may be called immediately from engine constructor."""
        connect_ip, connect_port = addr
        self.checkpoint_handler.Checkpoint("AsyncConnector::engine_connect_tcp", components.traffic.module_id, checkpoint.DEBUG,
                                           addr=str(addr), enable_ssl=str(enable_ssl), disable_certificate_verification=str(disable_certificate_verification))
        self.connector = AsyncConnectionTcp(self._log_environment, self.async_service, self.get_mutex(),
                                            cb_tcp_connected, cb_tcp_error, connect_ip, connect_port, enable_ssl=enable_ssl, disable_certificate_verification=disable_certificate_verification)

    # callback from engine
    def engine_to_remote(self, data_to_client):
        if not data_to_client:
            self.checkpoint_handler.Checkpoint("AsyncConnector::engine_to_remote", components.traffic.module_id, checkpoint.DEBUG,
                to_client='SHUTDOWN')
        return self.remote_dispatch_remote_read(data_to_client)

    def remote_read(self, data_chunk):
        if not data_chunk and self._remote_buffer:
            self.checkpoint_handler.Checkpoint("AsyncConnector::remote_read", components.traffic.module_id, checkpoint.DEBUG,
                msg="EOF from client with pending data", pending=len(self._remote_buffer))
            self._remote_buffer = ''
        else:
            self._remote_buffer += data_chunk
        self._remote_buffer = self.engine.from_client(self._remote_buffer)
        if self._remote_buffer is None:
            self.checkpoint_handler.Checkpoint("AsyncConnector::remote_read", components.traffic.module_id, checkpoint.DEBUG,
                msg="close")
            self.remote_dispatch_remote_read('')
            self.close_all()

    def remote_closed(self):
        self.checkpoint_handler.Checkpoint("AsyncConnector::remote_closed", components.traffic.module_id, checkpoint.DEBUG)
        self._remote_is_ready = False
        print self.engine.__class__
        self.engine.close()

    def remote_eof(self):
        self.checkpoint_handler.Checkpoint("AsyncConnector::remote_eof", components.traffic.module_id, checkpoint.DEBUG)
        self.remote_read('')

    def close_all(self):
        
        # Ensure that close_all functionality only applyed once
        if self._close_all_called:
            return
        self._close_all_called = True

        self.checkpoint_handler.Checkpoint("AsyncConnector::close_all", components.traffic.module_id, checkpoint.DEBUG)
        self.engine.close()
        if self._remote_is_ready:
            self.remote_dispatch_remote_closed()
            self._remote_is_ready = False
        self._closed_callback()
        if self.connector:
            self.connector.async_socket_tcp_closed()
            self.connector = None

    def tcp_closed(self, tag):
        self.checkpoint_handler.Checkpoint("AsyncConnector::tcp_closed", components.traffic.module_id, checkpoint.DEBUG,
                                           tag=str(tag))


class AsyncConnectionTcp(async_socket.AsyncSocketTCPConnectorCallback):

    def __init__(self, log_environment, async_service, mutex, cb_tcp_connected, cb_tcp_error, connect_ip, connect_port, enable_ssl=False, disable_certificate_verification=False):
        async_socket.AsyncSocketTCPConnectorCallback.__init__(self)
        self.checkpoint_handler = log_environment.checkpoint_handler
        self.access_logger = log_environment.access_logger.get_logger(module_name="server_launc_async", log_id="tcp::log0")
        self.access_log_server_session = log_environment.access_logger.access_log_server_session
        self.async_service = async_service
        self._mutex = mutex
        self._cb_tcp_connected = cb_tcp_connected
        self._cb_tcp_error = cb_tcp_error
        self._failover = failover.Failover(connect_ip, connect_port)
        self._connector = None
        self._connect_target = None
        self._tcp = None
        self._pending_data_to_tcp = '' # queue up until tcp is ready
        self._enable_ssl = enable_ssl
        self._disable_certificate_verification = disable_certificate_verification
        self._failover_connect()

    def _failover_connect(self):
        self._connect_target = self._failover.get()
        self.checkpoint_handler.Checkpoint("AsyncConnectionTcp::failover_connect", components.traffic.module_id, checkpoint.DEBUG,
                                           host=str(self._connect_target[0]), port=str(self._connect_target[1]))
        self._connector = async_socket.AsyncSocketTCPConnector(self.checkpoint_handler, self.async_service,
                                                               self._connect_target[0], self._connect_target[1], self)
        if self._enable_ssl:
            self._connector.enable_ssl(self._disable_certificate_verification)
            
        self._connector.set_mutex(self._mutex)
        self._connector.set_connect_timeout_sec(self._failover.timeout)
        self._connector.connect()

    def async_socket_tcp_connector_connected(self, _callback_param, raw_connection):
        self.checkpoint_handler.Checkpoint("AsyncConnectionTcp::async_socket_tcp_connector_connected", components.traffic.module_id, checkpoint.DEBUG)
        self._tcp = AsyncConnectedTcp(self.checkpoint_handler, self.async_service, raw_connection, self._connector.get_mutex(), self._cb_tcp_connected)
        self.access_logger(None, log_type="tcp_connected", local_ip=self._tcp.get_local(), remote_ip=self._tcp.get_remote())
        if self._pending_data_to_tcp:
            self._tcp.write(self._pending_data_to_tcp)
            self._pending_data_to_tcp = None
        self._tcp.read_start()

    def async_socket_tcp_connect_failed(self, _callback_param, message):
        self.checkpoint_handler.Checkpoint("AsyncConnectionTcp::async_socket_tcp_connect_failed", components.traffic.module_id, checkpoint.ERROR)
        self.access_logger(None, log_type="tcp_connect_failed", remote_ip=self._connect_target)
        self.access_log_server_session.report_access_log_gateway_error('traffic', 'traffic:0002', 'Unable to connect', message)

        if self._connector is not None:
            self._connector.close_no_callback()
            self._connector = None
            
        if self._failover.failure(self._connect_target):
            self._failover_connect()
        else:            
            self._cb_tcp_error('tcp connect failed')

    def async_socket_tcp_closed(self):
        self.access_logger = None
        self.access_log_server_session = None
        self.async_service = None
        self._mutex = None
        self._cb_tcp_connected = None
        self._cb_tcp_error = None
        self._failover = None
        if self._connector is not None:
            self._connector.close_no_callback()
        self._connector = None
        self._connect_target = None

    def write(self, chunk):
        if self._tcp:
            self._tcp.write(chunk)
            return
        # not connected yet
        self._pending_data_to_tcp += chunk


class AsyncConnectedTcp(async_socket.AsyncSocketTCPConnection):
    """
    Wrap async tcp connection and make callback to handler
    """
    def __init__(self, checkpoint_handler, async_service, raw_connection, mutex, cb_tcp_connected):
        async_socket.AsyncSocketTCPConnection.__init__(self, checkpoint_handler, raw_connection)
        self.set_mutex(mutex)
        self.async_service = async_service
        self._local_buffer = '' # data from local tcp (server) not yet processed by engine
        self._tcp_read, self._tcp_closed = cb_tcp_connected(raw_connection.get_ip_local(), raw_connection.get_ip_remote(), self.write, self.close)

    def read(self, data_chunk):
        self.checkpoint_handler.Checkpoint("AsyncConnectedTcp.read", components.traffic.module_id, checkpoint.DEBUG);
        if not data_chunk and self._local_buffer:
            self.checkpoint_handler.Checkpoint("AsyncConnector::from_tcp", components.traffic.module_id, checkpoint.DEBUG,
                msg="EOF from server with pending data", pending=len(self._local_buffer))
            self._local_buffer = '' # will probably cause engine to call back on engine_close_tcp
        else:
            self._local_buffer += data_chunk
        if not self._local_buffer:
            self.checkpoint_handler.Checkpoint("AsyncConnector::from_tcp.eof", components.traffic.module_id, checkpoint.DEBUG)
        self._local_buffer = self._tcp_read(self._local_buffer)
        if self._local_buffer is None:
            self.checkpoint_handler.Checkpoint("AsyncConnection::from_tcp", components.traffic.module_id, checkpoint.DEBUG, msg="engine responded close")
            self.close()
        else:
            self.read_start() # TODO: check will_listen?

    def write_buffer_empty(self):
        pass

    def closed_eof(self):
        self.read('')

    def closed(self):
        self._tcp_closed()
        self._tcp_read = None
        self._tcp_closed = None
