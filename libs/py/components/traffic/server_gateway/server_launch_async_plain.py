from lib import checkpoint
import components.traffic

def mk_enginefactory(log_environment, launch_element, expand_variables, server_host, server_port, servers):

    checkpoint_handler = log_environment.checkpoint_handler

    class PlainEngine(object):
        """Like a plain port forward"""

        def __init__(self, cb_connect, cb_to_client, cb_close_client):
            self._cb_to_client = cb_to_client
            self._cb_close_client = cb_close_client
            self._pending_connect = []
            self._cb_to_server = lambda chunk: self._pending_connect.append(chunk)
            self._cb_close_server = None
            cb_connect((server_host, server_port), cb_tcp_connected=self._connected, cb_tcp_error=self._connect_error)

        # call back from cb_connect
        def _connected(self, local, remote, write, close):
            checkpoint_handler.Checkpoint("async plain _connected", components.traffic.module_id, checkpoint.DEBUG,
                                          local='%s:%s' % (local[0], local[1]), remote='%s:%s' % (remote[0], remote[1]))
            self._cb_to_server = write
            self._cb_close_server = close
            for chunk in self._pending_connect:
                self._cb_to_server(chunk)
            self._pending_connect = None
            return (self._from_server, self._server_error)

        # call back from cb_connect
        def _connect_error(self, msg=None):
            checkpoint_handler.Checkpoint("async plain _connect_error", components.traffic.module_id, checkpoint.DEBUG, msg=msg)
            self.close()

        # given as call back when _connected
        def _from_server(self, data):
            """
            Handles data from server
            Returns excess data - or none for close
            """
            self._cb_to_client(data)
            return ''

        # given as call back when _connected
        def _server_error(self, msg=None):
            checkpoint_handler.Checkpoint("async plain _server_error", components.traffic.module_id, checkpoint.DEBUG, msg=msg)
            self.close()

        # called from owner of engine / client connection
        def from_client(self, data):
            """
            Handles data from client
            Not called before _connected.
            """
            self._cb_to_server(data)
            return ''

        # internal _and_ called from engine owner
        def close(self):
            cb_close_server, self._cb_close_server = self._cb_close_server, None
            if cb_close_server:
                cb_close_server()
            cb_close_client, self._cb_close_client = self._cb_close_client, None
            if cb_close_client:
                cb_close_client()

    return PlainEngine
