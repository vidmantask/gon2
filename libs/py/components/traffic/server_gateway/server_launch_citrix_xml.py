from lib import checkpoint

from lib import variable_expansion

import components.traffic
from components.communication import tunnel_endpoint_base

import server_portforward
from components.traffic.server_common import database_schema
import citrix_xml_service
import failover


class FailoverService(object):
    '''Wrapper of citrix_xml_service.XenAppXMLService that transparently adds failover'''

    def __init__(self, launch_id, expand_variables):
        launch_element = database_schema.LaunchElement.lookup(launch_id)
        self._failover = failover.Failover(
            launch_element.portforwards[0].server_host or '',
            launch_element.portforwards[0].server_port or 0,
            )
        self._sso_login = expand_variables(launch_element.sso_login)
        self._sso_password = expand_variables(launch_element.sso_password)
        self._sso_domain = expand_variables(launch_element.sso_domain)
        self._xml_service = None
        self._xml_service_addr = None
        self._gen_xml_service()

    @property
    def apps(self):
        return self._xml_service.apps

    def launch_info(self, appname):
        while True:
            try:
                return self._xml_service.launch_info(appname)
            except citrix_xml_service.XmlServiceException:
                if not self._failover.failure(self._xml_service_addr):
                    raise
                self._gen_xml_service()

    def _gen_xml_service(self):
        '''Set self._xml_service to a working service ... or fail'''
        while True:
            self._xml_service_addr = self._failover.get()
            try:
                self._xml_service = citrix_xml_service.XenAppXMLService(
                        self._xml_service_addr[0], self._xml_service_addr[1],
                        self._sso_login, self._sso_password, self._sso_domain,
                        timeout = self._failover.timeout,
                        )
                return
            except citrix_xml_service.XmlServiceException:
                if not self._failover.failure(self._xml_service_addr):
                    raise

PORTFORWARD_CHILD_ID = 1

def icb_closed():
    """
    Notify that the portforward has been closed
    """
    pass


class LaunchXmlIca(tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch):
    def __init__(self, async_service, checkpoint_handler, new_tunnelendpoint, launch_id, own_id,
                 expand_variables, cb_closed, access_log_server_session, cb_report_error,
                 xml_service, appname):
        tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch.__init__(self, async_service, checkpoint_handler, new_tunnelendpoint)
        self._access_log_server_session = access_log_server_session
        self.launch_id = launch_id
        self._own_id = own_id
        self.expand_variables = expand_variables
        self.cb_closed = cb_closed
        self.cb_report_error = cb_report_error
        self._close_from_client = False
        self.xml_service = xml_service
        self.appname = appname

        self._portforward = None

    def tunnelendpoint_connected(self):
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::_launch", components.traffic.module_id, checkpoint.DEBUG, launch_id=self.launch_id):
            launch_element = database_schema.LaunchElement.lookup(self.launch_id)
            if not launch_element:
                self.checkpoint_handler.Checkpoint("LaunchPortForward::_launch", components.traffic.module_id, checkpoint.ERROR,
                    message="Launch id not found")
                return

            launch_command = self.expand_variables(launch_element.citrix_command) # Not .launch_command !!!
            close_command = self.expand_variables(launch_element.close_command)
            working_directory = self.expand_variables(launch_element.working_directory)
            param_file_name = self.expand_variables(launch_element.param_file_name)
            param_file_lifetime = launch_element.param_file_lifetime

            try:
                launch_info = self.xml_service.launch_info(self.appname)
            except citrix_xml_service.XmlServiceException, e:
                self.checkpoint_handler.Checkpoint("LaunchPortForward::_launch", components.traffic.module_id, checkpoint.ERROR,
                    message="Error getting launch info: %s" % e)
                return
            def getter(first, second):
                if first == 'ica':
                    return launch_info.get(second)
            param_file_template = variable_expansion.expand(launch_element.param_file_template, getter)

            close_with_process = launch_element.close_with_process
            kill_on_close = launch_element.kill_on_close
            tcp_options = [x.encode('ascii', 'ignore').strip().upper() for x in launch_element.citrix_command.split(',')]

            if ':' in launch_info['address']:
                server_host, port_str = str(launch_info['address']).split(':', 1)
                server_port = int(port_str)
            else:
                server_host = str(launch_info['address'])
                server_port = 1494

            self._access_log_server_session.report_access_log_menu_action_info(PORTFORWARD_CHILD_ID, "Server: %s:%d" % (server_host, server_port))
            
            self._portforward = server_portforward.PortForwardServer(self.async_service,
                                                                  self.checkpoint_handler,
                                                                  server_host,
                                                                  server_port,
                                                                  self._portforward_ready,
                                                                  self._portforward_closed,
                                                                  self.cb_report_error
                                                                  )
            self.add_tunnelendpoint(PORTFORWARD_CHILD_ID, self._portforward.as_tunnelendpoint())

            portforward_spec = launch_element.portforwards[0]
            self._access_log_server_session.report_access_log_traffic_start(
                    (self._own_id, PORTFORWARD_CHILD_ID),
                    portforward_spec.client_host, portforward_spec.client_port,
                    server_host, self._portforward.server_port)

            self.tunnelendpoint_remote('remote_call_launch_with_tcp_options',
                                       child_id=PORTFORWARD_CHILD_ID,
                                       launch_command=launch_command,
                                       close_command=close_command,
                                       working_directory=working_directory,
                                       param_file_name=param_file_name,
                                       param_file_lifetime=param_file_lifetime,
                                       param_file_template=param_file_template,
                                       close_with_process=close_with_process,
                                       kill_on_close=kill_on_close,
                                       client_host = portforward_spec.client_host,
                                       client_port = portforward_spec.client_port,
                                       lock_to_process_pid = portforward_spec.lock_to_process_pid,
                                       sub_processes = portforward_spec.sub_processes,
                                       lock_to_process_name = portforward_spec.lock_to_process_name,
                                       tcp_options = tcp_options
                                       )

    def _portforward_ready(self):
        self.checkpoint_handler.Checkpoint("LaunchPortForward::_portforward_ready", components.traffic.module_id, checkpoint.DEBUG)

    def _portforward_closed(self):
        """
        Portforward has closed down
        """
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::_cb_closed", components.traffic.module_id, checkpoint.DEBUG):
            # Last portforward closed
            if not self._close_from_client:
                self.tunnelendpoint_remote('remote_call_closed')
            self._reset()
            self.cb_closed()

    def remote_call_launch_response(self, is_ok):
        """
        Called from client when the launch is done
        """
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::remote_call_launch_response", components.traffic.module_id, checkpoint.DEBUG,
                                                      is_ok="%s" % is_ok):
            pass

    def close(self, force = False, close_and_forget = False):
        """
        Close down the launched portforward
        """
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::close", components.traffic.module_id, checkpoint.DEBUG):
            if self._portforward:
                self._portforward.close(force, close_and_forget)

            if close_and_forget:
                self._reset()

    def remote_call_closed(self):
        """
        Client is telling that is has closed down
        """
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::remote_call_closed", components.traffic.module_id, checkpoint.DEBUG):
            self._close_from_client = True
            if self._portforward:
                self._portforward.close(True, True)
            self._reset()
            self.cb_closed()

    def _reset(self):
        self._access_log_server_session.report_access_log_traffic_close((self._own_id, PORTFORWARD_CHILD_ID))
        self.remove_tunnelendpoint_tunnel(PORTFORWARD_CHILD_ID)
