"""
This file constains the functionality related to a traffic session on the server
"""

from lib import checkpoint
import lib.variable_expansion

import traceback

from components.communication import tunnel_endpoint_base
from components.traffic.server_gateway import server_launch_portforward
from components.traffic.server_gateway import server_launch_citrix
from components.traffic.server_gateway import server_launch_internal
from components.traffic.server_gateway import server_wake_on_lan
from components.traffic.server_gateway import server_launch_citrix_xml
from components.traffic.server_gateway import server_launch_async
from components.plugin.server_gateway import plugin_socket_traffic
import components.management_message.server_gateway.session_send
import components.dialog.server_common.dialog_api

from components.traffic.server_common import database_schema
import components.traffic.server_gateway.server_launch_internal
from components.database.server_common.database_api import QuerySession, in_

class LaunchMenuItem(object):

    def __init__(self, launch_id, title, tag_plugin_socket, launch_tags, launch_tag_generators, icon_id = None):
        self.launch_id = launch_id
        self.title = title
        self.tags = set()
        self.icon_id = icon_id
        self._launch_tags = launch_tags
        self._tag_plugin_socket = tag_plugin_socket
        self.tooltip = ""
        self.deactivated = False

        self._tag_generator_set = launch_tag_generators


    def generate_tags(self):
        self.tags = set(self._launch_tags)
        self.tags.update(self._tag_plugin_socket.generate_tags(self._tag_generator_set))




class ITrafficSessionCallback(object):
    """
    tbd
    """
    def traffic_update_ids(self, action_ids, deactivated_actions):
        """
        Notification from the auth component that traffic session should give access to the given list of action_id's.
        """
        raise NotImplementedError

    def traffic_launch(self, launch_id):
        """
        Called from dialog component when launch item is selected for launch
        """
        raise NotImplementedError


class TrafficSession(ITrafficSessionCallback, tunnel_endpoint_base.TunnelendpointSession, components.traffic.server_gateway.server_launch_internal.LaunchSession):
    """
    tbd
    """
    def __init__(self, environment, async_service, checkpoint_handler, tunnelendpoint, plugins, user_session, launch_sessions, tag_plugin_socket, access_log_server_session, service_management_message_send, server_config):
        tunnel_endpoint_base.TunnelendpointSession.__init__(self, async_service, checkpoint_handler, tunnelendpoint)
        self.plugins = plugins
        self.plugin_socket_traffic = plugin_socket_traffic.PluginSocket(environment.plugin_manager, self.plugins)
        self._next_child_id = 1
        self._children = {}
        self.dialog_callback = None
        self.action_ids = []
        self.deactivated_actions = dict()
        self.launch_ids = dict()

        self.launch_element_dict = None
        self.tag_generator_dict = None
        self.tag_dict = None

        self._user_session = user_session
        self.launch_sessions = launch_sessions
        self._tag_plugin_socket = tag_plugin_socket
        self._tag_plugin_socket.set_cb(self)
        self._access_log_server_session = access_log_server_session
        self._action_id_cache = {}
        self.server_config = server_config

        self._management_message_sender = components.management_message.server_gateway.session_send.ManagementMessageSessionSender(components.traffic.module_id, service_management_message_send)


    def set_dialog_callback(self, dialog_callback):
        """ Called from server main"""
        self.dialog_callback = dialog_callback

    def set_auth_callback(self, auth_callback):
        """ Called from server main"""
        self.auth_callback = auth_callback

    def _get_launch_elements(self, action_id):
        element_list = self.launch_element_dict.get(action_id, None)
        if element_list is None:
            with QuerySession() as db_session:
                element_list = db_session.select(database_schema.LaunchElement, database_schema.LaunchElement.action_id==action_id)
                self.launch_element_dict[action_id] = element_list
                launch_ids = [e.id for e in element_list]
                if launch_ids:
                    tag_generators = db_session.select(database_schema.LaunchTagGenerator,
                                                       in_(database_schema.LaunchTagGenerator.launch_id, launch_ids))
                    self._update_tag_generator_dict(tag_generators)

        return element_list


    def _update_tag_generator_dict(self, tag_generators):
        for tag_generator in tag_generators:
            launch_id = tag_generator.launch_id
            generator_list = self.tag_generator_dict.get(launch_id, set())
            generator_list.add(tag_generator.tag_generator)
            self.tag_generator_dict[launch_id] = generator_list


    def initialize_data(self):
        self._action_id_cache = {}
        self.launch_element_dict = dict()
        with QuerySession() as db_session:
            launch_elements = db_session.select(database_schema.LaunchElement)

            for e in launch_elements:
                element_list = self.launch_element_dict.get(e.action_id, [])
                element_list.append(e)
                self.launch_element_dict[e.action_id] = element_list

            tag_generators = db_session.select(database_schema.LaunchTagGenerator)
            self.tag_generator_dict = dict()
            self._update_tag_generator_dict(tag_generators)

            self.tag_dict = components.dialog.server_common.dialog_api.get_all_tags_by_launch_id(db_session)

    def session_start_and_connected(self):
        """
        This is called when tunnelendpoint_connected and session_start both has been called
        """
        with self.checkpoint_handler.CheckpointScope("session_start_and_connected", components.traffic.module_id, checkpoint.DEBUG):
            self.initialize_data()

    def session_close(self):
        """
        Hard close of session. Called from main session when communication is terminated
        """
        with self.checkpoint_handler.CheckpointScope("session_close", components.traffic.module_id, checkpoint.DEBUG, children_keys="%s" % self._children.keys()):
            for child_id in self._children.keys():
                child = self._children.pop(child_id)
                child.close(force=True, close_and_forget=True)
                self.remove_tunnelendpoint_tunnel(child_id)
            self.auth_callback = None
            self._user_session = None
            self.launch_sessions = None
            self.dialog_callback = None
            self.plugin_socket_traffic = None
            if self._tag_plugin_socket is not None:
                self._tag_plugin_socket = None
            self.reset_tunnelendpoint()

    def expand_variables(self, template_string):
        """
        Expand variables of form "%(foo.bar)" in template_string using plugin foo.get_attribute(bar).
        """
        return lib.variable_expansion.expand(template_string, self.get_attribute)

    def get_attribute(self, plugin_name, attribute_name):
        try:
            if plugin_name == "user":
                return self._user_session.get_attribute(attribute_name)
            else:
                return self.plugin_socket_traffic.get_attribute(plugin_name, attribute_name)
        except KeyError:
            return None

    def create_folder(self, dynamic_menu_tags, folder_name, caption, parents):
        existing = dynamic_menu_tags.get(folder_name)
        if not existing:
            menu_dict = dict()
            menu_dict["name"] = folder_name
            menu_dict["caption"] = caption
            menu_dict["parents"] = parents
            dynamic_menu_tags[folder_name] = menu_dict


    def _add_restrictions(self, action_id, menu_item, deactivated_actions):
        restrictions = deactivated_actions.get(action_id)
        if restrictions:
            menu_item.deactivated = True
            menu_item.tooltip = "\n".join(restrictions)
        else:
            if menu_item.deactivated:
                menu_item.deactivated = False
                menu_item.tooltip = ""

    def traffic_update_ids(self, action_ids, deactivated_actions):
        """
        Implementation if ITrafficSessionCallback, called from auth_session

        Lookup the action_id's and find the corresponding launch_ids, and tell the dialog component about the change.
        """
        with self.checkpoint_handler.CheckpointScope("traffic_update_ids", components.traffic.module_id, checkpoint.DEBUG) as cps:
            self.action_ids = action_ids
            self.deactivated_actions = deactivated_actions
            self.launch_ids = dict()
            menu_launch_elements = []
            dynamic_menu_tags = dict()
            for action_id in self.action_ids:
                if not action_id in self._action_id_cache:
                    action_id_cache_entry = []
                    launch_elements = self._get_launch_elements(action_id)
                    for launch_element in launch_elements:
                        launch_tags = self.tag_dict.get(str(launch_element.id), set())
                        launch_tag_generators = self.tag_generator_dict.get(launch_element.id, set())
                        menu_title = self.expand_variables(launch_element.menu_title)

                        self.checkpoint_handler.Checkpoint('traffic_update_ids', components.traffic.module_id, checkpoint.DEBUG, menu_title=menu_title, action_name=launch_element.name)

                        if launch_element.launch_type == 4 and not launch_element.command:

                            with self.checkpoint_handler.CheckpointScope('traffic_update_ids.citrix_xml', components.traffic.module_id, checkpoint.DEBUG, action_name=launch_element.name) as cps_citrix:
                                # TODO: async?
                                try:
                                    # Note: launch_element do not have portforwards expanded, so we only use the id and let FailoverService look it up
                                    xml_service = server_launch_citrix_xml.FailoverService(launch_element.id, self.expand_variables)
                                except Exception, e:
                                    self.checkpoint_handler.Checkpoint('traffic_update_ids', components.traffic.module_id, checkpoint.ERROR,
                                        message="Error connecting to XML service: %r" % e, action_id=action_id)
                                    menu_item = LaunchMenuItem('%s' % (launch_element.id,),
                                                                '%s %s' % (menu_title, str(e)),
                                                                self._tag_plugin_socket, launch_tags, launch_tag_generators)
                                    self._add_restrictions(action_id, menu_item, deactivated_actions)
                                    action_id_cache_entry.append(menu_item)
                                    xml_service = None

                                if xml_service:
                                    cps_citrix.add_complete_attr(xml_service_started=True)

                                    for app in xml_service.apps:
                                            title_prefix = menu_title + ' '
                                            folders = app.Folder.split("\\") if app.Folder else []
                                            # Create dynamic Citrix menu
                                            if self.dialog_callback.create_citrix_menu() and folders:
                                                menu_tags, other_tags = self.dialog_callback.split_menu_tags_from_other(launch_tags)
                                                folder_name = ':%s' % launch_element.id
                                                parents = menu_tags
                                                for f in folders:
                                                    folder_name += ':' + f
                                                    caption = title_prefix + f
                                                    self.create_folder(dynamic_menu_tags, folder_name, caption, parents)
                                                    parents = [folder_name]
                                                    title_prefix = ''

                                                other_tags.extend(parents)
                                                menu_item_tags = other_tags
                                            else:
                                                menu_item_tags = launch_tags
                                            title = app.FName if hasattr(app, "FName") and app.FName else app.InName
                                            menu_item = LaunchMenuItem('%s %s' % (launch_element.id, app.InName),
                                                                        title_prefix + title,
                                                                        self._tag_plugin_socket, menu_item_tags, launch_tag_generators)
                                            action_id_cache_entry.append(menu_item)
                                            self._add_restrictions(action_id, menu_item, deactivated_actions)

                                            self.checkpoint_handler.Checkpoint('traffic_update_ids.citrix_menu_item', components.traffic.module_id, checkpoint.DEBUG, menu_title=menu_item.title, action_name=launch_element.name)

                        else:
                                menu_item = LaunchMenuItem('%s' % (launch_element.id,),
                                                           menu_title,
                                                           self._tag_plugin_socket, launch_tags, launch_tag_generators, launch_element.image_id)
                                action_id_cache_entry.append(menu_item)
                                self._add_restrictions(action_id, menu_item, deactivated_actions)

                    self._action_id_cache[action_id] = action_id_cache_entry
                else:
                    action_id_cache_entry = self._action_id_cache.get(action_id)
                    for menu_item in action_id_cache_entry:
                        self._add_restrictions(action_id, menu_item, deactivated_actions)


                for launch_menu_item in self._action_id_cache[action_id]:
                    launch_menu_item.generate_tags()
                    menu_launch_elements.append(launch_menu_item)

            cps.add_complete_attr(launch_ids=repr([launch_menu_item.launch_id for launch_menu_item in menu_launch_elements]))
            self.launch_ids = dict([(launch_menu_item.launch_id, launch_menu_item) for launch_menu_item in menu_launch_elements])
            assert self.dialog_callback
            self.dialog_callback.add_dynamic_menu_tags(dynamic_menu_tags.values())
            self.dialog_callback.dialog_update_ids(menu_launch_elements)


    def cb_tag_changed(self):
        """
        Callback from tag-plugin-socket telling that one of the tags has changed
        """
        self.traffic_update_ids(self.action_ids, self.deactivated_actions)

    def recalculate_menu(self):
        self.auth_callback.recalculate()

    def traffic_launch(self, launch_id):
        """
        Implementation if ITrafficSessionCallback, called from dialog_session
        """
        try:
            launch_element = self.launch_ids.get(launch_id)
            if not launch_element:
                self.checkpoint_handler.Checkpoint('traffic_launch.error', components.traffic.module_id, checkpoint.WARNING, launch_id=launch_id, message='Unauthorized launch')
                self.tunnelendpoint_remote('show_message',
                        title='Launch failed', text='Not authorized to launch id %r', textparams=[launch_id])
            else:
                if launch_element.deactivated:
                    self.tunnelendpoint_remote('show_message',
                            title='Launch failed', text="Menu Action '%s' is deactivated: %s", textparams=(launch_element.title, launch_element.tooltip))
                    return

                if ' ' in launch_id:
                    launch_id, sub_id = launch_id.split(' ', 1)
                else:
                    sub_id = None
                launch_element = database_schema.LaunchElement.lookup(launch_id)

                # Empty launch_element can occur if the launch has been removed because we at the moment do not update the menu if authorization changes
                if launch_element is None:
                    self.checkpoint_handler.Checkpoint('traffic_launch.error', components.traffic.module_id, checkpoint.ERROR, message="invalid launch_element", launch_id=launch_id)
                    self.tunnelendpoint_remote('show_message',
                            title='Launch failed', text='Launch id %r not found', textparams=[launch_id])
                    return

                with self.checkpoint_handler.CheckpointScope('traffic_launch', components.traffic.module_id, checkpoint.DEBUG, message='disabled'):
# TWA: Gave unicode problem in 5.8
#                                                             **dict((k, v) for (k, v) in launch_element.__dict__.items() if not k.startswith('_'))):

                    if launch_element.launch_type == 3:
                        # no tunnel - WOL has no self._children and no client-side
                        launched = server_wake_on_lan.LaunchWOL(self.checkpoint_handler,
                                                                launch_id,
                                                                self.expand_variables,
                                                                )
                        return

                    child_id = self._next_child_id
                    self._next_child_id += 1

                    self._access_log_server_session.report_access_log_menu_action_launched(child_id, database_schema.launch_element2dict(launch_element))

                    if lib.version.Version.compare(self.get_version_remote(), lib.version.Version.create(5, 5, 0)) == -1:
                        self.tunnelendpoint_remote('traffic_launch_child', child_id=child_id, launch_type=launch_element.launch_type)
                    else:
                        self.tunnelendpoint_remote('traffic_launch_child_with_launch_id', child_id=child_id, launch_type=launch_element.launch_type, launch_id=launch_id)

                    new_tunnelendpoint = self.create_and_add_tunnelendpoint_tunnel(self.checkpoint_handler, child_id)

                    if launch_element.launch_type == 0:
                        launched = server_launch_portforward.LaunchPortForward(self.async_service,
                                                                               self.checkpoint_handler,
                                                                               new_tunnelendpoint,
                                                                               launch_id,
                                                                               child_id,
                                                                               self.expand_variables,
                                                                               lambda child_id=child_id: self._cb_child_closed(child_id),
                                                                               self._access_log_server_session,
                                                                               self.report_error
                                                                               )
                    elif launch_element.launch_type == 1:
                        launched = server_launch_citrix.LaunchCitrix(self.async_service,
                                                                     self.checkpoint_handler,
                                                                     new_tunnelendpoint,
                                                                     launch_id,
                                                                     self.expand_variables,
                                                                     lambda child_id=child_id: self._cb_child_closed(child_id),
                                                                     self._access_log_server_session,
                                                                     self.report_error
                                                                     )
                    elif launch_element.launch_type == 2:
                        launched = server_launch_internal.LaunchInternal(self.async_service,
                                                                         self.checkpoint_handler,
                                                                         new_tunnelendpoint,
                                                                         launch_id,
                                                                         self,
                                                                         self.launch_sessions,
                                                                         lambda child_id=child_id: self._cb_child_closed(child_id),
                                                                         )
                    # launch_element.launch_type == 3 is handled above
                    elif launch_element.launch_type == 4:
                        try:
                            xml_service = server_launch_citrix_xml.FailoverService(launch_element.id, self.expand_variables)
                        except Exception, e:
                            self.checkpoint_handler.Checkpoint('traffic_launch.error', components.traffic.module_id, checkpoint.ERROR,
                                message="Error connecting to XML service: %r" % e, launch_id=launch_id)
                            self.report_error('server_session', 1, "Error connecting to XML service: %r" % e)
                            self.tunnelendpoint_remote('show_message',
                                    title='Launch failed', text='Error connecting to XML service: %s', textparams=[e.args and e.args[0]])
                            xml_service = None
                            self._access_log_server_session.report_access_log_menu_action_closed(child_id, error_message="Error connecting to XML service: %r" % e)
                            return

                        if xml_service:
                            launched = server_launch_citrix_xml.LaunchXmlIca(self.async_service,
                                                                           self.checkpoint_handler,
                                                                           new_tunnelendpoint,
                                                                           launch_id,
                                                                           child_id,
                                                                           self.expand_variables,
                                                                           lambda child_id=child_id: self._cb_child_closed(child_id),
                                                                           self._access_log_server_session,
                                                                           self.report_error,
                                                                           xml_service,
                                                                           sub_id or launch_element.command,
                                                                           )

                    elif launch_element.launch_type in [5, 6, 7, 8, 9, 10, 11]:
                        launched = server_launch_async.LaunchAsync(
                            self.async_service,
                            self.checkpoint_handler,
                            new_tunnelendpoint,
                            self._access_log_server_session,
                            self.expand_variables,
                            child_id,
                            launch_id,
                            lambda child_id=child_id: self._cb_child_closed(child_id),
                            #self.report_error,
                            self.server_config
                            )

                    else:
                        self.checkpoint_handler.Checkpoint('traffic_launch.error', components.traffic.module_id, checkpoint.ERROR, message="invalid launchtype", launch_type=launch_element.launch_type)
                        self.report_error('server_session', 1, "Invalid launch type %s in launch specification." % (launch_element.launch_type))
                        self.tunnelendpoint_remote('show_message',
                                title='Launch failed', text='Unknown launch type %r', textparams=[launch_element.launch_type])
                        return

                    self.checkpoint_handler.Checkpoint('traffic_launch.add_child', components.traffic.module_id, checkpoint.DEBUG, child_id=child_id)
                    self._children[child_id] = launched

                    self._management_message_sender.message_remote('sink_traffic_item_launched',
                                                                   launch_id=launch_id,
                                                                   launch_type = launch_element.launch_type,
                                                                   user_id = self._user_session.get_user_id())


        except Exception, e:
            self.checkpoint_handler.Checkpoint('traffic_launch.error', components.traffic.module_id, checkpoint.ERROR, message="%r %s" % (e, traceback.format_exc()))
            self.report_error('server_session', 1, "Error : %r" % e)
            self.tunnelendpoint_remote('show_message',
                    title='Launch failed', text='Error : %s', textparams=[e.args and e.args[0]])

    def remote_report_error(self, error_source, error_code, error_message):
        with self.checkpoint_handler.CheckpointScope("TrafficSession::remote_report_error", components.traffic.module_id, checkpoint.DEBUG, error_source=error_source, error_code=error_code, error_message=error_message):
            self._access_log_server_session.report_access_log_traffic_error(error_source, error_code, error_message)

    def report_error(self, error_source, error_code, error_message):
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::report_error", components.traffic.module_id, checkpoint.DEBUG, error_source=error_source, error_code=error_code, error_message=error_message):
            self._access_log_server_session.report_access_log_traffic_error(error_source, error_code, error_message)

    def _cb_child_closed(self, child_id):
        """
        A child is reporting that is has closed
        """
        with self.checkpoint_handler.CheckpointScope("TrafficSession::_cb_child_closed", components.traffic.module_id, checkpoint.DEBUG, child_id=child_id):
            if self._children.has_key(child_id):
                self._children.pop(child_id)
                self.remove_tunnelendpoint_tunnel(child_id)
            self._access_log_server_session.report_access_log_menu_action_closed(child_id)
