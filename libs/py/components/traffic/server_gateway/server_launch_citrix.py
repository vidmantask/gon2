"""
"A citrix launch", instantiated on launch and does whatever it designates of launch and traffic and stuff
MetaFrame Presentation Server -> Citrix Presentation Server (since 2005) -> Citrix XenApp
"""

import cookielib
import urllib
import urllib2
import threading

from lib import checkpoint
from components.communication import tunnel_endpoint_base
import server_launch_citrix_ica

from components.traffic.server_common import database_schema

auth_login_path = 'auth/login.aspx'
site_default_aspx = 'site/default.aspx'
site_launch_ica = 'site/launch.ica'
site_launcher_aspx = 'site/launcher.aspx'


class LaunchCitrix(tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch):

    def __init__(self, async_service, checkpoint_handler, new_tunnel, launch_id,
                 expand_variables, cb_closed, access_log_server_session, cb_error):
        tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch.__init__(self, async_service, checkpoint_handler, new_tunnel)
        self.expand_variables = expand_variables
        self.cb_closed = cb_closed # FIXME: When to call this???
        self._access_log_server_session = access_log_server_session
        self.cb_error = cb_error
        # start portforward and browser
        self.launch_id = launch_id
        launch_element = database_schema.LaunchElement.lookup(launch_id)
        assert len(launch_element.portforwards) == 1
        portforward_spec = launch_element.portforwards[0]
        self.server_host = portforward_spec.server_host.strip()
        self.server_port = portforward_spec.server_port
        self.citrix_command = expand_variables(launch_element.citrix_command)
        self.citrix_metaframe_path = '/' + expand_variables(launch_element.citrix_metaframe_path).strip('/').encode('utf-8') + '/'
        self.lock_to_process_pid = portforward_spec.lock_to_process_pid
        self.close_with_process = launch_element.close_with_process
        self.sub_processes = portforward_spec.sub_processes
        self.lock_to_process_name = portforward_spec.lock_to_process_name
        self._children = {} # IcaLaunchPortForward's
        self._next_child_id = 1
        self.logged_in = False
        self.url_opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookielib.LWPCookieJar()))
        self._run_in_thread_lock = threading.Semaphore()

        user = expand_variables(launch_element.sso_login)
        password = expand_variables(launch_element.sso_password)
        domain = expand_variables(launch_element.sso_domain)

        def login_thread():
            """Threadlet because URL fetching will block
            Will make process continue when done - and set .logged_in
            """
            # Login
            if launch_element.citrix_https:
                self.citrix_server = 'https://%s:%s' % (self.server_host.encode('utf-8'), self.server_port)
            else:
                self.citrix_server = 'http://%s:%s' % (self.server_host.encode('utf-8'), self.server_port)

            login_params = {
             'LoginType': 'Explicit',
             'user': user.encode('utf-8'),
             'password': password.encode('utf-8'),
             'submitMode': 'submit',
             'slLanguage': 'en',
             'ClientType': 'Ica-Local',
             'ReconnectAtLoginOption': 'DisconnectedAndActive',
             'login': 'Log In',
            }
            if domain:
                login_params['domain'] = domain.encode('utf-8') # specifying this when it isn't needed seems not be rejected ...
            login_url = self.citrix_server + self.citrix_metaframe_path + auth_login_path
            with self.checkpoint_handler.CheckpointScope("LaunchCitrix", "traffic", checkpoint.DEBUG,
                                                         login_url=login_url) as cps:
                try:
                    resp = self.url_opener.open(
                        urllib2.Request(
                            login_url,
                            urllib.urlencode(login_params)))
                    resp_data = resp.read() # FIXME: blocking!
                except (urllib2.HTTPError, urllib2.URLError), e:
                    self.checkpoint_handler.Checkpoint("http_request_error", "server_launch_citrix",
                                                       checkpoint.ERROR,
                                                       message='Error opening url %r: %s' % (login_url, e))

                    if self.cb_error is not None:
                        self.cb_error('server_citrix', 0, "Error opening url %r: %s" % (login_url, e))
                    return
                #self.logged_in = 'NFuse_MessageType=Error' not in resp_data and 'ERROR:' not in resp_data
                self.logged_in = site_default_aspx in resp.url
                cps.add_complete_attr(logged_in="%s" % self.logged_in, url=resp.url, code=resp.code, msg=resp.msg)
                if not self.logged_in:
                    self.checkpoint_handler.Checkpoint("login_failure", "server_launch_citrix",
                                                       checkpoint.ERROR,
                                                       message=resp_data)

            if launch_element.command.startswith('?'):
                # command is the part of a web-interface ica-url, starting with the "?" after site/launch.ica
                path = self.citrix_metaframe_path + site_launch_ica + launch_element.command.strip().encode('utf-8')
                try:
                    resp = self.url_opener.open(urllib2.Request(self.citrix_server + path)) # FIXME: blocking!
                    resp_data = resp.read() # FIXME: blocking!
                except (urllib2.HTTPError, urllib2.URLError), e:
                    self.checkpoint_handler.Checkpoint("http_request_error", "server_launch_citrix",
                                                       checkpoint.ERROR,
                                                       message='Error opening url %r: %s' % (self.citrix_server + path, e))
                    #TODO: Show error message on client
                    return
                if resp_data.lstrip().startswith('<'):
                    self.checkpoint_handler.Checkpoint("http_request_error", "server_launch_citrix",
                                                       checkpoint.ERROR,
                                                       message='No ica on %r' % (self.citrix_server + path),
                                                       resp_data=resp_data)
                    #TODO: Show error message on client
                else:
                    self._launch_ica_from_other_thread(resp_data)
            else:
                # Start http server for web interface
                self.tunnelendpoint_remote('start', browser_opener=expand_variables(launch_element.command))
        # end of login_phase
        self._run_in_thread('login_thread', login_thread)

    # called from owning session
    def close(self, force=False, close_and_forget=False):
        self.checkpoint_handler.Checkpoint("close", "traffic", checkpoint.DEBUG, server_host=self.server_host, server_port=self.server_port)
        for child_id in list(self._children):
            pf = self._children.pop(child_id, None)
            if pf:
                pf.close()
            self.close_tunnelendpoint_tunnel(child_id)
        self.tunnelendpoint_remote('tunnel_close')

    # called from peer
    def started(self, is_ok):
        self.checkpoint_handler.Checkpoint("started", "traffic",
                                           checkpoint.DEBUG,
                                           is_ok=is_ok, server_host=self.server_host, server_port=self.server_port) # FIXME: boolean checkpoint attr

    # called from peer
    def please_close(self, child_id):
        """Client requests a close of a wfica etc"""
        self.checkpoint_handler.Checkpoint("please_close", "traffic", checkpoint.DEBUG, child_id=child_id)
        pf = self._children.pop(child_id, None)
        if pf:
            pf.close()
        self.close_tunnelendpoint_tunnel(child_id)


    # called from peer
    def http_request(self, path):
        with self.checkpoint_handler.CheckpointScope("http_request", "traffic", checkpoint.DEBUG,
                                                     path=path) as cps:
            if '/auth/' in path or '/logout.aspx' in path or '/disconnect.aspx' in path:
                cps.add_complete_attr(logged_in='no more')
                self.logged_in = False # We _could_ close all existing connections, but preventing new connections should be enough
            if not self.logged_in:
                cps.add_complete_attr(http_response_data="Not logged in", status=200)
                self.tunnelendpoint_remote('http_response', status=200, data='Not logged in')
                return

            def request_thread():
                """Threadlet because URL fetching will block"""
                with self.checkpoint_handler.CheckpointScope("http_request_thread", "traffic", checkpoint.DEBUG,
                                                             path=path) as cps:
                    try:
                        resp = self.url_opener.open(urllib2.Request(self.citrix_server + path)) # FIXME: blocking!
                        resp_data = resp.read() # FIXME: blocking!
                    except (urllib2.HTTPError, urllib2.URLError), e:
                        self.checkpoint_handler.Checkpoint("http_request_error", "server_launch_citrix",
                                                           checkpoint.ERROR,
                                                           message='Error opening url %r: %s' % (self.citrix_server + path, e))
                        self.tunnelendpoint_remote('http_response', status=404, data='Error getting page')
                        return
                    content_type = resp.headers.getheader('Content-Type')
                    if (content_type == 'application/x-ica' or
                        (path.startswith(self.citrix_metaframe_path + site_launch_ica + '?') or
                         path.startswith(self.citrix_metaframe_path + site_launcher_aspx + '?'))
                         and not resp_data.lstrip().startswith('<')): # TODO: Check for "Content-Type: application/x-ica" instead?
                        cps.add_complete_attr(launching=path, content_type=content_type)
                        self._launch_ica_from_other_thread(resp_data)
                        status = 304
                        data = 'ICA launch handled elsewhere'
                        cps.add_complete_attr(status=status, data=data)
                        self.tunnelendpoint_remote('http_response', status=status, data=data)
                    else:
                        cps.add_complete_attr(http_response_bytes=len(resp_data), status=200, content_type=content_type)
                        self.tunnelendpoint_remote('http_response', status=200, data=resp_data)
                        #if not '.gif' in path and not '.jpg' in path and not '/icons' in path:
                        #    print resp_data
                return
            # end of request_thread
        self._run_in_thread('request_thread', request_thread)

    def _launch_ica_from_other_thread(self, ica_content):
        self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 0, 0, self, '_launch_ica', (ica_content))

    def _launch_ica(self, (ica_content)):
        """Create IcaLaunchPortForward"""
        self.tunnelendpoint_remote('create', child_id=self._next_child_id)
        new_tunnel = self.create_and_add_tunnelendpoint_tunnel(self.checkpoint_handler, self._next_child_id)
        self._children[self._next_child_id] = server_launch_citrix_ica.IcaLaunchPortForward(
            self.async_service,
            self.checkpoint_handler, new_tunnel,
            self.launch_id, self._next_child_id,
            self.expand_variables,
            lambda child_id=self._next_child_id: self.please_close(child_id),
            self._access_log_server_session,
            self.cb_error,
            ica_content)

        self._next_child_id += 1

    def _run_in_thread(self, name, f):
        """Run f in a daemon thread, ensuring that only one runs at a time"""
        def locked_f():
            #print 'trying', name
            with self._run_in_thread_lock:
                #print 'running', name
                f()
                #print 'finished', name
        t = threading.Thread(name=name, target=locked_f)
        t.daemon = True
        t.start()
