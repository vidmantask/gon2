"""
Handle launch of internal functionality that can be activated by menu items.
"""
from __future__ import with_statement
from lib import checkpoint

import components.traffic
from components.communication import tunnel_endpoint_base


from components.traffic.server_common import database_schema



class LaunchSession(object):
    """
    Sessions using the internal launch must have this api
    """
    def launch_internal_close(self):
        """
        Callback from launch-internal framework to inform that the tunnelendpoint controlling the launch has been asked to close down
        """
        pass

    def launch_internal_remote_closed(self):
        """
        Callback from launch-internal framework to inform that the tunnelendpoint controlling the launch has been closed by remote
        """
        pass


class LaunchSessions(object):
    def __init__(self, cpm_session, endpoint_session, user_session):
        self.cpm_session = cpm_session
        self.endpoint_session = endpoint_session
        self.user_session = user_session

    def reset(self):
        self.cpm_session = None
        self.endpoint_session = None


class LaunchInternal(tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch):
    PORTFORWARD_CHILD_ID = 1

    STATE_INIT = 0
    STATE_WAITING_FOR_CLIENT = 1
    STATE_RUNNING = 2
    STATE_ERROR = 3

    CPM_COMMAND_UPDATE = 'cpm_update'
    CPM_COMMAND_INSTALL = 'cpm_install'
    CPM_COMMAND_REMOVE = 'cpm_remove'
    ENDPOINT_ENROLLMENT = 'endpoint_enrollment'
    DEVICE_ENROLLMENT = 'device_enrollment'
    CHANGE_PASSWORD = 'change_password'
    TEST = 'test'

    def __init__(self, async_service, checkpoint_handler, new_tunnelendpoint, launch_id, traffic_session, launch_sessions, cb_closed):
        tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch.__init__(self, async_service, checkpoint_handler, new_tunnelendpoint)
        self._cb_closed = cb_closed
        self.launch_sessions = launch_sessions
        self.traffic_session = traffic_session
        self.launched_session = None
        self._state = LaunchInternal.STATE_INIT
        self.launch_id = launch_id
        self._launch_element = None

    def tunnelendpoint_connected(self):
        """
        Callback from tunnelendpoint framework telling that the two tunnelendpoints are connected
        """
        self._launch(self.launch_id)

    def _launch(self, launch_id):
        with self.checkpoint_handler.CheckpointScope("LaunchInternal::_launch", components.traffic.module_id, checkpoint.DEBUG, launch_id=launch_id):
            self._launch_element = database_schema.LaunchElement.lookup(launch_id)
            if self._launch_element == None:
                self._state = LaunchInternal.STATE_ERROR
                self.checkpoint_handler.Checkpoint("LaunchInternal::_launch", components.traffic.module_id, checkpoint.ERROR, message="Launch id not found")
                return

            if self._launch_element.command in [LaunchInternal.CPM_COMMAND_UPDATE, LaunchInternal.CPM_COMMAND_INSTALL, LaunchInternal.CPM_COMMAND_REMOVE]:
                self._launch_cpm()
                return

            if self._launch_element.command in [LaunchInternal.ENDPOINT_ENROLLMENT]:
                self._launch_endpoint_enrollment()
                return

            if self._launch_element.command in [LaunchInternal.DEVICE_ENROLLMENT]:
                self._launch_device_enrollment()
                return

            if self._launch_element.command in [LaunchInternal.CHANGE_PASSWORD]:
                self._launch_change_password()
                return

            if self._launch_element.command in [LaunchInternal.TEST]:
                self._launch_test()
                return

            self._state = LaunchInternal.STATE_ERROR
            self.checkpoint_handler.Checkpoint("LaunchInternal::_launch", components.traffic.module_id, checkpoint.ERROR, message="Invalid command '%s'" % self._launch_element.command)

    def _launch_cpm(self):
        self.launched_session = self.launch_sessions.cpm_session
        if not self.launched_session.analyze_ready():
            self._state = LaunchInternal.STATE_ERROR
            self.checkpoint_handler.Checkpoint("LaunchInternal::_launch", components.traffic.module_id, checkpoint.ERROR, message="CPM Session is not ready")
            return
        self._state = LaunchInternal.STATE_WAITING_FOR_CLIENT
        self.tunnelendpoint_remote('remote_launch_cpm')

    def remote_launch_cpm_response(self):
        if self._state != LaunchInternal.STATE_WAITING_FOR_CLIENT:
            self.checkpoint_handler.Checkpoint("LaunchInternal::remote_launch_cpm_response.error.invalid_state", components.traffic.module_id, checkpoint.ERROR)
            self.close()
        if self._launch_element.command == LaunchInternal.CPM_COMMAND_UPDATE:
            self.launch_sessions.cpm_session.analyze_start_update()
            self._state = LaunchInternal.STATE_RUNNING
        elif self._launch_element.command == LaunchInternal.CPM_COMMAND_INSTALL:
            self.launch_sessions.cpm_session.analyze_start_install()
            self._state = LaunchInternal.STATE_RUNNING
        elif self._launch_element.command == LaunchInternal.CPM_COMMAND_REMOVE:
            self.launch_sessions.cpm_session.analyze_start_remove()
            self._state = LaunchInternal.STATE_RUNNING

    def _launch_endpoint_enrollment(self):
        self.launched_session = self.launch_sessions.endpoint_session
        if not self.launched_session.launch_internal_ready():
            self._state = LaunchInternal.STATE_ERROR
            self.checkpoint_handler.Checkpoint("LaunchInternal::_launch", components.traffic.module_id, checkpoint.ERROR, message="Computer Token Session is not ready")
            return
        self._state = LaunchInternal.STATE_WAITING_FOR_CLIENT
        self.tunnelendpoint_remote('remote_launch_endpoint_enrollment')

    def _launch_device_enrollment(self):
        self.launched_session = self.launch_sessions.endpoint_session
        if not self.launched_session.launch_internal_ready():
            self._state = LaunchInternal.STATE_ERROR
            self.checkpoint_handler.Checkpoint("LaunchInternal::_launch", components.traffic.module_id, checkpoint.ERROR, message="Device Session is not ready")
            return
        self._state = LaunchInternal.STATE_WAITING_FOR_CLIENT
        self.tunnelendpoint_remote('remote_launch_device_enrollment')

    def _launch_change_password(self):
        self.launch_sessions.user_session.launch_change_password()
        self._state = LaunchInternal.STATE_RUNNING
        self.tunnelendpoint_remote('close')
        

    def _launch_test(self):
        self.traffic_session.recalculate_menu()


    def remote_launch_endpoint_enrollment_response(self):
        if self._state != LaunchInternal.STATE_WAITING_FOR_CLIENT:
            self.checkpoint_handler.Checkpoint("LaunchInternal::remote_launch_endpoint_enrollment_response.error.invalid_state", components.traffic.module_id, checkpoint.ERROR)
            self.close()
        if self._launch_element.command == LaunchInternal.ENDPOINT_ENROLLMENT:
            self.launched_session.launch_internal_enroll_start()
            self._state = LaunchInternal.STATE_RUNNING

    def remote_launch_device_enrollment_response(self):
        if self._state != LaunchInternal.STATE_WAITING_FOR_CLIENT:
            self.checkpoint_handler.Checkpoint("LaunchInternal::remote_launch_device_enrollment_response.error.invalid_state", components.traffic.module_id, checkpoint.ERROR)
            self.close()
        if self._launch_element.command == LaunchInternal.DEVICE_ENROLLMENT:
            self.launched_session.launch_device_enroll_start(self._launch_element)
            self._state = LaunchInternal.STATE_RUNNING

    def remote_closed(self):
        with self.checkpoint_handler.CheckpointScope("LaunchInternal::remote_closed", components.traffic.module_id, checkpoint.DEBUG):
            if self.launched_session is not None:
                self.launched_session.launch_internal_remote_closed()
                self.launched_session = None
            self._cb_closed()

    def close(self, force = False, close_and_forget = False):
        """
        Close down the launched portforward
        """
        with self.checkpoint_handler.CheckpointScope("LaunchInternal::close", components.traffic.module_id, checkpoint.DEBUG):
            self.tunnelendpoint_remote('remote_closed')
            if not close_and_forget:
                if self.launched_session is not None:
                    self.launched_session.launch_internal_close()
                    self.launched_session = None
                self._cb_closed()

