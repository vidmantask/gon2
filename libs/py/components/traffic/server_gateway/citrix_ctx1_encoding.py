# -*- coding: utf-8 -*-
"""
Citrix XML Service password encoding format "ctx1"

Reverse engineered (based on some hints and examples found on the net).
"""

unicodeencoding = 'utf-8'

def encoder(s):
    prev = 0
    for c in s:
        now = prev ^ ord(c) ^ 0xa5
        yield chr((now//16)+65)
        yield chr((now%16)+65)
        prev = now

def encoded(s):
    return ''.join(encoder(s.encode(unicodeencoding)))

def decoder(s):
    prev = 0
    for i in range(0, len(s), 2):
        now = ((ord(s[i]) - 65) << 4) + (ord(s[i+1]) - 65)
        yield chr(now ^ prev ^ 0xa5)
        prev = now

def decoded(s):
    return (''.join(decoder(s))).decode(unicodeencoding)

def test():
    for l in ur'''
            0    JF
            1    JE
            2    JH
            3    JG
            4    JB
            5    JA
            A    OE
            B    OH
            C    OG
            a    ME
            b    MH
            c    MG
            d    MB
            e    MA
            f    MD
            g    MC
            x    NN
            AA    OEAA
            aa    MEAA
            bb    MHAA
            cc    MGAA
            aaa   MEAAME
            aaaa  MEAAMEAA
            aaaaa MEAAMEAAME
            secret    NGBGNAAHMHBG
            !"#$%&'()*+,-./    IEADIFAEIEAHIFAIIEALIFAMIEAPIF
            ()*+,-./0123456789:;    INABIOAAIJABIKAAJFABJGAAJBABJCAAJNABJOAA
            abcdefghijklmnopqrstuvwxyz    MEADMFAEMEAHMFAIMEALMFAMMEAPMFBAMEBDMFBEMEBHMFBIMEBL
            ABCDEFGHIJKLMNOPQRSTUVWXYZ    OEADOFAEOEAHOFAIOEALOFAMOEAPOFBAOEBDOFBEOEBHOFBIOEBL
            :;<=>?[\]^_`{|}~    JPABJIAAJLABPPAGPOAFPPDKOEDNOFDO
            charlotte    MGALMPBINBBLMKBLNL
            saturn99    NGBCMDBDMEAPJDAP
            æøåÆØÅ    GGGFADBOHIHIBODNFLGGAACA
            '''.strip().splitlines():
        pw, enc = l.strip().split(' ', 1)
        enc = enc.strip()
        print pw, enc
        my_enc = encoded(pw)
        assert my_enc == enc, (my_enc, enc)
        my_dec = decoded(my_enc)
        assert my_dec == pw, (my_dec, pw)
        #print pw, '\t', ' '.join('%02x' % ord(x) for x in pw)

if __name__ == '__main__':
    import sys
    if len(sys.argv) == 2:
        try:
            print 'Encoded %r (%s): %r (%s)' % (sys.argv[1], sys.argv[1], encoded(sys.argv[1]), encoded(sys.argv[1]))
        except Exception, e:
            print "Can't encode %r" % sys.argv[1], e
        try:
            print 'Decoded %r (%s): %r (%s)' % (sys.argv[1], sys.argv[1], decoded(sys.argv[1]), decoded(sys.argv[1]))
        except Exception, e:
            print "Can't decode %r" % sys.argv[1], e
    else:
        test()
