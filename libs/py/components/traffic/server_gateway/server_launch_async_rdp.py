from lib import checkpoint
from lib import encode

from components.traffic.server_gateway.rdp import rdp_engine
import components.traffic


def mk_enginefactory(log_environment, launch_element, expand_variables, server_host, server_port, servers):

    checkpoint_handler = log_environment.checkpoint_handler

    domain = expand_variables(launch_element.sso_domain)
    login = expand_variables(launch_element.sso_login)
    password = expand_variables(launch_element.sso_password)
    

    def mk_logger(level, formatted=True):
        id_ = "RDP::log%s" % level
        if formatted:
            def logger(fmt, *a, **b):
                msg = fmt % a
                checkpoint_handler.Checkpoint(id_, components.traffic.module_id, checkpoint.DEBUG, msg=msg)
        else:
            def logger(fmt, *a, **b):
                if 'dump' in b:
                    b['dump'] = repr(b['dump'])
                d = dict(fmt = fmt)
                d.update((str(i), str(z)) for (i, z) in enumerate(a))
                d.update((k, str(v)) for (k, v) in b.items())
                checkpoint_handler.Checkpoint(id_, components.traffic.module_id, checkpoint.DEBUG, **d)
        return logger

    class GrdprxConfig:
        RANDOM_CLIENT_MITM_SERVERSIDE = '' # '01234567890123456789012345678901'
        RANDOM_SERVER_MITM_CLIENTSIDE = '' # '12345678901234567890123456789012'
        # default keylength when generating keys when no other keys configured
        RSA_LEN = 512
        # default key - sufficiently secure when connecting through G/On
        RSA_PUB_EXP = 65537
        RSA_MOD = 8491507037002009553711464877115059195468672348166072109314232083269468318852493973859578082392998336549474288271169279104681934382050637971687536882537913
        RSA_PRI_EXP = 1253442163601590558350316786261365070982253327069572632032376843211450577789298293827167060288671446282022591042634907018422084651355504852904311317276553

        DOMAIN = domain
        USERNAME = login
        PASSWORD = password

        RECONNECT_TARGETNETADDRESS = '127.0.0.1' # we assume that the client side portforward is listening here, and will make it reconnect here
        SERVER_ADDR = server_host # the server to connect to next time - might be changed from rdp_protocol

        STRICT = False # check as much as possible - setting this might cause trouble and prevent buffer overruns etc
        CHECK_BER_ROUNDTRIP = False

        @classmethod
        def set_config(cls, field, value): # intended for SERVER_ADDR, config class belongs to one portforward instance, so it is ok
            setattr(cls, field, value)

    class Environment:
        config = GrdprxConfig
        log1 = staticmethod(lambda * a, **b: None)
        log2 = staticmethod(lambda * a, **b: None)
        log3 = staticmethod(lambda * a, **b: None)
        #log1 = staticmethod(mk_logger(1)) # too noisy ...
        #log2 = staticmethod(mk_logger(2))
        #log3 = staticmethod(mk_logger(3))

    class RdpProxyEngine(object):

        def __init__(self, cb_connect, cb_to_client, cb_close_client):
            self._cb_to_client = cb_to_client
            self._cb_close_client = cb_close_client
            self._rdp = rdp_engine.RdpEngine(Environment)
            self._pending_connect = []
            self._cb_to_server = lambda chunk: self._pending_connect.append(chunk)
            self._cb_close_server = None
            cb_connect((encode.preferred(GrdprxConfig.SERVER_ADDR), server_port), cb_tcp_connected=self._connected, cb_tcp_error=self._connect_error)

        # call back from cb_connect
        def _connected(self, local, remote, write, close):
            checkpoint_handler.Checkpoint("async rdp connected", components.traffic.module_id, checkpoint.DEBUG,
                                          local='%s:%s' % (local[0], local[1]), remote='%s:%s' % (remote[0], remote[1]))
            self._cb_to_server = write
            self._cb_close_server = close
            for chunk in self._pending_connect:
                self._cb_to_server(chunk)
            self._pending_connect = None
            return (self._from_server, self._server_error)

        # call back from cb_connect
        def _connect_error(self, msg=None):
            checkpoint_handler.Checkpoint("async rdp connect_error", components.traffic.module_id, checkpoint.DEBUG, msg=msg)
            self.close()

        # given as call back when _connected
        def _from_server(self, data):
            """
            Handles data from server
            Returns excess data or None
            """
            while True:
                try:
                    data_to_client, data = self._rdp.from_server(data)
                except rdp_engine.RdpEngine.NeedMoreData:
                    return data
                except rdp_engine.CloseConnection:
                    checkpoint_handler.Checkpoint("async rdp _from_server", components.traffic.module_id, checkpoint.DEBUG, msg='CloseConnection')
                    print 'from_server CloseConnection'
                    return None # use _cb_close_client ?
                self._cb_to_client(data_to_client)
                continue # explicit

        # given as call back when _connected
        def _server_error(self, msg=None):
            checkpoint_handler.Checkpoint("async rdp _server_error", components.traffic.module_id, checkpoint.DEBUG, msg=msg)
            self.close()

        # called from owner of engine / client connection
        def from_client(self, data):
            """
            Handles data from client
            Returns: excess data or None
            """
            if not data:
                return None
            while True:
                try:
                    data_to_server, data = self._rdp.from_client(data)
                except rdp_engine.RdpEngine.NeedMoreData:
                    return data
                except rdp_engine.CloseConnection:
                    checkpoint_handler.Checkpoint("async rdp from_client", components.traffic.module_id, checkpoint.DEBUG, msg='CloseConnection')
                    return None # use _cb_close_client ?
                self._cb_to_server(data_to_server)
                continue # explicit

        # internal _and_ called from engine owner
        def close(self):
            cb_close_server, self._cb_close_server = self._cb_close_server, None
            if cb_close_server:
                cb_close_server()
            cb_close_client, self._cb_close_client = self._cb_close_client, None
            if cb_close_client:
                cb_close_client()

    return RdpProxyEngine
