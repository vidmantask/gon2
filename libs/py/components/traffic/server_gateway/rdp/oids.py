CHECK_OIDS = False

class OIDS(object):

    oid_names = {} # asn.1 declarations, name to number-tuple
    oid_sub_names = {} # naming of pat elements in asn.1 declarations
    oid_numbers = {} # asn.1 declarations, number-tuple to name
    oid_selects = {} # number-tuple to selected type

    @classmethod
    def parse_oid(cls, *args):
        oid = []
        if len(args) == 1 and isinstance(args[0], str):
            if args[0].startswith('{') and args[0].endswith('}'): # asn1 syntax
                s = args[0][1:-1]
                args = []
                for part in s.split():
                    if part.endswith(')'):
                        elem_name, elem_val_str = part[:-1].split('(', 1)
                        args.append((elem_name, int(elem_val_str)))
                    elif part.isdigit():
                        args.append(int(part))
                    elif not args and part in cls.oid_names:
                        args[:] = cls.oid_names[part]
                    else:
                        raise RuntimeError('error parsing {%s}: %r is unknown' % (s, part))
            elif args[0][0].isdigit():
                args = tuple(int(x) for x in args[0].split('.'))
        for arg in args:
            if isinstance(arg, tuple):
                elem_name, elem_val = arg
                oid_last_name = tuple(oid + [elem_name])
                if oid_last_name in cls.oid_sub_names:
                    assert cls.oid_sub_names[oid_last_name] == elem_val, 'elem name %r already %r not %r' % (oid_last_name, cls.oid_sub_names[oid_last_name], elem_val)
                cls.oid_sub_names[oid_last_name] = elem_val
                oid.append(elem_val)
                oid_last_num = tuple(oid)
                if oid_last_num in cls.oid_sub_names:
                    if cls.oid_sub_names[oid_last_num] != elem_name:
                        print 'elem num %r already %r not %r' % (oid_last_num, cls.oid_sub_names[oid_last_num], elem_name)
                cls.oid_sub_names[oid_last_num] = elem_name
            elif isinstance(arg, str):
                assert arg in cls.oid_names, 'elem name %r not found in %r' % (arg, cls.oid_names.keys())
                oid.extend(cls.oid_names[arg])
            else:
                elem_val = arg
                oid.append(elem_val)
        assert oid, (args, oid) # Empty tuple isn't an oid!
        return tuple(oid)

    @classmethod
    def _select(cls, oid, selects):
        assert oid not in cls.oid_selects, (oid, cls.oid_selects)
        cls.oid_selects[oid] = selects

    @classmethod
    def declare(cls, name, *args, **argv):
        selects = argv.pop('selects', None)
        assert not argv, argv
        oid = cls.parse_oid(*args)
        if oid in cls.oid_numbers:
            assert cls.oid_numbers[oid] == name, 'oid %r already %r not %r' % (oid, cls.oid_numbers[oid], name)
        else:
            cls.oid_numbers[oid] = name
        if name in cls.oid_names:
            assert cls.oid_names[name] == oid, 'oid name %r already %r not %r' % (name, cls.oid_names[name], oid)
        else:
            cls.oid_names[name] = oid
        if oid not in cls.oid_sub_names:
            cls.oid_sub_names[oid] = name
            #print 'setting %s in cls.oid_sub_names as %s' % (oid, name)
        if selects:
            cls._select(oid, selects)

    @classmethod
    def select(cls, *args, **argv):
        selects = argv.pop('selects', None)
        assert not argv, argv
        oid = cls.parse_oid(*args)
        cls._select(oid, selects)

    @classmethod
    def dump(cls):
        from pprint import pprint
        print 'oid_names'
        pprint(cls.oid_names)
        print 'oid_sub_names'
        pprint(cls.oid_sub_names)
        print 'oid_numbers'
        pprint(cls.oid_numbers)
        print 'oid_selects'
        pprint(cls.oid_selects)

    @classmethod
    def str(cls, oid):
        oid = tuple(oid)
        elems = []
        for i, elem in enumerate(oid):
            oid_path = oid[:i + 1]
            try:
                oid_path_sub_name = cls.oid_sub_names[oid_path]
                elems.append('%s(%s)' % (oid_path_sub_name, elem))
            except KeyError:
                msg = 'Anonymous oid %s in %s' % ('.'.join(str(x) for x in oid_path), '.'.join(str(x) for x in oid))
                if CHECK_OIDS:
                    raise RuntimeError(msg)
                print msg
                elems.append('%s' % (elem,)) # FIXME: Allow anon elems in path???
        s = '.'.join(elems)
        if oid in cls.oid_numbers:
            return '%s(%s)' % (cls.oid_numbers[oid], s)
        return s

    @classmethod
    def repr(cls, oid):
        return '.'.join(str(x) for x in oid)

    @classmethod
    def get_selection(cls, oid):
        return cls.oid_selects.get(tuple(oid))
