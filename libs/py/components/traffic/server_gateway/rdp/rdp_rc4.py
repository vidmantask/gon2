"""
RC4 implementation based on wikipedia description
"""

import util
import Crypto.Cipher.ARC4


class py_PRNG(object):

    def __init__(self, key_str):
        """
        Key scheduling - generate S based on key
        """
        key = util.to_list(key_str)
        self.S = range(256)
        j = 0
        for i in range(256):
            j = (j + key[i % len(key)] + self.S[i]) & 255
            self.S[i], self.S[j] = self.S[j], self.S[i]

        self.i = 0
        self.j = 0

    def _next(self):
        self.i = (self.i + 1) & 255
        self.j = (self.j + self.S[self.i]) & 255
        self.S[self.i], self.S[self.j] = self.S[self.j], self.S[self.i]
        return self.S[(self.S[self.i] + self.S[self.j]) & 255]

    def xor(self, l):
        """
        XOR l with rc4
        """
        #print 'xor input:'; util.dump(l)
        result = [n ^ self._next() for n in util.to_list(l)] # return list with computations done, NOT generator with delayed evalution!
        #print 'xor output:'; util.dump(result)
        return util.to_str(result)

class pycrypto_PRNG(object):

    def __init__(self, key):
        """
        Key scheduling - generate S based on key
        """
        self.S = Crypto.Cipher.ARC4.new(key)

    def xor(self, l):
        return self.S.encrypt(l)

PRNG = pycrypto_PRNG


def _test():
    print
    print "Wikipedia test vectors"
    test_vectors = [
        ("Key", "Plaintext", "BBF316E8D940AF0AD3"),
        ("Wiki", "pedia", "1021BF0420"),
        ("Secret", "Attack at dawn", "45A01F645FC35B383552544B9BF5"),
        ]

    for (key, plain, ciph_ok) in test_vectors:
        rc4prng = PRNG(key)
        ciph = rc4prng.xor(plain)
        ciph_hexstring = ''.join('%02X' % ord(c) for c in ciph)
        print key, plain, ciph_ok, ciph_hexstring
        assert ciph_ok == ciph_hexstring, (key, plain, ciph_ok, ciph_hexstring)

def _rdesktop_dump_rc4():
    """Dumped from rdesktop"""

    print
    print 'Encrypt rc4 key'
    en_key = util.undump("""
0000 7a e4 5d 0a d6 91 b8 05 5b 87 09 28 01 c0 56 db z.].....[..(..V.
    """)

    rc4prng_en = PRNG(en_key)

    # sec_encrypt: cleartext - dumping 0x22 (34) bytes:
    en_clear = util.undump("""
0000 00 00 00 00 3b 01 00 00 10 00 04 00 10 00 00 00 ....;...........
0010 00 00 47 00 69 00 72 00 69 00 74 00 65 00 63 00 ..G.i.r.i.t.e.c.
0020 68 00 00 00 6d 00 6b 00 00 00                   .
    """)

    my_en_rc4 = rc4prng_en.xor(en_clear)

    # sec_encrypt: rc4 data - dumping 0x22 (34) bytes:
    en_rc4 = util.undump("""
0000 77 9a 8f 10 9c 22 9e b8 fe 88 8e 71 09 67 c3 b6 w....".....q.g..
0010 98 f6 fc 12 ec 7d 9b 2d dd 4a 03 5f c5 6a 7d 99 .....}.-.J._.j}.
0020 cd 52 ef 80 07 ab 67 5a 3b f3                     .
    """)

    print 'my_en_rc4:'; util.dump(my_en_rc4)
    assert my_en_rc4 == en_rc4, (my_en_rc4, en_rc4)

    print 'Decrypt rc4 key'
    de_key = util.undump("""
0000 b1 f2 68 99 40 79 2b 7b b0 2a ac a4 26 11 5c 52 ..h.@y+{.*..&.\R
    """)

    rc4prng_de = PRNG(de_key)

    # sec_decrypt: rc4 data - dumping 0x16 (22) bytes:
    de_rc4 = util.undump("""
0000 2a 5c 78 75 51 a7 2a 4d 43 2c a7 8a 69 d4 c0 cb ...
0010 7a bf 2f 22 0d cb 6d 3f ab c3 28 df 01 0f fe 82 z./"..m?..(.....
0020 21 d5 83 a0 cc f7 fc b6 44 74 14 6e c9 48 56 8f !.......Dt.n.HV.
    """)

    my_de_clear = rc4prng_de.xor(de_rc4)

    # sec_decrypt: cleartext - dumping 0x16 (22) bytes:
    de_clear = util.undump("""
0000 67 01 11 00 ea 03 ea 03 01 00 04 00 51 01 52 44 g...........Q.RD
0010 50 00 0d 00 00 00 09 00 08 00 ea 03 5f e1 01 00 P..........._...
0020 18 00 01 00 03 00 00 02 00 00 00 00 1d 04 00 00 ................
    """)

    print 'my_de_clear:'; util.dump(my_de_clear)
    assert my_de_clear == de_clear, (my_de_clear, de_clear)


if __name__ == '__main__':
    _test()
    _rdesktop_dump_rc4()
