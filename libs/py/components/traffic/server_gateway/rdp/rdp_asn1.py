from asn1 import *

# http://www.oid-info.com/get/0.0.20.124.0.1
# Description: Version 1 of Recommendation ITU-T T.124 (February 1998): "Generic Conference Control".
oids.OIDS.declare('T124-GCC',
    '{itu-t(0) recommendation(0) t(20) t124(124) version(0) 1}') # no name for last element?

# http://www.oid-info.com/get/1.3.6.1.4.1.311.18
oids.OIDS.declare('MS-crypto-oids',
    '{iso(1) identified-organization(3) dod(6) internet(1) private(4) enterprise(1) microsoft(311) 18}',
    )
# http://support.microsoft.com/kb/287547
# Microsoft Hydra.................................1.3.6.1.4.1.311.18
#      Manufacturer value
#         szOID_PKIX_MANUFACTURER                 1.3.6.1.4.1.311.18.2
oids.OIDS.declare('szOID_PKIX_MANUFACTURER', '1.3.6.1.4.1.311.18.2')
#      OID for Certificate Version Stamp
#         szOID_PKIX_HYDRA_CERT_VERSION           1.3.6.1.4.1.311.18.4
oids.OIDS.declare('szOID_PKIX_HYDRA_CERT_VERSION', '1.3.6.1.4.1.311.18.4')
#      OID for License Server to identify licensed product.
#         szOID_PKIX_LICENSED_PRODUCT_INFO        1.3.6.1.4.1.311.18.5
oids.OIDS.declare('szOID_PKIX_LICENSED_PRODUCT_INFO', '1.3.6.1.4.1.311.18.5')
#      OID for License Server specific info.
#         szOID_PKIX_MS_LICENSE_SERVER_INFO       1.3.6.1.4.1.311.18.6
oids.OIDS.declare('szOID_PKIX_MS_LICENSE_SERVER_INFO', '1.3.6.1.4.1.311.18.6')
#      Extension OID reserved for product policy module - only one is allowed.
#         szOID_PKIS_TLSERVER_SPK_OID             1.3.6.1.4.1.311.18.8
oids.OIDS.declare('szOID_PKIS_TLSERVER_SPK_OID', '1.3.6.1.4.1.311.18.8')

oids.OIDS.declare('Authenticode',
    '{iso(1) identified-organization(3) dod(6) internet(1) private(4) enterprise(1) microsoft(311) 2}',
    )
oids.OIDS.declare('SoftwarePublishing', '1.3.6.1.4.1.311.2.1')
oids.OIDS.declare('SPC_SP_AGENCY_INFO_OBJID', '1.3.6.1.4.1.311.2.1.10')

# http://www.oid-info.com/get/1.3.6.1.4.1.311.20
# Description: Microsoft Enrollment Infrastructure
# Microsoft Enrollment Infrastructure..............1.3.6.1.4.1.311.20
oids.OIDS.declare('MS-enrollment-oids', '1.3.6.1.4.1.311.20')

#      Extension contain certificate type
#         szOID_ENROLL_CERTTYPE_EXTENSION         1.3.6.1.4.1.311.20.2
# http://www.oid-info.com/get/1.3.6.1.4.1.311.20.2
oids.OIDS.declare('szOID_ENROLL_CERTTYPE_EXTENSION', '1.3.6.1.4.1.311.20.2')

# Microsoft CertSrv Infrastructure.................1.3.6.1.4.1.311.21
# http://www.oid-info.com/get/1.3.6.1.4.1.311.21
oids.OIDS.declare('MS-cert-oids', '1.3.6.1.4.1.311.21')
#      CertSrv (with associated encoders/decoders)
#         szOID_CERTSRV_CA_VERSION                1.3.6.1.4.1.311.21.1
# http://www.oid-info.com/get/1.3.6.1.4.1.311.21.1
oids.OIDS.declare('szOID_CERTSRV_CA_VERSION', '1.3.6.1.4.1.311.21.1')
oids.OIDS.declare('szOID_CERTSRV_PREVIOUS_CERT_HASH', '1.3.6.1.4.1.311.21.2') # Contains the sha1 hash of the previous version of the CA certificate.

# http://www.oid-info.com/get/1.3.6.1.5.5.7.1.1
oids.OIDS.declare('authorityInfoAccess',
    '{iso(1) identified-organization(3) dod(6) internet(1) security(5) mechanisms(5) pkix(7) privateExtension(1) authorityInfoAccess(1)}')

# http://msdn.microsoft.com/en-us/library/aa379070
# http://www.alvestrand.no/objectid/2.5.29.1.html
# Old Authority Key Identifier
# enables distinct keys used by the same certification authority to be differentiated
# http://www.oid-info.com/get/2.5.29.1
# Description: X.509 old Authority Key Identifier
# Deprecated, use {2 5 29 35} instead
oids.OIDS.declare('authorityKeyIdentifier',
    '{joint-iso-itu-t(2) ds(5) ce(29) authorityKeyIdentifier(1)}')

# http://www.oid-info.com/get/2.5.29.37
# Description: Certificate extension: "extKeyUsage" (Extended key usage)
# This field indicates one or more purposes for which the certified public key may be used, in addition to or in place of the basic purposes indicated in the key usage extension field.
oids.OIDS.declare('extKeyUsage',
    '{joint-iso-itu-t(2) ds(5) ce(29) extKeyUsage(37)}')

# http://www.oid-info.com/get/2.5.29.31
# Description: Certificate Revocation List distribution points.
oids.OIDS.declare('cRLDistributionPoints',
    '{joint-iso-itu-t(2) ds(5) ce(29) cRLDistributionPoints(31)}')


#
# T124
#

ChannelId = INTEGER(per_bits=16) # INTEGER (0..65535)                  -- range is 16 bits
DynamicChannelId = INTEGER(per_bits=16, min_value=1001) # (1001..65535)      -- those created and deleted # FIXME: Should be constrained ChannelId
UserId = DynamicChannelId() #                       -- created by Attach-User, deleted by Detach-User

H221NonStandardIdentifier = OCTET_STRING(min_size=4) # (SIZE (4..255))
# First four octets shall be country code and Manufacturer code, assigned as specified in Annex A/H.221 for NS-cap and NS-comm

Key = CHOICE( #Identifier of a standard or non-standard object
    Field('object', OBJECT_IDENTIFIER(), per_index=0),
    Field('h221NonStandard', H221NonStandardIdentifier(), per_index=1),
    per_bits=1,
    )

SimpleNumericString = NumericString(min_size=1) # (SIZE (1..255)) (FROM ("0123456789"))

#simpleTextFirstCharacter UniversalString ::= {0, 0, 0, 0}
#simpleTextLastCharacter UniversalString ::= {0, 0, 0, 255}
SimpleTextString = BMPString() #  (SIZE (0..255)) (FROM (simpleTextFirstCharacter..simpleTextLastCharacter))

TextString = BMPString() # (SIZE (0..255)) # Basic Multilingual Plane of ISO/IEC 10646-1 (Unicode)

SimpleStuffReuse = SEQUENCE(
    Field('numeric', SimpleNumericString(), ),
    Field('text', SimpleTextString(), optional=True),
    # ...,
    #Field('unicodeText', TextString(), optional=True),
    extension=True,
)

Password = SimpleStuffReuse()
ConferenceName = SimpleStuffReuse()

Set_Privilege = TextString() # FIXME: temp HACK

UserData = SET_OF(
    SEQUENCE(
        Field('key', Key()),
        Field('value', OCTET_STRING(), optional=True),
        )
    )

TerminationMethod = ENUMERATED(
    dict(automatic=0, manual=1), # ...
    extension=True,
)

Connect_Reason = ENUMERATED( # Reason in Connect, response, confirm
    dict(
        rt_successful = 0,
        rt_domain_merging = 1,
        rt_domain_not_hierarchical = 2,
        rt_no_such_channel = 3,
        rt_no_such_domain = 4,
        rt_no_such_user = 5,
        rt_not_admitted = 6,
        rt_other_user_id = 7,
        rt_parameters_unacceptable = 8,
        rt_token_not_available = 9,
        rt_token_not_possessed = 10,
        rt_too_many_channels = 11,
        rt_too_many_tokens = 12,
        rt_too_many_users = 13,
        rt_unspecified_failure = 14,
        rt_user_rejected = 15,
        ),
)

ConferenceCreateRequest = SEQUENCE( # MCS-Connect-Provider request user data
    Field('conferenceName',         ConferenceName()),
    Field('convenerPassword',       Password(), optional=True),
    Field('password',               Password(), optional=True),
    Field('lockedConference',       BOOLEAN()),
    Field('listedConference',       BOOLEAN()),
    Field('conductibleConference',  BOOLEAN()),
    Field('terminationMethod',      TerminationMethod()),
    Field('conductorPrivileges',    Set_Privilege(), optional=True),
    Field('conductedPrivileges',    Set_Privilege(), optional=True),
    Field('nonConductedPrivileges', Set_Privilege(), optional=True),
    Field('conferenceDescription',  TextString(), optional=True),
    Field('callerIdentifier',       TextString(), optional=True),
    Field('userData',               UserData(), optional=True),
    #...,
    #Field('conferencePriority',     ConferencePriority, optional=True),
    #Field('conferenceMode',         ConferenceMode, optional=True),
    extension=True,
)

ConferenceCreateResponse = SEQUENCE( # MCS-Connect-Provider response user data
    Field('nodeID', UserId()), # Node ID of the sending node
    Field('tag', INTEGER(per_bits=16)),
    Field('result', ENUMERATED(
        dict(
            success                          = 0,
            userRejected                     = 1,
            resourcesNotAvailable            = 2,
            rejectedForSymmetryBreaking      = 3,
            lockedConferenceNotSupported     = 4,
            ), #...
        extension=True,
        )),
     Field('userData', UserData(), optional=True),
     extension=True,
     )

ConnectGCCPDU = CHOICE(
     Field('conferenceCreateRequest', ConferenceCreateRequest(), per_index=0),
     Field('conferenceCreateResponse', ConferenceCreateResponse(), per_index=1),
     #conferenceQueryRequest
     #conferenceQueryResponse
     #conferenceJoinRequest
     #conferenceJoinResponse
     #conferenceInviteRequest
     #conferenceInviteResponse
     extension=True,
     per_bits=3,
)

#
# T125
#

Result = ENUMERATED( # in Connect, response, confirm
    dict(
        rt_successful                     = 0,
        rt_domain_merging                 = 1,
        rt_domain_not_hierarchical        = 2,
        rt_no_such_channel                = 3,
        rt_no_such_domain                 = 4,
        rt_no_such_user                   = 5,
        rt_not_admitted                   = 6,
        rt_other_user_id                  = 7,
        rt_parameters_unacceptable        = 8,
        rt_token_not_available            = 9,
        rt_token_not_possessed            = 10,
        rt_too_many_channels              = 11,
        rt_too_many_tokens                = 12,
        rt_too_many_users                 = 13,
        rt_unspecified_failure            = 14,
        rt_user_rejected                  = 15,
    ),
)

ConnectData = SEQUENCE(
    Field('t124Identifier', Key()), # This shall be set to the value {itu-t recommendation t 124 version(0) 1}
    Field('connectPDU', OCTET_STRING()),
)


'''Variable-length PER-encoded MCS Domain PDU which encapsulates an
    MCS Send Data Request structure, as specified in [T125] (the ASN.1 structure definitions are
    given in [T125] section 7, parts 7 and 10). The userData field of the MCS Send Data Request
    contains the Security Exchange PDU data.
        SendDataRequest ::= [APPLICATION 25] IMPLICIT SEQUENCE
            initiator                   UserId,
            channelId                   ChannelId,
            dataPriority                DataPriority,
            segmentation                Segmentation,
            userData                    OCTET STRING
        DynamicChannelId ::= ChannelId (1001..65535)      -- those created and deleted
        UserId ::= DynamicChannelId                       -- created by Attach-User, deleted by Detach-User

    2.2.1.10.1        Security Exchange PDU Data (TS_SECURITY_PACKET)
    The TS_SECURITY_PACKET structure contains the encrypted client random value which is used together with the server random (see section 2.2.1.4.3) to derive session keys to secure the connection (see sections 5.3.4 and 5.3.5).

    basicSecurityHeader (4 bytes): TS_SECURITY_HEADER (4 bytes). The basic security header, as specified in section 2.2.8.1.1.2.1. The flags field of the security header MUST contain the SEC_EXCHANGE_PKT flag (0x0001).
    length (4 bytes): A 32-bit, unsigned integer. The size in bytes of the buffer containing the encrypted client random value, not including the header length.
    encryptedClientRandom (variable): The client random value encrypted with the public key of the server (see section 5.3.4).
    '''

DataPriority = ENUMERATED(dict(top=0, high=1, medium=2, low=3)) # FIXME: DataPriority ::= CHOICE { top high medium low ... }
Segmentation = BIT_STRING(dict(begin=0, end=1)) # (SIZE (2))

DomainParameters = SEQUENCE(
    Field('maxChannelIds', INTEGER()), # a limit on channel ids in use, static + user id + private + assigned
    Field('maxUserIds', INTEGER()), # a sublimit on user id channels alone
    Field('maxTokenIds', INTEGER()), # a limit on token ids in use grabbed + inhibited + giving + ungivable + given
    Field('numPriorities', INTEGER()), # the number of TCs in an MCS connection
    Field('minThroughput', INTEGER()), # the enforced number of octets per second
    Field('maxHeight', INTEGER()), # a limit on the height of a provider
    Field('maxMCSPDUsize', INTEGER()), # an octet limit on domain MCSPDUs
    Field('protocolVersion', INTEGER()),
    )

ConnectInitial = SEQUENCE(
    Field('callingDomainSelector', OCTET_STRING()),
    Field('calledDomainSelector', OCTET_STRING()),
    Field('upwardFlag', BOOLEAN()),
    Field('targetParameters', DomainParameters()),
    Field('minimumParameters', DomainParameters()),
    Field('maximumParameters', DomainParameters()),
    Field('userData', OCTET_STRING()),
    )

ConnectResponse = SEQUENCE(
    Field('result', Result),
    Field('calledConnectId', INTEGER()),
    Field('domainParameters', DomainParameters()),
    Field('userData', OCTET_STRING()),
    )

ConnectAdditional = SEQUENCE( # [APPLICATION 103] IMPLICIT SEQUENCE
     Field('calledConnectId', INTEGER()),
     Field('dataPriority', DataPriority()),
     )

ConnectResult = SEQUENCE( # [APPLICATION 104] IMPLICIT SEQUENCE
     Field('result', Result())
     )

SendDataRequest = SEQUENCE( # [APPLICATION 25] IMPLICIT SEQUENCE
    Field('initiator', UserId()),
    Field('channelId', ChannelId()),
    Field('dataPriority', DataPriority()),
    Field('segmentation', Segmentation()),
    Field('userData', OCTET_STRING()), # HACK: workaround for rdesktop implementation shortcut?
    )

SendDataIndication = SEQUENCE( # [APPLICATION 26] IMPLICIT SEQUENCE
    Field('initiator', UserId()),
    Field('channelId', ChannelId()),
    Field('dataPriority', DataPriority()),
    Field('segmentation', Segmentation()),
    Field('userData', OCTET_STRING()),
    )

'''PER-encoded MCS Domain PDU which encapsulates an MCS Erect Domain
Request structure, as specified in [T125] (the ASN.1 structure definitions are given in [T125]
section 7, parts 3 and 10).
ErectDomainRequest ::= [APPLICATION 1] IMPLICIT SEQUENCE
subHeight                  INTEGER (0..MAX), -- height in domain of the MCSPDU transmitter
subInterval                INTEGER (0..MAX)  -- its throughput enforcement interval in milliseconds'''

ErectDomainRequest = SEQUENCE( # [APPLICATION 1] IMPLICIT SEQUENCE
    Field('subHeight', INTEGER(per_bits=16)), # height in domain of the MCSPDU transmitter
    Field('subInterval', INTEGER(per_bits=16)), # its throughput enforcement interval in milliseconds
    )

'''-- Part 4: Disconnect provider
'''
Disconnect_Reason = ENUMERATED( # Reason in DisconnectProviderUltimatum, DetachUserRequest, DetachUserIndication
    dict(
        rn_domain_disconnected = 0,
        rn_provider_initiated = 1,
        rn_token_purged = 2,
        rn_user_requested = 3,
        rn_channel_purged = 4,
        ),
    )

DisconnectProviderUltimatum = SEQUENCE( # [APPLICATION 8] IMPLICIT
    Field('reason', Disconnect_Reason()),
    )

'''PER-encoded MCS Domain PDU which encapsulates an MCS Attach User
Request structure, as specified in [T125] (the ASN.1 structure definitions are given in [T125]
section 7, parts 5 and 10).
    AttachUserRequest ::= [APPLICATION 10] IMPLICIT SEQUENCE'''

AttachUserRequest = SEQUENCE() # [APPLICATION 10] IMPLICIT SEQUENCE

'''PER-encoded MCS Domain PDU which encapsulates an MCS Channel Join
Request structure as specified in [T125] (the ASN.1 structure definitions are given in [T125]
section 7, parts 6 and 10).
DynamicChannelId ::= ChannelId (1001..65535)      -- those created and deleted
UserId ::= DynamicChannelId                       -- created by Attach-User, deleted by Detach-User
ChannelJoinRequest ::= [APPLICATION 14] IMPLICIT SEQUENCE
  initiator UserId,
  channelId ChannelId -- may be zero'''

'''PER-encoded MCS Domain PDU which encapsulates an MCS Attach User
Confirm structure, as specified in [T125] (the ASN.1 structure definitions are given in [T125]
section 7, parts 5 and 10).
    AttachUserConfirm ::= [APPLICATION 11] IMPLICIT SEQUENCE
        result                      Result,
        initiator                   UserId OPTIONAL'''

AttachUserConfirm = SEQUENCE( # [APPLICATION 11] IMPLICIT SEQUENCE
    Field('result', Result()),
    Field('initiator', UserId(), optional=True),
    )

'''PER-encoded MCS Domain PDU which encapsulates an MCS Channel Join
Request structure as specified in [T125] (the ASN.1 structure definitions are given in [T125]
section 7, parts 6 and 10).
DynamicChannelId ::= ChannelId (1001..65535)      -- those created and deleted
UserId ::= DynamicChannelId                       -- created by Attach-User, deleted by Detach-User
ChannelJoinRequest ::= [APPLICATION 14] IMPLICIT SEQUENCE
  initiator UserId,
  channelId ChannelId -- may be zero'''

ChannelJoinRequest = SEQUENCE( #[APPLICATION 14] IMPLICIT SEQUENCE
    Field('initiator', UserId()),
    Field('channelId', ChannelId()), # may be zero
    )


'''PER-encoded MCS Domain PDU which encapsulates an MCS Channel Join
Confirm PDU structure, as specified in [T125] (the ASN.1 structure definitions are given in
[T125] section 7, parts 6 and 10).
    ChannelJoinConfirm ::= [APPLICATION 15] IMPLICIT SEQUENCE
        result                        Result,
        initiator                     UserId,
        requested                     ChannelId, -- may be zero
        channelId                     ChannelId OPTIONAL
'''

ChannelJoinConfirm = SEQUENCE( # [APPLICATION 15] IMPLICIT SEQUENCE
    Field('result', Result()),
    Field('initiator', UserId()),
    Field('requested', ChannelId()), # may be zero
    Field('channelId', ChannelId(), optional=True),
    )

# T.125 7.2 Part 10: MCSPDU repertoire
ConnectMCSPDU = CHOICE(
    Field('connect-initial', ConnectInitial(), APPLICATION(101)),
    Field('connect-response', ConnectResponse(), APPLICATION(102)),
    Field('connect-additional', ConnectAdditional(), APPLICATION(103)),
    Field('connect-result', ConnectResult(), APPLICATION(104)),
    )

DomainMCSPDU = CHOICE(
    Field('erectDomainRequest', ErectDomainRequest(), per_index=1),
    Field('disconnectProviderUltimatum', DisconnectProviderUltimatum(), per_index=8),
    Field('attachUserRequest', AttachUserRequest(), per_index=10),
    Field('attachUserConfirm', AttachUserConfirm(), per_index=11),
    Field('channelJoinRequest', ChannelJoinRequest(), per_index=14),
    Field('channelJoinConfirm', ChannelJoinConfirm(), per_index=15),
    Field('sendDataRequest', SendDataRequest(), per_index=25),
    Field('sendDataIndication', SendDataIndication(), per_index=26),
    per_bits=6,
    )


def rdp_test():
    print
    print 'ConnectMCSPDU ConnectInitial'
    _MCS_CONNECT_INITIAL = 0x7f65
    connect_mcs_pdu = '\x7fe\x82\x01\x8a\x04\x01\x01\x04\x01\x01\x01\x01\xff0 \x02\x02\x00"\x02\x02\x00\x02\x02\x02\x00\x00\x02\x02\x00\x01\x02\x02\x00\x00\x02\x02\x00\x01\x02\x02\xff\xff\x02\x02\x00\x020 \x02\x02\x00\x01\x02\x02\x00\x01\x02\x02\x00\x01\x02\x02\x00\x01\x02\x02\x00\x00\x02\x02\x00\x01\x02\x02\x04 \x02\x02\x00\x020 \x02\x02\xff\xff\x02\x02\xfc\x17\x02\x02\xff\xff\x02\x02\x00\x01\x02\x02\x00\x00\x02\x02\x00\x01\x02\x02\xff\xff\x02\x02\x00\x02\x04\x82\x01\x17\x00\x05\x00\x14|\x00\x01\x81\x0e\x00\x08\x00\x10\x00\x01\xc0\x00Duca\x81\x00\x01\xc0\xd4\x00\x04\x00\x08\x00 \x03X\x02\x01\xca\x03\xaa\t\x04\x00\x00(\n\x00\x00d\x00e\x00v\x00-\x00m\x00k\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x04\x00\x00\x00\x00\x00\x00\x00\x0c\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\xca\x01\x00\x00\x00\x00\x00\x18\x00\x07\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x04\xc0\x0c\x00\t\x00\x00\x00\x00\x00\x00\x00\x02\xc0\x0c\x00\x03\x00\x00\x00\x00\x00\x00\x00\x03\xc0\x14\x00\x01\x00\x00\x00cliprdr\x00\xc0\xa0\x00\x00'
    # dumpasn1 -h mcs_pdu

    #file('mcs_pdu','w').write(connect_mcs_pdu)

    test_ber_roundtrip('ConnectMCSPDU connect_mcs_pdu', ConnectMCSPDU, connect_mcs_pdu)

    # bogus tlv ...
    _xx = '\x01\xc0\xd4\x00\x04\x00\x08\x00 \x03X\x02\x01\xca\x03\xaa\t\x04\x00\x00(\n\x00\x00d\x00e\x00v\x00-\x00m\x00k\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x04\x00\x00\x00\x00\x00\x00\x00\x0c\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\xca\x01\x00\x00\x00\x00\x00\x18\x00\x07\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x04\xc0\x0c\x00\t\x00\x00\x00\x00\x00\x00\x00\x02\xc0\x0c\x00\x03\x00\x00\x00\x00\x00\x00\x00\x03\xc0\x14\x00\x01\x00\x00\x00cliprdr\x00\xc0\xa0\x00\x00'
    # print de_ber(SEQUENCE(), xx)


def test_ber_round():
    x224_body = util.undump('''
0000 7f 66 82 06 30 0a 01 00 02 01 00 30 1a 02 01 22 .f..0......0..."
0010 02 01 03 02 01 00 02 01 01 02 01 00 02 01 01 02 ................
0020 03 00 ff f8 02 01 02 04 82 06 0a 00 05 00 14 7c ...............|
0030 00 01 2a 14 76 0a 01 01 00 01 c0 00 4d 63 44 6e ..*.v.......McDn
0040 85 f3 01 0c 0c 00 04 00 08 00 00 00 00 00 03 0c ................
0050 0c 00 eb 03 01 00 ec 03 00 00 02 0c db 05 02 00 ................
0060 00 00 02 00 00 00 20 00 00 00 a7 05 00 00 79 0d ...... .......y.
0070 1a 4c a5 fb 4f 3a 69 61 c1 e5 ba e9 84 41 47 ab .L..O:ia.....AG.
0080 6e 11 df 1e c8 92 45 ba 63 9f 57 af e9 3b 02 00 n.....E.c.W..;..
0090 00 00 02 00 00 00 e2 01 00 00 30 82 01 de 30 82 ..........0...0.
00a0 01 8c a0 03 02 01 02 02 08 01 9e b1 9e fd 57 f2 ..............W.
00b0 40 30 09 06 05 2b 0e 03 02 1d 05 00 30 42 31 40 @0...+......0B1@
00c0 30 0f 06 03 55 04 03 1e 08 00 50 00 41 00 49 00 0...U.....P.A.I.      PAI
00d0 4e 30 2d 06 03 55 04 07 1e 26 00 53 00 69 00 74 N0-..U...&.S.i.t
00e0 00 65 00 20 00 4c 00 69 00 63 00 65 00 6e 00 73 .e. .L.i.c.e.n.s
00f0 00 65 00 20 00 53 00 65 00 72 00 76 00 65 00 72 .e. .S.e.r.v.e.r
0100 30 1e 17 0d 37 30 31 31 32 32 31 31 30 37 35 37 0...701122110757
0110 5a 17 0d 34 39 31 31 32 32 31 31 30 37 35 37 5a Z..491122110757Z
0120 30 42 31 40 30 0f 06 03 55 04 03 1e 08 00 50 00 0B1@0...U.....P.        P
0130 41 00 49 00 4e 30 2d 06 03 55 04 07 1e 26 00 53 A.I.N0-..U...&.S AI
0140 00 69 00 74 00 65 00 20 00 4c 00 69 00 63 00 65 .i.t.e. .L.i.c.e
0150 00 6e 00 73 00 65 00 20 00 53 00 65 00 72 00 76 .n.s.e. .S.e.r.v
0160 00 65 00 72 30 5c 30 0d 06 09 2a 86 48 86 f7 0d .e.r0\0...*.H...
0170 01 01 01 05 00 03 4b 00 30 48 02 41 00 d2 31 5b ......K.0H.A..1[    K
0180 32 b9 e2 91 a0 dc 87 92 1f 0d 54 ec 55 f6 c3 a0 2.........T.U...
0190 53 6e ee 39 44 b6 39 3a 3b 66 dd e3 a6 88 78 c0 Sn.9D.9:;f....x.
01a0 e6 34 37 71 69 55 ae 8e 51 7c a4 1d 5f 1b f7 ae .47qiU..Q|.._...
01b0 cc a4 ef a0 5e 17 8a 3a cb 38 58 ec 35 02 03 01 ....^..:.8X.5...
01c0 00 01 a3 6a 30 68 30 0f 06 03 55 1d 13 04 08 30 ...j0h0...U....0
01d0 06 01 01 ff 02 01 00 30 55 06 09 2b 06 01 04 01 .......0U..+....
01e0 82 37 12 08 04 48 59 00 54 00 52 00 32 00 43 00 .7...HY.T.R.2.C.    YTR2C
01f0 4a 00 56 00 33 00 39 00 4b 00 36 00 4d 00 59 00 J.V.3.9.K.6.M.Y. JV39K6MY
0200 47 00 38 00 59 00 59 00 46 00 4a 00 44 00 46 00 G.8.Y.Y.F.J.D.F. G8YYFJDF
0210 46 00 4b 00 58 00 48 00 36 00 51 00 46 00 51 00 F.K.X.H.6.Q.F.Q. FKXH6QFQ
0220 50 00 4a 00 48 00 57 00 44 00 46 00 00 00 30 09 P.J.H.W.D.F...0. PJHWDF
0230 06 05 2b 0e 03 02 1d 05 00 03 41 00 2a 1f 19 4d ..+.......A.*..M      A
0240 77 e6 6f c6 7b a7 4a d6 f9 40 02 8d 3f 14 55 84 w.o.{.J..@..?.U.
0250 a9 64 f8 80 39 54 32 b0 c8 08 54 2a 35 b6 95 3d .d..9T2...T*5..=
0260 c9 9f e6 63 ca 5c f1 58 bf 5f 99 31 b3 3f af f0 ...c.\.X._.1.?..
0270 e1 92 67 a2 db 32 f6 c0 8d bb e5 09 a5 03 00 00 ..g..2..........
0280 30 82 03 a1 30 82 03 4f a0 03 02 01 02 02 05 01 0...0..O........
0290 00 00 00 45 30 09 06 05 2b 0e 03 02 1d 05 00 30 ...E0...+......0
02a0 42 31 40 30 0f 06 03 55 04 03 1e 08 00 50 00 41 B1@0...U.....P.A
02b0 00 49 00 4e 30 2d 06 03 55 04 07 1e 26 00 53 00 .I.N0-..U...&.S.       &S
02c0 69 00 74 00 65 00 20 00 4c 00 69 00 63 00 65 00 i.t.e. .L.i.c.e. ite Lice
02d0 6e 00 73 00 65 00 20 00 53 00 65 00 72 00 76 00 n.s.e. .S.e.r.v. nse Serv
02e0 65 00 72 30 1e 17 0d 37 39 31 32 33 31 32 33 30 e.r0...791231230 e
02f0 30 30 30 5a 17 0d 33 38 30 31 31 39 30 33 31 34 000Z..3801190314
0300 30 37 5a 30 81 a4 31 81 a1 30 27 06 03 55 04 03 07Z0..1..0'..U..
0310 1e 20 00 6e 00 63 00 61 00 63 00 6e 00 5f 00 6e . .n.c.a.c.n._.n
0320 00 70 00 3a 00 31 00 39 00 32 00 2e 00 31 00 36 .p.:.1.9.2...1.6
0330 00 38 30 31 06 03 55 04 07 1e 2a 00 6e 00 63 00 .801..U...*.n.c.      *nc
0340 61 00 63 00 6e 00 5f 00 6e 00 70 00 3a 00 31 00 a.c.n._.n.p.:.1. acn_np:1
0350 39 00 32 00 2e 00 31 00 36 00 38 00 2e 00 34 00 9.2...1.6.8...4. 92.168.4
0360 35 00 2e 00 36 30 43 06 03 55 04 05 1e 3c 00 31 5...60C..U...<.1 5.
0370 00 42 00 63 00 4b 00 65 00 57 00 35 00 37 00 58 .B.c.K.e.W.5.7.X
0380 00 6c 00 74 00 4e 00 46 00 2f 00 79 00 50 00 6c .l.t.N.F./.y.P.l
0390 00 7a 00 42 00 78 00 5a 00 48 00 5a 00 33 00 34 .z.B.x.Z.H.Z.3.4
03a0 00 43 00 30 00 3d 00 0d 00 0a 30 5c 30 0d 06 09 .C.0.=....0\0...
03b0 2a 86 48 86 f7 0d 01 01 04 05 00 03 4b 00 30 48 *.H.........K.0H       K
03c0 02 41 00 c3 83 df 44 5c 5d 4d a7 d3 6a be f1 08 .A....D\]M..j...
03d0 76 6a c3 fc 62 c3 cd cf f8 6f 7a b6 b8 a2 20 2d vj..b....oz... -
03e0 f0 03 6d d2 ba 39 21 1c ee fd 61 49 d1 61 75 e8 ..m..9!...aI.au.
03f0 e0 a1 05 0e 32 2c 9e 71 7c da 52 20 79 b2 e9 a7 ....2,.q|.R y...
0400 cc fb 15 02 03 01 00 01 a3 82 01 cb 30 82 01 c7 ............0...
0410 30 14 06 09 2b 06 01 04 01 82 37 12 04 01 01 ff 0...+.....7.....
0420 04 04 01 00 05 00 30 3c 06 09 2b 06 01 04 01 82 ......0<..+.....
0430 37 12 02 01 01 ff 04 2c 4d 00 69 00 63 00 72 00 7......,M.i.c.r.     Micr
0440 6f 00 73 00 6f 00 66 00 74 00 20 00 43 00 6f 00 o.s.o.f.t. .C.o. osoft Co
0450 72 00 70 00 6f 00 72 00 61 00 74 00 69 00 6f 00 r.p.o.r.a.t.i.o. rporatio
0460 6e 00 00 00 30 81 cd 06 09 2b 06 01 04 01 82 37 n...0....+.....7 n
0470 12 05 01 01 ff 04 81 bc 00 30 00 00 01 00 00 00 .........0......
0480 02 00 00 00 06 04 00 00 1c 00 4a 00 66 00 4a 00 ..........J.f.J.      JfJ
0490 b0 00 01 00 33 00 64 00 32 00 36 00 37 00 39 00 ....3.d.2.6.7.9.   3d2679
04a0 35 00 34 00 2d 00 65 00 65 00 62 00 37 00 2d 00 5.4.-.e.e.b.7.-. 54-eeb7-
04b0 31 00 31 00 64 00 31 00 2d 00 62 00 39 00 34 00 1.1.d.1.-.b.9.4. 11d1-b94
04c0 65 00 2d 00 30 00 30 00 63 00 30 00 34 00 66 00 e.-.0.0.c.0.4.f. e-00c04f
04d0 61 00 33 00 30 00 38 00 30 00 64 00 00 00 33 00 a.3.0.8.0.d...3. a3080d 3
04e0 64 00 32 00 36 00 37 00 39 00 35 00 34 00 2d 00 d.2.6.7.9.5.4.-. d267954-
04f0 65 00 65 00 62 00 37 00 2d 00 31 00 31 00 64 00 e.e.b.7.-.1.1.d. eeb7-11d
0500 31 00 2d 00 62 00 39 00 34 00 65 00 2d 00 30 00 1.-.b.9.4.e.-.0. 1-b94e-0
0510 30 00 63 00 30 00 34 00 66 00 61 00 33 00 30 00 0.c.0.4.f.a.3.0. 0c04fa30
0520 38 00 30 00 64 00 00 00 00 00 00 10 00 80 d4 00 8.0.d........... 80d
0530 00 00 00 00 30 7e 06 09 2b 06 01 04 01 82 37 12 ....0~..+.....7.
0540 06 01 01 ff 04 6e 00 30 00 00 00 00 0a 00 3a 00 .....n.0......:.        :
0550 50 00 41 00 49 00 4e 00 00 00 36 00 39 00 37 00 P.A.I.N...6.9.7. PAIN 697
0560 31 00 32 00 2d 00 33 00 34 00 37 00 2d 00 37 00 1.2.-.3.4.7.-.7. 12-347-7
0570 35 00 39 00 33 00 36 00 33 00 32 00 2d 00 34 00 5.9.3.6.3.2.-.4. 593632-4
0580 32 00 36 00 33 00 35 00 00 00 53 00 69 00 74 00 2.6.3.5...S.i.t. 2635 Sit
0590 65 00 20 00 4c 00 69 00 63 00 65 00 6e 00 73 00 e. .L.i.c.e.n.s. e Licens
05a0 65 00 20 00 53 00 65 00 72 00 76 00 65 00 72 00 e. .S.e.r.v.e.r. e Server
05b0 00 00 00 00 30 21 06 03 55 1d 23 01 01 ff 04 17 ....0!..U.#.....
05c0 30 15 a1 0c a4 0a 50 00 41 00 49 00 4e 00 00 00 0.....P.A.I.N...    PAIN
05d0 82 05 01 00 00 00 45 30 09 06 05 2b 0e 03 02 1d ......E0...+....
05e0 05 00 03 41 00 9d ee b1 62 5d 88 09 5d e8 b5 b1 ...A....b]..]...
05f0 c6 0a 9e 00 f3 9b cf 0d 8c 96 d9 a4 de a5 ae ed ................
0600 7f 79 90 47 ba 6f 0d 62 cb 90 40 38 a8 a2 29 f1 .y.G.o.b..@8..).
0610 eb f4 74 45 fe 40 5c 7c b0 d1 f1 d7 68 3a fc 0f ..tE.@\|....h:..
0620 46 17 8b 3c 22 00 00 00 00 00 00 00 00 00 00 00 F..<"...........   "
0630 00 00 00 00 00                                  .....
        ''')
    test_ber_roundtrip('ConnectMCSPDU x224_body', ConnectMCSPDU, x224_body, decimal=True)

def per_test():
    print 'ConnectData'

    mstsc_user_data = '\x00\x05\x00\x14|\x00\x01\x816\x00\x08\x00\x10\x00\x01\xc0\x00Duca\x81(\x01\xc0\xd8\x00\x04\x00\x08\x00\x00\x04\x00\x03\x01\xca\x03\xaa\t\x04\x00\x00q\x17\x00\x00D\x00E\x00V\x00-\x00T\x00E\x00S\x00T\x00-\x00D\x004\x001\x000\x00X\x00P\x00\x00\x00\x04\x00\x00\x00\x00\x00\x00\x00\x0c\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\xca\x01\x00\x00\x00\x00\x00\x18\x00\x0f\x00\x0b\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x04\xc0\x0c\x00\r\x00\x00\x00\x00\x00\x00\x00\x02\xc0\x0c\x00\x1b\x00\x00\x00\x00\x00\x00\x00\x03\xc08\x00\x04\x00\x00\x00rdpdr\x00\x00\x00\x00\x00\x80\x80rdpsnd\x00\x00\x00\x00\x00\xc0drdynvc\x00\x00\x00\x80\xc0cliprdr\x00\x00\x00\xa0\xc0'
    test_per_roundtrip('ConnectData mstsc_user_data', ConnectData, mstsc_user_data)
    v = ConnectData.per_decode(mstsc_user_data)
    connectPDU = v['connectPDU']

    test_per_roundtrip('ConnectGCCPDU connectPDU', ConnectGCCPDU, connectPDU)

    rdesk_user_data = '\x00\x05\x00\x14|\x00\x01\x81\x0e\x00\x08\x00\x10\x00\x01\xc0\x00Duca\x81\x00\x01\xc0\xd4\x00\x04\x00\x08\x00 \x03X\x02\x01\xca\x03\xaa\t\x04\x00\x00(\n\x00\x00d\x00e\x00v\x00-\x00m\x00k\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x04\x00\x00\x00\x00\x00\x00\x00\x0c\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\xca\x01\x00\x00\x00\x00\x00\x18\x00\x07\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x04\xc0\x0c\x00\t\x00\x00\x00\x00\x00\x00\x00\x02\xc0\x0c\x00\x03\x00\x00\x00\x00\x00\x00\x00\x03\xc0\x14\x00\x01\x00\x00\x00cliprdr\x00\xc0\xa0\x00\x00'
    test_per_roundtrip('ConnectData rdesk_user_data', ConnectData, rdesk_user_data)

    print
    print
    print

    """
    4.1.14     Client Security Exchange PDU
      The following is an annotated dump of the Security Exchange PDU (section 2.2.1.10).
        00000000   03  00 00 5e  02  f0 80 64 00  06 03 eb 70  50 01 02  ...^...d....pP..
        00000010   00  00 48 00  00  00 91 ac 0c  8f 64 8c 39  f4 e7 ff  ..H.......d.9...
        00000020   0a  3b 79 11  5c  13 51 2a cb  72 8f 9d b7  42 2e f7  .;y.\.Q*.r...B..
        00000030   08  4c 8e ae  55  99 62 d2 81  81 e4 66 c8  05 ea d4  .L..U.b....f....
        00000040   73  06 3f c8  5f  af 2a fd fc  f1 64 b3 3f  0a 15 1d  s.?._.*...d.?...
        00000050   db  2c 10 9d  30  11 00 00 00  00 00 00 00  00        .,..0.........
    """
    mcsSDrq414 = util.to_str([
        #0x03,0x00,0x00,0x5e,0x02,0xf0,0x80,
                                           0x64,0x00,0x06,0x03,0xeb,0x70,0x50,0x01,0x02,
        0x00,0x00,0x48,0x00,0x00,0x00,0x91,0xac,0x0c,0x8f,0x64,0x8c,0x39,0xf4,0xe7,0xff,
        0x0a,0x3b,0x79,0x11,0x5c,0x13,0x51,0x2a,0xcb,0x72,0x8f,0x9d,0xb7,0x42,0x2e,0xf7,
        0x08,0x4c,0x8e,0xae,0x55,0x99,0x62,0xd2,0x81,0x81,0xe4,0x66,0xc8,0x05,0xea,0xd4,
        0x73,0x06,0x3f,0xc8,0x5f,0xaf,0x2a,0xfd,0xfc,0xf1,0x64,0xb3,0x3f,0x0a,0x15,0x1d,
        0xdb,0x2c,0x10,0x9d,0x30,0x11,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
        ])

    test_per_roundtrip('DomainMCSPDU mcsSDrq414', DomainMCSPDU, mcsSDrq414)

    mcsSDrq = 'd\x00\x04\x03\xebp\x80P\x01\x00\x00\x00H\x00\x00\x00Ip\x91\xa3\xd50\x02\xbbc\xef2\xf4\x1d\xe1\xcd\x0b\xe1\xb0\xc0{\xe0<}\xc8H\xb0OvM\x02\x1c\xad~\xf9h7\xf7\x83D\xe59:\xf1!F\x16)\x1b\tm\xdb\xaa\xd8\xe2S1\xa2\x86\xe7\x8e2\x1b\x03\x18\x00\x00\x00\x00\x00\x00\x00\x00'

    test_per_roundtrip('DomainMCSPDU mcsSDrq - note that this is not properly PER encoded', DomainMCSPDU, mcsSDrq)


    # 4.1.15     Client Info PDU,   The following is an annotated dump of the Client Info PDU (section 2.2.1.11).
    mcsSDrq415 = util.to_str([
        #0x03, 0x00, 0x01, 0xab, 0x02, 0xf0, 0x80,
                                                  0x64, 0x00, 0x06, 0x03, 0xeb, 0x70, 0x81, 0x9c, 0x48,
        0x00, 0x00, 0x00, 0x45, 0xca, 0x46, 0xfa, 0x5e, 0xa7, 0xbe, 0xbc, 0x74, 0x21, 0xd3, 0x65, 0xe9,
        0xba, 0x76, 0x12, 0x7c, 0x55, 0x4b, 0x9d, 0x84, 0x3b, 0x3e, 0x07, 0x29, 0x20, 0x73, 0x25, 0x7b,
        0xe6, 0x9a, 0xbb, 0xe8, 0x41, 0x8a, 0xa0, 0x69, 0x3f, 0x26, 0x9a, 0xcd, 0xbc, 0xa6, 0x03, 0x27,
        0xf5, 0xce, 0xbb, 0xa8, 0xc2, 0xff, 0x0f, 0x38, 0xa3, 0xbf, 0x74, 0x81, 0xac, 0xcb, 0xc9, 0x08,
        0x49, 0x0a, 0x43, 0xcf, 0x91, 0x31, 0x36, 0xcd, 0xba, 0x3d, 0x16, 0x4f, 0x11, 0xd7, 0x69, 0x12,
        0xc8, 0xe9, 0x57, 0xc0, 0xb8, 0x0f, 0xc4, 0x72, 0x66, 0x79, 0xbd, 0x86, 0xba, 0x30, 0x60, 0x76,
        0xb4, 0xcd, 0x52, 0x5e, 0x79, 0x8e, 0x88, 0x95, 0xf0, 0x9a, 0x43, 0x20, 0xd9, 0x96, 0x74, 0x1d,
        0x5c, 0x8a, 0x9a, 0xe3, 0x8a, 0x5d, 0xd2, 0x55, 0x17, 0x8c, 0xf2, 0x66, 0x6b, 0x3f, 0x3d, 0x3a,
        0xe3, 0x2a, 0xd4, 0xff, 0xd5, 0x11, 0x30, 0x30, 0xe2, 0xff, 0xe2, 0xe4, 0x11, 0x0c, 0x7f, 0x6a,
        0x1e, 0xa3, 0xf4, 0x2f, 0xdd, 0x4f, 0x89, 0x8c, 0xc0, 0xca, 0xd3, 0x8a, 0x49, 0xd7, 0x00, 0xd9,
        0x09, 0x40, 0xab, 0x79, 0x1a, 0x72, 0xf9, 0x89, 0x42, 0xaf, 0x20, 0xaa, 0x50, 0xc7, 0xcd, 0xd0,
        0xb8, 0x1e, 0xab, 0xd3, 0xeb, 0x10, 0x01, 0x82, 0x68, 0x9f, 0xf5, 0xc9, 0x05, 0xfe, 0x20, 0xbb,
        0x7c, 0x68, 0xb4, 0x72, 0xcd, 0x37, 0x53, 0xdf, 0x43, 0x0a, 0x6d, 0xde, 0xcb, 0xbe, 0x5f, 0x80,
        0x05, 0x1e, 0xb8, 0xf3, 0x5d, 0x04, 0x0c, 0xc6, 0x66, 0x3b, 0x39, 0x5f, 0x5d, 0xa2, 0xda, 0xb9,
        0xea, 0xc9, 0xda, 0xba, 0x7c, 0x9d, 0x4e, 0x4a, 0x4f, 0x4a, 0x16, 0x04, 0xea, 0x4e, 0x23, 0xd3,
        0x6d, 0x2c, 0x2b, 0x42, 0x58, 0x19, 0x69, 0x10, 0x23, 0xd4, 0xe1, 0xaf, 0x46, 0x34, 0xfc, 0x23,
        0x81, 0x59, 0x54, 0x65, 0x5f, 0x6c, 0x67, 0x57, 0x14, 0x62, 0x57, 0x94, 0xf1, 0x81, 0x86, 0x00,
        0xfe, 0x1c, 0x27, 0xf6, 0x76, 0xe2, 0x00, 0xea, 0xc5, 0xf7, 0xb5, 0xe9, 0xb2, 0xad, 0xef, 0x7f,
        0x87, 0x8b, 0x8a, 0xb0, 0xd3, 0x1e, 0x43, 0x54, 0x4b, 0xab, 0xf6, 0xba, 0x7f, 0x5a, 0xb9, 0xe5,
        0x2d, 0x5f, 0x81, 0xab, 0x2a, 0x15, 0xc4, 0x97, 0xbc, 0xd3, 0x92, 0x9a, 0xda, 0xbe, 0x8a, 0xb0,
        0xfb, 0xa4, 0x1a, 0xa0, 0x96, 0x26, 0x86, 0x23, 0x10, 0x1b, 0x21, 0x0a, 0x91, 0x05, 0x22, 0x4d,
        0x6c, 0x4d, 0x01, 0x4c, 0x84, 0xf3, 0x50, 0x56, 0x4f, 0x3a, 0xe4, 0xc0, 0x24, 0xbf, 0x35, 0xf6,
        0xf5, 0x8b, 0x3f, 0x20, 0x55, 0x98, 0x91, 0x05, 0x4d, 0xee, 0x46, 0x95, 0x44, 0x6d, 0x06, 0x33,
        0x42, 0x1f, 0x9f, 0x84, 0x91, 0xe7, 0xc5, 0x9f, 0x04, 0x11, 0xde, 0xcf, 0xa5, 0x07, 0x5f, 0x27,
        0xdd, 0xc0, 0xac, 0xb1, 0xa7, 0x98, 0x9d, 0x6d, 0x79, 0x00, 0x70, 0x33, 0xbf, 0x4e, 0x16, 0x23,
        0x57, 0xf5, 0xc7, 0x88, 0x82, 0xd1, 0xc6, 0xa3, 0xb4, 0x0b, 0x29])

    test_per_roundtrip('DomainMCSPDU mcsSDrq415', DomainMCSPDU, mcsSDrq415)


if __name__ == '__main__':
    util.hook()
    rdp_test()
    per_test()
    test_ber_round()
