import os

import rdp_rsa
import pkcs1
import rdp_common_connection_state


class ClientConnectionState(rdp_common_connection_state.CommonConnectionState):

    def __init__(self, env):
        rdp_common_connection_state.CommonConnectionState.__init__(self, env)

        # On the connection to the client we act MITM as server:

        # Received in Security Exchange PDU
        self._client_random = None # clients own random
        self.PreMasterSecret = None

        self.mitm_pub_key = None # my public key as server for client
        self._mitm_server_random = None # my random as server to client

        self.encryptionLevel = None
        self.encryptionMethodKeyLength = None # key length used towards client

        self.LicenseClientSecret = None # no accessor - direct access

        # Note: rdesktop to sos4 only worked with 2048 bit keys.
        # rdesktop to xrdp requires 512 or 1024 bit keys, but from mstsc only 512 works.
        # How to distinguish? What decides? Reusing the bitlen seems to be the only secure option ...
        #self.client_connection_state.mitm_pub_key = rdp_rsa.terminal_services_signing_key # This one works too ...
        #self.client_connection_state.mitm_pub_key = rdp_rsa.test_key_one_512
        #self.client_connection_state.mitm_pub_key = rdp_rsa.test_key_two_512
        if self._env.config.RSA_PRI_EXP:
            self._env.log1('\tUsing configured MITM pub key')
            self.mitm_pub_key = pkcs1.RsaLong(self._env.config.RSA_MOD, self._env.config.RSA_PUB_EXP, self._env.config.RSA_PRI_EXP)
        else:
            rsa_len = self._env.config.RSA_LEN or 1024
            self._env.log1('\tCreating MITM pub key bitlen %s', rsa_len)
            self.mitm_pub_key = pkcs1.RsaLong.new(rsa_len)
            # Log it so that it can be copied to config
            self._env.log1('\tRSA_MOD = %s', self.mitm_pub_key.modulus)
            self._env.log1('\tRSA_PUB_EXP = %s', self.mitm_pub_key.public_exponent)
            self._env.log1('\tRSA_PRI_EXP = %s', self.mitm_pub_key.private_exponent)
            # Store for reuse for the rest of the lifetime
            self._env.config.RSA_MOD = self.mitm_pub_key.modulus
            self._env.config.RSA_PUB_EXP = self.mitm_pub_key.public_exponent
            self._env.config.RSA_PRI_EXP = self.mitm_pub_key.private_exponent

    def handle_client_security_data(self, encryptionMethods, extEncryptionMethods):
        pass # TODO ...

    def handle_client_security_exchange(self, encryptedClientRandom, encryptionLevel, encryptionMethodKeyLength):
        # http://msdn.microsoft.com/en-us/library/cc240783 5.3.4.2 Decrypting Client Random
        self._client_random = rdp_rsa.le_str_from_long(self.mitm_pub_key.decrypt(
            rdp_rsa.le_str_to_long(encryptedClientRandom)))
        self._env.log3('client_random = decrypted encryptedClientRandom:', dump=self._client_random)

        self.encryptionLevel = encryptionLevel
        self.encryptionMethodKeyLength = encryptionMethodKeyLength
        self._env.log1('client-side encryptionLevel: %s, encryptionMethodKeyLength: %s', self.encryptionLevel, self.encryptionMethodKeyLength)

        self._env.log1('Initializing Client-side Initial Session Key')
        self.generate_session_keys(
            self._client_random, self._mitm_server_random, self.encryptionMethodKeyLength)

    def get_mitm_server_random(self):
        assert not self._mitm_server_random, self._mitm_server_random
        self._mitm_server_random = self._env.config.RANDOM_SERVER_MITM_CLIENTSIDE or os.urandom(32)
        self._env.log3('generated mitm_server_random:', dump=self._mitm_server_random)
        return self._mitm_server_random

    def handle_EncryptedPreMasterSecret(self, EncryptedPreMasterSecret):
        # This blob contains a encrypted 48-byte random number. For instructions on how to encrypt this random number, see section 5.1.2.1.
        # http://msdn.microsoft.com/en-us/library/cc241991 5.1.2.2 Decrypting the Premaster Secret
        # http://msdn.microsoft.com/en-us/library/cc241969 3.3.1.6 Encrypted Premaster Secret
        self.PreMasterSecret = rdp_rsa.le_str_from_long(
            self.mitm_pub_key.decrypt(rdp_rsa.le_str_to_long(EncryptedPreMasterSecret)),
            length=48)
