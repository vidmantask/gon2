"""
X.224 Class 0 as specified in [X224] section 13 (Indirectly through MSDN rdp documentation)
"""
# FIXME: Where is this standard?

import util

CONNECTION_STRUCT = util.PackedStruct('<BBHHB')

def parse_request(x224_chunk):
    """
    Check X.224 Class 0 Connection Request header and return payload
    """
    # x224Crq (7 bytes): An X.224 Class 0 Connection Request transport protocol data unit (TPDU), as specified in [X224] section 13.3.
    # x224 13.3 Connection Request (CR) TPDU
    length, tpdu_code, dst_ref, _src_ref, class_, x224_body = CONNECTION_STRUCT.unpack(x224_chunk)
    assert length == 6 + len(x224_body), (length, x224_body) # 13.3.5: no userdata in class 0
    assert tpdu_code == 0xe0, tpdu_code # CR-TPDU in class 0
    assert dst_ref == 0, dst_ref
    #assert src_ref == 0 # usually, but not necessarily
    assert class_ == 0, class_ # rdp says class 0
    return x224_body # this is parameters, not userdata

def encode_request(x224_body):
    """
    """
    length = 6 + len(x224_body) # this is parameters, not userdata
    tpdu_code = 0xe0
    dst_ref = 0
    src_ref = 0
    class_ = 0
    x224_chunk = CONNECTION_STRUCT.pack(length, tpdu_code, dst_ref, src_ref, class_) + x224_body
    return x224_chunk

def parse_confirm(x224_chunk):
    """
    Check X.224 Class 0 Connection Confirm header and return payload
    """
    # x224Ccf (7 bytes): An X.224 Class 0 Connection Confirm TPDU, as specified in [X224] section 13.4.
    # x224 13.4 Connection Confirm (CC) TPDU
    length, tpdu_code, _dst_ref, _src_ref, class_, x224_body = CONNECTION_STRUCT.unpack(x224_chunk)
    assert length == 6 + len(x224_body), (length, x224_body) # 13.4.5: no userdata in class 0
    assert tpdu_code == 0xd0, tpdu_code # CC-TPDU in class 0
    #assert dst_ref == 0, dst_ref # usually, but not necessarily
    # src_ref is often 0x3412, but 0 for xrdp ...
    assert class_ == 0, class_ # rdp says class 0
    return x224_body # this is parameters, not userdata

def encode_confirm(x224_body):
    """
    """
    length = 6 + len(x224_body) # this is parameters, not userdata
    tpdu_code = 0xd0
    dst_ref = 0
    src_ref = 0x3412 # A very common value TODO: use the right value instead?
    class_ = 0
    x224_chunk = CONNECTION_STRUCT.pack(length, tpdu_code, dst_ref, src_ref, class_) + x224_body
    return x224_chunk

# x224 13.7.1 DT, Normal format for class 0
DATA_STRUCT = util.PackedStruct('<BBB')

def parse_data(x224_chunk):
    """
    Check X.224 Class 0 Data TPDU header and return payload
    """
    # An X.224 Class 0 Data TPDU, as specified in [X224] section 13.7.
    # 13.7 Data (DT) TPDU
    length, tpdu_code, eot_nr, x224_body = DATA_STRUCT.unpack(x224_chunk)
    #print 'X.224 header (', 'body length:', len(x224_body), ')'
    if length == 2:
        assert tpdu_code == 0xf0, hex(tpdu_code) # apparently no ROA in class 0
        assert eot_nr == 0x80, eot_nr # assuming EOT set. TPDU-NR is 0 in class 0.
        return x224_body

    # oh - it was probably an x224 13.5 Disconnect Request (DR) TPDU
    length, tpdu_code, _dst_ref, _src_ref, _reason, x224_body = CONNECTION_STRUCT.unpack(x224_chunk)
    assert length == 6, length
    assert tpdu_code == 0x80, tpdu_code
    return None # we ignore this - TODO

def encode_data(x224_body):
    """
    """
    length = 2
    tpdu_code = 0xf0
    eot_nr = 0x80
    x224_chunk = DATA_STRUCT.pack(length, tpdu_code, eot_nr) + x224_body
    return x224_chunk


def test():
    from pprint import pprint
    a = 'foo'
    print 'Request, Encoding %r ...' % a
    x224_chunk = encode_request(a)
    print 'Request, Decoding:'; util.dump(x224_chunk)
    x224_body = parse_request(x224_chunk)
    print 'Request, Decoded:'; util.dump(x224_body)
    assert x224_body == a, (x224_body, a)

    print 'Confirm, Encoding %r ...' % a
    x224_chunk = encode_confirm(a)
    print 'Confirm, Decoding:'; util.dump(x224_chunk)
    x224_body = parse_confirm(x224_chunk)
    print 'Confirm, Decoded:'; util.dump(x224_body)
    assert x224_body == a, (x224_body, a)

    print 'Data, Encoding %r ...' % a
    x224_chunk = encode_data(a)
    print 'Data, Decoding:'; util.dump(x224_chunk)
    x224_body = parse_data(x224_chunk)
    print 'Data, Decoded:'; util.dump(x224_body)
    assert x224_body == a, (x224_body, a)


if __name__ == '__main__':
    util.hook()
    test()
