"""
Simple parser for ASN.1
Based on:
    http://www.itu.int/ITU-T/studygroups/com17/languages/X.690-0207.pdf X.690-0207 ITU-T Recommendation X.690 Information technology
    - ASN.1 encoding rules: Specification of Basic Encoding Rules (BER), Canonical Encoding Rules (CER) and Distinguished Encoding Rules (DER)
    http://www.itu.int/ITU-T/studygroups/com17/languages/X.691-0207.pdf ITU-T Recommendation X.691 Information technology
    - ASN.1 encoding rules: Specification of Packed Encoding Rules (PER)
    http://www.itu.int/ITU-T/studygroups/com17/languages/X.680-X.693-0207w.zip ITU-T X.690 / ISO/IEC 8825-1
    http://en.wikipedia.org/wiki/Basic_Encoding_Rules
    http://luca.ntop.org/Teaching/Appunti/asn1.html = ftp://ftp.rsa.com/pub/pkcs/ascii/layman.asc
    http://www.oss.com/asn1/dubuisson.html
    http://www.oss.com/asn1/larmouth.html
Other:
    http://www.obj-sys.com/asn1tutorial/asn1only.html
    http://lionet.info/asn1c/
    http://pyasn1.sourceforge.net/
    http://harmony.apache.org/subcomponents/classlibrary/asn1_framework.html

http://erlang.org/pipermail/erlang-questions/2000-April/001156.html
    Since then the standards has grown and the whole area around ASN.1 is very big
    with Information Object Classes,
    Parameterization, Constraints, PER etc. and it is very hard to support
    everything in those standards. It is also difficult
    to make a choice of what is most important to support next. Therefore my
    current strategy is to implement new things
    based on what my customers need and based upon the actual ASN.1 specifications
    that they want to implement contains.

    I have realized that it is necessary to support ANY and other things from the
    old 1988 standard at the same time as
    the new stuff because there are quite many specifications based on that
    standard that are still interesting.

See http://www.mozilla.org/projects/security/pki/nss/tech-notes/tn1.html for NS take on this
"""

WARNINGS = False # monkey-patch this to enable warnings

# Tracing control constants
TRACE_DER_EN = 0
TRACE_BER_DE = 0
TRACE_PER_EN = 0
TRACE_PER_DE = 0
TRACE_TLV = 0

import util
import oids


def parse_GeneralizedTime(s, utctime=False):
    # Type GeneralizedTime takes values of the year, month, day, hour, time, minute,second, and second fraction in any of three forms.
    # 1. Local time only. ''YYYYMMDDHHMMSS.fff'', where the optional fff is accurate to three decimal places.
    # 2. Universal time (UTC time) only. ''YYMMDDHHMMSS.fffZ''.
    # 3. Difference between local and UTC times. ''YYYYMMDDHHMMSS.fff+-HHMM''.
    if s.endswith('Z'):
        tz = s[-1]
        s = s[:-1]
    elif s[-5] in '+-':
        tz = s[-5:]
        s = s[:-5]
    else:
        tz = None
    if '.' in s:
        s, dotsec = s.split('.', 1)
    else:
        dotsec = None
    assert len(s) == 12 if utctime else 14, s
    secs = s[-2:]
    mins = s[-4:-2]
    hours = s[-6:-4]
    date = s[-8:-6]
    month = s[-10:-8]
    year = s[:-10]
    return '%s-%s-%s %s:%s:%s %s%s' % (year, month, date, hours, mins, secs, ('.' + dotsec) if dotsec else '', tz or '')


# Definition

CLASS_UNIVERSAL = 0 # same in all applications; these types are only defined in X.208
CLASS_APPLICATION = 1 # specific to an application
CLASS_CONTEXT = 2 # specific to a given structured type; context-specific tags are used to distinguish between component types with the same underlying tag within the context of a given structured type
CLASS_PRIVATE = 3 # specific to a given enterprise
CLASS_DESC = {
    CLASS_UNIVERSAL: 'UNIVERSAL',
    CLASS_APPLICATION: 'APPLICATION',
    CLASS_CONTEXT: 'CONTEXT',
    CLASS_PRIVATE: 'PRIVATE'}

def APPLICATION(tag_id):
    return CLASS_APPLICATION, tag_id

def UNIVERSAL(tag_id):
    return CLASS_UNIVERSAL, tag_id

def CONTEXT(tag_id):
    return CLASS_CONTEXT, tag_id

def PRIVATE(tag_id):
    return CLASS_PRIVATE, tag_id


# PER utilities

@util.tracer(TRACE_PER_DE)
def per_int_from_bits(bit_count, per_string, per_string_octet_pos, per_string_bit_pos):
    """
    Return integer from bit_count bigendian bits starting at per_string/per_string_octet_pos/per_string_bit_pos
    bit_count indicates how many bits should be read to the int, 1..inf
    """
    #print 'eating', bit_count, 'bits at', per_string_octet_pos, per_string_bit_pos,
    if per_string_bit_pos:
        bit_value = ord(per_string[per_string_octet_pos]) & (0xff >> per_string_bit_pos)
        if per_string_bit_pos + bit_count < 8:
            # we don't want 8-per_string_bit_pos bits, but only bit_count bits
            return bit_value >> (8 - per_string_bit_pos - bit_count), per_string_octet_pos, per_string_bit_pos + bit_count
        # We want all the 8-per_string_bit_pos bits
        bit_count -= 8 - per_string_bit_pos
        per_string_octet_pos += 1
        if not bit_count: # We are done
            return bit_value, per_string_octet_pos, 0
    else:
        bit_value = 0
    # yeah, byte aligned - don't care about per_string_bit_pos
    while bit_count >= 8:
        bit_value = (bit_value << 8) | ord(per_string[per_string_octet_pos])
        bit_count -= 8
        per_string_octet_pos += 1
    if bit_count: # need bigendian part of next byte
        bit_value = (bit_value << bit_count) | (ord(per_string[per_string_octet_pos]) >> (8 - bit_count))
        return bit_value, per_string_octet_pos, bit_count
    return bit_value, per_string_octet_pos, 0

@util.tracer(TRACE_PER_DE)
def per_eat_pad_bits(per_string, per_string_octet_pos, per_string_bit_pos):
    """
    Eat and ignore padding bits so that per_string_bit_pos == 0
    """
    if per_string_bit_pos:
        #print 'eating padding:',
        bit_value, per_string_octet_pos, per_string_bit_pos = per_int_from_bits(8 - per_string_bit_pos, per_string, per_string_octet_pos, per_string_bit_pos)
        assert not bit_value, hex(bit_value) # assume zero padding
    assert not per_string_bit_pos, per_string_bit_pos
    return per_string_octet_pos, per_string_bit_pos

@util.tracer(TRACE_PER_EN)
def per_bits_to_octets(per_octet_list, per_bit_list):
    """
    All bits from per_bit_list are eaten and appended to per_octet_list as octets.
    Both per_octet_list and per_bit_list are modified.
    Octets are padded with zero if not enough bits available.
    """
    per_bit_list.extend([False] * (8 - ((len(per_bit_list) % 8) or 8))) # pad
    assert not (len(per_bit_list) % 8), per_bit_list
    octet = 0
    for i, bit in enumerate(per_bit_list):
        if bit:
            octet |= (0x80 >> (i % 8))
        if (i % 8) == 7:
            per_octet_list.append(chr(octet))
            octet = 0
    assert not octet, octet
    del per_bit_list[:]
    assert not per_bit_list, per_bit_list

@util.tracer(TRACE_PER_EN)
def per_int_to_bits(per_bit_list, int_value, number_of_bits):
    """
    An int_value is returned to a list with number_of_bits bits.
    Used when integer with known range is encoded un-aligned.
    """
    l = []
    while int_value:
        l.append(bool(int_value & 1))
        int_value >>= 1
    per_bit_list.extend([False] * (number_of_bits - len(l)))
    per_bit_list.extend(reversed(l))

@util.tracer(TRACE_PER_DE)
def per_size_from_octets(per_string, per_string_octet_pos, per_string_bit_pos):
    """Byte-aligns if necessary and then reads size"""
    per_string_octet_pos, per_string_bit_pos = per_eat_pad_bits(per_string, per_string_octet_pos, per_string_bit_pos)
    #print 'eating size at', per_string_octet_pos, per_string_bit_pos, ':',
    size = ord(per_string[per_string_octet_pos])
    if size & 0x80 == 0:
        per_string_octet_pos += 1
    elif size & 0xc0 == 0x80:
        size = ((ord(per_string[per_string_octet_pos]) & 0x3f) << 8) | ord(per_string[per_string_octet_pos + 1])
        per_string_octet_pos += 2
        if size < 0x80:
            if WARNINGS: print "read_per_size warning: PER size 0x%x was encoded in 2 bytes - you can't do that, that isn't canonical!" % size
    else: # chunks
        m = size & 0x3f
        assert m and m <= 4, m
        raise NotImplementedError # chunked encoding not implemented

    #print size
    return size, per_string_octet_pos, per_string_bit_pos

@util.tracer(TRACE_PER_EN)
def per_size_to_octets(per_octet_list, per_bit_list, size_val):
    """
    Encodes size_val as aligned octets.
    Both per_octet_list and per_bit_list are modified.
    """
    per_bits_to_octets(per_octet_list, per_bit_list)
    if size_val < 0x80:
        per_octet_list.append(chr(size_val))
    elif size_val < 0x4000:
        per_octet_list.append(chr((size_val >> 8) | 0x80))
        per_octet_list.append(chr(size_val & 0xff))
    else: # TODO: This requires use of chunks ...
        raise NotImplementedError


# BER utilities

def guess_value_type(t_val, tag_constructed):
    """
    Guess value type based on ASN.1 standard/guidelines for tags
    """
    (tag_class, tag_id) = t_val
    desc = '0x%02x=%s:' % (tag_id, CLASS_DESC[tag_class])

    if tag_class == CLASS_UNIVERSAL:

        for value_type_class in [
            # Universal 0 is reserved for BER
            BOOLEAN,
            INTEGER,
            BIT_STRING, # an arbitrary string of bits (ones and zeroes).
            OCTET_STRING, # an arbitrary string of octets (eight-bit values).
            NULL,
            OBJECT_IDENTIFIER, # an object identifier, which is a sequence of integer components that identify an object such as an algorithm or attribute type.
            # ODESC = OBJECT DESCRIPTOR = ObjectDescripter
            # EXT = EXTERNAL = INSTANCE OF
            # REAL
            ENUMERATED,
            # EMBEDDED PDV
            UnicodeString, # = UTF8String
            # RELATIVE-OID
            # Universal 14 is reserved
            # Universal 15 is reserved
            SEQUENCE, # SEQUENCE OF, Structured type, an ordered collection of one or more types. ... OF may be empty
            SET, # SET OF, Structured type, an unordered collection of one or more types. ... OF may be empty
            NumericString, # [0-9 ], encodable in 4 bits
            PrintableString, # an arbitrary string of printable characters, for example you can't encode an '@'
            T61String, # = TeletexString, an arbitrary string of T.61 (X.680) (eight-bit) characters, can select different character sets using escape codes
            VideotexString, # ???
            IA5String, # an arbitrary string of IA5 (ASCII) characters, basically 7-bit ASCII (including all the control codes), but with the dollar sign potentially replaced with a "currency symbol"
            UTCTime, # a "coordinated universal time" or Greenwich Mean Time (GMT) value. y2k buggy!
            GeneralizedTime, # ???
            GraphicString, # ???
            VisibleString, # = ISO646String, IA5String without the control codes
            GeneralString, # ???
            UniversalString, # UCS-4 big-endian
            CHARACTER_STRING, # ???
            BMPString, # UCS-2 big-endian
            #HighTagNumberForm, # Universal 31 reserved?
            ]:
            #print tag_type.tag_id, t_val, tag_type.tag_id == tag_id
            if value_type_class.tag_id == tag_id:
                # FIXME: Check constructued?
                return desc + value_type_class.__name__, value_type_class()
        #print 'universal', t_val, 'not found'
        return desc + 'Unknown', None

    if tag_constructed:
        return desc + 'UnknownConstructed', Constructed()
    else:
        return desc + 'UnknownSimple', Simple()

@util.tracer(TRACE_TLV)
def decode_ber_tlv(ber_string, ber_string_pos):
    """
    Read some TLV from ber_string starting at index ber_string_pos,
    decode with T (t_val), whether constructed, eaten length, and v_val (V of length L).
    Return None and actual length if not enough available.
    """
    error_value = (None, None, len(ber_string) - ber_string_pos, buffer(ber_string, ber_string_pos)) # Eat and return rest of ber - including "T" and "L"
    if len(ber_string) <= ber_string_pos:
        print "tlv_from_ber: Can't read tag from %r" % ber_string
        return error_value

    #Split BER tag byte to class, constructed flag and id.
    tag_1st_octet = ord(ber_string[ber_string_pos])
    tag_class = tag_1st_octet >> 6
    assert tag_class in CLASS_DESC.keys(), (tag_class, CLASS_DESC)
    tag_constructed = bool(tag_1st_octet & 0x20)
    tag_id = tag_1st_octet & 0x1f
    t_len = 1
    if tag_id == 0x1f:
        # High-tag-number form. Two or more octets. First octet is as in low-tag-number form, except that bits 5-1 all have value "1."
        # Second and following octets give the tag number, base 128, most significant digit first, with as few digits as possible,
        # and with the bit 8 of each octet except the last set to "1."
        tag_id = 0
        for c in buffer(ber_string, ber_string_pos + 1):
            octet = ord(c)
            tag_id = (tag_id << 7) + (octet & 0x7f)
            t_len += 1
            if octet & 0x80 == 0:
                break
        else: # ber exhausted without break
            print "tlv_from_ber: Can't read whole tag from %r" % ber_string
            return error_value
        if tag_id < 0x1f:
            if WARNINGS: print 'tlv_from_ber warning: Found Tag id 0x%x illegally "BER" encoded in %s bytes' % (tag_id, t_len)
    t_val = (tag_class, tag_id)

    if t_val == (0, 0): # FIXME: is hack needed?
        print 'tlv_from_ber: Read zero tag hack'
        return (t_val, False, len(ber_string) - ber_string_pos, buffer(ber_string, ber_string_pos)) # FIXME: how much to eat?

    if len(ber_string) - ber_string_pos < t_len + 1:
        print "tlv_from_ber: Can't read length from %r" % ber_string
        return error_value
    len_1st_octet = ord(ber_string[ber_string_pos + t_len])
    if len_1st_octet < 0x80: # ASN.1 BER/DER Length coding in 1 Byte
        l_len = 1
        v_len = len_1st_octet
    elif len_1st_octet == 0x80:
        print 'tlv_from_ber: Constructed, indefinite-length method not implemented' # Runs until End-of-contents octets. Two octets, 00 00.
        raise NotImplementedError
    else: # ASN.1 BER/DER Length coding in >1 Byte
        l_len = (len_1st_octet & 0x7f) + 1
        if len(ber_string) - ber_string_pos <= t_len + l_len: # FIXME: why = ?
            print "tlv_from_ber: Can't read whole length from %r" % ber_string
            return error_value
        v_len = 0
        for i in range(1, l_len):
            v_len = (v_len << 8) + ord(ber_string[ber_string_pos + t_len + i])
        if v_len < 0x80:
            if WARNINGS: print 'tlv_from_ber warning: Found Length 0x%x illegally "BER" encoded in %s bytes' % (v_len, l_len)
    v_val = buffer(ber_string, ber_string_pos + t_len + l_len, v_len)
    #print 't=%s/%s/%02x' % t_val, 'l=%04x' % expected_l, 'ber=%r' % value_seq, ber
    if len(v_val) != v_len:
        print "tlv_from_ber: L said V of length %s but got V of length %s" % (v_len, len(v_val))
        print 'Guess:', guess_value_type(t_val, tag_constructed) # FIXME: Why guess here???
        return error_value
    return (t_val, tag_constructed, t_len + l_len + v_len, v_val)

@util.tracer(TRACE_TLV)
def encode_der_tlv(t_val, tag_constructed, v_val):
    """
    Make DER TLV.
    T octets are determined from t_val and tag_constructed and encoded,
    L octets are determined from V and encoded,
    V octets are already encoded in v_val
    """
    result = [] # pieces of string
    (tag_class, tag_id) = t_val
    tag_1st_octet = tag_class << 6
    if tag_constructed:
        tag_1st_octet |= 0x20
    if tag_id < 0x1f:
        # normal encoding in 5 lower bits
        result.append(chr(tag_1st_octet | tag_id))
    else:
        # High-tag-number form. Two or more octets. First octet is as in low-tag-number form, except that bits 5-1 all have value "1."
        # Second and following octets give the tag number, base 128, most significant digit first, with as few digits as possible,
        # and with the bit 8 of each octet except the last set to "1."
        result.append(chr(tag_1st_octet | 0x1f))
        tag_seq = []
        while tag_id:
            tag_seq.append(chr(tag_id & 0x7f | (0x80 if tag_seq else 0)))
            tag_id >>= 7
        result.extend(reversed(tag_seq))

    v_len = len(v_val)
    if v_len < 0x7f: # FIXME
        result.extend(chr(v_len))
    else: # ASN.1 BER/DER Length coding in >1 Byte
        len_seq = []
        l = v_len
        while l:
            len_seq.append(chr(l & 0xff))
            l >>= 8
        result.append(chr(len(len_seq) | 0x80))
        result.extend(reversed(len_seq))

    result.extend(v_val)
    return ''.join(result)


# Spec types

class TAG_MISMATCH(object):
    """
    Flag returned as value when a value of the expected type wasn't found.
    Used to handle optional sequence elements and choices.
    """


class SpecTypeBase(object):
    """
    Whatever is common for all kind of specs
    """

    def __repr__(self):
        return '<%s>' % self.typename()

    # duck typing as something looking like ValueTypeBase, usable in Tags
    def typename(self):
        return self.__class__.__name__

    @util.tracer(TRACE_BER_DE)
    def ber_decode(self, ber_string):
        """
        Main entry point for decoding a Value of this type - without Tag and Length
        """
        raise NotImplementedError

    @util.tracer(TRACE_PER_DE)
    def per_decode(self, per_string, return_index=False):
        """
        Main entry point for per decoding
        Assumes whole string is eaten - unless return_index ...
        """
        per_string_octet_pos, per_string_bit_pos, value = self.per_decode_from(per_string, 0, 0)
        per_string_octet_pos, per_string_bit_pos = per_eat_pad_bits(per_string, per_string_octet_pos, per_string_bit_pos)
        if return_index: # ugly hack - wouldn't be needed if RDP was sane
            return (value, per_string_octet_pos)
        else:
            assert per_string_octet_pos == len(per_string), (per_string_octet_pos, len(per_string))
            return value

    def der_encode(self, value):
        raise NotImplementedError

    def per_encode(self, value_struct):
        """
        Main entry point for encoding of value struct
        """
        per_octet_list = [] # list of strings, in whatever chunks seems most efficient
        per_bit_list = [] # bits pending for per_octet_list
        self.per_encode_to(per_octet_list, per_bit_list, value_struct)
        per_bits_to_octets(per_octet_list, per_bit_list)
        return ''.join(per_octet_list)

    # Module internal:

    def per_decode_from(self, per_string, per_string_octet_pos, per_string_bit_pos):
        """
        Decode value struct from per, continuing from current position
        """
        raise NotImplementedError

    def per_encode_to(self, per_octet_list, per_bit_list, value):
        """
        Encode value by appending to per_octet_list and per_bit_list
        """
        raise NotImplementedError

    def __call__(self):
        """
        Derived types are created by instantiation of base classes, and this allows extra layers of id instantiation
        TODO: if any parameters specified then create copy of self and modify it with new parameters, allowing multilevel constraints.
        """
        return self


class TaggingTypeBase(SpecTypeBase):
    """
    Whatever is common for Tag and Choice ;-)
    """

    @util.tracer(TRACE_BER_DE)
    def ber_decode(self, ber_string):
        t_val, t_constructed, eaten, v_val = decode_ber_tlv(ber_string, 0)
        assert t_val, ("can't read TLV", t_val, ber_string)
        value = self.ber_decode_tv(t_val, t_constructed, v_val)
        assert value is not TAG_MISMATCH, (self, t_val, v_val)
        if eaten != len(ber_string):
            print self, 'ber_decode: Unused Extra:', ber_string[eaten:]
            raise RuntimeError('ber_decode overload')
        return value

    @util.tracer(TRACE_DER_EN)
    def der_encode(self, value):
        return self.der_encode_tlv(value) # TLV

    # Module internal:

    @util.tracer(TRACE_BER_DE)
    def ber_decode_tv(self, t_val, t_constructed, v_val): # Takes both T and V!
        raise NotImplementedError

    def per_decode_from(self, per_string, per_string_octet_pos, per_string_bit_pos):
        raise NotImplementedError # abstract implementation of abstract method

    def der_encode_tlv(self, value):
        raise NotImplementedError

    def per_encode_to(self, per_octet_list, per_bit_list, value):
        raise NotImplementedError


class ValueTypeBase(SpecTypeBase):

    @util.tracer(TRACE_BER_DE)
    def ber_decode(self, ber_string):
        # Just skip T and L - just decode V
        value = self.ber_decode_v(ber_string)
        assert value is not TAG_MISMATCH, (self, ber_string)
        return value

    @util.tracer(TRACE_DER_EN)
    def der_encode(self, value):
        v_val = self.der_encode_v(value) # V without TL
        return v_val

    # Module internal:

    # tag_id has no default, we prefer an access error instead
    tag_constructed = False # not used for decoding - informational and used for encoding

    def ber_decode_v(self, v_val):
        raise NotImplementedError

    def per_decode_from(self, per_string, per_string_octet_pos, per_string_bit_pos):
        raise NotImplementedError # abstract implementation of abstract method

    def der_encode_v(self, value):
        raise NotImplementedError

    def per_encode_to(self, per_octet_list, per_bit_list, value):
        raise NotImplementedError


class Tag(object):
    """
    Tag a type before it becomes a field
    """

    def __init__(self, value_type, implicit=None, explicit=None, optional=None, per_index=None, default=None):
        """
        implicit/explicit are a (decomposed) T value, the V is stored under name
        """
        self.value_type = value_type
        self.implicit = implicit
        self.explicit = explicit
        self.optional = optional
        self.per_index = per_index
        self.default = default

    def __call__(self, *args, **kwargs):
        return self.__class__(self.value_type(*args, **kwargs),
                implicit=self.implicit, explicit=self.explicit, optional=self.optional, per_index=self.per_index, default=self.default)

def _merge(a, b):
    """return the non-None of a and b"""
    if a is None:
        return b
    if b is None:
        return a
    raise Exception('collision between %r and %r' % (a, b))

class Field(TaggingTypeBase):
    """
    Tag wrapper for TLV attribute types
    """

    def __init__(self, name, value_type, implicit=None, explicit=None, optional=None, per_index=None, default=None):
        """
        implicit/explicit are a (decomposed) T value, the V is stored under name
        """
        TaggingTypeBase.__init__(self)
        while isinstance(value_type, Tag):
            implicit = _merge(implicit, value_type.implicit)
            explicit = _merge(explicit, value_type.explicit)
            optional = _merge(optional, value_type.optional)
            per_index = _merge(per_index, value_type.per_index)
            default = _merge(default, value_type.default)
            value_type = value_type.value_type
        self.name = name
        if isinstance(value_type, type):
            value_type = value_type() # instantiate if uninstantiated
        self.value_type = value_type
        self.implicit = implicit
        self.explicit = explicit
        self.optional = optional
        self.per_index = per_index
        self.default = default

        assert self.name is None or isinstance(self.name, basestring), self.name
        assert (
            isinstance(self.value_type, ValueTypeBase) or
            isinstance(self.value_type, TaggingTypeBase) or # CHOICE or ANY
            isinstance(self.value_type, ANY_DEFINED_BY)
            ), self.value_type # can't tag values of this type

    def __repr__(self):
        return '<Tag %r %s %s%s>' % (
            self.name, self.value_type,
            ('%s' % (self.implicit,)) if self.implicit else 'UNIV', # FIXME: Seems strange ...
            (' %s' % (self.per_index,)) if self.per_index else '',
            )

    @util.tracer(TRACE_BER_DE)
    def ber_decode_tv(self, t_val, t_constructed, v_val): # Takes both T and V!
        """
        Process T and V from TLV-triple: check T, and recursively decode V and return value.
        """
        assert t_val, t_val # other (more "real") types can ignore type and just go for the value - Tag can't ...
        #print self, 'with', self._tag or (self.value_type, self.value_type.tag_id), 'analyzing', t_val, 'with', v_val
        if self.explicit:
            #print 'testing', t_val, 'for explicit tag', self.explicit
            if t_val != self.explicit:
                #print 'mismatch', t_val, self.explicit
                return TAG_MISMATCH
            assert t_constructed, t_constructed # explicit tagging _is_ constructed
            t_val_2, t_constructed_2, eaten, v_val_2 = decode_ber_tlv(v_val, 0)
            assert t_val, ("can't read TLV", t_val_2, v_val)
            assert eaten == len(v_val), (eaten, v_val)
            # we already found the old t_val, now hunt expect the inner tag instead
            t_val = t_val_2
            t_constructed = t_constructed_2
            v_val = v_val_2
        if isinstance(self.value_type, TaggingTypeBase):
            assert not self.implicit, self.implicit # FIXME: Can non-"explicit" tags be combined with CHOICE? guess not ...
            value = self.value_type.ber_decode_tv(t_val, t_constructed, v_val)
        else:
            # FIXME: Can "explicit" and "explicit" tags be combined?
            spec_t_val = self.implicit or (CLASS_UNIVERSAL, self.value_type.tag_id)
            if t_val != spec_t_val:
                #print 'mismatch', t_val, spec_t_val
                return TAG_MISMATCH
            value = self.value_type.ber_decode_v(v_val)
        return value

    def per_decode_from(self, per_string, per_string_octet_pos, per_string_bit_pos):
        # Tags are not encoded in PER
        (per_string_octet_pos, per_string_bit_pos, value) = self.value_type.per_decode_from(per_string, per_string_octet_pos, per_string_bit_pos)
        return (per_string_octet_pos, per_string_bit_pos, value)

    def per_encode_to(self, per_octet_list, per_bit_list, value):
        # Tags are not encoded in PER
        self.value_type.per_encode_to(per_octet_list, per_bit_list, value)

    @util.tracer(TRACE_DER_EN)
    def der_encode_tlv(self, value):
        if isinstance(self.value_type, TaggingTypeBase): # CHOICE already encoded t_val - several layers away
            tlv_der = self.value_type.der_encode_tlv(value)
        else:
            v_val = self.value_type.der_encode_v(value)
            #print self, 'with', self.implicit or (self.value_type, self.value_type.tag_id), 'saw', self.value_type, 'encoding', repr(value), 'as'; util.dump(v_val)
            if self.implicit:
                t_val = self.implicit
            else:
                t_val = (CLASS_UNIVERSAL, self.value_type.tag_id) # use default
            tlv_der = encode_der_tlv(t_val, self.value_type.tag_constructed, v_val)
        if self.explicit:
            tlv_der = encode_der_tlv(self.explicit, True, tlv_der)
        return tlv_der


class Unknown(ValueTypeBase):
    """
    Unknown TLV types
    """

    @util.tracer(TRACE_BER_DE)
    def ber_decode_v(self, v_val):
        raise NotImplementedError

    def per_decode_from(self, per_string, per_string_octet_pos, per_string_bit_pos):
        raise NotImplementedError

    def der_encode_v(self, value):
        raise NotImplementedError

    def per_encode_to(self, per_octet_list, per_bit_list, value):
        raise NotImplementedError


class BOOLEAN(ValueTypeBase):
    """
    """
    tag_id = 0x01

    @util.tracer(TRACE_BER_DE)
    def ber_decode_v(self, v_val):
        assert len(v_val) == 1, v_val
        return bool(v_val[0])

    @util.tracer(TRACE_PER_DE)
    def per_decode_from(self, per_string, per_string_octet_pos, per_string_bit_pos):
        bit_value, per_string_octet_pos, per_string_bit_pos = per_int_from_bits(1, per_string, per_string_octet_pos, per_string_bit_pos)
        value = bool(bit_value)
        return per_string_octet_pos, per_string_bit_pos, value

    @util.tracer(TRACE_DER_EN)
    def der_encode_v(self, value):
        return '\xff' if value else '\0'

    @util.tracer(TRACE_PER_EN)
    def per_encode_to(self, per_octet_list, per_bit_list, value):
        per_bit_list.append(value)


class NULL(ValueTypeBase):
    """
    """
    tag_id = 0x05

    @util.tracer(TRACE_BER_DE)
    def ber_decode_v(self, v_val):
        assert not v_val, v_val
        return None

    @util.tracer(TRACE_PER_DE)
    def per_decode_from(self, per_string, per_string_octet_pos, per_string_bit_pos):
        return per_string_octet_pos, per_string_bit_pos, None

    @util.tracer(TRACE_DER_EN)
    def der_encode_v(self, value):
        assert value is None, value
        return ''

    def per_encode_to(self, per_octet_list, per_bit_list, value):
        raise NotImplementedError


class INTEGER(ValueTypeBase):
    """
    """
    tag_id = 0x02

    def __init__(self, values=None, min_value=0, per_bits=None):
        ValueTypeBase.__init__(self)
        self.values = values or {} # name to value
        assert isinstance(self.values, dict), self.values
        self.value_names = dict((v, k) for k, v in self.values.items()) # value to name
        self.min_value = min_value # only used for PER
        self.per_bits = per_bits # because of the complex rules in X.691-0207 10.3-10.8 it _must_ be specified explicitly

    @util.tracer(TRACE_BER_DE)
    def ber_decode_v(self, v_val):
        assert v_val, repr(v_val) # can't be zero length
        value = 0
        for c in v_val:
            value = (value << 8) + ord(c)
        #print 'int', v_val, value
        v_val_0 = ord(v_val[0])
        if v_val_0 & 0x80: # negative sign
            value -= 1 << (len(v_val) * 8)
        if len(v_val) > 1:
            if v_val_0 == 0 and ord(v_val[1]) & 0x80 == 0:
                if WARNINGS: print self, "ber_decode_v warning: BER Integer 0x%x was encoded in %s bytes with unnecessary leading zero - you can't do that, that isn't canonical!" % (value, len(v_val))
            if v_val_0 == 0xff and ord(v_val[1]) & 0x80 > 0:
                if WARNINGS: print self, "ber_decode_v warning: BER Integer -0x%x was encoded in %s bytes with unnecessary sign extension - you can't do that, that isn't canonical!" % (-value, len(v_val))
        return self.value_names.get(value, value)

    @util.tracer(TRACE_PER_DE)
    def per_decode_from(self, per_string, per_string_octet_pos, per_string_bit_pos):
        assert self.per_bits == 16, self.per_bits # FIXME: assuming two-byte encoding
        per_string_octet_pos, per_string_bit_pos = per_eat_pad_bits(per_string, per_string_octet_pos, per_string_bit_pos)
        value = (ord(per_string[per_string_octet_pos]) << 8) + ord(per_string[per_string_octet_pos + 1]) + self.min_value
        per_string_octet_pos += 2
        return per_string_octet_pos, per_string_bit_pos, self.value_names.get(value, value)

    @util.tracer(TRACE_DER_EN)
    def der_encode_v(self, value):
        value = self.values.get(value, value)
        v_val = []
        if value >= 0:
            while value:
                v_val.append(chr(value & 0xff))
                value >>= 8
            if not v_val or (ord(v_val[-1]) & 0x80): # MSB set, or 0 must be encoded as something
                #print self, 'der_encode_v: Adding leading 0 to Integer encoding in order to be positive'
                v_val.append('\0') # make sure sign indicator in MSB not set (note that bytelist is reversed)
        else:
            while value != -1:
                v_val.append(chr(value & 0xff))
                value >>= 8
            if not v_val or not(ord(v_val[-1]) & 0x80): # MSB not set, or -1 must be encoded as something
                #print self, 'der_encode_v: Adding leading 0xff to Integer encoding in order to be negative'
                v_val.append('\xff') # make sure sign indicator in MSB is set (note that bytelist is reversed)
        return ''.join(reversed(v_val))

    @util.tracer(TRACE_PER_EN)
    def per_encode_to(self, per_octet_list, per_bit_list, value):
        value = self.values.get(value, value)
        assert value >= 0, value # FIXME
        assert self.per_bits == 16, self.per_bits # FIXME: assuming two-byte encoding
        int_value = value - self.min_value
        per_bits_to_octets(per_octet_list, per_bit_list)
        per_octet_list.append(chr(int_value >> 8))
        per_octet_list.append(chr(int_value & 0xff))


class ENUMERATED(ValueTypeBase):
    """
    """
    # TODO: Handle negative values
    tag_id = 0x0a

    def __init__(self, values=None, extension=False):
        ValueTypeBase.__init__(self)
        self.extension = extension
        self.enum_values = values or {} # name to value
        assert isinstance(self.enum_values, dict), self.enum_values
        self.enum_names = dict((v, k) for k, v in self.enum_values.items()) # value to name
        # PER doesn't encode values but their index in a sorted list
        self.per_values = sorted(self.enum_names.keys()) # PER index to value
        self.per_indexes = dict((v, i) for i, v in enumerate(self.per_values)) # value to PER index in per_values
        self.per_bits = 0
        if self.per_values:
            c = len(self.per_values) - 1
            while c:
                self.per_bits += 1
                c >>= 1

    @util.tracer(TRACE_BER_DE)
    def ber_decode_v(self, v_val):
        i = 0
        for c in v_val:
            i = (i << 8) + ord(c)
        return self.enum_names.get(i, 'enum %s' % i)

    @util.tracer(TRACE_PER_DE)
    def per_decode_from(self, per_string, per_string_octet_pos, per_string_bit_pos):
        if self.extension:
            bit_value, per_string_octet_pos, per_string_bit_pos = per_int_from_bits(1, per_string, per_string_octet_pos, per_string_bit_pos)
            #print 'extension for', self, bool(bit_value)
            assert not bit_value, ("don't know how to handle extensions ...", bit_value)
        bit_value, per_string_octet_pos, per_string_bit_pos = per_int_from_bits(self.per_bits, per_string, per_string_octet_pos, per_string_bit_pos)
        i = self.per_values[bit_value]
        return per_string_octet_pos, per_string_bit_pos, self.enum_names.get(i, 'enum %s' % i)

    @util.tracer(TRACE_DER_EN)
    def der_encode_v(self, value):
        int_value = self.enum_values[value]
        v_val = []
        while int_value or not v_val: # at least one byte
            v_val.append(chr(int_value & 0xff))
            int_value >>= 8
        return ''.join(reversed(v_val))

    @util.tracer(TRACE_PER_EN)
    def per_encode_to(self, per_octet_list, per_bit_list, value):
        if self.extension:
            per_bit_list.append(False) # FIXME: assume extension not set

        int_value = self.enum_values[value]
        per_index = self.per_indexes[int_value] # FIXME
        per_int_to_bits(per_bit_list, int_value, self.per_bits)


class BIT_STRING(ValueTypeBase): # aka bitstring
    """
    Bit String
    Named bits ... but x509 uses it for certificates which are octets ...
    """
    tag_id = 0x03

    def __init__(self, values=None, octets=None):
        # FIXME: this is only for named bits!
        ValueTypeBase.__init__(self)
        self.bit_values = values or {}
        assert isinstance(self.bit_values, dict), self.bit_values
        self.bit_names = dict((v, k) for k, v in self.bit_values.items()) # value to name
        self.octets = octets
        if self.octets is None and not values: # hack because x509 encode some octet strings as bit strings ...
            self.octets = True # octets not disabled and no names - so we show as octets FIXME: bad when guessing ...
        assert not (values and self.octets), (values, self.octets)
        # PER requires fixed size
        self.per_bits = len(self.bit_values)
        assert self.per_bits == (max(self.bit_names) + 1 if self.bit_names else 0), (self.per_bits, self.bit_names)

    @util.tracer(TRACE_BER_DE)
    def ber_decode_v(self, v_val):
        unused_bits = ord(v_val[0])
        if self.octets and not unused_bits: # can't if not octet-aligned
            return ('octets', v_val[1:]) # _excludes_ zero octet!
        result = set()
        used_bits = 8 * (len(v_val) - 1) - unused_bits
        for i in xrange(used_bits):
            if ord(v_val[(i >> 3) + 1]) & (0x80 >> (i & 7)):
                result.add(self.bit_names.get(i, 'bit %s' % i))
        return result

    @util.tracer(TRACE_PER_DE)
    def per_decode_from(self, per_string, per_string_octet_pos, per_string_bit_pos):
        if self.octets:
            raise NotImplementedError
        bit_value, per_string_octet_pos, per_string_bit_pos = per_int_from_bits(self.per_bits, per_string, per_string_octet_pos, per_string_bit_pos)
        value = set()
        for i in xrange(self.per_bits):
            if (1 << (self.per_bits - 1 - i)) & bit_value:
                value.add(self.bit_names.get(i, 'bit %s' % i))
        return per_string_octet_pos, per_string_bit_pos, value

    @util.tracer(TRACE_DER_EN)
    def der_encode_v(self, value):
        if self.octets and value and isinstance(value, tuple) and value[0] == 'octets':
            assert len(value) == 2, value
            return '\0' + value[1]

        remaining_flag_names = set(value)
        octets = []
        flag_value = 0
        while True:
            octet = 0
            for i in range(8):
                name = self.bit_names.get(flag_value) or ('bit %s' % i)
                if name in remaining_flag_names:
                    octet |= 0x80 >> i
                    remaining_flag_names.remove(name)
                if not remaining_flag_names:
                    return chr(7 - i) + ''.join(chr(b) for b in octets) + chr(octet)
                flag_value += 1
            octets.append(octet)
        raise RuntimeError # impossible

    @util.tracer(TRACE_PER_EN)
    def per_encode_to(self, per_octet_list, per_bit_list, value):
        if self.octets:
            raise NotImplementedError

        flags = set(value)
        for i in xrange(self.per_bits):
            bit_name = self.bit_names.get(i, 'bit %s' % i)
            per_bit_list.append(bit_name in flags)
            flags.discard(bit_name)
        assert not flags, flags


class CommonString(ValueTypeBase):
    """
    Binary safe
    """

    def __init__(self, min_size=0):
        ValueTypeBase.__init__(self)
        self.min_size = min_size # Only used for PER

    def _common_decode(self, octet_string):
        #assert isinstance(octet_string, str), octet_string
        return octet_string

    def _common_encode(self, s):
        return s

    @util.tracer(TRACE_BER_DE)
    def ber_decode_v(self, v_val):
        return self._common_decode(str(v_val))

    @util.tracer(TRACE_PER_DE)
    def per_decode_from(self, per_string, per_string_octet_pos, per_string_bit_pos):
        # FIXME: consider number of bits used for encoding ...
        size, per_string_octet_pos, per_string_bit_pos = per_size_from_octets(per_string, per_string_octet_pos, per_string_bit_pos)
        size += self.min_size
        value = self._common_decode(str(per_string[per_string_octet_pos : per_string_octet_pos + size]))
        per_string_octet_pos += size
        return per_string_octet_pos, per_string_bit_pos, value

    @util.tracer(TRACE_DER_EN)
    def der_encode_v(self, value):
        return self._common_encode(value)

    @util.tracer(TRACE_PER_EN)
    def per_encode_to(self, per_octet_list, per_bit_list, value):
        per_size_to_octets(per_octet_list, per_bit_list, len(value) - self.min_size)
        per_octet_list.append(self._common_encode(value))


class CommonTime(CommonString): pass # pretend that the times are related ...

class UTCTime(CommonTime):
    tag_id = 0x17 # for example '991018013625Z'
    # Like GeneralizedTime but no fff and optional SS
class GeneralizedTime(CommonTime):
    tag_id = 0x18


class OCTET_STRING(CommonString): tag_id = 0x04
class PrintableString(CommonString): tag_id = 0x13 # ASCII A, B, ..., Z, a, b, ..., z, 0, 1, ..., 9, (space) ' ( ) + , - . / : = ?
class VisibleString(CommonString): tag_id = 0x1a
ISO646String = VisibleString
class IA5String(CommonString): tag_id = 0x16 # ASCII
class T61String(CommonString): tag_id = 0x14 # ASCII extended to 8 bit with strange escape codes
TeletexString = T61String
class VideotexString(CommonString): tag_id = 0x15
class GraphicString(CommonString): tag_id = 0x19
class GeneralString(CommonString): tag_id = 0x1b
class UniversalString(CommonString): tag_id = 0x1c # 32 bit values - unicode, utf-32?
class CHARACTER_STRING(CommonString): tag_id = 0x1d


class BMPString(CommonString):
    """
    String, 16-bit encoded, we assume it is UTF-16 big-endian Unicode
    """

    tag_id = 0x1e

    def _common_decode(self, octet_string):
        try:
            return octet_string.decode('utf_16_be')
        except UnicodeDecodeError:
            raise Exception('UnicodeError decoding %r' % (octet_string))

    def _common_encode(self, s):
        return s.encode('utf_16_be')


class UnicodeString(CommonString):
    """
    Unicode string, we assume it is UTF-8
    """
    tag_id = 0x0c

    def _common_decode(self, octet_string):
        try:
            return octet_string.decode('utf-8')
        except UnicodeDecodeError:
            raise Exception('UnicodeError decoding %r' % (octet_string))

    def _common_encode(self, s):
        return s.encode('utf-8')

UTF8String = UnicodeString


class NumericString(CommonString):
    """
    String, but only encodes 0-9 and space, which PER fits into 4 bits - but it starts aligned
    """
    tag_id = 0x12

    # FIXME: Decoded values are currently just simple octet-expansion of the nibbles. Should map correctly.

    @util.tracer(TRACE_PER_DE)
    def per_decode_from(self, per_string, per_string_octet_pos, per_string_bit_pos):
        size, per_string_octet_pos, per_string_bit_pos = per_size_from_octets(per_string, per_string_octet_pos, per_string_bit_pos)
        res = []
        for i in range(size + self.min_size):
            bit_value, per_string_octet_pos, per_string_bit_pos = per_int_from_bits(4, per_string, per_string_octet_pos, per_string_bit_pos)
            res.append(chr(bit_value))
        value = ''.join(res)
        return per_string_octet_pos, per_string_bit_pos, value

    @util.tracer(TRACE_PER_EN)
    def per_encode_to(self, per_octet_list, per_bit_list, value):
        per_size_to_octets(per_octet_list, per_bit_list, len(value) - self.min_size)
        for nibble_char in value:
            nibble_val = ord(nibble_char)
            per_bit_list.extend([nibble_val & 8, nibble_val & 4, nibble_val & 2, nibble_val & 1]) # FIXME: Could be implemented more efficient because it is nibble-aligned


class OBJECT_IDENTIFIER(ValueTypeBase):
    """
    Apparently an OID path ...
    """
    tag_id = 0x06

    def __init__(self):
        ValueTypeBase.__init__(self)

    def _common_decode(self, octet_string):
        assert octet_string, ('Invalid ObjectIdentifier; empty ...', octet_string)
        l = [ord(octet_string[0]) // 40, ord(octet_string[0]) % 40] # decimal 40!
        tmp = 0
        for c in buffer(octet_string, 1):
            byte = ord(c)
            tmp = (tmp << 7) + (byte & 0x7f)
            if byte & 0x80 == 0:
                l.append(tmp)
                tmp = 0
        assert not tmp, 'Invalid ObjectIdentifier; bit 8 in last byte %02x set' % octet_string[-1]
        return (tuple(l), oids.OIDS.str(l))

    def _common_encode(self, value):
        oid = value[0]
        v_val = [chr(oid[0] * 40 + oid[1])]
        for i in oid[2:]:
            tmp = []
            while i or not tmp: # at least one
                tmp.append(chr(i & 0x7f | (0x80 if tmp else 0)))
                i >>= 7
            v_val.extend(reversed(tmp))
        return ''.join(v_val)

    @util.tracer(TRACE_BER_DE)
    def ber_decode_v(self, v_val):
        return self._common_decode(v_val)

    @util.tracer(TRACE_PER_DE)
    def per_decode_from(self, per_string, per_string_octet_pos, per_string_bit_pos):
        size, per_string_octet_pos, per_string_bit_pos = per_size_from_octets(per_string, per_string_octet_pos, per_string_bit_pos)
        value = self._common_decode(buffer(per_string, per_string_octet_pos, size))
        per_string_octet_pos += size
        return per_string_octet_pos, per_string_bit_pos, value

    @util.tracer(TRACE_DER_EN)
    def der_encode_v(self, value):
        # ((1, 3, 14, 3, 2, 29), 'sha1WithRSAEncryption-obsolete(iso(1).identified-organization(3).oiw(14).secsig(3).algorithms(2).sha1WithRSAEncryption-obsolete(29))')
        # => [43, 14, 3, 2, 29]
        return self._common_encode(value)

    @util.tracer(TRACE_PER_EN)
    def per_encode_to(self, per_octet_list, per_bit_list, value):
        encoded_oid = self._common_encode(value)
        per_size_to_octets(per_octet_list, per_bit_list, len(encoded_oid))
        per_octet_list.append(encoded_oid)


class SEQUENCE(ValueTypeBase):
    """
    Untagged container for an exact sequence of tagged values
    """

    tag_id = 0x10
    tag_constructed = True

    def __init__(self, *args, **kwargs):
        ValueTypeBase.__init__(self)
        self.extension = kwargs.pop('extension', False)
        self.as_dict = kwargs.pop('as_dict', True)
        self.guess = kwargs.pop('guess', not args) # FIXME: is this guess guessing good?
        assert not kwargs, 'Unknown kwargs %s' % kwargs
        self.tag_seq = args

    @util.tracer(TRACE_BER_DE)
    def ber_decode_v(self, v_val):
        """Extra data will be returned with '' key"""
        res = []
        v_val_index = 0
        for tag in self.tag_seq:
            #print self, 'looking for', tag
            if v_val_index == len(v_val):
                value = TAG_MISMATCH # no more v_val data, so was wasn't matched
            else:
                elem_t_val, elem_t_constructed, eaten, elem_v_val = decode_ber_tlv(v_val, v_val_index)
                #print self, 'eat got', elem_t_val, eaten, repr(str(elem_v_val)), 'from', repr(v_val[v_val_index:v_val_index+eaten])
                assert elem_t_val, ("can't read TLV", elem_t_val, v_val, v_val_index)
                assert isinstance(tag, Field), tag # all sequence members must be tagged
                if isinstance(tag.value_type, ANY_DEFINED_BY):
                    #print 'any defined by', tag.value_type, self.tag_seq
                    new_tag = None
                    for (i, ref_spec) in enumerate(self.tag_seq): # TODO: No linear search ...
                        if ref_spec.name == tag.value_type.defining_tag_name:
                            value_field_name, value_field_value = res[i]
                            selected_type = oids.OIDS.get_selection(value_field_value[0])
                            #print 'found', tag.value_type.defining_tag_name, 'at', i, 'as', ref_spec, 'with', value_field_value[0], 'selecting', selected_type
                            assert selected_type, 'OID %s from field %r doesn\'t select any type' % (value_field_value, value_field_name)
                            #spec = Field('%s->%s=%s' % (ref_spec.name, ref_spec.name, oids.OIDS.str(res[i][1][0])), selected_type)
                            new_tag = Field(tag.name, selected_type, implicit=tag.implicit, explicit=tag.explicit, optional=tag.optional) # replace tag_seq tag with "runtime" one
                            break
                    assert new_tag, (ref_spec.name, self.tag_seq)
                    tag = new_tag
                    assert not isinstance(tag.value_type, ANY_DEFINED_BY), tag.value_type # referenced field found and defined a real tag?
                    #print self, 'now looking for', tag, 'in', repr(str(elem_v_val))
                value = tag.ber_decode_tv(elem_t_val, elem_t_constructed, elem_v_val)
            if value is TAG_MISMATCH: # no more v_val data or v_val mismatch - no matter what: no v_val data has been used
                if tag.default is not None:
                    #print 'defaultable %s not found - using default %s' % (spec, spec.default)
                    res.append((tag.name, tag.default)) # default value is included in result
                elif tag.optional:
                    #print 'optional %s not found - skipping' % (spec)
                    pass
                else: # mandatory tag not found
                    print self, 'ber_decode_v: mandatory %s not found in %r - stopping sequence' % (tag, v_val[v_val_index:])
                    if self.guess:
                        break
                    raise RuntimeError('%s missing field %s' % (self, tag))
            else:
                #print self, 'matched', spec, 'using', eaten
                res.append((tag.name, value))
                v_val_index += eaten

        if self.guess:
            # help for rev eng; guess BER types and make up tag names:
            i = 0
            while v_val_index < len(v_val):
                elem_t_val, elem_t_constructed, eaten, elem_v_val = decode_ber_tlv(v_val, v_val_index)
                if elem_t_val is None:
                    print self, "ber_decode_v: can't guess TLV"
                    break # can't read TLV
                desc, value_type = guess_value_type(elem_t_val, elem_t_constructed)
                if value_type is None:
                    print self, "ber_decode_v: can't guess type of", elem_t_val
                    break # can't guess type
                value = value_type.ber_decode_v(elem_v_val)
                res.append(('%s|%s' % (i, desc), value))
                v_val_index += eaten
                i += 1

        if v_val_index < len(v_val):
            print self, 'decoding value with too many octets - adding them to result'
            res.append(('', v_val[v_val_index:]))
            #import pdb;pdb.set_trace()
        if self.as_dict:
            res_dict = dict(res)
            assert len(res_dict) == len(res), 'Duplicate tags found when converting %s to dict' % res
            return res_dict
        return res

    @util.tracer(TRACE_PER_DE)
    def per_decode_from(self, per_string, per_string_octet_pos, per_string_bit_pos):
        if self.extension:
            bit_value, per_string_octet_pos, per_string_bit_pos = per_int_from_bits(1, per_string, per_string_octet_pos, per_string_bit_pos)
            #print 'extension for', self, bool(bit_value)
            assert not bit_value, ("don't know how to handle extensions ...", bit_value)
        presents = set()
        for tag in self.tag_seq:
            if tag.optional:
                is_present, per_string_octet_pos, per_string_bit_pos = per_int_from_bits(1, per_string, per_string_octet_pos, per_string_bit_pos)
                #print 'optional', spec, 'present:', is_present
                if is_present:
                    presents.add(tag)
        value = []
        for tag in self.tag_seq:
            assert per_string_octet_pos < len(per_string), (per_string_octet_pos, per_string)
            if tag.optional and tag not in presents:
                #print 'skipping non-present optional', spec
                continue
            #print 'handling', spec, 'at', per_string_octet_pos, per_string_bit_pos
            #print self, 'now parsing', spec, 'with', spec.value_type, 'per_string_octet_pos', per_string_octet_pos, per_string_bit_pos
            per_string_octet_pos, per_string_bit_pos, item_value = tag.per_decode_from(per_string, per_string_octet_pos, per_string_bit_pos) # no tagging needed when no optional
            #print 'handling', spec, 'as', repr(value)
            assert item_value is not None, item_value
            value.append((tag.name, item_value))
            #print res
        if self.as_dict:
            res_dict = dict(value)
            assert len(res_dict) == len(value), 'Duplicate tags found when converting %s to dict' % value
            return (per_string_octet_pos, per_string_bit_pos, res_dict)
        return per_string_octet_pos, per_string_bit_pos, value

    @util.tracer(TRACE_DER_EN)
    def der_encode_v(self, value):
        values = dict(value) # assume k-v-tupple-list or dict
        handled_values = dict(value) # copy
        v_val = []
        for tag in self.tag_seq:
            #print self, 'handling', spec
            assert isinstance(tag, Field), tag # all sequence members must be tagged
            if tag.name in values:
                elem_value = values.pop(tag.name)

                if isinstance(tag.value_type, ANY_DEFINED_BY):
                    #print 'defined from', tag.value_type.defining_tag_name
                    controlling_value = handled_values[tag.value_type.defining_tag_name]
                    #print 'controlling value', controlling_value
                    oid = controlling_value[0]
                    #print 'oid', oid
                    selected_type = oids.OIDS.get_selection(oid)
                    #print 'selected', selected_type
                    assert selected_type, (selected_type, oid)
                    #print 'value', repr(elem_value)
                    tag = Field(tag.name, selected_type, implicit=tag.implicit, explicit=tag.explicit, optional=tag.optional) # replace tag_seq tag with "runtime" one

                if (tag.default is not None) and elem_value == tag.default:
                    #print 'NOT encoding default value %s' % elem_value
                    pass # just skip defaults
                else:
                    elem_der = tag.der_encode_tlv(elem_value) # includes tlv
                    v_val.extend(elem_der)
            elif not tag.optional:
                raise RuntimeError('mandatory tag %s not found in values %s' % (tag, values))
        assert not values, values # all used?
        return ''.join(v_val)

    @util.tracer(TRACE_PER_EN)
    def per_encode_to(self, per_octet_list, per_bit_list, value):
        seq_values = dict(value)
        if self.extension:
            # FIXME: assumes extension not set
            per_bit_list.append(False)

        for tag in self.tag_seq:
            if tag.optional: # or default???
                per_bit_list.append(tag.name in seq_values)
            elif tag.default is not None:
                per_bit_list.append(seq_values[tag.name] != tag.default)
                raise NotImplementedError # never tested ...

        for tag in self.tag_seq:
            if tag.name in seq_values:
                # FIXME: If present
                tag_value = seq_values.pop(tag.name)
                if (tag.default is None) or tag_value != tag.default:
                    tag.per_encode_to(per_octet_list, per_bit_list, tag_value)
                #else: print self, 'per_encode_to: NOT encoding default value %s' % tag_value # That is FYI - not a warning

        assert not seq_values, seq_values


class SEQUENCE_OF(ValueTypeBase):
    """
    Wrapper around untagged value type
    """

    tag_id = 0x10
    tag_constructed = True

    def __init__(self, untagged_value_type, min_size=0):
        ValueTypeBase.__init__(self)
        self.untagged_value_type = untagged_value_type # FIXME: Couldn't it have explicit tags?
        self.untagged_value_class = CLASS_UNIVERSAL # FIXME: Can't other classes be used?
        self.min_size = min_size # Only used for BER

    @util.tracer(TRACE_BER_DE)
    def ber_decode_v(self, v_val):
        res = []
        v_val_index = 0
        while v_val_index < len(v_val):
            #print self, self.untagged_value_type, 'handling', v_val[v_val_index:]
            elem_t_val, elem_t_constructed, eaten, elem_v_val = decode_ber_tlv(v_val, v_val_index)
            #print self, 'eat got', value_tag, 'from', v_val[v_val_index:v_val_index+eaten]
            assert elem_t_val, ("can't read TLV", elem_t_val, v_val, v_val_index)
            if isinstance(self.untagged_value_type, TaggingTypeBase):
                value = self.untagged_value_type.ber_decode_tv(elem_t_val, elem_t_constructed, elem_v_val)
            else:
                assert elem_t_val == (self.untagged_value_class, self.untagged_value_type.tag_id), (elem_t_val, self.untagged_value_type.tag_id) # FIXME: That's it?
                value = self.untagged_value_type.ber_decode_v(elem_v_val)
            if value is TAG_MISMATCH:
                print self, 'ber_decode_v: expected %s not found, got %s %s' % (self.untagged_value_type, elem_t_val, elem_v_val)
                raise RuntimeError('Sequence error')
            else:
                #print self, 'matched', spec
                res.append(value) # no tag name
            v_val_index += eaten
        assert v_val_index == len(v_val), (v_val_index, v_val)
        return res

    @util.tracer(TRACE_PER_DE)
    def per_decode_from(self, per_string, per_string_octet_pos, per_string_bit_pos):
        size, per_string_octet_pos, per_string_bit_pos = per_size_from_octets(per_string, per_string_octet_pos, per_string_bit_pos)
        value = []
        for i in range(size + self.min_size):
            #print self, 'handling', i
            per_string_octet_pos, per_string_bit_pos, item_value = self.untagged_value_type.per_decode_from(per_string, per_string_octet_pos, per_string_bit_pos)
            #print self, 'handling', i, 'as', value
            value.append(item_value)
        return per_string_octet_pos, per_string_bit_pos, value

    @util.tracer(TRACE_DER_EN)
    def der_encode_v(self, value):
        v_val = []
        if isinstance(self.untagged_value_type, TaggingTypeBase):
            for elem_value in value:
                tlv_der = self.untagged_value_type.der_encode_tlv(elem_value)
                v_val.extend(tlv_der)
        else:
            t_val = (self.untagged_value_class, self.untagged_value_type.tag_id) # the same for all elements
            for elem_value in value:
                elem_v_val = self.untagged_value_type.der_encode_v(elem_value)
                #print self, 'saw', self.untagged_value_type, 'encoding', repr(elem_value), 'as'; util.dump(elem_v_val)
                tlv_der = encode_der_tlv(t_val, self.untagged_value_type.tag_constructed, elem_v_val)
                v_val.extend(tlv_der)
        return ''.join(v_val)

    @util.tracer(TRACE_PER_EN)
    def per_encode_to(self, per_octet_list, per_bit_list, value):
        per_size_to_octets(per_octet_list, per_bit_list, len(value) - self.min_size)

        for item_value in value:
            self.untagged_value_type.per_encode_to(per_octet_list, per_bit_list, item_value)


class SET(SEQUENCE): # FIXME: similar to sequence, but not the same ...

    tag_id = 0x11
    tag_constructed = True


class SET_OF(SEQUENCE_OF): # FIXME: similar to sequence_of, but not the same ...

    tag_id = 0x11
    tag_constructed = True


class CHOICE(TaggingTypeBase):
    """
    Untagged container for an exact sequence of tagged values
    """

    def __init__(self, *args, **kwargs):
        TaggingTypeBase.__init__(self)
        self.per_bits = kwargs.pop('per_bits', None) # FIXME: calculate size, like for enums
        self.extension = kwargs.pop('extension', False)
        assert not kwargs, 'Unknown kwargs %s' % kwargs
        self.tag_seq = args

    @util.tracer(TRACE_BER_DE)
    def ber_decode_tv(self, t_val, t_constructed, v_val): # Takes both T and V!
        assert t_val, t_val # other (more "real") types can ignore type and just go for the value - CHOICE can't ...
        #print self, 'choosing for', t_val
        for tag in self.tag_seq:
            #print self, 'testing', spec, spec.tag_id
            value = tag.ber_decode_tv(t_val, t_constructed, v_val)
            if value is TAG_MISMATCH:
                continue
            #print 'found', spec
            return (tag.name, value)
        return TAG_MISMATCH

    @util.tracer(TRACE_PER_DE)
    def per_decode_from(self, per_string, per_string_octet_pos, per_string_bit_pos):
        if self.extension:
            bit_value, per_string_octet_pos, per_string_bit_pos = per_int_from_bits(1, per_string, per_string_octet_pos, per_string_bit_pos)
            #print 'extension for', self, bool(bit_value)
            assert not bit_value, ("don't know how to handle extensions ...", bit_value)
        index, per_string_octet_pos, per_string_bit_pos = per_int_from_bits(self.per_bits, per_string, per_string_octet_pos, per_string_bit_pos)
        #print self, 'choosing for', index,
        for tag in self.tag_seq:
            # print 'checking', spec, spec.per_index
            assert tag.per_index is not None, "can't choose among %s with no index" % tag
            if tag.per_index == index:
                #print 'found', spec
                per_string_octet_pos, per_string_bit_pos, item_value = tag.per_decode_from(per_string, per_string_octet_pos, per_string_bit_pos)
                value = (tag.name, item_value)
                return per_string_octet_pos, per_string_bit_pos, value
        assert False, 'nothing chosen for %s' % index

    @util.tracer(TRACE_DER_EN)
    def der_encode_tlv(self, value_tuple):
        spec_name, value = value_tuple
        assert isinstance(spec_name, str)
        for tag in self.tag_seq: # TODO: Optimize with dict-lookup instead of linear search
            #print self, 'handling', spec
            if tag.name == spec_name:
                tlv_der = tag.der_encode_tlv(value)
                return tlv_der
        assert False, value_tuple

    @util.tracer(TRACE_PER_EN)
    def per_encode_to(self, per_octet_list, per_bit_list, value):
        chosen_name, chosen_value = value
        if self.extension:
            # FIXME: assumes extension not set
            per_bit_list.append(False)

        for tag in self.tag_seq:
            if tag.name == chosen_name:
                per_int_to_bits(per_bit_list, tag.per_index, self.per_bits)
                tag.per_encode_to(per_octet_list, per_bit_list, chosen_value)
                return
        raise RuntimeError('Choice not found')


class Simple(CommonString):
    """
    Not universal, not Constructed
    """

    tag_id = 0


class Constructed(SEQUENCE):
    """
    Not universal, not Simple
    """

    tag_id = 0
    tag_constructed = True

    def __init__(self, *args, **kwargs):
        kwargs['guess'] = True # FIXME - we use sequence to get BER decoding of a number of TLVs
        SEQUENCE.__init__(self, *args, **kwargs)


class ANY_DEFINED_BY(object):
    """
    A place-holder that on runtime should be replaced by some other type.
    Must appear in sequence - a bit like Tag
    References an earlier field in the sequence.
    The referenced field must have a type where its values selects a type
    """

    def __init__(self, defining_tag_name):
        self.defining_tag_name = defining_tag_name


class ANY(TaggingTypeBase):
    """
    Usually a kind of under-specified ANY_DEFINED_BY, acting more like an ignorant CHOICE.
    Deprecated.
    """

    def __init__(self):
        TaggingTypeBase.__init__(self)

    @util.tracer(TRACE_BER_DE)
    def ber_decode_tv(self, t_val, t_constructed, v_val): # Takes both T and V!
        return t_val, t_constructed, str(v_val)

    @util.tracer(TRACE_PER_DE)
    def per_decode_from(self, per_string, per_string_octet_pos, per_string_bit_pos):
        raise NotImplementedError

    @util.tracer(TRACE_DER_EN)
    def der_encode_tlv(self, value_tuple):
        t_val, t_constructed, v_val = value_tuple
        tlv_der = encode_der_tlv(t_val, t_constructed, v_val)
        return tlv_der

    @util.tracer(TRACE_PER_EN)
    def per_encode_to(self, per_octet_list, per_bit_list, value):
        raise NotImplementedError


# Try to dump structure from type info in value
Guess = Field('GUESSED', SEQUENCE(guess=True))


# 7816-4 table 12
FileControlParameter = (
    Field('fcp',
        SEQUENCE(
            Field('size', INTEGER(), CONTEXT(0)),
            Field('descriptor', INTEGER(), CONTEXT(2)),
            ),
        APPLICATION(2))
    )

# What is FileControlInformation? Almost same as FCP?
integer_context_0 = Tag(INTEGER(), CONTEXT(0)) # defining context when it is unknown is crazy - APPLICATION makes a bit more sense
FileControlInformation = (
    Field('fci',
        SEQUENCE(
            Field('size', integer_context_0),
            Field('descriptor', INTEGER(), CONTEXT(2)),
            ),
        APPLICATION(0xf))
    )

def test_ber_roundtrip(desc, spec, ber_string, oneway=False, decimal=False):
    from pprint import pprint
    print
    print 'Testing roundtrip from BER:', desc; util.dump(ber_string, decimal=decimal)
    print 'Initial decoding...'
    value = spec.ber_decode(ber_string)
    print 'value:'
    pprint(value)
    if oneway:
        return
    print 'Encoding again to complete roundtrip ...'
    ber2 = spec.der_encode(value)
    if ber2 == ber_string:
        print 'BER-value-BER roundtrip is PERFECT!'
    else:
        print "BER-value-BER roundtrip got lost - got:"; util.dump(ber2, decimal=decimal)
        print 'Decoding again to try to find roundtrip fix point ...'
        value2 = spec.ber_decode(ber2)
        if value2 == value:
            print "Value-BER-value roundtrip OK - either an implementation error or the initial BER wasn't canonical DER!"
        else:
            print 'Roundtrip really failed - no fixpoint found:'
            pprint(value2)
            #import pdb;pdb.set_trace()
            raise RuntimeError('No fixpoint')
    print

def test_per_roundtrip(desc, spec, per_string, oneway=False, decimal=False):
    from pprint import pprint
    print
    print 'Testing roundtrip from PER:', desc; util.dump(per_string, decimal=decimal)
    print 'Initial decoding...'
    value = spec.per_decode(per_string)
    print 'value:'
    pprint(value)
    if oneway:
        return
    print 'Encoding again to complete roundtrip ...'
    per2 = spec.per_encode(value)
    if per2 == per_string:
        print 'PER-value-PER roundtrip OK'
    else:
        print "PER-value-PER roundtrip got lost - got:"; util.dump(per2, decimal=decimal)
        print 'Decoding again to try to find roundtrip fix point ...'
        value2 = spec.per_decode(per2)
        if value2 == value:
            print "Value-PER-value roundtrip OK - either an implementation error or the initial PER just wasn't canonical!!!?"
        else:
            print 'Roundtrip really failed - no fixpoint found:'
            pprint(value2)
            #import pdb;pdb.set_trace()
            raise RuntimeError('No fixpoint')
    print

def simple_demo():
    from pprint import pprint
    print 'FCP'
    fcp_enc = util.to_str([98, 7, 128, 2, 1, 44, 130, 1, 1])
    test_ber_roundtrip('FileControlParameter fcp_enc', FileControlParameter, fcp_enc)

    print
    print 'FCI'
    fci_enc = util.to_str([0x6f, 0x07, 0x80, 0x02, 0x00, 0x75, 0x82, 0x01, 0x01]) # note: 117=0x75 encoded in two bytes - not DER!
    test_ber_roundtrip('FileControlInformation fci_enc', FileControlInformation, fci_enc)

    print
    print 'PublicKey' # almost ... but not ... rfc3447 A.1.1 RSA public key syntax
    PublicKeyTransfer = SEQUENCE(
        Field('public_modulus', OCTET_STRING(), CONTEXT(1)),
        Field('public_exponent', OCTET_STRING(), CONTEXT(2)),
        )
    pk_enc = util.to_str([0x81, 0x82, 0x00, 0x3, 0x01, 0x00, 0x01, 0x82, 0x3, 0x01, 0x00, 0x01]) # note: length 3 encoded in 1+2 bytes
    test_ber_roundtrip('PublicKeyTransfer pk_enc', PublicKeyTransfer, pk_enc)

    pk_enc = util.to_str([0x81, 0x82, 0x00, 0x3, 0x01, 0x00, 0x01, 0x82, 0x3, 0x01, 0x00, 0x01, 0]) # note: length 3 encoded in 1+2 bytes
    d = PublicKeyTransfer.ber_decode(pk_enc)
    v = util.ObjectDict(d)
    print 'mod %r' % v['public_modulus'] # v.public_modulus
    print 'exp %r' % v['public_exponent'] # v.public_exponent
    print 'extra %r' % v['']


def demo():
    from pprint import pprint

    print
    print 'Guess'
    oids.OIDS.declare('commonName', '{joint-iso-itu-t(2) ds(5) attributeType(4) commonName(3)}')
    oids.OIDS.declare('organizationName', '{joint-iso-itu-t(2) ds(5) attributeType(4) organizationName(10)}')
    guesss = '071#0!\x06\x03U\x04\x03\x0c\x1a0108535916024f419C0B02af981\x100\x0e\x06\x03U\x04\n\x0c\x07IronKey'
    test_ber_roundtrip('Guess some IronKey stuff', Guess, guesss, oneway=True)

    print
    print 'TokenInfo'
    Label = UnicodeString()
    TokenInfo = Field(
        'tokenInfo',
        SEQUENCE(
            Field('version', INTEGER()),
            Field('serialNumber', OCTET_STRING()),
            Field('manufacturerID', Label()),
            Field('label', Label(), CONTEXT(0)),
            Field('tokenflags', BIT_STRING(dict(readonly=0, loginRequired=1, prnGeneration=2, eidCompliant=3))), # TokenFlags()),
            Field('seInfo', SEQUENCE_OF(Simple()), optional=True), # SecurityEnvironmentInfo()), # FIXME: SEQUENCE OF SecurityEnvironmentInfo
            Field('recordInfo', Simple(), CONTEXT(1), optional=True), # RecordInfo()
            Field('supportedAlgorithms', SEQUENCE_OF(Simple()), optional=True), # AlgorithmInfo() # FIXME: SEQUENCE OF AlgorithmInfo
            #...,
            Field('issuerId', Label(), CONTEXT(3)),
            #Field('holderId', Label(), CONTEXT(4), optional=True),
            #Field('lastUpdate', Unknown(), CONTEXT(5), optional=True), # LastUpdate()
            #Field('preferredLanguage', PrintableString(), None, optional=True),
            extension=True,
        )
        )
    ti = util.to_str([0x30, 0x73, 0x02, 0x01, 0x00, 0x04, 0x08, 0x20, 0x90, 0x70, 0x07, 0x00, 0x11, 0x0d, 0x0f, 0x0c, 0x12, 0x41, 0x2e, 0x45, 0x2e, 0x54, 0x2e, 0x20, 0x45, 0x75, 0x72, 0x6f, 0x70, 0x65, 0x20, 0x42, 0x2e, 0x56, 0x2e, 0x80, 0x20, 0x57, 0x68, 0x69, 0x74, 0x65, 0x20, 0x54, 0x65, 0x73, 0x74, 0x20, 0x43, 0x61, 0x72, 0x64, 0x20, 0x4f, 0x6e, 0x65, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x03, 0x02, 0x04, 0x50, 0x83, 0x2a, 0x43, 0x6f, 0x70, 0x79, 0x72, 0x69, 0x67, 0x68, 0x74, 0x20, 0x28, 0x63, 0x29, 0x20, 0x31, 0x39, 0x39, 0x37, 0x2d, 0x32, 0x30, 0x30, 0x36, 0x20, 0x41, 0x2e, 0x45, 0x2e, 0x54, 0x2e, 0x20, 0x45, 0x75, 0x72, 0x6f, 0x70, 0x65, 0x20, 0x42, 0x2e, 0x56, 0x2e])
    test_ber_roundtrip('TokenInfo ti', TokenInfo, ti)


    encode_example = """
    token_info = TokenInfo()
    token_info.setComponentByName('version', 0)
    token_info.setComponentByName('serialNumber', ' \x90p\x07\x00\x11\r\x0f')
    token_info.setComponentByName('tokenflags', (0,1,0,1))
    token_info.setComponentByName('tokenflags', TokenFlags().clone('loginRequired,eidCompliant'))
    token_info.setComponentByName('manufacturerID', 'A.E.T. Europe B.V.')
    token_info.setComponentByName('label', 'White Test Card One             ')
    token_info.setComponentByName('issuerId', 'Copyright (c) 1997-2006 A.E.T. Europe B.V.')
    """

    print
    print 'XX'
    XX = SEQUENCE(
        Field('seq',
            SEQUENCE(
                Field('seq', SEQUENCE()),
                guess=True,
                ),
            ),
        guess=True,
        )
    test1 = (
         '0\x110\x0c\x04\x02C\x00\x02\x02\x10\xa6\x80\x02\x11\x07\x04\x01\x820\x100\x0b\x04\x02C\x01\x02\x01\x00\x80\x02\x00\xea\x04\x01\x82\x000\x100\x0b\x04\x02C\x01\x02\x01\x00\x80\x02\x00\xea\x04\x01\x82\x00\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff'
         )
    test_ber_roundtrip('XX test1', XX, test1, oneway=True)

    print
    print 'Cert'
    Cert = SEQUENCE(
        Field('topseq',
            SEQUENCE(
                Field('someName', UnicodeString(), PRIVATE(2), optional=True),
                Field('someGUID', OCTET_STRING(), optional=True),
                Field('seqtwo',
                    SEQUENCE(
                        Field('issuedesc', UnicodeString()),
                        guess=True,
                        ),
                    ),
                guess=True,
                ),
            ),
            guess=True,
            )

    test2 = (
         "0\x81\xc20-\x0c$Mads Kiilerich's Giritech Root CA ID\x03\x02\x06@\x04\x01\x820&\x04$1ce7f113-43cd-4447-bb73-ec5877461100\xa1i0g0\x0b\x04\x02C\x00\x02\x01\x00\x80\x02\x06\x0e\xa0L0J1\x130\x11\x06\n\t\x92&\x89\x93\xf2,d\x01\x19\x16" +
         "\x03com1\x180\x16\x06\n\t\x92&\x89\x93\xf2,d\x01\x19\x16\x08giritech1\x190\x17\x06\x03U\x04\x03\x13\x10Giritech Root CA\x02\n!\x91\xed\xfd\x00\x00\x00\x00\x00\x120\x81\x9801\x0c(Mads Kristian Kiilerich's TDC OCES CA ID\x03\x02\x06@\x04\x01\x820\x16\x04\x14{" +
         '\xb8~\xd9,\xc6\x08\x8f\xd1\x83\xf9\x17Qb\x12!\xb9U\xe4\xa1\xa1K0I0\x0c\x04\x02C\x00\x02\x02\x06\x0e\x80\x02\x05{\xa03011\x0b0\t\x06\x03U\x04\x06\x13\x02DK1\x0c0\n\x06\x03U\x04\n\x13\x03TDC1\x140\x12\x06\x03U\x04\x03\x13\x0bTDC OCES CA\x02\x04Eq\x1bc0~0*\x0c!TDC OCES CA issued by TDC ' +
         'OCES CA\x03\x02\x06@\x04\x01\x820\x03\x04\x01\x00\xa1K0I0\x0c\x04\x02C\x00\x02\x02\x0b\x89\x80\x02\x05\x1d\xa03011\x0b0\t\x06\x03U\x04\x06\x13\x02DK1\x0c0\n\x06\x03U\x04\n\x13\x03TDC1\x140\x12\x06\x03U\x04\x03\x13\x0bTDC OCES CA\x02\x04>H\xbd\xc4\x00\x16\x08giritech1\x190\x17\x06\x03U\x04\x03\x13\x10Giritech R' +
         '0t0-\x0c$1a3f5778-b259-4cae-876e-9eed496c7a80\x03\x02\x06\xc0\x04\x01\x8203\x04$1a3f5778-b259-4cae-876e-9eed496c7a80\x03\x03\x06t\x00\x03\x02\x03\xb8\x02\x02\x00\x84\xa0\x020\x00\xa1\n0\x080\x02\x04\x00\x02\x02\x04\x000t0-\x0c$b3da' +
         '7a2d-241b-486e-b9d1-d14d10437d2b\x03\x02\x06\xc0\x04\x01\x8203\x04$b3da7a2d-241b-486e-b9d1-d14d10437d2b\x03\x03\x06t\x00\x03\x02\x03\xb8\x02\x02\x00\x85\xa0\x020\x00\xa1\n0\x080\x02\x04\x00\x02\x02\x04\x000t0-\x0c$Mads Kiilerich' +
         "'s Giritech Root CA ID\x03\x02\x06\xc0\x04\x01\x8203\x04$1ce7f113-43cd-4447-bb73-ec5877461100\x03\x03\x06t\x00\x03\x02\x03\xb8\x02\x02\x00\x86\xa0\x020\x00\xa1\n0\x080\x02\x04\x00\x02\x02\x04\x000\x81\xeb01\x0c(Mads Kristian Kiilerich" +
         "'s TDC OCES CA ID\x03\x02\x06\xc0\x04\x01\x820#\x04\x14{\xb8~\xd9,\xc6\x08\x8f\xd1\x83\xf9\x17Qb\x12!\xb9U\xe4\xa1\x03\x03\x06t\x00\x03\x02\x03\x80\x02\x02\x00\x87\xa0\x81\x840\x81\x810\x7f1\x0b0\t\x06\x03U\x04\x06\x13\x02DK1)0'\x06\x03U\x04\n\x13 Ingen organisatorisk tilknytning1E0" +
         '\x1e\x06\x03U\x04\x03\x13\x17Mads Kristian Kiilerich0#\x06\x03U\x04\x05\x13\x1cPID:9208-2002-2-554999497797\xa1\n0\x080\x02\x04\x00\x02\x02\x04\x00\x00\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff'
         )[:197]
    # This contains bogus TLVs ...
    file('x', 'w').write(test2)
    import x509 # get declare_oid('rfc2247-domainComponent'
    test_ber_roundtrip('Cert test2', Cert, test2, oneway=True)

    print
    print 'Foo'
    Foo = SEQUENCE(
        Field('topseq',
            SEQUENCE(
                Field('someName', UnicodeString(), PRIVATE(2), optional=True),
                Field('someGUID', OCTET_STRING(), optional=True),
                Field('seqtwo',
                    SEQUENCE(
                        Field('pindesc', UnicodeString()),
                        guess=True,
                        ),
                    ),
                guess=True,
                ),
            CONTEXT(0)),
            guess=True,
            )
    test3 = (
    '\xa0>0\x16\x0c\x10User FingerPrint\x03\x02\x06\xc00\x03\x04\x01\x83\xa1\x1f0\x1d\x03\x03\x07@\x80\x06\x02(\x000\x06\n\x01\x01\n\x01\x01\x02\x02\x00\x010\x06\x04\x04?\x00P\x150J0\x11\x0c\x08User Pin\x03\x02\x06\xc0\x04\x01\x830\x03\x04\x01\x82\xa100.\x03\x03\x04\xcc\x90\n\x01\x01\x02\x01\x04\x02\x01\x08\x80\x02\x00\x82\x04\x01\x00\x18\x0f20080408112'
    +
    '059Z0\x06\x04\x04?\x00P\x150E0\x0c\x0c\x06SO Pin\x03\x02\x06\xc00\x03\x04\x01\x83\xa100.\x03\x03\x04\xcf\x90\n\x01\x01\x02\x01\x04\x02\x01\x08\x80\x02\x00\x83\x04\x01\x00\x18\x0f20080408140609Z0\x06\x04\x04?\x00P\x15\x00\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff'
    )
    oids.OIDS.declare('foo', '{1 0(0) 0}')
    test_ber_roundtrip('Foo test3', Foo, test3, oneway=True)

def test_parse_GeneralizedTime():
    print
    print 'parse_GeneralizedTime'
    print parse_GeneralizedTime('20001231235959.999')
    print parse_GeneralizedTime('20001231205959.999Z')
    print parse_GeneralizedTime('20001231235959.999+0300')
    print
    print 'decode an oid:'
    s = "+\016\003\002\035" # '+\x0e\x03\x02\x1d'
    print OBJECT_IDENTIFIER().ber_decode(s)
    # +((1, 3, 14, 3, 2, 29), 'sha1WithRSAEncryption-obsolete(iso(1).identified-organization(3).oiw(14).secsig(3).algorithms(2).sha1WithRSAEncryption-obsolete(29))')
    # NID_sha1WithRSA

if __name__ == '__main__':
    util.hook()
    simple_demo()
    demo()
    test_parse_GeneralizedTime()
