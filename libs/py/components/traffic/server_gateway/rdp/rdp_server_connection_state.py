import os

import rdp_rsa
import pkcs1
import rdp_common_connection_state


class ServerConnectionState(rdp_common_connection_state.CommonConnectionState):

    def __init__(self, env):
        rdp_common_connection_state.CommonConnectionState.__init__(self, env)

        # On the connection to the server we act MITM as client:

        # http://msdn.microsoft.com/en-us/library/cc240654 3.2.1.1 Received Server Data
        # This store is initialized when processing the MCS Connect Response PDU with GCC Conference Create Response (see sections 2.2.1.4 and 3.2.5.3.4).
        self._server_random = None # servers own random
        self._terminal_server_pub_key = None # terminal servers public key
        self._license_server_pub_key = None # license servers public key

        self._mitm_client_random = None # my random as client to server

        # http://msdn.microsoft.com/en-us/library/cc240654 3.2.1.1 Received Server Data
        # This store is initialized when processing the MCS Connect Response PDU with GCC Conference Create Response (see sections 2.2.1.4 and 3.2.5.3.4).
        self.encryptionMethod = None
        self.encryptionMethodKeyLength = None # 40, 56 or 128
        self.encryptionLevel = None

    def handle_server_security_data(self, server_random, encryptionMethod, encryptionLevel):
        self._server_random = server_random
        self._env.log3('\tserver_random:', dump=self._server_random)

        assert not self._mitm_client_random, self._mitm_client_random
        self._mitm_client_random = self._env.config.RANDOM_CLIENT_MITM_SERVERSIDE or os.urandom(32)
        self._env.log3('\tgenerated mitm_client_random:', dump=self._mitm_client_random)

        self.encryptionMethod = encryptionMethod
        self.encryptionLevel = encryptionLevel
        self._env.log1('\tserver-side encryptionMethod: %s, encryptionLevel: %s', self.encryptionMethod, self.encryptionLevel)
        # If the encryptionMethod and encryptionLevel fields are both set to 0 then this field MUST NOT be present.
        assert not (self.encryptionMethod.ENCRYPTION_METHOD_NONE and self.encryptionLevel.ENCRYPTION_LEVEL_NONE), self.encryptionMethod
        if self.encryptionMethod.ENCRYPTION_METHOD_40BIT:
            self.encryptionMethodKeyLength = 40
        elif self.encryptionMethod.ENCRYPTION_METHOD_56BIT:
            self.encryptionMethodKeyLength = 56
        elif self.encryptionMethod.ENCRYPTION_METHOD_128BIT:
            self.encryptionMethodKeyLength = 128
        else:
            raise RuntimeError('unknown encryptionMethod 0x%x' % encryptionMethod.value)

        self._env.log1('\tInitializing Server-side Initial Session Key')
        self.generate_session_keys(
            self._mitm_client_random, self._server_random, self.encryptionMethodKeyLength)

    def get_encrypted_mitm_client_random(self):
        # http://msdn.microsoft.com/en-us/library/cc240782 5.3.4.1 Encrypting Client Random
        return rdp_rsa.le_str_from_long(self._terminal_server_pub_key.encrypt(
            rdp_rsa.le_str_to_long(self._mitm_client_random)))

    def set_terminal_server_pub_key(self, modulus, public_exponent):
        self._terminal_server_pub_key = pkcs1.RsaLong(modulus, public_exponent)

    def set_license_server_pub_key(self, modulus, public_exponent):
        self._license_server_pub_key = pkcs1.RsaLong(modulus, public_exponent)

    def create_EncryptedPreMasterSecret(self, PreMasterSecret):
        return rdp_rsa.le_str_from_long(self._terminal_server_pub_key.encrypt(rdp_rsa.le_str_to_long(PreMasterSecret)),
                length=64 + 8) # Always 64 bytes = 512 bits? Plus additional 8 bytes zero-padding
