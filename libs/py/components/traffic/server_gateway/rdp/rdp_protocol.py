import util
import asn1
import rdp_defs
import rdp_asn1
import x509
import rdp_rsa
import rdp_crypto
import rdp_license
import rdp_client_connection_state
import rdp_server_connection_state
import pkcs7

STATE_X224_UNCONNECTED = 'STATE_X224_UNCONNECTED'
STATE_X224_UNCONFIRMED = 'STATE_X224_UNCONFIRMED'
STATE_MCS_UNCONNECTED = 'STATE_MCS_UNCONNECTED'
STATE_MCS_UNCONFIRMED = 'STATE_MCS_UNCONFIRMED'
STATE_MCS_CONNECTED = 'STATE_MCS_CONNECTED'

HACK_ENFORCE_PROTOCOL_RDP = True # ensure fallback from NLA/SSL to plain RDP

simple_ca_cert = \
{'signatureAlgorithm': {'algorithm': ((1, 3, 14, 3, 2, 29),
                                      'sha1WithRSAEncryption-obsolete(iso(1).identified-organization(3).oiw(14).secsig(3).algorithms(2).sha1WithRSAEncryption-obsolete(29))'),
                        'parameters': None},
 'signatureValue': ('octets', ''),
 'tbsCertificate': {
#                    'extensions': [{'critical': False,
#                                    'extnID': ((2, 5, 29, 19),
#                                               'id-ce-basicConstraints(joint-iso-itu-t(2).ds(5).ce(29).id-ce-basicConstraints(19))'),
#                                    'extnValue': '0\x06\x01\x01\xff\x02\x01\x00'}],
                    'issuer': ('rdnSequence',
                               [[{'type': ((2, 5, 4, 3),
                                           'commonName(joint-iso-itu-t(2).ds(5).attributeType(4).commonName(3))'),
                                  'value': ('bmpString', u'Giritech G/On CA')}]]),
                    'serialNumber': 1,
                    'signature': {'algorithm': ((1, 3, 14, 3, 2, 29),
                                                'sha1WithRSAEncryption-obsolete(iso(1).identified-organization(3).oiw(14).secsig(3).algorithms(2).sha1WithRSAEncryption-obsolete(29))'),
                                  'parameters': None},
                    'subject': ('rdnSequence',
                                [[{'type': ((2, 5, 4, 3),
                                           'commonName(joint-iso-itu-t(2).ds(5).attributeType(4).commonName(3))'),
                                  'value': ('bmpString', u'Giritech G/On CA')}]]),
                    'subjectPublicKeyInfo': {'algorithm': {'algorithm': ((1, 2, 840, 113549, 1, 1, 1),
                                                                         'rsaEncryption(iso(1).member-body(2).us(840).rsadsi(113549).pkcs(1).pkcs-1(1).rsaEncryption(1))'),
                                                           'parameters': None},
                                             'subjectPublicKey': ('octets', '')},
                    'validity': {'notAfter': ('utcTime', '490617171811Z'),
                                 'notBefore': ('utcTime', '700617171811Z')},
                    'version': 2}}

simple_ts_cert = \
{'signatureAlgorithm': {'algorithm': ((1, 3, 14, 3, 2, 29),
                                      'sha1WithRSAEncryption-obsolete(iso(1).identified-organization(3).oiw(14).secsig(3).algorithms(2).sha1WithRSAEncryption-obsolete(29))'),
                        'parameters': None},
 'signatureValue': ('octets', ''),
 'tbsCertificate': {
#                    'extensions': [{'critical': True,
#                                    'extnID': ((1, 3, 6, 1, 4, 1, 311, 18, 4),
#                                               'szOID_PKIX_HYDRA_CERT_VERSION(iso(1).identified-organization(3).dod(6).internet(1).private(4).enterprise(1).microsoft(311).MS-crypto-oids(18).szOID_PKIX_HYDRA_CERT_VERSION(4))'),
#                                    'extnValue': '\x01\x00\x05\x00'},
#                                   {'critical': True,
#                                    'extnID': ((1, 3, 6, 1, 4, 1, 311, 18, 2),
#                                               'szOID_PKIX_MANUFACTURER(iso(1).identified-organization(3).dod(6).internet(1).private(4).enterprise(1).microsoft(311).MS-crypto-oids(18).szOID_PKIX_MANUFACTURER(2))'),
#                                    'extnValue': 'M\x00i\x00c\x00r\x00o\x00s\x00o\x00f\x00t\x00 \x00C\x00o\x00r\x00p\x00o\x00r\x00a\x00t\x00i\x00o\x00n\x00\x00\x00'},
#                                   {'critical': True,
#                                    'extnID': ((1, 3, 6, 1, 4, 1, 311, 18, 5),
#                                               'szOID_PKIX_LICENSED_PRODUCT_INFO(iso(1).identified-organization(3).dod(6).internet(1).private(4).enterprise(1).microsoft(311).MS-crypto-oids(18).szOID_PKIX_LICENSED_PRODUCT_INFO(5))'),
#                                    'extnValue': '\x000\x00\x00\x01\x00\x00\x00\x02\x00\x00\x00\t\x04\x00\x00\x1c\x00J\x00f\x00J\x00\xb0\x00\x01\x003\x00d\x002\x006\x007\x009\x005\x004\x00-\x00e\x00e\x00b\x007\x00-\x001\x001\x00d\x001\x00-\x00b\x009\x004\x00e\x00-\x000\x000\x00c\x000\x004\x00f\x00a\x003\x000\x008\x000\x00d\x00\x00\x003\x00d\x002\x006\x007\x009\x005\x004\x00-\x00e\x00e\x00b\x007\x00-\x001\x001\x00d\x001\x00-\x00b\x009\x004\x00e\x00-\x000\x000\x00c\x000\x004\x00f\x00a\x003\x000\x008\x000\x00d\x00\x00\x00\x00\x00\x00\x10\x00\x80\xe4\x00\x00\x00\x00\x00'},
#                                   {'critical': True,
#                                    'extnID': ((1, 3, 6, 1, 4, 1, 311, 18, 6),
#                                               'szOID_PKIX_MS_LICENSE_SERVER_INFO(iso(1).identified-organization(3).dod(6).internet(1).private(4).enterprise(1).microsoft(311).MS-crypto-oids(18).szOID_PKIX_MS_LICENSE_SERVER_INFO(6))'),
#                                    'extnValue': '\x000\x00\x00\x00\x00\x14\x00D\x00V\x00T\x00S\x00T\x00-\x00S\x00O\x00S\x004\x00\x00\x005\x005\x000\x004\x001\x00-\x002\x002\x002\x00-\x006\x009\x003\x009\x009\x007\x006\x00-\x007\x006\x007\x004\x007\x00\x00\x00D\x00E\x00V\x00T\x00E\x00S\x00T\x002\x000\x000\x008\x00\x00\x00\x00\x00'},
#                                   {'critical': True,
#                                    'extnID': ((2, 5, 29, 35),
#                                               'id-ce-authorityKeyIdentifier(joint-iso-itu-t(2).ds(5).ce(29).id-ce-authorityKeyIdentifier(35))'),
#                                    'extnValue': '0\x1f\xa1\x16\xa4\x14V\x00T\x00S\x00T\x00-\x00S\x00O\x00S\x004\x00\x00\x00\x82\x05\x01\x00\x00\x00\x04'}],
                    'issuer': ('rdnSequence',
                               [[{'type': ((2, 5, 4, 3),
                                           'commonName(joint-iso-itu-t(2).ds(5).attributeType(4).commonName(3))'),
                                  'value': ('bmpString', u'Giritech G/On CA')}]]),
                    'serialNumber': 1,
                    'signature': {'algorithm': ((1, 3, 14, 3, 2, 29),
                                                'sha1WithRSAEncryption-obsolete(iso(1).identified-organization(3).oiw(14).secsig(3).algorithms(2).sha1WithRSAEncryption-obsolete(29))'),
                                  'parameters': None},
                    'subject': ('rdnSequence',
                                [[{'type': ((2, 5, 4, 3),
                                            'commonName(joint-iso-itu-t(2).ds(5).attributeType(4).commonName(3))'),
                                   'value': ('bmpString',
                                             u'localhost')}]]),
                    'subjectPublicKeyInfo': {'algorithm': {'algorithm': ((1, 3, 14, 3, 2, 15),
                                                                         'shaWithRSASignature(iso(1).identified-organization(3).oiw(14).secsig(3).algorithms(2).shaWithRSASignature(15))'),
                                                           'parameters': None},
                                             'subjectPublicKey': ('octets', '')},
                    'validity': {'notAfter': ('utcTime', '380119031407Z'),
                                 'notBefore': ('utcTime', '090617172506Z')},
                    'version': 2}}

class RdpPdu(object):
    """
    RDP object keeping state and receiving/checking packages
    """

    def __init__(self, env):
        self._env = env
        self._state = STATE_X224_UNCONNECTED

        # On the connection to the server we act MITM as client:
        self.server_connection_state = rdp_server_connection_state.ServerConnectionState(self._env)

        # http://msdn.microsoft.com/en-us/library/cc240652 3.2 Client Details
        # http://msdn.microsoft.com/en-us/library/cc240653 3.2.1 Abstract Data Model

        # http://msdn.microsoft.com/en-us/library/cc240681 3.2.1.2 Static Virtual Channel IDs
        # This data store is initialized when processing the Server Network Data (see sections 2.2.1.4.4 and 3.2.5.3.4).
        self.ChannelDefs = {} # maps from number such as 0 to descriptive name
        self.StaticVirtualChannelIDs = {} # maps from ChannelId to to descriptive name
        # The MCS channel IDs returned in the channelIdArray should be saved in the Static Virtual Channel IDs store (see section 3.2.1.2)

        # http://msdn.microsoft.com/en-us/library/cc240656 3.2.1.3 I/O Channel ID
        # This data store is initialized when processing the Server Network Data (see sections 2.2.1.4.4 and 3.2.5.3.4).
        self.IOChannelID = None # MCSChannelId field should be saved in the I/O Channel ID store (see section 3.2.1.3)

        # http://msdn.microsoft.com/en-us/library/cc240657 3.2.1.4 User Channel ID
        # This data store is initialized when processing the MCS Attach User Confirm PDU (see sections 2.2.1.7 and 3.2.5.3.7).
        self.UserChannelID = None

        # http://msdn.microsoft.com/en-us/library/cc240906 3.2.1.5 Server Channel ID
        # This data store is initialized when processing the Demand Active PDU (see sections 2.2.1.13.1.1 and 3.2.5.3.13.1).
        self.ServerChannelID = None

        # http://msdn.microsoft.com/en-us/library/cc240905 3.2.1.6 Server Capabilities
        # capability sets (see section 1.7) received from the server in the Demand Active PDU (see sections 2.2.1.13.1 and 3.2.5.3.13.1).

        # http://msdn.microsoft.com/en-us/library/cc240924 3.2.1.7 Share ID
        # This data store is initialized when processing the Demand Active PDU (see sections 2.2.1.13.1 and 3.2.5.3.13.1) and is used to initialize the shareId field of the Share Data Header when sending basic client-to-server Slow-Path PDUs (see section 3.2.5.1).
        self.ShareID = None

        # http://msdn.microsoft.com/en-us/library/cc240818 3.2.1.8 Automatic Reconnection Cookie
        # The cookie is received in a Save Session Info PDU (see sections 2.2.10.1 and 3.2.5.9.5.1).

        # http://msdn.microsoft.com/en-us/library/cc240917 3.2.1.9 Server Licensing Encryption Ability
        # This fact is communicated to the client by setting the SEC_LICENSE_ENCRYPT_CS (0x0200) flag in all licensing PDUs sent from the server.

        # http://msdn.microsoft.com/en-us/library/cc240882 3.2.1.10 Pointer Image Cache
        # collection of pointer images saved from Color Pointer Updates (see sections 2.2.9.1.2.1.7, 3.2.5.9.2, and 3.2.5.9.3) and New Pointer Updates (see sections 2.2.9.1.2.1.8, 3.2.5.9.2, and 3.2.5.9.3)

        # On the connection to the client we act MITM as server:
        self.client_connection_state = rdp_client_connection_state.ClientConnectionState(self._env)

        # http://msdn.microsoft.com/en-us/library/cc240712 3.3 Server Details
        # http://msdn.microsoft.com/en-us/library/cc240713 3.3.1 Abstract Data Model

        # http://msdn.microsoft.com/en-us/library/cc240714 3.3.1.1 Received Client Data
        # This store is initialized when processing the X.224 Connection Request PDU (section 2.2.1.1),
        # MCS Connect Initial PDU with GCC Conference Create Request (see sections 2.2.1.3 and 3.3.5.3.3), and
        # Client Info PDU (see sections 2.2.1.11 and 3.3.5.3.11).

        # http://msdn.microsoft.com/en-us/library/cc240930 3.3.1.2 User Channel ID
        # = self.UserChannelID

        # http://msdn.microsoft.com/en-us/library/cc240715 3.3.1.3 I/O Channel ID
        # = self.IOChannelID

        # http://msdn.microsoft.com/en-us/library/cc240716 3.3.1.4 Server Channel ID
        # In Microsoft RDP server implementations, this value is always 0x03EA.
        # = self.ServerChannelID

        # http://msdn.microsoft.com/en-us/library/cc240717 3.3.1.5 Client Licensing Encryption Ability
        # communicated to the server as part of the Security Exchange PDU (see sections 2.2.1.10 and 3.2.5.3.10).

        # http://msdn.microsoft.com/en-us/library/cc240824 3.3.1.6 Client Capabilities
        # received from the client in the Confirm Active PDU (see sections 2.2.1.13.2 and 3.3.5.3.13.2)

        # http://msdn.microsoft.com/en-us/library/cc240879 3.3.1.7 Persistent Bitmap Keys

        # http://msdn.microsoft.com/en-us/library/cc240883 3.3.1.8 Pointer Image Cache
        # sent to the client in Color Pointer Updates (see sections 2.2.9.1.2.1.7, 3.3.5.9.2, and 3.3.5.9.3) and New Pointer Updates (see sections 2.2.9.1.2.1.8, 3.3.5.9.2, and 3.3.5.9.3)

    def channel_name(self, channelId):
        if channelId == self.UserChannelID:
            return 'User Channel' # the MCS channel ID is stored in the User Channel ID store (section 3.2.1.4)
        if channelId == self.IOChannelID:
            return 'I/O Channel' # the MCS channel ID is stored in the I/O Channel ID store (section 3.2.1.3)
        if channelId == self.ServerChannelID:
            return 'Server Channel'
        if channelId == 1002:
            return 'Server Channel (Apparently)' # server channel ID (in Microsoft RDP server implementations, this value is always 0x03EA=1002).
        if channelId in self.StaticVirtualChannelIDs:
            return 'Static Virtual Channel %r' % self.StaticVirtualChannelIDs[channelId]
        # the MCS channel IDs are stored in the Static Virtual Channel IDs store (section 1.3.3)
        return 'Unknown Channel %r' % channelId

    @util.exceptor
    def ClientServerX224Request(self, x224_body):
        assert self._state == STATE_X224_UNCONNECTED, self._state
        # http://msdn.microsoft.com/en-us/library/cc240663 3.2.5.3.1 Sending X.224 Connection Request PDU
        # http://msdn.microsoft.com/en-us/library/cc240470 2.2.1.1 Client X.224 Connection Request PDU
        # The request might explicitly or implicitly ask for a protocol we don't support - but we handle that in the response
        #self._env.log1('Client X.224 Connection Request PDU') # already logged by rdp_engine
        #self._env.log2('x224_body:', dump=x224_body)
        if '\r\n' in x224_body:
            routingToken, rest = x224_body.split('\r\n', 1)
        else:
            routingToken, rest = '', x224_body # no separator between optional fields - assuming only rdpNegData and/or rdpCorrelationInfo present
        # "see [MSFT-SDLBTS]"
        self._env.log1('routingToken: %r', routingToken)
        # http://download.microsoft.com/download/8/6/2/8624174c-8587-4a37-8722-00139613a5bc/TS_Session_Directory.doc
        if routingToken.startswith('Cookie: msts=') and routingToken.count('.') == 2:
            try:
                a, b, _c = routingToken.split('=', 1)[1].split('.', 2)
                ip = '.'.join(str((int(a) >> i) & 0xff) for i in range(0, 32, 8))
                port = (int(b) >> 8) | ((int(b) & 0xff) << 8)
                #assert int(c) == 0
                self._env.log1('routingToken address: %s:%s', ip, port)
            except ValueError: pass # might also have been be Cookie: mstshash=IDENTIFIER
        if rest:
            # may start with rdpNegData or rdpCorrelationInfo (both are optional)
            if rest[0] == '\x01': # (TYPE_RDP_NEG_REQ)
                rdpNegData = rest[:8] # rdpNegData is 8 bytes
                rdpCorrelationInfo = rest[8:] # rdpCorrelationInfo optionally follows
                assert len(rdpNegData) == 8, (rdpNegData,)
                # http://msdn.microsoft.com/en-us/library/cc240500 2.2.1.1.1 RDP Negotiation Request (RDP_NEG_REQ)
                type_, flags, length, requestedProtocols_int, rest = util.PackedStruct('<BBHI').unpack(rdpNegData)
                assert not rest, (rest,)
                assert type_ == 0x01, type_ # MUST be set to 0x01 (TYPE_RDP_NEG_REQ) to indicate that the packet is a Negotiation Request.
                assert flags == 0 or flags == 8, flags # MUST be set to 0x00 or 0x08 (CORRELATION_INFO_PRESENT).
                assert not(flags == 8) or rdpCorrelationInfo, flags     # if the CORRELATION_INFO_PRESENT (0x08) flag is set, then this field MUST be present
                assert not(flags == 0) or not rdpCorrelationInfo, flags # if the CORRELATION_INFO_PRESENT (0x08) flag is not set, then this field MUST NOT be present
                assert length == 8, length # MUST be set to 0x0008
                requestedProtocols = rdp_defs.RDP_NEG_REQ_requestedProtocols(requestedProtocols_int)
                self._env.log1('requestedProtocols: %s', requestedProtocols)
            else:
                rdpCorrelationInfo = rest
            #
            if rdpCorrelationInfo:
                assert len(rdpCorrelationInfo) == 36, (rdpCorrelationInfo,)
                # //http://msdn.microsoft.com/en-us/library/dn366776 2.2.1.1.2 RDP Correlation Info (RDP_NEG_CORRELATION_INFO)
                type_, flags, length, rest = util.PackedStruct('<BBH').unpack(rdpCorrelationInfo)
                correlationId = rest[:16]
                rest = rest[16:]
                assert type_ == 0x06, type_ # MUST be set to 0x06 (TYPE_RDP_CORRELATION_INFO)
                assert flags == 0, flags # MUST be set to 0x00.
                assert length == 36, length # MUST be set to 0x0024
                self._env.log1('correlationId: %r', correlationId)
        else:
            self._env.log1('rest: %r', rest) # Seen with XP Home sp2

        new_x224_body = x224_body

        self._state = STATE_X224_UNCONFIRMED
        return new_x224_body

    @util.exceptor
    def ServerClientX224Confirm(self, x224_body):
        assert self._state == STATE_X224_UNCONFIRMED, self._state
        # http://msdn.microsoft.com/en-us/library/cc240677 3.2.5.3.2 Processing X.224 Connection Confirm PDU
        # http://msdn.microsoft.com/en-us/library/cc240501 2.2.1.2 Server X.224 Connection Confirm PDU
        #self._env.log1('Server X.224 Connection Confirm PDU') # already logged by rdp_engine
        rdpNegData = x224_body # Optional RDP Negotiation Response (section 2.2.1.2.1) structure or an optional RDP Negotiation Failure (section 2.2.1.2.2) structure.

        if rdpNegData:
            type_, flags, length, rdpNeg_rest = util.PackedStruct('<BBH').unpack(rdpNegData)
            self._env.log1('type: %s', type_)
        else:
            self._env.log1('rdpNegData: %r', rdpNegData) # Default value: RDP_NEG_RSP granting whatever was requested
            type_ = 0x02
            flags = 0
            length = 8 # MUST be set to 0x0008 (8 bytes).
            rdpNeg_rest = None # we don't know what ...

        if type_ == 0x02: # TYPE_RDP_NEG_RSP to indicate that the packet is a Negotiation Response.
            # http://msdn.microsoft.com/en-us/library/cc240506 2.2.1.2.1 RDP Negotiation Response (RDP_NEG_RSP)
            self._env.log1('flags: %s', rdp_defs.RDP_NEG_RSP_flags(flags)) # Negotiation packet flags.
            assert length == 8, length # MUST be set to 0x0008 (8 bytes).

            if rdpNeg_rest is not None:
                selectedProtocol_int, rest = util.PackedStruct('<I').unpack(rdpNeg_rest)
                assert not rest, (rest,)
                selectedProtocol = rdp_defs.RDP_NEG_PROTOCOL(selectedProtocol_int)
            else:
                selectedProtocol = None
            self._env.log1('selectedProtocol: %s', selectedProtocol)

            if HACK_ENFORCE_PROTOCOL_RDP and selectedProtocol and not selectedProtocol.PROTOCOL_RDP:
                # Report failure - the client will connect again and request another protocol
                self._env.log1('Objection - sending RDP_NEG_FAILURE.SSL_NOT_ALLOWED_BY_SERVER instead!')
                new_type_ = 3 # TYPE_RDP_NEG_FAILURE
                new_flags = 0
                new_x224_body = util.PackedStruct('<BBHI').pack(new_type_, new_flags, length, rdp_defs.RDP_NEG_FAILURE.SSL_NOT_ALLOWED_BY_SERVER)
            else:
                new_x224_body = x224_body # don't change

        elif type_ == 0x03: # TYPE_RDP_NEG_FAILURE to indicate that the packet is a Negotiation Failure.
            # http://msdn.microsoft.com/en-us/library/cc240507 2.2.1.2.2 RDP Negotiation Failure (RDP_NEG_FAILURE)
            assert not flags, rdp_defs.RDP_NEG_RSP_flags(flags) # There are currently no defined flags so the field MUST be set to 0x00.
            assert length == 8, length # MUST be set to 0x0008 (8 bytes).

            failureCode_int, rest = util.PackedStruct('<I').unpack(rdpNeg_rest)
            assert not rest, (rest,)
            failureCode = rdp_defs.RDP_NEG_FAILURE(failureCode_int)
            self._env.log1('failureCode: %s', failureCode) # x-men will respond SSL_NOT_ALLOWED_BY_SERVER when asked for SSL
            new_x224_body = x224_body

        else:
            assert False, type_

        self._state = STATE_MCS_UNCONNECTED
        return new_x224_body

    @util.exceptor
    def ClientServerPdu(self, x224_body):

        if self._state == STATE_MCS_UNCONNECTED:
            # http://msdn.microsoft.com/en-us/library/cc240681 3.2.5.3.3 Sending MCS Connect Initial PDU with GCC Conference Create Request
            # http://msdn.microsoft.com/en-us/library/cc240508 2.2.1.3 Client MCS Connect Initial PDU with GCC Conference Create Request
            self._env.log1('Client MCS Connect Initial PDU with GCC Conference Create Request')

            # mcsCi, Variable-length BER-encoded MCS Connect Initial structure (using definite-length encoding)
            # as described in [T125] (the ASN.1 structure definition is detailed in [T125] section 7, part 2).
            connectMCSPDU = rdp_asn1.ConnectMCSPDU.ber_decode(x224_body)
            #self._env.log1('connectMCSPDU:', pprint=connectMCSPDU)
            (choice_tag, connectInitial) = connectMCSPDU
            assert choice_tag == 'connect-initial', choice_tag
            if self._env.config.STRICT:
                # The client SHOULD initialize the fields of the MCS Connect Initial PDU as follows.
                assert connectInitial['calledDomainSelector'] == '\x01', connectInitial
                assert connectInitial['callingDomainSelector'] == '\x01', connectInitial
                assert connectInitial['upwardFlag'], connectInitial
                # The targetParameters, minimumParameters, and maximumParameters domain parameter structures SHOULD be initialized as follows.
                if connectInitial['targetParameters'] != {
                    'maxChannelIds': 34,
                    'maxHeight': 1,
                    'maxMCSPDUsize': -1,
                    'maxTokenIds': 0,
                    'maxUserIds': 2,
                    'minThroughput': 0,
                    'numPriorities': 1,
                    'protocolVersion': 2}:
                    self._env.log2('Unexpected targetParameters: %s', connectInitial['targetParameters'])
                if connectInitial['minimumParameters'] != {
                    'maxChannelIds': 1,
                    'maxHeight': 1,
                    'maxMCSPDUsize': 1056,
                    'maxTokenIds': 1,
                    'maxUserIds': 1,
                    'minThroughput': 0,
                    'numPriorities': 1,
                    'protocolVersion': 2}:
                    self._env.log2('Unexpected minimumParameters: %s', connectInitial['minimumParameters'])
                if connectInitial['maximumParameters'] != {
                    'maxChannelIds': -1,
                    'maxHeight': 1,
                    'maxMCSPDUsize': -1,
                    'maxTokenIds': -1,
                    'maxUserIds': -1001,
                    'minThroughput': 0,
                    'numPriorities': 1,
                    'protocolVersion': 2}:
                    self._env.log2('Unexpected maximumParameters: %s', connectInitial['maximumParameters'])

            # "The userData field of the MCS Connect Initial encapsulates the GCC Conference Create Request data
            # (contained in the gccCCrq and subsequent fields)."
            # Our interpretation:
            connectInitialUserData = connectInitial['userData']
            connectData = rdp_asn1.ConnectData.per_decode(connectInitialUserData)
            #self._env.log1('connectData:', pprint=connectData)
            # ... encapsulates a Connect GCC PDU ...
            # (t124Identifier shall be set to the value {itu-t recommendation t 124 version(0) 1} )
            assert connectData['t124Identifier'][0] == 'object', connectData
            assert connectData['t124Identifier'][1][0] == (0, 0, 20, 124, 0, 1), connectData
            connectPDU = connectData['connectPDU']
            # ... [that contains ... "appended" ... to the] MCS Connect Initial (using the format described in [T124] sections 9.5 and 9.6)
            connectGCCPDU = rdp_asn1.ConnectGCCPDU.per_decode(connectPDU)
            #self._env.log1('connectGCCPDU:', pprint=connectGCCPDU)
            # ... that contains a GCC Conference Create Request structure as described in [T124] section 8.7 "appended as user data" to the
            assert connectGCCPDU[0] == 'conferenceCreateRequest', connectGCCPDU[0]
            conferenceCreateRequest = connectGCCPDU[1]
            #self._env.log1('conferenceCreateRequest:', pprint=conferenceCreateRequest)

            assert conferenceCreateRequest['conferenceName'] == {'numeric': '\x01'}, conferenceCreateRequest
            assert not conferenceCreateRequest['lockedConference'], conferenceCreateRequest
            assert not conferenceCreateRequest['listedConference'], conferenceCreateRequest
            assert not conferenceCreateRequest['conductibleConference'], conferenceCreateRequest
            assert conferenceCreateRequest['terminationMethod'] == 'automatic', conferenceCreateRequest

            # The userData field of the GCC Conference Create Request ...
            conferenceCreateRequestUserData = conferenceCreateRequest['userData'] # [UserData field is SET_OF SEQUENCE - a list of sequences with key-tuple and (opt) value-tuple]
            assert len(conferenceCreateRequestUserData) == 1, conferenceCreateRequestUserData # ...  contains one user data set ...
            conferenceCreateRequestUserDataOnly = conferenceCreateRequestUserData[0]
            assert conferenceCreateRequestUserDataOnly['key'] == ('h221NonStandard', 'Duca'), conferenceCreateRequestUserDataOnly['key'] # Ducati
            client_data = conferenceCreateRequestUserDataOnly['value'] # ... consisting of concatenated client data blocks.
            while client_data:
                # http://msdn.microsoft.com/en-us/library/cc240509 2.2.1.3.1 User Data Header (TS_UD_HEADER)
                ts_ud_header_format = util.PackedStruct('<HH')
                header_type_int, l, _rest = ts_ud_header_format.unpack(client_data)
                client_data_body = client_data[ts_ud_header_format.size:l] # Note: l _includes_ the header length, so we reuse client_data and don't use _rest!
                client_data = client_data[l:]

                header_type = rdp_defs.TS_UD_HEADER_type(header_type_int)
                self._env.log1('header_type: %s', header_type)

                if header_type.CS_CORE:
                    # http://msdn.microsoft.com/en-us/library/cc240510 2.2.1.3.2 Client Core Data (TS_UD_CS_CORE)
                    (version_, desktopWidth, desktopHeight, colorDepth, SASSequence,
                        keyboardLayout, clientBuild, clientName, keyboardType, keyboardSubType, keyboardFunctionKey,
                        imeFileName, postBeta2ColorDepth, clientProductId, serialNumber, highColorDepth, supportedColorDepths,
                        earlyCapabilityFlags, clientDigProductId, connectionType, rest
                        ) = util.PackedStruct('<IHHHHII32sIII64sHHIHHH64sB').unpack(client_data_body)
                    if rest:
                        _pad2octets,rest = util.PackedStruct('<B').unpack(rest)
                    serverSelectedProtocol = 0
                    if rest: # mstsc goes here
                        serverSelectedProtocol, rest = util.PackedStruct('<I').unpack(rest)
                    desktopPhysicalWidth, desktopPhysicalHeight = None, None
                    if rest:
                        desktopPhysicalWidth, desktopPhysicalHeight, rest = util.PackedStruct('<II').unpack(rest)

                    self._env.log1('\tVersion: %s', rdp_defs.TS_UD_clientVersions(version_))
                    self._env.log1('\tDesktop: %s x %s, colorDepth: %s', desktopWidth, desktopHeight, rdp_defs.TS_UD_CS_CORE_ColorDepth(colorDepth))
                    assert SASSequence == 0xAA03, SASSequence # Secure access sequence. This field SHOULD be set to RNS_UD_SAS_DEL (0xAA03).
                    self._env.log1('\tkeyboardLayout: 0x%x', keyboardLayout) # Keyboard layout (active input locale identifier). For a list of possible input locales, see [MSDN-MUI].
                    self._env.log1('\tclient: Build %s, name: %r', clientBuild, clientName.decode('utf-16le'))
                    self._env.log1('\tkeyboard: Type: %s, SubType: %s, FunctionKey: %s',
                        rdp_defs.TS_UD_CS_CORE_keyboardType(keyboardType), keyboardSubType, keyboardFunctionKey)
                    self._env.log1('\timeFileName: %r', imeFileName.decode('utf-16le'))
                    self._env.log1('\tpostBeta2ColorDepth: %s', rdp_defs.TS_UD_CS_CORE_ColorDepth(postBeta2ColorDepth))
                    self._env.log1('\tclientProductId: %s', clientProductId)
                    self._env.log1('\tserialNumber: %s', serialNumber)
                    self._env.log1('\thighColorDepth: %s, supportedColorDepths: %s',
                        rdp_defs.TS_UD_CS_CORE_highColorDepth(highColorDepth), rdp_defs.TS_UD_CS_CORE_supportedColorDepths(supportedColorDepths))
                    self._env.log1('\tearlyCapabilityFlags: %s', rdp_defs.TS_UD_CS_CORE_earlyCapabilityFlags(earlyCapabilityFlags))
                    self._env.log1('\tclientDigProductId: %r', clientDigProductId.decode('utf-16le'))
                    self._env.log1('\tconnectionType: %s', rdp_defs.TS_UD_CS_CORE_connectionType(connectionType)) # valid if RNS_UD_CS_VALID_CONNECTION_TYPE
                    self._env.log1('\tserverSelectedProtocol: %s', rdp_defs.RDP_NEG_PROTOCOL(serverSelectedProtocol))
                    self._env.log1('\tdesktopPhysical: %s x %s', desktopPhysicalWidth, desktopPhysicalHeight)

                elif header_type.CS_SECURITY:
                    # http://msdn.microsoft.com/en-us/library/cc240511 2.2.1.3.3 Client Security Data (TS_UD_CS_SEC)

                    encryptionMethods_int, extEncryptionMethods, rest = util.PackedStruct('<II').unpack(client_data_body)
                    encryptionMethods = rdp_defs.TS_UD_CS_SEC_encryptionMethods(encryptionMethods_int)
                    assert not rest, (rest,)
                    self._env.log1('\tencryptionMethods: %s extEncryptionMethods: 0x%x',
                        encryptionMethods, extEncryptionMethods)

                    self.client_connection_state.handle_client_security_data(encryptionMethods, extEncryptionMethods)

                elif header_type.CS_NET:
                    # http://msdn.microsoft.com/en-us/library/cc240512 2.2.1.3.4 Client Network Data (TS_UD_CS_NET)
                    channelCount, channelDefArray = util.PackedStruct('<I').unpack(client_data_body)

                    for i in range(channelCount):
                        # http://msdn.microsoft.com/en-us/library/cc240513 2.2.1.3.4.1 Channel Definition Structure (CHANNEL_DEF)
                        name, options, channelDefArray = util.PackedStruct('<8sI').unpack(channelDefArray)

                        if options and not(options >> 16):
                            self._env.log1('\tFixing rdesktop bogus endian of encoding of options, was: %s', rdp_defs.CHANNEL_DEF_options(options))
                            options = (
                                (((options >> 0) & 0xff) << 24) |
                                (((options >> 8) & 0xff) << 16) |
                                (((options >> 16) & 0xff) << 8) |
                                (((options >> 24) & 0xff) << 0))
                        self._env.log1('\tRequested ChannelDef %s: %r %s', i, name, rdp_defs.CHANNEL_DEF_options(options))
                        self.ChannelDefs[i] = name.rstrip('\0')

                    assert not channelDefArray, (channelDefArray,)

                elif header_type.CS_CLUSTER:
                    # http://msdn.microsoft.com/en-us/library/cc240514 2.2.1.3.5 Client Cluster Data (TS_UD_CS_CLUSTER_flags)
                    Flags_int, RedirectedSessionID, rest = util.PackedStruct('<II').unpack(client_data_body)
                    Flags = rdp_defs.TS_UD_CS_CLUSTER_flags(Flags_int & ~rdp_defs.TS_UD_CS_CLUSTER_version.ServerSessionRedirectionVersionMask)
                    self._env.log1('\tFlags: %s', Flags)
                    if Flags.REDIRECTED_SESSIONID_FIELD_VALID:
                        version = rdp_defs.TS_UD_CS_CLUSTER_version(Flags_int & rdp_defs.TS_UD_CS_CLUSTER_version.ServerSessionRedirectionVersionMask)
                        self._env.log1('\tVersion: %s, RedirectedSessionID: 0x%x', version, RedirectedSessionID)
                    # LB redirect will return a SessionID to the client which will reconnect and set REDIRECTED_SESSIONID_FIELD_VALID and RedirectedSessionID
                    # NOTE: REDIRECTED_SESSIONID_FIELD_VALID seemed to control console/admin/personal login!?
                    if rest:
                        self._env.log2('\tUnknown rest: %r', rest) # 264*'\0' has been seen from Mac ...

                elif header_type.CS_MONITOR:
                    # http://msdn.microsoft.com/en-us/library/dd305336 2.2.1.3.6 Client Monitor Data (TS_UD_CS_MONITOR)
                    _flags_int, monitorCount, rest = util.PackedStruct('<II').unpack(client_data_body)
                    self._env.log1('\tmonitorCount: %s', monitorCount)
                    for i in range(monitorCount):
                        # http://msdn.microsoft.com/en-us/library/dd342324 2.2.1.3.6.1 Monitor Definition (TS_MONITOR_DEF)
                        left, top, right, bottom, flags, rest = util.PackedStruct('<IIIII').unpack(rest)
                        self._env.log1('\tmonitor %s: %s,%s - %s,%s %s', i, left, top, right, bottom, flags)
                    assert not rest, (rest,)

                else: # no header_type.X matched
                    self._env.log1('Unknown Client User Data Header: %s', header_type)
                    if self._env.config.STRICT:
                        assert False, header_type

            self._state = STATE_MCS_UNCONFIRMED
            new_x224_body = x224_body

        elif self._state == STATE_MCS_CONNECTED:
            domainMCSPDU = rdp_asn1.DomainMCSPDU.per_decode(x224_body)
            #self._env.log1('DomainMCSPDU:', pprint=domainMCSPDU)
            pdu_type = domainMCSPDU[0]

            if pdu_type == 'erectDomainRequest':
                erectDomainRequest = domainMCSPDU[1]
                # http://msdn.microsoft.com/en-us/library/cc240683 3.2.5.3.5 Sending MCS Erect Domain Request PDU
                # http://msdn.microsoft.com/en-us/library/cc240523 2.2.1.5 Client MCS Erect Domain Request PDU
                self._env.log1('Client MCS Erect Domain Request PDU')
                # mcsEDrq (5 bytes): PER-encoded MCS Domain PDU which encapsulates an MCS Erect Domain Request structure,
                # as specified in [T125] (the ASN.1 structure definitions are given in [T125] section 7, parts 3 and 10).

                # The client SHOULD initialize both the subHeight and subinterval fields of the MCS Erect Domain Request PDU to 0x01.
                self._env.log1('subHeight: %s', erectDomainRequest['subHeight'])
                self._env.log1('subInterval: %s', erectDomainRequest['subInterval'])

                new_domainMCSPDU = domainMCSPDU

            elif pdu_type == 'attachUserRequest':
                attachUserRequest = domainMCSPDU[1]
                # http://msdn.microsoft.com/en-us/library/cc240684 3.2.5.3.6 Sending MCS Attach User Request PDU
                # http://msdn.microsoft.com/en-us/library/cc240524 2.2.1.6 Client MCS Attach User Request PDU
                self._env.log1('Client MCS Attach User Request PDU')
                # mcsAUrq (1 byte): PER-encoded MCS Domain PDU which encapsulates an MCS Attach User Request structure,
                # as specified in [T125] (the ASN.1 structure definitions are given in [T125] section 7, parts 5 and 10).

                assert not attachUserRequest, attachUserRequest # mcsAUrq contains nothing

                new_domainMCSPDU = domainMCSPDU

            elif pdu_type == 'channelJoinRequest':
                channelJoinRequest = domainMCSPDU[1]
                # http://msdn.microsoft.com/en-us/library/cc240686 3.2.5.3.8 Sending MCS Channel Join Request PDU(s)
                # http://msdn.microsoft.com/en-us/library/cc240526 2.2.1.8 Client MCS Channel Join Request PDU
                self._env.log1('Client MCS Channel Join Request PDU')
                # domainMCSPDU is PER-encoded MCS Domain PDU which encapsulates an MCS Channel Join Request structure
                # as specified in [T125] (the ASN.1 structure definitions are given in [T125] section 7, parts 6 and 10).

                self._env.log1('initiator: %s', self.channel_name(channelJoinRequest['initiator'])) # The initiator field is initialized with the User Channel ID obtained during the processing of the MCS Attach User Confirm PDU and stored in the User Channel ID store.
                assert channelJoinRequest['initiator'] == self.UserChannelID, (channelJoinRequest, self.UserChannelID)
                channelId = channelJoinRequest['channelId'] # The channelId field is initialized with the MCS channel ID of the channel that is being joined.
                # Multiple MCS Channel Join Request PDUs are sent to join the following channels:
                self._env.log1('channelId: %s', self.channel_name(channelId))

                new_domainMCSPDU = domainMCSPDU

            elif pdu_type == 'sendDataRequest':
                sendDataRequest = domainMCSPDU[1]
                # Described in the description of the contained PDUs
                # mcsSDrq (variable): Variable-length PER-encoded MCS Domain PDU which encapsulates an MCS Send Data Request structure, as specified in [T125] (the ASN.1 structure definitions are given in [T125] section 7, parts 7 and 10).
                self._env.log1('Client MCS Send Data Request PDU')

                self._env.log1('\tinitiator: %s', self.channel_name(sendDataRequest['initiator']))
                assert sendDataRequest['initiator'] in [self.UserChannelID, 1002], (sendDataRequest, self.UserChannelID) # The embedded initiator field MUST be set to the MCS user channel ID (held in the User Channel ID store specified in section 3.2.1.4) - but Wyse sends 1002
                channelId = sendDataRequest['channelId']
                self._env.log1('\tchannelId: %s', self.channel_name(channelId))
                self._env.log1('\tdataPriority: %s', sendDataRequest['dataPriority']) # Client usually sends with "high" data priority
                self._env.log1('\tsegmentation: %s', ' '.join(sendDataRequest['segmentation']))

                # T125 specifies that SendDataRequest contains userData
                sendDataRequestUserData = sendDataRequest['userData']
                # We predict that the userData contains a security header ...
                # http://msdn.microsoft.com/en-us/library/cc240579 2.2.8.1.1.2.1 Basic (TS_SECURITY_HEADER)
                sec_flags_int, sec_flagsHi, security_payload = util.PackedStruct('<HH').unpack(sendDataRequestUserData)
                sec_flags = rdp_defs.TS_SECURITY_HEADER_flags(sec_flags_int)
                self._env.log1('\tsec_flags: %s', sec_flags)
                if sec_flags.SEC_FLAGSHI_VALID:
                    self._env.log1('\tflagsHi - reserved for future RDP needs: 0x%x', sec_flags.value)
                    if self._env.config.STRICT:
                        assert False, sec_flags.value
                #else: # can contain any 16-bit, unsigned integer value.

                if sec_flags.SEC_ENCRYPT:
                    assert not sec_flags.SEC_EXCHANGE_PKT, sec_flags # Can't happen; we don't have keys yet
                    self._env.log2('Encrypted PDU - decrypting')
                    # http://msdn.microsoft.com/en-us/library/cc240580 2.2.8.1.1.2.2 Non-FIPS (TS_SECURITY_HEADER1)
                    # We already parsed sec_flags of TS_SECURITY_HEADER from userData
                    # FIXME: Check type of security header, based on negotiated security level - but we assume it includes MAC and end encryption
                    header_mac, encrypted_security_payload = util.PackedStruct('<8s').unpack(security_payload)

                    self._env.log3('encrypted_security_payload:', dump=encrypted_security_payload) # good for checking that we get what the client sent
                    cleartext_security_payload = self.client_connection_state.client_server_xor(encrypted_security_payload)
                    self._env.log3('cleartext_security_payload:', dump=cleartext_security_payload)

                    self.client_connection_state.check_mac(cleartext_security_payload, header_mac, sec_flags.SEC_SECURE_CHECKSUM)

                else:
                    cleartext_security_payload = security_payload

                if channelId == self.IOChannelID:
                    # The embedded channelId field MUST be set to the MCS I/O channel ID (held in the I/O Channel ID store described in section 3.2.1.3)

                    if sec_flags.SEC_EXCHANGE_PKT:
                        # mstsc further sets SEC_LICENSE_ENCRYPT_CS | SEC_LICENSE_ENCRYPT_SC

                        # http://msdn.microsoft.com/en-us/library/cc240664 3.2.5.3.10 Sending Security Exchange PDU
                        # http://msdn.microsoft.com/en-us/library/cc240471 2.2.1.10 Client Security Exchange PDU
                        self._env.log1('Client Security Exchange PDU')
                        # We already parsed mcsSDrq - Variable-length PER-encoded MCS Domain PDU which encapsulates an MCS Send Data Request structure,
                        # as specified in [T125] (the ASN.1 structure definitions are given in [T125] section 7, parts 7 and 10).
                        # The userData field of the MCS Send Data Request contains the Security Exchange PDU data.

                        # http://msdn.microsoft.com/en-us/library/cc240472 2.2.1.10.1 Security Exchange PDU Data (TS_SECURITY_PACKET)
                        # We already parsed TS_SECURITY_HEADER from userData
                        length, padded_encryptedClientRandom = util.PackedStruct('<I').unpack(security_payload)
                        assert length == len(padded_encryptedClientRandom), (length, padded_encryptedClientRandom)

                        # FIXME: 8 trailing zero-bytes - is documented, but where and why???
                        assert padded_encryptedClientRandom[-8:] == '\0' * 8, (padded_encryptedClientRandom[-8:],)
                        encryptedClientRandom = padded_encryptedClientRandom[:-8]

                        # http://msdn.microsoft.com/en-us/library/cc240781 5.3.4 Client and Server Random Values
                        self._env.log3('encryptedClientRandom:', dump=encryptedClientRandom)
                        if not encryptedClientRandom.strip('\0'):
                            self._env.log1('encryptedClientRandom is all zeroes - looks like some kind of crypto problem') # for example if the key size is too small ...

                        encryptionLevel = self.server_connection_state.encryptionLevel
                        encryptionMethodKeyLength = self.server_connection_state.encryptionMethodKeyLength

                        self.client_connection_state.handle_client_security_exchange(
                            encryptedClientRandom,
                            encryptionLevel,
                            encryptionMethodKeyLength)

                        # Encrypt mitm_client_random with server_pub_key
                        encrypted_mitm_client_random = self.server_connection_state.get_encrypted_mitm_client_random()
                        self._env.log3('encrypted_random_client_mitm_serverside:', dump=encrypted_mitm_client_random)

                        padded_encrypted_random_client_mitm_serverside = encrypted_mitm_client_random + '\0' * 8
                        new_cleartext_security_payload = util.PackedStruct('<I').pack(
                            len(padded_encrypted_random_client_mitm_serverside)
                            ) + padded_encrypted_random_client_mitm_serverside

                    elif sec_flags.SEC_INFO_PKT:
                        # http://msdn.microsoft.com/en-us/library/cc240665 3.2.5.3.11 Sending Client Info PDU
                        # http://msdn.microsoft.com/en-us/library/cc240473 2.2.1.11 Client Info PDU
                        # http://msdn.microsoft.com/en-us/library/cc240474 2.2.1.11.1 Client Info PDU Data (CLIENT_INFO_PDU)
                        self._env.log1('Client Info PDU')
                        # We already parsed mcsSDrq - Variable-length PER-encoded MCS Domain PDU which encapsulates an MCS Send Data Request structure,
                        # as specified in [T125] (the ASN.1 structure definitions are given in [T125] section 7, parts 7 and 10).
                        # The userData field of the MCS Send Data Request contains the Clientdefs. Info PDU Data (section 2.2.1.11.1).

                        # http://msdn.microsoft.com/en-us/library/cc240475 2.2.1.11.1.1 Info Packet (TS_INFO_PACKET)
                        CodePage, info_flags_int, cbDomain, cbUserName, cbPassword, cbAlternateShell, cbWorkingDir, rest = (
                            util.PackedStruct('<LLHHHHH').unpack(cleartext_security_payload))
                        info_flags = rdp_defs.TS_INFO_PACKET_flags(info_flags_int)
                        assert info_flags.INFO_UNICODE, info_flags
                        # "This size excludes the length of the mandatory null terminator."
                        # "The field must contain at least a null terminator character in ANSI or Unicode format (depending on the presence of the INFO_UNICODE flag)."
                        Domain = rest[:cbDomain + 2]
                        rest = rest[cbDomain + 2:]
                        UserName = rest[:cbUserName + 2]
                        rest = rest[cbUserName + 2:]
                        Password = rest[:cbPassword + 2]
                        rest = rest[cbPassword + 2:]
                        AlternateShell = rest[:cbAlternateShell + 2]
                        rest = rest[cbAlternateShell + 2:]
                        WorkingDir = rest[:cbWorkingDir + 2]
                        rest = rest[cbWorkingDir + 2:]
                        extraInfo = rest

                        self._env.log1('CodePage: 0x%x', CodePage) # the active input locale identifier in the low word if INFO_UNICODE set
                        self._env.log1('Flags_int: %s', info_flags)
                        self._env.log1('Domain: %r', Domain.decode('utf-16le'))
                        self._env.log1('UserName: %r', UserName.decode('utf-16le'))
                        # Password is usually utf-16le encoded, but it might be 120 binary bytes from from redirection which can't be decoded.
                        self._env.log1('Password: %s', '*' * len(Password)) # Respect some privacy - and note that we count bytes not words
                        self._env.log1('AlternateShell: %r', AlternateShell.decode('utf-16le'))
                        self._env.log1('WorkingDir: %r', WorkingDir.decode('utf-16le'))

                        # http://msdn.microsoft.com/en-us/library/cc240476 2.2.1.11.1.1.1 Extended Info Packet (TS_EXTENDED_INFO_PACKET)
                        #self._env.log1('extraInfo:', dump=extraInfo)
                        clientAddressFamily, cbClientAddress, rest = util.PackedStruct('<HH').unpack(extraInfo)
                        clientAddress = rest[:cbClientAddress]
                        rest = rest[cbClientAddress:]
                        self._env.log1('clientAddressFamily: %s', clientAddressFamily)
                        if clientAddressFamily == 0x0002:
                            # RDP only uses TCP/IP, so this field MUST be set to AF_INET (0x0002). xfreerdp + 2008 update do however receive 0 + plenty of garbage.
                            try:
                                self._env.log1('clientAddress: %r', clientAddress.decode('utf-16le'))
                            except UnicodeDecodeError:
                                self._env.log1('clientAddress: %r (incorrectly encoded)', clientAddress) # extra non-unicode characters seen with RDC on 10.5

                            cbClientDir, rest = util.PackedStruct('<H').unpack(rest)
                            clientDir = rest[:cbClientDir]
                            rest = rest[cbClientDir:]
                            try:
                                self._env.log1('clientDir: %r', clientDir.decode('utf-16le'))
                            except UnicodeDecodeError:
                                self._env.log1('clientDir: %r (incorrectly encoded)', clientDir)

                            clientTimeZone, clientSessionId, performanceFlags, cbAutoReconnectLen, rest = util.PackedStruct('<172sLLH').unpack(rest)
                            autoReconnectCookie = rest[:cbAutoReconnectLen] # unused???
                            rest = rest[cbAutoReconnectLen:]
                            # http://msdn.microsoft.com/en-us/library/cc240477 2.2.1.11.1.1.1.1 Time Zone Information (TS_TIME_ZONE_INFORMATION)
                            self._env.log1('clientTimeZone:', dump=clientTimeZone)
                            self._env.log1('clientSessionId: 0x%x', clientSessionId) # This field was added in RDP 5.1 and is currently ignored by the server. It SHOULD be set to 0.
                            self._env.log1('performanceFlags: %s', rdp_defs.TS_EXTENDED_INFO_PACKET_performanceFlags(performanceFlags))
                            self._env.log1('autoReconnectCookie: %r', autoReconnectCookie) # attempting an automatic reconnection operation using a cookie stored in the Automatic Reconnection Cookie store (section 3.2.1.8)

                            # The spec is clear that either none or both of reserved1 and reserved2 must be present, but iRemoteDesktop and iRdesktop only sends one ...
                            if rest:
                                reserved1, rest = util.PackedStruct('<H').unpack(rest) # If this field is present, the reserved2 field MUST be present.
                                self._env.log1('reserved1: %s', reserved1 and hex(reserved1)) #  This field is reserved for future use and has no affect on RDP wire traffic. If this field is present, the reserved2 field MUST be present.
                            if rest:
                                reserved2, rest = util.PackedStruct('<H').unpack(rest) # This field MUST be present if the reserved1 field is present.
                                self._env.log1('reserved2: %s', reserved2 and hex(reserved2)) #  This field is reserved for future use and has no affect on RDP wire traffic. This field MUST be present if the reserved1 field is present.

                            if rest:
                                cbDynamicDSTTimeZoneKeyName, rest = util.PackedStruct('<H').unpack(rest)
                                dynamicDSTTimeZoneKeyName = rest[:cbDynamicDSTTimeZoneKeyName]
                                rest = rest[cbDynamicDSTTimeZoneKeyName:]
                                dynamicDaylightTimeDisabled, rest = util.PackedStruct('<H').unpack(rest)
                                try:
                                    self._env.log1('dynamicDSTTimeZoneKeyName: %r', dynamicDSTTimeZoneKeyName.decode('utf-16le'))
                                except UnicodeDecodeError:
                                    self._env.log1('dynamicDSTTimeZoneKeyName: %r (incorrectly encoded)', dynamicDSTTimeZoneKeyName)
                                self._env.log1('dynamicDaylightTimeDisabled: %r', bool(dynamicDaylightTimeDisabled))
                                # http://msdn.microsoft.com/en-us/library/cc240477%28v=prot.10%29.aspx 2.2.1.11.1.1.1.1 Time Zone Information (TS_TIME_ZONE_INFORMATION)
                                bias, standardName, standardDate, standardBias, daylightName, daylightDate, daylightBias, rest = util.PackedStruct('<I64s16sI64s16sI').unpack(rest)
                                self._env.log1('\tBias: %s', bias)
                                self._env.log1('\tStandardName: %r', standardName)
                                self._env.log1('\tStandardDate: %r', standardDate)
                                self._env.log1('\tStandardBias: %s', standardBias)
                                self._env.log1('\tDaylightName: %r', daylightName)
                                self._env.log1('\tDaylightDate: %r', daylightDate)
                                self._env.log1('\tDaylightBias: %s', daylightBias)
                                # followed by 42 zeros ...
                        else:
                            self._env.log1('ignoring bogus Extended Info Packet')

                        if self._env.config.PASSWORD:
                            self._env.log1('Setting UserName: %r Domain: %r', self._env.config.USERNAME, self._env.config.DOMAIN)
                            new_info_flags_int = rdp_defs.TS_INFO_PACKET_flags(info_flags_int, INFO_AUTOLOGON=True).value
                            # FIXME: Will fail if values not in pure ASCII - should decode from whatever encoding to unicode first
                            new_Domain = self._env.config.DOMAIN.encode('utf-16le')
                            new_UserName = self._env.config.USERNAME.encode('utf-16le')
                            new_Password = self._env.config.PASSWORD.encode('utf-16le')
                        else:
                            new_info_flags_int = info_flags.value
                            new_Domain = Domain
                            new_UserName = UserName
                            new_Password = Password

                        new_cleartext_security_payload = (
                            util.PackedStruct('<LLHHHHH').pack(CodePage, new_info_flags_int, len(new_Domain), len(new_UserName), len(new_Password), cbAlternateShell, cbWorkingDir) +
                            new_Domain + '\0\0' +
                            new_UserName + '\0\0' +
                            new_Password + '\0\0' +
                            AlternateShell +
                            WorkingDir +
                            extraInfo
                            )

                    elif sec_flags.SEC_LICENSE_PKT:
                        # http://msdn.microsoft.com/en-us/library/cc240667 3.2.5.3.12 Processing License Error PDU - Valid Client
                        # http://msdn.microsoft.com/en-us/library/cc241913 2.2.2 Licensing PDU (TS_LICENSING_PDU)
                        # http://msdn.microsoft.com/en-us/library/cc240479 2.2.1.12 Server License Error PDU - Valid Client
                        validClientLicenseData = cleartext_security_payload

                        self._env.log1('Client Licensing PDU')
                        # http://msdn.microsoft.com/en-us/library/cc241989 5.1.2 Client and Server Random Values and Premaster Secrets
                        # http://msdn.microsoft.com/en-us/library/cc746161 2.2.1.12.1 Valid Client License Data (LICENSE_VALID_CLIENT_DATA)
                        # http://msdn.microsoft.com/en-us/library/cc240480 2.2.1.12.1.1 Licensing Preamble (LICENSE_PREAMBLE)
                        bMsgType_int, bVersion_int, wMsgSize, validClientMessage = rdp_defs.LICENSING_PREAMPLE.unpack(validClientLicenseData)

                        bMsgType = rdp_defs.LICENSE_PREAMBLE_bMsgType(bMsgType_int) # A type of the licensing packet. For more details about the different licensing packets, see [MS-RDPELE].
                        self._env.log1('\tbMsgType: %s', bMsgType)
                        bVersion = rdp_defs.LICENSE_PREAMBLE_bVersion(bVersion_int)
                        self._env.log1('\tbVersion: %s', bVersion) # The license protocol version.

                        assert wMsgSize == len(validClientLicenseData), (wMsgSize, validClientLicenseData) # The size in bytes of the licensing packet (including the size of the preamble).

                        if bMsgType.LICENSE_INFO:
                            # http://msdn.microsoft.com/en-us/library/cc241919 2.2.2.3 Client License Information (CLIENT_LICENSE_INFO)
                            self._env.log1('Client License Information')
                            PreferredKeyExchangeAlg_int, PlatformId_int, ClientRandom, rest = util.PackedStruct('<LL32s').unpack(validClientMessage)
                            PreferredKeyExchangeAlg = rdp_defs.PreferredKeyExchangeAlg(PreferredKeyExchangeAlg_int)
                            assert PreferredKeyExchangeAlg.KEY_EXCHANGE_ALG_RSA, PreferredKeyExchangeAlg
                            PlatformId = rdp_defs.PlatformId_flags(PlatformId_int & 0xffff0000)
                            self._env.log1('PlatformId: %s, os build: 0x%x', PlatformId, PlatformId_int & 0xffff) # The remaining two bytes in the PlatformId field are used by the ISV to identify the build number of the operating system)
                            #assert PlatformId.CLIENT_IMAGE_ID_MICROSOFT, PlatformId # usually ...
                            self._env.log1('ClientRandom:', dump=ClientRandom)
                            self.client_connection_state.LicenseClientSecret = ClientRandom

                            # http://msdn.microsoft.com/en-us/library/cc240481 2.2.1.12.1.2 Licensing Binary Blob (LICENSE_BINARY_BLOB)
                            wBlobType_int, wBlobLen, rest = util.PackedStruct('<HH').unpack(rest)
                            if wBlobType_int:
                                wBlobType = rdp_defs.LICENSE_BINARY_BLOB_wBlobType(wBlobType_int)
                                assert wBlobType.BB_RANDOM_BLOB, wBlobType
                            else:
                                self._env.log1('Ignoring missing BB_RANDOM_BLOB flag')
                            EncryptedPreMasterSecret = rest[:wBlobLen]
                            rest = rest[wBlobLen:]
                            self._env.log1('EncryptedPreMasterSecret:', dump=EncryptedPreMasterSecret) # ends with 8 bytes zero-padding
                            self.client_connection_state.handle_EncryptedPreMasterSecret(EncryptedPreMasterSecret)
                            self._env.log1('Decrypted PreMasterSecret:', dump=self.client_connection_state.PreMasterSecret)

                            # http://msdn.microsoft.com/en-us/library/cc240481 2.2.1.12.1.2 Licensing Binary Blob (LICENSE_BINARY_BLOB)
                            wBlobType_int, wBlobLen, rest = util.PackedStruct('<HH').unpack(rest)
                            wBlobType = rdp_defs.LICENSE_BINARY_BLOB_wBlobType(wBlobType_int)
                            assert wBlobType.BB_DATA_BLOB, wBlobType
                            CAL = rest[:wBlobLen]
                            rest = rest[wBlobLen:]
                            self._env.log1('CAL:', dump=CAL)

                            # http://msdn.microsoft.com/en-us/library/cc240481 2.2.1.12.1.2 Licensing Binary Blob (LICENSE_BINARY_BLOB)
                            wBlobType_int, wBlobLen, rest = util.PackedStruct('<HH').unpack(rest)
                            wBlobType = rdp_defs.LICENSE_BINARY_BLOB_wBlobType(wBlobType_int)
                            assert wBlobType.BB_ENCRYPTED_DATA_BLOB or wBlobType.BB_DATA_BLOB, wBlobType # FIXME: rdesktop sends BB_DATA_BLOB!
                            EncryptedHWID = rest[:wBlobLen]
                            rest = rest[wBlobLen:]
                            self._env.log1('EncryptedHWID:', dump=EncryptedHWID)

                            MACData = rest
                            self._env.log1('MACData:', dump=MACData)
                            assert len(MACData) == 16, (MACData,)


                            new_ClientRandom = ClientRandom # reusing
                            new_EncryptedPreMasterSecret = self.server_connection_state.create_EncryptedPreMasterSecret(
                                self.client_connection_state.PreMasterSecret) # reusing but reencrypting

                            new_validClientMessage = (
                                util.PackedStruct('<LL32s').pack(PreferredKeyExchangeAlg_int, PlatformId_int, new_ClientRandom) +
                                util.PackedStruct('<HH').pack(rdp_defs.LICENSE_BINARY_BLOB_wBlobType.BB_RANDOM_BLOB,
                                    len(new_EncryptedPreMasterSecret)) + new_EncryptedPreMasterSecret +
                                util.PackedStruct('<HH').pack(rdp_defs.LICENSE_BINARY_BLOB_wBlobType.BB_DATA_BLOB,
                                    len(CAL)) + CAL +
                                util.PackedStruct('<HH').pack(rdp_defs.LICENSE_BINARY_BLOB_wBlobType.BB_ENCRYPTED_DATA_BLOB, # or BB_DATA_BLOB?
                                    len(EncryptedHWID)) + EncryptedHWID +
                                MACData # wrong ... perhaps
                                )

                        elif bMsgType.NEW_LICENSE_REQUEST:
                            # http://msdn.microsoft.com/en-us/library/cc241918 2.2.2.2 Client New License Request (CLIENT_NEW_LICENSE_REQUEST)
                            self._env.log1('Client New License Request')
                            PreferredKeyExchangeAlg_int, PlatformId_int, ClientRandom, rest = util.PackedStruct('<LL32s').unpack(validClientMessage)
                            PreferredKeyExchangeAlg = rdp_defs.PreferredKeyExchangeAlg(PreferredKeyExchangeAlg_int)
                            assert PreferredKeyExchangeAlg.KEY_EXCHANGE_ALG_RSA, PreferredKeyExchangeAlg
                            PlatformId = rdp_defs.PlatformId_flags(PlatformId_int & 0xffff0000)
                            self._env.log1('PlatformId: %s, os build: 0x%x', PlatformId, PlatformId_int & 0xffff) # The remaining two bytes in the PlatformId field are used by the ISV to identify the build number of the operating system)
                            self._env.log1('ClientRandom:', dump=ClientRandom)
                            self.client_connection_state.LicenseClientSecret = ClientRandom

                            # http://msdn.microsoft.com/en-us/library/cc240481 2.2.1.12.1.2 Licensing Binary Blob (LICENSE_BINARY_BLOB)
                            wBlobType_int, wBlobLen, rest = util.PackedStruct('<HH').unpack(rest)
                            if wBlobType_int:
                                wBlobType = rdp_defs.LICENSE_BINARY_BLOB_wBlobType(wBlobType_int)
                                assert wBlobType.BB_RANDOM_BLOB, wBlobType
                            else:
                                self._env.log1('Ignoring missing BB_RANDOM_BLOB flag')
                            EncryptedPreMasterSecret = rest[:wBlobLen]
                            rest = rest[wBlobLen:]
                            self._env.log1('EncryptedPreMasterSecret:', dump=EncryptedPreMasterSecret) # ends with 8 bytes zero-padding
                            self.client_connection_state.handle_EncryptedPreMasterSecret(EncryptedPreMasterSecret)
                            self._env.log1('Decrypted PreMasterSecret:', dump=self.client_connection_state.PreMasterSecret)

                            # http://msdn.microsoft.com/en-us/library/cc240481 2.2.1.12.1.2 Licensing Binary Blob (LICENSE_BINARY_BLOB)
                            wBlobType_int, wBlobLen, rest = util.PackedStruct('<HH').unpack(rest)
                            wBlobType = rdp_defs.LICENSE_BINARY_BLOB_wBlobType(wBlobType_int)
                            assert wBlobType.BB_CLIENT_USER_NAME_BLOB, wBlobType
                            ClientUserName = rest[:wBlobLen]
                            rest = rest[wBlobLen:]
                            self._env.log1('ClientUserName: %r', ClientUserName)

                            # http://msdn.microsoft.com/en-us/library/cc240481 2.2.1.12.1.2 Licensing Binary Blob (LICENSE_BINARY_BLOB)
                            wBlobType_int, wBlobLen, rest = util.PackedStruct('<HH').unpack(rest)
                            wBlobType = rdp_defs.LICENSE_BINARY_BLOB_wBlobType(wBlobType_int)
                            assert wBlobType.BB_CLIENT_MACHINE_NAME_BLOB, wBlobType
                            ClientMachineName = rest[:wBlobLen]
                            rest = rest[wBlobLen:]
                            self._env.log1('ClientMachineName: %r', ClientMachineName)

                            assert not rest, (rest,)

                            new_ClientRandom = ClientRandom # reusing
                            new_EncryptedPreMasterSecret = self.server_connection_state.create_EncryptedPreMasterSecret(
                                self.client_connection_state.PreMasterSecret) # reusing but reencrypting
                            self._env.log1('new_EncryptedPreMasterSecret:', dump=new_EncryptedPreMasterSecret) # ends with 8 bytes zero-padding

                            new_validClientMessage = (
                                util.PackedStruct('<LL32s').pack(PreferredKeyExchangeAlg_int, PlatformId_int, new_ClientRandom) +
                                util.PackedStruct('<HH').pack(rdp_defs.LICENSE_BINARY_BLOB_wBlobType.BB_RANDOM_BLOB,
                                    len(new_EncryptedPreMasterSecret)) + new_EncryptedPreMasterSecret +
                                util.PackedStruct('<HH').pack(rdp_defs.LICENSE_BINARY_BLOB_wBlobType.BB_CLIENT_USER_NAME_BLOB,
                                    len(ClientUserName)) + ClientUserName +
                                util.PackedStruct('<HH').pack(rdp_defs.LICENSE_BINARY_BLOB_wBlobType.BB_CLIENT_MACHINE_NAME_BLOB,
                                    len(ClientMachineName)) + ClientMachineName
                                )

                        elif bMsgType.PLATFORM_CHALLENGE_RESPONSE:
                            # http://msdn.microsoft.com/en-us/library/cc241922 2.2.2.5 Client Platform Challenge Response (CLIENT_PLATFORM_CHALLENGE_RESPONSE)
                            self._env.log1('Client Platform Challenge Response')
                            # http://msdn.microsoft.com/en-us/library/cc240481 2.2.1.12.1.2 Licensing Binary Blob (LICENSE_BINARY_BLOB)
                            wBlobType_int, wBlobLen, rest = util.PackedStruct('<HH').unpack(validClientMessage)
                            wBlobType = rdp_defs.LICENSE_BINARY_BLOB_wBlobType(wBlobType_int)
                            assert wBlobType.BB_ENCRYPTED_DATA_BLOB or wBlobType.BB_DATA_BLOB, wBlobType # FIXME: rdesktop sendsd BB_DATA_BLOB!
                            EncryptedPlatformChallengeResponse = rest[:wBlobLen]
                            rest = rest[wBlobLen:]
                            self._env.log1('EncryptedPlatformChallengeResponse:', dump=EncryptedPlatformChallengeResponse)

                            # http://msdn.microsoft.com/en-us/library/cc240481 2.2.1.12.1.2 Licensing Binary Blob (LICENSE_BINARY_BLOB)
                            wBlobType_int, wBlobLen, rest = util.PackedStruct('<HH').unpack(rest)
                            wBlobType = rdp_defs.LICENSE_BINARY_BLOB_wBlobType(wBlobType_int)
                            assert wBlobType.BB_ENCRYPTED_DATA_BLOB or wBlobType.BB_DATA_BLOB, wBlobType # FIXME: rdesktop sends BB_DATA_BLOB!
                            EncryptedHWID = rest[:wBlobLen]
                            rest = rest[wBlobLen:]
                            self._env.log1('EncryptedHWID:', dump=EncryptedHWID)

                            # http://msdn.microsoft.com/en-us/library/cc241995 5.1.6 MAC Generation
                            MACData = rest
                            self._env.log1('MACData:', dump=MACData)
                            assert len(MACData) == 16, (MACData,)

                            new_validClientMessage = validClientMessage

                        else:
                            self._env.log1('Unknown Client Licensing PDU 0x%x', bMsgType_int)
                            if self._env.config.STRICT:
                                assert False, bMsgType_int
                            new_validClientMessage = validClientMessage

                        new_cleartext_security_payload = rdp_defs.LICENSING_PREAMPLE.pack(
                            bMsgType_int, 2, len(new_validClientMessage) + rdp_defs.LICENSING_PREAMPLE.size
                            ) + new_validClientMessage

                    else:
                        assert not sec_flags.SEC_INFO_PKT and not sec_flags.SEC_LICENSE_PKT, sec_flags

                        # http://msdn.microsoft.com/en-us/library/cc240576 2.2.8.1.1.1.1 Share Control Header (TS_SHARECONTROLHEADER)
                        totalLength, pduType_versionLow, versionHigh, pduSource, pduData = util.PackedStruct('<HBBH').unpack(cleartext_security_payload)
                        assert totalLength == len(cleartext_security_payload), (totalLength, len(cleartext_security_payload))
                        pduType = rdp_defs.TS_SHARECONTROLHEADER_PDUTYPEs(pduType_versionLow & 0xf)
                        self._env.log1('pduType: %s', pduType)
                        versionLow = pduType_versionLow >> 4
                        self._env.log1('\tversion: high %02x low %02x', versionHigh, versionLow)
                        assert versionLow == 1, versionLow # versionLow (4 bits): Most significant 4 bits of the least significant byte. This field MUST be set to TS_PROTOCOL_VERSION (0x1).
                        assert versionHigh == 0, versionHigh # Most significant byte. This field MUST be set to 0x00.
                        self._env.log1('\tpduSource: %s', self.channel_name(pduSource)) # The channel ID which is the transmission source of the PDU.
                        assert pduSource in [self.UserChannelID, 1002], (pduSource, self.UserChannelID) # The pduSource of the embedded Share Control Header MUST be set to the MCS user channel ID held in the User Channel ID store, described in section 3.2.1.4. - but Wyse sends 1002

                        if pduType.PDUTYPE_DATAPDU:
                            self._env.log1('Share Data Header PDU')

                            # http://msdn.microsoft.com/en-us/library/cc240577 2.2.8.1.1.1.2 Share Data Header (TS_SHAREDATAHEADER)
                            shareId, _pad1, streamId, uncompressedLength, pduType2_int, compressedType_int, compressedLength, clientInputEventData = util.PackedStruct('<IBBHBBH').unpack(pduData)
                            self._env.log1('shareId: %s 0x%x', shareId, shareId) # Share identifier for the packet (see [T128] section 8.4.2 for more information about share IDs).
                            self._env.log1('streamId: %s', streamId) # The stream identifier for the packet.
                            self._env.log1('uncompressedLength: %s', uncompressedLength) # The uncompressed length of the packet in bytes.

                            pduType2 = rdp_defs.TS_SHAREDATAHEADER_PDUTYPE2(pduType2_int) # The type of data PDU.
                            self._env.log1('pduType2: %s', pduType2)

                            packet_compression = rdp_defs.TS_SHAREDATAHEADER_PACKET_COMPRESSION(compressedType_int & 0xf0)
                            self._env.log1('packet_compression: %s', packet_compression)
                            packet_compr_type = rdp_defs.TS_SHAREDATAHEADER_PACKET_COMPR_TYPE(compressedType_int & 0xf)
                            self._env.log1('packet_compr_type: %s', packet_compr_type)
                            self._env.log1('compressedLength: %s', compressedLength) # The compressed length of the packet in bytes.

                            if pduType2.PDUTYPE2_INPUT:
                                # http://msdn.microsoft.com/en-us/library/cc240582 2.2.8.1.1.3 Client Input Event PDU (TS_INPUT_PDU)
                                self._env.log1('Client Input Event PDU')
                                # http://msdn.microsoft.com/en-us/library/cc746160 2.2.8.1.1.3.1 Client Input Event PDU Data (TS_INPUT_PDU_DATA)
                                #self._env.log1('clientInputEventData:', dump=clientInputEventData)
                                numberEvents, _pad2octets, slowPathInputEvents = util.PackedStruct('<HH').unpack(clientInputEventData)
                                self._env.log1('numberEvents: %s', numberEvents)

                                for i in range(numberEvents):
                                    # http://msdn.microsoft.com/en-us/library/cc240583 2.2.8.1.1.3.1.1 Slow-Path Input Event (TS_INPUT_EVENT)
                                    eventTime, messageType_int, slowPathInputData = util.PackedStruct('<IH').unpack(slowPathInputEvents)
                                    messageType = rdp_defs.TS_INPUT_EVENT_type(messageType_int)
                                    self._env.log1('Event: %s eventTime: %s messageType: %s', i, eventTime, messageType)

                                    if messageType.INPUT_EVENT_SCANCODE:
                                        # http://msdn.microsoft.com/en-us/library/cc240584 2.2.8.1.1.3.1.1.1 Keyboard Event (TS_KEYBOARD_EVENT)
                                        self._env.log1('\tKeyboard Event')
                                        keyboardFlags_int, keyCode, _pad2octets, slowPathInputEvents = util.PackedStruct('<HHH').unpack(slowPathInputData)
                                        keyboardFlags = rdp_defs.TS_KEYBOARD_EVENT_flags(keyboardFlags_int)
                                        self._env.log1('\tkeyboardFlags: %s', keyboardFlags)
                                        self._env.log1('\tkeyCode: %s', keyCode)

                                    elif messageType.INPUT_EVENT_SYNC:
                                        # http://msdn.microsoft.com/en-us/library/cc240588 # 2.2.8.1.1.3.1.1.5 Synchronize Event (TS_SYNC_EVENT)
                                        self._env.log1('\tSynchronize Event')
                                        _pad2octets, toggleFlags_int, slowPathInputEvents = util.PackedStruct('<HI').unpack(slowPathInputData)
                                        toggleFlags = rdp_defs.TS_SYNC_EVENT_toggleFlags(toggleFlags_int)
                                        self._env.log1('\ttoggleFlags: %s', toggleFlags)

                                    elif messageType.INPUT_EVENT_MOUSE:
                                        # http://msdn.microsoft.com/en-us/library/cc240586 2.2.8.1.1.3.1.1.3 Mouse Event (TS_POINTER_EVENT)
                                        self._env.log1('\tMouse Event')
                                        pointerFlags_int, xPos, yPos, slowPathInputEvents = util.PackedStruct('<HHH').unpack(slowPathInputData)
                                        pointerFlags = rdp_defs.TS_POINTER_EVENT_pointerFlags(pointerFlags_int)
                                        self._env.log1('\tpointerFlags: %s', pointerFlags) # The flags describing the pointer event. The possible flags are identical to those found in the pointerFlags field of the TS_POINTER_EVENT structure.
                                        self._env.log1('\txPos, yPos: %s, %s', xPos, yPos) # The x/y-coordinate of the pointer.

                                    elif messageType.INPUT_EVENT_UNICODE:
                                        # http://msdn.microsoft.com/en-us/library/cc240585 Unicode Keyboard Event (TS_UNICODE_KEYBOARD_EVENT)
                                        _pad2OctetsA, unicodeCode, _pad2OctetsB, slowPathInputEvents = util.PackedStruct('<HHH').unpack(slowPathInputData)
                                        self._env.log1('\tunicodeCode: 0x%x', unicodeCode) # The Unicode character input code.

                                    else:
                                        self._env.log1('Unknown messageType: %s', messageType)
                                        if self._env.config.STRICT:
                                            assert False, messageType

                                assert not slowPathInputEvents, (slowPathInputEvents,)

                            elif pduType2.PDUTYPE2_SUPPRESS_OUTPUT:
                                # http://msdn.microsoft.com/en-us/library/cc240647 2.2.11.3 Client Suppress Output PDU
                                # http://msdn.microsoft.com/en-us/library/cc240648 2.2.11.3.1 Suppress Output PDU Data (TS_SUPPRESS_OUTPUT_PDU)
                                self._env.log1('Suppress Output PDU Data') # TODO:

                            elif pduType2.PDUTYPE2_SYNCHRONIZE:
                                # http://msdn.microsoft.com/en-us/library/cc240671 3.2.5.3.14 Sending Synchronize PDU
                                # http://msdn.microsoft.com/en-us/library/cc240489 2.2.1.14 Client Synchronize PDU
                                # http://msdn.microsoft.com/en-us/library/cc240729 3.3.5.3.14 Processing Synchronize PDU
                                self._env.log1('Client Synchronize PDU')

                                if not sec_flags.SEC_IGNORE_SEQNO:
                                    self._env.log1('No SEC_IGNORE_SEQNO!') # The flags field of the security header SHOULD contain the SEC_IGNORE_SEQNO flag (see section 2.2.8.1.1.2.1).

                                synchronizePduData = clientInputEventData # The contents of the Synchronize PDU, as specified in section 2.2.1.14.1.
                                # http://msdn.microsoft.com/en-us/library/cc240490 2.2.1.14.1 Synchronize PDU Data (TS_SYNCHRONIZE_PDU)
                                messageType, targetUser, rest = util.PackedStruct('<HH').unpack(synchronizePduData)
                                assert not rest, (rest,)
                                assert messageType == 1, messageType # The message type. This field MUST be set to SYNCMSGTYPE_SYNC (1).
                                # The contents of the targetUser field MUST be ignored.
                                # The targetUser field SHOULD be set to the MCS server channel ID (held in the Server Channel ID store (section 3.2.1.5)).
                                self._env.log1('targetUser: %s', self.channel_name(targetUser))

                            elif pduType2.PDUTYPE2_CONTROL:
                                self._env.log1('Client Control PDU ...')
                                controlPduData = clientInputEventData # The actual contents of the Control PDU, as specified in section 2.2.1.15.1.
                                # http://msdn.microsoft.com/en-us/library/cc240492 2.2.1.15.1 Control PDU Data (TS_CONTROL_PDU)
                                action_int, grantId, controlId, rest = util.PackedStruct('<HHI').unpack(controlPduData)
                                assert not rest, (rest,)
                                action = rdp_defs.TS_CONTROL_PDU_action(action_int) # The action code.
                                self._env.log1('action: %s', action)
                                self._env.log1('grantId: %s', self.channel_name(grantId))
                                self._env.log1('controlId: %s', self.channel_name(controlId))

                                if action.CTRLACTION_COOPERATE:
                                    self._env.log1('Client Control PDU - Cooperate')
                                    # http://msdn.microsoft.com/en-us/library/cc240672 3.2.5.3.15 Sending Control PDU - Cooperate
                                    # The grantId and controlId fields SHOULD be set to zero.
                                    # http://msdn.microsoft.com/en-us/library/cc240491 2.2.1.15 Client Control PDU - Cooperate
                                    # The grantId and controlId fields of the Control PDU Data MUST both be set to zero
                                    assert not grantId, grantId # The grant identifier.
                                    assert not controlId, controlId # The control identifier.
                                elif action.CTRLACTION_REQUEST_CONTROL:
                                    self._env.log1('Client Control PDU - Request Control')
                                    # http://msdn.microsoft.com/en-us/library/cc240673 3.2.5.3.16 Sending Control PDU - Request Control
                                    # The grantId and controlId fields SHOULD be set to zero.
                                    # http://msdn.microsoft.com/en-us/library/cc240493 2.2.1.16 Client Control PDU - Request Control
                                    # The grantId and controlId fields of the Control PDU Data MUST both be set to zero
                                    assert not grantId, grantId # The grant identifier.
                                    assert not controlId, controlId # The control identifier.
                                else:
                                    self._env.log1('Unknown Client Control Action: %s', action)
                                    if self._env.config.STRICT:
                                        assert False, action

                            elif pduType2.PDUTYPE2_BITMAPCACHE_PERSISTENT_LIST:
                                # http://msdn.microsoft.com/en-us/library/cc240674 3.2.5.3.17 Sending Persistent Key List PDU(s)
                                # http://msdn.microsoft.com/en-us/library/cc240494 2.2.1.17 Client Persistent Key List PDU
                                self._env.log1('Client Persistent Key List PDU')

                                # http://msdn.microsoft.com/en-us/library/cc240495 2.2.1.17.1 Persistent Key List PDU Data (TS_BITMAPCACHE_PERSISTENT_LIST_PDU)
                                # TODO

                            elif pduType2.PDUTYPE2_FONTLIST:
                                # http://msdn.microsoft.com/en-us/library/cc240675 3.2.5.3.18 Sending Font List PDU
                                # http://msdn.microsoft.com/en-us/library/cc240497 2.2.1.18 Client Font List PDU
                                self._env.log1('Client Font List PDU')

                                # http://msdn.microsoft.com/en-us/library/cc240498 2.2.1.18.1 Font List PDU Data (TS_FONT_LIST_PDU)
                                # TODO

                            elif pduType2.PDUTYPE2_SHUTDOWN_REQUEST:
                                # http://msdn.microsoft.com/en-us/library/cc240530 2.2.2.2 Client Shutdown Request PDU
                                self._env.log1('Client Shutdown Request PDU')

                                shutdownRequestPduData = clientInputEventData
                                # http://msdn.microsoft.com/en-us/library/cc240531 2.2.2.2.1 Shutdown Request PDU Data (TS_SHUTDOWN_REQ_PDU)
                                assert not shutdownRequestPduData, (shutdownRequestPduData,) # no PDU body.

                            elif pduType2.PDUTYPE2_REFRESH_RECT:
                                # http://msdn.microsoft.com/en-us/library/cc240646 2.2.11.2.1 Refresh Rect PDU Data (TS_REFRESH_RECT_PDU)
                                self._env.log1('Client Refresh Rect PDU')

                                # TODO

                            else:
                                self._env.log1('Unknown pduType2: %s', pduType2, dump=clientInputEventData)
                                if self._env.config.STRICT:
                                    assert False, pduType2

                        elif pduType.PDUTYPE_CONFIRMACTIVEPDU:
                            # http://msdn.microsoft.com/en-us/library/cc240670 3.2.5.3.13.2 Sending Confirm Active PDU
                            # http://msdn.microsoft.com/en-us/library/cc240487 2.2.1.13.2 Client Confirm Active PDU
                            # - is response to the Demand Active PDU.

                            # http://msdn.microsoft.com/en-us/library/cc240488 2.2.1.13.2.1 Confirm Active PDU Data (TS_CONFIRM_ACTIVE_PDU)
                            # http://msdn.microsoft.com/en-us/library/cc240486 2.2.1.13.1.1.1 Capability Set (TS_CAPS_SET)
                            self._env.log1('Client Confirm Active PDU')
                            shareId, originatorId, lengthSourceDescriptor, lengthCombinedCapabilities, rest = util.PackedStruct('<IHHH').unpack(pduData)
                            sourceDescriptor = rest[:lengthSourceDescriptor]
                            rest = rest[lengthSourceDescriptor:]
                            assert len(rest) == lengthCombinedCapabilities, (rest, lengthCombinedCapabilities)
                            numberCapabilities, _pad2octets, capabilitySets = util.PackedStruct('<HH').unpack(rest)

                            self._env.log1('shareId: %s 0x%x', shareId, shareId) # The share identifier for the packet (see [T128] section 8.4.2 for more information regarding share IDs).
                            self._env.log1('originatorId: %s', self.channel_name(originatorId)) # The identifier of the packet originator.
                            if (originatorId, self.ServerChannelID) == (1002, 0):
                                self._env.log1('Client Confirm Active PDU on channel 1002 (rdesktop?) but should be 0 (VirtualBox?)')
                            else:
                                assert originatorId == self.ServerChannelID, (originatorId, self.ServerChannelID) # This field MUST be set to the server channel ID (in Microsoft RDP server implementations, this value is always 0x03EA=1002).
                            self._env.log1('sourceDescriptor: %r', sourceDescriptor) # Source descriptor. The Microsoft RDP client sets the contents of this field to { 0x4D, 0x53, 0x54, 0x53, 0x43, 0x00 }, which is the ANSI representation of the null-terminated string "MSTSC" in hexadecimal.
                            #self._env.log1('numberCapabilities: %s', numberCapabilities) # Number of capability sets included in the Confirm Active PDU.
                            #self._env.log1('capabilitySets:', dump=capabilitySets) # (variable): An array of TS_CAPS_SET (section 2.2.1.13.1.1.1) structures. Collection of capability sets, each conforming to the TS_CAPS_SET structure. The number of capability sets is specified by the numberCapabilities field.

                            self.analyze_capability_set(numberCapabilities, capabilitySets)

                        else:
                            self._env.log1('Unknown pduType: %s', pduType, dump=pduData)
                            if self._env.config.STRICT:
                                assert False, pduType

                        new_cleartext_security_payload = cleartext_security_payload # just re-encode our input

                    # end of encrypted sec_flags cases

                else: # not IOChannelID
                    self._env.log1('Not processing data to this channel')
                    new_cleartext_security_payload = cleartext_security_payload # just re-encode our input

                if sec_flags.SEC_ENCRYPT:
                    # Re-encrypt new cleartext_security_payload
                    new_mac = self.server_connection_state.generate_mac(new_cleartext_security_payload, secure=sec_flags.SEC_SECURE_CHECKSUM)
                    new_encrypted_security_payload = self.server_connection_state.client_server_xor(new_cleartext_security_payload)
                    self._env.log3('new_encrypted_security_payload:', dump=new_encrypted_security_payload)

                    new_security_payload = new_mac + new_encrypted_security_payload # simple concatenation

                else:
                    new_security_payload = new_cleartext_security_payload

                # http://msdn.microsoft.com/en-us/library/cc240579 2.2.8.1.1.2.1 Basic (TS_SECURITY_HEADER)
                new_sendDataRequestUserData = util.PackedStruct('<HH').pack(sec_flags.value, sec_flagsHi) + new_security_payload

                new_sendDataRequest = dict(sendDataRequest) # make a copy we can modify
                new_sendDataRequest['userData'] = new_sendDataRequestUserData # selected overwrite in copy
                new_domainMCSPDU = (pdu_type, new_sendDataRequest)

            elif pdu_type == 'disconnectProviderUltimatum':
                disconnectProviderUltimatum = domainMCSPDU[1]
                # http://msdn.microsoft.com/en-us/library/cc240900 3.1.5.1.1 Sending of MCS Disconnect Provider Ultimatum PDU
                # http://msdn.microsoft.com/en-us/library/cc240890 3.1.5.1.2 Processing of MCS Disconnect Provider Ultimatum PDU
                # http://msdn.microsoft.com/en-us/library/cc240529 2.2.2.1 MCS Disconnect Provider Ultimatum PDU
                # MSDN is WRONG; it messes DisconnectProviderUltimatum and RejectMCSPDUUltimatum up
                self._env.log1('DisconnectProviderUltimatum, Reason: %s', disconnectProviderUltimatum['reason'])
                # Only the rn- provider-initiated (1) or rn-user-requested (3) reason codes SHOULD be used in the reason field.

                new_domainMCSPDU = domainMCSPDU

            else:
                self._env.log1('unexpected pdu_type: %s', pdu_type, pprint=domainMCSPDU)
                if self._env.config.STRICT:
                    assert False, pdu_type
                new_domainMCSPDU = domainMCSPDU

            new_x224_body = rdp_asn1.DomainMCSPDU.per_encode(new_domainMCSPDU)

        else:
            self._env.log1('Not ready for data from client in state: %s', self._state)
            assert False, self._state

        return new_x224_body

    @util.exceptor
    def ServerClientPdu(self, x224_body):

        if self._state == STATE_MCS_UNCONFIRMED:
            # http://msdn.microsoft.com/en-us/library/cc240682 3.2.5.3.4 Processing MCS Connect Response PDU with GCC Conference Create Response
            # http://msdn.microsoft.com/en-us/library/cc240515 2.2.1.4 Server MCS Connect Response PDU with GCC Conference Create Response
            self._env.log1('Server MCS Connect Response PDU with GCC Conference Create Response')
            # mcsCrsp (variable): Variable-length BER-encoded MCS Connect Response structure (using definite-length encoding)
            # as described in [T125] (the ASN.1 structure definition is detailed in [T125] section 7, part 2).
            connectMCSPDU = rdp_asn1.ConnectMCSPDU.ber_decode(x224_body)
            #self._env.log1('connectMCSPDU:', pprint=connectMCSPDU)
            # The client ignores the calledConnectId and domainParameters fields of this PDU.
            (choice_tag, connectResponse) = connectMCSPDU
            assert choice_tag == 'connect-response', choice_tag
            result = connectResponse['result']
            if result == 'rt_unspecified_failure':
                self._env.log1('connectResponse result: %r', result)
                return x224_body # seen after protocol negation failure, and also when trying to connect to booting vista
            assert result == 'rt_successful', result # rt_unspecified_failure also seen after protocol negation failure
            # The userData field of the MCS Connect Response encapsulates the GCC Conference Create Response data
            # (contained in the gccCCrsp and subsequent fields).
            connectResponseUserData = connectResponse['userData']

            # gccCCrsp (variable): Variable-length PER-encoded GCC Connect Data structure ...
            connectData, index = rdp_asn1.ConnectData.per_decode(connectResponseUserData, return_index=True)
            #self._env.log1('connectData:', pprint=connectData)
            # (t124Identifier shall be set to the value {itu-t recommendation t 124 version(0) 1} )
            assert connectData['t124Identifier'][0] == 'object', connectData
            assert connectData['t124Identifier'][1][0] == (0, 0, 20, 124, 0, 1), connectData
            # Microsoft RDP Servers incorrectly hard-code the length of the MCS Connect Response PDU user data as 0x2A (42) bytes - the client SHOULD ignore this incorrect length and MUST NOT generate an error.
            connectPDU42 = connectData['connectPDU'] # Note: that connectPDU length is hard-coded to 42 by the server and ignored by the client!!!
            assert ord(connectResponseUserData[7]) == 42, (connectResponseUserData,) # No; this is NOT the answer, no matter what the question is!
            connectPDU = connectResponseUserData[index - len(connectPDU42):] # hack hack hack
            assert connectPDU.startswith(connectPDU42), (connectPDU, connectPDU42) # (in)sanity check

            # ... which encapsulates a Connect GCC PDU that contains a GCC Conference Create Response structure
            # as described in [T124] (the ASN.1 structure definitions are specified in [T124] section 8.7)
            # appended as user data to the MCS Connect Response (using the format specified in [T124] sections 9.5 and 9.6).

            connectGCCPDU = rdp_asn1.ConnectGCCPDU.per_decode(connectPDU)
            #self._env.log1('connectGCCPDU:', pprint=connectGCCPDU)
            assert connectGCCPDU[0] == 'conferenceCreateResponse', connectGCCPDU # choice name
            conferenceCreateResponse = connectGCCPDU[1] # choice value
            # The client ignores all of the GCC Conference Create Response fields, except for the userData field.
            conferenceCreateResponseUserData = conferenceCreateResponse['userData'] # The userData field of the GCC Conference Create Response ...
            assert len(conferenceCreateResponseUserData) == 1, conferenceCreateResponseUserData # ... contains one user data set ...
            connectGCCPDUuserDataOnly = conferenceCreateResponseUserData[0]
            assert connectGCCPDUuserDataOnly['key'] == ('h221NonStandard', 'McDn'), connectGCCPDUuserDataOnly # [junk food!]
            server_data_blocks = connectGCCPDUuserDataOnly['value'] # ... consisting of concatenated server data blocks.
            new_server_data_blocks = ''
            rest_server_data_blocks = server_data_blocks
            while rest_server_data_blocks:
                # http://msdn.microsoft.com/en-us/library/cc240516 2.2.1.4.1 User Data Header (TS_UD_HEADER) ->
                # http://msdn.microsoft.com/en-us/library/cc240509 2.2.1.3.1 User Data Header (TS_UD_HEADER)
                ts_ud_header_format = util.PackedStruct('<HH')
                header_type_int, l, _rest = ts_ud_header_format.unpack(rest_server_data_blocks)
                server_data_block_body = rest_server_data_blocks[ts_ud_header_format.size:l] # Note: l _includes_ the header length!
                rest_server_data_blocks = rest_server_data_blocks[l:]

                header_type = rdp_defs.TS_UD_HEADER_type(header_type_int)
                self._env.log1('header_type: %s', header_type)

                if header_type.SC_CORE:
                    # http://msdn.microsoft.com/en-us/library/cc240517 2.2.1.4.2 Server Core Data (TS_UD_SC_CORE)
                    version_, rest = util.PackedStruct('<I').unpack(server_data_block_body)
                    clientRequestedProtocols = 0 # In the event that an RDP Negotiation Request structure was not sent, this field MUST be initialized to PROTOCOL_RDP (0).
                    if rest:
                        clientRequestedProtocols, rest = util.PackedStruct('<I').unpack(rest)
                    earlyCapabilityFlags = 0
                    if rest:
                        earlyCapabilityFlags, rest = util.PackedStruct('<I').unpack(rest)
                    assert not rest, (rest,)

                    self._env.log1('\tVersion: %s', rdp_defs.TS_UD_clientVersions(version_))
                    self._env.log1('\tclientRequestedProtocols: %s', rdp_defs.RDP_NEG_REQ_requestedProtocols(clientRequestedProtocols))
                    self._env.log1('\tearlyCapabilityFlags: %s', rdp_defs.RNS_UD_SC_earlyCapabilityFlags(earlyCapabilityFlags))
                    # TODO: ensure that it contains the same flags that the client sent to the server in the RDP Negotiation Response (see section 3.2.5.3.1)
                    new_server_data_block_body = server_data_block_body

                elif header_type.SC_SECURITY:
                    # http://msdn.microsoft.com/en-us/library/cc240518 2.2.1.4.3 Server Security Data (TS_UD_SC_SEC1)
                    encryptionMethod_int, encryptionLevel_int, serverRandomLen, serverCertLen, rest = util.PackedStruct('<IIII').unpack(server_data_block_body)
                    assert serverRandomLen == 32, serverRandomLen # If this field does not contain a value of 32, the client SHOULD drop the connection.
                    server_random = rest[:serverRandomLen]
                    rest = rest[serverRandomLen:]
                    serverCertificate = rest[:serverCertLen]
                    rest = rest[serverCertLen:]
                    assert not rest, (rest,)

                    encryptionMethod = rdp_defs.TS_UD_SC_SEC1_encryptionMethod(encryptionMethod_int)
                    encryptionLevel = rdp_defs.TS_UD_SC_SEC1_encryptionLevel(encryptionLevel_int)
                    self._env.log1('\tencryptionMethod: %s', encryptionMethod)
                    self._env.log1('\tencryptionLevel: %s', encryptionLevel)

                    # The variable-length server random value used to derive session keys (see sections 5.3.4 and 5.3.5).
                    self.server_connection_state.handle_server_security_data(
                        server_random,
                        encryptionMethod,
                        encryptionLevel,
                        )

                    # http://msdn.microsoft.com/en-us/library/cc240521 2.2.1.4.3.1 Server Certificate (SERVER_CERTIFICATE)
                    #self._env.log1('\tserverCertificate:', dump=serverCertificate)
                    dwVersion_int, certData = util.PackedStruct('<I').unpack(serverCertificate)

                    dwVersion = rdp_defs.SERVER_CERTIFICATE_dwVersion(dwVersion_int)
                    self._env.log1('\tServer Certificate Version: %s', dwVersion)
                    #self._env.log1('certData:', dump=certData)

                    if dwVersion.CERT_CHAIN_VERSION_1: # This mode can be used without TS license and is used for admin/console logins
                        # http://msdn.microsoft.com/en-us/library/cc240519 2.2.1.4.3.1.1 Server Proprietary Certificate (PROPRIETARYSERVERCERTIFICATE)
                        self._env.log1('\tServer Proprietary Certificate')
                        # dwVersion already eaten
                        dwSigAlgId, dwKeyAlgId_int, wPublicKeyBlobType, wPublicKeyBlobLen, rest = util.PackedStruct('<IIHH').unpack(certData)

                        assert dwSigAlgId == 1, dwSigAlgId # The signature algorithm identifier. This field MUST be set to SIGNATURE_ALG_RSA (0x00000001).
                        dwKeyAlgId = rdp_defs.PreferredKeyExchangeAlg(dwKeyAlgId_int)
                        assert dwKeyAlgId.KEY_EXCHANGE_ALG_RSA, dwKeyAlgId # The key algorithm identifier. This field MUST be set to KEY_EXCHANGE_ALG_RSA (0x00000001).

                        assert wPublicKeyBlobType == 6, wPublicKeyBlobType # The type of data in the PublicKeyBlob field. This field MUST be set to BB_RSA_KEY_BLOB (0x0006).

                        PublicKeyBlob = rest[:wPublicKeyBlobLen]

                        wSignatureBlobType, wSignatureBlobLen, SignatureKeyBlob = util.PackedStruct('<HH').unpack(rest[wPublicKeyBlobLen:])

                        assert wSignatureBlobType == 8, wSignatureBlobType # The type of data in the SignatureKeyBlob field. This field is set to BB_RSA_SIGNATURE_BLOB (0x0008).
                        assert len(SignatureKeyBlob) == wSignatureBlobLen, (SignatureKeyBlob, wSignatureBlobLen)

                        self._env.log1('\tPublicKeyBlob:', dump=PublicKeyBlob)
                        # http://msdn.microsoft.com/en-us/library/cc240520 2.2.1.4.3.1.1.1 RSA Public Key (RSA_PUBLIC_KEY)
                        magic, keylen, bitlen, datalen, pubExp, modulus = util.PackedStruct('<IIIII').unpack(PublicKeyBlob)

                        assert magic == 0x31415352, magic # The sentinel value. This field MUST be set to 0x31415352 ("RSA1" in ANSI when the bytes are arranged in little-endian order).
                        self._env.log1('\tbitlen: %s 0x%x', bitlen, bitlen) # The number of bits in the public key modulus.
                        assert keylen == ((bitlen / 8) + 8), (keylen, bitlen) # The size in bytes of the modulus field. This value is directly related to the bitlen field and MUST be ((bitlen / 8) + 8) bytes.
                        self._env.log1('\tdatalen: %s', datalen) # The maximum number of bytes that can be encoded using the public key.
                        assert datalen == bitlen / 8 - 1, (datalen, bitlen)
                        self._env.log1('\tpubExp: 0x%x', pubExp) # The public exponent of the public key.
                        self._env.log1('\tmodulus:', dump=modulus) # A variable-length array of bytes containing the public key modulus. The length in bytes of this field is given by the keylen field. The modulus field contains all (bitlen / 8) bytes of the public key modulus and 8 bytes of zero padding (which MUST follow after the modulus bytes).
                        assert len(modulus) == keylen, (modulus, keylen)

                        self._env.log1('\tSignatureKeyBlob:', dump=SignatureKeyBlob) # ends with 8 extra zeros, but doesn't matter because LE

                        # http://msdn.microsoft.com/en-us/library/cc240778 5.3.3.1.2 Signing a Proprietary Certificate
                        check_hash = rdp_crypto.MD5(
                            util.PackedStruct('<III').pack(dwVersion_int, dwSigAlgId, dwKeyAlgId_int) +
                            util.PackedStruct('<HH').pack(wPublicKeyBlobType, len(PublicKeyBlob)) + PublicKeyBlob)
                        padded_check_hash = check_hash + '\x00' + '\xff' * 45 + '\x01\x00'
                        self._env.log1('\tpadded_check_hash:', dump=padded_check_hash)

                        # http://msdn.microsoft.com/en-us/library/cc240776 5.3.3.1.1 Terminal Services Signing Key
                        de_signed_padded_hash = rdp_rsa.le_str_from_long(rdp_rsa.terminal_services_signing_key.encrypt(rdp_rsa.le_str_to_long(SignatureKeyBlob)))
                        self._env.log1('\tde_signed_padded_hash:', dump=de_signed_padded_hash)
                        # http://msdn.microsoft.com/en-us/library/cc240779 5.3.3.1.3 Validating a Proprietary Certificate
                        if de_signed_padded_hash == padded_check_hash:
                            pass
                        elif de_signed_padded_hash == check_hash:
                            self._env.log1('\tincorrect signature without padding - VirtualBox?')
                        else:
                            self._env.log1('\tcheck of servers pubkey sign failed %r vs %r', de_signed_padded_hash, padded_check_hash)
                            if self._env.config.STRICT:
                                assert False, (de_signed_padded_hash, padded_check_hash)

                        self.server_connection_state.set_terminal_server_pub_key(rdp_rsa.le_str_to_long(modulus), pubExp)

                        self._env.log1('\tUsing MITM signing key ...')
                        new_pubExp = self.client_connection_state.mitm_pub_key.public_exponent
                        new_modulus = rdp_rsa.le_str_from_long(self.client_connection_state.mitm_pub_key.modulus) + 8 * '\0'
                        self._env.log1('\tnew_modulus:', dump=new_modulus)

                        new_keylen = len(new_modulus)
                        new_bitlen = (new_keylen - 8) * 8
                        new_datalen = new_bitlen / 8 - 1
                        new_PublicKeyBlob = util.PackedStruct('<IIIII').pack(magic, new_keylen, new_bitlen, new_datalen, new_pubExp) + new_modulus

                        new_signed_blob = (
                            util.PackedStruct('<III').pack(dwVersion_int, dwSigAlgId, dwKeyAlgId_int) +
                            util.PackedStruct('<HH').pack(wPublicKeyBlobType, len(new_PublicKeyBlob)) + new_PublicKeyBlob)

                        new_hash = rdp_crypto.MD5(new_signed_blob)
                        padded_new_hash = new_hash + '\x00' + '\xff' * 45 + '\x01\x00'
                        self._env.log1('\tpadded_new_hash:', dump=padded_new_hash)
                        # http://msdn.microsoft.com/en-us/library/cc240776 5.3.3.1.1 Terminal Services Signing Key
                        signed_padded_new_hash = rdp_rsa.le_str_from_long(rdp_rsa.terminal_services_signing_key.decrypt(rdp_rsa.le_str_to_long(padded_new_hash)))
                        self._env.log1('\tsigned_padded_new_hash:', dump=signed_padded_new_hash)
                        signed_padded_new_hash = signed_padded_new_hash + 8 * '\x00'
                        assert len(signed_padded_new_hash) == len(SignatureKeyBlob), (len(signed_padded_new_hash), len(SignatureKeyBlob))

                        new_signature_octets = util.PackedStruct('<HH').pack(wSignatureBlobType, len(signed_padded_new_hash)) + signed_padded_new_hash

                        new_serverCertificate = new_signed_blob + new_signature_octets
                        self._env.log1('\tnew_serverCertificate:', dump=new_serverCertificate)

                    elif dwVersion.CERT_CHAIN_VERSION_2: # apparently this is used for Application Server Mode - license-controlled logins
                        # http://msdn.microsoft.com/en-us/library/cc240780 5.3.3.2 X.509 Certificate Chains
                        self._env.log1('\tX.509 Certificate Chains')
                        # The last certificate is the certificate of the server; the second-to-last is the license server's certificate, and so forth.
                        # More details on the structure of the chain and the component certificates are in section of [MS-RDPELE].
                        # http://msdn.microsoft.com/en-us/library/cc241880 [MS-RDPELE]: Remote Desktop Protocol: Licensing Extension
                        # http://msdn.microsoft.com/en-us/library/cc241988 5.1.1 X.509 Certificate
                        # http://msdn.microsoft.com/en-us/library/cc241889 1.3.2 X.509 Certificate Chains
                        # http://msdn.microsoft.com/en-us/library/cc241910 2.2.1.4.2 X.509 Certificate Chain (X509_CERTIFICATE_CHAIN)
                        NumCertBlobs, CertBlobArray = util.PackedStruct('<I').unpack(certData)

                        der_certs = []
                        verification_cert_octets = None # Start selfsigned - no cert to verify
                        signing_key = None # Start self-signed - no key to sign
                        signing_verification_cert_octets = None # Start selfsigned - no cert to verify

                        for cert_number in range(NumCertBlobs):
                            # http://msdn.microsoft.com/en-us/library/cc241911 2.2.1.4.2.1 CertBlob (CERT_BLOB)
                            cbCert, rest = util.PackedStruct('<I').unpack(CertBlobArray)
                            abCert = rest[:cbCert]
                            CertBlobArray = rest[cbCert:]

                            self._env.log1('\tx509 Cert chain index %s:', cert_number)
                            self._env.log3('\t\tabCert:', dump=abCert)
                            if self._env.config.CHECK_BER_ROUNDTRIP:
                                asn1.test_ber_roundtrip('Tag x509.Certificate abCert', x509.TaggedCertificate, abCert)

                            der_certs.append(abCert)
                            #self._env.log1('abCert x509.Certificate:', dump=abCert)

                            self._env.log1('\t\tVerifying certificate chain link')
                            try:
                                x509.verify(verification_cert_octets or abCert, abCert, logger=self._env.log2, cert_logger=self._env.log2)
                                self._env.log1('\t\tCertificate WAS correctly signed by its CA ...')
                            except AssertionError:
                                self._env.log1('\t\tCertificate NOT correctly signed by its CA ...')
                                raise

                            cert_struct = x509.TaggedCertificate.ber_decode(abCert)

                            # Chains ends with the _real_ public key from client_connection_state.mitm_pub_key
                            if cert_number == NumCertBlobs - 1:
                                self._env.log1('\t\tRetrieving terminal server pub key')
                                self._env.log2('\t\tcert_struct:', pprint=cert_struct)
                                public_key_octets = cert_struct['tbsCertificate']['subjectPublicKeyInfo']['subjectPublicKey'][1] # [0] == 'octets'
                                public_key_struct = x509.SubjectPublicKeyInfo_subjectPublicKey.ber_decode(public_key_octets)
                                self._env.log1('\t\tpublic_key_struct:', pprint=public_key_struct)
                                self.server_connection_state.set_terminal_server_pub_key(public_key_struct['mod'], public_key_struct['exp'])

                            # http://msdn.microsoft.com/en-us/library/cc241910 2.2.1.4.2 X.509 Certificate Chain (X509 _CERTIFICATE_CHAIN)
                            # The second-to-last element in the array is the license server certificate. An X.509 certificate used for signing CALs.
                            if cert_number == NumCertBlobs - 2:
                                # http://msdn.microsoft.com/en-us/library/cc241988 5.1.1 X.509 Certificate
                                self._env.log1('\t\tRetrieving license server pub key')
                                self._env.log2('\t\tcert_struct:', pprint=cert_struct)
                                public_key_octets = cert_struct['tbsCertificate']['subjectPublicKeyInfo']['subjectPublicKey'][1] # [0] == 'octets'
                                public_key_struct = x509.SubjectPublicKeyInfo_subjectPublicKey.ber_decode(public_key_octets)
                                self._env.log1('\t\tpublic_key_struct:', pprint=public_key_struct)
                                self.server_connection_state.set_terminal_server_pub_key(public_key_struct['mod'], public_key_struct['exp'])

                            verification_cert_octets = abCert # Next time verify their cert with this cert

                        # Padding (variable): A byte array of the length 8 + 4*NumCertBlobs is appended at the end the packet.
                        trailing_zeros = '\x00' * (8 + 4 * NumCertBlobs)
                        assert CertBlobArray == trailing_zeros, (CertBlobArray, trailing_zeros)

                        patched_key = self.client_connection_state.mitm_pub_key # FIXME: Could/should be different for each chain cert
                        new_CertBlobChunks = []
                        # Replace the sent certificate chain with a simple chain
                        # It seems like hacking TS license certs caused trouble with the MS Mac RDP client
                        der_certs = [x509.TaggedCertificate.der_encode(simple_ca_cert),
                                     x509.TaggedCertificate.der_encode(simple_ts_cert)]

                        for cert_number, abCert in enumerate(der_certs):

                            self._env.log1('\t\tPatching cert %s with MITM pub key and resign', cert_number)
                            new_abCert = x509.patch_sign(abCert, patched_key, signing_key or patched_key, logger=self._env.log2)

                            self._env.log1('\t\tVerifying patched cert correctly self-signed')
                            try:
                                x509.verify(signing_verification_cert_octets or new_abCert, new_abCert, logger=self._env.log2)
                                self._env.log1('\t\tPatched certificate was correctly signed ...')
                            except AssertionError:
                                self._env.log1('\t\tPatched certificate NOT correctly signed ...')
                                raise

                            signing_key = patched_key # Next time sign our patched cert with this cert
                            signing_verification_cert_octets = new_abCert # Next time verify our patched cert with this cert

                            new_CertBlobChunks.append(util.PackedStruct('<I').pack(len(new_abCert)) + new_abCert)

                        new_CertBlobArray = ''.join(new_CertBlobChunks) + '\x00' * (8 + 4 * len(der_certs))
                        new_certData = util.PackedStruct('<I').pack(len(der_certs)) + new_CertBlobArray
                        new_serverCertificate = util.PackedStruct('<I').pack(dwVersion_int | rdp_defs.SERVER_CERTIFICATE_dwVersion.TEMP) + new_certData

                    else:
                        self._env.log1('unknown dwVersion %s', dwVersion_int)
                        assert False, dwVersion_int

                    mitm_server_random = self.client_connection_state.get_mitm_server_random()
                    new_server_data_block_body = util.PackedStruct('<IIII').pack(
                        encryptionMethod_int, encryptionLevel_int, len(mitm_server_random), len(new_serverCertificate),
                        ) + mitm_server_random + new_serverCertificate

                elif header_type.SC_NET:
                    # http://msdn.microsoft.com/en-us/library/cc240522 2.2.1.4.4 Server Network Data (TS_UD_SC_NET)
                    # (These IDs will be used by the client when sending MCS Channel Join Request PDUs (see sections 2.2.1.8 and 3.2.5.3.8).)
                    MCSChannelId, channelCount, channelIdArray_rest = util.PackedStruct('<HH').unpack(server_data_block_body)

                    self.IOChannelID = MCSChannelId # The MCSChannelId field should be saved in the I/O Channel ID store (see section 3.2.1.3)
                    self._env.log1('\tAllocated I/O Channel, MCSChannelId: %s', self.IOChannelID)

                    # The MCS channel IDs returned in the channelIdArray should be saved in the Static Virtual Channel IDs store (see section 3.2.1.2)
                    for i in range(channelCount):
                        channelId, channelIdArray_rest = util.PackedStruct('<H').unpack(channelIdArray_rest)

                        self.StaticVirtualChannelIDs[channelId] = self.ChannelDefs.get(i, 'Unrequested channel %s' % i)
                        self._env.log1('\tAllocated channel for %s: %s', self.StaticVirtualChannelIDs[channelId], channelId)

                    if channelIdArray_rest:
                        assert len(channelIdArray_rest) == 2, (channelIdArray_rest,) # padding

                    new_server_data_block_body = server_data_block_body

                else: # no header_type.X matched
                    self._env.log1('Unknown Server User Data Header: %s', header_type)
                    if self._env.config.STRICT:
                        assert False, header_type
                    new_server_data_block_body = server_data_block_body

                new_server_data_blocks = new_server_data_blocks + ts_ud_header_format.pack(
                    header_type_int, ts_ud_header_format.size + len(new_server_data_block_body)
                    ) + new_server_data_block_body

            self._env.log1('No more Server data blocks')

            new_connectGCCPDUuserDataOnly = {
                'key': ('h221NonStandard', 'McDn'),
                'value': new_server_data_blocks,
                }

            new_conferenceCreateResponseUserData = [new_connectGCCPDUuserDataOnly] # sequence length one

            new_conferenceCreateResponse = dict(conferenceCreateResponse) # copy
            new_conferenceCreateResponse['userData'] = new_conferenceCreateResponseUserData

            new_connectGCCPDU = ('conferenceCreateResponse', new_conferenceCreateResponse) # choice

            new_connectPDU = rdp_asn1.ConnectGCCPDU.per_encode(new_connectGCCPDU) # PER encode

            new_connectData = dict(connectData) # copy
            new_connectData['connectPDU'] = new_connectPDU[:42] # Only encode 42 bytes ...

            # connectData only has 42 bytes of data ...
            new_connectResponseUserData42 = rdp_asn1.ConnectData.per_encode(new_connectData)
            assert new_connectResponseUserData42.endswith(new_connectPDU[:42]), (new_connectResponseUserData42, new_connectPDU) # the 42 bytes is at the end
            new_connectResponseUserData = new_connectResponseUserData42 + new_connectPDU[42:] # append the rest ...

            new_connectResponse = dict(connectResponse) # copy
            new_connectResponse['userData'] = new_connectResponseUserData
            new_connectMCSPDU = ('connect-response', new_connectResponse) # choice
            new_x224_body = rdp_asn1.ConnectMCSPDU.der_encode(new_connectMCSPDU) # DER encode

            self._state = STATE_MCS_CONNECTED

        elif self._state == STATE_MCS_CONNECTED:

            domainMCSPDU = rdp_asn1.DomainMCSPDU.per_decode(x224_body)
            #self._env.log1('DomainMCSPDU:', pprint=domainMCSPDU)
            pdu_type = domainMCSPDU[0]

            if pdu_type == 'attachUserConfirm':
                attachUserConfirm = domainMCSPDU[1]
                # http://msdn.microsoft.com/en-us/library/cc240685 3.2.5.3.7 Processing MCS Attach User Confirm PDU
                # http://msdn.microsoft.com/en-us/library/cc240525 2.2.1.7 Server MCS Attach User Confirm PDU
                self._env.log1('Server MCS Attach User Confirm PDU')
                # mcsAUcf (4 bytes): PER-encoded MCS Domain PDU which encapsulates an MCS Attach User Confirm structure,
                # as specified in [T125] (the ASN.1 structure definitions are given in [T125] section 7, parts 5 and 10).

                assert attachUserConfirm['result'] == 'rt_successful', attachUserConfirm
                self.UserChannelID = attachUserConfirm['initiator']
                self._env.log1('Allocated User Channel, initiator: %s', self.UserChannelID) # If the initiator is present, the client stores the value in the User Channel ID store (section 3.2.1.4).
                # Once the user channel ID has been extracted, the client MUST send an MCS Channel Join Request PDU for the user channel (section 3.2.5.3.8).

                new_domainMCSPDU = domainMCSPDU

            elif pdu_type == 'channelJoinConfirm':
                channelJoinConfirm = domainMCSPDU[1]
                # http://msdn.microsoft.com/en-us/library/cc240687 3.2.5.3.9 Processing MCS Channel Join Confirm PDU(s)
                # http://msdn.microsoft.com/en-us/library/cc240527 2.2.1.9 Client MCS Channel Join Confirm PDU
                self._env.log1('[Server] Client MCS Channel Join Confirm PDU')
                # mcsCJcf (8 bytes): PER-encoded MCS Domain PDU which encapsulates an MCS Channel Join Confirm PDU structure,
                # as specified in [T125] (the ASN.1 structure definitions are given in [T125] section 7, parts 6 and 10).

                assert channelJoinConfirm['result'] == 'rt_successful', channelJoinConfirm
                self._env.log1('initiator: %s', self.channel_name(channelJoinConfirm['initiator']))
                if channelJoinConfirm['initiator'] != self.UserChannelID:
                    self._env.log1('- ignoring that it isn\'t User Channel ID') # MAY be ignored
                self._env.log1('requested: %s', self.channel_name(channelJoinConfirm['requested'])) # MAY be ignored
                channelId = channelJoinConfirm['channelId']
                self._env.log1('channelId: %s', self.channel_name(channelId))
                # If the value of the channelId field does not correspond with the value of the channelId field sent in the previous MCS Channel Join Request PDU (section 2.2.1.8) the connection SHOULD be dropped.

                new_domainMCSPDU = domainMCSPDU

            elif pdu_type == 'sendDataIndication':
                self._env.log1('Server sendDataIndication')
                # mcsSDin: MCS Send Data Indication PDU (see [T125] section 7, part 7)
                #self._env.log1('DomainMCSPDU:', pprint=domainMCSPDU)

                sendDataIndication = domainMCSPDU[1]
                self._env.log1('\tinitiator: %s', self.channel_name(sendDataIndication['initiator']))
                channelId = sendDataIndication['channelId']
                self._env.log1('\tchannelId: %s', self.channel_name(channelId))
                self._env.log1('\tdataPriority: %s', sendDataIndication['dataPriority']) # Server usually sends with "high" or "low" priority, but occasionally also the special "top", (could also be "medium")
                self._env.log1('\tsegmentation: %s', ' '.join(sendDataIndication['segmentation']))
                sendDataIndicationUserData = sendDataIndication['userData']

                # FIXME: The TS_SECURITY_HEADER structure is attached to server-to-client traffic when the Encryption Level selected by the server (see sections 5.3.2 and 2.2.1.4.3) is ENCRYPTION_LEVEL_LOW (1).
                # http://msdn.microsoft.com/en-us/library/cc240579 2.2.8.1.1.2.1 Basic (TS_SECURITY_HEADER)
                sec_flags_int, sec_flagsHi, security_payload = util.PackedStruct('<HH').unpack(sendDataIndicationUserData)
                sec_flags = rdp_defs.TS_SECURITY_HEADER_flags(sec_flags_int)
                self._env.log1('\tsec_flags: %s', sec_flags)
                if sec_flags.SEC_FLAGSHI_VALID:
                    self._env.log1('\tflagsHi - reserved for future RDP needs: 0x%x', sec_flags.value)
                    if self._env.config.STRICT:
                        assert False, sec_flags.value
                #else: # can contain any 16-bit, unsigned integer value.

                if sec_flags.SEC_ENCRYPT or sec_flags.SEC_REDIRECTION_PKT:
                    assert not sec_flags.SEC_INFO_PKT, sec_flags
                    self._env.log2('Encrypted PDU - decrypting')
                    # http://msdn.microsoft.com/en-us/library/cc240580 2.2.8.1.1.2.2 Non-FIPS (TS_SECURITY_HEADER1)
                    # We already parsed flags of TS_SECURITY_HEADER from userData
                    # FIXME: Check type of security header, based on negotiated security level - but we assume it includes MAC and end encryption
                    header_mac, encrypted_security_payload = util.PackedStruct('<8s').unpack(security_payload)

                    self._env.log3('encrypted_security_payload:', dump=encrypted_security_payload) # good for checking that we get what the client received
                    cleartext_security_payload = self.server_connection_state.server_client_xor(encrypted_security_payload)
                    self._env.log3('cleartext_security_payload:', dump=cleartext_security_payload)

                    self.server_connection_state.check_mac(cleartext_security_payload, header_mac, sec_flags.SEC_SECURE_CHECKSUM)

                else:
                    cleartext_security_payload = security_payload

                if sec_flags.SEC_REDIRECTION_PKT:
                    # http://msdn.microsoft.com/en-us/library/ee441959 2.2.13.2.1 Standard Security Server Redirection PDU (TS_STANDARD_SECURITY_SERVER_REDIRECTION)
                    self._env.log1('Standard Security Server Redirection PDU')
                    serverRedirectionPDU = cleartext_security_payload # Information required by the client to initiate a reconnection to a given session on a target server encapsulated in a Server Redirection Packet (section 2.2.13.1) structure.
                    # http://msdn.microsoft.com/en-us/library/ee443575 2.2.13.1 Server Redirection Packet (RDP_SERVER_REDIRECTION_PACKET)
                    REDIR_STRUCT = util.PackedStruct('<HHLL')
                    Flags, Length, SessionID, RedirFlags_int, rest = REDIR_STRUCT.unpack(serverRedirectionPDU)
                    assert Flags == rdp_defs.TS_SECURITY_HEADER_flags.SEC_REDIRECTION_PKT, Flags
                    assert Length == len(serverRedirectionPDU), (Length, serverRedirectionPDU)

                    self._env.log1('\tSessionID: %s', SessionID)
                    # SessionID (4 bytes): A 32-bit, unsigned integer. The session identifier to which the client MUST reconnect.
                    # This identifier MUST be specified in the RedirectSessionID [RedirectedSessionID] field of the Client Cluster Data (section 2.2.1.3.5) if a reconnect attempt takes place.
                    # The Client Cluster Data is transmitted as part of the MCS Connect Initial PDU with GCC Conference Create Request (section 2.2.1.3).
                    RedirFlags = rdp_defs.RedirFlags(RedirFlags_int)
                    self._env.log1('\tRedirFlags: %s', RedirFlags)

                    def parse_string(rest, uni=True):
                        l, rest = util.PackedStruct('<L').unpack(rest)
                        s = rest[:l]
                        assert s.endswith('\0\0'), (s,)
                        s = s[:-2]
                        if uni:
                            s = s.decode('utf-16le')
                        return s, rest[l:]

                    TargetNetAddress = None
                    if RedirFlags.LB_TARGET_NET_ADDRESS:
                        TargetNetAddress, rest = parse_string(rest)
                        self._env.log1('\tTargetNetAddress: %s', TargetNetAddress)
                    if RedirFlags.LB_LOAD_BALANCE_INFO:
                        LoadBalanceInfo, rest = parse_string(rest, uni=False)
                        self._env.log1('\tLoadBalanceInfo: %s', LoadBalanceInfo)
                        # Array of bytes containing load balancing information that MUST be treated as opaque data by the client and passed to the server
                        # if the LB_TARGET_NET_ADDRESS (0x00000001) flag is not present in the RedirFlags field and a reconnection takes place.
                    if RedirFlags.LB_USERNAME:
                        UserName, rest = parse_string(rest)
                        self._env.log1('\tUserName: %s', UserName)
                    if RedirFlags.LB_DOMAIN:
                        Domain, rest = parse_string(rest)
                        self._env.log1('\tDomain: %s', Domain)
                    if RedirFlags.LB_PASSWORD:
                        Password, rest = parse_string(rest, uni=False)
                        self._env.log1('\tPassword:', dump=Password) # apparently some kind of one-time-token - we dare to show it ...
                    if RedirFlags.LB_TARGET_FQDN:
                        TargetFQDN, rest = parse_string(rest)
                        self._env.log1('\tTargetFQDN: %s', TargetFQDN)
                    if RedirFlags.LB_TARGET_NETBIOS_NAME:
                        TargetNetBiosName, rest = parse_string(rest)
                        self._env.log1('\tTargetNetBiosName: %s', TargetNetBiosName)
                    if RedirFlags.LB_CLIENT_TSV_URL:
                        TsvUrl, rest = parse_string(rest)
                        self._env.log1('\tTsvUrl: %s', TsvUrl)
                        # The TsvUrl field is supported only on Windows 7 and Windows Server 2008 R2.
                        # If the client has previously sent a TsvUrl field in the LoadBalanceInfo to the server in the expected format, then the server will return the same TsvUrl to the client in this field.
                        # The client verifies that it is the same as the one that it previously passed to the server and if they do not match, the client immediately disconnects the connection.
                    if RedirFlags.LB_TARGET_NET_ADDRESSES:
                        # http://msdn.microsoft.com/en-us/library/ee442590 2.2.13.1.1 Target Net Addresses (TARGET_NET_ADDRESSES)
                        TargetNetAddressesLength, addressCount, rest = util.PackedStruct('<LL').unpack(rest)
                        self._env.log1('\tTargetNetAddresses: %s', addressCount)
                        # BSH: The following assertion could fail, if there is padding, as allowed by the protocol specification
                        assert len(rest) == TargetNetAddressesLength + 4, (len(rest), TargetNetAddressesLength)
                        for i in range(addressCount):
                            # http://msdn.microsoft.com/en-us/library/ee442109 2.2.13.1.1.1 Target Net Address (TARGET_NET_ADDRESS)
                            aTargetNetAddress, rest = parse_string(rest)
                            self._env.log1('\t\t%s', aTargetNetAddress)
                    if rest:
                        self._env.log1('\tpad:', dump=rest)

                    # Fake the redirection ...
                    #
                    # BSH: In Windows 2012 there is a new Remote desktop "collection" concept, with redirection based on "Session hint / TSVUrl"
                    # This is however not strictly necessary, since there is a workaround for clients not supporting it, so we leave it out, for now
                    # http://blogs.technet.com/b/askperf/archive/2015/06/11/walkthrough-on-session-hint-tsvurl-on-windows-server-2012.aspx
                    if TargetNetAddress:
                        self._env.log1('\tSet server address to loopback, so client reconnects to same port and we can connect to %s', TargetNetAddress)
                        self._env.config.set_config('SERVER_ADDR', TargetNetAddress)
                        new_RedirFlags_int = RedirFlags_int & (
                            rdp_defs.RedirFlags.LB_LOAD_BALANCE_INFO |
                            rdp_defs.RedirFlags.LB_USERNAME |
                            rdp_defs.RedirFlags.LB_DOMAIN |
                            rdp_defs.RedirFlags.LB_PASSWORD) # censor the 117 kinds of wild explicit target addressing
                        new_RedirFlags_int = new_RedirFlags_int | rdp_defs.RedirFlags.LB_TARGET_NET_ADDRESS # BSH: indicate that a single IP address will be used (necessary for Mac MRD to do the reconnect)

                        def pack_string(s, uni=True):
                            if uni:
                                s = s.encode('utf-16le')
                            return util.PackedStruct('<L').pack(len(s) + 2) + s + '\0\0'

                        tmp = ''
                        tmp += pack_string(self._env.config.RECONNECT_TARGETNETADDRESS) # BSH: LB_TARGET_NET_ADDRESS field with explicit addressing to local loopback (necessary for Mac MRD and rdesktop to do the reconnect)
                        if RedirFlags.LB_LOAD_BALANCE_INFO:
                            tmp += pack_string(LoadBalanceInfo, uni=False)
                        if RedirFlags.LB_USERNAME:
                            tmp += pack_string(UserName)
                        if RedirFlags.LB_DOMAIN:
                            tmp += pack_string(Domain)
                        if RedirFlags.LB_PASSWORD:
                            tmp += pack_string(Password, uni=False)

                        new_Length = REDIR_STRUCT.size + len(tmp)
                        new_serverRedirectionPDU = REDIR_STRUCT.pack(Flags, new_Length, SessionID, new_RedirFlags_int) + tmp

                        new_cleartext_security_payload = new_serverRedirectionPDU

                    else:
                        self._env.log1("\tNo TargetNetAddress received - can't redirect")
                        # BSH: The description of the field: LoadBalanceInfo however indicates that TargetNetAddress could be missing (see above). What to do in that situation?
                        new_cleartext_security_payload = cleartext_security_payload

                # The embedded channelId field within the mcsSDin is used to route the PDU to the appropriate target channel.
                elif channelId == self.IOChannelID:
                    # The embedded channelId field MUST be set to the MCS I/O channel ID (held in the I/O Channel ID store described in section 3.2.1.3)

                    if sec_flags.SEC_LICENSE_PKT: # The flags field of the security header MUST contain the SEC_LICENSE_PKT (0x0080) flag (see Basic (TS_SECURITY_HEADER)).
                        # TODO: If the SEC_LICENSE_ENCRYPT_CS (0x0200) flag is present, then the server is able to accept encrypted licensing packets when using Standard RDP Security mechanisms (see section 5.3). This fact is stored in the Server Licensing Encryption Ability store (section 3.2.1.9).
                        # FIXME: SEC_LICENSE_ENCRYPT_CS / SEC_LICENSE_ENCRYPT_SC
                        # http://msdn.microsoft.com/en-us/library/cc240667 3.2.5.3.12 Processing License Error PDU - Valid Client
                        # http://msdn.microsoft.com/en-us/library/cc241913 2.2.2 Licensing PDU (TS_LICENSING_PDU)
                        # http://msdn.microsoft.com/en-us/library/cc240479 2.2.1.12 Server License Error PDU - Valid Client
                        validClientLicenseData = cleartext_security_payload

                        self._env.log1('Server Licensing PDU')
                        # http://msdn.microsoft.com/en-us/library/cc241989 5.1.2 Client and Server Random Values and Premaster Secrets
                        # http://msdn.microsoft.com/en-us/library/cc746161 2.2.1.12.1 Valid Client License Data (LICENSE_VALID_CLIENT_DATA)
                        # http://msdn.microsoft.com/en-us/library/cc240480 2.2.1.12.1.1 Licensing Preamble (LICENSE_PREAMBLE)
                        bMsgType_int, bVersion_int, wMsgSize, validClientMessage = rdp_defs.LICENSING_PREAMPLE.unpack(validClientLicenseData)

                        bMsgType = rdp_defs.LICENSE_PREAMBLE_bMsgType(bMsgType_int) # A type of the licensing packet. For more details about the different licensing packets, see [MS-RDPELE].
                        self._env.log1('\tbMsgType: %s', bMsgType)
                        bVersion = rdp_defs.LICENSE_PREAMBLE_bVersion(bVersion_int)
                        self._env.log1('\tbVersion: %s', bVersion) # The license protocol version.

                        assert wMsgSize == len(validClientLicenseData), (wMsgSize, validClientLicenseData) # The size in bytes of the licensing packet (including the size of the preamble).

                        if bMsgType.LICENSE_REQUEST:
                            # http://msdn.microsoft.com/en-us/library/cc241914 2.2.2.1 Server License Request (SERVER_LICENSE_REQUEST)
                            self._env.log1('Server License Request')
                            ServerRandom, dwVersion, cbCompanyName, rest = util.PackedStruct('<32sLL').unpack(validClientMessage)
                            # ServerRandom (32 bytes): A 32-byte array containing a random number. This random number is created using a cryptographically secure pseudo-random number generator and is used to generate licensing encryption keys (see section 5.1.3). These keys are used to encrypt licensing data in subsequent licensing messages (see sections 5.1.4 and 5.1.5).
                            self._env.log1('ServerRandom:', dump=ServerRandom)
                            self.server_connection_state.LicenseServerRandom = ServerRandom
                            # ProductInfo (variable): A variable-length Product Information structure. This structure contains the details of the product license required for connecting to the terminal server.
                            # http://msdn.microsoft.com/en-us/library/cc241915 2.2.2.1.1 Product Information (PRODUCT_INFO)
                            #- dwVersion (4 bytes): A 32-bit unsigned integer that contains the license version information. The high-order word contains the major version of the operating system on which the terminal server is running while the low-order word contains the minor version.<4>
                            self._env.log1('dwVersion: 0x%x', dwVersion)
                            #- cbCompanyName (4 bytes): An unsigned 32-bit integer that contains the number of bytes in the pbCompanyName field, including the null terminator.
                            #- pbCompanyName (variable): Contains a null-terminated Unicode string that specifies the company name. For example, for licenses issued by Tailspin Toys, this string contains the text "Tailspin Toys".
                            pbCompanyName = rest[:cbCompanyName]
                            rest = rest[cbCompanyName:]
                            self._env.log1('pbCompanyName: %r', pbCompanyName.decode('utf-16le'))
                            #- cbProductId (4 bytes): An unsigned 32-bit integer that contains the number of bytes in the pbProductId field, including the null terminator.
                            #- pbProductId (variable): Contains a null-terminated Unicode string that identifies the type of the license that is required by the license server. It MAY have the following string value.
                            cbProductId, rest = util.PackedStruct('<L').unpack(rest)
                            pbProductId = rest[:cbProductId]
                            rest = rest[cbProductId:]
                            self._env.log1('pbProductId: %r', pbProductId.decode('utf-16le')) # "A02" = Per device license
                            # back to http://msdn.microsoft.com/en-us/library/cc241914 2.2.2.1 Server License Request (SERVER_LICENSE_REQUEST)

                            # KeyExchangeList (variable): A Licensing Binary BLOB structure (see [MS-RDPBCGR] section 2.2.1.12.1.2) of type BB_KEY_EXCHG_ALG_BLOB (0x000D). This BLOB contains the list of 32-bit unsigned integers specifying key exchange algorithms that the server supports. The terminal server supports only one key exchange algorithm as of now, so the BLOB contains the following value.
                            # http://msdn.microsoft.com/en-us/library/cc240481 2.2.1.12.1.2 Licensing Binary Blob (LICENSE_BINARY_BLOB)
                            wBlobType_int, wBlobLen, rest = util.PackedStruct('<HH').unpack(rest)
                            wBlobType = rdp_defs.LICENSE_BINARY_BLOB_wBlobType(wBlobType_int)
                            self._env.log1('wBlobType: %s', wBlobType)
                            blobData = rest[:wBlobLen]
                            rest = rest[wBlobLen:]
                            assert wBlobType.BB_KEY_EXCHG_ALG_BLOB, wBlobType # # The data type of the binary information. If wBlobLen is set to 0, then the contents of this field SHOULD be ignored.
                            alg_int, rest2 = util.PackedStruct('<L').unpack(blobData)
                            alg = rdp_defs.PreferredKeyExchangeAlg(alg_int)
                            self._env.log1('PreferredKeyExchangeAlg: %s', alg)
                            assert alg.KEY_EXCHANGE_ALG_RSA, alg #  Indicates RSA key exchange algorithm with a 512-bit asymmetric key.
                            assert not rest2, (rest2,)

                            # ServerCertificate (variable): A Licensing Binary BLOB structure (see [MS-RDPBCGR] section 2.2.1.12.1.2) of type BB_CERTIFICATE_BLOB (0x0003). This BLOB contains the terminal server certificate (see section 2.2.1.4). The terminal server can choose not to send the certificate by setting the wblobLen field in the Licensing Binary BLOB structure to 0. If encryption is in effect and is already protecting RDP traffic, the licensing protocol MAY<3> choose not to send the server certificate (for RDP security measures, see [MS-RDPBCGR] sections 5.3 and 5.4). If the licensing protocol chooses not to send the server certificate, then the client uses the public key obtained from the server certificate sent as part of Server Security Data in the Server MCS Connect Response PDU (see [MS-RDPBCGR] section 2.2.1.4).

                            # http://msdn.microsoft.com/en-us/library/cc240481 2.2.1.12.1.2 Licensing Binary Blob (LICENSE_BINARY_BLOB)
                            wBlobType_int, wBlobLen, rest = util.PackedStruct('<HH').unpack(rest)
                            wBlobType = rdp_defs.LICENSE_BINARY_BLOB_wBlobType(wBlobType_int)
                            self._env.log1('wBlobType: %s', wBlobType)
                            blobData = rest[:wBlobLen]
                            rest = rest[wBlobLen:]

                            if wBlobLen:
                                assert wBlobType.BB_CERTIFICATE_BLOB, wBlobType # The data type of the binary information. Ignore if no data.
                                self._env.log1('BB_CERTIFICATE_BLOB:', dump=blobData)
                            else: # If wBlobLen is set to 0, then the contents of this field SHOULD be ignored.
                                # If the licensing protocol chooses not to send the server certificate, then the client uses the public key obtained from the server certificate sent as part of Server Security Data in the Server MCS Connect Response PDU (see [MS-RDPBCGR] section 2.2.1.4).
                                self._env.log1('No license certificate specified - using connect certificate instead')
                            # ScopeList (variable): A variable-length Scope List structure that contains a list of entities that issued the client license. This list is used by the client in conjunction with ProductInfo to search for an appropriate license in its license store.
                            # http://msdn.microsoft.com/en-us/library/cc241916 2.2.2.1.2 Scope List (SCOPE_LIST)
                            ScopeCount, rest = util.PackedStruct('<L').unpack(rest)
                            self._env.log1('BB_CERTIFICATE_BLOB ScopeCount: %s', ScopeCount) # A 32-bit unsigned integer containing the number of elements in the ScopeArray field.
                            # http://msdn.microsoft.com/en-us/library/cc241917 2.2.2.1.2.1 Scope (SCOPE)

                            for i in range(ScopeCount):
                                # http://msdn.microsoft.com/en-us/library/cc240481 2.2.1.12.1.2 Licensing Binary Blob (LICENSE_BINARY_BLOB)
                                wBlobType_int, wBlobLen, rest = util.PackedStruct('<HH').unpack(rest)
                                wBlobType = rdp_defs.LICENSE_BINARY_BLOB_wBlobType(wBlobType_int)
                                blobData = rest[:wBlobLen]
                                rest = rest[wBlobLen:]
                                assert wBlobType.BB_SCOPE_BLOB, wBlobType
                                self._env.log1('BB_SCOPE_BLOB blobData: %r', blobData) # This BLOB contains the name of a license issuer in NULL-terminated ANSI character set string format.
                            assert not rest, (rest,)

                            new_validClientMessage = validClientMessage # we reuse ServerRandom

                        elif bMsgType.PLATFORM_CHALLENGE:
                            # http://msdn.microsoft.com/en-us/library/cc241921 2.2.2.4 Server Platform Challenge (SERVER_PLATFORM_CHALLENGE)
                            self._env.log1('Server Platform Challenge:')
                            ConnectFlags = validClientMessage[:4]
                            self._env.log1('ConnectFlags (unused):', dump=ConnectFlags)
                            rest = validClientMessage[4:]

                            # http://msdn.microsoft.com/en-us/library/cc240481 2.2.1.12.1.2 Licensing Binary Blob (LICENSE_BINARY_BLOB)
                            wBlobType_int, wBlobLen, rest = util.PackedStruct('<HH').unpack(rest)
                            wBlobType = rdp_defs.LICENSE_BINARY_BLOB_wBlobType(wBlobType_int)
                            # NOTE: The wBlobType field is unused in EncryptedPlatformChallenge (part of SERVER_PLATFORM_CHALLENGE PDU), which is sent by the server to the client
                            EncryptedPlatformChallenge = rest[:wBlobLen]
                            self._env.log1('EncryptedPlatformChallenge:', dump=EncryptedPlatformChallenge) # platform challenge data encrypted with the licensing encryption key using RC4
                            # Rumours indicate that the challenge is 54 00 45 00 53 00 54 00 00 00 "TEST\0"

                            lk = rdp_license.LicenseKeys(
                                self.client_connection_state.LicenseClientSecret,
                                self.server_connection_state.LicenseServerRandom,
                                self.client_connection_state.PreMasterSecret)
                            PlatformChallenge = lk.xor(EncryptedPlatformChallenge)
                            self._env.log1('PlatformChallenge:', dump=PlatformChallenge) # "TEST\0" in UTF-16

                            MACData = rest[wBlobLen:] # MD5 digest (MAC) generated over the unencrypted platform challenge BLOB
                            self._env.log1('MACData:', dump=MACData)
                            assert len(MACData) == 16, (MACData,)

                            #new_validClientMessage = (
                            #    ConnectFlags +
                            #    util.PackedStruct('<HH').pack(wBlobType_int, len(EncryptedPlatformChallenge)) +
                            #    EncryptedPlatformChallenge +
                            #    new_MACData)
                            new_validClientMessage = validClientMessage

                        elif bMsgType.NEW_LICENSE or bMsgType.UPGRADE_LICENSE:
                            if bMsgType.NEW_LICENSE:
                                # http://msdn.microsoft.com/en-us/library/cc241926 2.2.2.7 Server New License (SERVER_NEW_LICENSE)
                                self._env.log1('Server New License:')
                            else: # UPGRADE_LICENSE
                                # http://msdn.microsoft.com/en-us/library/cc241924 2.2.2.6 Server Upgrade License (SERVER_UPGRADE_LICENSE)
                                self._env.log1('Server Upgrade License') # Note: This is apparently where a temporary client CAL is upgraded to a permanent one that can't be revoked!

                            wBlobType_int, wBlobLen, rest = util.PackedStruct('<HH').unpack(validClientMessage)
                            wBlobType = rdp_defs.LICENSE_BINARY_BLOB_wBlobType(wBlobType_int)
                            self._env.log1('wBlobType: %s', wBlobType)
                            #assert wBlobType.BB_ENCRYPTED_DATA_BLOB, wBlobType # For Windows Server 2008 and Windows Server 2008 R2, the EncryptedLicenseInfo variable (part of the Server Upgrade License PDU) sent by the server to the client has the wBlobType parameter set to value BB_ENCRYPTED_DATA_BLOB (0x0009). For Windows Server 2003, the value of wBlobType is not defined.
                            blobData = rest[:wBlobLen] # This BLOB contains the encrypted New License Information (section 2.2.2.6.1) packet and is encrypted with the licensing encryption key (see section 5.1.3), using RC4 (for instructions on how to perform the encryption, see section 5.1.4).
                            MACData = rest[wBlobLen:]
                            lk = rdp_license.LicenseKeys(
                                self.client_connection_state.LicenseClientSecret,
                                self.server_connection_state.LicenseServerRandom,
                                self.client_connection_state.PreMasterSecret)
                            li = lk.xor(blobData)
                            self._env.log3('decrypted New License Information:', dump=li)

                            dwVersion, cbScope, rest = util.PackedStruct('<LL').unpack(li)
                            self._env.log1('dwVersion: 0x%x', dwVersion)
                            pbScope = rest[:cbScope] # Contains the NULL-terminated ANSI character set string giving the name of the issuer of this license. For example, for licenses issued by TailSpin Toys, this field contains the string "TailSpin Toys".
                            rest = rest[cbScope:]
                            self._env.log1('pbScope: %r', pbScope)
                            cbCompanyName, rest = util.PackedStruct('<L').unpack(rest)
                            pbCompanyName = rest[:cbCompanyName]
                            rest = rest[cbCompanyName:]
                            self._env.log1('pbCompanyName: %r', pbCompanyName.decode('utf-16le'))
                            cbProductId, rest = util.PackedStruct('<L').unpack(rest)
                            pbProductId = rest[:cbProductId]
                            rest = rest[cbProductId:]
                            self._env.log1('pbProductId: %r', pbProductId.decode('utf-16le')) # "A02" = Per device license
                            cbLicenseInfo, rest = util.PackedStruct('<L').unpack(rest)
                            # pbLicenseInfo (variable): This field contains the CAL issued to the client by the license server. This license consists of an X.509 certificate chain generated by the license server. The binary data contained in this field is opaque to the client. The client sends this information back to the server in the Client License Information message.
                            # http://msdn.microsoft.com/en-us/library/cc241889 X.509 Certificate Chains
                            pbLicenseInfo = rest[:cbLicenseInfo]
                            rest = rest[cbLicenseInfo:]
                            self._env.log2('pbLicenseInfo:', dump=pbLicenseInfo)

                            cert_struct = pkcs7.TaggedContentInfo.ber_decode(pbLicenseInfo)
                            self._env.log2('\t\tPKCS #7 ContentInfo:', pprint=cert_struct)

                            # An array of 16 bytes containing an MD5 digest (Message Authentication Code) generated over the unencrypted New License Information structure. For instructions on how to generate this message digest, see section 5.1.6; for a description of how the server uses the MACData field to verify the integrity of the New License Information packet, see section 3.1.5.1.
                            self._env.log1('MACData:', dump=MACData)
                            assert len(MACData) == 16, (MACData,)

                            new_validClientMessage = validClientMessage

                        elif bMsgType.ERROR_ALERT: # The bMsgType field of the preamble structure should be set to ERROR_ALERT (0xFF).
                            # http://msdn.microsoft.com/en-us/library/cc241927 2.2.2.7.1 License Error Message (LICENSE_ERROR_MESSAGE)
                            self._env.log1('Server License Error Message')
                            # validClientMessage (variable): A Licensing Error Message (section 2.2.1.12.1.3) structure.
                            # http://msdn.microsoft.com/en-us/library/cc240482 2.2.1.12.1.3 Licensing Error Message (LICENSE_ERROR_MESSAGE)

                            # Note: A successful STATUS_VALID_CLIENT is received immediately after Client Info PDU when connecting to the console - licensed connections are harder ...
                            dwErrorCode_int, dwStateTransition_int, bbErrorInfo = util.PackedStruct('<II').unpack(validClientMessage)

                            dwErrorCode = rdp_defs.LICENSE_ERROR_MESSAGE_dwErrorCode(dwErrorCode_int) # The error or status code.
                            self._env.log1('dwErrorCode: %s', dwErrorCode)
                            dwStateTransition = rdp_defs.LICENSE_ERROR_MESSAGE_dwStateTransition(dwStateTransition_int) # The licensing state to transition into upon receipt of this message. For more details about how this field is used, see [MS-RDPELE].
                            self._env.log1('dwStateTransition: %s', dwStateTransition)

                            if dwErrorCode.STATUS_VALID_CLIENT: # The dwErrorCode field of the error message structure MUST be set to STATUS_VALID_CLIENT (0x00000007)
                                assert dwStateTransition.ST_NO_TRANSITION, dwStateTransition # The dwStateTransition field MUST be set to ST_NO_TRANSITION (0x00000002).

                            # A LICENSE_BINARY_BLOB (section 2.2.1.12.1.2) structure which MUST contain a BLOB of type BB_ERROR_BLOB (0x0004)
                            # that includes information relevant to the error code specified in dwErrorCode.
                            # http://msdn.microsoft.com/en-us/library/cc240481 2.2.1.12.1.2 Licensing Binary Blob (LICENSE_BINARY_BLOB)
                            wBlobType_int, wBlobLen, blobData = util.PackedStruct('<HH').unpack(bbErrorInfo)
                            wBlobType = rdp_defs.LICENSE_BINARY_BLOB_wBlobType(wBlobType_int)
                            self._env.log1('wBlobType: %s', wBlobType)
                            if wBlobLen:
                                assert wBlobType.BB_ERROR_BLOB, wBlobType # The bbErrorInfo field MUST contain an empty binary large object (BLOB) of type BB_ERROR_BLOB (0x0004).
                                assert wBlobLen == len(blobData), (wBlobLen, blobData)
                                self._env.log1('blobData:', dump=blobData) # The size of this data in bytes is given by the wBlobLen field.
                            else: # If wBlobLen is set to 0, then the contents of this field SHOULD be ignored.
                                self._env.log1('wblobLen: %s', wBlobLen)

                            new_validClientMessage = validClientMessage

#                           new_validClientMessage = (
#                               util.PackedStruct('<II').pack(
#                                   rdp_defs.LICENSE_ERROR_MESSAGE_dwErrorCode.STATUS_VALID_CLIENT,
#                                   rdp_defs.LICENSE_ERROR_MESSAGE_dwStateTransition.ST_NO_TRANSITION) +
#                               util.PackedStruct('<HH').pack(
#                                   rdp_defs.LICENSE_BINARY_BLOB_wBlobType.BB_ERROR_BLOB, # ignored anyway
#                                   0
#                                   ) + ''
#                              )

                        else:
                            self._env.log1('Unknown Server Licensing PDU 0x%x', bMsgType_int)
                            if self._env.config.STRICT:
                                assert False, bMsgType_int
                            new_validClientMessage = validClientMessage

                        new_cleartext_security_payload = rdp_defs.LICENSING_PREAMPLE.pack(
                            bMsgType_int, 2, len(new_validClientMessage) + rdp_defs.LICENSING_PREAMPLE.size
                            ) + new_validClientMessage

                    else:
                        rest_payload = cleartext_security_payload
                        new_cleartext_security_payload = ''
                        HBB = util.PackedStruct('<HBB')
                        while rest_payload: # allow looping because VirtualBox does it ...
                            # http://msdn.microsoft.com/en-us/library/cc240576 2.2.8.1.1.1.1 Share Control Header (TS_SHARECONTROLHEADER)
                            totalLength, pduType_versionLow, versionHigh, rest = HBB.unpack(rest_payload)
                            this_payload = rest_payload[:totalLength] # totelLength might be 0x8000 - that is ok
                            rest_payload = rest_payload[totalLength:]
                            totalLength, pduType_versionLow, versionHigh, rest = HBB.unpack(this_payload)
                            pduType = rdp_defs.TS_SHARECONTROLHEADER_PDUTYPEs(pduType_versionLow & 0xf)
                            self._env.log1('pduType: %s', pduType)
                            versionLow = pduType_versionLow >> 4
                            self._env.log1('\tversion: high %02x low %02x', versionHigh, versionLow)
                            if totalLength != 0x8000: # Not for keepalive PDUs where versionLow apparently is 4
                                if versionLow == 0:
                                    self._env.log1('\tincorrect versionLow = 0 - VirtualBox?')
                                else:
                                    assert versionLow == 1, versionLow # versionLow (4 bits): Most significant 4 bits of the least significant byte. This field MUST be set to TS_PROTOCOL_VERSION (0x1).
                                assert totalLength == len(this_payload), (totalLength, len(this_payload))
                            assert versionHigh == 0, versionHigh # Most significant byte. This field MUST be set to 0x00.

                            if rest: # PDUTYPE_DEACTIVATEALLPDU doesn't always carry this!?
                                pduSource, pduData = util.PackedStruct('<H').unpack(rest)
                            else:
                                # When connecting from vista to vista and SSO fails and we cancel password prompt then we come here with PDUTYPE_DEACTIVATEALLPDU
                                pduSource = None
                                pduData = None
                            self._env.log1('\tpduSource: %s', self.channel_name(pduSource)) # The channel ID which is the transmission source of the PDU.

                            if totalLength == 0x8000: # Keepalive PDU
                                # Hulk sends this after 100 seconds - apparently not documented anywhere, but freerdp says that it is keepalive ...
                                self._env.log1('Server keepalive', dump=pduData) # Apparently pduData == ea 03 == 1002 = Server Channel
                            elif pduType.PDUTYPE_DEMANDACTIVEPDU:
                                # http://msdn.microsoft.com/en-us/library/cc240669 3.2.5.3.13.1 Processing Demand Active PDU
                                # http://msdn.microsoft.com/en-us/library/cc240484 2.2.1.13.1 Server Demand Active PDU
                                # http://msdn.microsoft.com/en-us/library/cc240485 2.2.1.13.1.1 Demand Active PDU Data (TS_DEMAND_ACTIVE_PDU)
                                self._env.log1('Server Demand Active PDU')

                                self.ServerChannelID = pduSource # The server MCS channel ID (present in the pduSource field) MUST be stored in the Server Channel ID store (section 3.2.1.5).
                                self._env.log1('Allocated Server Channel ID: %s', self.ServerChannelID)

                                shareId, lengthSourceDescriptor, lengthCombinedCapabilities, rest = util.PackedStruct('<IHH').unpack(pduData)
                                sourceDescriptor = rest[:lengthSourceDescriptor]
                                rest = rest[lengthSourceDescriptor:]
                                rest_sessionId = rest[lengthCombinedCapabilities:]

                                self._env.log1('shareId: %s 0x%x', shareId, shareId) # The share identifier for the packet (see [T128] section 8.4.2 for more information regarding share IDs).
                                self.ShareID = shareId # The contents of the shareId field MUST be stored in the Share ID store (section 3.2.1.7). FIXME: Every time???

                                self._env.log1('sourceDescriptor: %r', sourceDescriptor) # Source descriptor. The Microsoft RDP client sets the contents of this field to { 0x4D, 0x53, 0x54, 0x53, 0x43, 0x00 }, which is the ANSI representation of the null-terminated string "MSTSC" in hexadecimal.

                                numberCapabilities, _pad2octets, capabilitySets = util.PackedStruct('<HH').unpack(rest[:lengthCombinedCapabilities])
                                self._env.log1('numberCapabilities: 0x%x', numberCapabilities) # Number of capability sets included in the Confirm Active PDU.
                                #self._env.log1('capabilitySets:', dump=capabilitySets) # An array of TS_CAPS_SET (section 2.2.1.13.1.1.1) structures. Collection of capability sets, each conforming to the TS_CAPS_SET structure. The number of capability sets is specified by the numberCapabilities field.

                                # FIXME: The capabilities received from the server MUST be stored in the Server Capabilities store (section 3.2.1.6) and MUST be used to determine what subset of RDP to send to the server.
                                self.analyze_capability_set(numberCapabilities, capabilitySets)

                                sessionId, rest = util.PackedStruct('<I').unpack(rest_sessionId)
                                assert not rest, (rest,)
                                self._env.log1('sessionId: 0x%x', sessionId)

                            elif pduType.PDUTYPE_DATAPDU:
                                self._env.log1('Share Data Header PDU')

                                # http://msdn.microsoft.com/en-us/library/cc240577 2.2.8.1.1.1.2 Share Data Header (TS_SHAREDATAHEADER)
                                shareId, _pad1, streamId, uncompressedLength, pduType2_int, compressedType_int, compressedLength, pduData2 = util.PackedStruct('<IBBHBBH').unpack(pduData)
                                self._env.log1('shareId: %s 0x%x', shareId, shareId) # shareId Share identifier for the packet (see [T128] section 8.4.2 for more information about share IDs).
                                self._env.log1('streamId: %s', streamId) # The stream identifier for the packet.
                                self._env.log1('uncompressedLength: %s', uncompressedLength) # The uncompressed length of the packet in bytes.

                                pduType2 = rdp_defs.TS_SHAREDATAHEADER_PDUTYPE2(pduType2_int) # The type of data PDU.
                                self._env.log1('pduType2: %s', pduType2)

                                packet_compression = rdp_defs.TS_SHAREDATAHEADER_PACKET_COMPRESSION(compressedType_int & 0xf0)
                                self._env.log1('packet_compression: %s', packet_compression)
                                packet_compr_type = rdp_defs.TS_SHAREDATAHEADER_PACKET_COMPR_TYPE(compressedType_int & 0xf)
                                self._env.log1('packet_compr_type: %s', packet_compr_type)
                                self._env.log1('compressedLength: %s', compressedLength) # The compressed length of the packet in bytes.

                                if pduType2.PDUTYPE2_SYNCHRONIZE:
                                    # http://msdn.microsoft.com/en-us/library/cc240676 3.2.5.3.19 Processing Synchronize PDU
                                    # http://msdn.microsoft.com/en-us/library/cc240499 2.2.1.19 Server Synchronize PDU
                                    self._env.log1('Server Synchronize PDU')

                                    if not sec_flags.SEC_IGNORE_SEQNO:
                                        self._env.log1('No SEC_IGNORE_SEQNO!') # The flags field of the security header SHOULD contain the SEC_IGNORE_SEQNO flag (see section 2.2.8.1.1.2.1).

                                    synchronizePduData = pduData2 # The contents of the Synchronize PDU, as specified in section 2.2.1.14.1.
                                    # http://msdn.microsoft.com/en-us/library/cc240490 2.2.1.14.1 Synchronize PDU Data (TS_SYNCHRONIZE_PDU)
                                    messageType, targetUser, rest = util.PackedStruct('<HH').unpack(synchronizePduData)
                                    assert not rest, (rest,)
                                    if messageType == 0:
                                        self._env.log1('\tincorrect messageType = 0 - VirtualBox?') # No longer seen
                                    else:
                                        assert messageType == 1, messageType # The message type. This field MUST be set to SYNCMSGTYPE_SYNC (1).
                                    # The targetUser field SHOULD be set to the MCS server channel ID (held in the Server Channel ID store (section 3.2.1.5)).
                                    # The contents of the targetUser field MUST be ignored.
                                    self._env.log1('targetUser: %s', self.channel_name(targetUser)) # The MCS channel ID of the target user.

                                elif pduType2.PDUTYPE2_CONTROL:
                                    self._env.log1('Server Control PDU ...')
                                    controlPduData = pduData2 # The actual contents of the Control PDU, as specified in section 2.2.1.15.1.
                                    # http://msdn.microsoft.com/en-us/library/cc240492 2.2.1.15.1 Control PDU Data (TS_CONTROL_PDU)
                                    action_int, grantId, controlId, rest = util.PackedStruct('<HHI').unpack(controlPduData)
                                    assert not rest, (rest,)
                                    action = rdp_defs.TS_CONTROL_PDU_action(action_int) # The action code.
                                    self._env.log1('action: %s', action)
                                    self._env.log1('grantId: %s', self.channel_name(grantId))
                                    self._env.log1('controlId: %s', self.channel_name(controlId))

                                    if action.CTRLACTION_COOPERATE:
                                        self._env.log1('Server Control PDU - Cooperate')
                                        # http://msdn.microsoft.com/en-us/library/cc240678 3.2.5.3.20 Processing Control PDU - Cooperate
                                        # The contents of the controlId and grantId fields MAY be ignored.
                                        # http://msdn.microsoft.com/en-us/library/cc240502 2.2.1.20 Server Control PDU - Cooperate
                                        # The grantId and controlId fields of the Control PDU Data MUST both be set to zero
                                    elif action.CTRLACTION_GRANTED_CONTROL:
                                        self._env.log1('Server Control PDU - Granted Control')
                                        # http://msdn.microsoft.com/en-us/library/cc240679 3.2.5.3.21 Processing Control PDU - Granted Control
                                        # The contents of the controlId and grantId fields MAY be ignored.
                                        # http://msdn.microsoft.com/en-us/library/cc240503 2.2.1.21 Server Control PDU - Granted Control
                                        # The grantId field MUST be set to the user channel ID (see sections 2.2.1.6 and 2.2.1.7),
                                        # while the controlId field MUST be set to the server channel ID (in Microsoft RDP server implementations this value is always 0x03EA).
                                    else:
                                        self._env.log1('Unknown Server Control Action: %s', action)
                                        if self._env.config.STRICT:
                                            assert False, action

                                elif pduType2.PDUTYPE2_FONTMAP:
                                    # http://msdn.microsoft.com/en-us/library/cc240680 3.2.5.3.22 Processing Font Map PDU
                                    # http://msdn.microsoft.com/en-us/library/cc240504 2.2.1.22 Server Font Map PDU
                                    self._env.log1('Server Font Map PDU')

                                    _fontMapPduData = pduData2
                                    # http://msdn.microsoft.com/en-us/library/cc240505 2.2.1.22.1 Font Map PDU Data (TS_FONT_MAP_PDU)
                                    # TODO

                                elif pduType2.PDUTYPE2_SAVE_SESSION_INFO:
                                    # http://msdn.microsoft.com/en-us/library/cc240706 3.2.5.9.5.1 Processing Save Session Info PDU
                                    # http://msdn.microsoft.com/en-us/library/cc240635 2.2.10.1 Server Save Session Info PDU
                                    self._env.log1('Server Save Session Info PDU')

                                    saveSessionInfoPduData = pduData2
                                    # http://msdn.microsoft.com/en-us/library/cc240636 2.2.10.1.1 Save Session Info PDU Data (TS_SAVE_SESSION_INFO_PDU_DATA)

                                    infoType_int, infoData = util.PackedStruct('<I').unpack(saveSessionInfoPduData)
                                    infoType = rdp_defs.TS_SAVE_SESSION_INFO_PDU_DATA_INFOTYPE(infoType_int)
                                    self._env.log1('infoType: %s', infoType) # The type of logon information.
                                    self._env.log1('infoData:', dump=infoData) # TS_LOGON_INFO, TS_LOGON_INFO_VERSION_2, or TS_LOGON_INFO_EXTENDED: Variable length logon information structure. The type of data which follows depends on the value of the infoType field.

                                elif pduType2.PDUTYPE2_SET_ERROR_INFO_PDU:
                                    # http://msdn.microsoft.com/en-us/library/cc240696 3.2.5.7.1 Processing Set Error Info PDU
                                    # http://msdn.microsoft.com/en-us/library/cc240543 2.2.5.1 Server Set Error Info PDU
                                    self._env.log1('Server Set Error Info PDU')

                                    errorInfoPduData = pduData2
                                    # http://msdn.microsoft.com/en-us/library/cc240544 2.2.5.1.1 Set Error Info PDU Data (TS_SET_ERROR_INFO_PDU)
                                    errorInfo_int, rest = util.PackedStruct('<I').unpack(errorInfoPduData)
                                    errorInfo = rdp_defs.TS_SET_ERROR_INFO_PDU_errorInfo(errorInfo_int) # Error code.
                                    assert not rest, (rest,)

                                    self._env.log1('errorInfo: %s', errorInfo)

                                elif pduType2.PDUTYPE2_ARC_STATUS_PDU:
                                    # http://msdn.microsoft.com/en-us/library/cc240539 2.2.4.1.1 Auto-Reconnect Status PDU Data (TS_AUTORECONNECT_STATUS_PDU)
                                    self._env.log1('Server Auto-Reconnect Status PDU')

                                    arcStatusPduData = pduData2
                                    assert not pduSource, pduSource
                                    arcStatus, rest = util.PackedStruct('<I').unpack(arcStatusPduData)
                                    assert not arcStatus, arcStatus # This field MUST be set to 0.
                                    assert not rest, (rest,)

                                elif pduType2.PDUTYPE2_SHUTDOWN_DENIED:
                                    # http://msdn.microsoft.com/en-us/library/cc240532 2.2.2.3 Server Shutdown Request Denied PDU
                                    self._env.log1('Server Shutdown Request Denied PDU')

                                    shutdownRequestDeniedPduData = pduData2
                                    # http://msdn.microsoft.com/en-us/library/cc240533 2.2.2.3.1 Shutdown Request Denied PDU Data (TS_SHUTDOWN_DENIED_PDU)
                                    assert not shutdownRequestDeniedPduData, (shutdownRequestDeniedPduData,) # no PDU body.

                                elif pduType2.PDUTYPE2_UPDATE:
                                    # http://msdn.microsoft.com/en-us/library/cc240607 2.2.9.1.1.3 Server Graphics Update PDU (TS_GRAPHICS_PDU)
                                    self._env.log1('Server PDUTYPE2_UPDATE PDU')

                                    slowPathGraphicsUpdates = pduData2
                                    # http://msdn.microsoft.com/en-us/library/cc240608 2.2.9.1.1.3.1 Slow Path Graphics Update (TS_GRAPHICS_UPDATE)
                                    updateType_int, updateData = util.PackedStruct('<H').unpack(slowPathGraphicsUpdates)
                                    updateType = rdp_defs.TS_GRAPHICS_UPDATE_updateType(updateType_int)
                                    self._env.log1('updateType: %s', updateType, dump=updateData) # Type of the graphics update.
                                    # TODO ...

                                elif pduType2.PDUTYPE2_POINTER:
                                    # http://msdn.microsoft.com/en-us/library/cc240614 2.2.9.1.1.4 Server Pointer Update PDU (TS_POINTER_PDU)
                                    self._env.log1('Server PDUTYPE2_POINTER PDU')

                                    messageType_int, _pad2octets, pointerAttributeData = util.PackedStruct('<HH').unpack(pduData2)

                                    messageType = rdp_defs.PDUTYPE2_POINTER_messageType(messageType_int)
                                    self._env.log1('messageType: %s', messageType) # Type of pointer update.

                                    self._env.log1('pointerAttributeData:', dump=pointerAttributeData) # Type of pointer update.
                                    # TS_POINTERPOSATTRIBUTE (4 bytes),
                                    # TS_SYSTEMPOINTERATTRIBUTE (4 bytes),
                                    # TS_COLORPOINTERATTRIBUTE (variable number of bytes),
                                    # TS_POINTERATTRIBUTE (variable number of bytes), or
                                    # TS_CACHEDPOINTERATTRIBUTE (2 bytes):

                                elif pduType2.PDUTYPE2_PLAY_SOUND:
                                    # http://msdn.microsoft.com/en-us/library/cc240632 2.2.9.1.1.5 Server Play Sound PDU
                                    self._env.log1('Server Play Sound PDU')
                                    playSoundPduData = pduData2
                                    # http://msdn.microsoft.com/en-us/library/cc240633 2.2.9.1.1.5.1 Play Sound PDU Data (TS_PLAY_SOUND_PDU_DATA)

                                    duration, frequency, rest = util.PackedStruct('<II').unpack(playSoundPduData)
                                    assert not rest, (rest,)
                                    self._env.log1('duration, ms: %s', duration) # Duration of the beep the client should play.
                                    self._env.log1('frequency, Hz: %s', frequency) # Frequency of the beep the client should play.

                                elif pduType2.PDUTYPE2_SET_KEYBOARD_INDICATORS:
                                    # http://msdn.microsoft.com/en-us/library/cc240599 2.2.8.2.1.1 Set Keyboard Indicators PDU Data (TS_SET_KEYBOARD_INDICATORS_PDU)
                                    self._env.log1('Server Set Keyboard Indicators PDU Data')
                                    UnitId, LedFlags_int, rest = util.PackedStruct('<HH').unpack(pduData2)
                                    self._env.log1('UnitId: %s', UnitId) # A 16-bit, unsigned integer. Hardware related value. This field SHOULD be ignored by the client and as a consequence SHOULD be set to 0 by the server.
                                    LedFlags = rdp_defs.LedFlags(LedFlags_int)
                                    self._env.log1('LedFlags: %s', LedFlags) #  A 16-bit, unsigned integer. The flags indicating the "on" status of the keyboard toggle keys.

                                elif pduType2.PDUTYPE2_MONITOR_LAYOUT_PDU:
                                    # http://msdn.microsoft.com/en-us/library/dd341197 2.2.12.1 Monitor Layout PDU
                                    self._env.log1('Monitor Layout PDU Data')
                                    monitorCount, rest = util.PackedStruct('<I').unpack(pduData2)
                                    self._env.log1('\tmonitorCount: %s', monitorCount)
                                    for i in range(monitorCount):
                                        # http://msdn.microsoft.com/en-us/library/dd342324 2.2.1.3.6.1 Monitor Definition (TS_MONITOR_DEF)
                                        left, top, right, bottom, flags, rest = util.PackedStruct('<IIIII').unpack(rest)
                                        self._env.log1('\tmonitor %s: %s,%s - %s,%s %s', i, left, top, right, bottom, flags)
                                    assert not rest, (rest,)

                                else:
                                    self._env.log1('Unknown pduType2: %s', pduType2, dump=pduData2)
                                    if self._env.config.STRICT:
                                        assert False, pduType2

                            elif pduType.PDUTYPE_DEACTIVATEALLPDU:
                                # http://msdn.microsoft.com/en-us/library/cc240535 2.2.3.1 Server Deactivate All PDU
                                # http://msdn.microsoft.com/en-us/library/cc240536 2.2.3.1.1 Deactivate All PDU Data (TS_DEACTIVATE_ALL_PDU)
                                self._env.log1('Server Deactivate All PDU')

                                deactivateAllPduData = pduData

                                if pduSource is None:
                                    # When connecting from vista to vista and SSO fails and we cancel password prompt then we come here with PDUTYPE_DEACTIVATEALLPDU
                                    self._env.log1("No pduSource and no data - ok, we have seen that before ...")
                                    assert not deactivateAllPduData, (deactivateAllPduData)

                                else:
                                    shareId, lengthSourceDescriptor, sourceDescriptor = util.PackedStruct('<IH').unpack(deactivateAllPduData)

                                    self._env.log1('shareId: %s 0x%x', shareId, shareId) # The share identifier for the packet (see [T128] section 8.4.2 for more information regarding share IDs).
                                    assert len(sourceDescriptor) == lengthSourceDescriptor, (sourceDescriptor, lengthSourceDescriptor) # The size in bytes of the sourceDescriptor field.
                                    self._env.log1('sourceDescriptor:', dump=sourceDescriptor) # The source descriptor. This field SHOULD be set to 0x00.
                                    assert sourceDescriptor == '\x00', (sourceDescriptor,)

                            else:
                                self._env.log1('Unknown pduType: %s', pduType, dump=pduData)
                                if self._env.config.STRICT:
                                    assert False, pduType

                            new_cleartext_security_payload += this_payload
                        # end of Share Control Header stuff
                        new_cleartext_security_payload = cleartext_security_payload

                else: # not IOChannelID
                    self._env.log1('Not processing data to this channel')
                    assert not sec_flags.SEC_LICENSE_PKT, sec_flags # and many others?
                    new_cleartext_security_payload = cleartext_security_payload

                if sec_flags.SEC_ENCRYPT or sec_flags.SEC_REDIRECTION_PKT:
                    # We must fix encryption and mac in encrypted packages ...
                    new_mac = self.client_connection_state.generate_mac(new_cleartext_security_payload, secure=sec_flags.SEC_SECURE_CHECKSUM)
                    new_encrypted_security_payload = self.client_connection_state.server_client_xor(new_cleartext_security_payload)
                    self._env.log3('new_encrypted_security_payload:', dump=new_encrypted_security_payload)

                    new_security_payload = new_mac + new_encrypted_security_payload

                else:
                    new_security_payload = new_cleartext_security_payload

                # http://msdn.microsoft.com/en-us/library/cc240579 2.2.8.1.1.2.1 Basic (TS_SECURITY_HEADER)
                new_sendDataIndicationUserData = util.PackedStruct('<HH').pack(sec_flags.value, sec_flagsHi) + new_security_payload

                new_sendDataIndication = dict(sendDataIndication)
                new_sendDataIndication['userData'] = new_sendDataIndicationUserData
                new_domainMCSPDU = (pdu_type, new_sendDataIndication)

            elif pdu_type == 'disconnectProviderUltimatum':
                disconnectProviderUltimatum = domainMCSPDU[1]
                # http://msdn.microsoft.com/en-us/library/cc240529 2.2.2.1 MCS Disconnect Provider Ultimatum PDU
                # MSDN is WRONG; it messes DisconnectProviderUltimatum and RejectMCSPDUUltimatum up
                self._env.log1('DisconnectProviderUltimatum')
                self._env.log1('Reason: %s', disconnectProviderUltimatum['reason'])

                new_domainMCSPDU = domainMCSPDU

            else:
                self._env.log1('unexpected pdu_type: %s', pdu_type, pprint=domainMCSPDU)
                if self._env.config.STRICT:
                    assert False, pdu_type
                new_domainMCSPDU = domainMCSPDU

            new_x224_body = rdp_asn1.DomainMCSPDU.per_encode(new_domainMCSPDU)

        else:
            self._env.log1('Not ready for data from server in state: %s', self._state)
            assert False, self._state

        return new_x224_body


    @util.exceptor
    def fastpath_from_server_to_client(self, chunk):
        """
        Returns how much has been read and used
        """
        # http://msdn.microsoft.com/en-us/library/cc240621 2.2.9.1.2 Server Fast-Path Update PDU (TS_FP_UPDATE_PDU)
        self._env.log1("Server sent Fastpath")
        #self._env.log1('chunk:', dump=chunk)
        util.ensure_enough_data(chunk, rdp_defs.FASTPATH_HEADER.size)
        fpInputHeader, length1, rest_fpInputHeader = rdp_defs.FASTPATH_HEADER.unpack(chunk)
        # actionCode (2 bits): A 2-bit code indicating whether the PDU is in fast-path or slow-path format.
        actionCode = rdp_defs.fpInputHeader_server_actioncode_enum(fpInputHeader & 0x3)
        self._env.log1('actionCode: %s', actionCode)
        assert actionCode.FASTPATH_OUTPUT_ACTION_FASTPATH, actionCode
        encryptionFlags = rdp_defs.fpInputHeader_server_encryptionFlags_flags(fpInputHeader >> 6)
        self._env.log1('encryptionFlags: %s', encryptionFlags)
        assert not (fpInputHeader & 0x3c), fpInputHeader & 0x3c

        if length1 & 0x80:
            util.ensure_enough_data(rest_fpInputHeader, rdp_defs.FASTPATH_LENGTH2.size)
            length2, rest_length2 = rdp_defs.FASTPATH_LENGTH2.unpack(rest_fpInputHeader)
            length = ((length1 & 0x7f) << 8) + length2
            util.ensure_enough_data(rest_length2, length - rdp_defs.FASTPATH_HEADER.size - rdp_defs.FASTPATH_LENGTH2.size)
            length_body = rest_length2[:length - rdp_defs.FASTPATH_HEADER.size - rdp_defs.FASTPATH_LENGTH2.size]
        else:
            length = length1
            util.ensure_enough_data(rest_fpInputHeader, length - rdp_defs.FASTPATH_HEADER.size)
            length_body = rest_fpInputHeader[:length - rdp_defs.FASTPATH_HEADER.size]
        self._env.log2('length: %s', length)
        assert len(chunk) >= length, (chunk, length) # redundant check

        # fipsInformation (4 bytes): Optional FIPS header information, present when the Encryption Level selected by the server
        # (see sections 5.3.2 and 2.2.1.4.3) is ENCRYPTION_LEVEL_FIPS (4). The fast-path FIPS information structure is specified in section 2.2.8.1.2.1.
        if self.server_connection_state.encryptionLevel.ENCRYPTION_LEVEL_FIPS:
            fipsInformation, fips_body = util.PackedStruct('<4s').unpack(length_body)
            self._env.log1('fipsInformation: %s', dump=fipsInformation)
        else:
            fips_body = length_body

        # dataSignature (8 bytes): MAC generated over the packet using one of the techniques specified in section 5.3.6.1 (the FASTPATH_INPUT_SECURE_CHECKSUM flag,
        # which is set in the fpInputHeader field, describes the method used to generate the signature). This field is present if the FASTPATH_INPUT_ENCRYPTED flag
        # is set in the fpInputHeader field.
        if encryptionFlags.FASTPATH_OUTPUT_ENCRYPTED:
            dataSignature, crypted_fpOutputUpdates = util.PackedStruct('<8s').unpack(fips_body)

            self._env.log3('crypted_fpOutputUpdates:', dump=crypted_fpOutputUpdates)
            fpOutputUpdates = self.server_connection_state.server_client_xor(crypted_fpOutputUpdates)

            self.server_connection_state.check_mac(fpOutputUpdates, dataSignature, secure=encryptionFlags.FASTPATH_OUTPUT_SECURE_CHECKSUM)

        else:
            fpOutputUpdates = fips_body

        # fpOutputUpdates (variable): An array of TS_FP_UPDATE structures (variable number of bytes) containing a collection of fast-path updates to be processed by the client.
        # http://msdn.microsoft.com/en-us/library/cc240622 2.2.9.1.2.1 Fast-Path Update (TS_FP_UPDATE)
        #self._env.log1('cleartext TS_FP_UPDATEs:', dump=fpOutputUpdates)

        rest_fpOutputUpdates = fpOutputUpdates
        while rest_fpOutputUpdates:
            updateHeader, rest_updateHeader = util.PackedStruct('<B').unpack(rest_fpOutputUpdates)
            updateCode = rdp_defs.FASTPATH_UPDATETYPEs(updateHeader & 0xf)
            self._env.log1('updateCode: %s', updateCode)
            fragmentation = rdp_defs.FASTPATH_FRAGMENTs((updateHeader >> 4) & 0x3)
            self._env.log1('\tfragmentation: %s', fragmentation)
            compression = rdp_defs.FASTPATH_OUTPUT_COMPRESSION(updateHeader >> 6)
            self._env.log1('\tcompression: %s', compression)

            if compression.FASTPATH_OUTPUT_COMPRESSION_USED:
                # Optional compression flags. The flags used in this field are exactly the same as the MPPC flags used in the compressedType field
                # in the Share Data Header (section 2.2.8.1.1.1.2) and have the same meaning.
                compressionFlags, rest_compressionFlags = util.PackedStruct('<B').unpack(rest_updateHeader)
                self._env.log1('\tcompressionFlags: %s', compressionFlags)
            else:
                rest_compressionFlags = rest_updateHeader

            # The size in bytes of the data in the updateData field.
            size, rest_size = util.PackedStruct('<H').unpack(rest_compressionFlags)
            assert size <= len(rest_size), (size, rest_size)

            assert len(rest_size) >= size, (len(rest_size), size)

            updateData = rest_size[:size]
            rest_fpOutputUpdates = rest_size[size:]
            self._env.log3('\tupdateData:', dump=updateData)

            # TODO: Analyse this!

        # Re-pack with new keys
        new_fastpath_chunk = rdp_defs.FASTPATH_HEADER.pack(fpInputHeader, length1)
        if length1 & 0x80:
            new_fastpath_chunk = new_fastpath_chunk + rdp_defs.FASTPATH_LENGTH2.pack(length2)
        if self.client_connection_state.encryptionLevel.ENCRYPTION_LEVEL_FIPS:
            new_fastpath_chunk = new_fastpath_chunk + util.PackedStruct('<4s').pack(fipsInformation)
        if encryptionFlags.FASTPATH_OUTPUT_ENCRYPTED:
            new_dataSignature = self.client_connection_state.generate_mac(fpOutputUpdates, secure=encryptionFlags.FASTPATH_OUTPUT_SECURE_CHECKSUM)
            new_crypted_fpOutputUpdates = self.client_connection_state.server_client_xor(fpOutputUpdates)
            self._env.log3('new_crypted_fpOutputUpdates:', dump=new_crypted_fpOutputUpdates)
            new_fastpath_chunk = new_fastpath_chunk + new_dataSignature + new_crypted_fpOutputUpdates
        else:
            new_fastpath_chunk = new_fastpath_chunk + fpOutputUpdates

        assert len(new_fastpath_chunk) == length, (new_fastpath_chunk, length) # if it has changed then some lengths must be backpatched...
        return new_fastpath_chunk, chunk[length:]

    @util.exceptor
    def fastpath_from_client_to_server(self, chunk):
        """
        Returns how much has been read and used
        """
        # http://msdn.microsoft.com/en-us/library/cc240589 2.2.8.1.2 Client Fast-Path Input Event PDU (TS_FP_INPUT_PDU)
        self._env.log1("Client sent Fastpath")
        #self._env.log1('chunk:', dump=chunk)
        util.ensure_enough_data(chunk, rdp_defs.FASTPATH_HEADER.size)
        fpInputHeader, length1, rest_fpInputHeader = rdp_defs.FASTPATH_HEADER.unpack(chunk)
        # actionCode (2 bits): A 2-bit code indicating whether the PDU is in fast-path or slow-path format.
        actionCode = rdp_defs.TS_FP_INPUT_PDU_actionCode(fpInputHeader & 0x3)
        self._env.log1('actionCode: %s', actionCode)
        assert actionCode.FASTPATH_INPUT_ACTION_FASTPATH, actionCode
        numberEvents4bits = (fpInputHeader >> 2) & 0xf
        self._env.log1('numberEvents: %s', numberEvents4bits)
        encryptionFlags = rdp_defs.TS_FP_INPUT_PDU_encryptionFlags(fpInputHeader >> 6)
        self._env.log1('encryptionFlags: %s', encryptionFlags)

        if length1 & 0x80:
            util.ensure_enough_data(rest_fpInputHeader, rdp_defs.FASTPATH_LENGTH2.size)
            length2, rest_fpInputHeader = rdp_defs.FASTPATH_LENGTH2.unpack(rest_fpInputHeader)
            length = ((length1 & 0x7f) << 8) + length2
            util.ensure_enough_data(rest_fpInputHeader, length - rdp_defs.FASTPATH_HEADER.size - rdp_defs.FASTPATH_LENGTH2.size)
            length_body = rest_fpInputHeader[:length - rdp_defs.FASTPATH_HEADER.size - rdp_defs.FASTPATH_LENGTH2.size]
        else:
            length = length1
            util.ensure_enough_data(rest_fpInputHeader, length - rdp_defs.FASTPATH_HEADER.size)
            length_body = rest_fpInputHeader[:length - rdp_defs.FASTPATH_HEADER.size]
        self._env.log2('length: %s', length)
        assert len(chunk) >= length, (chunk, length) # redundant check

        # fipsInformation (4 bytes): Optional FIPS header information, present when the Encryption Level selected by the server
        # (see sections 5.3.2 and 2.2.1.4.3) is ENCRYPTION_LEVEL_FIPS (4). The fast-path FIPS information structure is specified in section 2.2.8.1.2.1.
        if self.client_connection_state.encryptionLevel.ENCRYPTION_LEVEL_FIPS:
            #http://msdn.microsoft.com/en-us/library/cc240590 2.2.8.1.2.1 Fast-Path FIPS Information (TS_FP_FIPS_INFO)
            fipsInformation, fips_body = util.PackedStruct('<4s').unpack(length_body)
            self._env.log1('fipsInformation:', dump=fipsInformation)
        else:
            fips_body = length_body

        # dataSignature (8 bytes): The MAC generated over the packet using one of the techniques described in Non-FIPS
        # (the FASTPATH_INPUT_SECURE_CHECKSUM flag, which is set in the fpInputHeader field, describes the method used to generate the signature).
        # This field is present if the FASTPATH_INPUT_ENCRYPTED flag is set in the fpInputHeader field.
        if encryptionFlags.FASTPATH_INPUT_ENCRYPTED:
            dataSignature, crypted_fpInputEvents = util.PackedStruct('<8s').unpack(fips_body)

            self._env.log3('crypted_fpInputEvents:', dump=crypted_fpInputEvents)
            fpInputEvents = self.client_connection_state.client_server_xor(crypted_fpInputEvents)

            self._env.log3('Header MAC, dataSignature:', dump=dataSignature)
            self.client_connection_state.check_mac(fpInputEvents, dataSignature, encryptionFlags.FASTPATH_INPUT_SECURE_CHECKSUM)

        else:
            fpInputEvents = fips_body

        # The number of fast-path input events packed together in the fpInputEvents field (up to 255).
        # This field is present if the numberEvents bit field in the fast-path header byte is zero.
        if not numberEvents4bits:
            numberEvents, fpInputEvents = util.PackedStruct('<B').unpack(fpInputEvents)
            self._env.log1('numberEvents: %s', numberEvents)
        else:
            numberEvents = numberEvents4bits

        # fpInputEvents (variable): A collection of Fast-Path Input Event (section 2.2.8.1.2.2) structures to be processed by the server.
        # The number of events present in this array is given by the numberEvents bit field in the fast-path header byte,
        # or by the numberEvents field in the Fast-Path Input Event PDU (if it is present).
        #self._env.log1('cleartext TS_FP_INPUT_EVENTs:', dump=fpInputEvents)
        rest_fpInputEvents = fpInputEvents
        for _i in range(numberEvents):
            # http://msdn.microsoft.com/en-us/library/cc240591 2.2.8.1.2.2 Fast-Path Input Event (TS_FP_INPUT_EVENT)
            #self._env.log1('rest_fpInputEvents:', dump=rest_fpInputEvents)
            eventHeader_int, rest_eventData = util.PackedStruct('<B').unpack(rest_fpInputEvents)
            eventFlags_int = eventHeader_int & 0x1f
            eventCode = rdp_defs.TS_FP_INPUT_EVENT_eventCode(eventHeader_int >> 5)

            self._env.log1('eventCode: %s', eventCode)
            if eventCode.FASTPATH_INPUT_EVENT_SCANCODE:
                # http://msdn.microsoft.com/en-us/library/cc240592 2.2.8.1.2.2.1 Fast-Path Keyboard Event (TS_FP_KEYBOARD_EVENT)
                eventFlags = rdp_defs.TS_FP_KEYBOARD_EVENT_eventFlags(eventFlags_int)
                keyCode, rest_fpInputEvents = util.PackedStruct('<B').unpack(rest_eventData)
                self._env.log1('\teventFlags: %s', eventFlags) # flags describing the keyboard event.
                self._env.log1('\tkeyCode: %s', keyCode) # The scancode of the key which triggered the event.

            elif eventCode.FASTPATH_INPUT_EVENT_UNICODE:
                # http://msdn.microsoft.com/en-us/library/cc240593 2.2.8.1.2.2.2 Fast-Path Unicode Keyboard Event (TS_FP_UNICODE_KEYBOARD_EVENT)
                if eventFlags_int:
                    self._env.log1('\tinvalid eventFlags: %s', eventFlags_int) # iTAP sends 1 ...
                unicodeCode, rest_fpInputEvents = util.PackedStruct('<H').unpack(rest_eventData)
                self._env.log1('\tunicodeCode: 0x%x', unicodeCode) # The Unicode character input code.

            elif eventCode.FASTPATH_INPUT_EVENT_MOUSE:
                # http://msdn.microsoft.com/en-us/library/cc240594 2.2.8.1.2.2.3 Fast-Path Mouse Event (TS_FP_POINTER_EVENT)
                assert not eventFlags_int, eventFlags_int
                pointerFlags_int, xPos, yPos, rest_fpInputEvents = util.PackedStruct('<HHH').unpack(rest_eventData)
                pointerFlags = rdp_defs.TS_POINTER_EVENT_pointerFlags(pointerFlags_int)
                self._env.log1('\tpointerFlags: %s', pointerFlags) # The flags describing the pointer event. The possible flags are identical to those found in the pointerFlags field of the TS_POINTER_EVENT structure.
                self._env.log1('\txPos, yPos: %s, %s', xPos, yPos) # The x/y-coordinate of the pointer.

            elif eventCode.FASTPATH_INPUT_EVENT_MOUSEX:
                # http://msdn.microsoft.com/en-us/library/cc240595 2.2.8.1.2.2.4 Fast-Path Extended Mouse Event (TS_FP_POINTERX_EVENT)
                assert not eventFlags_int, eventFlags_int
                pointerFlags_int, xPos, yPos, rest_fpInputEvents = util.PackedStruct('<HHH').unpack(rest_eventData)
                pointerFlags = rdp_defs.TS_POINTERX_EVENT_pointerFlags(pointerFlags_int)
                self._env.log1('\tpointerFlags: %s', pointerFlags) # The flags describing the pointer event. The possible flags are identical to those found in the pointerFlags field of the TS_POINTERX_EVENT structure.
                self._env.log1('\txPos, yPos: %s, %s', xPos, yPos) # The x/y-coordinate of the pointer.

            elif eventCode.FASTPATH_INPUT_EVENT_SYNC:
                # http://msdn.microsoft.com/en-us/library/cc240596 2.2.8.1.2.2.5 Fast-Path Synchronize Event (TS_FP_SYNC_EVENT)
                eventFlags = rdp_defs.TS_FP_SYNC_EVENT_eventFlags(eventFlags_int)
                rest_fpInputEvents = rest_eventData
                self._env.log1('\teventFlags: %s', eventFlags) # flags indicating the "on" status of the keyboard toggle keys.

            else:
                self._env.log1('\tUnknown eventCode: %s', eventCode)
                if self._env.config.STRICT:
                    assert False, eventCode
                # We can't continue; we don't know what we we didn't understand
                rest_fpInputEvents = None
                break

        assert not rest_fpInputEvents, (rest_fpInputEvents,)

        # Re-pack with new keys
        if not numberEvents4bits:# numberEvents not encoded in fpInputHeader
            self._env.log1('numberEvents: %s', numberEvents)
            fpInputEvents = util.PackedStruct('<B').pack(numberEvents) + fpInputEvents
        new_fastpath_chunk = rdp_defs.FASTPATH_HEADER.pack(fpInputHeader, length1)
        if length1 & 0x80:
            new_fastpath_chunk = new_fastpath_chunk + rdp_defs.FASTPATH_LENGTH2.pack(length2)
        if self.server_connection_state.encryptionLevel.ENCRYPTION_LEVEL_FIPS:
            new_fastpath_chunk = new_fastpath_chunk + util.PackedStruct('<4s').pack(fipsInformation)
        if encryptionFlags.FASTPATH_INPUT_ENCRYPTED:
            new_dataSignature = self.server_connection_state.generate_mac(fpInputEvents, secure=encryptionFlags.FASTPATH_INPUT_SECURE_CHECKSUM)
            new_crypted_fpInputEvents = self.server_connection_state.client_server_xor(fpInputEvents)
            self._env.log3('new_crypted_fpInputEvents:', dump=new_crypted_fpInputEvents)
            new_fastpath_chunk = new_fastpath_chunk + new_dataSignature + new_crypted_fpInputEvents
        else:
            new_fastpath_chunk = new_fastpath_chunk + fpInputEvents

        assert len(new_fastpath_chunk) == length, (new_fastpath_chunk, length) # if it has changed then some lengths must be backpatched...
        return new_fastpath_chunk, chunk[length:]

    def analyze_capability_set(self, numberCapabilities, rest_capabilitySets):
        # http://msdn.microsoft.com/en-us/library/cc240486 2.2.1.13.1.1.1 Capability Set (TS_CAPS_SET)
        for i in range(numberCapabilities):
            h2 = util.PackedStruct('<HH')
            capabilitySetType_int, lengthCapability, _rest = h2.unpack(rest_capabilitySets)
            capabilitySetType = rdp_defs.TS_CAPS_SET_type(capabilitySetType_int)
            capabilityData = rest_capabilitySets[h2.size:lengthCapability]
            rest_capabilitySets = rest_capabilitySets[lengthCapability:]

            self._env.log1('capability %s, capabilitySetType: %s 0x%x', i, capabilitySetType, capabilitySetType_int) #dump=capabilityData)

            if capabilitySetType.CAPSTYPE_GENERAL:
                # http://msdn.microsoft.com/en-us/library/cc240549 2.2.7.1.1 General Capability Set (TS_GENERAL_CAPABILITYSET)
                (osMajorType_int, osMinorType_int, protocolVersion, _pad2octetsA, generalCompressionTypes, extraFlags_int,
                 updateCapabilityFlag, remoteUnshareFlag, generalCompressionLevel, refreshRectSupport, suppressOutputSupport,
                 rest) = util.PackedStruct('<HHHHHHHHHBB').unpack(capabilityData)
                osMajorType = rdp_defs.TS_GENERAL_CAPABILITYSET_osMajorType(osMajorType_int)
                osMinorType = rdp_defs.TS_GENERAL_CAPABILITYSET_osMinorType(osMinorType_int)
                extraFlags = rdp_defs.TS_GENERAL_CAPABILITYSET_extraFlags(extraFlags_int) # General capability information. (T128 says it is padding field ...)
                assert not rest, (rest,)

                self._env.log1('\tosMajorType: %s', osMajorType) # The type of platform.
                self._env.log1('\tosMinorType: %s', osMinorType) # The version of the platform specified in the osMajorType field.
                assert protocolVersion == 0x0200, protocolVersion # The protocol version. This field MUST be set to TS_CAPS_PROTOCOLVERSION (0x0200).
                assert not generalCompressionTypes, generalCompressionTypes # General compression types. This field MUST be set to 0.
                self._env.log1('\textraFlags: %s', extraFlags) # General capability information.
                assert not updateCapabilityFlag, updateCapabilityFlag # Support for update capability. This field MUST be set to 0.
                assert not remoteUnshareFlag, remoteUnshareFlag # Support for remote unsharing. This field MUST be set to 0.
                assert not generalCompressionLevel, generalCompressionLevel # General compression level. This field MUST be set to 0.
                self._env.log1('\trefreshRectSupport: %s', bool(refreshRectSupport)) # Server-only flag that indicates whether the Refresh Rect PDU (section 2.2.11.2) is supported.
                self._env.log1('\tsuppressOutputSupport: %s', bool(suppressOutputSupport)) # Server-only flag that indicates whether the Suppress Output PDU (section 2.2.11.3) is supported.

            elif capabilitySetType.CAPSTYPE_BITMAP:
                # http://msdn.microsoft.com/en-us/library/cc240554 2.2.7.1.2 Bitmap Capability Set (TS_BITMAP_CAPABILITYSET)
                (preferredBitsPerPixel, receive1BitPerPixel, receive4BitsPerPixel, receive8BitsPerPixel,
                    desktopWidth, desktopHeight, _pad2octets, desktopResizeFlag, bitmapCompressionFlag, highColorFlags, drawingFlags,
                    multipleRectangleSupport, _pad2octetsB, rest) = util.PackedStruct('<HHHHHHHHHBBHH').unpack(capabilityData)
                assert not rest, (rest,)
                self._env.log1('\tpreferredBitsPerPixel: %s', preferredBitsPerPixel) # Color depth of the remote session. In RDP 4.0 and 5.0, this field MUST be set to 8 (even for a 16-color session).
                self._env.log1('\treceive1BitPerPixel: %s', receive1BitPerPixel) # Indicates whether the client can receive 1 bpp. This field is ignored and SHOULD be set to TRUE (0x0001).
                self._env.log1('\treceive4BitsPerPixel: %s', receive4BitsPerPixel) # Indicates whether the client can receive 4 bpp. This field is ignored and SHOULD be set to TRUE (0x0001).
                self._env.log1('\treceive8BitsPerPixel: %s', receive8BitsPerPixel) # Indicates whether the client can receive 8 bpp. This field is ignored and SHOULD be set to TRUE (0x0001).
                self._env.log1('\tdesktopWidth: %s', desktopWidth) # The width of the desktop in the remote session.
                self._env.log1('\tdesktopHeight: %s', desktopHeight) # The height of the desktop in the remote session.
                self._env.log1('\tdesktopResizeFlag: %s', bool(desktopResizeFlag)) # Indicates whether desktop resizing is supported.
                self._env.log1('\tbitmapCompressionFlag: %s', bool(bitmapCompressionFlag)) # Indicates whether the client supports bitmap compression. RDP requires bitmap compression and hence this field MUST be set to TRUE (0x0001). If it is not set to TRUE, the server MUST NOT continue with the connection.
                assert bitmapCompressionFlag, bitmapCompressionFlag
                self._env.log1('\thighColorFlags: %s', highColorFlags) # Client support for 16 bpp color modes. This field is ignored and SHOULD be set to 0.
                self._env.log1('\tdrawingFlags: %s', rdp_defs.TS_BITMAP_CAPABILITYSET_drawingFlags(drawingFlags)) # Flags describing support for 32 bpp bitmaps.
                self._env.log1('\tmultipleRectangleSupport: %s', multipleRectangleSupport) # Indicates whether the client supports the use of multiple bitmap rectangles. RDP requires the use of multiple bitmap rectangles and hence this field MUST be set to TRUE (0x0001). If it is not set to TRUE, the server MUST NOT continue with the connection.
                #assert multipleRectangleSupport FIXME???

            elif capabilitySetType.CAPSTYPE_ORDER:
                # http://msdn.microsoft.com/en-us/library/cc240556 2.2.7.1.3 Order Capability Set (TS_ORDER_CAPABILITYSET)
                self._env.log1('\tOrder Capability Set: %s', capabilitySetType, dump=capabilityData) # TODO

            elif capabilitySetType.CAPSTYPE_BITMAPCACHE:
                # http://msdn.microsoft.com/en-us/library/cc240559 2.2.7.1.4.1 Revision 1 Bitmap Caches (TS_BITMAPCACHE_CAPABILITYSET)
                self._env.log1('\tVirtual Channel Capability Set: %s', capabilitySetType, dump=capabilityData) # TODO

            elif capabilitySetType.CAPSTYPE_CONTROL:
                # http://msdn.microsoft.com/en-us/library/cc240568 2.2.7.2.2 Control Capability Set (TS_CONTROL_CAPABILITYSET)
                self._env.log1('\tControl Capability Set: %s', capabilitySetType, dump=capabilityData) # TODO

            elif capabilitySetType.CAPSTYPE_ACTIVATION:
                # http://msdn.microsoft.com/en-us/library/cc240569 2.2.7.2.3 Window Activation Capability Set (TS_WINDOWACTIVATION_CAPABILITYSET)
                self._env.log1('\tWindow Activation Capability Set: %s', capabilitySetType, dump=capabilityData) # TODO

            elif capabilitySetType.CAPSTYPE_POINTER:
                # http://msdn.microsoft.com/en-us/library/cc240562 2.2.7.1.5 Pointer Capability Set (TS_POINTER_CAPABILITYSET)
                self._env.log1('\tPointer Capability Set: %s', capabilitySetType, dump=capabilityData) # TODO

            elif capabilitySetType.CAPSTYPE_SHARE:
                # http://msdn.microsoft.com/en-us/library/cc240570 2.2.7.2.4 Share Capability Set (TS_SHARE_CAPABILITYSET)
                nodeId, _pad2octets, rest = util.PackedStruct('<HH').unpack(capabilityData)
                assert not rest, (rest,)
                self._env.log1('\tnodeId: %s', self.channel_name(nodeId)) # This field SHOULD be set to the server channel ID by the server (in Microsoft RDP server implementations, this value is always 0x03EA).

            elif capabilitySetType.CAPSTYPE_COLORCACHE:
                # http://msdn.microsoft.com/en-us/library/cc241564 MS-RDPEGDI.2.2.1.1 Color Table Cache Capability Set (TS_COLORTABLE_CAPABILITYSET)
                self._env.log1('\tColor Table Cache Capability Set: %s', capabilitySetType, dump=capabilityData) # TODO

            elif capabilitySetType.CAPSTYPE_SOUND:
                # http://msdn.microsoft.com/en-us/library/cc240552 2.2.7.1.11 Sound Capability Set (TS_SOUND_CAPABILITYSET)
                self._env.log1('\tSound Capability Set: %s', capabilitySetType, dump=capabilityData) # TODO

            elif capabilitySetType.CAPSTYPE_INPUT:
                # http://msdn.microsoft.com/en-us/library/cc240563 2.2.7.1.6 Input Capability Set (TS_INPUT_CAPABILITY_SET)
                self._env.log1('\tInput Capability Set: %s', capabilitySetType, dump=capabilityData) # TODO

            elif capabilitySetType.CAPSTYPE_FONT:
                # http://msdn.microsoft.com/en-us/library/cc240571 2.2.7.2.5 Font Capability Set (TS_FONT_CAPABILITYSET)
                if capabilityData:
                    fontSupportFlags, _pad2octets, rest = util.PackedStruct('<HH').unpack(capabilityData)
                    assert not rest, (rest,)
                    self._env.log1('\tfontSupportFlags: %s', fontSupportFlags) # The font support options. This field SHOULD be set to FONTSUPPORT_FONTLIST (0x0001).
                else:
                    self._env.log1('\tEh??? No data???') # Seen with x-men and xrdp ... TODO: Why?

            elif capabilitySetType.CAPSTYPE_BRUSH:
                # http://msdn.microsoft.com/en-us/library/cc240564 2.2.7.1.7 Brush Capability Set (TS_BRUSH_CAPABILITYSET)
                self._env.log1('\tBrush Capability Set: %s', capabilitySetType, dump=capabilityData) # TODO

            elif capabilitySetType.CAPSTYPE_GLYPHCACHE:
                # http://msdn.microsoft.com/en-us/library/cc240565 2.2.7.1.8 Glyph Cache Capability Set (TS_GLYPHCACHE_CAPABILITYSET)
                self._env.log1('\tGlyph Cache Capability Set: %s', capabilitySetType, dump=capabilityData) # TODO

            elif capabilitySetType.CAPSTYPE_OFFSCREENCACHE:
                # http://msdn.microsoft.com/en-us/library/cc240550 2.2.7.1.9 Offscreen Bitmap Cache Capability Set (TS_OFFSCREEN_CAPABILITYSET)
                self._env.log1('\tOffscreen Bitmap Cache Capability Set: %s', capabilitySetType, dump=capabilityData) # TODO

            elif capabilitySetType.CAPSTYPE_BITMAPCACHE_HOSTSUPPORT:
                # http://msdn.microsoft.com/en-us/library/cc240557 2.2.7.2.1 Bitmap Cache Host Support Capability Set (TS_BITMAPCACHE_HOSTSUPPORT_CAPABILITYSET)
                self._env.log1('\tcapabilityData:', dump=capabilityData) # TODO

            elif capabilitySetType.CAPSTYPE_BITMAPCACHE_REV2:
                # http://msdn.microsoft.com/en-us/library/cc240560 2.2.7.1.4.2 Revision 2 (TS_BITMAPCACHE_CAPABILITYSET_REV2)
                self._env.log1('\tRevision 2 Bitmap Caches: %s', capabilitySetType, dump=capabilityData) # TODO

            elif capabilitySetType.CAPSTYPE_VIRTUALCHANNEL:
                # http://msdn.microsoft.com/en-us/library/cc240551 2.2.7.1.10 Virtual Channel Capability Set (TS_VIRTUALCHANNEL_CAPABILITYSET)
                flags_int, rest = util.PackedStruct('<H').unpack(capabilityData)
                flags = rdp_defs.TS_VIRTUALCHANNEL_CAPABILITYSET_flags(flags_int)
                if rest:
                    VCChunkSize, rest = util.PackedStruct('<H').unpack(rest)
                else:
                    VCChunkSize = None
                self._env.log1('\tflags: %s', flags)
                self._env.log1('\tVCChunkSize: %s', VCChunkSize)
                if rest:
                    self._env.log1('\tIgnoring rest:', dump=rest)

            elif capabilitySetType.CAPSTYPE_DRAWNINEGRIDCACHE:
                # http://msdn.microsoft.com/en-us/library/cc241565 2.2.1.2 DrawNineGrid Cache Capability Set (TS_DRAW_NINEGRID_CAPABILITYSET)
                self._env.log1('\tDrawNineGrid Cache Capability Set: %s', capabilitySetType, dump=capabilityData) # TODO

            elif capabilitySetType.CAPSTYPE_DRAWGDIPLUS:
                # http://msdn.microsoft.com/en-us/library/cc241566 MS-RDPEGDI 2.2.1.3 Draw GDI+ Capability Set (TS_DRAW_GDIPLUS_CAPABILITYSET)
                self._env.log1('\tDraw GDI+ Capability Set: %s', capabilitySetType, dump=capabilityData) # TODO

            elif capabilitySetType.CAPSTYPE_RAIL:
                # http://msdn.microsoft.com/en-us/library/cc242518 MS-RDPERP.2.2.1.1.1 Remote Programs Capability Set
                self._env.log1('\tRemote Programs Capability Set: %s', capabilitySetType, dump=capabilityData) # TODO

            elif capabilitySetType.CAPSTYPE_WINDOW:
                # http://msdn.microsoft.com/en-us/library/cc242564 2.2.1.1.2 Window List Capability Set
                self._env.log1('\tWindow List Capability Set: %s', capabilitySetType, dump=capabilityData) # TODO

            elif capabilitySetType.CAPSETTYPE_COMPDESK:
                # http://msdn.microsoft.com/en-us/library/cc240855 2.2.7.2.8 Desktop Composition Capability Set (TS_COMPDESK_CAPABILITYSET)
                CompDeskSupportLevel, rest = util.PackedStruct('<H').unpack(capabilityData)
                assert not rest, (rest,)
                self._env.log1('\tCompDeskSupportLevel: %s', bool(CompDeskSupportLevel))

            elif capabilitySetType.CAPSETTYPE_MULTIFRAGMENTUPDATE:
                # http://msdn.microsoft.com/en-us/library/cc240649 2.2.7.2.6 Multifragment Update Capability Set (TS_MULTIFRAGMENTUPDATE_CAPABILITYSET)
                self._env.log1('\tMaxRequestSize:', dump=capabilityData) # TODO

            elif capabilitySetType.CAPSETTYPE_LARGE_POINTER:
                # http://msdn.microsoft.com/en-us/library/cc240650 2.2.7.2.7 Large Pointer Capability Set (TS_LARGE_POINTER_CAPABILITYSET)
                self._env.log1('\tlargePointerSupportFlags:', dump=capabilityData) # TODO

            elif capabilitySetType.CAPSETTYPE_SURFACE_COMMANDS:
                # http://msdn.microsoft.com/en-us/library/dd871563 2.2.7.2.9 Surface Commands Capability Set (TS_SURFCMDS_CAPABILITYSET)
                self._env.log1('\tcmdFlags:', dump=capabilityData) # TODO

            elif capabilitySetType.CAPSETTYPE_BITMAP_CODECS:
                # http://msdn.microsoft.com/en-us/library/dd891377 2.2.7.2.10 Bitmap Codecs Capability Set (TS_BITMAPCODECS_CAPABILITYSET)
                self._env.log1('\tsupportedBitmapCodecs:', dump=capabilityData) # TODO

            elif capabilitySetType_int == 0x1e:
                self._env.log1('\tunknown undocumented capability 0x1e:', dump=capabilityData) # TODO

            else:
                self._env.log1('\tUnknown capability: %s', capabilitySetType, dump=capabilityData)
                if self._env.config.STRICT:
                    assert False, capabilitySetType

        assert not rest_capabilitySets, (rest_capabilitySets,)
