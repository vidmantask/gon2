"""
PKCS#1 from RFC 3447
"""

import os
import hashlib

import util
import asn1

import Crypto.PublicKey.RSA # usually we use preconfigured values instead of using this

# Base implementations of RSA based on python longs and no CRT

class RsaLong(object):
    """
    Encryption and decryption primitives
    http://tools.ietf.org/html/rfc3447#section-5.1
    """

    def __init__(self, modulus, public_exponent, private_exponent=None):
        self.modulus = modulus
        self.public_exponent = public_exponent
        self.private_exponent = private_exponent

    def encrypt(self, cleartext):
        return long.__pow__(cleartext, self.public_exponent, self.modulus)

    def decrypt(self, ciphertext):
        if not self.private_exponent:
            raise RuntimeError('No private exponent')
        return long.__pow__(ciphertext, self.private_exponent, self.modulus)

    def __repr__(self):
        if self.private_exponent:
            return '<%s mod=%s pubexp=%s privexp=%s>' % (self.__class__.__name__, self.modulus, self.public_exponent, self.private_exponent)
        else:
            return '<%s mod=%s pubexp=%s>' % (self.__class__.__name__, self.modulus, self.public_exponent)

    @classmethod
    def new(cls, bits=512):
        rsa = Crypto.PublicKey.RSA.generate(bits, os.urandom)
        # rsa.e is public key exponent, F4
        # rsa.n is public key modulus
        # rsa.d is private key exponent, d = e^-1 mod n
        #print rsa.e, rsa.n, rsa.d, rsa.p, rsa.q
        return cls(rsa.n, rsa.e, rsa.d)


def i2osp(long_val):
    """
    I2OSP converts a nonnegative integer to an octet string of a specified length.
    http://tools.ietf.org/html/rfc3447#section-4.1
    """
    le_bytes = []
    while long_val or len(le_bytes) & 1: # until all done AND even number
        le_bytes.append(int(long_val & 0xff))
        long_val >>= 8
    return ''.join(chr(x) for x in reversed(le_bytes))

def os2ip(octetstring):
    """
    OS2IP converts an octet string to a nonnegative integer.
    http://tools.ietf.org/html/rfc3447#section-4.2
    """
    long_val = 0L
    for c in octetstring:
        long_val = (long_val << 8) | ord(c)
    return long_val


# http://tools.ietf.org/html/rfc3447#appendix-A.1
# http://www.oid-info.com/get/1.2.840.113549.1.1.1
# Description: RSA (PKCS #1 v1.5) key transport algorithm,
# Defined in RFC 2313, RFC 2437. See also RFC 3370.
# See also the equivalent but deprecated OID {joint-iso-itu-t(2) ds(5) algorithm(8) encryptionAlgorithm(1) rsa(1)}.
asn1.oids.OIDS.declare('rsaEncryption',
    '{iso(1) member-body(2) us(840) rsadsi(113549) pkcs(1) pkcs-1(1) rsaEncryption(1)}',
    selects=asn1.NULL())

# http://www.oid-info.com/get/1.2.840.113549.1.9.1
# Description: PKCS #9 Email Address attribute for use in signatures
# Deprecated, use an altName extension instead
asn1.oids.OIDS.declare('emailAddress',
    '{iso(1) member-body(2) us(840) rsadsi(113549) pkcs(1) pkcs-9(9) emailAddress(1)}',
    selects=asn1.IA5String())

# http://tools.ietf.org/html/rfc3447#appendix-C

asn1.oids.OIDS.declare('md2WithRSAEncryption',
    '{iso(1) member-body(2) us(840) rsadsi(113549) pkcs(1) pkcs-1(1) md2WithRSAEncryption(2)}',
    selects=asn1.NULL())

asn1.oids.OIDS.declare('md5WithRSAEncryption',
    '{iso(1) member-body(2) us(840) rsadsi(113549) pkcs(1) pkcs-1(1) md5WithRSAEncryption(4)}',
    selects=asn1.NULL())

asn1.oids.OIDS.declare('sha1WithRSAEncryption',
    '{iso(1) member-body(2) us(840) rsadsi(113549) pkcs(1) pkcs-1(1) sha1WithRSAEncryption(5)}',
    selects=asn1.NULL())

asn1.oids.OIDS.declare('sha256WithRSAEncryption',
    '{iso(1) member-body(2) us(840) rsadsi(113549) pkcs(1) pkcs-1(1) sha256WithRSAEncryption(11)}',
    selects=asn1.NULL())

asn1.oids.OIDS.declare('sha384WithRSAEncryption',
    '{iso(1) member-body(2) us(840) rsadsi(113549) pkcs(1) pkcs-1(1) sha384WithRSAEncryption(12)}',
    selects=asn1.NULL())

asn1.oids.OIDS.declare('sha512WithRSAEncryption',
    '{iso(1) member-body(2) us(840) rsadsi(113549) pkcs(1) pkcs-1(1) sha512WithRSAEncryption(13)}',
    selects=asn1.NULL())

# http://www.oid-info.com/get/1.3.14.3.2.15
# Description: SHA with RSA signature algorithm (Oddball OIW OID using 9796-2 padding rules)
# Seen on NetPartner 2008TS ...
asn1.oids.OIDS.declare('shaWithRSASignature',
    '{iso(1) identified-organization(3) oiw(14) secsig(3) algorithms(2) shaWithRSASignature(15)}',
    selects=asn1.NULL())

# http://www.oid-info.com/get/1.3.14.3.2.29
# Description: SHA1 with RSA signature (obsolete)
# This algorithm uses the RSA signature algorithm to sign a 160-but SHA1 digest.
asn1.oids.OIDS.declare('sha1WithRSAEncryption-obsolete',
    '{iso(1) identified-organization(3) oiw(14) secsig(3) algorithms(2) sha1WithRSAEncryption-obsolete(29)}',
    selects=asn1.NULL())


# http://tools.ietf.org/html/rfc3447#appendix-B.1

asn1.oids.OIDS.declare('id-md2',
    '{iso(1) member-body(2) us(840) rsadsi(113549) digestAlgorithm(2) 2}',
    selects=asn1.NULL())

asn1.oids.OIDS.declare('id-md5',
    '{iso(1) member-body(2) us(840) rsadsi(113549) digestAlgorithm(2) 5}',
    selects=asn1.NULL())

asn1.oids.OIDS.declare('id-sha1',
    '{iso(1) identified-organization(3) oiw(14) secsig(3) algorithms(2) 26}',
    selects=asn1.NULL())

asn1.oids.OIDS.declare('id-sha256',
    '{joint-iso-itu-t(2) country(16) us(840) organization(1) gov(101) csor(3) nistalgorithm(4) hashalgs(2) 1}',
    selects=asn1.NULL())

asn1.oids.OIDS.declare('id-sha384',
    '{joint-iso-itu-t(2) country(16) us(840) organization(1) gov(101) csor(3) nistalgorithm(4) hashalgs(2) 2}',
    selects=asn1.NULL())

asn1.oids.OIDS.declare('id-sha512',
    '{joint-iso-itu-t(2) country(16) us(840) organization(1) gov(101) csor(3) nistalgorithm(4) hashalgs(2) 3}',
    selects=asn1.NULL())

# http://tools.ietf.org/html/rfc3447#appendix-A.2.4

AlgorithmIdentifier = asn1.SEQUENCE( # like x509.AttributeTypeAndValue
    asn1.Field('type', asn1.OBJECT_IDENTIFIER()),
    asn1.Field('value', asn1.ANY_DEFINED_BY('type')),
    )

DigestInfo = asn1.SEQUENCE(
    asn1.Field('digestAlgorithm', AlgorithmIdentifier()),
    asn1.Field('digest', asn1.OCTET_STRING()),
    )

TaggedDigestInfo = asn1.Field(None, DigestInfo())

# RFC 3447 RSA private key syntax

Version = asn1.INTEGER() # { two-prime(0), multi(1) } (CONSTRAINED BY {-- version must be multi if otherPrimeInfos present --})

OtherPrimeInfo = asn1.SEQUENCE(
    asn1.Field('prime', asn1.INTEGER()), # ri
    asn1.Field('exponent', asn1.INTEGER()), # di
    asn1.Field('coefficient', asn1.INTEGER()), # ti
    )

OtherPrimeInfos = asn1.SEQUENCE_OF(OtherPrimeInfo(), min_size=1) # SIZE(1..MAX)

RSAPrivateKey = asn1.SEQUENCE(
    asn1.Field('version', Version()),
    asn1.Field('modulus', asn1.INTEGER()), # n
    asn1.Field('publicExponent', asn1.INTEGER()), # e
    asn1.Field('privateExponent', asn1.INTEGER()), # d
    asn1.Field('prime1', asn1.INTEGER()), # p
    asn1.Field('prime2', asn1.INTEGER()), # q
    asn1.Field('exponent1', asn1.INTEGER()), # d mod (p-1)
    asn1.Field('exponent2', asn1.INTEGER()), # d mod (q-1)
    asn1.Field('coefficient', asn1.INTEGER()), # (inverse of q) mod p
    asn1.Field('otherPrimeInfos', OtherPrimeInfos(), optional=True),
    )

TaggedRSAPrivateKey = asn1.Field(None, RSAPrivateKey())

def private_key_rsa(rsapriv):
    return RsaLong(rsapriv['modulus'], rsapriv['publicExponent'], rsapriv['privateExponent'])

def verify(ca_pub_key, subj_string, signature_string, logger=None):
    cleartext_padded = i2osp(ca_pub_key.encrypt(os2ip(signature_string)))
    if logger: logger('cleartext_padded:', dump=cleartext_padded)
    # http://tools.ietf.org/html/rfc3447#section-9.2
    zeropos = cleartext_padded.find('\x00', 2)
    assert cleartext_padded.startswith('\x00\x01' + '\xff' * (zeropos - 2)), 'Signature cleartext %r not properly pkcs1 padded' % cleartext_padded
    cleartext = cleartext_padded[zeropos + 1:]
    if logger: logger('cleartext:', dump=cleartext)
    signature_struct = TaggedDigestInfo.ber_decode(cleartext)
    if logger: logger('signature_struct:', pprint=signature_struct)
    signature_digest = signature_struct['digest']
    if logger: logger('signature_digest:', dump=signature_digest)

    # SHA-1 => (0x)30 21 30 09 06 05 2b 0e 03 02 1a 05 00 04 14 || H.
    oid = signature_struct['digestAlgorithm']['type'][0]
    if oid == (1, 3, 14, 3, 2, 26):
        calculated_digest = hashlib.sha1(subj_string).digest()
    elif oid == (1, 2, 840, 113549, 2, 5): # id-md5(iso(1).member-body(2).us(840).rsadsi(113549).digestAlgorithm(2).id-md5(5)
        calculated_digest = hashlib.md5(subj_string).digest()
    elif oid == (2, 16, 840, 1, 101, 3, 4, 2, 1): 
        calculated_digest = hashlib.sha256(subj_string).digest()
    else:
        raise RuntimeError('Digest algorithm %s' % (signature_struct['digestAlgorithm']['type'], ))
    if logger: logger('calculated_digest:', dump=calculated_digest)

    assert calculated_digest == signature_digest, 'Signature digest doesn\'t match actual digest!'
    return True


def sign(ca_key, subj_string, logger=None):
    # Note: We always sign with sha1, so we also set digestAlgorithm
    calculated_digest = hashlib.sha1(subj_string).digest()
    if logger: logger('calculated_digest:', dump=calculated_digest)

    signature_struct = {
        'digest': calculated_digest,
        'digestAlgorithm': {
            'type': (
                (1, 3, 14, 3, 2, 26),
                'id-sha1(iso(1).identified-organization(3).oiw(14).secsig(3).algorithms(2).id-sha1(26))'),
            'value': None}}

    cleartext = TaggedDigestInfo.der_encode(signature_struct)

    # http://tools.ietf.org/html/rfc3447#section-9.2
    cleartext_padded = '\x00\x01' + '\xff' * (64 - 2 - 1 - len(cleartext)) + '\x00' + cleartext
    if logger: logger('cleartext_padded:', dump=cleartext_padded)

    signature_string = i2osp(ca_key.decrypt(os2ip(cleartext_padded)))
    if logger: logger('signature_string:', dump=signature_string)
    return signature_string


def test_rsa():
    bits = 1024 # 2048 takes too long ...
    rsa = RsaLong.new(bits)
    print "Encrypt test:", bits
    cleartext = 1 << (bits-1)
    print 'cleartext:', hex(cleartext)
    ciphertext = rsa.encrypt(cleartext)
    #print 'ciphertext:', hex(ciphertext) # ephemeral
    deciphertext = rsa.decrypt(ciphertext)
    print 'deciphertext:', hex(deciphertext)
    print deciphertext == cleartext
    print

def test_pkcs11():
    print 'sample pkcs1 content'
    pkcs1_padding = '0!0\t\x06\x05+\x0e\x03\x02\x1a\x05\x00\x04\x14once upon a time the' # '\x00'
    asn1.test_ber_roundtrip('pkcs1', TaggedDigestInfo, pkcs1_padding)
    print

    print 'CK_MECHANISM CKM_RSA_PKCS'
    data = '\x30\x21\x30\x09\x06\x05\x2B\x0E\x03\x02\x1A\x05\x00\x04\x14' + '01234567890123456789'
    asn1.test_ber_roundtrip('CKM_RSA_PKCS', TaggedDigestInfo, data)
    print

    print 'encode a TaggedDigestInfo'
    print repr(TaggedDigestInfo.der_encode({'digestAlgorithm': {'type': ((1, 3, 14, 3, 2, 26), None), 'value': None}, 'digest': '01234567890123456789', }))
    print

def main():
    util.hook()
    # Tested from x509 ...
    test_rsa()
    test_pkcs11()

if __name__ == '__main__':
    main()
