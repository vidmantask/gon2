# Testing pkcs7.asn
# Module PKCS-7 { iso(1) member-body(2) us(840) rsadsi(113549) pkcs(1) 7 0 }

import asn1

AlgorithmIdentifier = asn1.SEQUENCE(
    asn1.Field('algorithm', asn1.OBJECT_IDENTIFIER()),
    asn1.Field('parameters', asn1.ANY_DEFINED_BY('algorithm'), optional=True),
    )
AttributeType = asn1.OBJECT_IDENTIFIER()
AttributeValue = asn1.ANY
AttributeValueAssertion = asn1.SEQUENCE(
    asn1.Field('AttributeType', AttributeType),
    asn1.Field('AttributeValue', AttributeValue),
    )
Validity = asn1.SEQUENCE(
    asn1.Field('notBefore', asn1.UTCTime),
    asn1.Field('notAfter', asn1.UTCTime),
    )
SubjectPublicKeyInfo = asn1.SEQUENCE(
    asn1.Field('algorithm', AlgorithmIdentifier),
    asn1.Field('subjectPublicKey', asn1.BIT_STRING),
    )
UniqueIdentifier = asn1.BIT_STRING
Extension = asn1.SEQUENCE(
    asn1.Field('extnId', asn1.OBJECT_IDENTIFIER()),
    asn1.Field('critical', asn1.BOOLEAN, default=False),
    asn1.Field('extnValue', asn1.OCTET_STRING),
    )
CertificateSerialNumber = asn1.INTEGER
SerialNumber = asn1.INTEGER
SignatureAlgorithmIdentifier = AlgorithmIdentifier
Signature = asn1.BIT_STRING
ContentEncryptionAlgorithmIdentifier = AlgorithmIdentifier
DigestAlgorithmIdentifier = AlgorithmIdentifier
DigestEncryptionAlgorithmIdentifier = AlgorithmIdentifier
KeyEncryptionAlgorithmIdentifier = AlgorithmIdentifier
Version = asn1.INTEGER
ContentType = asn1.OBJECT_IDENTIFIER()
Data = asn1.OCTET_STRING
DigestAlgorithmIdentifiers = asn1.SET_OF(DigestAlgorithmIdentifier)
EncryptedDigest = asn1.OCTET_STRING
Digest = asn1.OCTET_STRING
EncryptedContent = asn1.OCTET_STRING
EncryptedKey = asn1.OCTET_STRING
asn1.oids.OIDS.declare('pkcs-7', '{ iso(1) member-body(2) us(840) rsadsi(113549) pkcs(1) 7 }')
asn1.oids.OIDS.declare('data', '{ pkcs-7 1 }')
asn1.oids.OIDS.declare('signedData', '{ pkcs-7 2 }')
asn1.oids.OIDS.declare('envelopedData', '{ pkcs-7 3 }')
asn1.oids.OIDS.declare('signedAndEnvelopedData', '{ pkcs-7 4 }')
asn1.oids.OIDS.declare('digestedData', '{ pkcs-7 5 }')
asn1.oids.OIDS.declare('encryptedData', '{ pkcs-7 6 }')
Attribute = asn1.SEQUENCE(
    asn1.Field('type', AttributeType),
    asn1.Field('values', asn1.SET_OF(AttributeValue)),
    )
RelativeDistinguishedName = asn1.SET_OF(AttributeValueAssertion)
Extensions = asn1.SEQUENCE_OF(Extension)
CRLEntry = asn1.SEQUENCE(
    asn1.Field('userCertificate', SerialNumber),
    asn1.Field('revocationDate', asn1.UTCTime),
    )
Attributes = asn1.SET_OF(Attribute)
ContentInfo = asn1.SEQUENCE(
    asn1.Field('contentType', ContentType),
    asn1.Field('content', asn1.ANY_DEFINED_BY('contentType'), explicit=asn1.CONTEXT(0), optional=True),
    )
DigestInfo = asn1.SEQUENCE(
    asn1.Field('digestAlgorithm', DigestAlgorithmIdentifier),
    asn1.Field('digest', Digest),
    )
EncryptedContentInfo = asn1.SEQUENCE(
    asn1.Field('contentType', ContentType),
    asn1.Field('contentEncryptionAlgorithm', ContentEncryptionAlgorithmIdentifier),
    asn1.Field('encryptedContent', EncryptedContent, asn1.CONTEXT(0), optional=True),
    )
DigestedData = asn1.SEQUENCE(
    asn1.Field('version', Version),
    asn1.Field('digestAlgorithm', DigestAlgorithmIdentifier),
    asn1.Field('contentInfo', ContentInfo),
    asn1.Field('digest', Digest),
    )
EncryptedData = asn1.SEQUENCE(
    asn1.Field('version', Version),
    asn1.Field('encryptedContentInfo', EncryptedContentInfo),
    )
RDNSequence = asn1.SEQUENCE_OF(RelativeDistinguishedName)
Name = asn1.CHOICE(
    asn1.Field('RDNSequence', RDNSequence),
    )
TBSCertificateRevocationList = asn1.SEQUENCE(
    asn1.Field('signature', AlgorithmIdentifier),
    asn1.Field('issuer', Name),
    asn1.Field('lastUpdate', asn1.UTCTime),
    asn1.Field('nextUpdate', asn1.UTCTime),
    asn1.Field('revokedCertificates', asn1.SEQUENCE_OF(CRLEntry), optional=True),
    )
IssuerAndSerialNumber = asn1.SEQUENCE(
    asn1.Field('issuer', Name),
    asn1.Field('serialNumber', CertificateSerialNumber),
    )
SignerInfo = asn1.SEQUENCE(
    asn1.Field('version', Version),
    asn1.Field('issuerAndSerialNumber', IssuerAndSerialNumber),
    asn1.Field('digestAlgorithm', DigestAlgorithmIdentifier),
    asn1.Field('authenticatedAttributes', Attributes, asn1.CONTEXT(0), optional=True),
    asn1.Field('digestEncryptionAlgorithm', DigestEncryptionAlgorithmIdentifier),
    asn1.Field('encryptedDigest', EncryptedDigest),
    asn1.Field('unauthenticatedAttributes', Attributes, asn1.CONTEXT(1), optional=True),
    )
RecipientInfo = asn1.SEQUENCE(
    asn1.Field('version', Version),
    asn1.Field('issuerAndSerialNumber', IssuerAndSerialNumber),
    asn1.Field('keyEncryptionAlgorithm', KeyEncryptionAlgorithmIdentifier),
    asn1.Field('encryptedKey', EncryptedKey),
    )
TBSCertificate = asn1.SEQUENCE(
    asn1.Field('version', Version, explicit=asn1.CONTEXT(0), default='v1'),
    asn1.Field('serialNumber', CertificateSerialNumber),
    asn1.Field('signature', AlgorithmIdentifier),
    asn1.Field('issuer', Name),
    asn1.Field('validity', Validity),
    asn1.Field('subject', Name),
    asn1.Field('subjectPublicKeyInfo', SubjectPublicKeyInfo),
    asn1.Field('issuerUniqueIdentifier', UniqueIdentifier, asn1.CONTEXT(1), optional=True),
    asn1.Field('subjectUniqueIdentifier', UniqueIdentifier, asn1.CONTEXT(2), optional=True),
    asn1.Field('extensions', Extensions, explicit=asn1.CONTEXT(3), optional=True),
    )
CertificateRevocationList = asn1.SEQUENCE(
    asn1.Field('tbsCertificateRevocationList', TBSCertificateRevocationList),
    asn1.Field('signatureAlgorithm', AlgorithmIdentifier),
    asn1.Field('signature', asn1.BIT_STRING),
    )
CertificateRevocationLists = asn1.SET_OF(CertificateRevocationList)
SignerInfos = asn1.SET_OF(SignerInfo)
RecipientInfos = asn1.SET_OF(RecipientInfo)
Certificate = asn1.SEQUENCE(
    asn1.Field('tbsCertificate', TBSCertificate),
    asn1.Field('signatureAlgorithm', AlgorithmIdentifier),
    asn1.Field('signature', asn1.BIT_STRING),
    )
ExtendedCertificateInfo = asn1.SEQUENCE(
    asn1.Field('version', Version),
    asn1.Field('certificate', Certificate),
    asn1.Field('attributes', Attributes),
    )
EnvelopedData = asn1.SEQUENCE(
    asn1.Field('version', Version),
    asn1.Field('recipientInfos', RecipientInfos),
    asn1.Field('encryptedContentInfo', EncryptedContentInfo),
    )
ExtendedCertificate = asn1.SEQUENCE(
    asn1.Field('extendedCertificateInfo', ExtendedCertificateInfo),
    asn1.Field('signatureAlgorithm', SignatureAlgorithmIdentifier),
    asn1.Field('signature', Signature),
    )
ExtendedCertificateOrCertificate = asn1.CHOICE(
    asn1.Field('certificate', Certificate),
    asn1.Field('extendedCertificate', ExtendedCertificate, asn1.CONTEXT(0)),
    )
ExtendedCertificatesAndCertificates = asn1.SET_OF(ExtendedCertificateOrCertificate)
SignedData = asn1.SEQUENCE(
    asn1.Field('version', Version),
    asn1.Field('digestAlgorithms', DigestAlgorithmIdentifiers),
    asn1.Field('contentInfo', ContentInfo),
    asn1.Field('certificates', ExtendedCertificatesAndCertificates, asn1.CONTEXT(0), optional=True),
    asn1.Field('crls', CertificateRevocationLists, asn1.CONTEXT(1), optional=True),
    asn1.Field('signerInfos', SignerInfos),
    )
SignedAndEnvelopedData = asn1.SEQUENCE(
    asn1.Field('version', Version),
    asn1.Field('recipientInfos', RecipientInfos),
    asn1.Field('digestAlgorithms', DigestAlgorithmIdentifiers),
    asn1.Field('encryptedContentInfo', EncryptedContentInfo),
    asn1.Field('certificates', ExtendedCertificatesAndCertificates, asn1.CONTEXT(0), optional=True),
    asn1.Field('crls', CertificateRevocationLists, asn1.CONTEXT(1), optional=True),
    asn1.Field('signerInfos', SignerInfos),
    )

