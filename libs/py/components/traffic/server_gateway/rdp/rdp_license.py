"""
Crazy license circus for RDP
"""
# http://msdn.microsoft.com/en-us/library/cc241890 1.3.3 Licensing PDU Flows

import hashlib

import util
import rdp_rc4


def SHA(s):
    return hashlib.sha1(s).digest()

def MD5(s):
    return hashlib.md5(s).digest()

class LicenseKeys(object):
    # http://msdn.microsoft.com/en-us/library/cc241992 5.1.3 Generating the Licensing Encryption and MAC Salt Keys

    def __init__(self, ClientRandom, ServerRandom, PreMasterSecret):
        #print 'ClientRandom'; util.dump(ClientRandom);
        #print 'ServerRandom'; util.dump(ServerRandom);
        #print 'PreMasterSecret'; util.dump(PreMasterSecret);
        # The client and server random values and the decrypted premaster secret are first used to generate a 384-bit master secret.
        def SaltedHash(S, I):
            return MD5(S + SHA(I + S + ClientRandom + ServerRandom))
        def PreMasterHash(I):
            return SaltedHash(PreMasterSecret, I)
        MasterSecret = PreMasterHash('A') + PreMasterHash('BB') + PreMasterHash('CCC')
        #print 'MasterSecret'; util.dump(MasterSecret);

        # A 384-bit SessionKeyBlob is generated.
        def SaltedHash2(S, I):
            return MD5(S + SHA(I + S + ServerRandom + ClientRandom))

        def MasterHash(I):
            return SaltedHash2(MasterSecret, I)
        SessionKeyBlob = MasterHash('A') + MasterHash('BB') + MasterHash('CCC')

        # The first 128 bits of the SessionKeyBlob are used to generate the MAC salt key.
        self.MAC_salt_key = SessionKeyBlob[:128 / 8]
        # The MAC salt key is used to generate the MAC checksum that the recipient uses to check the integrity of the licensing message.
        # http://msdn.microsoft.com/en-us/library/cc241995 5.1.6 MAC Generation

        # The licensing encryption key is derived from the SessionKeyBlob.
        def FinalHash(K):
            return MD5(K + ClientRandom + ServerRandom)
        self.LicensingEncryptionKey = FinalHash(SessionKeyBlob[128 / 8:2 * 128 / 8])


    # http://msdn.microsoft.com/en-us/library/cc241994 5.1.5 Decrypting Licensing Session Data
    def xor(self, EncryptedData):
        rc4_prng = rdp_rc4.PRNG(self.LicensingEncryptionKey)
        return rc4_prng.xor(EncryptedData)


def _test_freerdp():
    print 'testing freerdp dump'
    client_random = util.undump("""
0000 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ................
0010 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ................
        """)
    server_random = util.undump("""
0000 89 f7 68 3f 92 bf f6 12 15 dc 75 73 05 77 62 b7 ..h?......us.wb.
0010 76 5a c7 47 30 5f 01 a0 99 df e9 08 f5 2f c8 e1 vZ.G0_......./..
        """)
    pre_master_secret = util.undump("""
0000 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ................
0010 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ................
0020 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ................
        """)
    _master_secret = util.undump("""
0000 22 11 83 4a cd 19 39 f2 94 59 fa 59 88 e6 94 3e "..J..9..Y.Y...>
0010 22 18 99 cd 86 59 a3 fd 84 4a 80 73 84 ef 33 6a "....Y...J.s..3j
0020 9b d9 c7 a8 1f 01 39 5d 95 39 f4 97 f1 8f 27 7f ......9].9....'.
        """)
    _key_block = util.undump("""
0000 2d 9d 60 a3 3f 03 18 85 fc b0 44 3c 7a 12 77 0d -.`.?.....D<z.w.
0010 10 41 4f 1b 77 fc 9a de e7 33 df 13 a9 f4 8a 0f .AO.w....3......
0020 72 18 cc aa aa c5 de 0b 35 13 44 ac d5 db 0e 9b r.......5.D.....
        """)
    _licence_sign_key = util.undump("""
0000 2d 9d 60 a3 3f 03 18 85 fc b0 44 3c 7a 12 77 0d -.`.?.....D<z.w.
        """)
    _licence_key = util.undump("""
0000 bf 8d c6 23 c0 b9 88 53 06 f1 bf 48 e0 3a 14 71 ...#...S...H.:.q
        """)
    _licence_key = util.undump("""
0000 bf 8d c6 23 c0 b9 88 53 06 f1 bf 48 e0 3a 14 71 ...#...S...H.:.q
        """)
    crypted = util.undump("""
0000 12 77 41 5e 67 a5 30 16 a0 b1                   .wA^g.0...
        """)
    result_token = util.undump("""
0000 54 00 45 00 53 00 54 00 00 00                   T.E.S.T...
        """)

    print 'crypted'; util.dump(crypted)
    s = LicenseKeys(client_random, server_random, pre_master_secret)
    print 'LicensingEncryptionKey'; util.dump(s.LicensingEncryptionKey)
    decrypted = s.xor(crypted)
    print 'decrypted'; util.dump(decrypted)
    assert decrypted == result_token, (decrypted, result_token)

    print 'MAC_salt_key'; util.dump(s.MAC_salt_key)
    print

def _test_msdn(): # BROKEN EXAMPLE!!!
    # http://msdn.microsoft.com/en-us/library/cc242011 4.1 SERVER LICENSE REQUEST
    ServerRandom = util.to_str(int(s, 16) for s in """
84 ef ae 20 b1 d5 9e 36
49 1a e8 2e 0a 99 89 ac
49 a6 47 4f 33 9b 5a b9
95 03 a6 c6 c2 3c 3f 61
        """.strip().split())

    # http://msdn.microsoft.com/en-us/library/cc242006 4.2 CLIENT NEW LICENSE REQUEST
    ClientRandom = util.to_str(int(s, 16) for s in """
dc 73 a0 c8 69 25 6b 18
af 0b 94 7a a9 a5 20 af
8b bc 0d cc a3 95 b7 b9
eb 81 5d be 0a 10 9c d8
        """.strip().split())
    PreMasterSecret = util.to_str(int(s, 16) for s in """
cf 7a db cb fb 0e 15 23 87 1c 84 81 ba 9d 4e 15
bb d2 56 bd d8 f7 f3 16 cc 35 3b e1 93 42 78 dd
92 9a e4 7a e2 99 d4 73 b1 aa 6f 55 94 3b c9 bc
        """.strip().split())

    # http://msdn.microsoft.com/en-us/library/cc242013 4.4 SERVER PLATFORM CHALLENGE
    EncryptedPlatformChallenge = util.to_str(int(s, 16) for s in """
46 37 85 54 8e c5 91 34
                  97 5d
        """.strip().split())

    s = LicenseKeys(ClientRandom, ServerRandom, PreMasterSecret)
    print 'decrypted'; util.dump(s.xor(EncryptedPlatformChallenge))

if __name__ == '__main__':
    _test_freerdp()
    #_test_msdn()
