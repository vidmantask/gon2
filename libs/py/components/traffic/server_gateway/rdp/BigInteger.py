class BigInteger(object):
    """
    Java-ish BigInteger
    """
    def __init__(self, *values):
        self.value = 0L
        try:
            (val,) = values
            self.value = long(val)
        except ValueError:
            (signum, magnitude) = values
            assert signum == 1, signum
            for b in magnitude:
                assert b == b & 0xff, b
                self.value = (self.value << 8) | b

    def modPow(self, exponent, m):
        return BigInteger(long.__pow__(self.value, exponent.value, m.value))

    def toByteArray(self):
        l = []
        val = self.value
        while val:
            l.append(int(val & 0xff))
            val >>= 8
        l.reverse()
        return l
