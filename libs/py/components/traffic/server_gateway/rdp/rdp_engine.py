import util
import tpkt
import x224
import rdp_protocol

STATE_ERROR = 'STATE_ERROR'
STATE_X224_UNCONNECTED = 'STATE_X224_UNCONNECTED' # start state
STATE_X224_UNCONFIRMED = 'STATE_X224_UNCONFIRMED'
STATE_X224_CONNECTED = 'STATE_X224_CONNECTED'

class CloseConnection(Exception): pass


class RdpEngine(object):
    """
    Handle the TPKT and X224 layers of the protocol before it gets to the real RDP stuff
    """

    def __init__(self, env):
        """
        None
        """
        self._env = env
        self._state = STATE_X224_UNCONNECTED
        self.rdp = rdp_protocol.RdpPdu(self._env)

    NeedMoreData = util.NeedMoreData

    CloseConnection = CloseConnection

    def from_client(self, in_data):
        """
        Handles data from client
        Returns tuple: data to server, excess data
        Might raise NeedMoreData or CloseConnection
        """
        self._env.log1('')
        self._env.log1('Client -> Server (%s)', len(in_data))

        if self._state == STATE_ERROR:
            self._env.log1('Data received from client after error', dump=in_data)
            raise CloseConnection

        if in_data.startswith('\x03'):
            # A TPKT Header, as specified in [T123] section 8, happens to be common for "all" messages
            # (except http://msdn.microsoft.com/en-us/library/cc240589 2.2.8.1.2 Client Fast-Path Input Event PDU (TS_FP_INPUT_PDU))
            tpkt_body, in_buffer_ok = tpkt.parse(in_data)

            excess = in_data[in_buffer_ok:]
            #self._env.log1('Client TPKT chunk size: %s', len(tpkt_chunk))

            if self._state == STATE_X224_UNCONNECTED: # not yet x224-connected
                # http://msdn.microsoft.com/en-us/library/cc240663 3.2.5.3.1 Sending X.224 Connection Request PDU
                # http://msdn.microsoft.com/en-us/library/cc240470 2.2.1.1 Client X.224 Connection Request PDU
                self._env.log1('Client X.224 Connection Request PDU')
                x224_body = x224.parse_request(tpkt_body)
                new_x224_body = self.rdp.ClientServerX224Request(x224_body)
                new_tpkt_body = x224.encode_request(new_x224_body)

                self._state = STATE_X224_UNCONFIRMED

            elif self._state == STATE_X224_CONNECTED:

                new_tpkt_body = None
                x224_body = x224.parse_data(tpkt_body)
                if not x224_body:
                    self._env.log1('No x224 body')
                    raise CloseConnection
                new_x224_body = self.rdp.ClientServerPdu(x224_body)
                if not new_x224_body:
                    self._env.log1('No new x224 body')
                    raise CloseConnection
                new_tpkt_body = x224.encode_data(new_x224_body)

            else: # end of x224 states

                self._env.log1('Data received from client in bad state %s:', self._state, dump=tpkt_body)
                raise CloseConnection

            if not new_tpkt_body:
                self._env.log1('No new tpkt body')
                raise CloseConnection

            new_tpkt_chunk = tpkt.encode(new_tpkt_body)

        else: # apparently not TPKT Header - let's assume it is fastpath

            new_tpkt_chunk, excess = self.rdp.fastpath_from_client_to_server(in_data)

        if not new_tpkt_chunk:
            self._env.log1('No new tpkt chunk')
            raise CloseConnection

        return (new_tpkt_chunk, excess)

    def from_server(self, in_data):
        """
        Handles data from server
        Returns tuple: data to client, excess data
        Might raise NeedMoreData or CloseConnection
        """
        self._env.log1('')
        self._env.log1('Server -> Client (%s)', len(in_data))

        if self._state == STATE_ERROR:
            self._env.log1('Data received from server after error', dump=in_data)
            raise CloseConnection

        if in_data.startswith('\x03'):
            # A TPKT Header, as specified in [T123] section 8, happens to be common for "all" messages
            # (except http://msdn.microsoft.com/en-us/library/cc240589 2.2.8.1.2 Client Fast-Path Input Event PDU (TS_FP_INPUT_PDU))
            tpkt_body, parse_buffer_ok = tpkt.parse(in_data)

            excess = in_data[parse_buffer_ok:]
            #self._env.log1('Server TPKT chunk size: %s', len(tpkt_chunk))

            if self._state == STATE_X224_UNCONFIRMED: # not yet fully x224 connected
                # http://msdn.microsoft.com/en-us/library/cc240677 3.2.5.3.2 Processing X.224 Connection Confirm PDU
                # http://msdn.microsoft.com/en-us/library/cc240501 2.2.1.2 Server X.224 Connection Confirm PDU
                self._env.log1('Server X.224 Connection Confirm PDU')
                x224_body = x224.parse_confirm(tpkt_body)
                new_x224_body = self.rdp.ServerClientX224Confirm(x224_body)
                new_tpkt_body = x224.encode_confirm(new_x224_body)

                self._state = STATE_X224_CONNECTED

            elif self._state == STATE_X224_CONNECTED:

                new_tpkt_body = None
                x224_body = x224.parse_data(tpkt_body)
                if not x224_body:
                    self._env.log1('No x224 body')
                    raise CloseConnection
                new_x224_body = self.rdp.ServerClientPdu(x224_body)
                if not new_x224_body:
                    self._env.log1('No new x224 body')
                    raise CloseConnection
                new_tpkt_body = x224.encode_data(new_x224_body)

            else: # end of x224 states

                self._env.log1('Data received from server in bad state %s:', self._state, dump=tpkt_body)
                raise CloseConnection

            if not new_tpkt_body:
                self._env.log1('No new tpkt body')
                raise CloseConnection

            new_tpkt_chunk = tpkt.encode(new_tpkt_body)

        else: # apparently not TPKT Header - let's assume it is fastpath

            new_tpkt_chunk, excess = self.rdp.fastpath_from_server_to_client(in_data)

        if not new_tpkt_chunk:
            self._env.log1('No new tpkt chunk')
            raise CloseConnection

        return (new_tpkt_chunk, excess)
