"""
TPKT from T123 - aka ISO DP 8073
"""

import util

TPKT_HEADER = util.PackedStruct('>BBH')

def parse(tpkt_chunk):
    """
    tpktHeader (4 bytes): A TPKT Header, as specified in [T123] section 8.
    Returns payload and how much has been read and used
    """
    if len(tpkt_chunk) < TPKT_HEADER.size:
        raise util.NeedMoreData(TPKT_HEADER.size - len(tpkt_chunk))
    tpkt_version, tpkt_reserved, tpkt_length, _rest = TPKT_HEADER.unpack(tpkt_chunk)
    assert tpkt_version == 3, tpkt_version
    assert not tpkt_reserved, tpkt_reserved
    if len(tpkt_chunk) < tpkt_length:
        raise util.NeedMoreData(tpkt_length - len(tpkt_chunk))
    tpkt_body = tpkt_chunk[TPKT_HEADER.size:tpkt_length] # tpkt_length includes own header!
    #print 'TPKT header (', 'body length:', len(tpkt_body), ')' # , 'unused:', len(chunk) - tpkt_length
    return tpkt_body, tpkt_length

def encode(tpkt_body):
    tpkt_version = 3
    tpkt_reserved = 0
    tpkt_length = TPKT_HEADER.size + len(tpkt_body) # tpkt_length includes own header!
    tpkt_chunk = TPKT_HEADER.pack(tpkt_version, tpkt_reserved, tpkt_length) + tpkt_body
    return tpkt_chunk

def test():
    a = 'foo'
    print 'Encoding %r ...' % a
    tpkt_chunk = encode(a)
    print 'Decoding:'; util.dump(tpkt_chunk)
    tpkt_body, tpkt_length = parse(tpkt_chunk)
    print 'Decoded:'; util.dump(tpkt_body)
    assert tpkt_body == a, (tpkt_body, a)
    assert tpkt_length == len(tpkt_chunk), (tpkt_length, tpkt_chunk)

if __name__ == '__main__':
    util.hook()
    test()
