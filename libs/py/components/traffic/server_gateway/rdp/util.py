import struct
import sys

def dump(x, stream=None, decimal=False, prefix=''):
    """Dump bytes (string or int-list) as hexdump to stream or stdout"""
    if isinstance(x, str):
        l = [ord(c) for c in x]
    else:
        l = tuple(x) # something indexable, need not be modifiable

    for offset in range(0, len(l), 16):
        line_data = l[offset : offset + 16]
        (stream or sys.stdout).write('%s%04x %-47s %-16s %-8s%s\n' % (
            prefix,
            offset,
            ' '.join('%02x' % b for b in line_data),
            ''.join(chr(b) if 32 <= b < 127 else '.' for b in line_data),
            # attempt to print utf-16 strings will fail if not word-aligned ...
            ''.join(
                chr(line_data[i]) if 32 <= line_data[i] < 127 and not line_data[i + 1] else ' '
                for i in range(0, len(line_data) & 30, 2)),
            ' %s' % (', '.join(str(b) for b in line_data)) if decimal else '',
            ))

def undump(s):
    """Recover string from dump"""
    res = []
    for l in s.strip().splitlines():
        l = l.strip()
        if l:
            int(l[:4], 16) # will throw if not ok ;-)
            assert l[4] == ' ', l
            assert l[52] == ' ', l
            res.extend(int(s, 16) for s in l[5 : 52].split())
    return to_str(res)

def to_str(x):
    """Convert int-list to string"""
    if isinstance(x, str):
        return str(x) # pylint pleasing
    else:
        return ''.join(chr(b) for b in x)

def to_list(x):
    """Convert string to int-list"""
    if isinstance(x, str):
        return tuple(ord(c) for c in x)
    else:
        return x

def cdump(x, stream=None, decimal=False, prefix=''):
    """Dump bytes (string or int-list) as hexdump to stream or stdout"""
    if isinstance(x, str):
        l = [ord(c) for c in x]
    else:
        l = tuple(x) # something indexable, need not be modifiable

    for offset in range(0, len(l), 16):
        line_data = l[offset : offset + 16]
        (stream or sys.stdout).write('%s\n' % (
            ' '.join('0x%02x,' % b for b in line_data),
            ))

# decorator
def exceptor(func):
    """Decorator catching and logging or debugging exceptions"""
    def wrapper(inst, *args, **kwds):
        return func(inst, *args, **kwds)
    return wrapper

class BitField(object):

    class_value_names = None # overridden in classes when instantiated
    class_value_names_items = None

    def __init__(self, value=0, **flags):
        # Note: Flags are bit-masks, not bit-numbers

        # Initialize class once
        if self.__class__.class_value_names is None:
            self.__class__.class_value_names = dict((bit_val, name) for (name, bit_val) in self.__class__.__dict__.items() if name[0].isupper())
            self.__class__.class_value_names_items = sorted(self.__class__.class_value_names.items())

        # Decide value from explicit modified with flags
        for k, v in flags.items():
            assert k[0].isupper(), k # assert k in self.__class__.class_value_names.values()
            if v:
                value |= getattr(self, k)
            else:
                value &= ~getattr(self, k)
        self.value = value # store effective value before we start eating bits

        active_bits = [] # collect to __str__

        # Set all flag names as member vars
        for (bit_val, name) in self.__class__.class_value_names_items:
            t = bool(value & bit_val == bit_val)
            setattr(self, name, t)
            if t:
                value &= ~bit_val
                active_bits.append(name)

        # Find unnamed bits
        bit_number = 0
        while value:
            bit_val = 1 << bit_number
            if value & bit_val:
                value &= ~bit_val
                active_bits.append('BIT_%s_0x%x' % (bit_number, bit_val))
            bit_number += 1

        self.str = '|'.join(active_bits) or 'None'
        self.repr = '<%s 0x%x=%s>' % (self.__class__.__name__, self.value, self.str)

    def __str__(self):
        return self.str

    def __repr__(self):
        return self.repr


class EnumField(object):

    defs = {} # default
    class_value_names = None # overridden in classes when instantiated

    def __init__(self, value=None, **selected):

        # Initialize class once
        if self.__class__.class_value_names is None:
            self.__class__.class_value_names = dict((bit_val, name) for (name, bit_val) in self.__class__.__dict__.items() if name[0].isupper())

        # Decide the value
        if value is None:
            ((k, _v),) = selected.items()
            assert k[0].isupper(), k
            self.value = getattr(self, k)
        else:
            assert not selected, selected
            self.value = value

        # Set all flag names as member vars
        for (bit_val, name) in self.__class__.class_value_names.items():
            setattr(self, name, self.value == bit_val)

        # Compute str
        if self.value in self.defs:
            self.str = self.defs[self.value]
            self.repr = '<%s 0x%x=%r>' % (self.__class__.__name__, self.value, self.str)
        elif self.value in self.__class__.class_value_names:
            self.str = self.__class__.class_value_names[self.value]
            self.repr = '<%s 0x%x=%s>' % (self.__class__.__name__, self.value, self.str)
        else:
            self.str = hex(self.value)
            self.repr = '<%s %s>' % (self.__class__.__name__, self.str)

    def __str__(self):
        return self.str

    def __repr__(self):
        return self.repr


class ObjectDict(object):
    """
    Allows record-like dot-access to a dict
    """
    def __init__(self, val=None):
        self.__dict__ = dict(val or [])

    __getitem__ = object.__getattribute__


class NeedMoreData(Exception): pass

def ensure_enough_data(s, length):
    """
    Raise NeedMoreData if not enough data available
    """
    if len(s) < length:
        raise NeedMoreData(length - len(s))

class PackedStruct(object):
    """
    OO wrapper for struct handling
    <: MS order
    B: 8-bit unsigned integer
    H: 16-bit unsigned integer
    I: 32-bit unsigned integer
    """
    def __init__(self, formatstring):
        self.formatstring = formatstring
        self.size = struct.calcsize(formatstring)

    def unpack(self, buf, offset=0):
        #assert len(buf) >= self.size + offset, (len(buf), self.size + offset)
        return struct.unpack_from(self.formatstring, buf, offset) + (buf[self.size + offset:],)

    def pack(self, *args):
        return struct.pack(self.formatstring, *args) # TODO: use pack_into?


def tracer(enabled=True, prefix='TRACE: '):
    """Decorator "logging" all invocations"""
    def wrapper(org_f):
        if not enabled:
            return org_f
        def logger(*args, **argkw):
            args_ = list(repr(x) for x in args)
            argkw_ = list('%s=%r' % (k, v) for (k, v) in argkw.items())
            res = org_f(*args, **argkw)
            print '%s%s(%s) => %r' % (prefix, org_f.__name__, ', '.join(args_ + argkw_), res)
            return res
        return logger
    return wrapper


def test_dump():
    print 'Test dump / undump'
    dump(to_str(undump(
        """
0000 30 82 03 4f a0 03 02 01 02 02 05 01 00 00 00 45 0..O...........E
0010 30 09 06 05 2b 0e 03 02 1d 05 00 30 42 31 40 30 0...+......0B1@0
0020 0f 06 03 55 04 03 1e 08 00 50 00 41 00 49 00 4e ...U.....P.A.I.N
0030 30 2d 06 03 55 04 07 1e 26 00 53 00 69 00 74 00 0-..U...&.S.i.t.     &Sit
03a0 22 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 "............... "
03b0 00                                              .
""")), decimal=True)
    print

def test_bitfield():
    class TestBitField(BitField):
        SEC_EXCHANGE_PKT = 0x0001 # Indicates that the packet is a Security Exchange PDU (section 2.2.1.10). This packet type is sent from client to server only. The client only sends this packet if it will be encrypting further communication and Standard RDP Security mechanisms (see section 5.3) are in effect.
        SEC_ENCRYPT = 0x0008 # Indicates that encryption is being used for the packet.

    print '.SEC_EXCHANGE_PKT', TestBitField.SEC_EXCHANGE_PKT
    print 'BitField from value'
    test_field = TestBitField(65537)
    print test_field
    print 'test_field.SEC_EXCHANGE_PKT', test_field.SEC_EXCHANGE_PKT
    print 'test_field.SEC_ENCRYPT', test_field.SEC_ENCRYPT
    print 'BitField from flag names'
    print 'SEC_ENCRYPT and SEC_EXCHANGE_PKT flags:', TestBitField(SEC_ENCRYPT=True, SEC_EXCHANGE_PKT=117)
    print 'repr SEC_ENCRYPT and SEC_EXCHANGE_PKT flags:', repr(TestBitField(SEC_ENCRYPT=True, SEC_EXCHANGE_PKT=7))
    print '0x1001 but no SEC_EXCHANGE_PKT but SEC_ENCRYPT flag:', TestBitField(0x10001, SEC_ENCRYPT='x', SEC_EXCHANGE_PKT='')
    print

def test_enumfield():
    class TestEnumField(EnumField):
        defs = {0: 'The protocol formerly known as PROTOCOL_RDP', 1: 'Ssssh!', 2: 'Hybris!'}
        PROTOCOL_RDP = 0
        PROTOCOL_SSL = 1
        PROTOCOL_HYBRID = 2
        CS_CORE = 0xC001 # The data block that follows contains Client Core Data (section 2.2.1.3.2).
        CS_SECURITY = 0xC002 # The data block that follows contains Client Security Data (section 2.2.1.3.3).
        CS_NET = 0xC003 # The data block that follows contains Client Network Data (section 2.2.1.3.4).
        CS_CLUSTER = 0xC004 # The data block that follows contains Client Cluster Data (section 2.2.1.3.5).

    print 'EnumField from value'
    test_field = TestEnumField(TestEnumField.CS_SECURITY)
    print test_field
    print 'test_field.CS_SECURITY', test_field.CS_SECURITY
    print 'test_field.CS_CLUSTER', test_field.CS_CLUSTER
    print 'BitField from enum names'
    print 'CS_CORE chosen:', TestEnumField(CS_CORE=7)
    print 'repr CS_CORE chosen:', repr(TestEnumField(CS_CORE=True))
    print 'value 7:', TestEnumField(7)
    print 'PROTOCOL_SSL chosen:', TestEnumField(PROTOCOL_SSL=True)
    print

    class keyboardType(EnumField):
        defs = {
            1: 'IBM PC/XT or compatible (83-key) keyboard',
            2: 'Olivetti "ICO" (102-key) keyboard',
            3: 'IBM PC/AT (84-key) and similar keyboards',
            4: 'IBM enhanced (101- or 102-key) keyboard',
            5: 'Nokia 1050 and similar keyboards',
            6: 'Nokia 9140 and similar keyboards',
            7: 'Japanese keyboard',
            }
    print 'repr PC/XT:', repr(keyboardType(1))
    print 'Olivetti:', keyboardType(2)
    print '117:', keyboardType(117)
    print

def test_tracer():
    @tracer(prefix='kurt: ')
    def seven(a=7, b=7):
        return [a] * b

    seven()
    seven(2)
    seven(a=2)
    seven(2, 3)
    seven(b=4)

    @tracer(False, 'disabled: ')
    def umpteen(x):
        return x
    umpteen(2)

    @tracer(True, 'enabled: ')
    def umptween(x):
        return x
    umptween(19)

if __name__ == '__main__':
    test_dump()
    test_bitfield()
    test_enumfield()
    test_tracer()
