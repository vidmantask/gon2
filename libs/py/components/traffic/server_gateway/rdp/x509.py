import util
if __name__ == '__main__':
    util.hook()

from asn1 import *
from oids import OIDS
import pkcs1


SomeKindOfString = CHOICE(# we introduce this hack choice ...
    Field('bmpString', BMPString()), # this is what MS certificates usually uses
    Field('printableString', PrintableString()), # this is what is used in an NSS certificate example ...
    Field('unicodeString', UnicodeString()), # this is used by PKCS#11 / IronKey
    Field('t61String', T61String()), # this is used by www.google.com
    )

# http://www.oid-info.com/get/2.5.4.6
# Description: Country name
OIDS.declare('countryName',
    '{joint-iso-itu-t(2) ds(5) attributeType(4) countryName(6)}',
    selects=SomeKindOfString())

# http://www.oid-info.com/get/2.5.4.8
# Description: State or Province name
OIDS.declare('stateOrProvinceName',
    '{joint-iso-itu-t(2) ds(5) attributeType(4) stateOrProvinceName(8)}',
    selects=SomeKindOfString())

# http://www.oid-info.com/get/2.5.4.10
# Description: Organization name
OIDS.declare('organizationName',
    '{joint-iso-itu-t(2) ds(5) attributeType(4) organizationName(10)}',
    selects=SomeKindOfString())

# http://www.oid-info.com/get/2.5.4.11
# Description: Organization unit name
OIDS.declare('organizationUnitName',
    '{joint-iso-itu-t(2) ds(5) attributeType(4) organizationUnitName(11)}',
    selects=SomeKindOfString())

# http://www.oid-info.com/get/2.16.840.1.113730.1.1
# Description: Netscape certificate type
# An X.509 v3 certificate extension used to identify whether
# the certificate subject is an SSL client, an SSL server, or
# a CA.
OIDS.declare('cert-type',
    '{joint-iso-itu-t(2) country(16) us(840) organization(1) netscape(113730) 1(1) cert-type(1)}', # a strange anonymous 1 ...
    selects=NULL())

# http://www.oid-info.com/get/0.9.2342.19200300.100.1.25
# Description: domainComponent ATTRIBUTE
# http://rfc.dotsrc.org/rfc/rfc2247.html
#    ( 0.9.2342.19200300.100.1.25 NAME 'dc' EQUALITY caseIgnoreIA5Match
#     SUBSTR caseIgnoreIA5SubstringsMatch
#     SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 SINGLE-VALUE )
OIDS.declare('domainComponent',
    '{itu-t(0) data(9) pss(2342) ucl(19200300) pilot(100) pilotAttributeType(1) domainComponent(25)}',
    selects=IA5String()
    )

# http://www.oid-info.com/get/2.5.4.3
# Description: Common name
# http://www.alvestrand.no/objectid/2.5.4.3.html
# commonName ATTRIBUTE ::= {
#    SUBTYPE OF name
#    WITH SYNTAX DirectoryString {ub-common-name}
#    ID {id-at-commonName}
#}
OIDS.declare('commonName',
    '{joint-iso-itu-t(2) ds(5) attributeType(4) commonName(3)}',
    selects=SomeKindOfString(),
    )

# http://www.oid-info.com/get/2.5.4.5
# Description: Serial number attribute type
oids.OIDS.declare('serialNumber',
    '{joint-iso-itu-t(2) ds(5) attributeType(4) serialNumber(5)}',
    selects=BMPString())

# http://www.oid-info.com/get/2.5.4.7
# Description: Locality Name
OIDS.declare('localityName',
    '{joint-iso-itu-t(2) ds(5) attributeType(4) localityName(7)}',
    selects=SomeKindOfString())


# http://www.faqs.org/rfcs/rfc3280.html


# A.1 Explicitly Tagged Module, 1988 Syntax

CertificateSerialNumber = INTEGER()

KeyIdentifier = OCTET_STRING()

CertPolicyId = OBJECT_IDENTIFIER()

AttributeType = OBJECT_IDENTIFIER()

AttributeTypeAndValue = SEQUENCE(
    Field('type', AttributeType()),
    Field('value', ANY_DEFINED_BY('type')), # AttributeValue referenced a field here from somewhere else!
    )

Attribute = SEQUENCE(
    Field('type', AttributeType()),
    Field('values', ANY_DEFINED_BY('type')), # at least one value is required - and AttributeValue referenced a field here from somewhere else!
    )

RelativeDistinguishedName = SET_OF(
    AttributeTypeAndValue()
    )

RDNSequence = SEQUENCE_OF(
    RelativeDistinguishedName()
    )

Name = CHOICE(
     Field('rdnSequence', RDNSequence())
     )

ORAddress = SEQUENCE(
   #Field('built-in-standard-attributes', BuiltInStandardAttributes),
   #Field('built-in-domain-defined-attributes', BuiltInDomainDefinedAttributes, optional=True),
   # see also teletex-domain-defined-attributes
   #Field('extension-attributes', ExtensionAttributes, optional=True),
   )

DirectoryString = CHOICE(
    Field('teletexString',           TeletexString(min_size=1)),
    Field('printableString',         PrintableString(min_size=1)),
    Field('universalString',         UniversalString(min_size=1)),
    Field('utf8String',              UTF8String(min_size=1)),
    Field('bmpString',               BMPString(min_size=1)),
    )

EDIPartyName = SEQUENCE(
    Field('nameAssigner', DirectoryString(), optional=True), # [0]
    Field('partyName', DirectoryString()), # [1]
    )

OtherName = SEQUENCE(
    Field('type-id', OBJECT_IDENTIFIER()),
    Field('value', ANY_DEFINED_BY('type-id'), explicit=CONTEXT(0)),
    )

GeneralName = CHOICE(
    Field('otherName', OtherName()), # [0]
    Field('rfc822Name', IA5String()), # [1]
    Field('dNSName', IA5String()), # [2]
    Field('x400Address', ORAddress()), # [3]
    Field('directoryName', Name()), # [4]
    Field('ediPartyName', EDIPartyName()), # [5]
    Field('uniformResourceIdentifier', IA5String()), # [6]
    Field('iPAddress', OCTET_STRING()), # [7]
    Field('registeredID', OBJECT_IDENTIFIER()), # [8]
    )

GeneralNames = SEQUENCE_OF(GeneralName(), min_size=1)

SubjectAltName = GeneralNames()


# 4.2.1  Standard Extensions

# http://www.oid-info.com/get/2.5.29
# Description: id-ce
# Automatically extracted from RFC3281
OIDS.declare('id-ce',
    '{joint-iso-itu-t(2) ds(5) ce(29)}')
#id-ce   OBJECT IDENTIFIER ::=  { joint-iso-ccitt(2) ds(5) 29 }
# joint-iso-itu-t = joint-iso-ccitt!


# 4.2.1.10  Basic Constraints

OIDS.declare('id-ce-basicConstraints', 'id-ce', 19)
# id-ce-basicConstraints OBJECT IDENTIFIER ::=  { id-ce 19 }

BasicConstraints = SEQUENCE(
    Field('cA', BOOLEAN()), #  DEFAULT FALSE
    Field('pathLenConstraint', INTEGER(), optional=True),
    )

# 4.2.1.9  Subject Directory Attributes

OIDS.declare('id-ce-subjectDirectoryAttributes', 'id-ce', 9)
# id-ce-subjectDirectoryAttributes OBJECT IDENTIFIER ::=  { id-ce 9 }

SubjectDirectoryAttributes = SEQUENCE_OF(Attribute, min_size=1)


# 4.2.1.8  Issuer Alternative Names

OIDS.declare('id-ce-issuerAltName', 'id-ce', 18)
# id-ce-issuerAltName OBJECT IDENTIFIER ::=  { id-ce 18 }

IssuerAltName = GeneralNames()


# 4.2.1.7  Subject Alternative Name

OIDS.declare('id-ce-subjectAltName', 'id-ce', 17)
# id-ce-subjectAltName OBJECT IDENTIFIER ::=  { id-ce 17 }


# 4.2.1.6  Policy Mappings

OIDS.declare('id-ce-policyMappings', 'id-ce', 33)
# id-ce-policyMappings OBJECT IDENTIFIER ::=  { id-ce 33 }

PolicyMappings = SEQUENCE_OF(
    SEQUENCE(
        Field('issuerDomainPolicy', CertPolicyId()),
        Field('subjectDomainPolicy', CertPolicyId()),
        ),
     min_size=1,
     )


# 4.2.2  Private Internet Extensions

OIDS.declare('id-pkix',
    '{ iso(1) identified-organization(3) dod(6) internet(1) security(5) mechanisms(5) pkix(7) }')

OIDS.declare('id-pe', 'id-pkix', 1)
#      id-pe  OBJECT IDENTIFIER  ::=  { id-pkix 1 }


# 4.2.1.5  Certificate Policies


DisplayText = CHOICE(
    Field('ia5String', IA5String(min_size=1)), # (SIZE (1..200)),
    Field('visibleString', VisibleString(min_size=1)), # (SIZE (1..200)),
    Field('bmpString', BMPString(min_size=1)), # (SIZE (1..200)),
    Field('utf8String', UTF8String(min_size=1)), # (SIZE (1..200)),
    )

NoticeReference = SEQUENCE(
    Field('organization', DisplayText()),
    Field('noticeNumbers', SEQUENCE_OF(INTEGER())),
    )

UserNotice = SEQUENCE(
    Field('noticeRef', NoticeReference(), optional=True),
    Field('explicitText', DisplayText(), optional=True),
    )

CPSuri = IA5String()

Qualifier = CHOICE(
    Field('cPSuri', CPSuri()),
    Field('userNotice', UserNotice()),
    )

# policyQualifierIds for Internet policy qualifiers

OIDS.declare('id-qt', 'id-pkix', 2)
# id-qt          OBJECT IDENTIFIER ::=  { id-pkix 2 }
OIDS.declare('id-qt-cps', 'id-qt', 1)
# id-qt-cps      OBJECT IDENTIFIER ::=  { id-qt 1 }
OIDS.declare('id-qt-unotice', 'id-qt', 2)
# id-qt-unotice  OBJECT IDENTIFIER ::=  { id-qt 2 }

PolicyQualifierId = OBJECT_IDENTIFIER() # ( id-qt-cps | id-qt-unotice ) # FIXME

OIDS.declare('id-ce-certificatePolicies', 'id-ce', 32)
# id-ce-certificatePolicies OBJECT IDENTIFIER ::=  { id-ce 32 }

OIDS.declare('anyPolicy', 'id-ce-certificatePolicies', 0)
# anyPolicy OBJECT IDENTIFIER ::= { id-ce-certificate-policies 0 } # casing bug!

PolicyQualifierInfo = SEQUENCE(
    Field('policyQualifierId', PolicyQualifierId()),
    Field('qualifier', ANY_DEFINED_BY('policyQualifierId')),
    )

CertPolicyId = OBJECT_IDENTIFIER()

PolicyInformation = SEQUENCE(
    Field('policyIdentifier', CertPolicyId()),
    Field('policyQualifiers', SEQUENCE_OF(PolicyQualifierInfo(), min_size=1), optional=True),
    )

certificatePolicies = SEQUENCE_OF(
    PolicyInformation(),
    min_size=1,
    )


# 4.2.1.4  Private Key Usage Period

OIDS.declare('id-ce-privateKeyUsagePeriod', 'id-ce', 16)
# id-ce-privateKeyUsagePeriod OBJECT IDENTIFIER ::=  { id-ce 16 }

PrivateKeyUsagePeriod = SEQUENCE(
    Field('notBefore', GeneralizedTime(), optional=True), # [0]
    Field('notAfter', GeneralizedTime(), optional=True), # [1]
    )


# 4.2.1.3  Key Usage

OIDS.declare('id-ce-keyUsage', 'id-ce', 15)
# id-ce-keyUsage OBJECT IDENTIFIER ::=  { id-ce 15 }

KeyUsage = BIT_STRING(
    dict(
        digitalSignature        =0,
        nonRepudiation          =1,
        keyEncipherment         =2,
        dataEncipherment        =3,
        keyAgreement            =4,
        keyCertSign             =5,
        cRLSign                 =6,
        encipherOnly            =7,
        decipherOnly            =8,
        ),
    )


# 4.2.1.2  Subject Key Identifier

OIDS.declare('id-ce-subjectKeyIdentifier', 'id-ce', 14)
# id-ce-subjectKeyIdentifier OBJECT IDENTIFIER ::=  { id-ce 14 }

SubjectKeyIdentifier = KeyIdentifier()


# 4.2.1.1  Authority Key Identifier

OIDS.declare('id-ce-authorityKeyIdentifier', 'id-ce', 35)
#id-ce-authorityKeyIdentifier OBJECT IDENTIFIER ::=  { id-ce 35 }

KeyIdentifier = OCTET_STRING()

AuthorityKeyIdentifier = SEQUENCE(
    Field('keyIdentifier', KeyIdentifier(), optional=True), # [0]
    Field('authorityCertIssuer', GeneralNames(), optional=True), # [1]
    Field('authorityCertSerialNumber', CertificateSerialNumber(), optional=True), # [2]
    )


# 4.1.1.2  signatureAlgorithm

AlgorithmIdentifier = SEQUENCE(
    Field('algorithm',               OBJECT_IDENTIFIER()),
    Field('parameters',              ANY_DEFINED_BY('algorithm'), optional=True),
    )

# 4.1  Basic Certificate Fields

Extension = SEQUENCE(
    Field('extnID',      OBJECT_IDENTIFIER()),
    Field('critical',    BOOLEAN(), default=False),
    Field('extnValue',   OCTET_STRING()), # Content type defined elsewhere
    )

Extensions = SEQUENCE_OF(
    Extension(),
    min_size=1,
    )

SubjectPublicKeyInfo = SEQUENCE(
    Field('algorithm',            AlgorithmIdentifier()),
    Field('subjectPublicKey',     BIT_STRING()),
    )

# This value is inside the BIT_STRING (which has a leading 0 more than OCTET_STRING!!!)
SubjectPublicKeyInfo_subjectPublicKey = Field(None, SEQUENCE(
    Field('mod', INTEGER()),
    Field('exp', INTEGER()),
    ))

UniqueIdentifier = BIT_STRING(octets=True)

Time = CHOICE(
    Field('utcTime',        UTCTime()),
    Field('generalTime',    GeneralizedTime()),
    )

Validity = SEQUENCE(
    Field('notBefore',      Time()),
    Field('notAfter',       Time()),
    )

CertificateSerialNumber = INTEGER()

Version = INTEGER(dict(v1=0, v2=1, v3=2))

TBSCertificate = SEQUENCE(
    Field('version',              Version, explicit=CONTEXT(0), default='v1'),
    Field('serialNumber',         CertificateSerialNumber()),
    Field('signature',            AlgorithmIdentifier()),
    Field('issuer',               Name()),
    Field('validity',             Validity()),
    Field('subject',              Name()),
    Field('subjectPublicKeyInfo', SubjectPublicKeyInfo()),
    Field('issuerUniqueID',       UniqueIdentifier(), CONTEXT(1), optional=True), # [1]  IMPLICIT
    # If present, version MUST be v2 or v3
    Field('subjectUniqueID',      UniqueIdentifier(), CONTEXT(2), optional=True), # [2]  IMPLICIT
    # If present, version MUST be v2 or v3
    Field('extensions',           Extensions(), explicit=CONTEXT(3), optional=True), # If present, version MUST be v3
    )

TaggedTBSCertificate = Field(None, TBSCertificate()) # this is what is hashed

Certificate = SEQUENCE(
    Field('tbsCertificate', TBSCertificate()),
    Field('signatureAlgorithm', AlgorithmIdentifier()),
    Field('signatureValue', BIT_STRING(octets=True)),
    )

TaggedCertificate = Field(None, Certificate()) # this is what is encoded in cert


def read_certificate(cert_string):
    """
    Read Certificate from standard RFC1113 textual encoding
    DER is the raw binary format, this is PEM, a base-64 encoded DER format with header and footer.
    """
    cert_longstring = ''.join(l.strip() for l in cert_string.splitlines())
    _begin, cert_body_extra = cert_longstring.split('-----BEGIN CERTIFICATE-----', 1)
    cert_body, _end = cert_body_extra.split('-----END CERTIFICATE-----', 1)
    return cert_body.decode('base64')

def write_certificate(cert_string):
    """
    Write Certificate in standard RFC1113 textual encoding
    """
    return '-----BEGIN CERTIFICATE-----\n' + cert_string.encode('base64') + '-----END CERTIFICATE-----\n'

def verify(ca_octets, cert_octets, logger=None, cert_logger=None):
    """
    Verify that cert in cert_octets has been correctly signed by ca_octets
    Note: The check is not complete!
    """
    if not cert_logger:
        cert_logger = logger

    ca_cert = TaggedCertificate.ber_decode(ca_octets)
    ca_public_key_octets = ca_cert['tbsCertificate']['subjectPublicKeyInfo']['subjectPublicKey'][1]
    ca_public_key_struct = SubjectPublicKeyInfo_subjectPublicKey.ber_decode(ca_public_key_octets)
    ca_pub_key = pkcs1.RsaLong(ca_public_key_struct['mod'], ca_public_key_struct['exp'])

    check_cert = TaggedCertificate.ber_decode(cert_octets)
    if cert_logger: cert_logger('check_cert:', pprint=check_cert)
    assert check_cert['tbsCertificate']['issuer'] == ca_cert['tbsCertificate']['subject'], (check_cert, ca_cert)

    tbsCertificate_der = TaggedTBSCertificate.der_encode(check_cert['tbsCertificate'])
    if logger: logger('tbsCertificate_der:', dump=tbsCertificate_der)

    if logger: logger("algorithm: %s", check_cert['signatureAlgorithm']['algorithm']) # sha-1WithRSAEncryption?
    signatureValue = check_cert['signatureValue'][1]
    if logger: logger('signatureValue:', dump=signatureValue)

    return pkcs1.verify(ca_pub_key, tbsCertificate_der, signatureValue, logger=logger)

def patch_sign(template_cert_octets, new_pub_key, ca_key, logger=None):
    """
    Patch cert in template_cert_octets with pub key from new_pub_key and re-sign with ca_key
    """
    new_public_key_struct = {
        'exp': new_pub_key.public_exponent,
        'mod': new_pub_key.modulus,
        }
    new_public_key_octets = SubjectPublicKeyInfo_subjectPublicKey.der_encode(new_public_key_struct)

    new_cert_struct = TaggedCertificate.ber_decode(template_cert_octets)
    if logger: logger('patching new_cert_struct:', pprint=new_cert_struct)
    new_cert_struct['tbsCertificate']['subjectPublicKeyInfo']['subjectPublicKey'] = ('octets', new_public_key_octets)

    tbsCertificate_der = TaggedTBSCertificate.der_encode(new_cert_struct['tbsCertificate'])
    signatureValue = pkcs1.sign(ca_key, tbsCertificate_der, logger=logger)
    new_cert_struct['signatureValue'] = ('octets', signatureValue)

    new_server_cert_octets = TaggedCertificate.der_encode(new_cert_struct)

    return new_server_cert_octets


def test_logger(f, *params, **kws):
    dump = kws.pop('dump', None)
    pprint_struct = kws.pop('pprint', None)
    assert not kws, kws
    import pprint
    print f % params
    if dump:
        util.dump(dump)
    if pprint_struct:
        pprint.pprint(pprint_struct)

def test_TaggedCertificate_pain():

    paincert = '0\x82\x01\xde0\x82\x01\x8c\xa0\x03\x02\x01\x02\x02\x08\x01\x9e\xb1\x9e\xfdW\xf2@0\t\x06\x05+\x0e\x03\x02\x1d\x05\x000B1@0\x0f\x06\x03U\x04\x03\x1e\x08\x00P\x00A\x00I\x00N0-\x06\x03U\x04\x07\x1e&\x00S\x00i\x00t\x00e\x00 \x00L\x00i\x00c\x00e\x00n\x00s\x00e\x00 \x00S\x00e\x00r\x00v\x00e\x00r0\x1e\x17\r701122110757Z\x17\r491122110757Z0B1@0\x0f\x06\x03U\x04\x03\x1e\x08\x00P\x00A\x00I\x00N0-\x06\x03U\x04\x07\x1e&\x00S\x00i\x00t\x00e\x00 \x00L\x00i\x00c\x00e\x00n\x00s\x00e\x00 \x00S\x00e\x00r\x00v\x00e\x00r0\\0\r\x06\t*\x86H\x86\xf7\r\x01\x01\x01\x05\x00\x03K\x000H\x02A\x00\xd21[2\xb9\xe2\x91\xa0\xdc\x87\x92\x1f\rT\xecU\xf6\xc3\xa0Sn\xee9D\xb69:;f\xdd\xe3\xa6\x88x\xc0\xe647qiU\xae\x8eQ|\xa4\x1d_\x1b\xf7\xae\xcc\xa4\xef\xa0^\x17\x8a:\xcb8X\xec5\x02\x03\x01\x00\x01\xa3j0h0\x0f\x06\x03U\x1d\x13\x04\x080\x06\x01\x01\xff\x02\x01\x000U\x06\t+\x06\x01\x04\x01\x827\x12\x08\x04HY\x00T\x00R\x002\x00C\x00J\x00V\x003\x009\x00K\x006\x00M\x00Y\x00G\x008\x00Y\x00Y\x00F\x00J\x00D\x00F\x00F\x00K\x00X\x00H\x006\x00Q\x00F\x00Q\x00P\x00J\x00H\x00W\x00D\x00F\x00\x00\x000\t\x06\x05+\x0e\x03\x02\x1d\x05\x00\x03A\x00*\x1f\x19Mw\xe6o\xc6{\xa7J\xd6\xf9@\x02\x8d?\x14U\x84\xa9d\xf8\x809T2\xb0\xc8\x08T*5\xb6\x95=\xc9\x9f\xe6c\xca\\\xf1X\xbf_\x991\xb3?\xaf\xf0\xe1\x92g\xa2\xdb2\xf6\xc0\x8d\xbb\xe5\t\xa5\x03\x00\x00'
    paincert = paincert[:-4] # my rdp misunderstanding???
    import rdp_asn1 # get MS oids
    print 'pain cert SEQUENCE'
    test_ber_roundtrip('Tag SEQUENCE paincert', Field(None, SEQUENCE()), paincert, oneway=True)
    #file('c','w').write(paincert)

    print 'pain cert Certificate'
    test_ber_roundtrip('Tag Certificate paincert', TaggedCertificate, paincert, oneway=True)


def test_SubjectPublicKeyInfo_subjectPublicKey():
    print 'public_key_struct:'
    # _content_ of BIT_STRING (without leading 0)
    public_key_octets = util.undump("""
0000 30 48 02 41 00 c3 83 df 44 5c 5d 4d a7 d3 6a be 0H.A....D\]M..j.
0010 f1 08 76 6a c3 fc 62 c3 cd cf f8 6f 7a b6 b8 a2 ..vj..b....oz...
0020 20 2d f0 03 6d d2 ba 39 21 1c ee fd 61 49 d1 61  -..m..9!...aI.a
0030 75 e8 e0 a1 05 0e 32 2c 9e 71 7c da 52 20 79 b2 u.....2,.q|.R y.
0040 e9 a7 cc fb 15 02 03 01 00 01                   ..........
    """)
    # FIXME: not DER - is that OK?
    test_ber_roundtrip('SubjectPublicKeyInfo_subjectPublicKey public_key_octets', SubjectPublicKeyInfo_subjectPublicKey, public_key_octets)


def test_pain_cert_chain():
    """Some certs from rdesktop to x-men"""
    # certutil:
    # http://blogs.sun.com/baban/entry/steps_to_setup_ssl_using NSS and OpenSSL side-by-side, detailed howto
    # http://docs.sun.com/app/docs/doc/819-3671/ablrf?a=view Using Network Security Services (NSS) Tools, good examples
    # http://groups.google.com/group/mozilla.dev.tech.crypto/msg/232392a34211cd77 good Q&A
    import rdp_asn1 # oid szOID_PKIS_TLSERVER_SPK_OID
    pain_ca = util.undump('''
0000 30 82 01 de 30 82 01 8c a0 03 02 01 02 02 08 01 0...0...........
0010 9e b1 9e fd 57 f2 40 30 09 06 05 2b 0e 03 02 1d ....W.@0...+....
0020 05 00 30 42 31 40 30 0f 06 03 55 04 03 1e 08 00 ..0B1@0...U.....
0030 50 00 41 00 49 00 4e 30 2d 06 03 55 04 07 1e 26 P.A.I.N0-..U...& PAI
0040 00 53 00 69 00 74 00 65 00 20 00 4c 00 69 00 63 .S.i.t.e. .L.i.c
0050 00 65 00 6e 00 73 00 65 00 20 00 53 00 65 00 72 .e.n.s.e. .S.e.r
0060 00 76 00 65 00 72 30 1e 17 0d 37 30 31 31 32 32 .v.e.r0...701122
0070 31 31 30 37 35 37 5a 17 0d 34 39 31 31 32 32 31 110757Z..4911221
0080 31 30 37 35 37 5a 30 42 31 40 30 0f 06 03 55 04 10757Z0B1@0...U.
0090 03 1e 08 00 50 00 41 00 49 00 4e 30 2d 06 03 55 ....P.A.I.N0-..U   PAI
00a0 04 07 1e 26 00 53 00 69 00 74 00 65 00 20 00 4c ...&.S.i.t.e. .L
00b0 00 69 00 63 00 65 00 6e 00 73 00 65 00 20 00 53 .i.c.e.n.s.e. .S
00c0 00 65 00 72 00 76 00 65 00 72 30 5c 30 0d 06 09 .e.r.v.e.r0\0...
00d0 2a 86 48 86 f7 0d 01 01 01 05 00 03 4b 00 30 48 *.H.........K.0H       K
00e0 02 41 00 d2 31 5b 32 b9 e2 91 a0 dc 87 92 1f 0d .A..1[2.........
00f0 54 ec 55 f6 c3 a0 53 6e ee 39 44 b6 39 3a 3b 66 T.U...Sn.9D.9:;f
0100 dd e3 a6 88 78 c0 e6 34 37 71 69 55 ae 8e 51 7c ....x..47qiU..Q|
0110 a4 1d 5f 1b f7 ae cc a4 ef a0 5e 17 8a 3a cb 38 .._.......^..:.8
0120 58 ec 35 02 03 01 00 01 a3 6a 30 68 30 0f 06 03 X.5......j0h0...
0130 55 1d 13 04 08 30 06 01 01 ff 02 01 00 30 55 06 U....0.......0U.
0140 09 2b 06 01 04 01 82 37 12 08 04 48 59 00 54 00 .+.....7...HY.T.       YT
0150 52 00 32 00 43 00 4a 00 56 00 33 00 39 00 4b 00 R.2.C.J.V.3.9.K. R2CJV39K
0160 36 00 4d 00 59 00 47 00 38 00 59 00 59 00 46 00 6.M.Y.G.8.Y.Y.F. 6MYG8YYF
0170 4a 00 44 00 46 00 46 00 4b 00 58 00 48 00 36 00 J.D.F.F.K.X.H.6. JDFFKXH6
0180 51 00 46 00 51 00 50 00 4a 00 48 00 57 00 44 00 Q.F.Q.P.J.H.W.D. QFQPJHWD
0190 46 00 00 00 30 09 06 05 2b 0e 03 02 1d 05 00 03 F...0...+....... F
01a0 41 00 2a 1f 19 4d 77 e6 6f c6 7b a7 4a d6 f9 40 A.*..Mw.o.{.J..@ A
01b0 02 8d 3f 14 55 84 a9 64 f8 80 39 54 32 b0 c8 08 ..?.U..d..9T2...
01c0 54 2a 35 b6 95 3d c9 9f e6 63 ca 5c f1 58 bf 5f T*5..=...c.\.X._
01d0 99 31 b3 3f af f0 e1 92 67 a2 db 32 f6 c0 8d bb .1.?....g..2....
01e0 e5 09                                           ..
        ''')
    # subject commonName=u'PAIN', localityName=u'Site License Server'
    # Non-critical extension id-ce-basicConstraints(joint-iso-itu-t(2).ds(5).ce(29).id-ce-basicConstraints(19))
    # Non-ciritcal extension szOID_PKIS_TLSERVER_SPK_OID(iso(1).identified-organization(3).dod(6).internet(1).private(4).enterprise(1).microsoft(311).MS-crypto-oids(18).szOID_PKIS_TLSERVER_SPK_OID(8))
    # subjectPublicKeyInfo algorithm rsaEncryption(iso(1).member-body(2).us(840).rsadsi(113549).pkcs(1).pkcs-1(1).rsaEncryption(1))
    # SEC_OID_PKCS1_RSA_ENCRYPTION
    test_ber_roundtrip('pain_ca x509 Certificate',
        TaggedCertificate, pain_ca)
    if False:
        file('pain_ca', 'w').write(pain_ca)
        import os
        os.system('certutil -d pki -A -n pain_ca -i pain_ca -t CTu,Cu,Cu')

    if verify(pain_ca, pain_ca, logger=test_logger):
        print 'Verified OK!'

    print 'Replace server pub key with MITM ca key and (self)resign'
    import rdp_rsa
    new_ca_key = rdp_rsa.test_key_one_512
    new_ca_cert_octets = patch_sign(pain_ca, new_ca_key, new_ca_key, logger=test_logger)

    print 'Verifying new ca cert correctly self-signed'
    if verify(new_ca_cert_octets, new_ca_cert_octets, logger=test_logger):
        print 'Verified OK!'

    pain_host = util.undump('''
0000 30 82 03 a1 30 82 03 4f a0 03 02 01 02 02 05 01 0...0..O........
0010 00 00 00 45 30 09 06 05 2b 0e 03 02 1d 05 00 30 ...E0...+......0
0020 42 31 40 30 0f 06 03 55 04 03 1e 08 00 50 00 41 B1@0...U.....P.A
0030 00 49 00 4e 30 2d 06 03 55 04 07 1e 26 00 53 00 .I.N0-..U...&.S.       &S
0040 69 00 74 00 65 00 20 00 4c 00 69 00 63 00 65 00 i.t.e. .L.i.c.e. ite Lice
0050 6e 00 73 00 65 00 20 00 53 00 65 00 72 00 76 00 n.s.e. .S.e.r.v. nse Serv
0060 65 00 72 30 1e 17 0d 37 39 31 32 33 31 32 33 30 e.r0...791231230 e
0070 30 30 30 5a 17 0d 33 38 30 31 31 39 30 33 31 34 000Z..3801190314
0080 30 37 5a 30 81 a4 31 81 a1 30 27 06 03 55 04 03 07Z0..1..0'..U..
0090 1e 20 00 6e 00 63 00 61 00 63 00 6e 00 5f 00 6e . .n.c.a.c.n._.n
00a0 00 70 00 3a 00 31 00 39 00 32 00 2e 00 31 00 36 .p.:.1.9.2...1.6
00b0 00 38 30 31 06 03 55 04 07 1e 2a 00 6e 00 63 00 .801..U...*.n.c.      *nc
00c0 61 00 63 00 6e 00 5f 00 6e 00 70 00 3a 00 31 00 a.c.n._.n.p.:.1. acn_np:1
00d0 39 00 32 00 2e 00 31 00 36 00 38 00 2e 00 34 00 9.2...1.6.8...4. 92.168.4
00e0 35 00 2e 00 36 30 43 06 03 55 04 05 1e 3c 00 31 5...60C..U...<.1 5.
00f0 00 42 00 63 00 4b 00 65 00 57 00 35 00 37 00 58 .B.c.K.e.W.5.7.X
0100 00 6c 00 74 00 4e 00 46 00 2f 00 79 00 50 00 6c .l.t.N.F./.y.P.l
0110 00 7a 00 42 00 78 00 5a 00 48 00 5a 00 33 00 34 .z.B.x.Z.H.Z.3.4
0120 00 43 00 30 00 3d 00 0d 00 0a 30 5c 30 0d 06 09 .C.0.=....0\0...
0130 2a 86 48 86 f7 0d 01 01 04 05 00 03 4b 00 30 48 *.H.........K.0H       K
0140 02 41 00 c3 83 df 44 5c 5d 4d a7 d3 6a be f1 08 .A....D\]M..j...
0150 76 6a c3 fc 62 c3 cd cf f8 6f 7a b6 b8 a2 20 2d vj..b....oz... -
0160 f0 03 6d d2 ba 39 21 1c ee fd 61 49 d1 61 75 e8 ..m..9!...aI.au.
0170 e0 a1 05 0e 32 2c 9e 71 7c da 52 20 79 b2 e9 a7 ....2,.q|.R y...
0180 cc fb 15 02 03 01 00 01 a3 82 01 cb 30 82 01 c7 ............0...
0190 30 14 06 09 2b 06 01 04 01 82 37 12 04 01 01 ff 0...+.....7.....
01a0 04 04 01 00 05 00 30 3c 06 09 2b 06 01 04 01 82 ......0<..+.....
01b0 37 12 02 01 01 ff 04 2c 4d 00 69 00 63 00 72 00 7......,M.i.c.r.     Micr
01c0 6f 00 73 00 6f 00 66 00 74 00 20 00 43 00 6f 00 o.s.o.f.t. .C.o. osoft Co
01d0 72 00 70 00 6f 00 72 00 61 00 74 00 69 00 6f 00 r.p.o.r.a.t.i.o. rporatio
01e0 6e 00 00 00 30 81 cd 06 09 2b 06 01 04 01 82 37 n...0....+.....7 n
01f0 12 05 01 01 ff 04 81 bc 00 30 00 00 01 00 00 00 .........0......
0200 02 00 00 00 06 04 00 00 1c 00 4a 00 66 00 4a 00 ..........J.f.J.      JfJ
0210 b0 00 01 00 33 00 64 00 32 00 36 00 37 00 39 00 ....3.d.2.6.7.9.   3d2679
0220 35 00 34 00 2d 00 65 00 65 00 62 00 37 00 2d 00 5.4.-.e.e.b.7.-. 54-eeb7-
0230 31 00 31 00 64 00 31 00 2d 00 62 00 39 00 34 00 1.1.d.1.-.b.9.4. 11d1-b94
0240 65 00 2d 00 30 00 30 00 63 00 30 00 34 00 66 00 e.-.0.0.c.0.4.f. e-00c04f
0250 61 00 33 00 30 00 38 00 30 00 64 00 00 00 33 00 a.3.0.8.0.d...3. a3080d 3
0260 64 00 32 00 36 00 37 00 39 00 35 00 34 00 2d 00 d.2.6.7.9.5.4.-. d267954-
0270 65 00 65 00 62 00 37 00 2d 00 31 00 31 00 64 00 e.e.b.7.-.1.1.d. eeb7-11d
0280 31 00 2d 00 62 00 39 00 34 00 65 00 2d 00 30 00 1.-.b.9.4.e.-.0. 1-b94e-0
0290 30 00 63 00 30 00 34 00 66 00 61 00 33 00 30 00 0.c.0.4.f.a.3.0. 0c04fa30
02a0 38 00 30 00 64 00 00 00 00 00 00 10 00 80 d4 00 8.0.d........... 80d
02b0 00 00 00 00 30 7e 06 09 2b 06 01 04 01 82 37 12 ....0~..+.....7.
02c0 06 01 01 ff 04 6e 00 30 00 00 00 00 0a 00 3a 00 .....n.0......:.        :
02d0 50 00 41 00 49 00 4e 00 00 00 36 00 39 00 37 00 P.A.I.N...6.9.7. PAIN 697
02e0 31 00 32 00 2d 00 33 00 34 00 37 00 2d 00 37 00 1.2.-.3.4.7.-.7. 12-347-7
02f0 35 00 39 00 33 00 36 00 33 00 32 00 2d 00 34 00 5.9.3.6.3.2.-.4. 593632-4
0300 32 00 36 00 33 00 35 00 00 00 53 00 69 00 74 00 2.6.3.5...S.i.t. 2635 Sit
0310 65 00 20 00 4c 00 69 00 63 00 65 00 6e 00 73 00 e. .L.i.c.e.n.s. e Licens
0320 65 00 20 00 53 00 65 00 72 00 76 00 65 00 72 00 e. .S.e.r.v.e.r. e Server
0330 00 00 00 00 30 21 06 03 55 1d 23 01 01 ff 04 17 ....0!..U.#.....
0340 30 15 a1 0c a4 0a 50 00 41 00 49 00 4e 00 00 00 0.....P.A.I.N...    PAIN
0350 82 05 01 00 00 00 45 30 09 06 05 2b 0e 03 02 1d ......E0...+....
0360 05 00 03 41 00 9d ee b1 62 5d 88 09 5d e8 b5 b1 ...A....b]..]...
0370 c6 0a 9e 00 f3 9b cf 0d 8c 96 d9 a4 de a5 ae ed ................
0380 7f 79 90 47 ba 6f 0d 62 cb 90 40 38 a8 a2 29 f1 .y.G.o.b..@8..).
0390 eb f4 74 45 fe 40 5c 7c b0 d1 f1 d7 68 3a fc 0f ..tE.@\|....h:..
03a0 46 17 8b 3c 22                                  F..<"
        ''')
    # subject commonName=u'ncacn_np:192.168', localityName=u'ncacn_np:192.168.45.6', serialNumber=u'1BcKeW57XltNF/yPlzBxZHZ34C0=\r\n'
    # Critical extension szOID_PKIX_HYDRA_CERT_VERSION(iso(1).identified-organization(3).dod(6).internet(1).private(4).enterprise(1).microsoft(311).MS-crypto-oids(18).szOID_PKIX_HYDRA_CERT_VERSION(4))
    # Critical extension szOID_PKIX_MANUFACTURER(iso(1).identified-organization(3).dod(6).internet(1).private(4).enterprise(1).microsoft(311).MS-crypto-oids(18).szOID_PKIX_MANUFACTURER(2))
    # Critical extension szOID_PKIX_LICENSED_PRODUCT_INFO(iso(1).identified-organization(3).dod(6).internet(1).private(4).enterprise(1).microsoft(311).MS-crypto-oids(18).szOID_PKIX_LICENSED_PRODUCT_INFO(5))
    # Critical extension szOID_PKIX_MS_LICENSE_SERVER_INFO(iso(1).identified-organization(3).dod(6).internet(1).private(4).enterprise(1).microsoft(311).MS-crypto-oids(18).szOID_PKIX_MS_LICENSE_SERVER_INFO(6))
    # Critical extension id-ce-authorityKeyIdentifier(joint-iso-itu-t(2).ds(5).ce(29).id-ce-authorityKeyIdentifier(35))
    # subjectPublicKeyInfo algorithm md5WithRSAEncryption(iso(1).member-body(2).us(840).rsadsi(113549).pkcs(1).pkcs-1(1).md5WithRSAEncryption(4))
    test_ber_roundtrip('pain_ca x509 Certificate',
        TaggedCertificate, pain_host)
    if False:
        file('pain_host', 'w').write(pain_host)
        #os.system('certutil -d pki -A -n pain_host -i pain_host -t p') # will fail - probably because of critical extensions
        # seckey.c:1028 seckey_ExtractPublicKey, tag = SEC_OID_PKCS1_MD5_WITH_RSA_ENCRYPTION not supported ...

    if verify(pain_ca, pain_host, logger=test_logger):
        print 'Verified OK!'

    print
    print 'Replace server pub key with MITM ca key and (self)resign'
    new_host_key = rdp_rsa.test_key_one_512
    new_server_cert_octets = patch_sign(pain_ca, new_host_key, new_ca_key, logger=test_logger)

    print 'Verifying new ca cert correctly self-signed'
    if verify(new_ca_cert_octets, new_server_cert_octets, logger=test_logger):
        print 'Verified OK!'

def test_netpartner_cert_chain():
    """Something from Jan Kristensen <jkr@netpartner.dk>, 10.220.10.36 on domain 2008TS."""
    print 'Self-signed CA cert (using md5 instead of sha-1):'
    self_ca_cert_md5_octets = util.undump(r'''
0000 30 82 04 12 30 82 02 fa a0 03 02 01 02 02 0f 00 0...0...........
0010 c1 00 8b 3c 3c 88 11 d1 3e f6 63 ec df 40 30 0d ...<<...>.c..@0.
0020 06 09 2a 86 48 86 f7 0d 01 01 04 05 00 30 70 31 ..*.H........0p1
0030 2b 30 29 06 03 55 04 0b 13 22 43 6f 70 79 72 69 +0)..U..."Copyri
0040 67 68 74 20 28 63 29 20 31 39 39 37 20 4d 69 63 ght (c) 1997 Mic
0050 72 6f 73 6f 66 74 20 43 6f 72 70 2e 31 1e 30 1c rosoft Corp.1.0.
0060 06 03 55 04 0b 13 15 4d 69 63 72 6f 73 6f 66 74 ..U....Microsoft
0070 20 43 6f 72 70 6f 72 61 74 69 6f 6e 31 21 30 1f  Corporation1!0.
0080 06 03 55 04 03 13 18 4d 69 63 72 6f 73 6f 66 74 ..U....Microsoft
0090 20 52 6f 6f 74 20 41 75 74 68 6f 72 69 74 79 30  Root Authority0
00a0 1e 17 0d 39 37 30 31 31 30 30 37 30 30 30 30 5a ...970110070000Z
00b0 17 0d 32 30 31 32 33 31 30 37 30 30 30 30 5a 30 ..201231070000Z0
00c0 70 31 2b 30 29 06 03 55 04 0b 13 22 43 6f 70 79 p1+0)..U..."Copy
00d0 72 69 67 68 74 20 28 63 29 20 31 39 39 37 20 4d right (c) 1997 M
00e0 69 63 72 6f 73 6f 66 74 20 43 6f 72 70 2e 31 1e icrosoft Corp.1.
00f0 30 1c 06 03 55 04 0b 13 15 4d 69 63 72 6f 73 6f 0...U....Microso
0100 66 74 20 43 6f 72 70 6f 72 61 74 69 6f 6e 31 21 ft Corporation1!
0110 30 1f 06 03 55 04 03 13 18 4d 69 63 72 6f 73 6f 0...U....Microso
0120 66 74 20 52 6f 6f 74 20 41 75 74 68 6f 72 69 74 ft Root Authorit
0130 79 30 82 01 22 30 0d 06 09 2a 86 48 86 f7 0d 01 y0.."0...*.H....
0140 01 01 05 00 03 82 01 0f 00 30 82 01 0a 02 82 01 .........0......
0150 01 00 a9 02 bd c1 70 e6 3b f2 4e 1b 28 9f 97 78 ......p.;.N.(..x
0160 5e 30 ea a2 a9 8d 25 5f f8 fe 95 4c a3 b7 fe 9d ^0....%_...L....
0170 a2 20 3e 7c 51 a2 9b a2 8f 60 32 6b d1 42 64 79 . >|Q....`2k.Bdy
0180 ee ac 76 c9 54 da f2 eb 9c 86 1c 8f 9f 84 66 b3 ..v.T.........f.
0190 c5 6b 7a 62 23 d6 1d 3c de 0f 01 92 e8 96 c4 bf .kzb#..<........
01a0 2d 66 9a 9a 68 26 99 d0 3a 2c bf 0c b5 58 26 c1 -f..h&..:,...X&.
01b0 46 e7 0a 3e 38 96 2c a9 28 39 a8 ec 49 83 42 e3 F..>8.,.(9..I.B.
01c0 84 0f bb 9a 6c 55 61 ac 82 7c a1 60 2d 77 4c e9 ....lUa..|.`-wL.
01d0 99 b4 64 3b 9a 50 1c 31 08 24 14 9f a9 e7 91 2b ..d;.P.1.$.....+
01e0 18 e6 3d 98 63 14 60 58 05 65 9f 1d 37 52 87 f7 ..=.c.`X.e..7R..
01f0 a7 ef 94 02 c6 1b d3 bf 55 45 b3 89 80 bf 3a ec ........UE....:.
0200 54 94 4e ae fd a7 7a 6d 74 4e af 18 cc 96 09 28 T.N...zmtN.....(
0210 21 00 57 90 60 69 37 bb 4b 12 07 3c 56 ff 5b fb !.W.`i7.K..<V.[. !
0220 a4 66 0a 08 a6 d2 81 56 57 ef b6 3b 5e 16 81 77 .f.....VW..;^..w
0230 04 da f6 be ae 80 95 fe b0 cd 7f d6 a7 1a 72 5c ..............r\
0240 3c ca bc f0 08 a3 22 30 b3 06 85 c9 b3 20 77 13 <....."0..... w.
0250 85 df 02 03 01 00 01 a3 81 a8 30 81 a5 30 81 a2 ..........0..0..
0260 06 03 55 1d 01 04 81 9a 30 81 97 80 10 5b d0 70 ..U.....0....[.p
0270 ef 69 72 9e 23 51 7e 14 b2 4d 8e ff cb a1 72 30 .ir.#Q~..M....r0
0280 70 31 2b 30 29 06 03 55 04 0b 13 22 43 6f 70 79 p1+0)..U..."Copy
0290 72 69 67 68 74 20 28 63 29 20 31 39 39 37 20 4d right (c) 1997 M
02a0 69 63 72 6f 73 6f 66 74 20 43 6f 72 70 2e 31 1e icrosoft Corp.1.
02b0 30 1c 06 03 55 04 0b 13 15 4d 69 63 72 6f 73 6f 0...U....Microso
02c0 66 74 20 43 6f 72 70 6f 72 61 74 69 6f 6e 31 21 ft Corporation1!
02d0 30 1f 06 03 55 04 03 13 18 4d 69 63 72 6f 73 6f 0...U....Microso
02e0 66 74 20 52 6f 6f 74 20 41 75 74 68 6f 72 69 74 ft Root Authorit
02f0 79 82 0f 00 c1 00 8b 3c 3c 88 11 d1 3e f6 63 ec y......<<...>.c.
0300 df 40 30 0d 06 09 2a 86 48 86 f7 0d 01 01 04 05 .@0...*.H.......
0310 00 03 82 01 01 00 95 e8 0b c0 8d f3 97 18 35 ed ..............5.
0320 b8 01 24 d8 77 11 f3 5c 60 32 9f 9e 0b cb 3e 05 ..$.w..\`2....>.
0330 91 88 8f c9 3a e6 21 f2 f0 57 93 2c b5 a0 47 c8 ....:.!..W.,..G.
0340 62 ef fc d7 cc 3b 3b 5a a9 36 54 69 fe 24 6d 3f b....;;Z.6Ti.$m?
0350 c9 cc aa de 05 7c dd 31 8d 3d 9f 10 70 6a bb fe .....|.1.=..pj..
0360 12 4f 18 69 c0 fc d0 43 e3 11 5a 20 4f ea 62 7b .O.i...C..Z O.b{
0370 af aa 19 c8 2b 37 25 2d be 65 a1 12 8a 25 0f 63 ....+7%-.e...%.c
0380 a3 f7 54 1c f9 21 c9 d6 15 f3 52 ac 6e 43 32 07 ..T..!....R.nC2.
0390 fd 82 17 f8 e5 67 6c 0d 51 f6 bd f1 52 c7 bd e7 .....gl.Q...R...
03a0 c4 30 fc 20 31 09 88 1d 95 29 1a 4d d5 1d 02 a5 .0. 1....).M....
03b0 f1 80 e0 03 b4 5b f4 b1 dd c8 57 ee 65 49 c7 52 .....[....W.eI.R
03c0 54 b6 b4 03 28 12 ff 90 d6 f0 08 8f 7e b8 97 c5 T...(.......~...
03d0 ab 37 2c e4 7a e4 a8 77 e3 76 a0 00 d0 6a 3f c1 .7,.z..w.v...j?.
03e0 d2 36 8a e0 41 12 a8 35 6a 1b 6a db 35 e1 d4 1c .6..A..5j.j.5...
03f0 04 e4 a8 45 04 c8 5a 33 38 6e 4d 1c 0d 62 b7 0a ...E..Z38nM..b..
0400 a2 8c d3 d5 54 3f 46 cd 1c 55 a6 70 db 12 3a 87 ....T?F..U.p..:.
0410 93 75 9f a7 d2 a0                               .u....
        ''')
    # (1, 2, 840, 113549, 2, 5), 'id-md5(iso(1).member-body(2).us(840).rsadsi(113549).digestAlgorithm(2).id-md5(5))')

    test_ber_roundtrip('self-signed md5 x509 Certificate',
        TaggedCertificate, self_ca_cert_md5_octets)

    if verify(self_ca_cert_md5_octets, self_ca_cert_md5_octets, logger=test_logger):
        print 'Selfsigning Verified OK!'

    print 'Replace server pub key with MITM ca key and (self)resign'
    import rdp_rsa
    new_ca_key = rdp_rsa.test_key_one_512
    new_ca_cert_octets = patch_sign(self_ca_cert_md5_octets, new_ca_key, new_ca_key, logger=test_logger)

    print 'Verifying new ca cert correctly self-signed'
    if verify(new_ca_cert_octets, new_ca_cert_octets, logger=test_logger):
        print 'Verified OK!'

    print 'Host cert signed with self-signed CA cert:'
    self_host_cert_octets = util.undump(r'''
0000 30 82 04 f1 30 82 03 d9 a0 03 02 01 02 02 0b 00 0...0...........
0010 eb aa 11 d6 2e 24 81 08 18 20 30 0d 06 09 2a 86 .....$... 0...*.
0020 48 86 f7 0d 01 01 04 05 00 30 70 31 2b 30 29 06 H........0p1+0).
0030 03 55 04 0b 13 22 43 6f 70 79 72 69 67 68 74 20 .U..."Copyright
0040 28 63 29 20 31 39 39 37 20 4d 69 63 72 6f 73 6f (c) 1997 Microso
0050 66 74 20 43 6f 72 70 2e 31 1e 30 1c 06 03 55 04 ft Corp.1.0...U.
0060 0b 13 15 4d 69 63 72 6f 73 6f 66 74 20 43 6f 72 ...Microsoft Cor
0070 70 6f 72 61 74 69 6f 6e 31 21 30 1f 06 03 55 04 poration1!0...U.
0080 03 13 18 4d 69 63 72 6f 73 6f 66 74 20 52 6f 6f ...Microsoft Roo
0090 74 20 41 75 74 68 6f 72 69 74 79 30 1e 17 0d 30 t Authority0...0
00a0 32 30 32 32 35 30 38 30 30 30 30 5a 17 0d 31 30 20225080000Z..10
00b0 30 32 32 36 30 38 30 30 30 30 5a 30 81 b9 31 0b 0226080000Z0..1.
00c0 30 09 06 03 55 04 06 13 02 55 53 31 13 30 11 06 0...U....US1.0..
00d0 03 55 04 08 13 0a 57 61 73 68 69 6e 67 74 6f 6e .U....Washington
00e0 31 10 30 0e 06 03 55 04 07 13 07 52 65 64 6d 6f 1.0...U....Redmo
00f0 6e 64 31 1e 30 1c 06 03 55 04 0a 13 15 4d 69 63 nd1.0...U....Mic
0100 72 6f 73 6f 66 74 20 43 6f 72 70 6f 72 61 74 69 rosoft Corporati
0110 6f 6e 31 2b 30 29 06 03 55 04 0b 13 22 43 6f 70 on1+0)..U..."Cop
0120 79 72 69 67 68 74 20 28 63 29 20 31 39 39 39 20 yright (c) 1999
0130 4d 69 63 72 6f 73 6f 66 74 20 43 6f 72 70 2e 31 Microsoft Corp.1
0140 36 30 34 06 03 55 04 03 13 2d 4d 69 63 72 6f 73 604..U...-Micros
0150 6f 66 74 20 45 6e 66 6f 72 63 65 64 20 4c 69 63 oft Enforced Lic
0160 65 6e 73 69 6e 67 20 49 6e 74 65 72 6d 65 64 69 ensing Intermedi
0170 61 74 65 20 50 43 41 30 82 01 22 30 0d 06 09 2a ate PCA0.."0...*
0180 86 48 86 f7 0d 01 01 01 05 00 03 82 01 0f 00 30 .H.............0
0190 82 01 0a 02 82 01 01 00 b4 00 54 6f 9b 51 26 76 ..........To.Q&v
01a0 b7 25 13 dd 4e 33 1a f2 7d 78 03 d3 f8 a6 c4 75 .%..N3..}x.....u
01b0 bc 37 73 5a ec 6a 20 ac ce da 2f 9d 12 bf 21 e4 .7sZ.j .../...!.
01c0 0e a8 84 41 90 81 1e 1f 71 53 12 eb 09 ab a7 8b ...A....qS......
01d0 20 2f d6 e0 ae 91 e7 dd 54 e5 ce 81 0f e2 42 86  /......T.....B.
01e0 ed 67 c8 30 40 cb cb bc e2 3a 67 82 7e 0e ef af .g.0@....:g.~...
01f0 27 09 64 86 89 9d a1 e4 0f af 0c 1e 43 96 5b c5 '.d.........C.[.
0200 44 e9 9f 59 56 9c d4 5d ee 2e 83 23 77 aa f1 99 D..YV..]...#w...
0210 ee b9 d6 32 bf de 31 a6 2a e1 87 55 af 30 87 8f ...2..1.*..U.0..
0220 13 ea 8b 04 48 80 be 80 2e 62 40 40 9e 3b 6d 3e ....H....b@@.;m>
0230 81 ff ec 74 d5 24 1c 53 2c 83 b7 f8 22 ca f8 08 ...t.$.S,..."...
0240 f5 e1 30 da 9d e5 f3 6a 49 9f 95 dc 96 44 a5 70 ..0....jI....D.p
0250 f9 dc 14 fd 65 94 2d 2f 33 3a 82 c1 92 66 7f 02 ....e.-/3:...f..
0260 f0 47 67 5e 04 4d d3 26 dd d4 df a7 c6 13 cb ad .Gg^.M.&........
0270 b6 03 85 c7 33 54 28 a4 be de 12 35 b2 50 1e 78 ....3T(....5.P.x
0280 b8 cf b8 45 6c 7c fe bd 1c 85 0e c1 0f 0c 62 da ...El|........b.
0290 c7 3e de d6 d5 62 a4 e7 02 03 01 00 01 a3 82 01 .>...b..........
02a0 40 30 82 01 3c 30 2b 06 03 55 1d 25 04 24 30 22 @0..<0+..U.%.$0"
02b0 06 08 2b 06 01 05 05 07 03 03 06 0a 2b 06 01 04 ..+.........+...
02c0 01 82 37 0a 06 01 06 0a 2b 06 01 04 01 82 37 0a ..7.....+.....7.
02d0 06 02 30 81 a2 06 03 55 1d 01 04 81 9a 30 81 97 ..0....U.....0..
02e0 80 10 5b d0 70 ef 69 72 9e 23 51 7e 14 b2 4d 8e ..[.p.ir.#Q~..M.
02f0 ff cb a1 72 30 70 31 2b 30 29 06 03 55 04 0b 13 ...r0p1+0)..U...
0300 22 43 6f 70 79 72 69 67 68 74 20 28 63 29 20 31 "Copyright (c) 1
0310 39 39 37 20 4d 69 63 72 6f 73 6f 66 74 20 43 6f 997 Microsoft Co
0320 72 70 2e 31 1e 30 1c 06 03 55 04 0b 13 15 4d 69 rp.1.0...U....Mi
0330 63 72 6f 73 6f 66 74 20 43 6f 72 70 6f 72 61 74 crosoft Corporat
0340 69 6f 6e 31 21 30 1f 06 03 55 04 03 13 18 4d 69 ion1!0...U....Mi
0350 63 72 6f 73 6f 66 74 20 52 6f 6f 74 20 41 75 74 crosoft Root Aut
0360 68 6f 72 69 74 79 82 0f 00 c1 00 8b 3c 3c 88 11 hority......<<..
0370 d1 3e f6 63 ec df 40 30 10 06 09 2b 06 01 04 01 .>.c..@0...+....
0380 82 37 15 01 04 03 02 01 00 30 1d 06 03 55 1d 0e .7.......0...U..
0390 04 16 04 14 a7 f6 94 65 92 e5 f7 89 1d 24 fe 48 .......e.....$.H
03a0 75 a8 8c 93 fb 1c 1d 46 30 19 06 09 2b 06 01 04 u......F0...+...
03b0 01 82 37 14 02 04 0c 1e 0a 00 53 00 75 00 62 00 ..7.......S.u.b.      Sub
03c0 43 00 41 30 0b 06 03 55 1d 0f 04 04 03 02 01 86 C.A0...U........ C
03d0 30 0f 06 03 55 1d 13 01 01 ff 04 05 30 03 01 01 0...U.......0...
03e0 ff 30 0d 06 09 2a 86 48 86 f7 0d 01 01 04 05 00 .0...*.H........
03f0 03 82 01 01 00 57 50 d5 08 d3 5f fd d2 8e 23 1d .....WP..._...#.
0400 34 de bd 0b b2 71 3c da 9d 1b 6d 3b 65 42 b7 e3 4....q<...m;eB..
0410 da 81 d9 38 86 c4 52 57 ba e9 dd 53 b3 6d cb 7b ...8..RW...S.m.{
0420 fe 14 44 ce 6c 12 f8 89 48 ba 49 9e e7 51 47 36 ..D.l...H.I..QG6
0430 96 ce e2 f6 52 7d b7 5e 49 1d 54 53 0c 9e 03 7e ....R}.^I.TS...~
0440 05 16 29 1b e0 19 3c 0c a9 79 0f 87 9f 71 c1 74 ..)...<..y...q.t
0450 f2 7b 24 65 6e 57 11 a5 71 78 1f 73 0e 2b a6 6c .{$enW..qx.s.+.l
0460 c1 c2 2c 02 80 92 9b 4a 87 da da c3 c1 8b d2 08 ..,....J........
0470 ae 08 d2 dd be f0 bb 02 58 80 bc 68 81 4e 66 c8 ........X..h.Nf.
0480 3b 8b 92 c8 62 5f a4 d3 e0 bd d7 a0 98 ab ab 07 ;...b_..........
0490 a3 97 31 1b 0c c8 cb 4f 96 b5 ae 48 0c f6 34 ba ..1....O...H..4.
04a0 ca e2 66 cf 2a bd 58 57 3c 1b 96 5b 62 81 5f cf ..f.*.XW<..[b._.
04b0 c1 6c b8 b9 05 ef 77 28 9f 5f b6 63 c7 03 6f 05 .l....w(._.c..o.
04c0 e3 0b 5c 27 40 9f 4c 7e cb 14 6a e4 e4 35 c4 43 ..\'@.L~..j..5.C
04d0 d4 9a 43 39 77 26 2d a3 61 e1 12 d6 b6 be 6d 11 ..C9w&-.a.....m.
04e0 62 f5 c0 2d 11 47 23 f0 cd 8c d0 95 3b 3c 02 94 b..-.G#.....;<..
04f0 45 02 b4 83 95                                  E....
        ''')
    test_ber_roundtrip('self-signed self_host_cert_octets x509 Certificate',
        TaggedCertificate, self_host_cert_octets)

    if verify(self_ca_cert_md5_octets, self_host_cert_octets, logger=test_logger):
        print 'Selfsigning Verified OK!'

    print 'Replace server pub key with MITM ca key and (self)resign'
    new_host_key = rdp_rsa.test_key_two_512
    new_host_cert_octets = patch_sign(self_host_cert_octets, new_host_key, new_ca_key, logger=test_logger)

    print 'Verifying new host cert correctly self-signed'
    if verify(new_ca_cert_octets, new_host_cert_octets, logger=test_logger):
        print 'Verified OK!'

def test_read_certificate():
    # https://developer.mozilla.org/en/Introduction_to_Public-Key_Cryptography#A_Typical_Certificate
    print 'Reading x509 "-----BEGIN CERTIFICATE-----" base64-encoded'
    cert_string = """
        -----BEGIN CERTIFICATE-----
        MIICKzCCAZSgAwIBAgIBAzANBgkqhkiG9w0BAQQFADA3MQswCQYDVQQGEwJVUzER
        MA8GA1UEChMITmV0c2NhcGUxFTATBgNVBAsTDFN1cHJpeWEncyBDQTAeFw05NzEw
        MTgwMTM2MjVaFw05OTEwMTgwMTM2MjVaMEgxCzAJBgNVBAYTAlVTMREwDwYDVQQK
        EwhOZXRzY2FwZTENMAsGA1UECxMEUHViczEXMBUGA1UEAxMOU3Vwcml5YSBTaGV0
        dHkwgZ8wDQYJKoZIhvcNAQEFBQADgY0AMIGJAoGBAMr6eZiPGfjX3uRJgEjmKiqG
        7SdATYazBcABu1AVyd7chRkiQ31FbXFOGD3wNktbf6hRo6EAmM5/R1AskzZ8AW7L
        iQZBcrXpc0k4du+2Q6xJu2MPm/8WKuMOnTuvzpo+SGXelmHVChEqooCwfdiZywyZ
        NMmrJgaoMa2MS6pUkfQVAgMBAAGjNjA0MBEGCWCGSAGG+EIBAQQEAwIAgDAfBgNV
        HSMEGDAWgBTy8gZZkBhHUfWJM1oxeuZc+zYmyTANBgkqhkiG9w0BAQQFAAOBgQBt
        I6/z07Z635DfzX4XbAFpjlRl/AYwQzTSYx8GfcNAqCqCwaSDKvsuj/vwbf91o3j3
        UkdGYpcd2cYRCgKi4MwqdWyLtpuHAH18hHZ5uvi00mJYw8W2wUOsY0RC/a/IDy84
        hW3WWehBUqVK5SY4/zJ4oTjx7dwNMdGwbWfpRqjd1A==
        -----END CERTIFICATE-----
    """
    cert_bytes = read_certificate(cert_string)
    util.dump(cert_bytes)

    test_ber_roundtrip('Real-world x509 Certificate',
        TaggedCertificate, cert_bytes)

def test_some_ad_cert_stuff():
    mads_from_giritech_ca = (
        "0\x81\xc20-\x0c$Mads Kiilerich's Giritech Root CA ID\x03\x02\x06@\x04\x01\x820&\x04$1ce7f113-43cd-4447-bb73-ec5877461100\xa1i0g0\x0b\x04\x02C\x00\x02\x01\x00\x80\x02\x06\x0e\xa0L0J1\x130\x11\x06\n\t\x92&\x89\x93\xf2,d\x01\x19\x16" +
        "\x03com1\x180\x16\x06\n\t\x92&\x89\x93\xf2,d\x01\x19\x16\x08giritech1\x190\x17\x06\x03U\x04\x03\x13\x10Giritech Root CA\x02\n!\x91\xed\xfd\x00\x00\x00\x00\x00\x120\x81\x9801\x0c(Mads Kristian Kiilerich's TDC OCES CA ID\x03\x02\x06@\x04\x01\x820\x16\x04\x14{" +
        '\xb8~\xd9,\xc6\x08\x8f\xd1\x83\xf9\x17Qb\x12!\xb9U\xe4\xa1\xa1K0I0\x0c\x04\x02C\x00\x02\x02\x06\x0e\x80\x02\x05{\xa03011\x0b0\t\x06\x03U\x04\x06\x13\x02DK1\x0c0\n\x06\x03U\x04\n\x13\x03TDC1\x140\x12\x06\x03U\x04\x03\x13\x0bTDC OCES CA\x02\x04Eq\x1bc0~0*\x0c!TDC OCES CA issued by TDC ' +
        'OCES CA\x03\x02\x06@\x04\x01\x820\x03\x04\x01\x00\xa1K0I0\x0c\x04\x02C\x00\x02\x02\x0b\x89\x80\x02\x05\x1d\xa03011\x0b0\t\x06\x03U\x04\x06\x13\x02DK1\x0c0\n\x06\x03U\x04\n\x13\x03TDC1\x140\x12\x06\x03U\x04\x03\x13\x0bTDC OCES CA\x02\x04>H\xbd\xc4\x00\x16\x08giritech1\x190\x17\x06\x03U\x04\x03\x13\x10Giritech R' +
        '0t0-\x0c$1a3f5778-b259-4cae-876e-9eed496c7a80\x03\x02\x06\xc0\x04\x01\x8203\x04$1a3f5778-b259-4cae-876e-9eed496c7a80\x03\x03\x06t\x00\x03\x02\x03\xb8\x02\x02\x00\x84\xa0\x020\x00\xa1\n0\x080\x02\x04\x00\x02\x02\x04\x000t0-\x0c$b3da' +
        '7a2d-241b-486e-b9d1-d14d10437d2b\x03\x02\x06\xc0\x04\x01\x8203\x04$b3da7a2d-241b-486e-b9d1-d14d10437d2b\x03\x03\x06t\x00\x03\x02\x03\xb8\x02\x02\x00\x85\xa0\x020\x00\xa1\n0\x080\x02\x04\x00\x02\x02\x04\x000t0-\x0c$Mads Kiilerich' +
        "'s Giritech Root CA ID\x03\x02\x06\xc0\x04\x01\x8203\x04$1ce7f113-43cd-4447-bb73-ec5877461100\x03\x03\x06t\x00\x03\x02\x03\xb8\x02\x02\x00\x86\xa0\x020\x00\xa1\n0\x080\x02\x04\x00\x02\x02\x04\x000\x81\xeb01\x0c(Mads Kristian Kiilerich" +
        "'s TDC OCES CA ID\x03\x02\x06\xc0\x04\x01\x820#\x04\x14{\xb8~\xd9,\xc6\x08\x8f\xd1\x83\xf9\x17Qb\x12!\xb9U\xe4\xa1\x03\x03\x06t\x00\x03\x02\x03\x80\x02\x02\x00\x87\xa0\x81\x840\x81\x810\x7f1\x0b0\t\x06\x03U\x04\x06\x13\x02DK1)0'\x06\x03U\x04\n\x13 Ingen organisatorisk tilknytning1E0" +
        '\x1e\x06\x03U\x04\x03\x13\x17Mads Kristian Kiilerich0#\x06\x03U\x04\x05\x13\x1cPID:9208-2002-2-554999497797\xa1\n0\x080\x02\x04\x00\x02\x02\x04\x00\x00\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff'
        )[:197]

    OIDS.declare('tja',
        '{0 6(6) 14(14)}')

    Cert = SEQUENCE(
        Field('a', SEQUENCE(
            Field('a-a-someName2', UnicodeString()),
            Field('a-b-someBitString', BIT_STRING()),
            Field('a-c-someOctetString82', OCTET_STRING()),
            )),
        Field('b', SEQUENCE(
            Field('b-guid', OCTET_STRING()),
            )),
        Field('c', SEQUENCE(
            Field('c-a', SEQUENCE(
                Field('c-a-a', SEQUENCE(
                    Field('c-a-a-a-someOctetString-c0', OCTET_STRING()),
                    Field('c-a-a-b-someInteger', INTEGER()),
                    Field('c-a-a-c-someOID', OBJECT_IDENTIFIER(), CONTEXT(0)),
                    )),
                Field('c-a-b', SEQUENCE(
                    Field('c-a-b-a', Name),
                    ), CONTEXT(0)),
                Field('c-a-c', INTEGER()),
                )),
            ), CONTEXT(1)),
        )

    test_ber_roundtrip('Mads Kiilerich\'s Giritech Root CA ID',
        Field(None, Cert), mads_from_giritech_ca)

def test_ironkey_cert():
    print 'sample IronKey CKO_CERTIFICATE info:'
    cka_value = '0\x82\x03\xfa0\x82\x02\xe2\xa0\x03\x02\x01\x02\x02\x08%&keP\x85k\x070\r\x06\t*\x86H\x86\xf7\r\x01\x01\x05\x05\x00041 0\x1e\x06\x03U\x04\x03\x0c\x17IronKey Browser CA - G11\x100\x0e\x06\x03U\x04\n\x0c\x07IronKey0\x1e\x17\r091210235732Z\x17\r191208235732Z071#0!\x06\x03U\x04\x03\x0c\x1a0108535916024f419C0B02af981\x100\x0e\x06\x03U\x04\n\x0c\x07IronKey0\x82\x01"0\r\x06\t*\x86H\x86\xf7\r\x01\x01\x01\x05\x00\x03\x82\x01\x0f\x000\x82\x01\n\x02\x82\x01\x01\x00\xb2$/\x0e)\xdd\xb8D\x98\x02\xc4\xff[\xb4\x98He\xbc\xc0\xaa\x81\x11\xa5\x82\x99\xeb\x80[C`o\x18G\x19_=\x03\x15#R5\x9a]\xf1AEXR\xaf\xe1\x0b\xcc\r>\r\xda\x89\x9f\xee\x7f\x17\xafh\xc1\xccr-\xb6\x84\x06<\xb4Jb\xea\x85|\xca\xc5\xceM\x13\xb6O\xddC\x8by\xd8\xc8\x1f\x97]\x9b\xa2\x13\xc5\xb2XVZ#\x87\xe2\x86\x8a\xc0@\x08\xc6CCy\xd3\xce1\x12\x822\xc5\xbc\xe2\xddRi@f1\xb9\x89\x17\x82)\xef\xf0\xe7\x9c\x04\xc5;H\x80\xdc\xben7\xd64M\xb9\x87\xc64\xaf,!\x1d\xd1%_n,g\x1b;D\xf0\x01\xcb\xdf\xf9\xdd\xbb\xee7\x16;;\xa9\xed\x90\x9fiQ*M\xe8z\xf3p\x9c\xe1\xe0\'\xb2\xd0\xc2P\x8fF\x15\x1c\xf5\xc0\xe3Q\xe7hUw\xb4Iy\x83\xc2\xe7s,\xe6s\xbc\x14u\xe3\xeae\xa7F\xc99Ff\x89\x88}g\xd9\xb1\xc4\x8e\xdb\x8b\xbf\x9e~\x0bI\xf6H\xf3pR\xf7 3\x95\x02\x03\x01\x00\x01\xa3\x82\x01\x0b0\x82\x01\x070\x0c\x06\x03U\x1d\x13\x01\x01\xff\x04\x020\x000\x0e\x06\x03U\x1d\x0f\x01\x01\xff\x04\x04\x03\x02\x05\xa00\x1d\x06\x03U\x1d\x0e\x04\x16\x04\x14\x84\'[\x19k\x19,\x97\xa1\xde\xe0\t\x8f\xcb\x83d\x81\xb4\xca\x810\x1f\x06\x03U\x1d#\x04\x180\x16\x80\x14R\xd7\x92\xa6[\x01\x94-[\xea~ O\x88\x98\xae\x1e\xc50M0V\x06\x03U\x1d\x1f\x04O0M0K\xa0I\xa0G\x86Ehttp://pki.ironkey.com/certificates/revocation/crl/IronKeyBrowserCAG10O\x06\x08+\x06\x01\x05\x05\x07\x01\x01\x04C0A0?\x06\x08+\x06\x01\x05\x05\x070\x01\x863http://pki.ironkey.com/certificates/revocation/ocsp0\r\x06\t*\x86H\x86\xf7\r\x01\x01\x05\x05\x00\x03\x82\x01\x01\x00Mc\xfdN\x80&f~\xc4\x80\xd2\xd5o\xb2\x96\x81 J\xe0\x17<4\xee\xa0}\x85x\xe7H\x13\xd8\xff\x8aB\x891\x04\x82\x95\xe7\xf0MC\x8f\x93B\xb66JRD\x94\xe8\xa4^\xc6\xcc>\x95\xd8\x1a3\x1d\xaanN\xa2k\xd0<\x07\xc6\xb7\xbe\xc1\x00|,z\xb8\x90F\xd3]g\xa4\xefxa\x16 \x0cu!\xf0\xbb\xe3\x14);n\x0b\xfc\xc9\xe4\xbe\xc0\x99z\xc7\x04\x95q"\x99\x8cGu\x84\xab9r\x13\xeac~\xe7\xb6\xa03\xb3\x9e{\xc1\xf2a\xea\x08\xaa\x87\xa9\xed\x1ffl\xb2\x83\x99`\x12\xba\x08\xbe\xcb\xb6\x01\x01\xf0N\xb1qk\x9d\xd1\xf8\x01\x91\x0c\xc5\x0f|\xf6\x14mT\x7f\x8d[w\xd51)\xf4\x7f<\x1f<&\x1f\xa7\x16tb\x05o\xdf\xd7I\xfd\x18[h\x18l*\xcaSd\x02rD\xa8q\x97\xb0\\\x11E\xfa\xb0\x8f\xbaG\xf1Z-r\xeeC\xf6\x1c\xfe\xa9\xf1\x8aM\xe2\x98\xb45}_o\x18\xe5\xe5\x94\xeb@\xe1\xda\x8f\x8b\x94<\x8f'
    test_ber_roundtrip('CKA_VALUE', TaggedCertificate, cka_value)

    cka_issuer = '041 0\x1e\x06\x03U\x04\x03\x0c\x17IronKey Browser CA - G11\x100\x0e\x06\x03U\x04\n\x0c\x07IronKey'
    test_ber_roundtrip('CKA_VALUE', Name, cka_issuer)

    cka_subject = '071#0!\x06\x03U\x04\x03\x0c\x1a0108535916024f419C0B02af981\x100\x0e\x06\x03U\x04\n\x0c\x07IronKey'
    test_ber_roundtrip('CKA_SUBJECT', Name, cka_subject)

def test_google_cert():
    print 'www.google.com'
    from pprint import pprint
    pprint(TaggedCertificate.ber_decode(read_certificate("""
-----BEGIN CERTIFICATE-----
MIIDIzCCAoygAwIBAgIEMAAAAjANBgkqhkiG9w0BAQUFADBfMQswCQYDVQQGEwJV
UzEXMBUGA1UEChMOVmVyaVNpZ24sIEluYy4xNzA1BgNVBAsTLkNsYXNzIDMgUHVi
bGljIFByaW1hcnkgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMDQwNTEzMDAw
MDAwWhcNMTQwNTEyMjM1OTU5WjBMMQswCQYDVQQGEwJaQTElMCMGA1UEChMcVGhh
d3RlIENvbnN1bHRpbmcgKFB0eSkgTHRkLjEWMBQGA1UEAxMNVGhhd3RlIFNHQyBD
QTCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEA1NNn0I0Vf67NMf59HZGhPwtx
PKzMyGT7Y/wySweUvW+Aui/hBJPAM/wJMyPpC3QrccQDxtLN4i/1CWPN/0ilAL/g
5/OIty0y3pg25gqtAHvEZEo7hHUD8nCSfQ5i9SGraTaEMXWQ+L/HbIgbBpV8yeWo
3nWhLHpo39XKHIdYYBkCAwEAAaOB/jCB+zASBgNVHRMBAf8ECDAGAQH/AgEAMAsG
A1UdDwQEAwIBBjARBglghkgBhvhCAQEEBAMCAQYwKAYDVR0RBCEwH6QdMBsxGTAX
BgNVBAMTEFByaXZhdGVMYWJlbDMtMTUwMQYDVR0fBCowKDAmoCSgIoYgaHR0cDov
L2NybC52ZXJpc2lnbi5jb20vcGNhMy5jcmwwMgYIKwYBBQUHAQEEJjAkMCIGCCsG
AQUFBzABhhZodHRwOi8vb2NzcC50aGF3dGUuY29tMDQGA1UdJQQtMCsGCCsGAQUF
BwMBBggrBgEFBQcDAgYJYIZIAYb4QgQBBgpghkgBhvhFAQgBMA0GCSqGSIb3DQEB
BQUAA4GBAFWsY+reod3SkF+fC852vhNRj5PZBSvIG3dLrWlQoe7e3P3bB+noOZTc
q3J5Lwa/q4FwxKjt6lM07e8eU9kGx1Yr0Vz00YqOtCuxN5BICEIlxT6Ky3/rbwTR
bcV0oveifHtgPHfNDs5IAn8BL7abN+AqKjbc1YXWrOU/VG+WHgWv
-----END CERTIFICATE-----
""")))
    print


if __name__ == '__main__':
    test_TaggedCertificate_pain()
    test_SubjectPublicKeyInfo_subjectPublicKey()
    test_pain_cert_chain()
    test_netpartner_cert_chain()
    test_read_certificate()
    test_some_ad_cert_stuff()
    test_ironkey_cert()
    test_google_cert()
