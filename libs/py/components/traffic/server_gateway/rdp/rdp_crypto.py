"""
Key generation and hashing for RDP
"""

import hashlib
import struct

import util
import rdp_rc4


def SHA(s):
    return hashlib.sha1(s).digest()

def MD5(s):
    return hashlib.md5(s).digest()

class SessionKeys(object):

    def __init__(self, ClientRandom, ServerRandom, key_length=128):
        """
        Initialize InitialServerEncryptKey, InitialServerDecryptKey, MACKeyN
        """
        self.key_length = key_length
        # http://msdn.microsoft.com/en-us/library/cc240784 5.3.5 Initial Session Key Generation
        # http://msdn.microsoft.com/en-us/library/cc240785 5.3.5.1 Non-FIPS

        def SaltedHash(S, I):
            return MD5(S + SHA(I + S + ClientRandom + ServerRandom))

        # The client and server random values are used to create a 384-bit Pre-Master Secret by concatenating the first 192 bits of the Client Random with the first 192 bits of the Server Random.
        PreMasterSecret = ClientRandom[:192/8] + ServerRandom[:192/8]
        #print 'PreMasterSecret'; util.dump(PreMasterSecret)

        # A 384-bit Master Secret is generated using the Pre-Master Secret, the client and server random values, and the MD5 and SHA-1 hash functions.
        def PreMasterHash(I):
            return SaltedHash(PreMasterSecret, I)

        MasterSecret = PreMasterHash('A') + PreMasterHash('BB') + PreMasterHash('CCC')
        #print 'MasterSecret'; util.dump(MasterSecret)

        # A 384-bit session key BLOB is generated as follows.
        def MasterHash(I):
            return SaltedHash(MasterSecret, I)
        SessionKeyBlob = MasterHash('X') + MasterHash('YY') + MasterHash('ZZZ')
        #print 'SessionKeyBlob'; util.dump(SessionKeyBlob)

        # From the session key BLOB the actual session keys which will be used are derived. Both client and server extract the same key data for generating MAC signatures.
        MACKey128 = SessionKeyBlob[:128/8]

        #The initial encryption and decryption keys are generated next (these keys are updated at a later point in the protocol, per section 5.3.6.1). The server generates its encryption and decryption keys as follows.
        def FinalHash(K):
            return MD5(K + ClientRandom + ServerRandom)

        InitialServerEncryptKey128 = FinalHash(SessionKeyBlob[1*128/8:2*128/8])
        #print 'InitialServerEncryptKey128'; util.dump(InitialServerEncryptKey128)
        InitialServerDecryptKey128 = FinalHash(SessionKeyBlob[2*128/8:3*128/8])
        #print 'InitialServerDecryptKey128'; util.dump(InitialServerDecryptKey128)

        if key_length == 128:
            InitialServerEncryptKey = InitialServerEncryptKey128
            InitialServerDecryptKey = InitialServerDecryptKey128
            self.MACKeyN = MACKey128

        elif key_length == 40:
            # Using the salt values, the 40-bit keys are generated as follows.
            InitialServerEncryptKey = '\xD1\x26\x9E' + InitialServerEncryptKey128[:64/8][-40/8:]
            InitialServerDecryptKey = '\xD1\x26\x9E' + InitialServerDecryptKey128[:64/8][-40/8:]
            self.MACKeyN = '\xD1\x26\x9E' + MACKey128[:64/8][-40/8:]

        elif key_length == 56:
            # The 56-bit keys are generated as follows.
            InitialServerEncryptKey = '\xD1' + InitialServerEncryptKey128[:64/8][-56/8:]
            InitialServerDecryptKey = '\xD1' + InitialServerDecryptKey128[:64/8][-56/8:]
            self.MACKeyN = '\xD1' + MACKey128[:64/8][-56/8:]

        else: raise RuntimeError('Unknown key_length %s' % key_length)

        # http://msdn.microsoft.com/en-us/library/cc240787 5.3.6 Encrypting and Decrypting the I/O Data Stream
        self.client_server_crypt = RdpRc4Update4096(InitialServerDecryptKey, key_length)
        self.server_client_crypt = RdpRc4Update4096(InitialServerEncryptKey, key_length)
        self.de_hasher = MacHasher(self.MACKeyN)
        self.en_hasher = MacHasher(self.MACKeyN)


class RdpRc4Update4096(object):

    def __init__(self, InitialKeyN, key_length):
        self.key_length = key_length
        self.InitialKeyN = InitialKeyN
        self.CurrentKeyN = self.InitialKeyN
        self._rc4_prng = rdp_rc4.PRNG(self.CurrentKeyN)
        self._use_count = 0

    def xor(self, s):
        res = self._rc4_prng.xor(s)
        #print self, 'use count', self._use_count
        self._use_count += 1
        if self._use_count == 4096:
            #print 'Updating RC4 keys after 4096 uses'
            #print 'self.InitialKeyN:'; util.dump(self.InitialKeyN)
            #print 'old CurrentKeyN:'; util.dump(self.CurrentKeyN)
            self.CurrentKeyN = self.update_session_keys(self.InitialKeyN, self.CurrentKeyN, self.key_length)
            #print 'new self.CurrentKeyN'; util.dump(self.CurrentKeyN)
            self._rc4_prng = rdp_rc4.PRNG(self.CurrentKeyN)
            self._use_count = 0
        return res

    @staticmethod
    def update_session_keys(InitialEncryptKeyN, CurrentEncryptKeyN, key_length):
        """
        Return NewEncryptKeyN
        """
        # http://msdn.microsoft.com/en-us/library/cc240791 5.3.7 Session Key Updates
        # http://msdn.microsoft.com/en-us/library/cc240792 5.3.7.1 Non-FIPS

        Pad1 = '\x36' * 40 # 320 bits
        Pad2 = '\x5C' * 48 # 384 bits

        # If the negotiated key strength is 40-bit or 56-bit, then the first 64 bits of the initial and current encryption keys will be used.
        if key_length == 128:
            InitialEncryptKey = InitialEncryptKeyN
            CurrentEncryptKey = CurrentEncryptKeyN
        elif key_length in [40, 56]:
            InitialEncryptKey = InitialEncryptKeyN[:64/8]
            CurrentEncryptKey = CurrentEncryptKeyN[:64/8]
        else: raise RuntimeError('Unknown key_length %s' % key_length)

        SHAComponent = SHA(InitialEncryptKey + Pad1 + CurrentEncryptKey)
        TempKey128 = MD5(InitialEncryptKey + Pad2 + SHAComponent)

        if key_length == 128:
            STableEncrypt = rdp_rc4.PRNG(TempKey128)
            # RC4 is then used to encrypt TempKey128 to obtain the new 128-bit encryption key.
            NewEncryptKey128 = STableEncrypt.xor(TempKey128)
            return NewEncryptKey128

        TempKey64 = TempKey128[:64/8]
        STableEncrypt = rdp_rc4.PRNG(TempKey64)
        # RC4 is then used to encrypt these 64 bits, and the first few bytes are salted according to the key strength to derive a new 40-bit or 56-bit encryption key (see section 5.3.5.1 for details on how to perform the salting operation).
        PreSaltKey = STableEncrypt.xor(TempKey64)
        if key_length == 40:
            NewEncryptKey40 = '\xD1\x26\x9E' + PreSaltKey[-40/8:]
            return NewEncryptKey40
        if key_length == 56:
            NewEncryptKey56 = '\xD1' + PreSaltKey[-56/8:]
            return NewEncryptKey56

        # Finally, the associated RC4 substitution table is reinitialized with the new encryption key (NewEncryptKey128), which can then be used to encrypt a further 4,096 packets.


class MacHasher(object):
    # http://msdn.microsoft.com/en-us/library/cc240788 5.3.6.1 Non-FIPS
    pad1 = '\x36' * 40 # 320 bits
    pad2 = '\x5C' * 48 # 384 bits

    def __init__(self, MACKeyN):
        """
        MACKeyN is either MACKey40, MACKey56 or MACKey128, depending on the negotiated key strength.
        """
        self._MACKeyN = MACKeyN
        self._EncryptionCount = 0

    def hash(self, s, secure=False):
        """
        Calculate 8 bytes hash for Data.
        """
        # DataLength is the size of the data to encrypt in bytes, expressed as a little-endian 32-bit integer.
        sha_body = self._MACKeyN + self.pad1 + struct.pack('<L', len(s)) + s
        if secure:
            # http://msdn.microsoft.com/en-us/library/cc240789 5.3.6.1.1 Salted MAC Generation
            sha_body += struct.pack('<I', self._EncryptionCount)
        sha_component = SHA(sha_body)
        self._EncryptionCount += 1
        md5_hash = MD5(self._MACKeyN + self.pad2 + sha_component)
        # The first 8 bytes of the generated MD5 hash are used as an 8-byte MAC value to send on the wire.
        return md5_hash[:8]

def _test_keygen():
    # sec_generate_keys client_random
    client_random = util.undump("""
0000 55 15 be 99 01 c2 a0 0e 70 7b 62 f7 f5 aa 84 0f U.......p{b.....
0010 f5 45 f7 2f 6b 58 bd ba 4f 82 3c 77 29 bf 1b 26 .E./kX..O.<w)..&
""")

    # sec_generate_keys server_random
    server_random = util.undump("""
0000 ac 47 2a 6f bb f5 79 89 71 90 b3 35 4c 22 17 bc .G*o..y.q..5L"..
0010 97 81 9e 7b f6 11 4b 2b 18 33 5b cf 55 87 03 b9 ...{..K+.3[.U...
""")

    session_keys = SessionKeys(
        client_random,
        server_random,
        128,
        )
    InitialServerEncryptKey = session_keys.server_client_crypt.InitialKeyN
    InitialServerDecryptKey = session_keys.client_server_crypt.InitialKeyN
    MACKeyN = session_keys.MACKeyN

    #print 'InitialServerEncryptKey'; util.dump(InitialServerEncryptKey)
    #print 'InitialServerDecryptKey'; util.dump(InitialServerDecryptKey)

    # sec_generate_keys rc4 decrypt key
    rc4_decrypt_key = util.undump("""
0000 91 9a 94 f1 f4 9f b8 57 df b0 c8 11 ac 10 72 54 .......W......rT
""")

    # sec_generate_keys rc4 encrypt key
    rc4_encrypt_key = util.undump("""
0000 4a fb ce 5f af a3 0f 23 db 85 30 c4 5d f5 2e 13 J.._...#..0.]...
""")

    # FIXME: Not dumped, but this value fixes what seems to work
    mac_key = util.to_str((196, 92, 173, 214, 42, 244, 101, 97, 191, 136, 199, 28, 238, 122, 83, 43))

    assert InitialServerEncryptKey == rc4_decrypt_key, util.to_list(InitialServerEncryptKey)
    assert InitialServerDecryptKey == rc4_encrypt_key, util.to_list(InitialServerDecryptKey)
    assert MACKeyN == mac_key, util.to_list(MACKeyN)

def _smoke_test():
    for l in [40, 56, 128]:
        print 'Key length', l
        MACKeyN = SessionKeys('x'*32, 'y'*32, l).MACKeyN
        print 'MACKeyN'; util.dump(MACKeyN)
        hasher = MacHasher(MACKeyN)
        print 'hash("hund")'; util.dump(hasher.hash("hund"))
        print

if __name__ == '__main__':
    _test_keygen()
    _smoke_test()
