import rdp_crypto


class CommonConnectionState(object):
    """
    Session keys and their use is the same for client and server
    """

    def __init__(self, env):
        self._env = env

        self._session_keys = None

    def generate_session_keys(self, client_random, server_random, encryptionMethodKeyLength):
        # http://msdn.microsoft.com/en-us/library/cc240784 5.3.5 Initial Session Key Generation
        # http://msdn.microsoft.com/en-us/library/cc240785 5.3.5.1 Non-FIPS
        self._session_keys = rdp_crypto.SessionKeys(client_random, server_random, encryptionMethodKeyLength)
        self._env.log3('InitialServerEncryptKey:', dump=self._session_keys.server_client_crypt.InitialKeyN) # the key used by server to encrypt and by client to decrypt
        self._env.log3('InitialServerDecryptKey:', dump=self._session_keys.client_server_crypt.InitialKeyN) # the key used by server to decrypt and by client to encrypt
        self._env.log3('MACKeyN:', dump=self._session_keys.MACKeyN)

    def client_server_xor(self, s):
        return self._session_keys.client_server_crypt.xor(s)

    def server_client_xor(self, s):
        return self._session_keys.server_client_crypt.xor(s)

    def generate_mac(self, s, secure=False):
        # http://msdn.microsoft.com/en-us/library/cc240788 5.3.6.1 Non-FIPS [MAC]
        new_mac = self._session_keys.en_hasher.hash(s, secure=secure)
        self._env.log3('generate_mac:', dump=new_mac)
        return new_mac

    def check_mac(self, s, signature, secure=False):
        # http://msdn.microsoft.com/en-us/library/cc240788 5.3.6.1 Non-FIPS [MAC]
        check_mac = self._session_keys.de_hasher.hash(s, secure=secure)
        if check_mac != signature:
            self._env.log1('Header MAC, dataSignature:', dump=signature)
            self._env.log1('Calculated:', dump=check_mac)
            raise RuntimeError('Bad MAC!')
