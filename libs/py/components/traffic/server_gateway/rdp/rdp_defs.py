import util


FASTPATH_HEADER = util.PackedStruct('>BB')
FASTPATH_LENGTH2 = util.PackedStruct('>B')

# http://msdn.microsoft.com/en-us/library/cc240480 2.2.1.12.1.1 Licensing Preamble (LICENSE_PREAMBLE)
LICENSING_PREAMPLE = util.PackedStruct('<BBH')

class fpInputHeader_server_actioncode_enum(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240621 2.2.9.1.2 Server Fast-Path Update PDU (TS_FP_UPDATE_PDU)
    # Code indicating whether the PDU is in fast-path or slow-path format.
    FASTPATH_OUTPUT_ACTION_FASTPATH = 0x0 # Indicates that the PDU is a fast-path output PDU.
    FASTPATH_OUTPUT_ACTION_X224 = 0x3 # Indicates the presence of a TPKT Header (see [T123] section 8) initial version byte, which implies that the PDU is a slow-path output PDU (in this case the full value of the initial byte MUST be 0x03).

class fpInputHeader_server_encryptionFlags_flags(util.BitField):
    # http://msdn.microsoft.com/en-us/library/cc240621 2.2.9.1.2 Server Fast-Path Update PDU (TS_FP_UPDATE_PDU)
    # Flags describing cryptographic parameters of the PDU.
    FASTPATH_OUTPUT_SECURE_CHECKSUM = 0x1 # Indicates that the MAC signature for the PDU was generated using the "salted MAC generation" technique (see section 5.3.6.1.1). If this bit is not set, then the standard technique was used (see sections 2.2.8.1.1.2.2 and 2.2.8.1.1.2.3).
    FASTPATH_OUTPUT_ENCRYPTED = 0x2 # Indicates that the PDU contains an 8-byte MAC signature after the optional length2 field (that is, the dataSignature field is present), and the contents of the PDU are encrypted using the negotiated encryption package (see sections 5.3.2 and 5.3.6).

class FASTPATH_UPDATETYPEs(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240622 2.2.9.1.2.1 Fast-Path Update (TS_FP_UPDATE)
    FASTPATH_UPDATETYPE_ORDERS = 0x0 # Indicates a Fast-Path Orders Update (see [MS-RDPEGDI] section 2.2.2.3).
    FASTPATH_UPDATETYPE_BITMAP = 0x1 # Indicates a Fast-Path Bitmap Update (see section 2.2.9.1.2.1.2).
    FASTPATH_UPDATETYPE_PALETTE = 0x2 # Indicates a Fast-Path Palette Update (see section 2.2.9.1.2.1.1).
    FASTPATH_UPDATETYPE_SYNCHRONIZE = 0x3 # Indicates a Fast-Path Synchronize Update (see section 2.2.9.1.2.1.3).
    FASTPATH_UPDATETYPE_PTR_NULL = 0x5 # Indicates a Fast-Path System Pointer Hidden Update (see section 2.2.9.1.2.1.5).
    FASTPATH_UPDATETYPE_PTR_DEFAULT = 0x6 # Indicates a Fast-Path System Pointer Default Update (see section 2.2.9.1.2.1.6).
    FASTPATH_UPDATETYPE_PTR_POSITION = 0x8 # Indicates a Fast-Path Pointer Position Update (see section 2.2.9.1.2.1.4).
    FASTPATH_UPDATETYPE_COLOR = 0x9 # Indicates a Fast-Path Color Pointer Update (see section 2.2.9.1.2.1.7).
    FASTPATH_UPDATETYPE_CACHED = 0xA # Indicates a Fast-Path Cached Pointer Update (see section 2.2.9.1.2.1.9).
    FASTPATH_UPDATETYPE_POINTER = 0xB # Indicates a Fast-Path New Pointer Update (see section 2.2.9.1.2.1.8).

class FASTPATH_FRAGMENTs(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240622 2.2.9.1.2.1 Fast-Path Update (TS_FP_UPDATE)
    FASTPATH_FRAGMENT_SINGLE = 0x0 # The fast-path data in the updateData field is not part of a sequence of fragments.
    FASTPATH_FRAGMENT_LAST = 0x1 # The fast-path data in the updateData field contains the last fragment in a sequence of fragments.
    FASTPATH_FRAGMENT_FIRST = 0x2 # The fast-path data in the updateData field contains the first fragment in a sequence of fragments.
    FASTPATH_FRAGMENT_NEXT = 0x3 # The fast-path data in the updateData field contains the second or subsequent fragment in a sequence of fragments.

class FASTPATH_OUTPUT_COMPRESSION(util.BitField):
    # http://msdn.microsoft.com/en-us/library/cc240622 2.2.9.1.2.1 Fast-Path Update (TS_FP_UPDATE)
    FASTPATH_OUTPUT_COMPRESSION_USED = 0x2 # Indicates that the compressionFlags field is present.


class fpInputHeader_client_actioncode_enum(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240589 2.2.8.1.2 Client Fast-Path Input Event PDU (TS_FP_INPUT_PDU)
    # Code indicating whether the PDU is in fast-path or slow-path format.
    FASTPATH_INPUT_ACTION_FASTPATH = 0x0 # Indicates the PDU is a fast-path input PDU.
    FASTPATH_INPUT_ACTION_X224 = 0x3 # Indicates the presence of a TPKT Header initial version byte, which implies that the PDU is a slow-path input PDU (in this case the full value of the initial byte MUST be 0x03).

class fpInputHeader_client_encryptionFlags_flags(util.BitField):
    # http://msdn.microsoft.com/en-us/library/cc240589 2.2.8.1.2 Client Fast-Path Input Event PDU (TS_FP_INPUT_PDU)
    # Flags describing cryptographic parameters of the PDU.
    FASTPATH_INPUT_SECURE_CHECKSUM = 0x1 # Indicates that the MAC signature for the PDU was generated using the "salted MAC generation" technique (see section 5.3.6.1.1). If this bit is not set, then the standard technique was used (see sections 2.2.8.1.1.2.2 and 2.2.8.1.1.2.3).
    FASTPATH_INPUT_ENCRYPTED = 0x2 # Indicates that the PDU contains an 8-byte MAC signature after the optional length2 field (that is, the dataSignature field is present) and the contents of the PDU are encrypted using the negotiated encryption package (see sections 5.3.2 and 5.3.6).

class TS_FP_INPUT_PDU_actionCode(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240589 2.2.8.1.2 Client Fast-Path Input Event PDU (TS_FP_INPUT_PDU)
    FASTPATH_INPUT_ACTION_FASTPATH = 0x0 # Indicates the PDU is a fast-path input PDU.
    FASTPATH_INPUT_ACTION_X224 = 0x3 # Indicates the presence of a TPKT Header initial version byte, which implies that the PDU is a slow-path input PDU (in this case the full value of the initial byte MUST be 0x03).

class TS_FP_INPUT_PDU_encryptionFlags(util.BitField):
    # http://msdn.microsoft.com/en-us/library/cc240589 2.2.8.1.2 Client Fast-Path Input Event PDU (TS_FP_INPUT_PDU)
    FASTPATH_INPUT_SECURE_CHECKSUM = 0x1 # Indicates that the MAC signature for the PDU was generated using the "salted MAC generation" technique (see section 5.3.6.1.1). If this bit is not set, then the standard technique was used (see sections 2.2.8.1.1.2.2 and 2.2.8.1.1.2.3).
    FASTPATH_INPUT_ENCRYPTED = 0x2 # Indicates that the PDU contains an 8-byte MAC signature after the optional length2 field (that is, the dataSignature field is present) and the contents of the PDU are encrypted using the negotiated encryption package (see sections 5.3.2 and 5.3.6).

class TS_FP_INPUT_EVENT_eventCode(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240589 2.2.8.1.2 Client Fast-Path Input Event PDU (TS_FP_INPUT_PDU)
    FASTPATH_INPUT_EVENT_SCANCODE = 0x0 # Indicates a Fast-Path Keyboard Event (section 2.2.8.1.2.2.1).
    FASTPATH_INPUT_EVENT_MOUSE = 0x1 # Indicates a Fast-Path Mouse Event (section 2.2.8.1.2.2.3).
    FASTPATH_INPUT_EVENT_MOUSEX = 0x2 # Indicates a Fast-Path Extended Mouse Event (section 2.2.8.1.2.2.4).
    FASTPATH_INPUT_EVENT_SYNC = 0x3 # Indicates a Fast-Path Synchronize Event (section 2.2.8.1.2.2.5).
    FASTPATH_INPUT_EVENT_UNICODE = 0x4 # Indicates a Fast-Path Unicode Keyboard Event (section 2.2.8.1.2.2.2).

class TS_SECURITY_HEADER_flags(util.BitField):
    # http://msdn.microsoft.com/en-us/library/cc240579 2.2.8.1.1.2.1 Basic (TS_SECURITY_HEADER)
    # The TS_SECURITY_HEADER structure is attached to server-to-client traffic when the Encryption Level selected by the server (see sections 5.3.2 and 2.2.1.4.3) is ENCRYPTION_LEVEL_LOW (1).
    # flags (2 bytes): A 16-bit, unsigned integer. The information flags describing properties of the attached data.
    SEC_EXCHANGE_PKT = 0x0001 # Indicates that the packet is a Security Exchange PDU (section 2.2.1.10). This packet type is sent from client to server only. The client only sends this packet if it will be encrypting further communication and Standard RDP Security mechanisms (see section 5.3) are in effect.
    SEC_TRANSPORT_REQ = 0x0002 # Indicates that the packet is an Inititiate Multitransport Request PDU (section 2.2.15.1).
    RDP_SEC_TRANSPORT_RSP = 0x0004 # Indicates that the packet is an Inititiate Multitransport Error PDU (section 2.2.15.2).
    SEC_ENCRYPT = 0x0008 # Indicates that encryption is being used for the packet.
    SEC_RESET_SEQNO = 0x0010 # This flag is set for legacy reasons when the packet is a Confirm Active PDU (section 2.2.1.13.2). Otherwise this flag is never used.
    SEC_IGNORE_SEQNO = 0x0020 # This flag is set for legacy reasons when the packet is a Confirm Active PDU or a Client Synchronize PDU (section 2.2.1.14). Otherwise this flag is never used.
    SEC_INFO_PKT = 0x0040 # Indicates that the packet is a Client Info PDU (section 2.2.1.11). This packet type is sent from client to server only. If Standard RDP Security mechanisms are in effect, then this packet MUST also be encrypted.
    SEC_LICENSE_PKT = 0x0080 # Indicates that the packet is a Licensing PDU (section 2.2.1.12).
    SEC_LICENSE_ENCRYPT_CS = 0x0200 # Indicates to the client that the server is capable of processing encrypted licensing packets. It is sent by the server together with any licensing PDUs it may send (see section 2.2.1.12).
    SEC_LICENSE_ENCRYPT_SC = 0x0200 # Indicates to the server that the client is capable of processing encrypted licensing packets. It is sent by the client together with the SEC_EXCHANGE_PKT flag when sending a Security Exchange PDU (section 2.2.1.10).
    SEC_REDIRECTION_PKT = 0x0400 # Indicates that the packet is a Standard Security Server Redirection PDU (see [MS-RDPEGDI] section 2.2.3.2.1). The presence of this flag implies that the PDU is encrypted, that is, the SEC_ENCRYPT (0x0008) flag MUST be considered to be set.
    SEC_SECURE_CHECKSUM = 0x0800 # Indicates that the MAC for the PDU was generated using the "salted MAC generation" technique (see section 5.3.6.1.1). If this flag is not present, then the standard technique was used (see Non-FIPS (section 2.2.8.1.1.2.2) and FIPS (section 2.2.8.1.1.2.3)).
    SEC_AUTODETECT_REQ = 0x1000 # Indicates that the autoDetectReqData field is present. This flag MUST NOT be present if the PDU containing the security header is being sent from client to server.
    SEC_AUTODETECT_RSP = 0x2000 # Indicates that the packet is an Auto-Detect Response PDU (2.2.14.2).
    SEC_FLAGSHI_VALID = 0x8000 # Indicates that the flagsHi field contains valid data. If this flag is not set, then the contents of the flagsHi field should be ignored.

class TS_INFO_PACKET_flags(util.BitField):
    # http://msdn.microsoft.com/en-us/library/cc240475 2.2.1.11.1.1 Info Packet (TS_INFO_PACKET)
    INFO_MOUSE = 0x00000001 # Indicates that the client machine has a mouse attached.
    INFO_DISABLECTRLALTDEL = 0x00000002 # Indicates that the CTRL+ALT+DEL (or the equivalent) secure access keyboard sequence is not required at the logon prompt.
    INFO_AUTOLOGON = 0x00000008 # The client requests auto logon using the included user name, password and domain.
    INFO_UNICODE = 0x00000010 # Indicates that the character set for strings in the Info Packet is Unicode. If this flag is absent, the character set is ANSI. The presence of this flag affects the meaning of the CodePage field in the Info Packet structure.
    INFO_MAXIMIZESHELL = 0x00000020 # Specifies whether the alternate shell (specified in the AlternateShell field of the Info Packet structure) should be started in a maximized state.
    INFO_LOGONNOTIFY = 0x00000040 # Indicates that the client wants to be informed of the user name and domain used to log on to the server, as well as the ID of the session to which the user connected. The Save Session Info PDU (section 2.2.10.1) is sent from the server to notify the client of this information using a Logon Info Version 1 (section 2.2.10.1.1.1) or Logon Info Version 2 (section 2.2.10.1.1.2) structure.
    INFO_COMPRESSION = 0x00000080 # Indicates that the CompressionTypeMask is valid and contains the highest compression package type supported by the client.
    INFO_ENABLEWINDOWSKEY = 0x00000100 # Indicates that the client uses the Windows key on Windows-compatible keyboards.
    INFO_REMOTECONSOLEAUDIO = 0x00002000 # Requests that any audio played in a remote session be played on the remote computer (see [MS-RDPEA]).
    INFO_FORCE_ENCRYPTED_CS_PDU = 0x00004000 # Indicates that the client will only send encrypted packets to the server if encryption is in force. Setting this flag prevents the server from processing unencrypted packets in man-in-the-middle attack scenarios. This flag is only understood by RDP 5.2 and later servers.
    INFO_RAIL = 0x00008000 # Indicates that the remote connection being established is for the purpose of launching remote programs (see Section [MS-RDPERP]). This flag is only understood by RDP 6.0 and later servers.
    INFO_LOGONERRORS = 0x00010000 # Indicates a request for logon error notifications using the Save Session Info PDU. This flag is only understood by RDP 6.0 and later servers.
    INFO_MOUSE_HAS_WHEEL = 0x00020000 # Indicates that the mouse which is connected to the client machine has a scroll wheel. This flag is only understood by RDP 6.0 and later servers.
    INFO_PASSWORD_IS_SC_PIN = 0x00040000 # Indicates that the Password field in the Info Packet contains a smart card personal identification number (PIN). This flag is only understood by RDP 6.0 and later servers.
    INFO_NOAUDIOPLAYBACK = 0x00080000 # Indicates that no audio redirection or playback (see [MS-RDPEA]) should take place. This flag is only understood by RDP 6.0 and later servers.
    INFO_USING_SAVED_CREDS = 0x00100000 # Any user credentials sent on the wire during the RDP Connection Sequence (see sections 1.3.1.1 and 1.3.1.2) have been retrieved from a credential store and were not obtained directly from the user.

    CompressionTypeMask = 0x00001E00 # = 0xf << 9 # Indicates the highest compression package type supported. See the discussion which follows this table for more information.
    PACKET_COMPR_TYPE_8K = 0 << 9# MPPC 8K compression (see MPPC-8K section 3.1.8.4.1).
    PACKET_COMPR_TYPE_64K = 1 << 9# MPPC 64K compression (see MPPC-64K section 3.1.8.4.2).
    PACKET_COMPR_TYPE_RDP6 = 2 << 9# RDP 6.0 bulk compression (see [MS-RDPEGDI] section 3.1.8.1).
    PACKET_COMPR_TYPE_RDP61 = 3 << 9# RDP 6.1 bulk compression (see [MS-RDPEGDI] section 3.1.8.2).

class TS_EXTENDED_INFO_PACKET_performanceFlags(util.BitField):
    # http://msdn.microsoft.com/en-us/library/cc240476 2.2.1.11.1.1.1 Extended Info Packet (TS_EXTENDED_INFO_PACKET)
    PERF_DISABLE_WALLPAPER = 0x00000001 # Disable desktop wallpaper.
    PERF_DISABLE_FULLWINDOWDRAG = 0x00000002 # Disable full-window drag (only the window outline is displayed when the window is moved).
    PERF_DISABLE_MENUANIMATIONS = 0x00000004 # Disable menu animations.
    PERF_DISABLE_THEMING = 0x00000008 # Disable user interface themes.
    PERF_RESERVED1 = 0x00000010 # Reserved for future use.
    PERF_DISABLE_CURSOR_SHADOW = 0x00000020 # Disable mouse cursor shadows.
    PERF_DISABLE_CURSORSETTINGS = 0x00000040 # Disable cursor blinking.
    PERF_ENABLE_FONT_SMOOTHING = 0x00000080 # Enable font smoothing.
    PERF_ENABLE_DESKTOP_COMPOSITION = 0x00000100 # Enable Desktop Composition.
    PERF_RESERVED2 = 0x80000000 # Reserved for future use.

class TS_UD_HEADER_type(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240509 2.2.1.3.1 User Data Header (TS_UD_HEADER)
    CS_CORE = 0xC001 # The data block that follows contains Client Core Data (section 2.2.1.3.2).
    CS_SECURITY = 0xC002 # The data block that follows contains Client Security Data (section 2.2.1.3.3).
    CS_NET = 0xC003 # The data block that follows contains Client Network Data (section 2.2.1.3.4).
    CS_CLUSTER = 0xC004 # The data block that follows contains Client Cluster Data (section 2.2.1.3.5).
    CS_MONITOR = 0xC005 # The data block that follows contains Client Monitor Data (section 2.2.1.3.6).
    SC_CORE = 0x0C01 # The data block that follows contains Server Core Data (section 2.2.1.4.2).
    SC_SECURITY = 0x0C02 # The data block that follows contains Server Security Data (section 2.2.1.4.3).
    SC_NET = 0x0C03 # The data block that follows contains Server Network Data (section 2.2.1.4.4).

class RDP_NEG_REQ_requestedProtocols(util.EnumField): # Note: this is partly a BitField, where some some combinations are enum-like ...
    # http://msdn.microsoft.com/en-us/library/cc240500 2.2.1.1.1 RDP Negotiation Request (RDP_NEG_REQ)
    PROTOCOL_RDP_FLAG = 0 # Legacy RDP encryption.
    PROTOCOL_SSL_FLAG = 1 # TLS 1.0 (section 5.4.5.1).
    PROTOCOL_HYBRID_FLAG_NO_SSL = 2 # If this flag is set, then the PROTOCOL_SSL_FLAG (0x00000001) SHOULD also be set because Transport Layer Security (TLS) is a subset of CredSSP.
    PROTOCOL_HYBRID_FLAG = 1 + 2 # Credential Security Support Provider protocol (CredSSP) (section 5.4.5.2).

class TS_UD_SC_SEC1_encryptionMethod(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240518 2.2.1.4.3 Server Security Data (TS_UD_SC_SEC1)
    ENCRYPTION_METHOD_NONE = 0x00000000 # No encryption or message authentication codes (MACs) will be used.
    ENCRYPTION_METHOD_40BIT = 0x00000001 # 40-bit session keys will be used to encrypt data (with RC4) and generate MACs.
    ENCRYPTION_METHOD_128BIT = 0x00000002 # 128-bit session keys will be used to encrypt data (with RC4) and generate MACs.
    ENCRYPTION_METHOD_56BIT = 0x00000008 # 56-bit session keys will be used to encrypt data (with RC4) and generate MACs.
    ENCRYPTION_METHOD_FIPS = 0x00000010 # All encryption and message authentication code generation routines will be FIPS 140-1 compliant.

class TS_UD_SC_SEC1_encryptionLevel(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240518 2.2.1.4.3 Server Security Data (TS_UD_SC_SEC1)
    # 5.3.6 Encrypting and Decrypting the I/O Data Stream
    ENCRYPTION_LEVEL_NONE = 0x00000000 # Enhanced RDP Security (section 5.4)
    ENCRYPTION_LEVEL_LOW = 0x00000001 # All data sent from the client to the server is protected by encryption based on the maximum key strength supported by the client.
    ENCRYPTION_LEVEL_CLIENT_COMPATIBLE = 0x00000002 # All data sent between the client and the server is protected by encryption based on the maximum key strength supported by the client.
    ENCRYPTION_LEVEL_HIGH = 0x00000003 # All data sent between the client and server is protected by encryption based on the server's maximum key strength. Clients that do not support this level of encryption cannot connect.
    ENCRYPTION_LEVEL_FIPS = 0x00000004 # All data sent between the client and server is protected using Federal Information Processing Standard 140-1 validated encryption methods. Clients that do not support this level of encryption cannot connect.

class RNS_UD_SC_earlyCapabilityFlags(util.BitField):
    # http://msdn.microsoft.com/en-us/library/cc240517 2.2.1.4.2 Server Core Data (TS_UD_SC_CORE)
    RNS_UD_SC_EDGE_ACTIONS_SUPPORTED = 0x00000001 # reserved key combinations and monitor boundaries
    RNS_UD_SC_DYNAMIC_DST_SUPPORTED = 0x00000002 # Indicates that the server supports Dynamic DST.

class SERVER_CERTIFICATE_dwVersion(util.BitField):
    # http://msdn.microsoft.com/en-us/library/cc240521 2.2.1.4.3.1 Server Certificate (SERVER_CERTIFICATE)
    CERT_CHAIN_VERSION_1 = 0x00000001 # The certificate contained in the certData field is a Server Proprietary Certificate (section 2.2.1.4.3.1.1).
    CERT_CHAIN_VERSION_2 = 0x00000002 # The certificate contained in the certData field is an X.509 Certificate (see section 5.3.3.2).
    TEMP = 0x80000000 # A 1-bit field. Indicates whether the certificate has been temporarily issued to the server.

class TS_UD_clientVersions(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240510 2.2.1.3.2 Client Core Data (TS_UD_CS_CORE)
    # http://msdn.microsoft.com/en-us/library/cc240517 2.2.1.4.2 Server Core Data (TS_UD_SC_CORE)
    defs = {
        0x00080001: 'RDP 4.0 clients',
        0x00080004: 'RDP 5.0, 5.1, 5.2, and 6.0 clients',
        }

class TS_UD_CS_CORE_ColorDepth(util.EnumField): # (2 bytes): A 16-bit, unsigned integer. The requested color depth. Values in this field MUST be ignored if the postBeta2ColorDepth field is present.
    # http://msdn.microsoft.com/en-us/library/cc240510 2.2.1.3.2 Client Core Data (TS_UD_CS_CORE)
    # both colorDepth and postBeta2ColorDepth
    RNS_UD_COLOR_4BPP = 0xCA00 # 4 bits-per-pixel (bpp)
    RNS_UD_COLOR_8BPP = 0xCA01 # 8 bpp

class TS_UD_CS_CORE_keyboardType(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240510 2.2.1.3.2 Client Core Data (TS_UD_CS_CORE)
    defs = {
        1: 'IBM PC/XT or compatible (83-key) keyboard',
        2: 'Olivetti "ICO" (102-key) keyboard',
        3: 'IBM PC/AT (84-key) and similar keyboards',
        4: 'IBM enhanced (101- or 102-key) keyboard',
        5: 'Nokia 1050 and similar keyboards',
        6: 'Nokia 9140 and similar keyboards',
        7: 'Japanese keyboard',
        }

class TS_UD_CS_CORE_highColorDepth(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240510 2.2.1.3.2 Client Core Data (TS_UD_CS_CORE)
    defs = {
        4: '4 bpp',
        8: '8 bpp',
        15: '15-bit 555 RGB mask (5 bits for red, 5 bits for green, and 5 bits for blue)',
        16: '16-bit 565 RGB mask (5 bits for red, 6 bits for green, and 5 bits for blue)',
        24: '24-bit RGB mask (8 bits for red, 8 bits for green, and 8 bits for blue)',
        }

class TS_UD_CS_CORE_supportedColorDepths(util.BitField):
    # http://msdn.microsoft.com/en-us/library/cc240510 2.2.1.3.2 Client Core Data (TS_UD_CS_CORE)
    RNS_UD_24BPP_SUPPORT = 0x0001 # 24-bit RGB mask (8 bits for red, 8 bits for green, and 8 bits for blue)
    RNS_UD_16BPP_SUPPORT = 0x0002 # 16-bit 565 RGB mask (5 bits for red, 6 bits for green, and 5 bits for blue)
    RNS_UD_15BPP_SUPPORT = 0x0004 # 15-bit 555 RGB mask (5 bits for red, 5 bits for green, and 5 bits for blue) (aka RNS_UD_32BPP_SUPPORT!)
    RNS_UD_32BPP_SUPPORT = 0x0008 # 32-bit RGB mask (8 bits for the alpha channel, 8 bits for red, 8 bits for green, and 8 bits for blue)

class TS_UD_CS_CORE_earlyCapabilityFlags(util.BitField):
    # http://msdn.microsoft.com/en-us/library/cc240510 2.2.1.3.2 Client Core Data (TS_UD_CS_CORE)
    RNS_UD_CS_SUPPORT_ERRINFO_PDU = 0x0001 # Indicates that the client supports the Set Error Info PDU (section 2.2.5.1).
    RNS_UD_CS_WANT_32BPP_SESSION = 0x0002 # Indicates that the client is requesting a session color depth of 32 bpp. This flag is necessary because the highColorDepth field does not support a value of 32. If this flag is set, the highColorDepth field SHOULD be set to 24 to provide an acceptable fallback for the scenario where the server does not support 32 bpp color.
    RNS_UD_CS_SUPPORT_STATUSINFO_PDU = 0x0004 # Indicates that the client supports the Server Status Info PDU (section 2.2.5.2).
    RNS_UD_CS_STRONG_ASYMMETRIC_KEYS = 0x0008 # Indicates that the client supports asymmetric keys larger than 512 bits for use with the Server Certificate (section 2.2.1.4.3.1) sent in the Server Security Data block (section 2.2.1.4.3).
    RNS_UD_CS_VALID_CONNECTION_TYPE = 0x0020 # Indicates that the connectionType field contains valid data.
    RNS_UD_CS_SUPPORT_MONITOR_LAYOUT_PDU = 0x0040 # Indicates that the client supports the Monitor Layout PDU (section 2.2.12.1).

class TS_UD_CS_CORE_connectionType(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240510 2.2.1.3.2 Client Core Data (TS_UD_CS_CORE)
    CONNECTION_TYPE_UNKNOWN = 0x00
    CONNECTION_TYPE_MODEM = 0x01 # Modem (56 Kbps)
    CONNECTION_TYPE_BROADBAND_LOW = 0x02 # Low-speed broadband (256 Kbps - 2 Mbps)
    CONNECTION_TYPE_SATELLITE = 0x03 # Satellite (2 Mbps - 16 Mbps with high latency)
    CONNECTION_TYPE_BROADBAND_HIGH = 0x04 # High-speed broadband (2 Mbps - 10 Mbps)
    CONNECTION_TYPE_WAN = 0x05 # WAN (10 Mbps or higher with high latency)
    CONNECTION_TYPE_LAN = 0x06 # LAN (10 Mbps or higher)

class RDP_NEG_RSP_flags(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240506 2.2.1.2.1 RDP Negotiation Response (RDP_NEG_RSP)
    NO_EXTENDED_CLIENT_DATA_SUPPORTED = 0 # assume this is implied ...
    EXTENDED_CLIENT_DATA_SUPPORTED = 0x00000001 # The server supports extended client data blocks in the GCC Conference Create Request user data (section 2.2.1.3).

class RDP_NEG_PROTOCOL(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240506 2.2.1.2.1 RDP Negotiation Response (RDP_NEG_RSP)
    PROTOCOL_RDP = 0
    PROTOCOL_SSL = 1
    PROTOCOL_HYBRID = 2

class RDP_NEG_FAILURE(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240507 2.2.1.2.2 RDP Negotiation Failure (RDP_NEG_FAILURE)
    SSL_REQUIRED_BY_SERVER = 0x00000001 # The server requires that the client support Enhanced RDP Security (section 5.4) with either TLS 1.0 (section 5.4.5.1) or CredSSP (section 5.4.5.2). If only CredSSP was requested then the server only supports TLS.
    SSL_NOT_ALLOWED_BY_SERVER = 0x00000002 # The server is configured to only use Standard RDP Security mechanisms (section 5.3) and does not support any External Security Protocols (section 5.4.5).
    SSL_CERT_NOT_ON_SERVER = 0x00000003 # The server does not possess a valid server authentication certificate and cannot initialize the External Security Protocol Provider (section 5.4.5).
    INCONSISTENT_FLAGS = 0x00000004 # The list of requested security protocols is not consistent with the current security protocol in effect. This error is only possible when the Direct Approach (see sections 5.4.2.2 and 1.3.1.2) is used and an External Security Protocol (section 5.4.5) is already being used.
    HYBRID_REQUIRED_BY_SERVER = 0x00000005 # The server requires that the client support Enhanced RDP Security (section 5.4) with CredSSP (section 5.4.5.2).

class TS_UD_CS_SEC_encryptionMethods(util.BitField):
    # http://msdn.microsoft.com/en-us/library/cc240511 2.2.1.3.3 Client Security Data (TS_UD_CS_SEC)
    ENCRYPTION_FLAG_40BIT = 0x00000001 # 40-bit session keys should be used to encrypt data (with RC4) and generate message authentication codes (MAC).
    ENCRYPTION_FLAG_128BIT = 0x00000002 # 128-bit session keys should be used to encrypt data (with RC4) and generate MACs.
    ENCRYPTION_FLAG_56BIT = 0x00000008 # 56-bit session keys should be used to encrypt data (with RC4) and generate MACs.
    FIPS_ENCRYPTION_FLAG = 0x00000010 # All encryption and message authentication code generation routines should be Federal Information Processing Standard (FIPS) 140-1 compliant.

# http://msdn.microsoft.com/en-us/library/cc240513 2.2.1.3.4.1 Channel Definition Structure (CHANNEL_DEF)
class CHANNEL_DEF_options(util.BitField):
    INITIALIZED = 0x80000000 # Absence of this flag indicates that this channel is a placeholder and that the server should not set it up.
    ENCRYPT_RDP = 0x40000000 # Channel data should be encrypted in the same way as RDP input and output data.
    ENCRYPT_SC = 0x20000000 # Server-to-client data should be encrypted. This flag is ignored if ENCRYPT_RDP is set.
    ENCRYPT_CS = 0x10000000 # Client-to-server data should be encrypted. This flag is ignored if ENCRYPT_RDP is set.
    PRI_HIGH = 0x08000000 # Channel data should be sent with high MCS priority.
    PRI_MED = 0x04000000 # Channel data should be sent with medium MCS priority.
    PRI_LOW = 0x02000000 # Channel data should be sent with low MCS priority.
    COMPRESS_RDP = 0x00800000 # Virtual channel data should be compressed if RDP data is being compressed.
    COMPRESS = 0x00400000 # Virtual channel data should be compressed, regardless of RDP compression.
    SHOW_PROTOCOL = 0x00200000 # Server virtual channel add-ins should be shown the full virtual channel packet header on receipt of data. If this option is not set, the server add-ins receive only the data stream without headers.
    REMOTE_CONTROL_PERSISTENT = 0x00100000 # Channel is persistent across remote control transactions.

class TS_UD_CS_CLUSTER_flags(util.BitField):
    # http://msdn.microsoft.com/en-us/library/cc240514 2.2.1.3.5 Client Cluster Data (TS_UD_CS_CLUSTER_flags)
    REDIRECTION_SUPPORTED = 0x00000001 # The client can receive server session redirection packets. If this flag is set, the ServerSessionRedirectionVersionMask will contain the server session redirection version that the client supports.
    REDIRECTED_SESSIONID_FIELD_VALID = 0x00000002 # The RedirectedSessionID field contains a valid session identifier to which the client wants to connect.
    REDIRECTED_SMARTCARD = 0x00000040 # The client logged on with a smart card.

class TS_UD_CS_CLUSTER_version(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240514 2.2.1.3.5 Client Cluster Data (TS_UD_CS_CLUSTER_flags)
    ServerSessionRedirectionVersionMask = 0x0000003C # The server session redirection version that the client supports. See the discussion which follows this table for more information.
    # ServerSessionRedirectionVersionMask 0x3c = 00111100 = 0xf << 2
    REDIRECTION_VERSION3 = 0x02 << 2 # If REDIRECTION_SUPPORTED is set, server session redirection version 3 is supported by the client.
    REDIRECTION_VERSION4 = 0x03 << 2 # If REDIRECTION_SUPPORTED is set, server session redirection version 4 is supported by the client.
    REDIRECTION_VERSION5 = 0x04 << 2 # If REDIRECTION_SUPPORTED is set, server session redirection version 4 is supported by the client.

class TS_SHARECONTROLHEADER_PDUTYPEs(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240576 2.2.8.1.1.1.1 Share Control Header (TS_SHARECONTROLHEADER)
    PDUTYPE_DEMANDACTIVEPDU = 1 # Demand Active PDU (section 2.2.1.13.1).
    PDUTYPE_CONFIRMACTIVEPDU = 3 # Confirm Active PDU (section 2.2.1.13.2).
    PDUTYPE_DEACTIVATEALLPDU = 6 # Deactivate All PDU (section 2.2.3.1).
    PDUTYPE_DATAPDU = 7 # Data PDU (actual type is revealed by the pduType2 field in the Share Data Header (section 2.2.8.1.1.1.2) structure).
    PDUTYPE_SERVER_REDIR_PKT = 0xA # Enhanced Security Server Redirection PDU (see [MS-RDPEGDI] section 2.2.3.3.1).

class TS_SHAREDATAHEADER_type(util.BitField):
    # http://msdn.microsoft.com/en-us/library/cc240577 2.2.8.1.1.1.2 Share Data Header (TS_SHAREDATAHEADER)
    #STREAM_UNDEFINED = 0x00 # Undefined stream priority. This value might be used in the Server Synchronize PDU (see section 2.2.1.19) due to a server-side RDP bug. It MUST not be used in conjunction with any other PDUs.
    STREAM_LOW = 0x01 # Low-priority stream.
    STREAM_MED = 0x02 # Medium-priority stream.
    STREAM_HI = 0x04 # High-priority stream.

class TS_SHAREDATAHEADER_PDUTYPE2(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240577 2.2.8.1.1.1.2 Share Data Header (TS_SHAREDATAHEADER)
    PDUTYPE2_UPDATE = 2 # Update PDU (section 2.2.9.1.1.3)
    PDUTYPE2_CONTROL = 20 # Control PDU (section 2.2.1.15.1)
    PDUTYPE2_POINTER = 27 # Pointer Update PDU (section 2.2.9.1.1.4)
    PDUTYPE2_INPUT = 28 # Input PDU (section 2.2.8.1.1.3)
    PDUTYPE2_SYNCHRONIZE = 31 # Synchronize PDU (section 2.2.1.14.1)
    PDUTYPE2_REFRESH_RECT = 33 # Refresh Rect PDU (section 2.2.11.2.1)
    PDUTYPE2_PLAY_SOUND = 34 # Play Sound PDU (section 2.2.9.1.1.5.1)
    PDUTYPE2_SUPPRESS_OUTPUT = 35 # Suppress Output PDU (section 2.2.11.3.1)
    PDUTYPE2_SHUTDOWN_REQUEST = 36 # Shutdown Request PDU (section 2.2.2.2.1)
    PDUTYPE2_SHUTDOWN_DENIED = 37 # Shutdown Request Denied PDU (section 2.2.2.3.1)
    PDUTYPE2_SAVE_SESSION_INFO = 38 # Save Session Info PDU (section 2.2.10.1.1)
    PDUTYPE2_FONTLIST = 39 # Font List PDU (section 2.2.1.18.1)
    PDUTYPE2_FONTMAP = 40 # Font Map PDU (section 2.2.1.22.1)
    PDUTYPE2_SET_KEYBOARD_INDICATORS = 41 # Set Keyboard Indicators PDU (section 2.2.8.2.1.1)
    PDUTYPE2_BITMAPCACHE_PERSISTENT_LIST = 43 # Persistent Key List PDU (section 2.2.1.17.1)
    PDUTYPE2_BITMAPCACHE_ERROR_PDU = 44 # Bitmap Cache Error PDU (see [MS-RDPEGDI] section 2.2.2.3.1)
    PDUTYPE2_SET_KEYBOARD_IME_STATUS = 45 # Set Keyboard IME Status PDU (section 2.2.8.2.2.1)
    PDUTYPE2_OFFSCRCACHE_ERROR_PDU = 46 # Offscreen Bitmap Cache Error PDU (see [MS-RDPEGDI] section 2.2.2.3.2)
    PDUTYPE2_SET_ERROR_INFO_PDU = 47 # Set Error Info PDU (section 2.2.5.1.1)
    PDUTYPE2_DRAWNINEGRID_ERROR_PDU = 48 # DrawNineGrid Cache Error PDU (see [MS-RDPEGDI] section 2.2.2.3.3)
    PDUTYPE2_DRAWGDIPLUS_ERROR_PDU = 49 # GDI+ Error PDU (see [MS-RDPEGDI] section 2.2.2.3.4)
    PDUTYPE2_ARC_STATUS_PDU = 50 # Auto-Reconnect Status PDU (section 2.2.4.1.1)
    PDUTYPE2_STATUS_INFO_PDU = 0x36 # Status Info PDU (section 2.2.5.2 )
    PDUTYPE2_MONITOR_LAYOUT_PDU = 0x37 # Monitor Layout PDU (section 2.2.12.1 )

class TS_SHAREDATAHEADER_PACKET_COMPRESSION(util.BitField):
    # http://msdn.microsoft.com/en-us/library/cc240577 2.2.8.1.1.1.2 Share Data Header (TS_SHAREDATAHEADER)
    PACKET_COMPRESSED = 0x20 # The payload data is compressed. This value corresponds to MPPC bit C (see [RFC2118] section 3.1).
    PACKET_AT_FRONT = 0x40 # The decompressed packet MUST be placed at the beginning of the history buffer. This value corresponds to MPPC bit B (see [RFC2118] section 3.1).
    PACKET_FLUSHED = 0x80 # The history buffer MUST be reinitialized. This value corresponds to MPPC bit A (see [RFC2118] section 3.1).

class TS_SHAREDATAHEADER_PACKET_COMPR_TYPE(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240577 2.2.8.1.1.1.2 Share Data Header (TS_SHAREDATAHEADER)
    PACKET_COMPR_TYPE_8K = 0 # MPPC-8K compression (see section 3.1.8.4.1)
    PACKET_COMPR_TYPE_64K = 1 # MPPC-64K compression (see section 3.1.8.4.2)
    PACKET_COMPR_TYPE_RDP6 = 2 # RDP 6.0 bulk compression (see [MS-RDPEGDI] section 3.1.8.1).
    PACKET_COMPR_TYPE_RDP61 = 2 # RDP 6.1 bulk compression (see [MS-RDPEGDI] section 3.1.8.2).

class TS_INPUT_EVENT_type(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240583 2.2.8.1.1.3.1.1 Slow-Path Input Event (TS_INPUT_EVENT)
    INPUT_EVENT_SYNC = 0x0000 # Indicates a Synchronize Event (section 2.2.8.1.1.3.1.1.5).
    INPUT_EVENT_SCANCODE = 0x0004 # Indicates a Keyboard Event (section 2.2.8.1.1.3.1.1.1).
    INPUT_EVENT_UNICODE = 0x0005 # Indicates a Unicode Keyboard Event (section 2.2.8.1.1.3.1.1.2).
    INPUT_EVENT_MOUSE = 0x8001 # Indicates a Mouse Event (section 2.2.8.1.1.3.1.1.3).
    INPUT_EVENT_MOUSEX = 0x8002 # Indicates an Extended Mouse Event (section 2.2.8.1.1.3.1.1.4).

class TS_KEYBOARD_EVENT_flags(util.BitField):
    # http://msdn.microsoft.com/en-us/library/cc240584 2.2.8.1.1.3.1.1.1 Keyboard Event (TS_KEYBOARD_EVENT)
    KBDFLAGS_EXTENDED = 0x0100 # The keystroke message contains an extended scancode. For enhanced 101-key and 102-key keyboards, extended keys include the right ALT and right CTRL keys on the main section of the keyboard; the INS, DEL, HOME, END, PAGE UP, PAGE DOWN and ARROW keys in the clusters to the left of the numeric keypad; and the Divide ("/") and ENTER keys in the numeric keypad.
    KBDFLAGS_DOWN = 0x4000 # Indicates that the key was down prior to this event.
    KBDFLAGS_RELEASE = 0x8000 # The absence of this flag indicates a key-down event, while its presence indicates a key-release event.

class TS_CAPS_SET_type(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240486 2.2.1.13.1.1.1 Capability Set (TS_CAPS_SET)
    CAPSTYPE_GENERAL = 1 # General Capability Set (section 2.2.7.1.1)
    CAPSTYPE_BITMAP = 2 # Bitmap Capability Set (section 2.2.7.1.2)
    CAPSTYPE_ORDER = 3 # Order Capability Set (section 2.2.7.1.3)
    CAPSTYPE_BITMAPCACHE = 4 # Revision 1 Bitmap Cache Capability Set (section 2.2.7.1.4.1)
    CAPSTYPE_CONTROL = 5 # Control Capability Set (section 2.2.7.2.2)
    CAPSTYPE_ACTIVATION = 7 # Window Activation Capability Set (section 2.2.7.2.3)
    CAPSTYPE_POINTER = 8 # Pointer Capability Set (section 2.2.7.1.5)
    CAPSTYPE_SHARE = 9 # Share Capability Set (section 2.2.7.2.4)
    CAPSTYPE_COLORCACHE = 10 # Color Table Cache Capability Set (see [MS-RDPEGDI] section 2.2.1.1)
    CAPSTYPE_SOUND = 12 # Sound Capability Set (section 2.2.7.1.11)
    CAPSTYPE_INPUT = 13 # Input Capability Set (section 2.2.7.1.6)
    CAPSTYPE_FONT = 14 # Font Capability Set (section 2.2.7.2.5)
    CAPSTYPE_BRUSH = 15 # Brush Capability Set (section 2.2.7.1.7)
    CAPSTYPE_GLYPHCACHE = 16 # Glyph Cache Capability Set (section 2.2.7.1.8)
    CAPSTYPE_OFFSCREENCACHE = 17 # Offscreen Bitmap Cache Capability Set (section 2.2.7.1.9)
    CAPSTYPE_BITMAPCACHE_HOSTSUPPORT = 18 # Bitmap Cache Host Support Capability Set (section 2.2.7.2.1)
    CAPSTYPE_BITMAPCACHE_REV2 = 19 # Revision 2 Bitmap Cache Capability Set (section 2.2.7.1.4.2)
    CAPSTYPE_VIRTUALCHANNEL = 20 # Virtual Channel Capability Set (section 2.2.7.1.10)
    CAPSTYPE_DRAWNINEGRIDCACHE = 21 # DrawNineGrid Cache Capability Set ([MS-RDPEGDI] section 2.2.1.2)
    CAPSTYPE_DRAWGDIPLUS = 22 # Draw GDI+ Cache Capability Set ([MS-RDPEGDI] section 2.2.1.3)
    CAPSTYPE_RAIL = 23 # Remote Programs Capability Set ([MS-RDPERP] section 2.2.1.1.1)
    CAPSTYPE_WINDOW = 24 # Window List Capability Set ([MS-RDPERP] section 2.2.1.1.2)
    CAPSETTYPE_COMPDESK = 25 # Desktop Composition Extension Capability Set (section 2.2.7.2.8)
    CAPSETTYPE_MULTIFRAGMENTUPDATE = 26 # Multifragment Update Capability Set (section 2.2.7.2.6)
    CAPSETTYPE_LARGE_POINTER = 27 # Large Pointer Capability Set (section 2.2.7.2.7)
    CAPSETTYPE_SURFACE_COMMANDS = 28 # Surface Commands Capability Set (section 2.2.7.2.9 )
    CAPSETTYPE_BITMAP_CODECS = 29 # Bitmap Codecs Capability Set (section 2.2.7.2.10 )

class TS_GENERAL_CAPABILITYSET_osMajorType(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240549 2.2.7.1.1 General Capability Set (TS_GENERAL_CAPABILITYSET)
    OSMAJORTYPE_UNSPECIFIED = 0x0000 # Unspecified platform
    OSMAJORTYPE_WINDOWS = 0x0001 # Windows platform
    OSMAJORTYPE_OS2 = 0x0002 # OS/2 platform
    OSMAJORTYPE_MACINTOSH = 0x0003 # Macintosh platform
    OSMAJORTYPE_UNIX = 0x0004 # UNIX platform

class TS_GENERAL_CAPABILITYSET_osMinorType(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240549 2.2.7.1.1 General Capability Set (TS_GENERAL_CAPABILITYSET)
    OSMINORTYPE_UNSPECIFIED = 0x0000 # Unspecified version
    OSMINORTYPE_WINDOWS_31X = 0x0001 # Windows 3.1x
    TS_OSMINORTYPE_WINDOWS_95 = 0x0002 # Windows 95
    TS_OSMINORTYPE_WINDOWS_NT = 0x0003 # Windows NT
    TS_OSMINORTYPE_OS2_V21 = 0x0004 # OS/2 2.1
    TS_OSMINORTYPE_POWER_PC = 0x0005 # PowerPC
    TS_OSMINORTYPE_MACINTOSH = 0x0006 # Macintosh
    TS_OSMINORTYPE_NATIVE_XSERVER = 0x0007 # Native X Server
    TS_OSMINORTYPE_PSEUDO_XSERVER = 0x0008 # Pseudo X Server

class TS_GENERAL_CAPABILITYSET_extraFlags(util.BitField):
    # http://msdn.microsoft.com/en-us/library/cc240549 2.2.7.1.1 General Capability Set (TS_GENERAL_CAPABILITYSET)
    #RDP 5.0 and later supports the following flags.
    FASTPATH_OUTPUT_SUPPORTED = 0x0001 # Advertiser supports fast-path output.
    NO_BITMAP_COMPRESSION_HDR = 0x0400 # The 8-byte Compressed Data Header (section 2.2.9.1.1.3.1.2.3) MUST NOT be used in conjunction with compressed bitmap data.
    #RDP 5.1 and later supports the following additional flags.
    SHADOW_COMPRESSION_LEVEL = 0x0002 # Advertiser supports shadow compression. When this flag is set, the participating shadow client can support data compression during shadowing, provided that the compression level matches among the shadow clients. RDP 5.0 has no data compression for shadowing.
    LONG_CREDENTIALS_SUPPORTED = 0x0004 # Advertiser (client or server) supports long-length credentials for the user name, password, or domain name.
    #RDP 5.2 and later supports the following additional flags.
    AUTORECONNECT_SUPPORTED = 0x0008 # Advertiser supports session auto-reconnection. This flag allows a disconnected client to seamlessly reconnect to its original session without the user resupplying logon credentials.
    ENC_SALTED_CHECKSUM = 0x0010 # Advertiser supports salted MAC generation (see section 5.3.6.1.1).

class TS_BITMAP_CAPABILITYSET_drawingFlags(util.BitField):
    # http://msdn.microsoft.com/en-us/library/cc240554 2.2.7.1.2 Bitmap Capability Set (TS_BITMAP_CAPABILITYSET)
    DRAW_ALLOW_DYNAMIC_COLOR_FIDELITY = 0x02 # Indicates support for lossy compression of 32 bpp bitmaps by reducing color-fidelity on a per-pixel basis.
    DRAW_ALLOW_COLOR_SUBSAMPLING = 0x04 # Indicates support for chroma subsampling when compressing 32 bpp bitmaps.
    DRAW_ALLOW_SKIP_ALPHA = 0x08 # Indicates that the client supports the removal of the alpha-channel when compressing 32 bpp bitmaps. In this case the alpha is assumed to be 0xFF, meaning the bitmap is opaque.

class TS_CONTROL_PDU_action(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240492 2.2.1.15.1 Control PDU Data (TS_CONTROL_PDU)
    CTRLACTION_REQUEST_CONTROL = 0x0001 # Request control
    CTRLACTION_GRANTED_CONTROL = 0x0002 # Granted control
    CTRLACTION_DETACH = 0x0003 # Detach
    CTRLACTION_COOPERATE = 0x0004 # Cooperate

class TS_VIRTUALCHANNEL_CAPABILITYSET_flags(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240551 2.2.7.1.10 Virtual Channel Capability Set (TS_VIRTUALCHANNEL_CAPABILITYSET)
    VCCAPS_NO_COMPR = 0x00000000 # Virtual channel compression is not supported.
    VCCAPS_COMPR_SC = 0x00000001 # Virtual channel compression is supported for server-to-client traffic. The highest compression level supported by the client is advertised in the Client Info PDU (section 2.2.1.11).
    VCCAPS_COMPR_CS_8K = 0x00000002 # Virtual channel compression is supported for client-to-server traffic. The compression level is implicitly limited to MPPC-8K for scalability reasons.

class TS_SAVE_SESSION_INFO_PDU_DATA_INFOTYPE(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240636 2.2.10.1.1 Save Session Info PDU Data (TS_SAVE_SESSION_INFO_PDU_DATA)
    INFOTYPE_LOGON = 0x00000000 # This is a notification that the user has logged on. The infoData field which follows contains a Logon Info Version 1 (section 2.2.10.1.1.1) structure.
    INFOTYPE_LOGON_LONG = 0x00000001 # This is a notification that the user has logged on. The infoData field which follows contains a Logon Info Version 2 (section 2.2.10.1.1.2) structure. This type was added in RDP 5.1 and SHOULD be used if the LONG_CREDENTIALS_SUPPORTED (0x00000004) flag is set in the General Capability Set (section 2.2.7.1.1).
    INFOTYPE_LOGON_PLAINNOTIFY = 0x00000002 # This is a notification that the user has logged on. The infoData field which follows contains a Plain Notify structure which contains 576 bytes of padding (see Section 2.2.10.1.1.3). This type was added in RDP 5.1.
    INFOTYPE_LOGON_EXTENDED_INF = 0x00000003 # The infoData field which follows contains a Logon Info Extended (section 2.2.10.1.1.4) structure. This type was added in RDP 5.2.

class TS_SET_ERROR_INFO_PDU_errorInfo(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240544 2.2.5.1.1 Set Error Info PDU Data (TS_SET_ERROR_INFO_PDU)
    # Protocol-independent codes:
    ERRINFO_RPC_INITIATED_DISCONNECT = 0x00000001 # The disconnection was initiated by an administrative tool on the server in another session.
    ERRINFO_RPC_INITIATED_LOGOFF = 0x00000002 # The disconnection was due to a forced logoff initiated by an administrative tool on the server in another session.
    ERRINFO_IDLE_TIMEOUT = 0x00000003 # The idle session limit timer on the server has elapsed.
    ERRINFO_LOGON_TIMEOUT = 0x00000004 # The active session limit timer on the server has elapsed.
    ERRINFO_DISCONNECTED_BY_OTHERCONNECTION = 0x00000005 # Another user connected to the server, forcing the disconnection of the current connection.
    ERRINFO_OUT_OF_MEMORY = 0x00000006 # The server ran out of available memory resources.
    ERRINFO_SERVER_DENIED_CONNECTION = 0x00000007 # The server denied the connection.
    ERRINFO_SERVER_INSUFFICIENT_PRIVILEGES = 0x00000009 # The user cannot connect to the server due to insufficient access privileges.
    ERRINFO_SERVER_FRESH_CREDENTIALS_REQUIRED = 0x0000000A # The server does not accept saved user credentials and requires that the user enter their credentials for each connection.
    ERRINFO_RPC_INITIATED_DISCONNECT_BYUSER = 0x0000000B # The disconnection was initiated by an administrative tool on the server running in the user's session.
    # Protocol-independent licensing codes:
    ERRINFO_LICENSE_INTERNAL = 0x00000100 # An internal error has occurred in the Terminal Services licensing component.
    ERRINFO_LICENSE_NO_LICENSE_SERVER = 0x00000101 # A Terminal Server License Server ([MS-RDPELE] section 1.1) could not be found to provide a license.
    ERRINFO_LICENSE_NO_LICENSE = 0x00000102 # There are no Client Access Licenses ([MS-RDPELE] section 1.1) available for the target remote computer.
    ERRINFO_LICENSE_BAD_CLIENT_MSG = 0x00000103 # The remote computer received an invalid licensing message from the client.
    ERRINFO_LICENSE_HWID_DOESNT_MATCH_LICENSE = 0x00000104 # The Client Access License ([MS-RDPELE] section 1.1) stored by the client has been modified.
    ERRINFO_LICENSE_BAD_CLIENT_LICENSE = 0x00000105 # The Client Access License ([MS-RDPELE] section 1.1) stored by the client is in an invalid format
    ERRINFO_LICENSE_CANT_FINISH_PROTOCOL = 0x00000106 # Network problems have caused the licensing protocol ([MS-RDPELE] section 1.3.3) to be terminated.
    ERRINFO_LICENSE_CLIENT_ENDED_PROTOCOL = 0x00000107 # The client prematurely ended the licensing protocol ([MS-RDPELE] section 1.3.3).
    ERRINFO_LICENSE_BAD_CLIENT_ENCRYPTION = 0x00000108 # A licensing message ([MS-RDPELE] sections 2.2 and 5.1) was incorrectly encrypted.
    ERRINFO_LICENSE_CANT_UPGRADE_LICENSE = 0x00000109 # The Client Access License ([MS-RDPELE] section 1.1) stored by the client could not be upgraded or renewed.
    ERRINFO_LICENSE_NO_REMOTE_CONNECTIONS = 0x0000010A # The remote computer is not licensed to accept remote connections
    # RDP specific codes:
    ERRINFO_UNKNOWNPDUTYPE2 = 0x000010C9 # Unknown pduType2 field in a received Share Data Header (section 2.2.8.1.1.1.2).
    ERRINFO_UNKNOWNPDUTYPE = 0x000010CA # Unknown pduType field in a received Share Control Header (section 2.2.8.1.1.1.1).
    ERRINFO_DATAPDUSEQUENCE = 0x000010CB # An out-of-sequence Slow-Path Data PDU (section 2.2.8.1.1.1.1) has been received.
    ERRINFO_CONTROLPDUSEQUENCE = 0x000010CD # An out-of-sequence Slow-Path Non-Data PDU (section 2.2.8.1.1.1.1) has been received.
    ERRINFO_INVALIDCONTROLPDUACTION = 0x000010CE # A Control PDU (sections 2.2.1.15 and 2.2.1.16) has been received with an invalid action field.
    ERRINFO_INVALIDINPUTPDUTYPE = 0x000010CF # (a) A Slow-Path Input Event (section 2.2.8.1.1.3.1.1) has been received with an invalid messageType field.
    # (b) A Fast-Path Input Event (section 2.2.8.1.2.2) has been received with an invalid eventCode field.
    ERRINFO_INVALIDINPUTPDUMOUSE = 0x000010D0 # (a) A Slow-Path Mouse Event (section 2.2.8.1.1.3.1.1.3) or Extended Mouse Event (section 2.2.8.1.1.3.1.1.4) has been received with an invalid pointerFlags field.
    # (b) A Fast-Path Mouse Event (section 2.2.8.1.2.2.3) or Fast-Path Extended Mouse Event (section 2.2.8.1.2.2.4) has been received with an invalid pointerFlags field.
    ERRINFO_INVALIDREFRESHRECTPDU = 0x000010D1 # An invalid Refresh Rect PDU (section 2.2.11.2) has been received.
    ERRINFO_CREATEUSERDATAFAILED = 0x000010D2 # The server failed to construct the GCC Conference Create Response user data (section 2.2.1.4).
    ERRINFO_CONNECTFAILED = 0x000010D3 # Processing during the Channel Connection phase of the RDP Connection Sequence (section 1.3.1.1) has failed.
    ERRINFO_CONFIRMACTIVEWRONGSHAREID = 0x000010D4 # A Confirm Active PDU (section 2.2.1.13.2) was received from the client with an invalid shareId field.
    ERRINFO_CONFIRMACTIVEWRONGORIGINATOR = 0x000010D5 # A Confirm Active PDU (section 2.2.1.13.2) was received from the client with an invalid originatorId field.
    ERRINFO_PERSISTENTKEYPDUBADLENGTH = 0x000010DA # There is not enough data to process a Persistent Key List PDU (section 2.2.1.17).
    ERRINFO_PERSISTENTKEYPDUILLEGALFIRST = 0x000010DB # A Persistent Key List PDU (section 2.2.1.17) marked as PERSIST_PDU_FIRST (0x01) was received after the reception of a prior Persistent Key List PDU also marked as PERSIST_PDU_FIRST.
    ERRINFO_PERSISTENTKEYPDUTOOMANYTOTALKEYS = 0x000010DC # A Persistent Key List PDU (section 2.2.1.17) was received which specified a total number of bitmap cache entries larger than 262144.
    ERRINFO_PERSISTENTKEYPDUTOOMANYCACHEKEYS = 0x000010DD # A Persistent Key List PDU (section 2.2.1.17) was received which specified an invalid total number of keys for a bitmap cache (the number of entries that can be stored within each bitmap cache is specified in the Revision 1 or 2 Bitmap Cache Capability Set (section 2.2.7.1.4) that is sent from client to server).
    ERRINFO_INPUTPDUBADLENGTH = 0x000010DE # There is not enough data to process Input Event PDU Data (section 2.2.8.1.1.3.1) or a Fast-Path Input Event PDU (section 2.2.8.1.2).
    ERRINFO_BITMAPCACHEERRORPDUBADLENGTH = 0x000010DF # There is not enough data to process the shareDataHeader, NumInfoBlocks, Pad1, and Pad2 fields of the Bitmap Cache Error PDU Data ([MS-RDPEGDI] section 2.2.2.3.1.1).
    ERRINFO_SECURITYDATATOOSHORT = 0x000010E0 # (a) The dataSignature field of the Fast-Path Input Event PDU (section 2.2.8.1.2) does not contain enough data.
    # (b) The fipsInformation and dataSignature fields of the Fast-Path Input Event PDU (section 2.2.8.1.2) do not contain enough data.
    ERRINFO_VCHANNELDATATOOSHORT = 0x000010E1 # (a) There is not enough data in the Client Network Data (section 2.2.1.3.4) to read the virtual channel configuration data.
    # (b) There is not enough data to read a complete Channel PDU Header (section 2.2.6.1.1).
    ERRINFO_SHAREDATATOOSHORT = 0x000010E2 # (a) There is not enough data to process Control PDU Data (section 2.2.1.15.1).
    # (b) There is not enough data to read a complete Share Control Header (section 2.2.8.1.1.1.1).
    # (c) There is not enough data to read a complete Share Data Header (section 2.2.8.1.1.1.2) of a Slow-Path Data PDU (section 2.2.8.1.1.1.1).
    # (d) There is not enough data to process Font List PDU Data (section 2.2.1.18.1).
    ERRINFO_BADSUPRESSOUTPUTPDU = 0x000010E3 # (a) There is not enough data to process Suppress Output PDU Data (section 2.2.11.3.1).
    # (b) The allowDisplayUpdates field of the Suppress Output PDU Data (section 2.2.11.3.1) is invalid.
    ERRINFO_CONFIRMACTIVEPDUTOOSHORT = 0x000010E5 # (a) There is not enough data to read the shareControlHeader, shareId, originatorId, lengthSourceDescriptor, and lengthCombinedCapabilities fields of the Confirm Active PDU Data (section 2.2.1.13.2.1).
    # (b) There is not enough data to read the sourceDescriptor, numberCapabilities, pad2Octets, and capabilitySets fields of the Confirm Active PDU Data (section 2.2.1.13.2.1).
    ERRINFO_CAPABILITYSETTOOSMALL = 0x000010E7 # There is not enough data to read the capabilitySetType and the lengthCapability fields in a received Capability Set (section 2.2.1.13.1.1.1).
    ERRINFO_CAPABILITYSETTOOLARGE = 0x000010E8 # A Capability Set (section 2.2.1.13.1.1.1) has been received with a lengthCapability field that contains a value greater than the total length of the data received.
    ERRINFO_NOCURSORCACHE = 0x000010E9 # (a) Both the colorPointerCacheSize and pointerCacheSize fields in the Pointer Capability Set (section 2.2.7.1.5) are set to zero.
    # (b) The pointerCacheSize field in the Pointer Capability Set (section 2.2.7.1.5) is not present, and the colorPointerCacheSize field is set to zero.
    ERRINFO_BADCAPABILITIES = 0x000010EA # The capabilities received from the client in the Confirm Active PDU (section 2.2.1.13.2) were not accepted by the server.
    ERRINFO_VIRTUALCHANNELDECOMPRESSIONERR = 0x000010EC # An error occurred while using the bulk compressor (section 3.1.8 and [MS-RDPEGDI] section 3.1.8) to decompress a Virtual Channel PDU (section 2.2.6.1)
    ERRINFO_INVALIDVCCOMPRESSIONTYPE = 0x000010ED # An invalid bulk compression package was specified in the flags field of the Channel PDU Header (section 2.2.6.1.1).
    ERRINFO_INVALIDCHANNELID = 0x000010EF # An invalid MCS channel ID was specified in the mcsPdu field of the Virtual Channel PDU (section 2.2.6.1).
    ERRINFO_VCHANNELSTOOMANY = 0x000010F0 # The client requested more than the maximum allowed 31 static virtual channels in the Client Network Data (section 2.2.1.3.4).
    ERRINFO_REMOTEAPPSNOTENABLED = 0x000010F3 # The INFO_RAIL flag (0x00008000) MUST be set in the flags field of the Info Packet (section 2.2.1.11.1.1) as the target remote session has been created to host remote applications.
    ERRINFO_CACHECAPNOTSET = 0x000010F4 # The client sent a Persistent Key List PDU (section 2.2.1.17) without including the prerequisite Revision 2 Bitmap Cache Capability Set (section 2.2.7.1.4.2) in the Confirm Active PDU (section 2.2.1.13.2).
    ERRINFO_BITMAPCACHEERRORPDUBADLENGTH2 = 0x000010F5 # The NumInfoBlocks field in the Bitmap Cache Error PDU Data is inconsistent with the amount of data in the Info field ([MS-RDPEGDI] section 2.2.2.3.1.1).
    ERRINFO_OFFSCRCACHEERRORPDUBADLENGTH = 0x000010F6 # There is not enough data to process an Offscreen Bitmap Cache Error PDU ([MS-RDPEGDI] section 2.2.2.3.2).
    ERRINFO_DNGCACHEERRORPDUBADLENGTH = 0x000010F7 # There is not enough data to process a DrawNineGrid Cache Error PDU ([MS-RDPEGDI] section 2.2.2.3.3).
    ERRINFO_GDIPLUSPDUBADLENGTH = 0x000010F8 # There is not enough data to process a GDI+ Error PDU ([MS-RDPEGDI] section 2.2.2.3.4).
    ERRINFO_SECURITYDATATOOSHORT2 = 0x00001111 # There is not enough data to read a Basic Security Header (section 2.2.8.1.1.2.1).
    ERRINFO_SECURITYDATATOOSHORT3 = 0x00001112 # There is not enough data to read a Non-FIPS Security Header (section 2.2.8.1.1.2.2) or FIPS Security Header (section 2.2.8.1.1.2.3).
    ERRINFO_SECURITYDATATOOSHORT4 = 0x00001113 # There is not enough data to read the basicSecurityHeader and length fields of the Security Exchange PDU Data (section 2.2.1.10.1).
    ERRINFO_SECURITYDATATOOSHORT5 = 0x00001114 # There is not enough data to read the CodePage, flags, cbDomain, cbUserName, cbPassword, cbAlternateShell, cbWorkingDir, Domain, UserName, Password, AlternateShell, and WorkingDir fields in the Info Packet (section 2.2.1.11.1.1).
    ERRINFO_SECURITYDATATOOSHORT6 = 0x00001115 # There is not enough data to read the CodePage, flags, cbDomain, cbUserName, cbPassword, cbAlternateShell, and cbWorkingDir fields in the Info Packet (section 2.2.1.11.1.1).
    ERRINFO_SECURITYDATATOOSHORT7 = 0x00001116 # There is not enough data to read the clientAddressFamily and cbClientAddress fields in the Extended Info Packet (section 2.2.1.11.1.1.1).
    ERRINFO_SECURITYDATATOOSHORT8 = 0x00001117 # There is not enough data to read the clientAddress field in the Extended Info Packet (section 2.2.1.11.1.1.1).
    ERRINFO_SECURITYDATATOOSHORT9 = 0x00001118 # There is not enough data to read the cbClientDir field in the Extended Info Packet (section 2.2.1.11.1.1.1).
    ERRINFO_SECURITYDATATOOSHORT10 = 0x00001119 # There is not enough data to read the clientDir field in the Extended Info Packet (section 2.2.1.11.1.1.1).
    ERRINFO_SECURITYDATATOOSHORT11 = 0x0000111A # There is not enough data to read the clientTimeZone field in the Extended Info Packet (section 2.2.1.11.1.1.1).
    ERRINFO_SECURITYDATATOOSHORT12 = 0x0000111B # There is not enough data to read the clientSessionId field in the Extended Info Packet (section 2.2.1.11.1.1.1).
    ERRINFO_SECURITYDATATOOSHORT13 = 0x0000111C # There is not enough data to read the performanceFlags field in the Extended Info Packet (section 2.2.1.11.1.1.1).
    ERRINFO_SECURITYDATATOOSHORT14 = 0x0000111D # There is not enough data to read the cbAutoReconnectLen field in the Extended Info Packet (section 2.2.1.11.1.1.1).
    ERRINFO_SECURITYDATATOOSHORT15 = 0x0000111E # There is not enough data to read the autoReconnectCookie field in the Extended Info Packet (section 2.2.1.11.1.1.1).
    ERRINFO_SECURITYDATATOOSHORT16 = 0x0000111F # The cbAutoReconnectLen field in the Extended Info Packet (section 2.2.1.11.1.1.1) contains a value which is larger than the maximum allowed length of 128 bytes.
    ERRINFO_SECURITYDATATOOSHORT17 = 0x00001120 # There is not enough data to read the clientAddressFamily and cbClientAddress fields in the Extended Info Packet (section 2.2.1.11.1.1.1).
    ERRINFO_SECURITYDATATOOSHORT18 = 0x00001121 # There is not enough data to read the clientAddress field in the Extended Info Packet (section 2.2.1.11.1.1.1).
    ERRINFO_SECURITYDATATOOSHORT19 = 0x00001122 # There is not enough data to read the cbClientDir field in the Extended Info Packet (section 2.2.1.11.1.1.1).
    ERRINFO_SECURITYDATATOOSHORT20 = 0x00001123 # There is not enough data to read the clientDir field in the Extended Info Packet (section 2.2.1.11.1.1.1).
    ERRINFO_SECURITYDATATOOSHORT21 = 0x00001124 # There is not enough data to read the clientTimeZone field in the Extended Info Packet (section 2.2.1.11.1.1.1).
    ERRINFO_SECURITYDATATOOSHORT22 = 0x00001125 # There is not enough data to read the clientSessionId field in the Extended Info Packet (section 2.2.1.11.1.1.1).
    ERRINFO_SECURITYDATATOOSHORT23 = 0x00001126 # There is not enough data to read the Client Info PDU Data (section 2.2.1.11.1).
    ERRINFO_BADMONITORDATA = 0x00001129 # The monitorCount field in the Client Monitor Data (section 2.2.1.3.6) is invalid.
    ERRINFO_UPDATESESSIONKEYFAILED = 0x00001191 # An attempt to update the session keys while using Standard RDP Security mechanisms (section 5.3.7) failed.
    ERRINFO_DECRYPTFAILED = 0x00001192 # (a) Decryption using Standard RDP Security mechanisms (section 5.3.6) failed.
    # (b) Session key creation using Standard RDP Security mechanisms (section 5.3.5) failed.
    ERRINFO_ENCRYPTFAILED = 0x00001193 # Encryption using Standard RDP Security mechanisms (section 5.3.6) failed.
    ERRINFO_ENCPKGMISMATCH = 0x00001194 # Failed to find a usable Encryption Method (section 5.3.2) in the encryptionMethods field of the Client Security Data (section 2.2.1.4.3).
    ERRINFO_DECRYPTFAILED2 = 0x00001195 # Unencrypted data was encountered in a protocol stream which is meant to be encrypted with Standard RDP Security mechanisms (section 5.3.6).

class LICENSE_PREAMBLE_bMsgType(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240480 2.2.1.12.1.1 Licensing Preamble (LICENSE_PREAMBLE)
    # Sent by server:
    LICENSE_REQUEST = 0x01 # Indicates a License Request PDU (see [MS-RDPELE]).
    PLATFORM_CHALLENGE = 0x02 # Indicates a Platform Challenge PDU (see [MS-RDPELE]).
    NEW_LICENSE = 0x03 # Indicates a New License PDU (see [MS-RDPELE]).
    UPGRADE_LICENSE = 0x04 # Indicates an Upgrade License PDU (see [MS-RDPELE]).
    # Sent by client:
    LICENSE_INFO = 0x12 # Indicates a License Info PDU (see [MS-RDPELE]).
    NEW_LICENSE_REQUEST = 0x13 # Indicates a New License Request PDU (see [MS-RDPELE]).
    PLATFORM_CHALLENGE_RESPONSE = 0x15 # Indicates a Platform Challenge Response PDU (see [MS-RDPELE]).
    # Sent by either client or server:
    ERROR_ALERT = 0xFF # Indicates a Licensing Error Message PDU (section 2.2.1.12.1.3).

class LICENSE_PREAMBLE_bVersion(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240480 2.2.1.12.1.1 Licensing Preamble (LICENSE_PREAMBLE)
    PREAMBLE_VERSION_2_0 = 0x02 # RDP 4.0
    PREAMBLE_VERSION_3_0 = 0x03 # RDP 5.0, 5.1, and 5.2
    # Undocumented, from example at http://msdn.microsoft.com/en-us/library/cc242006
    PREAMBLE_VERSION_2_0_ExtendedError = 0x82 # RDP 4.0
    PREAMBLE_VERSION_3_0_ExtendedError = 0x83 # RDP 5.0, 5.1, and 5.2

class LICENSE_ERROR_MESSAGE_dwErrorCode(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240482 2.2.1.12.1.3 Licensing Error Message (LICENSE_ERROR_MESSAGE)
    # Sent by client:
    ERR_INVALID_SERVER_CERTIFICATE = 0x00000001
    ERR_NO_LICENSE = 0x00000002
    # Sent by server:
    ERR_INVALID_SCOPE = 0x00000004
    ERR_NO_LICENSE_SERVER = 0x00000006
    STATUS_VALID_CLIENT = 0x00000007
    ERR_INVALID_CLIENT = 0x00000008
    ERR_INVALID_PRODUCTID = 0x0000000B
    ERR_INVALID_MESSAGE_LEN = 0x0000000C
    # Sent by client and server:
    ERR_INVALID_MAC = 0x00000003

class LICENSE_ERROR_MESSAGE_dwStateTransition(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240482 2.2.1.12.1.3 Licensing Error Message (LICENSE_ERROR_MESSAGE)
    ST_TOTAL_ABORT = 0x00000001
    ST_NO_TRANSITION = 0x00000002
    ST_RESET_PHASE_TO_START = 0x00000003
    ST_RESEND_LAST_MESSAGE = 0x00000004

class LICENSE_BINARY_BLOB_wBlobType(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240481 2.2.1.12.1.2 Licensing Binary Blob (LICENSE_BINARY_BLOB)
    BB_DATA_BLOB = 0x0001 # Used by License Info PDU and Platform Challenge Response PDU (see [MS-RDPELE]).
    BB_RANDOM_BLOB = 0x0002 # Used by License Info PDU and New License Request PDU (see [MS-RDPELE]).
    BB_CERTIFICATE_BLOB = 0x0003 # Used by License Request PDU (see [MS-RDPELE]).
    BB_ERROR_BLOB = 0x0004 # Used by License Error PDU (section 2.2.1.12).
    BB_ENCRYPTED_DATA_BLOB = 0x0009 # Used by Platform Challenge Response PDU and Server Upgrade License PDU (see [MS-RDPELE]).
    BB_KEY_EXCHG_ALG_BLOB = 0x000D # Used by License Request PDU (see [MS-RDPELE]).
    BB_SCOPE_BLOB = 0x000E # Used by License Request PDU ([MS-RDPELE]).
    BB_CLIENT_USER_NAME_BLOB = 0x000F # Used by New License Request PDU (see [MS-RDPELE]).
    BB_CLIENT_MACHINE_NAME_BLOB = 0x0010 # Used by New License Request PDU (see [MS-RDPELE]).

class TS_FP_KEYBOARD_EVENT_eventFlags(util.BitField):
    # http://msdn.microsoft.com/en-us/library/cc240592 2.2.8.1.2.2.1 Fast-Path Keyboard Event (TS_FP_KEYBOARD_EVENT)
    FASTPATH_INPUT_KBDFLAGS_RELEASE = 0x01 # The absence of this flag indicates a key-down event, while its presence indicates a key-release event.
    FASTPATH_INPUT_KBDFLAGS_EXTENDED = 0x02 # The keystroke message contains an extended scancode. For enhanced 101-key and 102-key keyboards, extended keys include the right ALT and right CTRL keys on the main section of the keyboard; the INS, DEL, HOME, END, PAGE UP, PAGE DOWN and ARROW keys in the clusters to the left of the numeric keypad; and the Divide ("/") and ENTER keys in the numeric keypad.

class TS_POINTER_EVENT_pointerFlags(util.BitField):
    # http://msdn.microsoft.com/en-us/library/cc240586 2.2.8.1.1.3.1.1.3 Mouse Event (TS_POINTER_EVENT)
    # Mouse wheel event:
    PTRFLAGS_WHEEL = 0x0200 # The event is a mouse wheel rotation. The only valid flags in a wheel rotation event are PTRFLAGS_WHEEL_NEGATIVE and the WheelRotationMask; all other pointer flags are ignored.
    PTRFLAGS_WHEEL_NEGATIVE = 0x0100 # The PTRFLAGS_ROTATION_MASK value is negative and must be sign-extended before injection at the server.
    WheelRotationMask = 0x01FF # The bit field describing the number of rotation units the mouse wheel was rotated. The value is negative if the PTRFLAGS_WHEEL_NEGATIVE flag is set.
    # Mouse movement event
    PTRFLAGS_MOVE = 0x0800 # Indicates that the mouse position should be updated to the location specified by the xPos and yPos fields.
    # Mouse button events:
    PTRFLAGS_DOWN = 0x8000 # Indicates that a click event has occurred at the position specified by the xPos and yPos fields. The button flags indicate which button has been clicked and at least one of these flags MUST be set.
    PTRFLAGS_BUTTON1 = 0x1000 # Mouse button 1 (left button) was clicked or released. If the PTRFLAGS_DOWN flag is set, then the button was clicked, otherwise it was released.
    PTRFLAGS_BUTTON2 = 0x2000 # Mouse button 2 (right button) was clicked or released. If the PTRFLAGS_DOWN flag is set, then the button was clicked, otherwise it was released.
    PTRFLAGS_BUTTON3 = 0x4000 # Mouse button 3 (middle button or wheel) was clicked or released. If the PTRFLAGS_DOWN flag is set, then the button was clicked, otherwise it was released.

class TS_POINTERX_EVENT_pointerFlags(util.BitField):
    # http://msdn.microsoft.com/en-us/library/cc240587 2.2.8.1.1.3.1.1.4 Extended Mouse Event (TS_POINTERX_EVENT)
    PTRXFLAGS_DOWN = 0x8000 # Indicates that a click event has occurred at the position specified by the xPos and yPos fields. The button flags indicate which button has been clicked and at least one of these flags MUST be set.
    PTRXFLAGS_BUTTON1 = 0x0001 # Extended mouse button 1 was clicked or released. If the PTRXFLAGS_DOWN flag is set, then the button was clicked, otherwise it was released.
    PTRXFLAGS_BUTTON2 = 0x0002 # Extended mouse button 2 was clicked or released. If the PTRXFLAGS_DOWN flag is set, then the button was clicked, otherwise it was released.

class TS_SYNC_EVENT_toggleFlags(util.BitField):
    # # http://msdn.microsoft.com/en-us/library/cc240588 # 2.2.8.1.1.3.1.1.5 Synchronize Event (TS_SYNC_EVENT)
    TS_SYNC_SCROLL_LOCK = 0x00000001 # Indicates that the Scroll Lock indicator light SHOULD be on.
    TS_SYNC_NUM_LOCK = 0x00000002 # Indicates that the Num Lock indicator light SHOULD be on.
    TS_SYNC_CAPS_LOCK = 0x00000004 # Indicates that the Caps Lock indicator light SHOULD be on.
    TS_SYNC_KANA_LOCK = 0x00000008 # Indicates that the Kana Lock indicator light SHOULD be on.

class TS_FP_SYNC_EVENT_eventFlags(util.BitField):
    # http://msdn.microsoft.com/en-us/library/cc240596 2.2.8.1.2.2.5 Fast-Path Synchronize Event (TS_FP_SYNC_EVENT)
    FASTPATH_INPUT_SYNC_SCROLL_LOCK = 0x01 # Indicates that the Scroll Lock indicator light SHOULD be on.
    FASTPATH_INPUT_SYNC_NUM_LOCK = 0x02 # Indicates that the Num Lock indicator light SHOULD be on.
    FASTPATH_INPUT_SYNC_CAPS_LOCK = 0x04 # Indicates that the Caps Lock indicator light SHOULD be on.
    FASTPATH_INPUT_SYNC_KANA_LOCK = 0x08 # Indicates that the Kana Lock indicator light SHOULD be on.

class TS_GRAPHICS_UPDATE_updateType(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240608 2.2.9.1.1.3.1 Slow Path Graphics Update (TS_GRAPHICS_UPDATE)
    UPDATETYPE_ORDERS = 0x0000 #Indicates an Orders Update (see [MS-RDPEGDI] section 2.2.2.2).
    UPDATETYPE_BITMAP = 0x0001 # Indicates a Bitmap Graphics Update (see section 2.2.9.1.1.3.1.2).
    UPDATETYPE_PALETTE = 0x0002 # Indicates a Palette Update (see section 2.2.9.1.1.3.1.1).
    UPDATETYPE_SYNCHRONIZE = 0x0003 # Indicates a Synchronize Update (see section 2.2.9.1.1.3.1.3).

class PDUTYPE2_POINTER_messageType(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc240614 2.2.9.1.1.4 Server Pointer Update PDU (TS_POINTER_PDU)
    TS_PTRMSGTYPE_SYSTEM = 0x0001 # Indicates a System Pointer Update (section 2.2.9.1.1.4.3).
    TS_PTRMSGTYPE_POSITION = 0x0003 # Indicates a Pointer Position Update (section 2.2.9.1.1.4.2).
    TS_PTRMSGTYPE_COLOR = 0x0006 # Indicates a Color Pointer Update (section 2.2.9.1.1.4.4).
    TS_PTRMSGTYPE_CACHED = 0x0007 # Indicates a Cached Pointer Update (section 2.2.9.1.1.4.6).
    TS_PTRMSGTYPE_POINTER = 0x0008 # Indicates a New Pointer Update (section 2.2.9.1.1.4.5).

class PlatformId_flags(util.BitField):
    # http://msdn.microsoft.com/en-us/library/cc242016 Appendix A: Product Behavior
    # http://msdn.microsoft.com/en-us/library/cc241918 2.2.2.2 Client New License Request (CLIENT_NEW_LICENSE_REQUEST)
    # The most significant byte of the PlatformId field contains the operating system version of the client.
    CLIENT_OS_ID_WINNT_351 = 0x01000000 # The client operating system version is 3.51.
    CLIENT_OS_ID_WINNT_40 = 0x02000000 # The client operating system version is 4.00.
    CLIENT_OS_ID_WINNT_50 = 0x03000000 # The client operating system version is 5.00.
    CLIENT_OS_ID_WINNT_POST_52 = 0x04000000 # The client operating system version is 5.20 or later.
    # The second most significant byte of the PlatformId field identifies the ISV that provided the client image.<10>
    CLIENT_IMAGE_ID_MICROSOFT = 0x00010000 # The ISV for the client image is Microsoft.
    CLIENT_IMAGE_ID_CITRIX = 0x00020000 # The ISV for the client image is Citrix.

class PreferredKeyExchangeAlg(util.EnumField):
    # http://msdn.microsoft.com/en-us/library/cc241918
    KEY_EXCHANGE_ALG_RSA = 0x00000001 # indicates an RSA-based key exchange with a 512-bit asymmetric key

class RedirFlags(util.BitField):
    # http://msdn.microsoft.com/en-us/library/ee443575
    # RedirFlags (4 bytes): A 32-bit, unsigned integer. A bit field that contains redirection information flags, some of which indicate the presence of additional data at the end of the packet.
    LB_TARGET_NET_ADDRESS = 0x00000001 # Indicates that the TargetNetAddressLength and TargetNetAddress fields are present.
    LB_LOAD_BALANCE_INFO = 0x00000002 # Indicates that the LoadBalanceInfoLength and LoadBalanceInfo fields are present.
    LB_USERNAME = 0x00000004 # Indicates that the UserNameLength and UserName fields are present.
    LB_DOMAIN = 0x00000008 # Indicates that the DomainLength and Domain fields are present.
    LB_PASSWORD = 0x00000010 # Indicates that the PasswordLength and Password fields are present.
    LB_DONTSTOREUSERNAME = 0x00000020 # Indicates that when reconnecting, the client MUST send the username specified in the UserName field to the server in the Client Info PDU (section 2.2.1.11.1.1).
    LB_SMARTCARD_LOGON = 0x00000040 # Indicates that the user can use a smart card for authentication.
    LB_NOREDIRECT = 0x00000080 # Indicates that the contents of the PDU are for informational purposes only. No actual redirection is required.
    LB_TARGET_FQDN = 0x00000100 # Indicates that the TargetFQDNLength and TargetFQDN fields are present.
    LB_TARGET_NETBIOS_NAME = 0x00000200 # Indicates that the TargetNetBiosNameLength and TargetNetBiosName fields are present.
    LB_TARGET_NET_ADDRESSES = 0x00000800 #  Indicates that the TargetNetAddressesLength and TargetNetAddresses fields are present.
    LB_CLIENT_TSV_URL = 0x00001000 # Indicates that the TsvUrlLength and TsvUrl fields are present. The LB_CLIENT_TSV_URL redirection flag is supported only on Windows 7 and Windows Server 2008 R2
    LB_SERVER_TSV_CAPABLE = 0x00002000 # Indicates that the server supports redirection based on the TsvUrl present in the LoadBalanceInfo sent by the client. The LB_SERVER_TSV_CAPABLE redirection flag is supported only on Windows 7 and Windows Server 2008 R2.

class LedFlags(util.BitField):
    # http://msdn.microsoft.com/en-us/library/cc240599 Set Keyboard Indicators PDU Data (TS_SET_KEYBOARD_INDICATORS_PDU)
    TS_SYNC_SCROLL_LOCK = 0x0001 # Indicates that the Scroll Lock indicator light SHOULD be on.
    TS_SYNC_NUM_LOCK = 0x0002 # Indicates that the Num Lock indicator light SHOULD be on.
    TS_SYNC_CAPS_LOCK = 0x0004 # Indicates that the Caps Lock indicator light SHOULD be on.
    TS_SYNC_KANA_LOCK = 0x0008 # Indicates that the Kana Lock indicator light SHOULD be on.

def test():
    print 'TS_UD_CS_CORE_highColorDepth(0x10):', TS_UD_CS_CORE_highColorDepth(0x10)
    print 'TS_UD_CS_CORE_highColorDepth(0x18):', TS_UD_CS_CORE_highColorDepth(0x18)

if __name__ == '__main__':
    test()
