import socket
import IPy

import components.traffic
from lib import checkpoint
from lib import encode
from lib import httpproxy

    # A proxy-like server
    # Do: use/modify requests, identification/addressing/dispatching, mostly transparent/gateway, mod
    # Don't: use/modify responses (almost), cache/modify content, modify connections or keep-alive,

TUNNELTAG = 'TUNNELTAG'
ERRORTAG = 'ERROR'

def mk_enginefactory(log_environment, launch_element, expand_variables, server_host, server_port, servers):

    checkpoint_handler = log_environment.checkpoint_handler

    def dnslookup(fqdn, default=None):
        #self._env.log3('Dns lookup for %s', fqdn)
        try:
            infos = socket.getaddrinfo(fqdn, 0, socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP)
            (_family, _socktype, _proto, _canonname, (ip, _port)) = infos[0]
            return ip
        except socket.error, e:
            #self._env.log.log2('Error resolving %r: %s', fqdn, e)
            return default

    transparent_addr = None
    white_ipranges = []
    whitehosts = {}
    if server_host:
        transparent_addr = (server_host, server_port)
        if servers:
            checkpoint_handler.Checkpoint("traffic async transparent", components.traffic.module_id, checkpoint.ERROR,
                                          msg='ignoring whitelist for transparent proxy')
    else:
        for server in servers:
            a_server_host = encode.preferred(server.server_host or '').strip().lower()
            a_server_port = server.server_port or 0
            hashtags = [x for x in a_server_host.split() if x.startswith('#')]
            hosts = [x for x in a_server_host.split() if not x.startswith('#')]
            if not hosts:
                checkpoint_handler.Checkpoint("traffic async http whitelist", components.traffic.module_id, checkpoint.DEBUG,
                                              msg='ignoring empty host')
                continue
            if len(hosts) != 1:
                checkpoint_handler.Checkpoint("traffic async http whitelist", components.traffic.module_id, checkpoint.ERROR,
                                              msg='multiple hosts', server_host=a_server_host)
                continue
            targetname = hosts[0]
            # First try to see if it is a valid IP address. If not we assume it is a DNS name
            try:
                iprange = IPy.IP(targetname)
            except ValueError, e:
                iprange = None
            if iprange:
                white_ipranges.append((iprange, a_server_port, hashtags))
                checkpoint_handler.Checkpoint("traffic async http whitelist", components.traffic.module_id, checkpoint.DEBUG,
                                                  whitelist_iprange=str(iprange), server_host=a_server_host, port=a_server_port, hashtags=' '.join(hashtags))
            else:
                whitehosts[(targetname, a_server_port)] = hashtags
                checkpoint_handler.Checkpoint("traffic async http whitelist", components.traffic.module_id, checkpoint.DEBUG,
                                              whitelist_host=targetname, server_host=a_server_host, port=a_server_port, hashtags=' '.join(hashtags))

    def get_authorized_ip(host, port):
        host = host.strip().lower()
        ip_s = None
        try:
            ip = IPy.IP(host)
        except ValueError, e:
            checkpoint_handler.Checkpoint("get_authorized_ip", components.traffic.module_id, checkpoint.DEBUG, msg='not recognised as ip address', e=str(e), server=host)
            ip = None
        if not ip:
            # look for a whitelist entry with the complete host DNS name (e.g. "foo.bar"), 
            # or a whitelist entry with a DNS wildcard spec (e.g. ".bar", or ".") that will be matched by the complete DNS name 
            hostspec = host
            while True:
                hashtags = whitehosts.get((hostspec, port))
                if hashtags is None:
                    hashtags = whitehosts.get((hostspec, 0))
                if hashtags is None:
                    # if we were searching for a DNS wildcard spec, prepare for creating the next, more general wildcard spec
                    if hostspec[0] == '.': 
                        hostspec = hostspec[1:]
                    # stop if we have already searched for all relevant wildcard specs
                    if hostspec == '': 
                        break
                    # create a DNS wildcard spec by removing the first label of the DNS name
                    hostspec = '.' + hostspec.partition('.')[2]
                else:
                    checkpoint_handler.Checkpoint("traffic async http whitelistcheck", components.traffic.module_id, checkpoint.DEBUG,
                                                  access='match', host=host, port=port, hashtags=' '.join(hashtags))
                    return (host, port), hashtags
            # prepare to match on IP
            ip_s = dnslookup(host)
            if not ip_s:
                checkpoint_handler.Checkpoint("traffic async http whitelistcheck getaddrinfo", components.traffic.module_id, checkpoint.ERROR,
                                              msg='access to %s:%s without IP denied' % (host, port))
                return None
            else:
                try:
                    ip = IPy.IP(ip_s)
                except ValueError:
                    checkpoint_handler.Checkpoint("traffic async http whitelistcheck", components.traffic.module_id, checkpoint.ERROR,
                                                  error='bad IP address', host=host, ip_s=ip_s)
                    return None
                
        # Now we got an ip address directly or through dns lookup                
        for (iprange, white_port, hashtags) in white_ipranges:
            if ip in iprange and (port == white_port or not white_port):
                checkpoint_handler.Checkpoint("traffic async http whitelistcheck", components.traffic.module_id, checkpoint.DEBUG,
                                              access='match', host=host, port=port, ip_s=ip_s, iprange=str(iprange),
                                              hashtags=' '.join(hashtags))
                return (host, port), hashtags
        checkpoint_handler.Checkpoint("traffic async http whitelistcheck", components.traffic.module_id, checkpoint.DEBUG,
                                      access='no match', host=host, port=port, ip_s=ip_s)
        return None


    def mk_logger(level, formatted=True):
        id_ = "HTTP::log%s" % level
        if formatted:
            def logger(fmt, *a, **b):
                msg = fmt % a
                checkpoint_handler.Checkpoint(id_, components.traffic.module_id, checkpoint.DEBUG, msg=msg)
        else:
            def logger(fmt, *a, **b):
                if 'dump' in b:
                    b['dump'] = repr(b['dump'])
                d = dict(fmt = fmt)
                d.update((str(i), str(z)) for (i, z) in enumerate(a))
                d.update((k, str(v)) for (k, v) in b.items())
                checkpoint_handler.Checkpoint(id_, components.traffic.module_id, checkpoint.DEBUG, **d)
        return logger


    class HttpConfig:
        PERSISTENT_HTTP = True # attempt persistent connections - squid supports it from client to proxy
        MAX_RECEIVE_HEADER_LENGTH = 10240 # maximum received HTTP header length in bytes
        MAX_REQUEST_BUFFER_MB = 10

    class Environment:
        config = HttpConfig
        log1 = staticmethod(lambda * a, **b: None)
        log2 = staticmethod(lambda * a, **b: None)
        log3 = staticmethod(lambda * a, **b: None)
        log0 = staticmethod(log_environment.access_logger.get_logger(module_name="server_launc_async_http", log_id="HTTP::log0"))
        log1 = staticmethod(mk_logger(1))
        #log2 = staticmethod(mk_logger(2))
        #log3 = staticmethod(mk_logger(3))


    class HttpProxyEngine(object):

        def __init__(self, cb_connect, cb_to_client, cb_close_client):
            self._env = Environment
            self._cb_to_client = cb_to_client
            self._cb_close_client = cb_close_client
            login = expand_variables(launch_element.sso_login)
            password = expand_variables(launch_element.sso_password)
            
            self._proxyengine = httpproxy.ProxyServer(self._env,
                                                      cb_will_listen=self._will_listen, cb_to_client=self._cb_to_client, cb_close=self.close, connector=cb_connect, 
                                                      get_authorized_ip=get_authorized_ip, transparent_addr=transparent_addr, 
                                                      username=login, password=password, expand_variables=expand_variables)
            self.active_tag = None
            self._error = None # if set as (code, msg, body) then reply with this when possible, with or without a client

        # callback from httpproxy
        def _will_listen(self, ready):
            self._env.log2('%s will listen is %s', self, ready) # TODO???

        def status(self):
            if self._proxyengine:
                self._env.log3('%s active: %s, available: %s', self, self.active_tag, ' '.join('%s/%s' % (a, b) for a, b in self._proxyengine._clients.items()))

        # called from owner of engine / client connection
        def from_client(self, data):
            """
            Handles data from client.
            Not called before connected.
            Returns excess data - or None if close.
            """
            self._env.log3('%s from_client %r', self, data)
            if not self._proxyengine:
                return None
            unused = self._proxyengine.tcp_from_client(data)
            self._env.log3('%s unused %r', self, unused)
            self.status()
            return unused

        # internal _and_ called from engine owner
        def close(self):
            """close everything _now_, for example because connection to client was closed"""
            proxyengine, self._proxyengine = self._proxyengine, None
            if proxyengine:
                proxyengine.close()
            cb_close_client, self._cb_close_client = self._cb_close_client, None
            if cb_close_client:
                cb_close_client()
            self._cb_to_client = None

        def __repr__(self):
            return '%s%X' % (self.__class__.__name__, id(self))

    return HttpProxyEngine
