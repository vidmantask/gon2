from lib import checkpoint

import components.traffic
from components.communication import tunnel_endpoint_base

import server_portforward
from components.traffic.server_common import database_schema

PORTFORWARD_CHILD_ID = 1

def icb_closed():
    """
    Notify that the portforward has been closed
    """
    pass


class IcaLaunchPortForward(tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch):
    def __init__(self, async_service, checkpoint_handler, new_tunnelendpoint, launch_id, own_id,
            expand_variables, cb_closed, access_log_server_session, cb_report_error,
            ica_body):
        tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch.__init__(self, async_service, checkpoint_handler, new_tunnelendpoint)
        self._access_log_server_session = access_log_server_session
        self.launch_id = launch_id
        self._own_id = own_id
        self.expand_variables = expand_variables
        self.cb_closed = cb_closed
        self.cb_report_error = cb_report_error
        self._close_from_client = False

        # parse ICA
        self.server_host = None
        self.server_port = None
        encoding = 'UTF8'
        ica_lines = []
        for line in ica_body.split('\r\n'):
            # Rewrite ICA - remove SOCKS settings which we won't use right now
            if line.startswith('ICASOCKS') or line.startswith('Proxy'):
                line = '# ' + line
            # Get server address and replace with port forward
            if line.startswith('Address='): # assuming it is in the ["applicationname"] section
                addr = line.split('=', 1)[1]
                if ':' in addr:
                    self.server_host, server_port_s = addr.split(':', 1)
                    self.server_port = int(server_port_s)
                else:
                    self.server_host = addr
                    self.server_port = 1494
                line = 'Address=%(portforward.host):%(portforward.port)'
            if line.startswith('InputEncoding='): # assuming it is in the Encoding section
                encoding = line.split('=', 1)[1] # usually 'UTF8' or 'ISO8859_1'
                line = 'InputEncoding=%(ica.inputencoding)'
            ica_lines.append(line)
        self.ica_content = '\r\n'.join(ica_lines).decode(encoding, 'replace') # assuming all encodings are known by Python
        assert self.server_host and self.server_port

        self._portforward = None

    def tunnelendpoint_connected(self):
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::_launch", components.traffic.module_id, checkpoint.DEBUG, launch_id=self.launch_id):
            launch_element = database_schema.LaunchElement.lookup(self.launch_id)
            if not launch_element:
                self.checkpoint_handler.Checkpoint("LaunchPortForward::_launch", components.traffic.module_id, checkpoint.ERROR, message="Launch id not found")
                return

            launch_command = self.expand_variables(launch_element.citrix_command) # Not .launch_command !!!
            working_directory = self.expand_variables(launch_element.working_directory)
            param_file_name = self.expand_variables(launch_element.param_file_name)
            param_file_lifetime = launch_element.param_file_lifetime

            close_with_process = launch_element.close_with_process
            kill_on_close = launch_element.kill_on_close
            tcp_options = [x.encode('ascii', 'ignore').strip().upper() for x in launch_element.citrix_command.split(',')]

            self._portforward = server_portforward.PortForwardServer(self.async_service,
                                                                  self.checkpoint_handler,
                                                                  self.server_host,
                                                                  self.server_port,
                                                                  self._portforward_ready,
                                                                  self._portforward_closed,
                                                                  self.cb_report_error
                                                                  )
            self.add_tunnelendpoint(PORTFORWARD_CHILD_ID, self._portforward.as_tunnelendpoint())

            portforward_spec = launch_element.portforwards[0]
            self._access_log_server_session.report_access_log_traffic_start(
                    (self._own_id, PORTFORWARD_CHILD_ID),
                    portforward_spec.client_host, portforward_spec.client_port,
                    self._portforward.server_host, self._portforward.server_port)

            self.tunnelendpoint_remote('remote_call_launch_with_tcp_options',
                                       child_id=PORTFORWARD_CHILD_ID,
                                       launch_command=launch_command,
                                       working_directory=working_directory,
                                       param_file_name=param_file_name,
                                       param_file_lifetime=param_file_lifetime,
                                       param_file_template=self.ica_content,
                                       close_with_process=close_with_process,
                                       kill_on_close=kill_on_close,
                                       client_host = portforward_spec.client_host,
                                       client_port = portforward_spec.client_port,
                                       lock_to_process_pid = portforward_spec.lock_to_process_pid,
                                       sub_processes = portforward_spec.sub_processes,
                                       lock_to_process_name = portforward_spec.lock_to_process_name,
                                       tcp_options = tcp_options
                                       )

    def _portforward_ready(self):
        self.checkpoint_handler.Checkpoint("LaunchPortForward::_portforward_ready", components.traffic.module_id, checkpoint.DEBUG)

    def _portforward_closed(self):
        """
        Portforward has closed down
        """
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::_cb_closed", components.traffic.module_id, checkpoint.DEBUG):
            # Last portforward closed
            if not self._close_from_client:
                self.tunnelendpoint_remote('remote_call_closed')
            self._reset()
            self.cb_closed()

    def remote_call_launch_response(self, is_ok):
        """
        Called from client when the launch is done
        """
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::remote_call_launch_response", components.traffic.module_id, checkpoint.DEBUG,
                                                      is_ok="%s" % is_ok):
            pass

    def close(self, force = False, close_and_forget = False):
        """
        Close down the launched portforward
        """
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::close", components.traffic.module_id, checkpoint.DEBUG):
            self._portforward.close(force, close_and_forget)

            if close_and_forget:
                self._reset()

    def remote_call_closed(self):
        """
        Client is telling that is has closed down
        """
        with self.checkpoint_handler.CheckpointScope("LaunchPortForward::remote_call_closed", components.traffic.module_id, checkpoint.DEBUG):
            self._close_from_client = True
            self._portforward.close(True, True)
            self._reset()
            self.cb_closed()

    def _reset(self):
        self._access_log_server_session.report_access_log_traffic_close((self._own_id, PORTFORWARD_CHILD_ID))
        self.remove_tunnelendpoint_tunnel(PORTFORWARD_CHILD_ID)
