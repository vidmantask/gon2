"""
Portforwardung using comunication component
"""
from __future__ import with_statement

from lib import checkpoint

import components.traffic
from components.communication import tunnel_endpoint_tcp


def icb_closed():
    """
    Notify that the portforward has been closed
    """
    pass

class PortForwardServer(tunnel_endpoint_tcp.TCPPortforwardServer):
    def __init__(self, async_service, checkpoint_handler, server_host, server_port, cb_ready, cb_closed, cb_error):
        tunnel_endpoint_tcp.TCPPortforwardServer.__init__(self, async_service, checkpoint_handler, server_host, server_port)
        self.server_host = server_host
        self.server_port = server_port
        self.cb_ready = cb_ready
        self.cb_closed = cb_closed
        self.cb_error = cb_error

    def ready(self):
        with self.checkpoint_handler.CheckpointScope("PortForwardServer::ready", components.traffic.module_id, checkpoint.DEBUG, server_host=self.server_host, server_port=self.server_port) as cps:
            self.cb_ready()

    def closed(self):
        with self.checkpoint_handler.CheckpointScope("PortForwardServer::closed", components.traffic.module_id, checkpoint.DEBUG, server_host=self.server_host, server_port=self.server_port) as cps:
            self.cb_closed()

    def error(self, message):
        self.cb_error('server_portforward', 0, message)
