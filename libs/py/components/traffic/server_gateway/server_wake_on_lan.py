from lib import checkpoint
from lib import encode

import components.traffic
from components.traffic.server_common import database_schema
import lib.wake_on_lan


class LaunchWOL(object):
    def __init__(self, checkpoint_handler, launch_id, expand_variables):
        with checkpoint_handler.CheckpointScope("LaunchPortForward::_wake", components.traffic.module_id, checkpoint.DEBUG, launch_id=launch_id):
            launch_element = database_schema.LaunchElement.lookup(launch_id)
            if launch_element:
                mac_addr = encode.preferred(expand_variables(launch_element.command).strip())
                for portforward_spec in launch_element.portforwards:
                    broadcast_address = encode.preferred(expand_variables(portforward_spec.server_host).strip())
                    broadcast_port = portforward_spec.server_port
                    lib.wake_on_lan.wake(mac_addr, broadcast_address, broadcast_port)
            else:
                checkpoint_handler.Checkpoint("LaunchPortForward::_wake", components.traffic.module_id, checkpoint.ERROR, message="Launch id not found")
