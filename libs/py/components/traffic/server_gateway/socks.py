"""
SOCKS protocol util definitions and support
"""

def quad_octet_to_ip4(quad_octet):
    """
    >>> quad_octet_to_ip4('1234')
    '49.50.51.52'
    """
    assert len(quad_octet) == 4
    return '.'.join(str(ord(octet)) for octet in quad_octet)

def hexdeca_octet_to_ip6(hexdeca_octet):
    """
    >>> hexdeca_octet_to_ip6('1234567890123456')
    '3132:3334:3536:3738:3930:3132:3334:3536'
    """
    assert len(hexdeca_octet) == 16
    return ':'.join('%02x%02x' % (ord(hexdeca_octet[2*i]), ord(hexdeca_octet[2*i+1])) for i in range(8))

def double_octet_to_port(double_octet):
    """
    >>> double_octet_to_port('XY')
    22617
    """
    assert len(double_octet) == 2
    return (ord(double_octet[0]) << 8) + ord(double_octet[1])

def ip4_to_octets(ip_str):
    """
    >>> ip4_to_octets('50.51.52.53')
    '2345'
    >>> ip4_to_octets('50.51.52.53.54')
    >>> ip4_to_octets('foo.bar.baz.com')
    """
    chunks = ip_str.split('.')
    if len(chunks) == 4:
        try:
            return ''.join(chr(int(octet_str)) for octet_str in chunks)
        except ValueError:
            pass

def port_to_octets(port):
    """
    >>> port_to_octets(22617)
    'XY'
    """
    return chr(port >> 8) + chr(port & 255)

SOCKS4 = 'SOCKS4'
SOCKS4a = 'SOCKS4a'
SOCKS5 = 'SOCKS5'

VER_4 = '\x04'
VER_5 = '\x05'

METHOD_NO_AUTH = '\x00'
METHOD_GSSAPI = '\x01'
METHOD_USERNAME_PASSWORD = '\x02'

CMD_CONNECT = '\x01'
CMD_BIND = '\02'
CMD_UDP_ASSOCIATE = '\03'

ATYP_IP_V4_address = '\01'
ATYP_DOMAINNAME = '\03'
ATYP_IP_V6_address = '\04'

REP4_version = '\x00'
REP4_request_granted = '\x5a' # 90
REP4_request_rejected_or_failed = '\x5b' # 91
REP4_request_rejected_becasue_SOCKS_server_cannot_connect_to_identd_on_the_client = '\x5c' # 92
REP4_request_rejected_because_the_client_program_and_identd_report_different_user_ids = '\x5d' # 93

REP5_succeeded = '\x00'
REP5_general_SOCKS_server_failure = '\x01'
REP5_connection_not_allowed_by_ruleset = '\x02'
REP5_Network_unreachable = '\x03' # Not used - can't distinguish from Connection Refused
REP5_Host_unreachable = '\x04' # Not used - can't distinguish from Connection Refused
REP5_Connection_refused = '\x05'
REP5_TTL_expired = '\x06'
REP5_Command_not_supported = '\x07'
REP5_Address_type_not_supported = '\x08'

def rep5_err_msg(code):
    if code == REP5_general_SOCKS_server_failure:
        return 'General SOCKS server failure'
    if code == REP5_connection_not_allowed_by_ruleset:
        return 'Access Denied'
    if code == REP5_Network_unreachable:
        return 'Network unreachable'
    if code == REP5_Host_unreachable:
        return 'Host unreachable'
    if code == REP5_Connection_refused:
        return 'Connection refused'
    if code == REP5_TTL_expired:
        return 'TTO expired'
    if code == REP5_Command_not_supported:
        return 'Command not supported'
    if code == REP5_Address_type_not_supported:
        return 'Address type not supported'
