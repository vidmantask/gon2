"""
This file contains the database schema for the traffic component
"""
from __future__ import with_statement
from components.database.server_common import schema_api
from components.database.server_common import database_api


dbapi = schema_api.SchemaFactory.get_creator("templates")


def connect_to_database(environment):
    dbapi.bind(environment.get_default_database())


table_template_use = dbapi.create_table("template_use",
                                        schema_api.Column('template_name', schema_api.String(256), nullable=False),
                                        schema_api.Column('element_id', schema_api.String(256), nullable=False), # Note: action_id for launch_specs
                                        )

table_template_value = dbapi.create_table("template_value",
                                            schema_api.Column('template_use_id', schema_api.Integer, nullable=False),
                                            schema_api.Column('field_name', schema_api.String(256), nullable=False),
                                            schema_api.Column('value', schema_api.Text),
                                            )
    

dbapi.add_unique_constraint(table_template_use, "template_name", "element_id")
dbapi.add_unique_constraint(table_template_value, "template_use_id", "field_name")
dbapi.add_foreign_key_constraint(table_template_value, 'template_use_id', table_template_use, ondelete="CASCADE")

schema_api.SchemaFactory.register_creator(dbapi)

class TemplateUse(database_api.PersistentObject):
    
    
    def add_template_value(self, field_name, value):
        template_value = TemplateValue(field_name=field_name, value=value)
        self.template_values.append(template_value)

    def update_template_values(self, dbt, value_dict):
        for template_value in self.template_values:
            if value_dict.has_key(template_value.field_name):
                new_value = value_dict.pop(template_value.field_name)
                template_value.value = new_value
            else:
                dbt.delete(template_value)
        for (field_name, value) in value_dict.items():
            self.add_template_value(field_name, value)
        
    ''' 
    def update_template_values(self, dbt, values):
        for template_value in self.template_values:
            if (len(values)==0):
                dbt.delete(template_value)
            else:
                (field_name, value) = values.pop()
                template_value.field_name = field_name
                template_value.value = value
        for (field_name, value) in values:
            self.add_template_value(field_name, value)
    ''' 
        
    def get_value(self, name):
        for template_value in self.template_values:
            if template_value.field_name == name:
                return template_value.value
        return None
             

class TemplateValue(database_api.PersistentObject):
    pass

schema_api.mapper(TemplateValue, table_template_value)
schema_api.mapper(TemplateUse, table_template_use, relations=[schema_api.Relation(TemplateValue, 'template_values')])


def get_template_use(dbt, element_id):
    dbt = database_api.SessionWrapper(dbt, read_only=True)
    template_use = dbt.select_first(TemplateUse, filter=(TemplateUse.element_id == element_id))
    return template_use

def create_template_use(dbt, template_name, element_id):
    dbt = database_api.SessionWrapper(dbt, read_only=False)
    template_use = TemplateUse(template_name=template_name, element_id=element_id)
    dbt.add(template_use)
    dbt.close()
    return template_use
    
def delete_template_use(dbt, element_id):
    template_use = get_template_use(dbt, element_id)
    if template_use:
        for template_value in template_use.template_values:
            dbt.delete(template_value)
        dbt.delete(template_use)
    
    return True



    