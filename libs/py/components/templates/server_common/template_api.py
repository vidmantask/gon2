from __future__ import with_statement
import os

import xmltodict
from components.rest.rest_api import RestApi
from components.database.server_common import database_api
import lib.checkpoint
import database_schema
import threading
import time

class TemplateHandler(object):

    _base_path = "."

    _template_map = None
    _template_file_map = None
    _load_thread = None
    _template_errors = dict()

    def __init__(self, checkpoint_handler):
        if not os.path.exists(self._base_path):
            raise Exception("Template base path '%s' does not exist" % self._base_path)
        self._template_map = TemplateHandler._template_map
        self._template_file_map = TemplateHandler._template_file_map
        self._checkpoint_handler = checkpoint_handler

    def wait_for_load(self):
        while(self._load_thread.isAlive()):
            print "Waiting for load to finish"
            time.sleep(1)

    @staticmethod
    def _xml_postprocessor(path, key, value):
        components = key.split("_")
        key = components[0] + "".join(x.title() for x in components[1:])
        bool_parameters = [
            "editEnabled",
            "deleteEnabled",
            "useAsTemplate",
            "readOnly",
            "editAllowed",
            "emptyAllowed",
            "mandatory",
            "secret",
            "advanced"
        ]
        int_parameters = ["maxLength"]
        if value is not None:
            if key in bool_parameters:
                if "emptyAllowed" == key:
                    if "0" == value:
                        value = False
                    else:
                        value = True
                else:
                    if "true" == value.lower():
                        value = True
                    elif "false" == value.lower():
                        value = False
            elif key in int_parameters:
                value = int(value)
        return key, value

    @classmethod
    def _read_template_file(self, file_path):
        f = file(file_path)
        source_str = f.read()
        f.close()
        template_dict = xmltodict.parse(source_str, attr_prefix="", dict_constructor=dict,
                                        postprocessor=self._xml_postprocessor)
        template_dict = next(iter(template_dict.values()))
        template_dict.pop("xmlns", None)
        template = RestApi.Admin.ConfigTemplate.ConfigurationTemplate()(**template_dict)
        self.check_template(template)
        return template

    @classmethod
    def set_base_path(cls, base_path):
        cls._base_path = base_path
#        cls._read_templates()
        cls._load_thread = threading.Thread(name="yo", target=cls._read_templates)
        cls._load_thread.daemon = True
        cls._load_thread.start()

    @classmethod
    def _run_in_thread(self, name, f):
        """Run f in a daemon thread, ensuring that only one runs at a time"""
        def locked_f():
            #print 'trying', name
            with self._run_in_thread_lock:
                #print 'running', name
                f()
                #print 'finished', name
        t = threading.Thread(name=name, target=locked_f)
        t.daemon = True
        t.start()


    @classmethod
    def check_template(self, template ):
        for field in template.get_field:
            field_type = field.get_field_type
            if not field_type in ['edit', 'custom_template', 'hidden']:
                raise Exception("Unknown field type '%s'" % field_type)

    @classmethod
    def _read_templates(cls):
        if cls._template_map is None:
            cls._template_map = dict()
            cls._template_file_map = dict()
            file_paths = cls._get_template_file_paths()
            cls._read_template_files(file_paths)
#            TemplateHandler._template_map = cls._template_map
#            TemplateHandler._template_file_map = self._template_file_map


    @classmethod
    def _read_template_files(cls, file_paths, checkpoint_handler=None):
        for file_path in file_paths:
            print "read " + file_path

            try:
                template = cls._read_template_file(file_path)
                cls._template_map[template.get_name] = template
                last_changed = os.path.getmtime(file_path)
                cls._template_file_map[template.get_name] = (file_path, last_changed)
                cls._template_errors.pop(file_path, None)
            except Exception, e:
                if checkpoint_handler:
                    cls._report_error(checkpoint_handler, file_path, e)
                else:
                    print e
                cls._template_errors[file_path] = e



    @classmethod
    def _report_error(self, checkpoint_handler, filename, error):
        checkpoint_handler.Checkpoint("load_template", "template_api", lib.checkpoint.ERROR, filename=filename, error=error)


    @classmethod
    def _get_template_file_paths(self):
        template_file_names = []
        file_names = os.listdir(self._base_path)
        for file_name in file_names:
            name, ext = os.path.splitext(file_name)
            if ext==".xml":
                file_path = os.path.join(self._base_path, file_name)
                template_file_names.append(file_path)
        return template_file_names

    def _reload_templates(self):
        print "reload_templates"
        if self._template_map is None:
            self._read_templates()
            for filename, error in self._template_errors.items():
                self._report_error(self._checkpoint_handler, filename, error)
        else:
            with self._checkpoint_handler.CheckpointScope("_reload_templates", "template_api", lib.checkpoint.DEBUG) as cps:
                templates_to_reload = []
                self._template_errors.clear()
                file_paths = set(self._get_template_file_paths())
                for (name, (file_path, last_changed)) in self._template_file_map.items():
                    if os.path.exists(file_path):
                        now_last_changed = os.path.getmtime(file_path)
                        if now_last_changed != last_changed:
                            self._template_map.pop(name)
                            self._template_file_map.pop(name)
                        else:
                            try:
                                file_paths.remove(file_path)
                            except KeyError:
                                pass
                    else:
                        self._template_map.pop(name)
                        self._template_file_map.pop(name)


                cps.add_complete_attr(no_of_files=len(file_paths))
                self._read_template_files(file_paths, self._checkpoint_handler)



    def clear_cache(self):
        self.wait_for_load()
        self._template_map = None
        self._template_file_map = None
        self._template_errors.clear()


    def get_templates(self):
        self.wait_for_load()
        self._reload_templates()
        return self._template_map.values()

    def get_error_template_names(self):
        return [os.path.split(file_path)[1] for file_path in self._template_errors.keys()]

    def get_template_defs(self, entity_type):
        self.wait_for_load()
        element_templates = self.get_templates()
        template_refs = []
        for template in element_templates:
            template_ref = RestApi.Admin.TemplateRef()
            template_ref.set_template_name(template.get_name)
            template_ref.set_template_title(template.get_title)
            template_ref.set_entity_type(template.get_entity_type)
            template_ref.set_template_id(-1)
            template_refs.append(template_ref)
        return template_refs

    def get_template_copy(self, name):
        self.wait_for_load()
        self._reload_templates()
        file_tuple = self._template_file_map.get(name)
        if file_tuple:
            file_path = file_tuple[0]
            if file_path:
                return self._read_template_file(file_path)
        return None


    def get_element_template(self, element_id):
        self.wait_for_load()
        with database_api.ReadonlySession() as dbt:
            template_use = database_schema.get_template_use(dbt, element_id)
            if template_use:
                template = self.get_template_copy(template_use.template_name)
                if template:
                    for field in template.get_field:
                        saved_value = template_use.get_value(field.get_name)
                        if not saved_value is None:
                            field.set_default_value(saved_value)
                    return template
            return None




    def save_template_values(self, dbt, template, element_id):
        dbt = database_api.SessionWrapper(dbt, read_only=False)
        template_name = template.get_name
        template_use = database_schema.get_template_use(dbt, element_id)
        if not template_use:
            template_use = database_schema.create_template_use(dbt, template_name, element_id)
        value_dict = dict()
        for field in template.get_field:
            value_dict[field.get_name] = field.get_default_value
        template_use.update_template_values(dbt, value_dict)
        dbt.close()

    def delete_template_use(self, dbt, element_id):
        dbt = database_api.SessionWrapper(dbt, read_only=False)
        database_schema.delete_template_use(dbt, element_id)
        dbt.close()


