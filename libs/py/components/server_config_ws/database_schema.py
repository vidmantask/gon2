from __future__ import with_statement
from components.database.server_common import database_api

import lib.version

dbapi = database_api.SchemaFactory.get_creator("server_management_config")


def connect_to_database(environment):
    dbapi.bind(environment.get_default_database())

def connect_to_database_using_connection(db_connection):
    dbapi.bind(db_connection)


table_version = dbapi.create_table("version",
                                   database_api.Column('major', database_api.Integer),
                                   database_api.Column('minor', database_api.Integer),
                                   database_api.Column('bugfix', database_api.Integer),
                                   database_api.Column('build_id', database_api.Integer),
                                   database_api.Column('previous', database_api.Integer),
                                   database_api.Column('next', database_api.Integer),
                                   )

database_api.SchemaFactory.register_creator(dbapi)

class Version(database_api.PersistentObject):

    def as_version(self):
       return lib.version.Version(self.major, self.minor, self.bugfix, self.build_id)

    def get_version_major(self):
        return self.major
    
    def get_version_minor(self):
        return self.minor

    def get_version_bugfix(self):
        return self.bugfix

    def get_version_build_id(self):
        return self.build_id
    
    def __repr__(self):
        return "%s.%s.%s.%s" % (self.major, self.minor, self.bugfix, self.build_id)

    
    def __init__(self, major, minor, bugfix, build_id):
        database_api.PersistentObject.__init__(self)
        self.major = major
        self.minor = minor
        self.bugfix = bugfix
        self.build_id = build_id

    @classmethod
    def create_from_version(cls, version):
        return Version(version.get_version_major(), version.get_version_minor(), version.get_version_bugfix(), version.get_version_build_id()) 


def create_mapper():
    database_api.mapper(Version, table_version)


create_mapper()

def set_db_current_version(db_session=None):
    return get_db_version(db_session)

def get_db_version(db_session=None):
    version = lookup_db_version(db_session)
    if version is None:
        with database_api.Transaction() as transaction:
            build_version = lib.version.Version.create_current()
            version = Version(major=build_version.get_version_major(),
                              minor=build_version.get_version_minor(),
                              bugfix=build_version.get_version_bugfix(),
                              build_id=build_version.get_version_build_id(),
                              )
            transaction.add(version)
    return version

def lookup_db_version(db_session=None):
    version = None
    with database_api.SessionWrapper(db_session, True) as session:
        versions = session.select(Version, Version.next == None)
        if len(versions)==1:
            version = versions[0]
        if len(versions)>1:
            raise Exception("Ambiguous version info in database")
    return version

def find_mmb(version, db_session=None):
    db_version = None
    with database_api.SessionWrapper(db_session, True) as session:
        db_versions = session.select(Version, database_api.and_(Version.major == version.get_version_major(), 
                                                                Version.minor == version.get_version_minor(),
                                                                Version.bugfix == version.get_version_bugfix()))
        if len(db_versions) > 0:
            db_version = db_versions[0]
    return db_version
