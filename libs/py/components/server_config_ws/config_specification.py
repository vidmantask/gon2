"""
This module contains...
"""
import lib.appl.config
import lib.version
import lib.commongon

import components.server_config_ws

import components.database.server_common.database_api as database_api
import components.database.server_common.database_config_api as database_config_api

import plugin_types.server_config.plugin_type_config_specification as ptcs

from components.plugin.server_common.config_api_lib import ConfigTemplateSpec

class ServerSpecificationManagementServer(ptcs.PluginTypeConfigSpecification):
    ID = 'server_management_server'
    TITLE = 'G/On Management Server Configuration'
    DESCRIPTION = 'This is the description of Management Server Configuration'

    def __init__(self, checkpoint_handler, server_configuration):
        ptcs.PluginTypeConfigSpecification.__init__(self, checkpoint_handler, server_configuration, components.server_config_ws.MODULE_ID, 'server_config_server_management')
        self.server_configuration = server_configuration

    def get_config_specification(self):
        config_spec = ConfigTemplateSpec()
        config_spec.init(ServerSpecificationManagementServer.ID, ServerSpecificationManagementServer.TITLE, ServerSpecificationManagementServer.DESCRIPTION)

        config_spec.add_field('management_ws_ip', 'Web Service Listen Address', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, tooltip="IP address of the network interface where the Management Server should listen for connections from the Management Client.\nAddress: 0.0.0.0 means all network interfaces on the machine.")
        config_spec.add_field('management_ws_port', 'Web Service Listen Port', ConfigTemplateSpec.VALUE_TYPE_INTEGER, advanced=False, tooltip="TCP port where the Management Server should listen for connections from the Management Client")
        config_spec.add_field('service_management_listen_ip', 'Gateway Server Listen Address', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, tooltip="IP address of the network interface where the Management Server should listen for connections from the Gateway Servers.\nAddress: 0.0.0.0 means all network interfaces on the machine.")
        config_spec.add_field('service_management_listen_port', 'Gateway Server Listen Port', ConfigTemplateSpec.VALUE_TYPE_INTEGER, advanced=False, tooltip="TCP port where the Management Server should listen for connections from the Gateway Servers.")
        config_spec.add_field('log_enabled', 'Logging enabled', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, advanced=True)
        config_spec.add_field('log_verbose', 'Logging verbose level', ConfigTemplateSpec.VALUE_TYPE_INTEGER, advanced=True)
        config_spec.add_field('enrollment_activate_assignment_rule', 'Automatic Approval of Enrollment Requests', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN , advanced=True)
        config_spec.add_field('portscan_enabled', 'Port scan enabled', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN , advanced=True)
        config_spec.add_field('portscan_ip_ranges', 'Port scan Address ranges', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=True)

        config_spec.set_values_from_object(self.server_configuration.configuration_server_management)

        return config_spec.get_template()

    def save(self, template):
        if template.get_name != ServerSpecificationManagementServer.ID:
            return None

        config_spec = ConfigTemplateSpec(template)
        config_spec.update_obj_with_values(self.server_configuration.configuration_server_management)
        return self.create_save_response(True)


class ServerSpecificationDatabaseBase(ptcs.PluginTypeConfigSpecification):

    INSTALL = 0
    CHANGE = 1
    UPGRADE = 2

    def __init__(self, checkpoint_handler, server_configuration, ID):
        ptcs.PluginTypeConfigSpecification.__init__(self, checkpoint_handler, server_configuration, components.server_config_ws.MODULE_ID, ID)
        self._old_database_name = None
        self._old_host_name = None


    def _get_config_specification(self, database_type, server_configuration, server_gateway_configuration, include_encoding):

        if database_type=="sqlite":
            return None
        elif database_type=="mssql":
            config_spec = ConfigTemplateSpec()
            config_spec.init(self.ID, "SQL Server database setup", "Setup SQL Server database")

            is_change = self.get_wizard_type() == self.CHANGE
            is_upgrade = self.get_wizard_type() == self.UPGRADE

            config_spec.add_field('db_type', 'Database type', ConfigTemplateSpec.VALUE_TYPE_STRING, field_type=ConfigTemplateSpec.FIELD_TYPE_HIDDEN)
            field = config_spec.add_field('db_host', 'Server', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, mandatory=True, tooltip="Host name or IP. Do not add port number here.\nUse port field below")
            config_spec.add_test_config_action(field, self.ID, title="Test Connection", test_type='test_connection')
            field = config_spec.add_field('db_database', 'Database Name', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, mandatory=True)
            if not is_change:
                config_spec.add_test_config_action(field, self.ID, title="Create database", test_type='create_database')
            config_spec.add_field('db_username', 'Username', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, tooltip="Leave blank to use NT credentials")
            field_password = config_spec.add_field('db_password', 'Password', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, tooltip="Leave blank to use NT credentials")
            field_password.set_secret(True)
            config_spec.add_field('db_username_gateway', 'Gateway server Username', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, tooltip="Leave blank to use same credentials on\nManagement and Gateway server")
            field_password = config_spec.add_field('db_password_gateway', 'Gateway server Password', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, tooltip="Leave blank to use same credentials on\nManagement and Gateway server")
            field_password.set_secret(True)

            config_spec.add_field('db_port', 'Port', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=True, tooltip="Leave blank for default port", value=1433)
            config_spec.add_field('db_query', 'Options', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=True, tooltip="Comma separated list of options to be passed to the database upon connect.\nExample: Driver={SQL Native Client},Failover_Partner=PartnerServerName")
            config_spec.add_field('db_log_enabled', 'Logging enabled', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, advanced=True)
            if include_encoding:
                config_spec.add_field('db_encoding', 'Encoding', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=True)
            else:
                config_spec.add_field('db_encoding', 'Encoding', ConfigTemplateSpec.VALUE_TYPE_STRING, field_type=ConfigTemplateSpec.FIELD_TYPE_HIDDEN)
#            config_spec.add_field('_db_test_database', 'Test connection', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, tooltip="Set to test database connection when 'Next' is pressed")

            if is_change or is_upgrade:
                config_spec.set_values_from_object(server_configuration)
                config_spec.set_value('db_username_gateway', server_gateway_configuration.db_username)
                config_spec.set_value('db_password_gateway', server_gateway_configuration.db_password)
                if is_upgrade:
                    version = lib.version.Version.create_current()
                    config_spec.set_value("db_database", _get_database_name(version))
            else:
                config_spec.set_value("db_type", server_configuration.db_type)


            return config_spec.get_template()
        elif database_type=="mysql":
            config_spec = ConfigTemplateSpec()
            config_spec.init(self.ID, "MySQL database setup", "Setup MySQL database")

            is_change = self.get_wizard_type() == self.CHANGE
            is_upgrade = self.get_wizard_type() == self.UPGRADE

            config_spec.add_field('db_type', 'Database type', ConfigTemplateSpec.VALUE_TYPE_STRING, field_type=ConfigTemplateSpec.FIELD_TYPE_HIDDEN)
            field = config_spec.add_field('db_host', 'Server', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, mandatory=True, tooltip="Host name or IP. Do not add port number here.\nUse port field below")
            config_spec.add_test_config_action(field, self.ID, title="Test Connection", test_type='test_connection')
            field = config_spec.add_field('db_database', 'Database Name', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, mandatory=True)
            if not is_change:
                config_spec.add_test_config_action(field, self.ID, title="Create database", test_type='create_database')
            config_spec.add_field('db_username', 'Username', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, tooltip="Leave blank to use NT credentials")
            field_password = config_spec.add_field('db_password', 'Password', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, tooltip="Leave blank to use NT credentials")
            field_password.set_secret(True)
            config_spec.add_field('db_username_gateway', 'Gateway server Username', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, tooltip="Leave blank to use same credentials on\nManagement and Gateway server")
            field_password = config_spec.add_field('db_password_gateway', 'Gateway server Password', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, tooltip="Leave blank to use same credentials on\nManagement and Gateway server")
            field_password.set_secret(True)

            config_spec.add_field('db_port', 'Port', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=True, tooltip="Leave blank for default port", value=3306)
            config_spec.add_field('db_query', 'Options', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=True, tooltip="Comma separated list of options to be passed to the database upon connect.\nExample: Driver={SQL Native Client},Failover_Partner=PartnerServerName")
            config_spec.add_field('db_log_enabled', 'Logging enabled', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, advanced=True)
            if include_encoding:
                config_spec.add_field('db_encoding', 'Encoding', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=True, tooltip="Character set encoding for database. Do not change unless there is problems with the character set chosen at installation")
            else:
                config_spec.add_field('db_encoding', 'Encoding', ConfigTemplateSpec.VALUE_TYPE_STRING, field_type=ConfigTemplateSpec.FIELD_TYPE_HIDDEN)

            if is_change or is_upgrade:
                config_spec.set_values_from_object(server_configuration)
                if is_upgrade:
                    version = lib.version.Version.create_current()
                    config_spec.set_value("db_database", _get_database_name(version))
            else:
                config_spec.set_value("db_type", server_configuration.db_type)


            return config_spec.get_template()
        else:
            raise Exception("Unknown database type '%s'" % database_type)

    def _pre_save(self, template):
        config_spec = ConfigTemplateSpec(template)
        db_connect_info = self._create_db_connect_info(config_spec)
        database_config_api.check_database_exists(db_connect_info)
        wizard_type = self.get_wizard_type()
        if wizard_type!=self.CHANGE:
            db_connect_info.encoding = database_config_api.get_database_encoding(db_connect_info)
            config_spec.set_value("db_encoding", db_connect_info.encoding)
        else:
            db_connect_info.encoding = config_spec.get_value("db_encoding")
        rc, error_msg = database_config_api.test_connection(db_connect_info, self.checkpoint_handler)
        if not rc:
            raise Exception(error_msg)

        return config_spec

    def get_wizard_type(self):
        raise NotImplementedError()



    def _create_db_connect_info(self, config_spec):
        db_connect_info = database_api.DB_ConnectInfo()
        db_connect_info.db_type = config_spec.get_value("db_type")
        db_connect_info.username = config_spec.get_value("db_username")
        db_connect_info.password = config_spec.get_value("db_password")
        db_connect_info.host = config_spec.get_value("db_host")
        db_connect_info.port = config_spec.get_value("db_port")
        db_connect_info.database = config_spec.get_value("db_database")
        db_connect_info.encoding = config_spec.get_value("db_encoding")
        db_connect_info.query = lib.commongon.string_to_dict(config_spec.get_value("db_query"))
        return db_connect_info

    def test_config(self, template, test_type=None, sub_type=None):
        if test_type == "test_connection":
            config_spec = ConfigTemplateSpec(template)
            db_connect_info = self._create_db_connect_info(config_spec)
            try:
                database_config_api.check_database_exists(db_connect_info)
                if not db_connect_info.encoding:
                    db_connect_info.encoding = database_config_api.get_database_encoding(db_connect_info)
                    config_spec.set_value("db_encoding", db_connect_info.encoding)

                return database_config_api.test_connection(db_connect_info)
            except Exception, e:
                return False, e

        if test_type == "create_database":
            config_spec = ConfigTemplateSpec(template)
            db_connect_info = self._create_db_connect_info(config_spec)
            try:
                try:
                    database_config_api.check_database_exists(db_connect_info)
                    return True, "The database already exists"
                except:
                    pass
                database_config_api.create_database(db_connect_info)
                return True, "The database was created"
            except Exception, e:
                return False, e


class ServerSpecificationDatabaseUpgrade(ServerSpecificationDatabaseBase):
    ID = 'database_upgrade'

    def __init__(self, checkpoint_handler, server_configuration, upgrade_session):
        ServerSpecificationDatabaseBase.__init__(self, checkpoint_handler, server_configuration, self.ID)
        self.server_configuration = server_configuration
        self.changed_management_server_configuration = None
        self.upgrade_session = upgrade_session

    def get_config_specification(self):
        if self.upgrade_session.to_version.equal_major_minor_bugfix(self.upgrade_session.from_version):
            return None
        from_management_configuration = self.upgrade_session.load_to_management_configuration()
        from_gateway_configuration = self.upgrade_session.load_to_gateway_configuration()
        database_type = from_management_configuration.db_type
        self.changed_management_server_configuration = from_management_configuration
        return self._get_config_specification(database_type, from_management_configuration, from_gateway_configuration, False)

    def get_wizard_type(self):
        return self.UPGRADE


    def save(self, template):
        if template.get_name != ServerSpecificationDatabaseUpgrade.ID:
            return None

        if self.changed_management_server_configuration:


            try:
                config_spec = self._pre_save(template)
            except Exception, e:
                return self.create_save_response(False, "db_host", message=e)

            database = config_spec.get_value("db_database")
            host = config_spec.get_value("db_host")
            if self._old_database_name and database == self._old_database_name and host == self._old_host_name:
                return self.create_save_response(False, "_db_database_name", message="You cannot upgrade to the same database.")


            config_spec.update_obj_with_values(self.changed_management_server_configuration)
            self.upgrade_session.save_to_management_configuration(self.changed_management_server_configuration)

            gateway_configuration = self.upgrade_session.load_to_gateway_configuration()
            config_spec.update_obj_with_values(gateway_configuration)
            if config_spec.get_value("db_username_gateway"):
                gateway_configuration.db_username = config_spec.get_value("db_username_gateway")
                gateway_configuration.db_password = config_spec.get_value("db_password_gateway")
            self.upgrade_session.save_to_gateway_configuration(gateway_configuration)

            config_service_configuration = self.upgrade_session.load_to_config_service_configuration()
            config_spec.update_obj_with_values(config_service_configuration)
            self.upgrade_session.save_to_config_service_configuration(config_service_configuration)

        return self.create_save_response(True)



def _get_database_name(version):
    version_str = "%d%d%d" % (version.get_version_major(), version.get_version_minor(), version.get_version_bugfix())
    return "gon%s" % version_str


class ServerSpecificationDatabase(ServerSpecificationDatabaseBase):
    ID = 'database'

    def __init__(self, checkpoint_handler, server_configuration, is_install):
        ServerSpecificationDatabaseBase.__init__(self, checkpoint_handler, server_configuration, self.ID)
        self.server_configuration = server_configuration
        self.is_install = is_install

    def get_config_specification(self):
        database_type = self.server_configuration.get_database_type()
        return self._get_config_specification(database_type,
                                              self.server_configuration.configuration_server_management,
                                              self.server_configuration.configuration_server_gateway,
                                              not self.is_install)

    def get_wizard_type(self):
        if self.is_install:
            return self.INSTALL
        else:
            return self.CHANGE

    def save(self, template):
        if template.get_name != ServerSpecificationDatabase.ID:
            return None

        try:
            config_spec = self._pre_save(template)
        except Exception, e:
            return self.create_save_response(False, "db_host", message=e)

        config_spec.update_obj_with_values(self.server_configuration.configuration_server_management)
        config_spec.update_obj_with_values(self.server_configuration.configuration_server_gateway)
        if config_spec.get_value("db_username_gateway"):
            self.server_configuration.configuration_server_gateway.db_username = config_spec.get_value("db_username_gateway")
            self.server_configuration.configuration_server_gateway.db_password = config_spec.get_value("db_password_gateway")
        config_spec.update_obj_with_values(self.server_configuration.configuration_server_config)

        return self.create_save_response(True)




class ServerSpecificationDatabaseChoice(ptcs.PluginTypeConfigSpecification):
    ID = 'database_choice'
    TITLE = 'G/On Database'
    DESCRIPTION = 'Choose database type'

    def __init__(self, checkpoint_handler, server_configuration, is_install):
        ptcs.PluginTypeConfigSpecification.__init__(self, checkpoint_handler, server_configuration, components.server_config_ws.MODULE_ID, "server_config_" + self.ID)
        self.server_configuration = server_configuration
        self.is_install = is_install

    def get_config_specification(self):
        if self.is_install:
            database_type = None
        else:
            database_type = self.server_configuration.configuration_server_management.db_type
        if not database_type:
            config_spec = ConfigTemplateSpec()
            config_spec.init(ServerSpecificationDatabaseChoice.ID, ServerSpecificationDatabaseChoice.TITLE, ServerSpecificationDatabaseChoice.DESCRIPTION)

            field = config_spec.add_field('database_type', 'Choose Database Provider', ConfigTemplateSpec.VALUE_TYPE_STRING, value="sqlite")
            config_spec.add_selection_choice(field, "Internal (SQLite)", "sqlite")
            config_spec.add_selection_choice(field, "Microsoft SQL Server", "mssql")
            #config_spec.add_selection_choice(field, "MySQL", "mysql")

            return config_spec.get_template()
        else:
            self.server_configuration.set_database_type(database_type)
            return None

    def save(self, template):
        if template.get_name != ServerSpecificationDatabaseChoice.ID:
            return None

        config_spec = ConfigTemplateSpec(template)
        database_type  = config_spec.get_value("database_type")
        self.server_configuration.set_database_type(database_type)

        self.server_configuration.configuration_server_management.db_type = database_type

        if database_type=="sqlite":
            self.server_configuration.configuration_server_management.set_to_default()
            self.server_configuration.configuration_server_gateway.set_to_default()
            self.server_configuration.configuration_server_config.set_to_default()

        return self.create_save_response(True)

class ServerSpecificationGatewayServer(ptcs.PluginTypeConfigSpecification):
    ID = 'server_gateway_server'
    TITLE = 'G/On Gateway Server Configuration'
    DESCRIPTION = 'This is the description of Gateway Server Configuration'

    def __init__(self, checkpoint_handler, server_configuration):
        ptcs.PluginTypeConfigSpecification.__init__(self, checkpoint_handler, server_configuration, components.server_config_ws.MODULE_ID, 'server_config_server_gateway')
        self.server_configuration = server_configuration

    def get_config_specification(self):
        license = self.server_configuration.get_license_handler().get_license()
        if license.get_one('Client Connect Address') == '*':
            service_client_connect_addresses_read_only = False
        else:
            service_client_connect_addresses = license.get_all('Client Connect Address')
            if not service_client_connect_addresses:
                raise Exception("Invalid license: Missing 'Client Connect Address' info")
            service_client_connect_addresses_read_only = True
            self.server_configuration.configuration_server_gateway.service_client_connect_addresses = service_client_connect_addresses

        if license.get_one('Client Connect Port') == '*':
            service_client_connect_ports_read_only = False
        else:
            service_client_connect_ports_read_only = True
            self.server_configuration.configuration_server_gateway.service_client_connect_ports = license.get_all('Client Connect Port')

        config_spec = ConfigTemplateSpec()
        config_spec.init(ServerSpecificationGatewayServer.ID, ServerSpecificationGatewayServer.TITLE, ServerSpecificationGatewayServer.DESCRIPTION)

        config_spec.add_field('service_ip', 'Listen Address', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=True)
        config_spec.add_field('service_port', 'Listen Port', ConfigTemplateSpec.VALUE_TYPE_INTEGER, advanced=False)
        config_spec.add_field('service_client_connect_addresses', 'Client Connect Addresses', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, read_only=service_client_connect_addresses_read_only)
        config_spec.add_field('service_client_connect_ports', 'Client Connect Ports', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, read_only=service_client_connect_ports_read_only)

        config_spec.add_field('service_management_connect_ip', 'Management Server Connect Address', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, tooltip="IP address or DNS name, which the Gateway Servers should use for connecting to the Management Server.")
        config_spec.add_field('service_management_connect_port', 'Management Server Connect Port', ConfigTemplateSpec.VALUE_TYPE_INTEGER, advanced=False, tooltip="TCP port, which the Gateway Servers should use for connecting to the Management Server.")

        config_spec.add_field('login_show_last_login', 'Show last login name at login', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, advanced=False, tooltip="If enabled, the user is presented with the full login last accepted on that particular client (token), e.g. 'user@mydomain.com'")
        config_spec.add_field('login_show_last_user_directory', 'Show last user directory at login', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, advanced=False, tooltip="If enabled, the user is presented with the part of the login containing the user directory of the last accepted login on that particular client (token), e.g. '@mydomain.com'")

        config_spec.add_field('log_enabled', 'Logging enabled', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, advanced=True)
        config_spec.add_field('log_verbose', 'Logging verbose level', ConfigTemplateSpec.VALUE_TYPE_INTEGER, advanced=True)

        config_spec.add_field('log_session_enabled', 'Session logging enabled', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN , advanced=True)
        config_spec.add_field('log_session_enabled_by_remote', 'Session logging enabled by remote', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN , advanced=True)
        config_spec.add_field('log_settings_traffic', 'Enabled traffic log settings', ConfigTemplateSpec.VALUE_TYPE_STRING , advanced=True, tooltip="List of enabled log settings, e.g. http,tcp")

        config_spec.add_field('session_timeout_min', 'User session timeout (minutes)', ConfigTemplateSpec.VALUE_TYPE_INTEGER, advanced=True)
        config_spec.add_field('authorization_timeout_sec', 'Authorization timeout (seconds)', ConfigTemplateSpec.VALUE_TYPE_INTEGER, advanced=True)
        config_spec.add_field('cpm_cuncurrent_downloads', 'CPM Concurrent downloads', ConfigTemplateSpec.VALUE_TYPE_INTEGER, advanced=True)

        config_spec.add_field('session_offline_credential_timeout_min', 'Mobile, offline user credential timeout (minutes)', ConfigTemplateSpec.VALUE_TYPE_INTEGER, advanced=True)
        config_spec.add_field('session_close_when_entering_background', 'Mobile, close when entering background', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, advanced=True)

        config_spec.add_field('dialog_welcome_message_enabled', 'Welcome message before first access enabled', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, advanced=True, tooltip="Welcome message before first access enabled")
        config_spec.add_field('dialog_welcome_message_filename', 'Welcome message before first access file name', ConfigTemplateSpec.VALUE_TYPE_STRING , advanced=True, tooltip="Full path to file containing welcome message")
        config_spec.add_field('dialog_welcome_message_close_on_cancel', 'Welcome message before first access, close on cancel', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN , advanced=True)

#        TEMP REMOVED: For device enrollment
#        config_spec.add_field('login_enable_sso', 'Enable SSO', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN , advanced=True, tooltip="Enable SSO via e.g. NTLM or Kerberos for G/On Clients")
#        config_spec.add_field('login_local_gateway_sso', 'SSO on gateway server', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN , advanced=True, tooltip="If enabled the SSO verification is done on the gateway server. In order to do so the server must be on the SSO domain. If disabled the SSO verification is done on the management server")
#        config_spec.add_field('login_security_package', 'SSO security package', ConfigTemplateSpec.VALUE_TYPE_STRING , advanced=True, tooltip="Security package used for SSO, e.g. NTLM, Kerberos, etc. Note that Kerberos requires targetSPN specification on client")

        config_spec.set_values_from_object(self.server_configuration.configuration_server_gateway)

        service_client_connect_addresses_as_string = lib.appl.config.convert_stringlist_to_string(self.server_configuration.configuration_server_gateway.service_client_connect_addresses)
        config_spec.set_value('service_client_connect_addresses', service_client_connect_addresses_as_string)

        service_client_connect_ports_as_string = lib.appl.config.convert_intlist_to_string(self.server_configuration.configuration_server_gateway.service_client_connect_ports)
        config_spec.set_value('service_client_connect_ports', service_client_connect_ports_as_string)

        log_settings_traffic_as_string = lib.appl.config.convert_stringlist_to_string(self.server_configuration.configuration_server_gateway.log_settings_traffic)
        config_spec.set_value('log_settings_traffic', log_settings_traffic_as_string)

        return config_spec.get_template()

    def save(self, template):
        if template.get_name != ServerSpecificationGatewayServer.ID:
            return None

        config_spec = ConfigTemplateSpec(template)
        config_spec.update_obj_with_values(self.server_configuration.configuration_server_gateway)

        self.server_configuration.configuration_server_gateway.service_client_connect_addresses = lib.appl.config.convert_string_to_stringlist(config_spec.get_value('service_client_connect_addresses'))
        self.server_configuration.configuration_server_gateway.service_client_connect_ports = lib.appl.config.convert_string_to_intlist(config_spec.get_value('service_client_connect_ports'))
        self.server_configuration.configuration_server_gateway.log_settings_traffic = lib.appl.config.convert_string_to_stringlist(config_spec.get_value('log_settings_traffic'))

        license = self.server_configuration.get_license_handler().get_license()
        if len(self.server_configuration.configuration_server_gateway.service_client_connect_addresses) > 1 and not license.has('Feature', 'Multiple Client Connect Addresses'):
            return self.create_save_response(False, 'service_client_connect_address', 'License prevents more that one Client Connect Address')
        if len(self.server_configuration.configuration_server_gateway.service_client_connect_ports) > 1 and not license.has('Feature', 'Multiple Client Connect Ports'):
            return self.create_save_response(False, 'service_client_connect_port', 'License prevents more that one Client Connect Port')
        return self.create_save_response(True)


class ServerSpecificationGatewayServerToH(ptcs.PluginTypeConfigSpecification):
    ID = 'server_gateway_server_toh'
    TITLE = 'G/On Gateway Server Configuration HTTP Encapsulation'
    DESCRIPTION = 'This is the description of Gateway Server Configuration for HTTP Encapsulation'

    def __init__(self, checkpoint_handler, server_configuration):
        ptcs.PluginTypeConfigSpecification.__init__(self, checkpoint_handler, server_configuration, components.server_config_ws.MODULE_ID, 'server_config_server_gateway_toh')
        self.server_configuration = server_configuration

    def get_config_specification(self):
        license = self.server_configuration.get_license_handler().get_license()
        if license.get_one('HTTP Encapsulation Client Connect Port') == '*':
            service_http_client_connect_ports_read_only = False
        else:
            service_http_client_connect_ports_read_only = True
            self.server_configuration.configuration_server_gateway.service_http_client_connect_ports = license.get_all('HTTP Encapsulation Client Connect Port')

        toh_diabled = not license.has('Feature', 'HTTP Encapsulation')

        config_spec = ConfigTemplateSpec()
        config_spec.init(ServerSpecificationGatewayServerToH.ID, ServerSpecificationGatewayServerToH.TITLE, ServerSpecificationGatewayServerToH.DESCRIPTION)

        config_spec.add_field('service_http_enabled', 'HTTP Encapsulation Enabled', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, advanced=False, read_only=toh_diabled)
        config_spec.add_field('service_http_ip', 'HTTP Encapsulation Listen Address', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=True)
        config_spec.add_field('service_http_port', 'HTTP Encapsulation Listen Ports', ConfigTemplateSpec.VALUE_TYPE_INTEGER, advanced=False)
        config_spec.add_field('service_http_client_connect_ports', 'HTTP Encapsulation Client Connect Ports', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, read_only=service_http_client_connect_ports_read_only)
        config_spec.add_field('service_http_log_2_enabled', 'HTTP Encapsulation Logging level 2 enabled', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, advanced=True)
        config_spec.add_field('service_http_log_3_enabled', 'HTTP Encapsulation Logging level 3 enabled', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, advanced=True)

        config_spec.set_values_from_object(self.server_configuration.configuration_server_gateway)

        service_http_client_connect_ports_as_string = lib.appl.config.convert_intlist_to_string(self.server_configuration.configuration_server_gateway.service_http_client_connect_ports)
        config_spec.set_value('service_http_client_connect_ports', service_http_client_connect_ports_as_string)
        return config_spec.get_template()

    def save(self, template):
        if template.get_name != ServerSpecificationGatewayServerToH.ID:
            return None

        config_spec = ConfigTemplateSpec(template)
        config_spec.update_obj_with_values(self.server_configuration.configuration_server_gateway)
        self.server_configuration.configuration_server_gateway.service_http_client_connect_ports = lib.appl.config.convert_string_to_intlist(config_spec.get_value('service_http_client_connect_ports'))

        license = self.server_configuration.get_license_handler().get_license()
        if len(self.server_configuration.configuration_server_gateway.service_client_connect_ports) > 1 and not license.has('Feature', 'Multiple Client Connect Ports'):
            return self.create_save_response(False, 'service_http_client_connect_ports', 'License prevents more that one HTTP Encapsulation Listen Port')
        return self.create_save_response(True)

