"""
This module contains the functionlaity for the server configuration
"""
from __future__ import with_statement

import sys
import os.path
import pickle
import datetime
import shutil
import hashlib
import zipfile

import lib.config
import components.license.gonlicense
import lib.gpm.gpm_analyzer
import components.config.common
import lib.appl.upgrade
import lib.utility
import lib.utility.xor_crypt

import lib.commongon

MODULE_ID = 'server_config'


class ServerConfigurationAll(object):
    INSTALLATION_STATE_NEW = 0
    INSTALLATION_STATE_DONE = 1
    INSTALLATION_STATE_FILENAME = 'installation_state'

    BACKUP_INI_FOLDER = 'ini'
    BACKUP_DEPLOYED_FOLDER = 'deployed'
    BACKUP_GPM_FOLDER = 'gpm'
    BACKUP_GPMDEFS_FOLDER = 'gpmdefs'
    BACKUP_GPMCDEFS_FOLDER = 'gpmcdefs'
    BACKUP_TEMPLATES_FOLDER = 'templates'
    BACKUP_LICENSE_FILENAME = 'gon_license.lic'
    DEFAULT_LICENSE_FILENAME = 'default.lic'
    SP_LOGS_FOLDER = 'logs'

    CHECKSUMS_FILENAME = 'checksums'

    def __init__(self, configuration_server_config):
        self._installation_state = {}
        self._installation_state['state'] = ServerConfigurationAll.INSTALLATION_STATE_NEW
        self.configuration_server_config = configuration_server_config
        self.configuration_server_management = components.config.common.ConfigServerManagement()
        self.configuration_server_management.set_ini_path(configuration_server_config.installation_server_management_abspath)
        self.configuration_server_gateway = components.config.common.ConfigServerGateway()
        self.configuration_server_gateway.set_ini_path(configuration_server_config.installation_server_gateway_abspath)
        self.configuration_client_management_service = components.config.common.ConfigClientManagementService()
        self.configuration_client_management_service.set_ini_path(configuration_server_config.installation_client_management_service_abspath)
#        self.configuration_client_gateway = components.config.common.ConfigClientGateway()
        self.configuration_client_management_service.set_ini_path(configuration_server_config.installation_client_management_service_abspath)
        self.installation_path_abs = configuration_server_config.installation_abspath
        self.instance_path_abs = os.path.join(configuration_server_config.get_instance_run_root(), '..', '..')
        self.database_type = "sqlite"

    def get_instance_path_abs(self):
        return self.instance_path_abs

    def get_installation_path_abs(self):
        return self.installation_path_abs

    def get_installers_path_abs(self):
        return os.path.join(self.installation_path_abs, 'installers')

    def get_config_service_path_abs(self):
        return os.path.normpath(os.path.join(self.configuration_server_config.get_instance_run_root(), self.configuration_server_config.installation_config_service_path))

    def get_server_management_path_abs(self):
        return os.path.normpath(os.path.join(self.configuration_server_config.get_instance_run_root(), self.configuration_server_config.installation_server_management_path))

    def get_server_gateway_path_abs(self):
        return os.path.normpath(os.path.join(self.configuration_server_config.get_instance_run_root(), self.configuration_server_config.installation_server_gateway_path))

    def get_client_management_service_path_abs(self):
        return os.path.normpath(os.path.join(self.configuration_server_config.get_instance_run_root(), self.configuration_server_config.installation_client_management_service_path))

    def get_database_type(self):
        return self.database_type

    def set_database_type(self, database_type):
        self.database_type = database_type

    def save(self):
        pass

    def load(self):
        pass

    def reload(self):
        self.configuration_server_management.read_config_file_from_folder(self.get_server_management_path_abs())
        self.configuration_server_gateway.read_config_file_from_folder(self.get_server_gateway_path_abs())
        self.configuration_client_management_service.read_config_file_from_folder(self.get_client_management_service_path_abs())

    def get_license_handler(self):
        return components.license.gonlicense.GonLicenseHandler(self.configuration_server_management.license_absfilename)

    def get_db_connect_string_abs(self):
        db_type = self.configuration_server_config.db_type
        if db_type=="sqlite":
            database_path = self.configuration_server_config.db_database_absfilename
            db_connect_string = self.configuration_server_config.get_db_connect_url(db_database=database_path)
        else:
            db_connect_string = self.configuration_server_config.get_db_connect_url()

        print "get_db_connect_string_abs db_connect_string:%s" % db_connect_string
        return db_connect_string

    def is_configured(self):
#        For testing client state without reinstalling:
#        self._installation_state = { 'state' : ServerConfigurationAll.INSTALLATION_STATE_NEW }
        self._read_installation_state()
        return self._installation_state.has_key('state') and self._installation_state['state'] ==  ServerConfigurationAll.INSTALLATION_STATE_DONE

    def create_missing_folders(self):
        """
        Check for folders that are supposed to be available, and create if not there
        This is only a help to the system admin, when he is asked to place files during installation/cofiguration.
        """
        management_server_root = self.get_server_management_path_abs()
        if os.path.exists(management_server_root):
            deployed_root = self.configuration_server_management.deployment_abspath
            if not os.path.exists(deployed_root):
                os.makedirs(deployed_root)

        client_management_service_root = self.get_client_management_service_path_abs()
        if os.path.exists(client_management_service_root):
            soft_token_root = self.configuration_client_management_service.deployment_soft_token_abspath
            if not os.path.exists(soft_token_root):
                os.makedirs(soft_token_root)


    def start_installation(self):
        """
        This method marks the installation as done
        """
        self._installation_state['state'] = ServerConfigurationAll.INSTALLATION_STATE_NEW
        self._write_installation_state()

        deploy_folder = self.configuration_server_management.deployment_abspath
        scramble_salt_filename = os.path.join(deploy_folder, components.config.common.SCRAMBLE_SALT_FILENAME)
        lib.utility.xor_crypt.generate_scramble_salt(scramble_salt_filename)
        lib.utility.xor_crypt.load_and_set_scramble_salt(scramble_salt_filename)


    def finialize_installation(self, logger, change_mode=False):
        """
        This method marks the installation as done
        """
        deploy_folder = self.configuration_server_management.deployment_abspath
        self.configuration_server_management.write_config_file(self.get_server_management_path_abs())
        self.configuration_server_gateway.write_config_file(self.get_server_gateway_path_abs())
        self.configuration_client_management_service.write_config_file(self.get_client_management_service_path_abs())
        self.configuration_server_config.write_config_file(self.get_config_service_path_abs())

        if self.configuration_server_config.setup_generate_servers:
            license = self.get_license_handler().get_license()
            client_connect_addresses = self.configuration_server_gateway.service_client_connect_addresses
            client_connect_ports = self.configuration_server_gateway.service_client_connect_ports
            toh_enabled = self.configuration_server_gateway.service_http_enabled and license.has('Feature', 'HTTP Encapsulation')
            toh_client_connect_ports = self.configuration_server_gateway.service_http_client_connect_ports

            servers_filename = os.path.join(deploy_folder, components.config.common.CLIENT_SERVERS_FILENAME)
            whitelist_filename = os.path.join(deploy_folder, components.config.common.CLIENT_FIREWALL_WHITHELIST_FILENAME)
            knownsecret_filename = os.path.join(deploy_folder, components.config.common.KS_CLIENT_FILENAME)
            qr_image_filename = os.path.join(deploy_folder, components.config.common.CONNECT_INFO_QR_IMAGE_FILENAME)
            dat_filename = os.path.join(deploy_folder, components.config.common.CONNECT_INFO_DAT_FILENAME)

            do_file_generation = True
            if change_mode:
                if os.path.exists(servers_filename) and os.path.exists(whitelist_filename):
                    if lib.appl.upgrade.has_changed(deploy_folder, components.config.common.CLIENT_SERVERS_FILENAME, lib.appl.upgrade.CHECKSUMS_LOCAL_FILENAME):
                        do_file_generation = False
                    if lib.appl.upgrade.has_changed(deploy_folder, components.config.common.CLIENT_FIREWALL_WHITHELIST_FILENAME, lib.appl.upgrade.CHECKSUMS_LOCAL_FILENAME):
                        do_file_generation = False

            if do_file_generation:
                logger.Checkpoint("finialize_installation.servers_changed", MODULE_ID, lib.checkpoint.DEBUG)
                lib.config.generate_servers(servers_filename, client_connect_addresses, client_connect_ports, toh_enabled, toh_client_connect_ports)
                lib.config.generate_gon_firewall_whitelist(whitelist_filename, client_connect_addresses, client_connect_ports, toh_enabled, toh_client_connect_ports)
                lib.appl.upgrade.generate_and_save_checksum_for_folder(deploy_folder, lib.appl.upgrade.CHECKSUMS_LOCAL_FILENAME)
            else:
                logger.Checkpoint("finialize_installation.servers_not_changed", MODULE_ID, lib.checkpoint.WARNING, message="File changed manualy")
                logger.Checkpoint("finialize_installation.firewall_whitelist_not_changed", MODULE_ID, lib.checkpoint.WARNING, message="File changed manualy")

            lib.config.generate_gon_qr_image(logger, servers_filename, knownsecret_filename, qr_image_filename, dat_filename)

        self._installation_state['state'] = ServerConfigurationAll.INSTALLATION_STATE_DONE
        self._write_installation_state()

    def _get_installation_state_filename(self):
        return os.path.join(self.configuration_server_config.deployment_abspath, ServerConfigurationAll.INSTALLATION_STATE_FILENAME)

    def _write_installation_state(self):
        installation_state_filename = os.path.normpath(self._get_installation_state_filename())
        if not os.path.exists(os.path.dirname(installation_state_filename)):
            os.makedirs(os.path.dirname(installation_state_filename))

        installation_state_file = open(installation_state_filename, 'wb')
        pickle.dump(self._installation_state, installation_state_file)
        installation_state_file.close()

    def _read_installation_state(self):
        installation_state_filename = self._get_installation_state_filename()
        if os.path.exists(installation_state_filename):
            installation_state_file = open(installation_state_filename, 'rb')
            self._installation_state = pickle.load(installation_state_file)
            installation_state_file.close()

    def generate_support_package_begin(self, logger, management_features_enabled=True):
        support_package_suffix='gsupport'
        if not management_features_enabled:
            support_package_suffix='gsupport_gateway'
        support_package_suffix = '%s_%s' % (support_package_suffix, datetime.datetime.now().isoformat('_').replace(':', ''))

        support_package_root = os.path.join(self.configuration_server_config.support_package_abspath, support_package_suffix)
        if not os.path.exists(support_package_root):
            os.makedirs(support_package_root)
        self._generate_support_package(logger, support_package_root, management_features_enabled)
        return support_package_root

    def generate_support_package_end(self, support_package_root):
        support_package_filename = os.path.join(os.path.dirname(support_package_root), '%s.zip' % os.path.basename(support_package_root))
        support_package_file = zipfile.ZipFile(support_package_filename, 'w')

        for root, dirs, files in os.walk(support_package_root):
            for filename in files:
                filename_abs = os.path.normpath(os.path.join(root, filename))
                filename_zip = filename_abs[len(os.path.normpath(support_package_root))+1:]
                support_package_file.write(filename_abs, filename_zip)
        support_package_file.close()
        shutil.rmtree(support_package_root)

    def _generate_support_package(self, logger, support_package_root, management_features_enabled=True):
        # Generating filelist
        self._generate_filelist(support_package_root)

        # Getting ini-files
        self._copy_ini_files(support_package_root, remove_sensitive_info=True, management_features_enabled=management_features_enabled)

        if management_features_enabled:
            # Getting templates files
            self._copy_templates(logger, support_package_root)

        # Getting log-files
        self._copy_logs(logger, support_package_root)

        # Copying gpm related files
        self._generate_gpm_info(logger, support_package_root)

        # Copying servers file
        deployed_path_dest = os.path.join(support_package_root, ServerConfigurationAll.BACKUP_DEPLOYED_FOLDER)
        os.makedirs(deployed_path_dest)
        deployed_path_source = os.path.join(self.configuration_server_config.deployment_abspath, components.config.common.CLIENT_SERVERS_FILENAME)
        if os.path.exists(deployed_path_source):
            shutil.copy(deployed_path_source, deployed_path_dest)

        deployed_path_source = os.path.join(self.configuration_server_config.deployment_abspath, components.config.common.CLIENT_FIREWALL_WHITHELIST_FILENAME)
        if os.path.exists(deployed_path_source):
            shutil.copy(deployed_path_source, deployed_path_dest)

        deployed_path_source = os.path.join(self.configuration_server_config.deployment_abspath, ServerConfigurationAll.BACKUP_LICENSE_FILENAME)
        if os.path.exists(deployed_path_source):
            shutil.copy(deployed_path_source, deployed_path_dest)


    def _generate_filelist(self, support_package_root):
        skip_folders = []
        skip_folders.append(self.configuration_server_config.support_package_abspath)
        skip_folders.append(self.configuration_client_management_service.deployment_soft_token_abspath)

        files_filename = os.path.join(support_package_root, 'fileslist.txt')
        files_file = open(files_filename, 'wt')
        for root, dirs, files in os.walk(self.installation_path_abs):
            if self._skip_folder(os.path.normpath(root), skip_folders):
                continue
            for filename in files:
                filename_abs = os.path.join(root, filename)
                try:
                    size = os.path.getsize(filename_abs)
                except IOError:
                    size     = 0
                try:
                    checksum = lib.utility.calculate_checksum_for_file(filename_abs)
                except IOError:
                    checksum = "unabel_to_calculate_checksum            "
                files_file.write('%s, %10d, %s\n' % (checksum, size, filename_abs))
        files_file.close()

    def _generate_gpm_info(self, logger, support_package_root, source_server_configuration=None):
        if source_server_configuration is None:
            source_server_configuration = self
        gpm_root = os.path.join(support_package_root, ServerConfigurationAll.BACKUP_GPM_FOLDER)
        gpmdefs_root = os.path.join(gpm_root, ServerConfigurationAll.BACKUP_GPMDEFS_FOLDER)
        gpmcdefs_root = os.path.join(gpm_root, ServerConfigurationAll.BACKUP_GPMCDEFS_FOLDER)

        os.makedirs(gpm_root)

        gpmdefs_root_source = source_server_configuration.configuration_server_config.cpm_gpm_defs_abspath
        lib.appl.upgrade.copy_tree_include_if_changed(gpmdefs_root_source, gpmdefs_root, logger, MODULE_ID)

        gpmcdefs_root_source = source_server_configuration.configuration_server_config.cpm_gpmc_defs_abspath
        lib.appl.upgrade.copy_tree_include_if_changed(gpmcdefs_root_source, gpmcdefs_root, logger, MODULE_ID)

        gpms_dependency_source = os.path.join(source_server_configuration.configuration_server_config.cpm_gpms_abspath, lib.gpm.gpm_analyzer.GpmRepository.GPM_DEPENDENCY_TOTAL_FILENAME)
        if os.path.exists(gpms_dependency_source):
            shutil.copy(gpms_dependency_source, gpm_root)
        gpms_dependency_error_source = os.path.join(source_server_configuration.configuration_server_config.cpm_gpms_abspath, lib.gpm.gpm_analyzer.GpmRepository.GPM_DEPENDENCY_ERROR_FILENAME)
        if os.path.exists(gpms_dependency_error_source):
            shutil.copy(gpms_dependency_error_source, gpm_root)

    def backup(self, backup_log, backup_root, cb_backup=None, source_server_configuration=None):
        if source_server_configuration is None:
            source_server_configuration = self
        self._copy_ini_files(backup_root, source_server_configuration)
        self._copy_deployment(backup_log, backup_root, cb_backup, source_server_configuration)
        self._copy_templates(backup_log, backup_root, cb_backup, source_server_configuration)
        self._generate_gpm_info(backup_log, backup_root, source_server_configuration)

    def restore(self, restore_log, restore_root, cb_restore=None, is_upgrade=False):
        """
        Using setting from ini-files to be restored, when restoring ini-files. Afterwards the instance contains the restored ini-files values.
        """
        try:
            # Restore ini-files
            ini_source = os.path.join(restore_root, ServerConfigurationAll.BACKUP_INI_FOLDER)
            self.configuration_server_management.read_config_file_from_folder(ini_source)
            if is_upgrade:
                self.configuration_server_management.reset_to_default_when_restore_durring_upgrade()
            self.configuration_server_management.write_config_file(self.get_server_management_path_abs())
            self.configuration_server_management.set_ini_path(self.get_server_management_path_abs())

            self.configuration_server_gateway.read_config_file_from_folder(ini_source)
            if is_upgrade:
                self.configuration_server_gateway.reset_to_default_when_restore_durring_upgrade()
            self.configuration_server_gateway.write_config_file(self.get_server_gateway_path_abs())
            self.configuration_server_gateway.set_ini_path(self.get_server_gateway_path_abs())

            self.configuration_server_config.read_config_file_from_folder(ini_source)
            if is_upgrade:
                self.configuration_server_config.reset_to_default_when_restore_durring_upgrade()
            self.configuration_server_config.write_config_file(self.get_config_service_path_abs())
            self.configuration_server_config.set_ini_path(self.get_config_service_path_abs())


            # Restore deployment
            deployed_root_source = os.path.join(restore_root, ServerConfigurationAll.BACKUP_DEPLOYED_FOLDER)
            deployed_root_dest = self.configuration_server_management.deployment_abspath
            lib.appl.upgrade.copy_tree_with_backup(deployed_root_source, deployed_root_dest, restore_log, MODULE_ID)

            # Restore templates
            templates_root_source = os.path.join(restore_root, ServerConfigurationAll.BACKUP_TEMPLATES_FOLDER)
            templates_root_dest =  self.configuration_server_management.templates_abspath
            lib.appl.upgrade.copy_tree_with_backup(templates_root_source, templates_root_dest, restore_log, MODULE_ID)

            # Restore gpmdefs
            gpmdefs_root_source = os.path.join(restore_root, ServerConfigurationAll.BACKUP_GPM_FOLDER, ServerConfigurationAll.BACKUP_GPMDEFS_FOLDER)
            gpmdefs_root_dest = self.configuration_server_config.cpm_gpm_defs_abspath
            lib.appl.upgrade.copy_tree_with_backup(gpmdefs_root_source, gpmdefs_root_dest, restore_log, MODULE_ID)

            # Restore gpmcdefs
            gpmcdefs_root_source = os.path.join(restore_root, ServerConfigurationAll.BACKUP_GPM_FOLDER, ServerConfigurationAll.BACKUP_GPMCDEFS_FOLDER)
            gpmcdefs_root_dest = self.configuration_server_config.cpm_gpmc_defs_abspath
            lib.appl.upgrade.copy_tree_with_backup(gpmcdefs_root_source, gpmcdefs_root_dest, restore_log, MODULE_ID)

        except:
            etype, evalue, etraceback = sys.exc_info()
            restore_log.CheckpointException("copy_deployment.folder_not_found", MODULE_ID, lib.checkpoint.ERROR, etype, evalue, etraceback)
            if cb_restore is not None:
                cb_restore.cb_restore_error("Unexpected error, see log file.")


    def _copy_ini_files(self, root, source_server_configuration=None, remove_sensitive_info=False, management_features_enabled=True):
        if source_server_configuration is None:
            source_server_configuration = self
        ini_root = os.path.join(root, ServerConfigurationAll.BACKUP_INI_FOLDER)
        os.makedirs(ini_root)

        if management_features_enabled:
            configuration_server_management = components.config.common.ConfigServerManagement()
            configuration_server_management.read_config_file_from_folder(source_server_configuration.get_server_management_path_abs())
            configuration_server_management.write_config_file(ini_root, remove_sensitive_info)
            configuration_server_management.sensitive_copy(source_server_configuration.get_server_management_path_abs(), ini_root)

        configuration_server_gateway = components.config.common.ConfigServerGateway()
        configuration_server_gateway.read_config_file_from_folder(source_server_configuration.get_server_gateway_path_abs())
        configuration_server_gateway.write_config_file(ini_root, remove_sensitive_info)
        configuration_server_gateway.sensitive_copy(source_server_configuration.get_server_gateway_path_abs(), ini_root)

        configuration_server_gateway_local = components.config.common.ConfigServerGatewayLocal()
        configuration_server_gateway_local.read_config_file_from_folder(source_server_configuration.get_server_gateway_path_abs())
        configuration_server_gateway_local.write_config_file(ini_root, remove_sensitive_info)
        configuration_server_gateway_local.sensitive_copy(source_server_configuration.get_server_gateway_path_abs(), ini_root)

        configuration_config = components.config.common.ConfigConfigService()
        configuration_config.read_config_file_from_folder(source_server_configuration.get_config_service_path_abs())
        configuration_config.write_config_file(ini_root, remove_sensitive_info)
        configuration_config.sensitive_copy(source_server_configuration.get_config_service_path_abs(), ini_root)

        configuration_config_local = components.config.common.ConfigConfigServiceLocal()
        configuration_config_local.read_config_file_from_folder(source_server_configuration.get_config_service_path_abs())
        configuration_config_local.write_config_file(ini_root, remove_sensitive_info)
        configuration_config_local.sensitive_copy(source_server_configuration.get_config_service_path_abs(), ini_root)


    def _copy_deployment(self, logger, backup_root, cb_config_backup, source_server_configuration=None):
        if source_server_configuration is None:
            source_server_configuration = self
        deployed_root = os.path.join(backup_root,  ServerConfigurationAll.BACKUP_DEPLOYED_FOLDER)
        deployed_root_source = source_server_configuration.configuration_server_management.deployment_abspath
        if os.path.exists(deployed_root_source):
            shutil.copytree(deployed_root_source, deployed_root, ignore=self._copy_deployment_ignore)
        else:
            logger.Checkpoint("copy_deployment.folder_not_found", MODULE_ID, lib.checkpoint.WARNING, deployed_root_source=deployed_root_source)
            if cb_config_backup is not None:
                cb_config_backup.cb_backup_error("Deployment folder not found, see log file.")

    def _copy_deployment_ignore(self, src, names):
        if ServerConfigurationAll.DEFAULT_LICENSE_FILENAME in names:
            return [ServerConfigurationAll.DEFAULT_LICENSE_FILENAME]
        return []

    def _copy_templates(self, logger, support_package_root, cb_config_backup=None, source_server_configuration=None):
        if source_server_configuration is None:
            source_server_configuration = self
        templates_root = os.path.join(support_package_root, ServerConfigurationAll.BACKUP_TEMPLATES_FOLDER)
        templates_root_source = source_server_configuration.configuration_server_management.templates_abspath

        if os.path.exists(templates_root_source):
            lib.appl.upgrade.copy_tree_include_if_changed(templates_root_source, templates_root, logger, MODULE_ID)
        else:
            logger.Checkpoint("copy_templates.folder_not_found", MODULE_ID, lib.checkpoint.WARNING, templates_root_source=templates_root_source)
            if cb_config_backup is not None:
                cb_config_backup.cb_backup_error("Templates folder not found, see log file.")

    def  _skip_folder(self, folder, skip_folders):
        for skip_folder in skip_folders:
            if folder.startswith(skip_folder):
                return True
        return False

    def _copy_logs(self, logger, support_package_root):
        skip_folders = []
        skip_folders.append(self.configuration_server_config.support_package_abspath)
        skip_folders.append(self.configuration_client_management_service.deployment_soft_token_abspath)

        logs_folder = os.path.join(support_package_root, ServerConfigurationAll.SP_LOGS_FOLDER)
        if not os.path.exists(logs_folder):
            os.makedirs(logs_folder)
        for root, dirs, files in os.walk(self.installation_path_abs):
            if self._skip_folder(os.path.normpath(root), skip_folders):
                continue
            dest_root_relative = lib.commongon.common_path_sufix(root, self.installation_path_abs)
            dest_root_abs = os.path.join(support_package_root, ServerConfigurationAll.SP_LOGS_FOLDER, dest_root_relative)
            for filename in files:
                if filename.endswith('.log'):
                    try:
                        if not os.path.exists(dest_root_abs):
                            os.makedirs(dest_root_abs)
                        filename_abs = os.path.join(root, filename)
                        shutil.copy(filename_abs, dest_root_abs)
                    except:
                        logger.Checkpoint("copy_logs.unable_to_copy", MODULE_ID, lib.checkpoint.WARNING, filename_abs=filename_abs)

    def get_configuration_server_management_from_backup(self, logger, backup_root):
        try:
            ini_source = os.path.join(backup_root, ServerConfigurationAll.BACKUP_INI_FOLDER)
            configuration_server_management = components.config.common.ConfigServerManagement()
            configuration_server_management.read_config_file_from_folder(ini_source)
            return configuration_server_management
        except:
            etype, evalue, etraceback = sys.exc_info()
            logger.CheckpointException("get_configuration_server_management_from_backup.unexpected_error", MODULE_ID, lib.checkpoint.ERROR, etype, evalue, etraceback)
