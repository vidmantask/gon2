from __future__ import with_statement
#from migrate.versioning.util import import_path

from components.database.server_common.schema_api import SchemaFactory
from components.database.server_common.database_api import Transaction
import os
import os.path
import tempfile
import datetime
import shutil

import components.server_config_ws.version_history as version_history
import components.server_config_ws.database_schema as database_schema
import components.database.server_common.schema_api as schema_api
import components.database.server_common.database_api as database_api
import server_config_jobs

import components.config.common
import components.server_config_ws.server_config

import components.plugin.server_config.plugin_socket_upgrade
import lib.checkpoint 
import lib.appl.gon_system_api
import lib.utility.xor_crypt

MODULE_ID = 'upgrade'




class JobUpgradeSession(object):
    FOLDERNAME_INIT_BACKUP = 'init_backup'
    FOLDERNAME_WORK_BACKUP = 'work_backup'
    
    def __init__(self, checkpoint_handler, server_configuration, gon_system_from):
        self.checkpoint_handler = checkpoint_handler
        self.server_configuration = server_configuration
        self.gon_system_from = gon_system_from
        
        self.from_version = self.gon_system_from.version
        self.to_version = lib.version.Version.create_current()
        (self.logger_filename_abs, self.logger, self.upgrade_root) = server_config_jobs.create_logger_in_sub_folder(self.checkpoint_handler, 'upgrade', self.server_configuration.get_config_service_path_abs())

        self.plugin_manager = components.plugin.server_config.manager.Manager(self.logger, self.server_configuration.configuration_server_config.plugin_modules_abspath, self.server_configuration)
        self.work_backup_folder = None
        
        self.from_management_configuration = None

    def load_to_management_configuration(self):
        if self.work_backup_folder is None:
            return None
        ini_root = os.path.join(self.work_backup_folder, lib.appl.upgrade.CONFIG_FOLDER, components.server_config_ws.server_config.ServerConfigurationAll.BACKUP_INI_FOLDER)
        configuration_server_management = components.config.common.ConfigServerManagement()
        configuration_server_management.read_config_file_from_folder(ini_root)
        return configuration_server_management

    def load_to_gateway_configuration(self):
        if self.work_backup_folder is None:
            return None
        ini_root = os.path.join(self.work_backup_folder, lib.appl.upgrade.CONFIG_FOLDER, components.server_config_ws.server_config.ServerConfigurationAll.BACKUP_INI_FOLDER)
        configuration_server_gateway = components.config.common.ConfigServerGateway()
        configuration_server_gateway.read_config_file_from_folder(ini_root)
        return configuration_server_gateway

    def load_to_config_service_configuration(self):
        if self.work_backup_folder is None:
            return None
        ini_root = os.path.join(self.work_backup_folder, lib.appl.upgrade.CONFIG_FOLDER, components.server_config_ws.server_config.ServerConfigurationAll.BACKUP_INI_FOLDER)
        configuration_server_config_service = components.config.common.ConfigConfigService()
        configuration_server_config_service.read_config_file_from_folder(ini_root)
        return configuration_server_config_service

    def save_to_management_configuration(self, configuration_server_management):
        if configuration_server_management is None:
            return
        ini_root = os.path.join(self.work_backup_folder, lib.appl.upgrade.CONFIG_FOLDER, components.server_config_ws.server_config.ServerConfigurationAll.BACKUP_INI_FOLDER)
        configuration_server_management.write_config_file(ini_root)

    def save_to_gateway_configuration(self, configuration_server_gateway):
        if configuration_server_gateway is None or self.work_backup_folder is None:
            return
        ini_root = os.path.join(self.work_backup_folder, lib.appl.upgrade.CONFIG_FOLDER, components.server_config_ws.server_config.ServerConfigurationAll.BACKUP_INI_FOLDER)
        configuration_server_gateway.write_config_file(ini_root)

    def save_to_config_service_configuration(self, configuration_config_service):
        if configuration_config_service is None or self.work_backup_folder is None:
            return
        ini_root = os.path.join(self.work_backup_folder, lib.appl.upgrade.CONFIG_FOLDER, components.server_config_ws.server_config.ServerConfigurationAll.BACKUP_INI_FOLDER)
        configuration_config_service.write_config_file(ini_root)


class JobPrepareUpgrade(lib.ws.job.Job):
    """
    This class holds the functionality for preparing a upgrade.
    """
    def __init__(self, upgrade_session):
        lib.ws.job.Job.__init__(self, upgrade_session.checkpoint_handler, MODULE_ID, 'job_prepare_update')
        self.upgrade_session = upgrade_session 
        self.logger = upgrade_session.logger

        self._cancled = False
        self._error_found = False
        self._warning_found = False

        self.backup_folder_database = None
        
    def do_run(self):
        with self.logger.CheckpointScope("prepare_upgrade_prepare.do_run", MODULE_ID, lib.checkpoint.DEBUG):
            if not self._cancled and not self._error_found:
                self.set_status('Upgrade, generating backup', '', 5)
                self._do_prepare_backup()
                self.upgrade_session.from_management_configuration = self.upgrade_session.server_configuration.get_configuration_server_management_from_backup(self.logger, self.upgrade_session.work_backup_folder)

            if not self._cancled and not self._error_found:
                self.set_status('Upgrade, Generating new database from backup', '', 50)
                with self.logger.CheckpointScope("prepare_upgrade_prepare.do_run", MODULE_ID, lib.checkpoint.DEBUG):
                    self.backup_folder_database = os.path.join(self.upgrade_session.work_backup_folder, lib.appl.upgrade.DATABASE_FOLDER)
                    upgrade_db_sqlite_filename = os.path.join(self.backup_folder_database, 'gon_server_upgrade_db.sqlite')
                    upgrade_db_connect_string = 'sqlite:///%s' % (upgrade_db_sqlite_filename)
                    upgrade_db_encoding = self.upgrade_session.server_configuration.configuration_server_config.db_encoding
                    upgrade_db_log_enabled = self.upgrade_session.server_configuration.configuration_server_config.db_log_enabled
                    environment = components.environment.Environment(self.logger, upgrade_db_connect_string, MODULE_ID, db_encoding=upgrade_db_encoding, db_logging=upgrade_db_log_enabled)
                    components.database.server_common.schema_api.SchemaFactory.restore(self.backup_folder_database, environment.get_default_database(), schema_update=True, create_constraints=False, checkpoint_handler=self.logger)

            if not self._cancled and not self._error_found:
                components.server_config_ws.database_schema.connect_to_database(environment)
                from_version_db = components.server_config_ws.database_schema.lookup_db_version()
                if from_version_db is not None:
                    self.logger.Checkpoint("from_version.from_db", MODULE_ID, lib.checkpoint.DEBUG)
                    from_version = from_version_db.as_version()
                else:
                    self.logger.Checkpoint("from_version.from_file", MODULE_ID, lib.checkpoint.DEBUG)
                    from_version = lib.appl.upgrade.load_version(self.upgrade_session.work_backup_folder)
                if from_version is None:
                    self.logger.Checkpoint("from_version.not found", MODULE_ID, lib.checkpoint.ERROR)
                    self.upgrade_error('Unable find from-version')

            if not self._cancled and not self._error_found:
                self.set_status('Upgrade, database and configuration', '', 80)

                # set salt from backup
                scramble_salt_filename = os.path.join(self.upgrade_session.work_backup_folder, lib.appl.upgrade.CONFIG_FOLDER, components.server_config_ws.server_config.ServerConfigurationAll.BACKUP_DEPLOYED_FOLDER, components.config.common.SCRAMBLE_SALT_FILENAME)
                self.logger.CheckpointScope("changing_salt", MODULE_ID, lib.checkpoint.DEBUG, salt_filename=scramble_salt_filename)
                lib.utility.xor_crypt.load_and_set_scramble_salt(scramble_salt_filename, set_if_not_found=False)
                
                to_version = lib.version.Version.create_current()
                self._do_run_upgrade(environment, from_version, to_version)

            if self._cancled:
                self.set_status_canceled()
                return

            if self._error_found:
                self.set_status_error('Upgrade', "Error during preparing upgrade, see logfile '%s'" %  self.upgrade_session.logger_filename_abs)
                return

            self.set_status_done()
            return

    def _do_prepare_backup(self):
        with self.logger.CheckpointScope("do_create_backup", MODULE_ID, lib.checkpoint.DEBUG):
            if self.upgrade_session.gon_system_from.is_system_type_installation():
                self.logger.Checkpoint("do_create_backup.from_installation", MODULE_ID, lib.checkpoint.DEBUG)
                init_backup_folder = os.path.join(self.upgrade_session.upgrade_root, JobUpgradeSession.FOLDERNAME_INIT_BACKUP)
                if self._do_prepare_backup_from_installation_using_old_tool(init_backup_folder):
                    self.logger.Checkpoint("do_create_backup.from_installation.using_old_tool", MODULE_ID, lib.checkpoint.DEBUG)
                else:
                    self._error_found = True
                    return

            elif self.upgrade_session.gon_system_from.is_system_type_backup():
                self.logger.Checkpoint("do_create_backup.from_backup", MODULE_ID, lib.checkpoint.DEBUG)
                init_backup_folder = os.path.join(self.upgrade_session.upgrade_root, JobUpgradeSession.FOLDERNAME_INIT_BACKUP)
                shutil.copytree(self.upgrade_session.gon_system_from.root_folder, init_backup_folder)

            if not self._error_found:
                # Create copy of backup for use during upgrade
                self.upgrade_session.work_backup_folder = os.path.normpath(os.path.join(self.upgrade_session.upgrade_root, JobUpgradeSession.FOLDERNAME_WORK_BACKUP))
                shutil.copytree(init_backup_folder, self.upgrade_session.work_backup_folder)

    def _do_prepare_backup_from_installation_using_old_tool(self, init_backup_folder):
        (backup_returncode, backup_stdout) = self.upgrade_session.gon_system_from.generate_backup(self.logger, init_backup_folder)
        if not backup_returncode == 0:
            self.logger.CheckpointMultilineMessage("prepare_upgrade_prepare.from_installation.error", MODULE_ID, lib.checkpoint.ERROR, backup_stdout)
            return False
        if backup_stdout is not None:
            self.logger.CheckpointMultilineMessage("prepare_upgrade_prepare.from_installation", MODULE_ID, lib.checkpoint.DEBUG, backup_stdout)
        return True
    
    def _cb_subjob_status_change(self, subjob_status):
        if subjob_status.status == lib.ws.job.JobStatus.STATUS_DONE_error:
            self._error_found = True

    def _do_run_upgrade(self, environment, from_version, to_version):
        with self.logger.CheckpointScope("prepare_upgrade_prepare.do_run_upgrade", MODULE_ID, lib.checkpoint.DEBUG, from_version=from_version.get_version_string(), to_version=to_version.get_version_string()):
            to_version = self.upgrade_session.to_version
            from_version = self.upgrade_session.from_version
            version_path_mmb = lib.version.Version.find_version_path_mmb(from_version.encode_mmb(), to_version.encode_mmb(), version_history.version_map)
            if version_path_mmb is None:
                self.upgrade_error('No version upgrade path exists')
                return
            
            from_version_mmb = version_path_mmb.pop()

            environment.plugin_manager = self.upgrade_session.plugin_manager 
            upgrade_socket = components.plugin.server_config.plugin_socket_upgrade.PluginSocket(environment.plugin_manager)

            while len(version_path_mmb)>0:
                if self._cancled or self._error_found:
                    return
                
                to_version_mmb = version_path_mmb.pop()
                self._do_run_upgrade_step(environment, upgrade_socket, from_version_mmb, to_version_mmb)
                from_version_mmb = to_version_mmb
    
            schema_api.SchemaFactory._restore_path = None




    def _do_run_upgrade_step(self, environment, upgrade_socket, from_version_mmb, to_version_mmb):
        with self.logger.CheckpointScope("prepare_upgrade_prepare.do_run_upgrade_step", MODULE_ID, lib.checkpoint.DEBUG):
            engine = environment.get_default_database()
            transaction = Transaction(connection=engine)

            update_step_is_ok = upgrade_socket.upgrade(from_version_mmb, to_version_mmb, database_api, transaction, self.upgrade_session.work_backup_folder, self.logger)
            if not update_step_is_ok:
                self.upgrade_error('Error during plugin update')
                transaction.close()
                return

            from_version = lib.version.Version.create_mmb(from_version_mmb)
            to_version = lib.version.Version.create_mmb(to_version_mmb)

            # Upgrade database version 
            db_version_from = database_schema.find_mmb(from_version, db_session=transaction)
            if db_version_from is None:
                db_version_from   = database_schema.Version.create_from_version(from_version)
                transaction.add(db_version_from)

            db_version_to = database_schema.find_mmb(to_version, db_session=transaction)
            if db_version_to is None:
                db_version_to = database_schema.Version.create_from_version(to_version)
                transaction.add(db_version_to)

            transaction.flush()
            db_version_from.next = db_version_to.id
            db_version_to.previous = db_version_from.id
            transaction.commit()
            
            # Backup version table
            database_schema.dbapi.bind(engine)
            database_schema.dbapi.backup(self.backup_folder_database)

            # Upgrade working backup version
            lib.appl.upgrade.save_version(self.upgrade_session.work_backup_folder, version=to_version)
            database_api.clear_database(transaction.get_connection())
            database_api.SchemaFactory.restore(self.backup_folder_database, transaction.get_connection(), schema_update=True, create_constraints=False)
            transaction.commit()


    def upgrade_error(self, message):
        self._error_found = True
        self.set_status_error('Upgrade', message)

    def do_cancel(self):
        self._cancled = True

            
class JobFinalizeUpgrade(lib.ws.job.Job):
    """
    This class holds the functionality for doing upgrade of database and system configuration based on a backup from a privious version.
    """
    def __init__(self, upgrade_session, config_specification_socket):
        lib.ws.job.Job.__init__(self, upgrade_session.checkpoint_handler, MODULE_ID, 'job_finalize_update')
        self.upgrade_session = upgrade_session
        self.logger = upgrade_session.logger
        
        self.server_configuration = self.upgrade_session.server_configuration
        self.config_specification_socket = config_specification_socket
        self.upgrade_steps = 0 
        self.upgrade_step_current = 0

        self._cancled = False
        self._error_found = False
        self._warning_found = False
        
        self.backup_folder_database = None
        
    def do_run(self):
        with self.logger.CheckpointScope("finalize_upgrade_prepare.do_run", MODULE_ID, lib.checkpoint.DEBUG):
            
            if not self._cancled and not self._error_found: 
                self.set_status('Upgrade, stopping and removing service', '', 0)
                job_remove_services = server_config_jobs.JobRemoveServices(self.server_configuration, self.logger)
                job_remove_services.set_status_change_callback(self._cb_subjob_status_change)
                job_remove_services.connect_to_parent_job(self, 0, 10)
                job_remove_services.run()
            
            if not self._cancled and not self._error_found:
                self.set_status('Upgrade, restoring database and configuration', '', 10)
                job_restore = server_config_jobs.JobRestore(self.server_configuration, self.logger, self.upgrade_session.work_backup_folder, self.config_specification_socket, False, is_upgrade=True)
                from_management_configuration = self.upgrade_session.load_to_management_configuration()
                database_type = from_management_configuration.db_type
                if (database_type=="mssql" or database_type=="mysql") and self.upgrade_session.to_version.equal_major_minor_bugfix(self.upgrade_session.from_version):
                    self.logger.Checkpoint("finalize_upgrade_prepare.do_run", MODULE_ID, lib.checkpoint.INFO, msg="Skip database restore for SQL Server on patch upgrade")
                    job_restore.skip_db_restore = True
                job_restore.set_status_change_callback(self._cb_subjob_status_change)
                job_restore.connect_to_parent_job(self, 10, 50)
                components.database.server_common.schema_api.SchemaFactory.disconnect()
                job_restore.run()
            
            if not self._cancled and not self._error_found:
                self.set_status('Upgrade, generating packages', '', 50)
                job_generate_gpms = server_config_jobs.JobGenerateGPMS(self.server_configuration, self.logger)
                job_generate_gpms.set_status_change_callback(self._cb_subjob_status_change)
                job_generate_gpms.connect_to_parent_job(self, 50, 90)
                job_generate_gpms.run()

            if not self._cancled and not self._error_found: 
                self.set_status('Upgrade, installing services', '', 90)
                job_install_services = server_config_jobs.JobInstallServices(self.server_configuration, self.logger, start_services=False)
                job_install_services.set_status_change_callback(self._cb_subjob_status_change)
                job_install_services.connect_to_parent_job(self, 90, 100)
                job_install_services.run()

            if self._cancled:
                self.set_status_canceled()
                return

            if self._error_found:
                self.set_status_error('Upgrade', "Error found during upgrade, see logfile '%s'" %  self.upgrade_session.logger_filename_abs)
                return

            self.set_status_done()
            return

             

    def upgrade_error(self, message):
        self._error_found = True
        self.set_status_error('Upgrade', message)
         
    def do_cancel(self):
        self._cancled = True

    def _cb_subjob_status_change(self, subjob_status):
        if subjob_status.status == lib.ws.job.JobStatus.STATUS_DONE_error:
            self._error_found = True
            return
