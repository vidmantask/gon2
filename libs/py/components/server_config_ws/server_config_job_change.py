"""
This module contains the jobs for change configuration
"""
from __future__ import with_statement

import datetime
import os.path
import sys

import lib.ws.job
import lib.checkpoint
import server_config_jobs
import database_schema

import components.environment
import lib.appl.service

import components.management_message.server_common.session_element_filedist


class JobChangeSession(object):
    def __init__(self, checkpoint_handler, server_configuration):
        self.server_configuration = server_configuration
        self.checkpoint_handler = checkpoint_handler
        logger_foldername_abs = os.path.normpath(os.path.join(self.server_configuration.get_config_service_path_abs(), server_config_jobs.EVENTS_FOLDER))
        (self.logger_filename_abs, self.logger) = server_config_jobs.create_logger(self.checkpoint_handler, 'change', logger_foldername_abs)

class JobPrepareChange(lib.ws.job.Job):
    """
    This class holds the functionality for preparing a change of configuration
    """
    def __init__(self, change_session):
        lib.ws.job.Job.__init__(self, change_session.checkpoint_handler, server_config_jobs.MODULE_ID, 'job_prepare_change')
        self.change_session = change_session
        self.server_configuration = change_session.server_configuration
        self.checkpoint_handler = change_session.checkpoint_handler
        self.logger = change_session.logger

    def do_run(self):
        with self.logger.CheckpointScope("prepare_change", server_config_jobs.MODULE_ID, lib.checkpoint.DEBUG):
            self.set_status_done()
        
    def do_cancel(self):
        pass


class JobFinalizeChange(lib.ws.job.Job):
    """
    This class holds the functionality for finialization of a change of the installation.
    """
    def __init__(self, change_session, config_specifications):
        lib.ws.job.Job.__init__(self, change_session.checkpoint_handler, server_config_jobs.MODULE_ID, 'job_finalize_installation')
        self.change_session = change_session
        self.config_specifications = config_specifications
        self.server_configuration = change_session.server_configuration
        self.logger = change_session.logger
        
        self._cancled = False
        self._error_found = False

    def do_run(self):
        with self.logger.CheckpointScope("finialize_installation", server_config_jobs.MODULE_ID, lib.checkpoint.DEBUG):

            if not self._cancled and not self._error_found: 
                self.set_status('Change configuration, updating system configuration', '', 5)
                self.server_configuration.finialize_installation(self.logger, change_mode=True)

            if self._cancled:
                self.set_status_canceled()
                return
            
            try:
                for config_specification in self.config_specifications:
                    config_specification.finalize()
            except Exception, e:
                (etype, evalue, etrace) = sys.exc_info()
                self.logger.CheckpointException("JobFinalizeChange", server_config_jobs.MODULE_ID, lib.checkpoint.ERROR, etype, evalue, etrace)
                self._error_found = e
                
            filedist_root = self.server_configuration.configuration_server_config.service_management_filedist_abspath
            components.management_message.server_common.session_element_filedist.FiledistManager.files_update(self.logger, filedist_root)        

            if self._error_found:
                self.set_status_error('Change configuration', "Error found during change of configuration, see logfile '%s'" %  self.change_session.logger_filename_abs)
                return

            self.set_status_done()
            return

    def _cb_subjob_status_change(self, subjob_status):
        if subjob_status.status == lib.ws.job.JobStatus.STATUS_DONE_error:
            self._error_found = True

    def do_cancel(self):
        self.set_status_cancel()
        self._cancled = True
