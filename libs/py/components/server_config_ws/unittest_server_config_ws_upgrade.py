"""
Unittest of Server Config web-service
"""
from __future__ import with_statement
import unittest
from lib import giri_unittest

import os.path
import time
import tempfile
import shutil

import components.server_config_ws.ws as ws
import components.server_config_ws.server_config_ws_service

import components.config.common
import server_config

import lib.checkpoint

#import components.server_config_ws.database_schema
#import components.database.server_common.schema_api
#import components.auth.server_common.database_schema
#import components.dialog.server_common.database_schema
#import components.management_message.server_common.database_schema
#import components.traffic.server_common.database_schema
#import components.access_log.server_common.database_schema
#import components.user.server_common.database_schema
#import components.endpoint.server_common.database_schema
#import components.templates.server_common.database_schema



import sqlalchemy.orm

class ServerConfigWSUpgrade(unittest.TestCase):

    def setUp(self):
        self.gon_installation = giri_unittest.UnittestDevEnvInstallation.create()
        self.gon_installation.generate_gpms()
#        self.gon_installation.generate_setupdata()
        self.gon_installation.restore_plugins()
        self.server_configuration_all = self.gon_installation.server_configuration_all
        self.server_config_ip = self.server_configuration_all.configuration_server_config.service_ip
        self.server_config_port = self.server_configuration_all.configuration_server_config.service_port
        self.server_config_service = components.server_config_ws.server_config_ws_service.ServerConfigWSService.run_default(giri_unittest.get_checkpoint_handler(), self.server_configuration_all, self.server_config_ip, self.server_config_port)
#        sqlalchemy.orm.clear_mappers()
#        components.server_config_ws.database_schema.create_mapper()

    def tearDown(self):
        time.sleep(2)
        self.server_config_service.stop()
        self.server_config_service.join()

    def _login(self, server_config_service):
        request = ws.server_config_ws_services.LoginRequest()
        request_content = ws.server_config_ws_services.ns0.LoginRequestType_Def(None).pyclass()
        request_content.set_element_username("ib")
        request_content.set_element_password("FidoTheDog")
        request.set_element_content(request_content)
        response = server_config_service.Login(request)
        return response.get_element_content().get_element_session_id()

    def _logout(self, server_config_service, session_id):
        request = ws.server_config_ws_services.LogoutRequest()
        request_content = ws.server_config_ws_services.ns0.LogoutRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.Logout(request)
        rc = response.get_element_content().get_element_rc()
        self.assertTrue(rc)

    def _wait_for_job(self, server_config_service, session_id, job_id):
        job_more = True
        while(job_more):
            request = ws.server_config_ws_services.GetJobInfoRequest()
            request_content = ws.server_config_ws_services.ns0.GetJobInfoRequestType_Def(None).pyclass()
            request_content.set_element_session_id(session_id)
            request_content.set_element_job_id(job_id)
            request.set_element_content(request_content)
            response = server_config_service.GetJobInfo(request)
            job_info = response.get_element_content().get_element_job_info()

            job_more = job_info.get_element_job_more()
            job_status = job_info.get_attribute_job_status()
            job_header = job_info.get_element_job_header()
            job_sub_header = job_info.get_element_job_sub_header()
            job_progress = job_info.get_element_job_progress()
#            print "Job Action", job_id, job_more, job_status, job_header, job_sub_header, job_progress
            time.sleep(1)
        print "Job Done"
        return job_status


    def atest_upgrade_5_3_1(self):
        backup_folder_src = os.path.join(giri_unittest.UNITTEST_DATA_FOLDER, 'backups', 'backup_5.3.1')
        backup_folder_dest= self.server_configuration_all.configuration_server_config.backup_abspath

        print "backup_folder_dest:", backup_folder_dest
        if os.path.exists(backup_folder_dest):
            shutil.rmtree(backup_folder_dest)
        os.makedirs(backup_folder_dest)
        shutil.copytree(backup_folder_src, os.path.join(backup_folder_dest, 'backup_1'))

        server_config_service = ws.server_config_ws_services.BindingSOAP("http://%s:%d" % (self.server_config_ip, self.server_config_port))
        session_id = self._login(server_config_service)

        request = ws.server_config_ws_services.GetGOnSystemsRequest()
        request_content = ws.server_config_ws_services.ns0.GetGOnSystemsRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.GetGOnSystems(request)
        gon_systems = response.get_element_content().get_element_gon_systems()
        for gon_system in gon_systems:
            print 'G/On System :', gon_system.get_element_name(), gon_system.get_element_title(), gon_system.get_element_version(), gon_system.get_element_management_is_running(), gon_system.get_element_gateway_is_running(), gon_system.get_element_can_be_used_for_upgrade(), gon_system.get_element_is_installation(), gon_system.get_element_is_current_installation()

        self.assertTrue(len(gon_systems) > 0)
        gon_system = gon_systems[0]

        request = ws.server_config_ws_services.StartJobPrepareUpgradeRequest()
        request_content = ws.server_config_ws_services.ns0.StartJobPrepareUpgradeRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request_content.set_element_system_name(gon_system.get_element_name())
        request.set_element_content(request_content)
        response = server_config_service.StartJobPrepareUpgrade(request)

        job_id = response.get_element_content().get_element_job_id()
        job_status = self._wait_for_job(server_config_service, session_id, job_id)
        print job_status
        self.assertEqual(job_status, 'done_ok_job_status')

        request = ws.server_config_ws_services.StartJobFinalizeUpgradeRequest()
        request_content = ws.server_config_ws_services.ns0.StartJobFinalizeUpgradeRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.StartJobFinalizeUpgrade(request)

        job_id = response.get_element_content().get_element_job_id()
        job_status = self._wait_for_job(server_config_service, session_id, job_id)
        print job_status
        self.assertEqual(job_status, 'done_ok_job_status')


    def atest_upgrade_5_4_0(self):
        backup_folder_src = os.path.join(giri_unittest.UNITTEST_DATA_FOLDER, 'backups', 'backup_5.4.0')
        backup_folder_dest= self.server_configuration_all.configuration_server_config.backup_abspath

        print "backup_folder_dest:", backup_folder_dest
        if os.path.exists(backup_folder_dest):
            shutil.rmtree(backup_folder_dest)
        os.makedirs(backup_folder_dest)
        shutil.copytree(backup_folder_src, os.path.join(backup_folder_dest, 'backup_1'))

        server_config_service = ws.server_config_ws_services.BindingSOAP("http://%s:%d" % (self.server_config_ip, self.server_config_port))
        session_id = self._login(server_config_service)

        request = ws.server_config_ws_services.GetGOnSystemsRequest()
        request_content = ws.server_config_ws_services.ns0.GetGOnSystemsRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.GetGOnSystems(request)
        gon_systems = response.get_element_content().get_element_gon_systems()
        for gon_system in gon_systems:
            print 'G/On System :', gon_system.get_element_name(), gon_system.get_element_title(), gon_system.get_element_version(), gon_system.get_element_management_is_running(), gon_system.get_element_gateway_is_running(), gon_system.get_element_can_be_used_for_upgrade(), gon_system.get_element_is_installation(), gon_system.get_element_is_current_installation()

        self.assertTrue(len(gon_systems) > 0)
        gon_system = gon_systems[0]

        request = ws.server_config_ws_services.StartJobPrepareUpgradeRequest()
        request_content = ws.server_config_ws_services.ns0.StartJobPrepareUpgradeRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request_content.set_element_system_name(gon_system.get_element_name())
        request.set_element_content(request_content)
        response = server_config_service.StartJobPrepareUpgrade(request)

        job_id = response.get_element_content().get_element_job_id()
        job_status = self._wait_for_job(server_config_service, session_id, job_id)
        print job_status
        self.assertEqual(job_status, 'done_ok_job_status')

        request = ws.server_config_ws_services.StartJobFinalizeUpgradeRequest()
        request_content = ws.server_config_ws_services.ns0.StartJobFinalizeUpgradeRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.StartJobFinalizeUpgrade(request)

        job_id = response.get_element_content().get_element_job_id()
        job_status = self._wait_for_job(server_config_service, session_id, job_id)
        print job_status
        self.assertEqual(job_status, 'done_ok_job_status')

    def atest_upgrade_5_4_1(self):
        backup_folder_src = os.path.join(giri_unittest.UNITTEST_DATA_FOLDER, 'backups', 'backup_5.4.1')
        backup_folder_dest= self.server_configuration_all.configuration_server_config.backup_abspath

        print "backup_folder_dest:", backup_folder_dest
        if os.path.exists(backup_folder_dest):
            shutil.rmtree(backup_folder_dest)
        os.makedirs(backup_folder_dest)
        shutil.copytree(backup_folder_src, os.path.join(backup_folder_dest, 'backup_1'))

        server_config_service = ws.server_config_ws_services.BindingSOAP("http://%s:%d" % (self.server_config_ip, self.server_config_port))
        session_id = self._login(server_config_service)

        request = ws.server_config_ws_services.GetGOnSystemsRequest()
        request_content = ws.server_config_ws_services.ns0.GetGOnSystemsRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.GetGOnSystems(request)
        gon_systems = response.get_element_content().get_element_gon_systems()
        for gon_system in gon_systems:
            print 'G/On System :', gon_system.get_element_name(), gon_system.get_element_title(), gon_system.get_element_version(), gon_system.get_element_management_is_running(), gon_system.get_element_gateway_is_running(), gon_system.get_element_can_be_used_for_upgrade(), gon_system.get_element_is_installation(), gon_system.get_element_is_current_installation()

        self.assertTrue(len(gon_systems) > 0)
        gon_system = gon_systems[0]

        request = ws.server_config_ws_services.StartJobPrepareUpgradeRequest()
        request_content = ws.server_config_ws_services.ns0.StartJobPrepareUpgradeRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request_content.set_element_system_name(gon_system.get_element_name())
        request.set_element_content(request_content)
        response = server_config_service.StartJobPrepareUpgrade(request)

        job_id = response.get_element_content().get_element_job_id()
        job_status = self._wait_for_job(server_config_service, session_id, job_id)
        print job_status
        self.assertEqual(job_status, 'done_ok_job_status')

        request = ws.server_config_ws_services.StartJobFinalizeUpgradeRequest()
        request_content = ws.server_config_ws_services.ns0.StartJobFinalizeUpgradeRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.StartJobFinalizeUpgrade(request)

        job_id = response.get_element_content().get_element_job_id()
        job_status = self._wait_for_job(server_config_service, session_id, job_id)
        print job_status
        self.assertEqual(job_status, 'done_ok_job_status')



    def atest_upgrade_5_5_1(self):
        backup_folder_src = os.path.join(giri_unittest.UNITTEST_DATA_FOLDER, 'backups', 'backup_5.5.1')
        backup_folder_dest= self.server_configuration_all.configuration_server_config.backup_abspath

        print "backup_folder_dest:", backup_folder_dest
        if os.path.exists(backup_folder_dest):
            shutil.rmtree(backup_folder_dest)
        os.makedirs(backup_folder_dest)
        shutil.copytree(backup_folder_src, os.path.join(backup_folder_dest, 'backup_1'))

        server_config_service = ws.server_config_ws_services.BindingSOAP("http://%s:%d" % (self.server_config_ip, self.server_config_port))
        session_id = self._login(server_config_service)

        request = ws.server_config_ws_services.GetGOnSystemsRequest()
        request_content = ws.server_config_ws_services.ns0.GetGOnSystemsRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.GetGOnSystems(request)
        gon_systems = response.get_element_content().get_element_gon_systems()
        for gon_system in gon_systems:
            print 'G/On System :', gon_system.get_element_name(), gon_system.get_element_title(), gon_system.get_element_version(), gon_system.get_element_management_is_running(), gon_system.get_element_gateway_is_running(), gon_system.get_element_can_be_used_for_upgrade(), gon_system.get_element_is_installation(), gon_system.get_element_is_current_installation()

        self.assertTrue(len(gon_systems) > 0)
        gon_system = gon_systems[0]

        request = ws.server_config_ws_services.StartJobPrepareUpgradeRequest()
        request_content = ws.server_config_ws_services.ns0.StartJobPrepareUpgradeRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request_content.set_element_system_name(gon_system.get_element_name())
        request.set_element_content(request_content)
        response = server_config_service.StartJobPrepareUpgrade(request)

        job_id = response.get_element_content().get_element_job_id()
        job_status = self._wait_for_job(server_config_service, session_id, job_id)
        print job_status
        self.assertEqual(job_status, 'done_ok_job_status')

        request = ws.server_config_ws_services.StartJobFinalizeUpgradeRequest()
        request_content = ws.server_config_ws_services.ns0.StartJobFinalizeUpgradeRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.StartJobFinalizeUpgrade(request)

        job_id = response.get_element_content().get_element_job_id()
        job_status = self._wait_for_job(server_config_service, session_id, job_id)
        print job_status
        self.assertEqual(job_status, 'done_ok_job_status')


#
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []

    def atest_upgrade_5_6_0(self):
        backup_folder_src = os.path.join(giri_unittest.UNITTEST_DATA_FOLDER, 'backups', 'backup_5.6.0')
        backup_folder_dest= self.server_configuration_all.configuration_server_config.backup_abspath

        print "backup_folder_dest:", backup_folder_dest
        if os.path.exists(backup_folder_dest):
            shutil.rmtree(backup_folder_dest)
        os.makedirs(backup_folder_dest)
        shutil.copytree(backup_folder_src, os.path.join(backup_folder_dest, 'backup_1'))

        server_config_service = ws.server_config_ws_services.BindingSOAP("http://%s:%d" % (self.server_config_ip, self.server_config_port))
        session_id = self._login(server_config_service)

        request = ws.server_config_ws_services.GetGOnSystemsRequest()
        request_content = ws.server_config_ws_services.ns0.GetGOnSystemsRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.GetGOnSystems(request)
        gon_systems = response.get_element_content().get_element_gon_systems()
        for gon_system in gon_systems:
            print 'G/On System :', gon_system.get_element_name(), gon_system.get_element_title(), gon_system.get_element_version(), gon_system.get_element_management_is_running(), gon_system.get_element_gateway_is_running(), gon_system.get_element_can_be_used_for_upgrade(), gon_system.get_element_is_installation(), gon_system.get_element_is_current_installation()

        self.assertTrue(len(gon_systems) > 0)
        gon_system = gon_systems[0]

        request = ws.server_config_ws_services.StartJobPrepareUpgradeRequest()
        request_content = ws.server_config_ws_services.ns0.StartJobPrepareUpgradeRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request_content.set_element_system_name(gon_system.get_element_name())
        request.set_element_content(request_content)
        response = server_config_service.StartJobPrepareUpgrade(request)

        job_id = response.get_element_content().get_element_job_id()
        job_status = self._wait_for_job(server_config_service, session_id, job_id)
        print job_status
        self.assertEqual(job_status, 'done_ok_job_status')

        request = ws.server_config_ws_services.StartJobFinalizeUpgradeRequest()
        request_content = ws.server_config_ws_services.ns0.StartJobFinalizeUpgradeRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.StartJobFinalizeUpgrade(request)

        job_id = response.get_element_content().get_element_job_id()
        job_status = self._wait_for_job(server_config_service, session_id, job_id)
        print job_status
        self.assertEqual(job_status, 'done_ok_job_status')


#
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []

    def test_upgrade_5_6_1(self):
        backup_folder_src = os.path.join(giri_unittest.UNITTEST_DATA_FOLDER, 'backups', 'backup_5.6.1')
        backup_folder_dest= self.server_configuration_all.configuration_server_config.backup_abspath

        print "backup_folder_dest:", backup_folder_dest
        if os.path.exists(backup_folder_dest):
            shutil.rmtree(backup_folder_dest)
        os.makedirs(backup_folder_dest)
        shutil.copytree(backup_folder_src, os.path.join(backup_folder_dest, 'backup_1'))

        server_config_service = ws.server_config_ws_services.BindingSOAP("http://%s:%d" % (self.server_config_ip, self.server_config_port))
        session_id = self._login(server_config_service)

        request = ws.server_config_ws_services.GetGOnSystemsRequest()
        request_content = ws.server_config_ws_services.ns0.GetGOnSystemsRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.GetGOnSystems(request)
        gon_systems = response.get_element_content().get_element_gon_systems()
        for gon_system in gon_systems:
            print 'G/On System :', gon_system.get_element_name(), gon_system.get_element_title(), gon_system.get_element_version(), gon_system.get_element_management_is_running(), gon_system.get_element_gateway_is_running(), gon_system.get_element_can_be_used_for_upgrade(), gon_system.get_element_is_installation(), gon_system.get_element_is_current_installation()

        self.assertTrue(len(gon_systems) > 0)
        gon_system = gon_systems[0]

        request = ws.server_config_ws_services.StartJobPrepareUpgradeRequest()
        request_content = ws.server_config_ws_services.ns0.StartJobPrepareUpgradeRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request_content.set_element_system_name(gon_system.get_element_name())
        request.set_element_content(request_content)
        response = server_config_service.StartJobPrepareUpgrade(request)

        job_id = response.get_element_content().get_element_job_id()
        job_status = self._wait_for_job(server_config_service, session_id, job_id)
        print job_status
        self.assertEqual(job_status, 'done_ok_job_status')

        request = ws.server_config_ws_services.StartJobFinalizeUpgradeRequest()
        request_content = ws.server_config_ws_services.ns0.StartJobFinalizeUpgradeRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.StartJobFinalizeUpgrade(request)

        job_id = response.get_element_content().get_element_job_id()
        job_status = self._wait_for_job(server_config_service, session_id, job_id)
        print job_status
        self.assertEqual(job_status, 'done_ok_job_status')


#
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []


GIRI_UNITTEST_OWNER = 'twa'
GIRI_UNITTEST_TIMEOUT = 60 * 6

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
