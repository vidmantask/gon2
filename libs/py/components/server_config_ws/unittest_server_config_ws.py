"""
Unittest of Server Config web-service
"""
from __future__ import with_statement
import unittest
from lib import giri_unittest

import os
import os.path
import time

import components.server_config_ws.ws as ws
import components.server_config_ws.server_config_ws_service

import components.config.common
import server_config


def list_files(base_root):
    for root, folders, filenames in os.walk(base_root):
        for filename in filenames:
            print os.path.join(root, filename)


class ServerConfigWSTest(unittest.TestCase):
    def setUp(self):
        self.gon_installation = giri_unittest.UnittestDevEnvInstallation.create()
        self.server_configuration_all = self.gon_installation.server_configuration_all
        self.server_config_ip = self.server_configuration_all.configuration_server_config.service_ip
        self.server_config_port = self.server_configuration_all.configuration_server_config.service_port
        self.server_config_service = components.server_config_ws.server_config_ws_service.ServerConfigWSService.run_default(giri_unittest.get_checkpoint_handler(), self.server_configuration_all, self.server_config_ip, self.server_config_port)

    def tearDown(self):
        time.sleep(2)
        self.server_config_service.stop()
        time.sleep(2)

    def _login(self, server_config_service):
        request = ws.server_config_ws_services.LoginRequest()
        request_content = ws.server_config_ws_services.ns0.LoginRequestType_Def(None).pyclass()
        request_content.set_element_username("ib")
        request_content.set_element_password("FidoTheDog")
        request.set_element_content(request_content)
        response = server_config_service.Login(request)
        return response.get_element_content().get_element_session_id()

    def _logout(self, server_config_service, session_id):
        request = ws.server_config_ws_services.LogoutRequest()
        request_content = ws.server_config_ws_services.ns0.LogoutRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.Logout(request)
        rc = response.get_element_content().get_element_rc()
        self.assertTrue(rc)

    def _wait_for_job(self, server_config_service, session_id, job_id):
        job_more = True
        while(job_more):
            request = ws.server_config_ws_services.GetJobInfoRequest()
            request_content = ws.server_config_ws_services.ns0.GetJobInfoRequestType_Def(None).pyclass()
            request_content.set_element_session_id(session_id)
            request_content.set_element_job_id(job_id)
            request.set_element_content(request_content)
            response = server_config_service.GetJobInfo(request)
            job_info = response.get_element_content().get_element_job_info()

            job_more = job_info.get_element_job_more()
            job_status = job_info.get_attribute_job_status()
            job_header = job_info.get_element_job_header()
            job_sub_header = job_info.get_element_job_sub_header()
            job_progress = job_info.get_element_job_progress()
            job_done_message = job_info.get_element_job_done_message()
            job_done_arg_01 = job_info.get_element_job_done_arg_01()
            print "Job Action", job_id, job_more, job_status, job_header, job_sub_header, job_progress
            time.sleep(1)
        print "Job Done", job_done_message, job_done_arg_01
        return job_status


    def test_generate_knownsecrets(self):
        time.sleep(1)
        server_config_service = ws.server_config_ws_services.BindingSOAP("http://%s:%d" % (self.server_config_ip, self.server_config_port))

        session_id = self._login(server_config_service)
        request = ws.server_config_ws_services.StartJobGenerateKnownsecretsRequest()
        request_content = ws.server_config_ws_services.ns0.StartJobGenerateKnownsecretsRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.StartJobGenerateKnownsecrets(request)

        job_id = response.get_element_content().get_element_job_id()
        job_status = self._wait_for_job(server_config_service, session_id, job_id)
        print job_status

        self.assertTrue( job_status == 'done_ok_job_status')
        self._logout(server_config_service, session_id)


    def test_generate_gpms(self):
        time.sleep(1)
        server_config_service = ws.server_config_ws_services.BindingSOAP("http://%s:%d" % (self.server_config_ip, self.server_config_port))

        session_id = self._login(server_config_service)
        request = ws.server_config_ws_services.StartJobGenerateGPMSRequest()
        request_content = ws.server_config_ws_services.ns0.StartJobGenerateGPMSRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.StartJobGenerateGPMS(request)

        job_id = response.get_element_content().get_element_job_id()
        job_status = self._wait_for_job(server_config_service, session_id, job_id)
        print job_status

        self.assertTrue( job_status == 'done_ok_job_status')
        self._logout(server_config_service, session_id)


    def test_generate_demodata(self):
        time.sleep(1)
        server_config_service = ws.server_config_ws_services.BindingSOAP("http://%s:%d" % (self.server_config_ip, self.server_config_port))

        session_id = self._login(server_config_service)
        request = ws.server_config_ws_services.StartJobGenerateDemodataRequest()
        request_content = ws.server_config_ws_services.ns0.StartJobGenerateDemodataRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.StartJobGenerateDemodata(request)

        job_id = response.get_element_content().get_element_job_id()
        job_status = self._wait_for_job(server_config_service, session_id, job_id)
        print job_status

        self.assertTrue( job_status == 'done_ok_job_status')
        self._logout(server_config_service, session_id)

    def test_generate_support_package(self):
        time.sleep(1)
        server_config_service = ws.server_config_ws_services.BindingSOAP("http://%s:%d" % (self.server_config_ip, self.server_config_port))

        session_id = self._login(server_config_service)
        request = ws.server_config_ws_services.StartJobGenerateSupportPackageRequest()
        request_content = ws.server_config_ws_services.ns0.StartJobGenerateSupportPackageRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.StartJobGenerateSupportPackage(request)

        job_id = response.get_element_content().get_element_job_id()
        job_status = self._wait_for_job(server_config_service, session_id, job_id)
        print job_status

        self.assertTrue( job_status == 'done_ok_job_status')
        self._logout(server_config_service, session_id)


    def test_install_services(self):
        time.sleep(1)
        server_config_service = ws.server_config_ws_services.BindingSOAP("http://%s:%d" % (self.server_config_ip, self.server_config_port))

        session_id = self._login(server_config_service)
        request = ws.server_config_ws_services.StartJobInstallServicesRequest()
        request_content = ws.server_config_ws_services.ns0.StartJobInstallServicesRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.StartJobInstallServices(request)

        job_id = response.get_element_content().get_element_job_id()
        job_status = self._wait_for_job(server_config_service, session_id, job_id)
        print job_status

#        self.assertTrue( job_status == 3)
        self._logout(server_config_service, session_id)

#    def test_get_license_info(self):
#        time.sleep(1)
#        server_config_service = ws.server_config_ws_services.BindingSOAP("http://%s:%d" % (self.server_config_ip, self.server_config_port))
#
#        session_id = self._login(server_config_service)
#        request = ws.server_config_ws_services.GetLicenseInfoRequest()
#        request_content = ws.server_config_ws_services.ns0.GetLicenseInfoRequestType_Def(None).pyclass()
#        request_content.set_element_session_id(session_id)
#        request.set_element_content(request_content)
#        response = server_config_service.GetLicenseInfo(request)
#
#        license_info = response.get_element_content().get_element_license_info()
#        self.assertNotEqual(license_info, None)
#        print license_info.get_element_licensed_to(), license_info.get_element_license_number(), license_info.get_element_license_file_number(), license_info.get_element_login_text(), license_info.get_element_is_default_license()

#    def test_set_license_info(self):
#        time.sleep(1)
#        server_config_service = ws.server_config_ws_services.BindingSOAP("http://%s:%d" % (self.server_config_ip, self.server_config_port))
#
#        license = "# modified\n"
#        license_file = open(self.gon_installation.get_license_filename(), 'r')
#        license += license_file.read()
#        license_file.close()
#
#        session_id = self._login(server_config_service)
#        request = ws.server_config_ws_services.SetLicenseRequest()
#        request_content = ws.server_config_ws_services.ns0.SetLicenseRequestType_Def(None).pyclass()
#        request_content.set_element_session_id(session_id)
#        request_content.set_element_license(license)
#        request.set_element_content(request_content)
#        response = server_config_service.SetLicense(request)
#
#        rc = response.get_element_content().get_element_rc()
#        print "SetLicenseRequest", rc


    def test_get_gon_systems(self):
        time.sleep(1)
        server_config_service = ws.server_config_ws_services.BindingSOAP("http://%s:%d" % (self.server_config_ip, self.server_config_port))

        session_id = self._login(server_config_service)
        request = ws.server_config_ws_services.GetGOnSystemsRequest()
        request_content = ws.server_config_ws_services.ns0.GetGOnSystemsRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.GetGOnSystems(request)

        for gon_system in response.get_element_content().get_element_gon_systems():
            print gon_system.get_element_version(), gon_system.get_element_is_current_version(), gon_system.get_element_default_for_upgrade()


    def test_management_service(self):
        time.sleep(1)
        server_config_service = ws.server_config_ws_services.BindingSOAP("http://%s:%d" % (self.server_config_ip, self.server_config_port))

        session_id = self._login(server_config_service)
        request = ws.server_config_ws_services.GOnManagementServiceGetStatusRequest()
        request_content = ws.server_config_ws_services.ns0.GOnManagementServiceGetStatusRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.GOnManagementServiceGetStatus(request)

        id = response.get_element_content().get_attribute_id()
        name = response.get_element_content().get_attribute_name()
        status = response.get_element_content().get_attribute_status()
        print "GOnManagementServiceGetStatus", id, name, status


#        request = ws.server_config_ws_services.GOnManagementServiceControlRequest()
#        request_content = ws.server_config_ws_services.ns0.GOnManagementServiceControlRequestType_Def(None).pyclass()
#        request_content.set_element_session_id(session_id)
#        request_content.set_attribute_command('restart')
#        request.set_element_content(request_content)
#        response = server_config_service.GOnManagementServiceControl(request)
#
#        rc = response.get_element_content().get_element_rc()
#        print "GOnManagementServiceControl rc:", rc



#    def test_ping(self):
#        time.sleep(1)
#        server_config_service = ws.server_config_ws_services.BindingSOAP("http://%s:%d" % (self.server_config_ip, self.server_config_port))
#
#        # Ping when login-ed
#        session_id = self._login(server_config_service)21815
#        request = ws.server_config_ws_services.PingRequest()
#        request_content = ws.server_config_ws_services.ns0.PingRequestType_Def(None).pyclass()
#        request_content.set_element_session_id(session_id)
#        request.set_element_content(request_content)
#        response = server_config_service.Ping(request)
#        rc = response.get_element_content().get_element_rc()
#        self.assertTrue(rc)
#        self._logout(server_config_service, session_id)
#
#        # Ping when not login-ed
#        request = ws.server_config_ws_services.PingRequest()
#        request_content = ws.server_config_ws_services.ns0.PingRequestType_Def(None).pyclass()
#        request_content.set_element_session_id(session_id)
#        request.set_element_content(request_content)
#        response = server_config_service.Ping(request)
#        rc = response.get_element_content().get_element_rc()
#        self.assertFalse(rc)

#
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'
GIRI_UNITTEST_TIMEOUT = 60 * 6

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
