"""
This file contains the
"""
import datetime

from components.rest.rest_api import RestApi

import config_specification

import lib.ws.job

import lib.checkpoint
import server_config_jobs
import server_config_job_installation
import server_config_job_change
import server_config_job_upgrade
import version_history

import lib.appl.gon_system_api


import components.plugin.server_config.manager
import components.plugin.server_config.plugin_socket_config_specification
import components.user.server_config.user_config as user_config

MODULE_ID = "appl_gon_config_service"


class SessionError(Exception):
    def __init__(self, error_message):
        Exception.__init__(self)
        self.error_message = error_message
    def __str__(self):
        return repr(self.error_message)


class ServerConfigWSSession(object):
    CONFIG_SPECIFICATION_TYPE_INSTALL = 0
    CONFIG_SPECIFICATION_TYPE_CHANGE = 1
    CONFIG_SPECIFICATION_TYPE_UPGRADE = 2

    """
    This class represent a session in the server config web service
    """
    def __init__(self, checkpoint_handler, server_configuration,  session_id, username, password):
        self.checkpoint_handler = checkpoint_handler
        self.server_configuration = server_configuration
        self.session_id = session_id
        self.username = username
        self.password = password
        self._last_activity = datetime.datetime.now()
        self._job_handler = lib.ws.job.JobHandler()
        self._config_specification_type = ServerConfigWSSession.CONFIG_SPECIFICATION_TYPE_INSTALL
        self._config_specifications = []
        self._config_specification_next = None
        self.plugin_manager = components.plugin.server_config.manager.Manager(checkpoint_handler, server_configuration.configuration_server_config.plugin_modules_abspath, server_configuration)
        self.config_specification_socket = components.plugin.server_config.plugin_socket_config_specification.PluginSocket(self.plugin_manager)
        self._gon_systems = None
        self._upgrade_session = None
        self._install_session = None
        self._change_session = None

    def report_activity(self):
        self._last_activity = datetime.datetime.now()

    def is_dead(self, timeout):
        return datetime.datetime.now() - self._last_activity > timeout

    def GetStatus(self, request):
        response = RestApi.ServerConfig.GetStatusResponse()
        response.set_configured(self.server_configuration.is_configured())
        return response

    def _get_gon_systems(self):
        if self._gon_systems is None:
            self._gon_systems = []
            self._gon_systems += lib.appl.gon_system_api.GOnSystem.create_list_installed(self.checkpoint_handler)
            backup_path = self.server_configuration.configuration_server_config.backup_abspath
            self._gon_systems += lib.appl.gon_system_api.GOnSystem.create_list_backuped(backup_path)

        self._gon_systems = sorted(self._gon_systems, lib.appl.gon_system_api.GOnSystem.compare_by_version, reverse=True)
        return self._gon_systems

    def _lookup_gon_system(self, name):
        gon_systems = self._get_gon_systems()
        for gon_system in gon_systems:
            if gon_system.get_name() == name:
                return gon_system
        return None

    def _reset_gon_systems(self):
        self._gon_systems = None

    def GetJobInfo(self, request):
        job_id = request.get_job_id
        job = self._job_handler.get_job(job_id)
        if job is not None:
            job_status = job.get_status()
            response = RestApi.ServerConfig.GetJobInfoResponse()
            response_info = RestApi.AdminDeploy.Job.JobInfo()
            response_info.set_job_more(job_status.more)
            response_info.set_job_status(job_status.get_response_job_status())
            response_info.set_job_header(job_status.header)
            response_info.set_job_sub_header(job_status.sub_header)
            response_info.set_job_progress(job_status.progress)
            if job_status.done_message is not None:
                response_info.set_job_done_message(job_status.done_message)
            if job_status.done_arg_01 is not None:
                response_info.set_job_done_arg_01(job_status.done_arg_01)
            response.set_job_info(response_info)
            return response
        raise SessionError('Invalid job id %d' % job_id)

    def CancelJob(self, request):
        job_id = request.get_job_id
        job = self._job_handler.get_job(job_id)
        if job is not None:
            job.cancel()
            response = RestApi.ServerConfig.CancelJobResponse()
            response.set_rc(True)
            return response
        raise SessionError('Invalid job id %d' % job_id)

    def StartJobGenerateKnownsecrets(self):
        job = server_config_jobs.JobGenerateKnownsecrets(self.server_configuration, self.checkpoint_handler)
        job_id = self._job_handler.add_job(job)
        response = RestApi.ServerConfig.StartJobGenerateKnownsecretsResponse()
        response.set_job_id(job_id)
        return response

    def StartJobGenerateGPMS(self):
        job = server_config_jobs.JobGenerateGPMS(self.server_configuration, self.checkpoint_handler)
        job_id = self._job_handler.add_job(job)
        response = RestApi.ServerConfig.StartJobGenerateGPMSResponse()
        response.set_job_id(job_id)
        return response

    def StartJobGenerateDemodata(self):
        job = server_config_jobs.JobGenerateDemodata(self.server_configuration, self.checkpoint_handler)
        job_id = self._job_handler.add_job(job)
        response = RestApi.ServerConfig.StartJobGenerateDemodataResponse()
        response.set_job_id(job_id)
        return response

    def StartJobInstallServices(self):
        job = server_config_jobs.JobInstallServices(self.server_configuration, self.checkpoint_handler)
        job_id = self._job_handler.add_job(job)
        response = RestApi.ServerConfig.StartJobInstallServicesResponse()
        response.set_job_id(job_id)
        self._reset_gon_systems()
        return response

    def StartJobGenerateSupportPackage(self):
        job = server_config_jobs.JobGenerateSupportPackage(self.server_configuration, self.checkpoint_handler, self.config_specification_socket)
        job_id = self._job_handler.add_job(job)
        response = RestApi.ServerConfig.StartJobGenerateSupportPackageResponse()
        response.set_job_id(job_id)
        return response

    def StartJobPrepareInstallation(self):
        self._config_specification_type = ServerConfigWSSession.CONFIG_SPECIFICATION_TYPE_INSTALL
        self._config_specifications = []
        self._install_session = server_config_job_installation.JobInstallationSession(self.checkpoint_handler, self.server_configuration)
        job = server_config_job_installation.JobPrepareInstallation(self._install_session)
        job_id = self._job_handler.add_job(job)
        response = RestApi.ServerConfig.StartJobPrepareInstallationResponse()
        response.set_job_id(job_id)
        return response

    def StartJobFinalizeInstallation(self):
        job = server_config_job_installation.JobFinalizeInstallation(self._install_session, self._config_specifications)
        job_id = self._job_handler.add_job(job)
        response = RestApi.ServerConfig.StartJobFinalizeInstallationResponse()
        response.set_job_id(job_id)
        self._install_session = None
        self._config_specifications = []
        self._reset_gon_systems()
        return response

    def StartJobPrepareChange(self):
        self._config_specification_type = ServerConfigWSSession.CONFIG_SPECIFICATION_TYPE_CHANGE
        self._config_specifications = []
        self._change_session = server_config_job_change.JobChangeSession(self.checkpoint_handler, self.server_configuration)
        job = server_config_job_change.JobPrepareChange(self._change_session)
        job_id = self._job_handler.add_job(job)
        response = RestApi.ServerConfig.StartJobPrepareChangeResponse()
        response.set_job_id(job_id)
        return response

    def StartJobFinalizeChange(self):
        job = server_config_job_change.JobFinalizeChange(self._change_session, self._config_specifications)
        job_id = self._job_handler.add_job(job)
        response = RestApi.ServerConfig.StartJobFinalizeChangeResponse()
        response.set_job_id(job_id)
        self._change_session = None
        self._config_specifications = []
        self._reset_gon_systems()
        return response

    def StartJobPrepareUpgrade(self, request):
        self._config_specification_type = ServerConfigWSSession.CONFIG_SPECIFICATION_TYPE_UPGRADE
        self._config_specifications = []
        system_name = request.get_system_name
        gon_system = self._lookup_gon_system(system_name)
        if gon_system is None:
            raise SessionError('Invalid system_name %s' % system_name)

        self._upgrade_session = server_config_job_upgrade.JobUpgradeSession(self.checkpoint_handler, self.server_configuration, gon_system)
        job = server_config_job_upgrade.JobPrepareUpgrade(self._upgrade_session)
        job_id = self._job_handler.add_job(job)
        response = RestApi.ServerConfig.StartJobPrepareUpgradeResponse()
        response.set_job_id(job_id)
        return response

    def StartJobFinalizeUpgrade(self):
        if self._upgrade_session is None:
            raise SessionError('Invalid upgrade session (PrepareUpgrade has not been called)')
        job = server_config_job_upgrade.JobFinalizeUpgrade(self._upgrade_session, self.config_specification_socket)
        job_id = self._job_handler.add_job(job)
        response = RestApi.ServerConfig.StartJobFinalizeUpgradeResponse()
        response.set_job_id(job_id)
        self._upgrade_session = None
        self._config_specifications = []
        self._reset_gon_systems()
        return response

    def _generate_config_specifications(self):
        if not self._config_specifications:
            self._config_specifications = []
            self._config_specification_next = None
            if self._config_specification_type in [ServerConfigWSSession.CONFIG_SPECIFICATION_TYPE_INSTALL, ServerConfigWSSession.CONFIG_SPECIFICATION_TYPE_CHANGE]:
                self.server_configuration.reload()
                is_install = self._config_specification_type == ServerConfigWSSession.CONFIG_SPECIFICATION_TYPE_INSTALL
                self._config_specifications.append(config_specification.ServerSpecificationDatabaseChoice(self.checkpoint_handler, self.server_configuration, is_install))
                self._config_specifications.append(config_specification.ServerSpecificationDatabase(self.checkpoint_handler, self.server_configuration, is_install))
                self._config_specifications.append(config_specification.ServerSpecificationManagementServer(self.checkpoint_handler, self.server_configuration))
                self._config_specifications.append(config_specification.ServerSpecificationGatewayServer(self.checkpoint_handler, self.server_configuration))
                self._config_specifications.append(config_specification.ServerSpecificationGatewayServerToH(self.checkpoint_handler, self.server_configuration))
                user_configuration = user_config.UserDirectoriesSpecification(self.checkpoint_handler, self.server_configuration, self.config_specification_socket)
                if user_configuration.has_config():
                    self._config_specifications.append(user_configuration)
                self._config_specifications.extend(self._get_plugin_specifications())
            elif self._config_specification_type in [ServerConfigWSSession.CONFIG_SPECIFICATION_TYPE_UPGRADE]:
                self.server_configuration.reload()
                self._config_specifications.append(config_specification.ServerSpecificationDatabaseUpgrade(self.checkpoint_handler, self.server_configuration, self._upgrade_session))
        self._config_specification_next = 0


    def _compare_plugins(self, plugin1, plugin2):
        plugin_type1, priority1 = plugin1.get_type_and_priority()
        plugin_type2, priority2 = plugin2.get_type_and_priority()
        if plugin_type1 > plugin_type2:
            return 1
        elif plugin_type1 < plugin_type2:
            return -1
        else:
            if priority1 > priority2:
                return 1
            elif priority1 < priority2:
                return -1
            else:
                return 0


    def _get_plugin_specifications(self):
        plugins = self.config_specification_socket.get_plugins_sorted()
        return [plugin for plugin in plugins if plugin.has_config()]

    def _SetNextConfigSpecification(self, response):
        if self._config_specification_next is not None:
            current = self._config_specification_next
            self._config_specification_next += 1

            if current >= len(self._config_specifications):
                return

            if self._config_specification_next >= len(self._config_specifications):
                self._config_specification_next = None

            config_specifiation = self._config_specifications[current].get_config_specification()
            if config_specifiation:
                response.set_config_specification(config_specifiation)
            else:
                self._SetNextConfigSpecification(response)

    def GetFirstConfigSpecification(self):
        self._generate_config_specifications()
        response = RestApi.ServerConfig.GetFirstConfigSpecificationResponse()
#        if len(self._config_specifications) > 0:
#            response_content.set_element_config_specification(self._config_specifications[0].get_config_specification())
        self._SetNextConfigSpecification(response)
        return response

    def GetNextConfigSpecification(self):
        response = RestApi.ServerConfig.GetNextConfigSpecificationResponse()

        self._SetNextConfigSpecification(response)
#        if self._config_specification_next is not None:
#            current = self._config_specification_next
#            self._config_specification_next += 1
#
#            if self._config_specification_next >= len(self._config_specifications):
#                self._config_specification_next = None
#            response_content.set_element_config_specification(self._config_specifications[current].get_config_specification())

        return response

    def SaveConfigSpecification(self, request):
        config_specification = request.get_config_specification

        index_next = 0
        for server_config_specification in self._config_specifications:
            index_next += 1
            save_response = server_config_specification.save(config_specification)
            if save_response is not None:
                break

        assert save_response is not None
        if index_next >= len(self._config_specifications):
            self._config_specification_next = None
        else:
            self._config_specification_next = index_next
        return save_response

    def ReloadConfigSpecification(self, request):
        current = self._config_specification_next
        config_specifiation = self._config_specifications[current].get_config_specification()

        response = RestApi.ServerConfig.ReloadConfigSpecificationResponse()
        response.set_config_specification(config_specifiation)

        return response

    def GetConfigTemplate(self, request):
        config_template_spec = request.get_config_template_spec

        module_id = config_template_spec.get_entity_type

        rc = False
        template = None
        for server_config_specification in self._config_specifications:
            if server_config_specification.ID == module_id:
                template = server_config_specification.get_template(config_template_spec)
                break

        if not template:
            self.checkpoint_handler.Checkpoint("SaveConfigTemplate", MODULE_ID, lib.checkpoint.ERROR, msg="Unable to find module", module_id=module_id)
            raise Exception("Unable to find module '%s'" % module_id)

        response = RestApi.ServerConfig.GetConfigTemplateResponse()
        response.set_config_template(template)
        return response

    def SaveConfigTemplate(self, request):
        config_template = request.get_config_template
        module_id = request.get_entity_type
        sub_type = request.get_sub_type
        element_id = request.get_element_id

        rc = False
        found = False
        for server_config_specification in self._config_specifications:
            if server_config_specification.ID == module_id:
                save_response = server_config_specification.save_template(config_template, element_id=element_id, sub_type=sub_type)
                found = True
                break

        if not found:
            self.checkpoint_handler.Checkpoint("SaveConfigTemplate", MODULE_ID, lib.checkpoint.ERROR, msg="Unable to find module", module_id=module_id)
            raise Exception("Unable to find module '%s'" % module_id)

        return save_response

    def DeleteConfigTemplate(self, request):
        config_template_spec = request.get_config_spec
        module_id = config_template_spec.get_entity_type

        rc = False
        found = False
        for server_config_specification in self._config_specifications:
            if server_config_specification.ID == module_id:
                rc = server_config_specification.delete_template(config_template_spec)
                found = True
                break

        if not found:
            self.checkpoint_handler.Checkpoint("SaveConfigTemplate", MODULE_ID, lib.checkpoint.ERROR, msg="Unable to find module", module_id=module_id)
            raise Exception("Unable to find module '%s'" % module_id)

        response = RestApi.ServerConfig.DeleteConfigTemplateResponse()
        response.set_rc(rc)
        return response

    def TestConfigSpecification(self, request):
        config_specification = request.get_config_specification
        module_id = request.get_module_id
        sub_type = request.get_sub_type
        test_type = request.get_test_type

        rc = False
        msg = "Internal error : could not perform action"
        found = False
        for server_config_specification in self._config_specifications:
            if server_config_specification.ID == module_id:
                rc, msg = server_config_specification.test_config(config_specification, test_type=test_type, sub_type=sub_type)
                found = True
                break

        if not found:
            self.checkpoint_handler.Checkpoint("TestConfigSpecification", MODULE_ID, lib.checkpoint.ERROR, msg="Unable to find module for test", module_id=module_id)

        response = RestApi.ServerConfig.TestConfigSpecificationResponse()
        response.set_rc(rc)
        response.set_message(msg)
        if rc:
            response.set_config_specification(config_specification)

        return response

    def GetServices(self):
        response = RestApi.ServerConfig.GetServicesResponse()

        services = lib.appl.service.GService.create_list()
        response_services = []
        for service in services:
            response_service = RestApi.ServerConfig.Service()
            response_service.set_name(service.service_name)
            response_service.set_title(service.service_title)
            response_service.set_description(service.service_description)
            response_service.set_is_installed(service.is_installed())
            response_service.set_is_running(service.is_running())
            response_service.set_is_current_version(service.is_current_version())
            response_services.append(response_service)
        response.set_services(response_services)
        return response

    def GetGOnSystems(self):
        response = RestApi.ServerConfig.GetGOnSystemsResponse()

        gon_systems = self._get_gon_systems()
        response_systems = []
        default_selected = False
        for gon_system in gon_systems:
            version = gon_system.version.get_version_string()
            if version is not None:
                response_system = RestApi.ServerConfig.GOnSystem()
                response_system.set_name(gon_system.get_name())
                response_system.set_title(gon_system.get_title())
                response_system.set_version(gon_system.version.get_version_string())
                (management_running, gateway_running) = gon_system.is_running()
                response_system.set_management_is_running(management_running)
                response_system.set_gateway_is_running(gateway_running)
                response_system.set_is_current_version(gon_system.is_current_version())
                is_current_installation = gon_system.is_current_installation(self.server_configuration.get_config_service_path_abs())
                response_system.set_is_current_installation(is_current_installation)
                do_version_path_exists = lib.version.Version.do_version_path_exists(gon_system.version, lib.version.Version.create_current(), version_history.version_map)
                can_be_used_for_upgrade = do_version_path_exists and not is_current_installation
                response_system.set_can_be_used_for_upgrade(can_be_used_for_upgrade)
                response_system.set_default_for_upgrade(False)
                if do_version_path_exists and not default_selected:
                    response_system.set_default_for_upgrade(True)
                    default_selected = True
                response_system.set_is_installation(gon_system.is_system_type_installation());
                response_systems.append(response_system)

        response.set_gon_systems(response_systems)
        return response

    def GetLicenseInfo(self):
        response = RestApi.ServerConfig.GetLicenseInfoResponse()
        license_info = None

        license = self.server_configuration.configuration_server_config.get_license_handler().get_license()
        if len(license.errors) > 0:
            self.checkpoint_handler.CheckpointMultilineMessages("GetLicenseInf.error", MODULE_ID, lib.checkpoint.ERROR, license.errors)
        if len(license.warnings) > 0:
            self.checkpoint_handler.CheckpointMultilineMessages("GetLicenseInfo.warning", MODULE_ID, lib.checkpoint.WARNING, license.warnings)

        if license.valid:
            license_info = RestApi.ServerConfig.LicenseInfo()
            license_info.set_licensed_to(license.get_one('Licensed To'))
            license_info.set_license_number(license.get_one('License Number'))
            license_info.set_license_file_number(license.get_one('License File Number'))
            license_info.set_license_expires(self._create_date_type(license.get_date('License Expiration Date')))
            license_info.set_maintenance_expires(self._create_date_type(license.get_date('Maintenance Expiration Date')))
            license_info.set_is_default_license(license.default_lic)
            license_info.set_login_text(license.get_all('Login Message', default=[]))

        response.set_license_info(license_info)
        return response

    def SetLicense(self, request):
        license = request.get_license
        rc = self.server_configuration.configuration_server_config.get_license_handler().set_license(license)
        response = RestApi.ServerConfig.SetLicenseResponse()
        response.set_rc(rc)
        return response

    def GOnManagementServiceGetStatus(self, request):
        response = RestApi.ServerConfig.GOnManagementServiceGetStatusResponse()
        service_management = lib.appl.service.GService.create_service_for_current_management_service(root=self.server_configuration.get_instance_path_abs())
        if service_management is not None:
            response.set_id(service_management.service_name)
            response.set_name(service_management.service_title)
            if service_management.is_running():
                response.set_status('running')
            else:
                response.set_status('not_running')
        else:
            response.set_id("Unknown")
            response.set_name("Unknown")
            response.set_status('service_not_found')

        return response

    def GOnManagementServiceControl(self, request):
        response = RestApi.ServerConfig.GOnManagementServiceControlResponse()

        command = request.get_command
        rc = False
        service_management = lib.appl.service.GService.create_service_for_current_management_service(root=self.server_configuration.get_instance_path_abs())
        if service_management is not None:
            if command == 'stop':
                service_management.stop_and_forget()
                rc = True
            elif command == 'restart':
                service_management.restart_and_forget()
                rc = True
            elif command == 'start':
                service_management.start_and_forget()
                rc = True
        response.set_rc(rc)

        return response


    def _create_date_type(self, date_time):
        if date_time is None:
            return None

        date_type = RestApi.ServerConfig.Date()
        date_type.set_yyyy(date_time.year)
        date_type.set_mm(date_time.month)
        date_type.set_dd(date_time.day)
        return date_type

