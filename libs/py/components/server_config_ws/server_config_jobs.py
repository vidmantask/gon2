"""
This module contains the jobs for functionlaity for the server configuration
"""
from __future__ import with_statement

import sys
import os
import os.path
import shutil
import tempfile
import glob
import datetime

import lib.ws.job
import lib.cryptfacility
import lib.checkpoint
import lib.version

import lib.gpm
import lib.gpm.gpm_env
import lib.gpm.gpm_builder
import lib.gpm.gpm_analyzer

import lib.appl.service
import lib.appl.upgrade
import lib.appl.gon_system_api
if sys.platform == "win32":
    import lib.appl.gon_system_win

from components.server_config_ws import MODULE_ID
import components.server_config_ws.database_schema
import components.access_log.server_common.database_schema
import components.endpoint.server_common.database_schema

import components.config.common

import components.database.server_common.schema_api
import components.database.server_common.schema_api as schema_api
import components.database.server_common.database_api as database_api

import components.environment
import demodata.demodata_default

import components.management_message.server_common.session_element_filedist

import components.plugin.server_config.plugin_socket_upgrade
from components.database.server_common import connection_factory


EVENTS_FOLDER = 'events'


def get_backup_version(checkpoint_handler, other_installation_path):
    if sys.platform == 'win32':
        return lib.appl.gon_system_win.get_system_version(checkpoint_handler, other_installation_path)
    return None


def create_logger(checkpoint_handler, logger_type_name, folder_abs):
    if not os.path.exists(folder_abs):
        os.makedirs(folder_abs)
    logger_date = datetime.datetime.now().strftime('%Y-%m-%d_%H%M%S')
    logger_filename = '%s_%s_log.txt' % (logger_date, logger_type_name)
    logger_filename_abs = os.path.join(folder_abs, logger_filename)
    logger = lib.checkpoint.CheckpointHandler(lib.checkpoint.CheckpointFilter_true(), lib.checkpoint.CheckpointOutputHandlerTEXT(logger_filename_abs, False), checkpoint_handler)
    return (logger_filename_abs, logger)

def create_logger_in_sub_folder(checkpoint_handler, event_type_name, folder_abs):
    event_date = datetime.datetime.now().strftime('%Y-%m-%d_%H%M%S')
    event_name = '%s_%s' % (event_date, event_type_name)
        
    event_folder = os.path.join(folder_abs, EVENTS_FOLDER, event_name)
    if not os.path.exists(event_folder):
        os.makedirs(event_folder)
        
    logger_filename = '%s_log.txt' % (event_name)
    logger_filename_abs = os.path.join(event_folder, logger_filename)
    logger = lib.checkpoint.CheckpointHandler(lib.checkpoint.CheckpointFilter_true(), lib.checkpoint.CheckpointOutputHandlerTEXT(logger_filename_abs, False), checkpoint_handler)
    return (logger_filename_abs, logger, event_folder)


class JobGenerateKnownsecrets(lib.ws.job.Job):
    """
    This class holds the functionality for generating knownsecrets used by communication component to 
    establish a secure connection.
    """
    def __init__(self, server_configuration, checkpoint_handler):
        lib.ws.job.Job.__init__(self, checkpoint_handler, MODULE_ID, 'job_generate_knownsecret')
        self.server_configuration = server_configuration
        
    def do_run(self):
        with self.checkpoint_handler.CheckpointScope("generate_knownsecrets", MODULE_ID, lib.checkpoint.DEBUG):

            deployed_path_abs = self.server_configuration.configuration_server_config.deployment_abspath
            if not os.path.exists(deployed_path_abs):
                os.makedirs(deployed_path_abs)

            # Knownsecrets for client connections
            secrets = lib.cryptfacility.generate_secrets()
            client_ks_filename = os.path.join(deployed_path_abs, components.config.common.KS_CLIENT_FILENAME)
            ks_client_file = open(client_ks_filename, 'w')
            ks_client_file.write(secrets[0])
            ks_client_file.close()

            server_ks_filename = os.path.join(deployed_path_abs, components.config.common.KS_SERVER_FILENAME)
            ks_server_file = open(server_ks_filename, 'w')
            ks_server_file.write(secrets[1])
            ks_server_file.close()
            
            # Knownsecrets for management service connections
            secrets = lib.cryptfacility.generate_secrets()
            client_ks_filename = os.path.join(deployed_path_abs, components.config.common.SERVICE_MANAGEMENT_KS_CLIENT_FILENAME)
            ks_client_file = open(client_ks_filename, 'w')
            ks_client_file.write(secrets[0])
            ks_client_file.close()

            server_ks_filename = os.path.join(deployed_path_abs, components.config.common.SERVICE_MANAGEMENT_KS_SERVER_FILENAME)
            ks_server_file = open(server_ks_filename, 'w')
            ks_server_file.write(secrets[1])
            ks_server_file.close()

        self.set_status_done()

    def do_cancel(self):
        pass



class JobGenerateGPMS(lib.ws.job.Job):
    """
    This class holds functionality for generating GPMs from gpm sepcificationse, and for calculating dependency between the packages.
    """
    def __init__(self, server_configuration, checkpoint_handler):
        lib.ws.job.Job.__init__(self, checkpoint_handler, MODULE_ID, 'job_generate_gpms')
        self.server_configuration = server_configuration
        self._do_cancel = False
        
    def do_run(self):
        with self.checkpoint_handler.CheckpointScope("generate_gpms", MODULE_ID, lib.checkpoint.DEBUG):

            self.set_status('Preparing generation of GPMS', '', 0)
            gpm_build_path_abs = self.server_configuration.configuration_server_config.cpm_gpm_build_abspath
            gpm_defs_path_abs = self.server_configuration.configuration_server_config.cpm_gpm_defs_abspath
            gpmc_defs_path_abs = self.server_configuration.configuration_server_config.cpm_gpmc_defs_abspath
            gpms_path_abs = self.server_configuration.configuration_server_config.cpm_gpms_abspath
            dist_path_abs = self.server_configuration.configuration_server_config.cpm_dist_abspath
            filedist_root = self.server_configuration.configuration_server_config.service_management_filedist_abspath
            
            tick_count = 0
            tick_count_max = 3 #For Dist build, Dependency and Cleaning up 

            if not os.path.exists(gpm_defs_path_abs):
                os.makedirs(gpm_defs_path_abs)
            
            if not os.path.exists(gpm_build_path_abs):
                os.makedirs(gpm_build_path_abs)
    
            if not os.path.exists(gpms_path_abs):
                os.makedirs(gpms_path_abs)

            if not os.path.exists(gpmc_defs_path_abs):
                os.makedirs(gpmc_defs_path_abs)

            for gpmdef_filename in glob.glob(os.path.join(gpm_defs_path_abs, '*.gpmdef.xml')):
                tick_count_max += 1

            gpm_env = lib.gpm.gpm_env.GpmBuildEnv()
            gpm_env.build_root = gpm_build_path_abs
            gpm_env.dest_root = gpms_path_abs
            gpm_env.temp_root = tempfile.mkdtemp()

            gpm_builder = lib.gpm.gpm_builder.GpmBuilder(gpm_env)
            gpm_error_handler = lib.gpm.gpm_env.GpmErrorHandler()

            for gpmdef_filename in glob.glob(os.path.join(gpm_defs_path_abs, '*.gpmdef.xml')):
                self.checkpoint_handler.Checkpoint("generate_gpm", MODULE_ID, lib.checkpoint.DEBUG, gpmdef_filename=gpmdef_filename)
                gpmdef_filename_short = os.path.basename(gpmdef_filename)
                self.set_status('Generating GPM', gpmdef_filename_short, (tick_count * 100)/tick_count_max)
                tick_count += 1
                gpm_builder.build_from_file(gpmdef_filename, gpm_error_handler)
        
                if self._do_cancel:
                    shutil.rmtree(gpm_env.temp_root)
                    return
        
       
            if gpm_error_handler.error():
                self.set_status_error('Generation error', gpm_error_handler.dump_as_string())
                shutil.rmtree(gpm_env.temp_root)
                return

        if self._do_cancel:
            shutil.rmtree(gpm_env.temp_root)
            return

        with self.checkpoint_handler.CheckpointScope("generate_gpms_dependency", MODULE_ID, lib.checkpoint.DEBUG):
            self.set_status('Generating GPM dependency', '', (tick_count * 100)/tick_count_max)
            tick_count += 1

            gpm_reporitory = lib.gpm.gpm_analyzer.GpmRepository()
            gpm_reporitory.init_from_gpms(gpm_error_handler, gpms_path_abs, gpmc_defs_path_abs)
            gpm_reporitory.generate_dot_files(gpms_path_abs)
            gpm_reporitory.generate_repository_cache(gpms_path_abs)

            if gpm_error_handler.error():
                self.set_status_error('Dependency error', gpm_error_handler.dump_as_string())
                shutil.rmtree(gpm_env.temp_root)
                return


            self.set_status('Building dist folders', '', (tick_count * 100)/tick_count_max)
            tick_count += 1

            dist_client_collection_id = 'dist_client_installer_win'
            dist_info = gpm_reporitory.get_gpm_collection_info(dist_client_collection_id)
            if dist_info is not None:
                dist_client_collection_path = os.path.join(dist_path_abs, 'gon_client_installer', 'win', 'nsis', 'gpms')
                if not os.path.isdir(dist_client_collection_path):
                    os.makedirs(dist_client_collection_path)
                for package_filename in gpm_reporitory.ids_to_filenames(gpms_path_abs, dist_info['gpm_ids']):
                    shutil.copy(package_filename, dist_client_collection_path)
            else:
                gpm_error_handler.emmit_warning("Dist collection for G/On Client Installer '%s' was not found" % dist_client_collection_id)

#
# Disabled because gon_client_stealth is disabled for now
#
#            dist_client_stealth_collection_id = 'dist_gon_client_stealth_runner_win'
#            dist_info = gpm_reporitory.get_gpm_collection_info(dist_client_stealth_collection_id)
#            if dist_info is not None:
#                dist_client_stealth_collection_path = os.path.join(dist_path_abs, 'gon_client_stealth_runner', 'win', 'nsis', 'gpms')
#                if not os.path.isdir(dist_client_stealth_collection_path):
#                    os.makedirs(dist_client_stealth_collection_path)
#                for package_filename in gpm_reporitory.ids_to_filenames(gpms_path_abs, dist_info['gpm_ids']):
#                    shutil.copy(package_filename, dist_client_stealth_collection_path)
#            else:
#                gpm_error_handler.emmit_warning("Dist collection for G/On Client Stealth '%s' was not found" % dist_client_stealth_collection_id)


            self.set_status('Cleaning up', '', (tick_count * 100)/tick_count_max)
            shutil.rmtree(gpm_env.temp_root)

        if self._do_cancel:
            return

        components.management_message.server_common.session_element_filedist.FiledistManager.files_update(self.checkpoint_handler, filedist_root)        

        if gpm_error_handler.warning():
            self.checkpoint_handler.CheckpointMultilineMessage("generate_gpms.warnings", MODULE_ID, lib.checkpoint.WARNING, gpm_error_handler.dump_as_string())
            self.set_status_done('Done with warning', gpm_error_handler.dump_as_string())
        else:
            self.set_status_done()

    def do_cancel(self):
        self._do_cancel = True

    
class JobGenerateDemodata(lib.ws.job.Job):
    """
    This class holds the functionality for generating demodata.
    """
    def __init__(self, server_configuration, checkpoint_handler):
        lib.ws.job.Job.__init__(self, checkpoint_handler, MODULE_ID, 'job_generate_demodata')
        self.server_configuration = server_configuration
        self.is_canceling = False
        
    def do_run(self):
        with self.checkpoint_handler.CheckpointScope("generate_demodata", MODULE_ID, lib.checkpoint.DEBUG):
            db_connect_string = self.server_configuration.configuration_server_config.get_db_connect_url()
            db_encoding = self.server_configuration.configuration_server_config.db_encoding
            db_log_enabled = self.server_configuration.configuration_server_config.db_log_enabled

            environment = components.environment.Environment(self.checkpoint_handler, db_connect_string, MODULE_ID, db_encoding, db_logging=db_log_enabled)

            # Ensure that plugins are loaded in order to registre databases
            # this also load our own 
            environment.plugin_manager = components.plugin.server_config.manager.Manager(self.checkpoint_handler, self.server_configuration.configuration_server_config.plugin_modules_abspath, self.server_configuration)
            upgrade_socket = components.plugin.server_config.plugin_socket_upgrade.PluginSocket(environment.plugin_manager)
            upgrade_socket.import_schema()
            
            demodata.demodata_default.generate_demodata(environment, self.checkpoint_handler, self)
        
        if not self.is_canceling:
            self.set_status_done()

    def do_cancel(self):
        self.is_canceling = True

class JobResetMgmtAccess(lib.ws.job.Job):
    """
    This class holds the functionality for generating demodata.
    """
    def __init__(self, server_configuration, checkpoint_handler):
        lib.ws.job.Job.__init__(self, checkpoint_handler, MODULE_ID, 'job_generate_demodata')
        self.server_configuration = server_configuration
        self.is_canceling = False
        
    def do_run(self):
        with self.checkpoint_handler.CheckpointScope("reset_mgmt_access", MODULE_ID, lib.checkpoint.DEBUG):
            db_connect_string = self.server_configuration.configuration_server_config.get_db_connect_url()
            db_encoding = self.server_configuration.configuration_server_config.db_encoding
            db_log_enabled = self.server_configuration.configuration_server_config.db_log_enabled

            environment = components.environment.Environment(self.checkpoint_handler, db_connect_string, MODULE_ID, db_encoding, db_logging=db_log_enabled)

            # Ensure that plugins are loaded in order to registre databases
            # this also load our own 
            environment.plugin_manager = components.plugin.server_config.manager.Manager(self.checkpoint_handler, self.server_configuration.configuration_server_config.plugin_modules_abspath, self.server_configuration)
            upgrade_socket = components.plugin.server_config.plugin_socket_upgrade.PluginSocket(environment.plugin_manager)
            upgrade_socket.import_schema()
            
            demodata.demodata_default.reset_mgmt_access(self.checkpoint_handler)
        
        if not self.is_canceling:
            self.set_status_done()

    def do_cancel(self):
        self.is_canceling = True

class JobInstallServices(lib.ws.job.Job):
    """
    This class holds the functionality for installing services.

    Gateway and management services are no longer installed by this job,
    as they are installed by the installer.
    
    """
    def __init__(self, server_configuration, checkpoint_handler, start_services=False):
        lib.ws.job.Job.__init__(self, checkpoint_handler, MODULE_ID, 'job_install_services')
        self.server_configuration = server_configuration
        self.start_services = start_services 
        
    def do_run(self):
        with self.checkpoint_handler.CheckpointScope("install_services", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                if not self.server_configuration.configuration_server_config.services_enabled:
                    self.checkpoint_handler.Checkpoint("install_services.handling_of_services_disabled", MODULE_ID, lib.checkpoint.DEBUG)
                    self.set_status_done()
                    return

                self.set_status('Adding installation to registry', '', 95)
                lib.appl.gon_system_api.GOnSystem.add_installed_system_in_registry(self.server_configuration.get_installation_path_abs())

                self.set_status_done()
            except lib.appl.service.GServiceError:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.Checkpoint("install_services.error", MODULE_ID, lib.checkpoint.ERROR, message=str(evalue))
                self.set_status_error('Error Installing Service', evalue)
                
    def do_cancel(self):
        pass


class JobRemoveServices(lib.ws.job.Job):
    """
    This class holds the functionality for removeing installed services.
    """
    def __init__(self, server_configuration, checkpoint_handler):
        lib.ws.job.Job.__init__(self, checkpoint_handler, MODULE_ID, 'job_remove_services')
        self.server_configuration = server_configuration
        
    def do_run(self):
        with self.checkpoint_handler.CheckpointScope("remove_services", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                if not self.server_configuration.configuration_server_config.services_enabled:
                    self.checkpoint_handler.Checkpoint("install_services.handling_of_services_disabled", MODULE_ID, lib.checkpoint.DEBUG)
                    self.set_status_done()
                    return
                
                self.set_status('Removing installation from registry', '', 95)
                lib.appl.gon_system_api.GOnSystem.remove_installed_system_from_registry(self.server_configuration.get_installation_path_abs())

                self.set_status_done()
            except lib.appl.service.GServiceError:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.Checkpoint("remove_services.error", MODULE_ID, lib.checkpoint.ERROR, message=str(evalue))
                self.set_status_error('Error Removing Service', evalue)
                
    def do_cancel(self):
        pass



class JobBackup(lib.ws.job.Job):
    STATUS_FILENAME = 'status'
    STATUS_OK = 'ok'
    STATUS_WARNING = 'warning'
    """
    This class holds the functionality for doing backup of database, and system configurations.
    """
    def __init__(self, checkpoint_handler, server_configuration, config_specification_socket, backup_destination_root, create_subfolder_for_backup=True, alternative_server_configuration=None, reflect_database=False, backup_version=None):
        lib.ws.job.Job.__init__(self, checkpoint_handler, MODULE_ID, 'job_backup')
        self.server_configuration = server_configuration
        self.alternative_server_configuration = alternative_server_configuration
        if self.alternative_server_configuration is None:
            self.alternative_server_configuration = self.server_configuration
        self.config_specification_socket = config_specification_socket
        self.create_subfolder_for_backup = create_subfolder_for_backup
        self.backup_steps = 0 
        self.backup_step_current = 0
        self.reflect_database = reflect_database
        self._cancled = False
        self._error_found = False
        self._warning_found = False
        self.backup_destination_root = backup_destination_root
        self.backup_version = backup_version
        if self.backup_version is None:
            self.backup_version = lib.version.Version.create_current()

        
    def do_run(self):
        with self.checkpoint_handler.CheckpointScope("backup", MODULE_ID, lib.checkpoint.DEBUG):
            self.set_status('Backup', 'Initializing', 0)
            backup_version_str = self.backup_version.get_version_string().strip()
            backup_date = datetime.datetime.now().isoformat('_').replace(':', '')
            if self.create_subfolder_for_backup:
                backup_destination_abs = os.path.normpath(os.path.join(self.backup_destination_root, 'backup_%s_%s' % (backup_version_str, backup_date)))
            else:
                backup_destination_abs = os.path.normpath(self.backup_destination_root)
            if not os.path.exists(backup_destination_abs):
                os.makedirs(backup_destination_abs)
            self.checkpoint_handler.Checkpoint("backup_folder_created", MODULE_ID, lib.checkpoint.DEBUG, backup_destination_abs=backup_destination_abs)
        
            backup_log_filename_abs = os.path.join(backup_destination_abs, 'backup_log.txt') 
            backup_log = lib.checkpoint.CheckpointHandler(lib.checkpoint.CheckpointFilter_true(), lib.checkpoint.CheckpointOutputHandlerTEXT(backup_log_filename_abs, False), self.checkpoint_handler)
            backup_log.Checkpoint("backup_info", MODULE_ID, lib.checkpoint.DEBUG, backup_version=backup_version_str, backup_date=backup_date)

            try:
                lib.appl.upgrade.save_version(backup_destination_abs, version=self.backup_version)
    
                if not self._cancled:
                    self.do_run_db_backup(backup_log, backup_destination_abs)
                if not self._cancled:
                    self.do_run_config_backup(backup_log, backup_destination_abs)
                if not self._cancled:
                    self.do_run_plugin_backup(backup_log, backup_destination_abs)
    
                if self._cancled:
                    backup_log.Checkpoint("backup_cancled", MODULE_ID, lib.checkpoint.DEBUG)
                    self.set_status_canceled()
                    return
                
                if self._error_found:
                    backup_log.Checkpoint("backup_error_found", MODULE_ID, lib.checkpoint.DEBUG)
                    self.set_status_error('Backup', "Error found during backup, see logfile '%s'" %  backup_log_filename_abs)
                    return
                    
                if self._warning_found:
                    backup_log.Checkpoint("backup_warning_found", MODULE_ID, lib.checkpoint.DEBUG)
                    self.set_status_done('Backup', "Warnings found during backup, see logfile '%s'" %  backup_log_filename_abs)
         
                status_file_filename = os.path.join(backup_destination_abs, JobBackup.STATUS_FILENAME)
                status_file = open(status_file_filename, 'wt')
                if self._warning_found:
                    status_file.write(JobBackup.STATUS_WARNING)
                else:
                    status_file.write(JobBackup.STATUS_OK)
                status_file.close()
                self.set_status_done()

            except:
                (etype, evalue, etrace) = sys.exc_info()
                backup_log.CheckpointException("backup_unexpected_error", MODULE_ID, lib.checkpoint.DEBUG, etype, evalue, etrace)
                self.set_status_error('Backup', "Error found during backup, see logfile '%s'" %  backup_log_filename_abs)
                raise

    def do_cancel(self):
        self.set_status_cancel()
        self._cancled = True
       
    def do_run_db_backup(self, backup_log, backup_destination_abs):
        db_backup_root_abs = os.path.join(backup_destination_abs, lib.appl.upgrade.DATABASE_FOLDER)
        if not os.path.exists(db_backup_root_abs):
            os.makedirs(db_backup_root_abs)
        
        db_connect_string = self.alternative_server_configuration.get_db_connect_string_abs()
        with backup_log.CheckpointScope("db_backup", MODULE_ID, lib.checkpoint.DEBUG, db_connect_string=db_connect_string):
            self.set_status('Database backup', 'Initializing', 0)
            
            if not self.reflect_database:
                environment = components.environment.Environment(backup_log, db_connect_string, MODULE_ID, 
                                                                 db_encoding=self.alternative_server_configuration.configuration_server_config.db_encoding,
                                                                 db_logging=self.alternative_server_configuration.configuration_server_config.db_log_enabled)
                
                # Ensure that plugins are loaded in order to registre databases
                # this also load our own 
                environment.plugin_manager = components.plugin.server_config.manager.Manager(backup_log, self.server_configuration.configuration_server_config.plugin_modules_abspath, self.server_configuration)
                upgrade_socket = components.plugin.server_config.plugin_socket_upgrade.PluginSocket(environment.plugin_manager)
                upgrade_socket.import_schema()
    
                components.database.server_common.schema_api.SchemaFactory.backup(db_backup_root_abs, environment.get_default_database(), callback=self, checkpoint_handler=backup_log, log_diff=True)
            else:
                environment = components.environment.Environment(backup_log, None, MODULE_ID, 
                                                                 db_encoding=self.alternative_server_configuration.configuration_server_config.db_encoding,
                                                                 db_logging=self.alternative_server_configuration.configuration_server_config.db_log_enabled)
                
                # Ensure that plugins are loaded in order to registre databases
                # this also load our own 
                environment.plugin_manager = components.plugin.server_config.manager.Manager(backup_log, self.server_configuration.configuration_server_config.plugin_modules_abspath, self.server_configuration)
                upgrade_socket = components.plugin.server_config.plugin_socket_upgrade.PluginSocket(environment.plugin_manager)
                upgrade_socket.import_schema()

                db_connection = connection_factory.ConnectionFactory(db_connect_string, self.alternative_server_configuration.configuration_server_config.db_encoding).get_default_connection(self.alternative_server_configuration.configuration_server_config.db_log_enabled)
                components.database.server_common.schema_api.SchemaFactory.backup_reflect(db_backup_root_abs, db_connection, callback=self, checkpoint_handler=backup_log)


    def cb_db_backup_begin(self, steps):
        self.set_status('Backup database', '', 0)
        self.backup_steps = steps

    def cb_db_backup_step(self, creator_name):
        if self.backup_steps > 0:
            complete = int((self.backup_step_current * 100) / self.backup_steps) 
        else:
            complete = 100
        self.set_status('Backup database', creator_name, complete)
        self.backup_step_current += 1

    def cb_db_backup_end(self):
        self.set_status('Backup database', '', 100)

    def do_run_config_backup(self, backup_log, backup_destination_abs):
        config_backup_root_abs = os.path.join(backup_destination_abs, lib.appl.upgrade.CONFIG_FOLDER)
        if not os.path.exists(config_backup_root_abs):
            os.makedirs(config_backup_root_abs)
        self.server_configuration.backup(backup_log, config_backup_root_abs, self, self.alternative_server_configuration)

    def do_run_plugin_backup(self, backup_log, backup_destination_abs):
        plugins_backup_root_abs = os.path.join(backup_destination_abs, lib.appl.upgrade.PLUGIN_FOLDER)
        if not os.path.exists(plugins_backup_root_abs):
            os.makedirs(plugins_backup_root_abs)
        self.config_specification_socket.backup(backup_log, plugins_backup_root_abs, self, self.alternative_server_configuration)

    def cb_backup_error(self, message):
        self._error_found = True

    def cb_backup_warning(self, message):
        self._warning_found = True



class JobRestore(lib.ws.job.Job):
    """
    This class holds the functionality for doing restoring of a backup.
    """
    def __init__(self, server_configuration, checkpoint_handler, restore_path, config_specification_socket, restore_create_schema, is_upgrade=False):
        lib.ws.job.Job.__init__(self, checkpoint_handler, MODULE_ID, 'job_db_restore')
        self.server_configuration = server_configuration
        self.config_specification_socket = config_specification_socket
        self.restore_path = restore_path
        self.restore_create_schema = restore_create_schema
        
        self.is_upgrade = is_upgrade
        self.restore_steps = 0 
        self.restore_step_current = 0

        self._cancled = False
        self._error_found = False
        self._warning_found = False
        
        self.skip_config_restore = False
        self.skip_db_restore = False

    def do_run(self):
        with self.checkpoint_handler.CheckpointScope("restore", MODULE_ID, lib.checkpoint.DEBUG):
        
            restore_current_version = lib.version.Version.create_current()
            restore_date = datetime.datetime.now().isoformat('_').replace(':', '')
            (restore_log_filename_abs, restore_log, restore_folder) = create_logger_in_sub_folder(self.checkpoint_handler, 'restore', self.server_configuration.get_config_service_path_abs())
            restore_log.Checkpoint("restore_info", MODULE_ID, lib.checkpoint.DEBUG, restore_date=restore_date, restore_current_version=restore_current_version.get_version_string())

            if self.restore_path is None or not os.path.exists(self.restore_path):
                restore_log.Checkpoint("restore_info.backup_folder_not_found", MODULE_ID, lib.checkpoint.ERROR, restore_path=self.restore_path)
                self.set_status_error('Restore', "The backup used for restore can not be found.")
                return
            
            # Check status of used backup
            status_filename = os.path.join(self.restore_path, JobBackup.STATUS_FILENAME)
            if not os.path.exists(status_filename):
                restore_log.Checkpoint("restore_info.backup_status_file_not_found", MODULE_ID, lib.checkpoint.ERROR, status_filename=status_filename)
                self.set_status_error('Restore', "The backup used for restore is invalid, see logfile '%s'." %  restore_log_filename_abs)
                return
                
            status_file = open(status_filename, 'r')
            status = status_file.read()
            if status not in [JobBackup.STATUS_OK, JobBackup.STATUS_WARNING]:
                restore_log.Checkpoint("restore_info.backup_status_invalid", MODULE_ID, lib.checkpoint.ERROR, status=status)
                self.set_status_error('Restore', "The backup used for restore is invalid, see logfile '%s'." %  restore_log_filename_abs)
                return
            if status in [JobBackup.STATUS_WARNING]:
                restore_log.Checkpoint("restore_info.backup_status_had_wwarning", MODULE_ID, lib.checkpoint.WARNING)
                self._warning_found = True

            # Check version of used backup
            version_filename = os.path.join(self.restore_path, lib.appl.upgrade.VERSION_FILENAME)
            if not os.path.exists(version_filename):
                restore_log.Checkpoint("restore_info.version_file_not_found", MODULE_ID, lib.checkpoint.ERROR, version_filename=version_filename)
                self.set_status_error('Restore', "The backup used for restore has no version, see logfile '%s'." %  restore_log_filename_abs)
                return
            
            version_file = open(version_filename, 'r')
            version_backup = lib.version.Version.create_from_string(version_file.read())
            version_file.close()
            if not restore_current_version.equal_major_minor_bugfix(version_backup):
                restore_log.Checkpoint("restore_info.invalid_restore_version", MODULE_ID, lib.checkpoint.ERROR, restore_current_version=restore_current_version.get_version_string(), version_backup=version_backup.get_version_string())
                self.set_status_error('Restore', "The backup used for restore has invalid version, see logfile '%s'." %  restore_log_filename_abs)
                return

            try:
                # Restore of config(ini-files) just be done before restoring the database
                if not self._cancled and not self.skip_config_restore:
                    self.do_run_config_restore(restore_log)

                if not self._cancled and not self.skip_db_restore:
                    self.do_run_db_restore(restore_log)
                elif self.skip_db_restore:
                    restore_log.Checkpoint("restore_db", MODULE_ID, lib.checkpoint.INFO, msg="Database restore skipped")
    
                if not self._cancled:
                    self.do_run_plugin_restore(restore_log)
    
                if self._cancled:
                    restore_log.Checkpoint("restore_cancled", MODULE_ID, lib.checkpoint.DEBUG)
                    self.set_status_canceled()
                    return
                
                if self._error_found:
                    restore_log.Checkpoint("restore_error_found", MODULE_ID, lib.checkpoint.DEBUG)
                    self.set_status_error('Restore', "Error found during restore, see logfile '%s'" %  restore_log_filename_abs)
                    return
                    
                if self._warning_found:
                    restore_log.Checkpoint("restore_warning_found", MODULE_ID, lib.checkpoint.DEBUG)
                    self.set_status_done('Restore', "Warnings found during restore, see logfile '%s'" %  restore_log_filename_abs)
                    return
         
                self.set_status_done()
            except:
                (etype, evalue, etrace) = sys.exc_info()
                restore_log.CheckpointException("unexpected_error", MODULE_ID, lib.checkpoint.DEBUG, etype, evalue, etrace)
                self.set_status_error('Restore', "Error found during restore, see logfile '%s'" %  restore_log_filename_abs)
                raise

    def do_cancel(self):
        self.set_status_cancel()
        self._cancled = True

    def do_run_db_restore(self, restore_log):
        db_restore_root_abs = os.path.join(self.restore_path, lib.appl.upgrade.DATABASE_FOLDER)
        with restore_log.CheckpointScope("db_restore", MODULE_ID, lib.checkpoint.DEBUG, db_restore_root_abs=db_restore_root_abs, db_connect_string=self.server_configuration.configuration_server_config.get_db_connect_info()):
            self.set_status('Restore database', 'Initializing', 0)

            environment = components.environment.Environment(self.checkpoint_handler, self.server_configuration.configuration_server_config.get_db_connect_url(), MODULE_ID, 
                                                             db_encoding=self.server_configuration.configuration_server_config.db_encoding,
                                                             db_logging=self.server_configuration.configuration_server_config.db_log_enabled)
            
            # Ensure that plugins are loaded in order to registre databases
            environment.plugin_manager = components.plugin.server_config.manager.Manager(restore_log, self.server_configuration.configuration_server_config.plugin_modules_abspath, self.server_configuration)

            upgrade_socket = components.plugin.server_config.plugin_socket_upgrade.PluginSocket(environment.plugin_manager)
            upgrade_socket.import_schema()
            
            if not os.path.exists(db_restore_root_abs):
                self.set_status_error('Error restoring database', "The restore folder '%s' was not found." %(db_restore_root_abs))
                return
            
            if self.restore_create_schema:
                components.database.server_common.schema_api.SchemaFactory.restore(db_restore_root_abs, environment.get_default_database(), schema_update=True, callback=self, checkpoint_handler=restore_log)
            else:
                components.database.server_common.schema_api.SchemaFactory.restore_data(db_restore_root_abs, environment.get_default_database(), callback=self, checkpoint_handler=restore_log)
            self.set_status_done('Restore', 'done')

    def cb_db_restore_begin(self, steps):
        self.set_status('Restore database', '', 0)
        self.restore_steps = steps

    def cb_db_restore_step(self, creator_name):
        if self.restore_steps > 0:
            complete = int((self.restore_step_current * 100) / self.restore_steps) 
        else:
            complete = 100
        self.set_status('Restore database', creator_name, complete)
        self.restore_step_current += 1

    def cb_db_restore_end(self):
        self.set_status('Restore database', '', 100)

    def do_run_config_restore(self, restore_log):
        config_restore_root_abs = os.path.join(self.restore_path, lib.appl.upgrade.CONFIG_FOLDER)
        self.server_configuration.restore(restore_log, config_restore_root_abs, self, is_upgrade=self.is_upgrade)

    def do_run_plugin_restore(self, restore_log):
        plugin_restore_root_abs = os.path.join(self.restore_path, lib.appl.upgrade.PLUGIN_FOLDER)
        self.config_specification_socket.restore(restore_log, plugin_restore_root_abs, self)

    def cb_restore_error(self, message):
        self._error_found = True

    def cb_restore_warning(self, message):
        self._warning_found = True


class JobGenerateSupportPackage(lib.ws.job.Job):
    """
    This class holds the functionality for generate a support-package which is a package of all relevant files 
    """
    def __init__(self, server_configuration, checkpoint_handler, config_specification_socket, management_features_enabled=True):
        lib.ws.job.Job.__init__(self, checkpoint_handler, MODULE_ID, 'job_generate_support_package')
        self.server_configuration = server_configuration
        self.config_specification_socket = config_specification_socket
        self.management_features_enabled = management_features_enabled
        
    def do_run(self):
        with self.checkpoint_handler.CheckpointScope("generate_support_package", MODULE_ID, lib.checkpoint.DEBUG):
            support_package_root = self.server_configuration.generate_support_package_begin(self.checkpoint_handler, management_features_enabled=self.management_features_enabled)
            lib.appl.upgrade.save_version(support_package_root)
            self.config_specification_socket.generate_support_package(support_package_root)
            self.server_configuration.generate_support_package_end(support_package_root)
        self.set_status_done(done_arg_01=support_package_root)

    def do_cancel(self):
        pass


class JobPruneData(lib.ws.job.Job):
    """
    This class holds the functionality for generating demodata.
    """
    def __init__(self, server_configuration, date_before, checkpoint_handler):
        lib.ws.job.Job.__init__(self, checkpoint_handler, MODULE_ID, 'job_prune_data')
        self.server_configuration = server_configuration
        self.date_before = date_before
        self.is_canceling = False
        self.current_pct = 0
        self.current_job_pct = 0
        
    def do_run(self):
        with self.checkpoint_handler.CheckpointScope("prune_data", MODULE_ID, lib.checkpoint.DEBUG):
            self.set_status('Prune data', 'Prune access log', 0)
            db_connect_string = self.server_configuration.configuration_server_config.get_db_connect_url()
            db_encoding = self.server_configuration.configuration_server_config.db_encoding
            db_log_enabled = self.server_configuration.configuration_server_config.db_log_enabled

            environment = components.environment.Environment(self.checkpoint_handler, db_connect_string, MODULE_ID, db_encoding, db_logging=db_log_enabled)

            # Ensure that plugins are loaded in order to registre databases
            # this also load our own 
            environment.plugin_manager = components.plugin.server_config.manager.Manager(self.checkpoint_handler, self.server_configuration.configuration_server_config.plugin_modules_abspath, self.server_configuration)
            upgrade_socket = components.plugin.server_config.plugin_socket_upgrade.PluginSocket(environment.plugin_manager)
            upgrade_socket.import_schema()
            
            
            with database_api.Transaction() as transaction:
                self.current_job_pct = 70
                self.current_pct = 5
                components.access_log.server_common.database_schema.prune_data(transaction, self.date_before, self.checkpoint_handler, self)
                self.set_status('Prune data', 'Prune endpoint access log', 75)
                self.current_job_pct = 25
                self.current_pct = 75
                components.endpoint.server_common.database_schema.prune_data(transaction, self.date_before, self.checkpoint_handler, self)
        
        if not self.is_canceling:
            self.set_status_done(sub_header="Done")
            
    def update_prune_status(self, sub_header, percentage):
        pct = self.current_pct + (percentage*self.current_job_pct / 100)
        self.set_status('Prune data', sub_header, pct)

    def do_cancel(self):
        self.is_canceling = True



class JobBackupOtherInstallation(lib.ws.job.Job):
    """
    This class holds the functionality creating backup of a different installation using current functionality (schema, and settings.
    This replaces functionality that was build into the upgrade functionality.
    """
    def __init__(self, checkpoint_handler, server_configuration_all, other_installation_path, config_specification_socket, plugin_manager, backup_version):
        lib.ws.job.Job.__init__(self, checkpoint_handler, MODULE_ID, 'job_backup_other_installation')
        self.checkpoint_handler = checkpoint_handler
        self.server_configuration_all = server_configuration_all
        self.other_installation_path = other_installation_path
        self.config_specification_socket = config_specification_socket
        self.plugin_manager = plugin_manager 
        self.backup_version = backup_version
        
        self._cancled = False
        self._error_found = False
        
    def do_run(self):
        with self.checkpoint_handler.CheckpointScope("job_backup_other_installation.do_run", MODULE_ID, lib.checkpoint.DEBUG):
            if not self._cancled and not self._error_found:
                self.set_status('Backup Other Installation, generating backup', '', 5)
                self._do_prepare_backup_from_installation_using_current_tool()

            if self._cancled:
                self.set_status_canceled()
                return

            if self._error_found:
                return

            self.set_status_done()
            return

    def _do_prepare_backup_from_installation_using_current_tool(self):
        old_configuration_server_config = components.config.common.ConfigConfigService()
        old_config_service_root = os.path.join(self.other_installation_path, old_configuration_server_config.installation_config_service_path)
        old_configuration_server_config.read_config_file_from_folder(old_config_service_root) 
        old_server_configuration_all = components.server_config_ws.server_config.ServerConfigurationAll(old_configuration_server_config)
        old_server_configuration_all.installation_path_abs = self.other_installation_path
        old_server_configuration_all.reload()

        backup_folder = os.path.join(self.server_configuration_all.get_config_service_path_abs(), self.server_configuration_all.configuration_server_config.backup_path) 

        # Get version 
        if self.backup_version is None:
            self.backup_version = get_backup_version(self.checkpoint_handler, self.other_installation_path)
        if self.backup_version is None:
            self.set_status_error("Backup Other Installation", "Unable to determine version")
            return
        
        # Need to set salt before doing backup
        old_salt =  lib.utility.xor_crypt.xor_salt
        old_configuration_server_config.load_and_set_scramble_salt(root=self.other_installation_path, set_if_not_found=False)

        # Change cwd to old system, for db_connect_string to work 
        current_cwd = os.getcwd()
        old_cwd = old_server_configuration_all.get_config_service_path_abs()
        os.chdir(old_cwd)
        
        config_specification_socket = components.plugin.server_config.plugin_socket_config_specification.PluginSocket(self.plugin_manager)
        job_backup = JobBackup(self.checkpoint_handler, self.server_configuration_all, self.config_specification_socket, backup_folder, create_subfolder_for_backup=True, alternative_server_configuration=old_server_configuration_all, reflect_database=True, backup_version=self.backup_version )
        job_backup.set_status_change_callback(self._cb_subjob_status_change)
        job_backup.connect_to_parent_job(self, 1, 100)
        job_backup.run()
        
        # restore cwd
        os.chdir(current_cwd)
        
        # restore salt
        lib.utility.xor_crypt.xor_salt = old_salt

    
    def _cb_subjob_status_change(self, subjob_status):
        if subjob_status.status == lib.ws.job.JobStatus.STATUS_DONE_error:
            self._error_found = True

    def upgrade_error(self, message):
        self._error_found = True
        self.set_status_error('Backup Other Installation', message)

    def do_cancel(self):
        self._cancled = True
