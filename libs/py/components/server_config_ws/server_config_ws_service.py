"""
This file contains the implementation of the web-services offered by the
ServerConfigWS web service interface.
"""
import lib.ws.zsi_hack
import lib.ws.ssl_hack
import lib.ws.zsi_extensions

import traceback

import os.path
import sys
import threading
import datetime
import httplib

from components.server_config_ws import MODULE_ID
import server_config_ws_session
import ws.server_config_ws_services_server
import lib.checkpoint

from components.rest.rest_api import RestApi
from components.rest.rest_service import RestService


class ServerConfigWSService(threading.Thread, ws.server_config_ws_services_server.server_config_wsService):
    """
    All server config web-service calls is dispatched by this class,
    it handles logging and propper error reporting, but the real
    work is dispatched to the session.

    Notice that each time the soap_login method is called the janitor
    that clean-up dead sessions is activated.
    """
    def __init__(self, checkpoint_handler, server_configuration, server_config_ws_ip_host, server_config_ws_ip_port):
        threading.Thread.__init__(self)
        ws.server_config_ws_services_server.server_config_wsService.__init__(self)
        self.checkpoint_handler = checkpoint_handler
        self._sessions = {}
        self._session_id_next = 0
        self._server_configuration = server_configuration
        self._server_config_ws_ip_host = server_config_ws_ip_host
        self._server_config_ws_ip_port = server_config_ws_ip_port
        self._session_timeout = datetime.timedelta(days=1, hours=0, minutes=0, seconds=0, microseconds=0)
        self._sc = None
        self._running = False

    @property
    def get_server_config_ws_ip_port(self):
        return self._server_config_ws_ip_port

    @property
    def get_server_config_ws_ip_host(self):
        return self._server_config_ws_ip_host

    @property
    def get_session_timeout(self):
        return self._session_timeout

    def get_session(self, session_id):
        try:
            return self._sessions[session_id]
        except KeyError:
            self.handle_unknown_session_exception()

    def run(self):
        try:
            self._running = True
            self.checkpoint_handler.Checkpoint("run", MODULE_ID, lib.checkpoint.DEBUG, host = self._server_config_ws_ip_host, port = self._server_config_ws_ip_port)
            services=(self,)
            address = (self._server_config_ws_ip_host, self._server_config_ws_ip_port)

            # TODO (vazarovic): Implement RestService as daemon (self._sc)
            RestService.start_server_config_wsgi(self)

            # self.server_config_thread = threading.Thread(name="ServerConfigWSGI",
            #                                                 target=RestService.start_server_config_wsgi,
            #                                                 args=(self,))
            # self.server_config_thread.setDaemon(True)
            # self.server_config_thread.start()

            # if self._server_configuration.configuration_server_config.service_https_enabled:
            #     self._sc = lib.ws.zsi_extensions.ServiceContainerHTTPS.create(self.checkpoint_handler, MODULE_ID,
            #    address, services, self._server_configuration.configuration_server_config.service_https_certificate_filename_abspath, self._server_configuration.configuration_server_config.service_https_key_filename_abspath)
            # else:
            #     self._sc = lib.ws.zsi_extensions.ServiceContainerHTTP.create(self.checkpoint_handler, MODULE_ID,
            #    address, services)

            self._sc.serve_forever()

            self.checkpoint_handler.Checkpoint("run.terminate", MODULE_ID, lib.checkpoint.DEBUG)
            self._sc.server_close()
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("run.error", MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)
        self._running = False

    def stop(self):
        if self._running:
            self._sc.shutdown()

    def rest_login(self, request):
        self.janitor_remove_dead_sessions()
        session_id = u"%i" % self._session_id_next
        self._session_id_next += 1
        with self.checkpoint_handler.CheckpointScope("rest_login", MODULE_ID, lib.checkpoint.DEBUG, session_id = "%r" % session_id):
            try:
                username = request.get_username
                password = request.get_password
                session = server_config_ws_session.ServerConfigWSSession(self.checkpoint_handler, self._server_configuration, session_id, username, password)
                self._sessions[session_id] = session
                response = RestApi.ServerConfig.LoginResponse()
                response.set_session_id(session_id)
                return response
            except:
                self.handle_unexpected_exception()

    def rest_logout(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_logout", MODULE_ID, lib.checkpoint.DEBUG, session_id="%r" % session_id):
            try:
                del self._sessions[session_id]
                response = RestApi.ServerConfig.LogoutResponse()
                response.set_rc(True)
                return response
            except KeyError:
                response = RestApi.ServerConfig.LogoutResponse()
                response.set_rc(False)
                return response
            except:
                self.handle_unexpected_exception()

    def rest_get_job_info(self, request):
        session_id = request.get_session_id
        try:
            return self.get_session(session_id).GetJobInfo(request)
        except:
            self.handle_unexpected_exception()

    def rest_cancel_job(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_cancel_job", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).CancelJob(request)
            except:
                self.handle_unexpected_exception()

    def rest_get_status(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_get_status", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).GetStatus(request)
            except:
                self.handle_unexpected_exception()

    def rest_start_job_generate_knownsecrets(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_start_job_generate_knownsecrets", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).StartJobGenerateKnownsecrets()
            except:
                self.handle_unexpected_exception()

    def rest_start_job_generate_gpms(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_start_job_generate_gpms", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).StartJobGenerateGPMS()
            except:
                self.handle_unexpected_exception()

    def rest_start_job_generate_demodata(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_start_job_generate_demodata", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).StartJobGenerateDemodata()
            except:
                self.handle_unexpected_exception()

    def rest_start_job_install_services(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_start_job_install_services", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).StartJobInstallServices()
            except:
                self.handle_unexpected_exception()

    def rest_start_job_generate_support_package(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_start_job_generate_support_package", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).StartJobGenerateSupportPackage()
            except:
                self.handle_unexpected_exception()

    def rest_start_job_prepare_installation(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_start_job_prepare_installation", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).StartJobPrepareInstallation()
            except:
                self.handle_unexpected_exception()

    def rest_start_job_finalize_installation(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_start_job_finalize_installation", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).StartJobFinalizeInstallation()
            except:
                self.handle_unexpected_exception()

    def rest_start_job_prepare_change(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_start_job_prepare_change", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).StartJobPrepareChange()
            except:
                self.handle_unexpected_exception()

    def rest_start_job_finalize_change(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_start_job_finalize_change", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).StartJobFinalizeChange()
            except:
                self.handle_unexpected_exception()

    def rest_start_job_prepare_upgrade(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_start_job_prepare_upgrade", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).StartJobPrepareUpgrade(request)
            except:
                self.handle_unexpected_exception()

    def rest_start_job_finalize_upgrade(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_start_job_finalize_upgrade", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).StartJobFinalizeUpgrade()
            except:
                self.handle_unexpected_exception()

    def rest_get_first_config_specification(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_get_first_config_specification", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).GetFirstConfigSpecification()
            except:
                self.handle_unexpected_exception()

    def rest_get_next_config_specification(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_get_next_config_specification", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).GetNextConfigSpecification()
            except:
                self.handle_unexpected_exception()

    def rest_save_config_specification(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_save_config_specification", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).SaveConfigSpecification(request)
            except:
                self.handle_unexpected_exception()

    def rest_reload_config_specification(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_reload_config_specification", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).ReloadConfigSpecification(request)
            except:
                self.handle_unexpected_exception()

    def rest_get_config_template(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_get_config_template", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).GetConfigTemplate(request)
            except:
                self.handle_unexpected_exception()

    def rest_save_config_template(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_save_config_template", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).SaveConfigTemplate(request)
            except:
                self.handle_unexpected_exception()

    def rest_delete_config_template(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_delete_config_template", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).DeleteConfigTemplate(request)
            except:
                self.handle_unexpected_exception()


    def rest_test_config_specification(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_test_config_specification", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).TestConfigSpecification(request)
            except:
                self.handle_unexpected_exception()

    def rest_get_services(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_get_services", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).GetServices()
            except:
                self.handle_unexpected_exception()

    def rest_get_g_on_systems(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_get_g_on_systems", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).GetGOnSystems()
            except:
                self.handle_unexpected_exception()

    def rest_ping(self, request):
        session_id = request.get_session_id
        rc = True
        if session_id and session_id not in self._sessions:
            rc = False
        response = RestApi.Admin.PingResponse()
        response.set_rc(rc)
        return response

    def rest_get_license_info(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_get_license_info", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).GetLicenseInfo()
            except:
                self.handle_unexpected_exception()

    def rest_set_license(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_set_license", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).SetLicense(request)
            except:
                self.handle_unexpected_exception()

    def rest_g_on_management_service_get_status(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_g_on_management_service_get_status", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).GOnManagementServiceGetStatus(request)
            except:
                self.handle_unexpected_exception()

    def rest_g_on_management_service_control(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_g_on_management_service_control", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).GOnManagementServiceControl(request)
            except:
                self.handle_unexpected_exception()


    def handle_unknown_session_exception(self):
        (etype, evalue, etrace) = sys.exc_info()
        self.checkpoint_handler.CheckpointException("handle_unknown_session_exception", MODULE_ID, lib.checkpoint.WARNING, etype, evalue, etrace)
        error_message = 'Server sesssion lost or expired'
        fault_response = RestApi.Admin.FaultServerElement()
        fault_response.set_error_message(error_message)
        raise fault_response

    def handle_unexpected_exception(self):
        (etype, evalue, etrace) = sys.exc_info()
        self.checkpoint_handler.CheckpointException("handle_unexpected_exception", MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)
        error_message = ''.join(traceback.format_exception_only(etype, evalue))
        fault_response = RestApi.Admin.FaultServerElement()
        fault_response.set_error_message(error_message)
        raise fault_response

    def janitor_remove_dead_sessions(self):
        for session in self._sessions.values():
            if(session.is_dead(self._session_timeout)):
                self.checkpoint_handler.Checkpoint("janitor_remove_dead_session", MODULE_ID, lib.checkpoint.DEBUG, session_id = "%r" % session.session_id)
                del self._sessions[session.session_id]

    def get_url(self):
        return "https://%s:%d" % (self._server_config_ws_ip_host, self._server_config_ws_ip_port)

    @classmethod
    def run_default(cls, checkpoint_handler, server_configuration, ip_host, ip_port):
        service = ServerConfigWSService(checkpoint_handler, server_configuration, ip_host, ip_port)
        service.start()
        return service




