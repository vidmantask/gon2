"""
Unittest of Server Config web-service for installation
"""
from __future__ import with_statement
import unittest
from lib import giri_unittest

import os.path
import time

import components.server_config_ws.ws as ws
import components.server_config_ws.server_config_ws_service

import components.config.common
import server_config


class ServerConfigWSInstallation(unittest.TestCase):

    def setUp(self):
        self.gon_installation = giri_unittest.UnittestDevEnvInstallation.create()
        self.gon_installation.generate_gpms()
        self.gon_installation.generate_setupdata()
        self.gon_installation.restore_plugins()
        self.server_configuration_all = self.gon_installation.server_configuration_all
        self.server_config_ip = self.server_configuration_all.configuration_server_config.service_ip
        self.server_config_port = self.server_configuration_all.configuration_server_config.service_port
        self.server_configuration_all.configuration_server_config.services_enabled = False
        self.server_configuration_all.configuration_server_gateway.service_http_enabled = True

        os.chdir(self.gon_installation.server_configuration_all.get_server_management_path_abs())
        self.server_config_service = components.server_config_ws.server_config_ws_service.ServerConfigWSService.run_default(giri_unittest.get_checkpoint_handler(), self.server_configuration_all, self.server_config_ip, self.server_config_port)

    def tearDown(self):
        time.sleep(2)
        self.server_config_service.stop()
        time.sleep(2)

    def _login(self, server_config_service):
        request = ws.server_config_ws_services.LoginRequest()
        request_content = ws.server_config_ws_services.ns0.LoginRequestType_Def(None).pyclass()
        request_content.set_element_username("ib")
        request_content.set_element_password("FidoTheDog")
        request.set_element_content(request_content)
        response = server_config_service.Login(request)
        return response.get_element_content().get_element_session_id()

    def _logout(self, server_config_service, session_id):
        request = ws.server_config_ws_services.LogoutRequest()
        request_content = ws.server_config_ws_services.ns0.LogoutRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.Logout(request)
        rc = response.get_element_content().get_element_rc()
        self.assertTrue(rc)

    def _wait_for_job(self, server_config_service, session_id, job_id):
        job_more = True
        while(job_more):
            request = ws.server_config_ws_services.GetJobInfoRequest()
            request_content = ws.server_config_ws_services.ns0.GetJobInfoRequestType_Def(None).pyclass()
            request_content.set_element_session_id(session_id)
            request_content.set_element_job_id(job_id)
            request.set_element_content(request_content)
            response = server_config_service.GetJobInfo(request)
            job_info = response.get_element_content().get_element_job_info()

            job_more = job_info.get_element_job_more()
            job_status = job_info.get_attribute_job_status()
            job_header = job_info.get_element_job_header()
            job_sub_header = job_info.get_element_job_sub_header()
            job_progress = job_info.get_element_job_progress()
            print "Job Action", job_id, job_more, job_status, job_header, job_sub_header, job_progress
            time.sleep(1)
        print "Job Done"
        return job_status

    def _get_config_specifications(self, server_config_service, session_id):
        specifications = []
        request = ws.server_config_ws_services.GetFirstConfigSpecificationRequest()
        request_content = ws.server_config_ws_services.ns0.GetFirstConfigSpecificationRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.GetFirstConfigSpecification(request)
        specification = response.get_element_content().get_element_config_specification()

        while specification:
            request = ws.server_config_ws_services.SaveConfigSpecificationRequest()
            request_content = ws.server_config_ws_services.ns0.SaveConfigSpecificationRequestType_Def(None).pyclass()
            request_content.set_element_session_id(session_id)
            request_content.set_element_config_specification(specification)
            request.set_element_content(request_content)
            response = server_config_service.SaveConfigSpecification(request)

            request = ws.server_config_ws_services.GetNextConfigSpecificationRequest()
            request_content = ws.server_config_ws_services.ns0.GetNextConfigSpecificationRequestType_Def(None).pyclass()
            request_content.set_element_session_id(session_id)
            request.set_element_content(request_content)
            try:
                response = server_config_service.GetNextConfigSpecification(request)
                specification = response.get_element_content().get_element_config_specification()
            except:
                specification  = None


    def test_install(self):
        time.sleep(1)
        server_config_service = ws.server_config_ws_services.BindingSOAP("http://%s:%d" % (self.server_config_ip, self.server_config_port))
        session_id = self._login(server_config_service)

        request = ws.server_config_ws_services.StartJobPrepareInstallationRequest()
        request_content = ws.server_config_ws_services.ns0.StartJobPrepareInstallationRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.StartJobPrepareInstallation(request)

        job_id = response.get_element_content().get_element_job_id()
        job_status = self._wait_for_job(server_config_service, session_id, job_id)
        print job_status
        self.assertEqual(job_status, 'done_ok_job_status')

        self._get_config_specifications(server_config_service, session_id)
        print job_status
        self.assertEqual(job_status, 'done_ok_job_status')

        request = ws.server_config_ws_services.StartJobFinalizeInstallationRequest()
        request_content = ws.server_config_ws_services.ns0.StartJobFinalizeInstallationRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.StartJobFinalizeInstallation(request)

        job_id = response.get_element_content().get_element_job_id()
        job_status = self._wait_for_job(server_config_service, session_id, job_id)
        print job_status
        self.assertEqual(job_status, 'done_ok_job_status')


    def test_change(self):
        time.sleep(1)
        server_config_service = ws.server_config_ws_services.BindingSOAP("http://%s:%d" % (self.server_config_ip, self.server_config_port))
        session_id = self._login(server_config_service)

        request = ws.server_config_ws_services.StartJobPrepareChangeRequest()
        request_content = ws.server_config_ws_services.ns0.StartJobPrepareChangeRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.StartJobPrepareChange(request)

        job_id = response.get_element_content().get_element_job_id()
        job_status = self._wait_for_job(server_config_service, session_id, job_id)
        print job_status
        self.assertEqual(job_status, 'done_ok_job_status')


        self._get_config_specifications(server_config_service, session_id)
        print job_status
        self.assertEqual(job_status, 'done_ok_job_status')


        request = ws.server_config_ws_services.StartJobFinalizeChangeRequest()
        request_content = ws.server_config_ws_services.ns0.StartJobFinalizeChangeRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = server_config_service.StartJobFinalizeChange(request)

        job_id = response.get_element_content().get_element_job_id()
        job_status = self._wait_for_job(server_config_service, session_id, job_id)
        print job_status
        self.assertEqual(job_status, 'done_ok_job_status')

#
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'
GIRI_UNITTEST_TIMEOUT = 60 * 6

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
