"""
This module contains the jobs for installation
"""
from __future__ import with_statement

import datetime
import os.path
import sys

import lib.ws.job
import lib.checkpoint
import server_config_jobs
import database_schema

import components.environment


class JobInstallationSession(object):
    def __init__(self, checkpoint_handler, server_configuration):
        self.server_configuration = server_configuration
        self.checkpoint_handler = checkpoint_handler
        logger_foldername_abs = os.path.join(self.server_configuration.get_config_service_path_abs(), server_config_jobs.EVENTS_FOLDER)
        (self.logger_filename_abs, self.logger) = server_config_jobs.create_logger(self.checkpoint_handler, 'installation', logger_foldername_abs)


class JobPrepareInstallation(lib.ws.job.Job):
    """
    This class holds the functionality for preparing a installation.
    """
    def __init__(self, installation_session):
        lib.ws.job.Job.__init__(self, installation_session.checkpoint_handler, server_config_jobs.MODULE_ID, 'job_prepare_installation')
        self.installation_session = installation_session
        self.server_configuration = installation_session.server_configuration
        self.checkpoint_handler = installation_session.checkpoint_handler
        self.logger = installation_session.logger

    def do_run(self):
        with self.logger.CheckpointScope("prepare_installation", server_config_jobs.MODULE_ID, lib.checkpoint.DEBUG):
            self.server_configuration.start_installation()
            self.set_status_done()
        
    def do_cancel(self):
        pass


class JobFinalizeInstallation(lib.ws.job.Job):
    """
    This class holds the functionality for finialization of the installation.
    """
    def __init__(self, installation_session, config_specifications):
        lib.ws.job.Job.__init__(self, installation_session.checkpoint_handler, server_config_jobs.MODULE_ID, 'job_finalize_installation')
        self.installation_session = installation_session
        self.config_specifications = config_specifications
        self.server_configuration = installation_session.server_configuration
        self.logger = installation_session.logger
        
        self._cancled = False
        self._error_found = False

    def do_run(self):
        with self.logger.CheckpointScope("finialize_installation", server_config_jobs.MODULE_ID, lib.checkpoint.DEBUG):

            if not self._cancled and not self._error_found: 
                self.set_status('Installing, generating known secrets', '', 5)
                job_generate_ks = server_config_jobs.JobGenerateKnownsecrets(self.server_configuration, self.logger)
                job_generate_ks.set_status_change_callback(self._cb_subjob_status_change)
                job_generate_ks.connect_to_parent_job(self, 5, 10)
                job_generate_ks.run()

            if not self._cancled and not self._error_found: 
                self.set_status('Installing, generating system configuration', '', 10)
                self.server_configuration.finialize_installation(self.logger)

            if not self._cancled and not self._error_found: 
                self.set_status('Installing, generating plugin configuration', '', 15)
                try:
                    for config_specification in self.config_specifications:
                        config_specification.finalize()
                except Exception, e:
                    (etype, evalue, etrace) = sys.exc_info()
                    self.logger.CheckpointException("JobFinalizeChange", server_config_jobs.MODULE_ID, lib.checkpoint.ERROR, etype, evalue, etrace)
                    self._error_found = e

            if not self._cancled and not self._error_found: 
                self.set_status('Installing, generating setup data', '', 20)
                job_generate_demodata = server_config_jobs.JobGenerateDemodata(self.server_configuration, self.logger)
                job_generate_demodata.set_status_change_callback(self._cb_subjob_status_change)
                job_generate_demodata.connect_to_parent_job(self, 20, 30)
                job_generate_demodata.run()

            if not self._cancled and not self._error_found: 
                self.set_status('Installing, generating packages', '', 30)
                job_generate_gpms = server_config_jobs.JobGenerateGPMS(self.server_configuration, self.logger)
                job_generate_gpms.set_status_change_callback(self._cb_subjob_status_change)
                job_generate_gpms.connect_to_parent_job(self, 30, 90)
                job_generate_gpms.run()

            if not self._cancled and not self._error_found: 
                self.set_status('Installing, initializing database', '', 90)
                environment = components.environment.Environment(self.logger, self.server_configuration.configuration_server_config.get_db_connect_url(), server_config_jobs.MODULE_ID, 
                                                                 db_encoding=self.server_configuration.configuration_server_config.db_encoding,
                                                                 db_logging=self.server_configuration.configuration_server_config.db_log_enabled)
                current_version = database_schema.set_db_current_version()
                
                
                

            if not self._cancled and not self._error_found: 
                self.set_status('Installing,  installing services', '', 95)
                job_install_services = server_config_jobs.JobInstallServices(self.server_configuration, self.logger)
                job_install_services.set_status_change_callback(self._cb_subjob_status_change)
                job_install_services.connect_to_parent_job(self, 95, 100)
                job_install_services.run()

            if self._cancled:
                self.set_status_canceled()
                return

            if self._error_found:
                self.set_status_error('Installation', "Error found during installation, see logfile '%s'" %  self.installation_session.logger_filename_abs)
                return

            self.set_status_done()
            return

    def _cb_subjob_status_change(self, subjob_status):
        if subjob_status.status == lib.ws.job.JobStatus.STATUS_DONE_error:
            self._error_found = True

    def do_cancel(self):
        self.set_status_cancel()
        self._cancled = True
