"""
Unittest of Server Config web-service for backup
"""
from __future__ import with_statement
import unittest
from lib import giri_unittest

import os.path
import time
import tempfile
import shutil

import components.plugin.server_config.manager
import components.plugin.server_config.plugin_socket_config_specification
import components.server_config_ws.server_config_jobs


class ServerConfigWSTest(unittest.TestCase):

    def setUp(self):
        self.gon_installation = giri_unittest.UnittestDevEnvInstallation.create()
        self.server_configuration_all = self.gon_installation.server_configuration_all
        
    def tearDown(self):
        pass

    def test_backup(self):
        server_config = self.server_configuration_all.configuration_server_config
        plugin_manager = components.plugin.server_config.manager.Manager(giri_unittest.get_checkpoint_handler(), server_config.plugin_modules_abspath, self.server_configuration_all)
        config_specification_socket = components.plugin.server_config.plugin_socket_config_specification.PluginSocket(plugin_manager)

        backup_path = giri_unittest.mkdtemp()
        job = components.server_config_ws.server_config_jobs.JobBackup(giri_unittest.get_checkpoint_handler(), self.server_configuration_all, config_specification_socket, backup_path)
        job.start()
        job.join()

        
        
        return 0

#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
