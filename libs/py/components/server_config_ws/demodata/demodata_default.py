"""
This module contains functionality for generating setup data neede in a new installation
"""
from __future__ import with_statement

import components.auth.server_management.rule_api as rule_api

import components.dialog.server_common.database_schema as dialog_schema
import components.traffic.server_common.database_schema as traffic_schema
import components.auth.server_common.auth_graph_model as auth_graph_model 
import components.server_config_ws.database_schema as version_schema

import datetime

from components.database.server_common.database_api import Transaction, and_, in_
from components.database.server_common.schema_api import SchemaFactory


from random import randint

from components.dialog.server_management.dialog_api import save_tag_values, create_dialog_element
from components.auth.server_common.auth_graph_model import RuleCriteria



class Temp(object):
    pass


def generate_demodata_auth(environment):
    with Transaction() as t:
        rule_api.create_build_in_elements(environment, t)


def addlaunchitemtags(t, launch_id, user_id, tag):
    dbrec = t.select(dialog_schema.DialogLaunchTags, 
                      dialog_schema.database_api.and_(
                      dialog_schema.DialogLaunchTags.launch_id==launch_id,
                      dialog_schema.DialogLaunchTags.user_id==user_id,
                      dialog_schema.DialogLaunchTags.tag_name==tag))
    new = False
    if len(dbrec) == 0:
        rec = dialog_schema.DialogLaunchTags()
    else:
        rec = dbrec[0]
    
        
    #basic
    rec.launch_id = launch_id
    rec.user_id = user_id
    rec.tag_name = tag
    if len(dbrec)==0:
        t.add(rec)




def setlaunchitemstats(t, launch_id, launch_key, count, date=None):

    dbrec = t.select(dialog_schema.DialogStatistics, 
                      dialog_schema.database_api.and_(
                      dialog_schema.DialogStatistics.launch_id==launch_id,
                      dialog_schema.DialogStatistics.launch_key==launch_key))
    
    new = False
    if len(dbrec) == 0:
        rec = dialog_schema.DialogStatistics()
    else:
        rec = dbrec[0]
    
        
    #basic
    rec.launch_id = launch_id
    rec.launch_key = launch_key
    rec.launch_count = count
    if date == None:
        rec.last_date = datetime.datetime.now()
    else:
        rec.last_date = date
        
    if len(dbrec)==0:
        t.add(rec)

    
    
def generate_demodata_dialog():
    
    # The item_show and item enabled should only be there as long as client_ok::IfPlatformIs("win32") doesn't
    # the right tags.
    with Transaction() as t:
        save_tag_values(t, 'ALL', -1, menu_show=True, menu_caption='All Programs', menu_sortitems='sort_title', auto_menu_all=True) #, item_show = True, item_enabled = True)
        save_tag_values(t, 'TOPX', -1, menu_show=True, menu_caption='Top 10', menu_sortitems='launch_count', menu_maxitems=10, auto_menu_all=True ) #, item_show = True, item_enabled = True)
        save_tag_values(t, 'WINDOWS', -1)
        save_tag_values(t, 'LINUX', -1)
        save_tag_values(t, 'MAC', -1)
        save_tag_values(t, 'BROWSER', -1, menu_show=True,menu_caption='Browser apps', menu_sortitems='sort_title' )
        save_tag_values(t, 'RDP', -1, menu_show=True, menu_caption='Remote Desktops', menu_sortitems='sort_title')
        save_tag_values(t, 'VNC', -1, menu_show=True, menu_caption='VNC - Shared desktops', menu_sortitems='sort_title')
        save_tag_values(t, 'GUPDATE', -1, menu_show=True, menu_caption='G/Update', menu_sortitems='sort_title')
        save_tag_values(t, 'HELP', -1, menu_show=True, menu_caption='Help', menu_sortitems='sort_title')
        save_tag_values(t, 'ITMANAGEMENT', -1, menu_show=True, menu_caption='IT Management', menu_sortitems='sort_title')
        save_tag_values(t, 'FILES', -1, menu_show=True, menu_caption='File Shares', menu_sortitems='sort_title')
        save_tag_values(t, 'MAIL', -1, menu_show=True, menu_caption='Mail', menu_sortitems='sort_title')
        save_tag_values(t, 'CITRIX', -1, menu_show=True, menu_caption='Citrix', menu_sortitems='sort_title')
        save_tag_values(t, 'SHOW', -1, item_show = True)
        save_tag_values(t, 'ENABLED', -1, item_enabled = True)
        save_tag_values(t, 'AUTOLAUNCH', -1, item_autolaunch_once = True)
        save_tag_values(t, 'AUTOLAUNCH_FIRST_START', -1, item_autolaunch_first_start = True)


def generate_demodata_version():
    version_schema.get_db_version()

def add_launch_entry(title, launch_type=0, command='', working_directory='', 
                     server_host='', server_port=0, client_host='', client_port=0, 
                     lock_to_process_pid=False, sub_processes=False, lock_to_process_name='', close_with_process=False, kill_on_close=False,
                     citrix_https=False, citrix_metaframe_path='', citrix_command='', 
                     param_file_name='', param_file_lifetime=5, param_file_template='', 
                     sso_login='', sso_password='', sso_domain='', 
                     tags = set(), tag_generators = set(), createRandomStat=True):

    auth_action = rule_api.create_action_criteria(unicode(title))
    with Transaction() as t:
        launch_spec = t.add(traffic_schema.LaunchElement())
        launch_spec.action_id = auth_action.get_id()
        launch_spec.launch_type = launch_type
        launch_spec.command = command
        launch_spec.working_directory = working_directory
        launch_spec.close_with_process = close_with_process
        launch_spec.kill_on_close = kill_on_close
        launch_spec.citrix_https = citrix_https
        launch_spec.citrix_metaframe_path = citrix_metaframe_path
        launch_spec.citrix_command = citrix_command
        launch_spec.param_file_name = param_file_name
        launch_spec.param_file_lifetime = param_file_lifetime
        launch_spec.param_file_template = param_file_template
        launch_spec.sso_login = sso_login
        launch_spec.sso_password = sso_password
        launch_spec.sso_domain = sso_domain
        
        launch_spec.name = title
        launch_spec.menu_title = title
        
        if server_host:
            if len(launch_spec.portforwards) == 0:
                portforward_spec = traffic_schema.Portforward()
                t.add(portforward_spec)
            elif len(launch_spec.portforwards) == 1:
                portforward_spec = launch_spec.portforwards[0]
            else:
                raise RuntimeError # we currently only support demo specs with 0 or 1 portforward
            portforward_spec.line_no = 0
            portforward_spec.server_host = server_host
            portforward_spec.server_port = server_port
            portforward_spec.client_host = client_host
            portforward_spec.client_port = client_port
            portforward_spec.lock_to_process_pid = lock_to_process_pid
            portforward_spec.sub_processes = sub_processes
            portforward_spec.lock_to_process_name = lock_to_process_name
            portforward_spec.launch_element = launch_spec
        
        t.flush()
        
        create_dialog_element(t, launch_spec.id, title)
        
        for tag in tags:
            addlaunchitemtags(t, launch_spec.id, -1, tag)

        for tag_generator in tag_generators:
            launch_spec.add_tag_generator(tag_generator)

        
        if createRandomStat:
            # add random launch statistics - click count 1-100, last use date during last 365 days
            setlaunchitemstats(t, launch_spec.id, 'na_all', randint(1,100), datetime.datetime.now() + datetime.timedelta(days=-randint(1,365)))


def generate_launch_entries():

    add_launch_entry(title="Client Package Manager, Update",
                     launch_type=2,
                     command="cpm_update",
                     tags=set(['SERVEROK', 'GUPDATE']),
                     tag_generators=set(['client_ok::IfPlatformIs("win")', 'client_ok::IfPlatformIs("mac")', 'client_ok::IfPlatformIs("linux-g-on-os")'])
                     )
 
    add_launch_entry(title="Client Package Manager, Install",
                     launch_type=2,
                     command="cpm_install",
                     tags=set(['SERVEROK', 'GUPDATE']),
                     tag_generators=set(['client_ok::IfPlatformIs("win")', 'client_ok::IfPlatformIs("mac")', 'client_ok::IfPlatformIs("linux-g-on-os")'])
                     )

    add_launch_entry(title="Client Package Manager, Remove",
                     launch_type=2,
                     command="cpm_remove",
                     tags=set(['SERVEROK', 'GUPDATE']),
                     tag_generators=set(['client_ok::IfPlatformIs("win")', 'client_ok::IfPlatformIs("mac")', 'client_ok::IfPlatformIs("linux-g-on-os")'])
                     )

    add_launch_entry(title="Field Enrollment",
                     launch_type=2,
                     command="endpoint_enrollment",
                     tags=set(['SERVEROK','AUTOLAUNCH_FIRST_START','ITMANAGEMENT']),
                     tag_generators=set(['client_ok::IfPlatformIs("win")', 'client_ok::IfPlatformIs("mac")', 'client_ok::IfPlatformIs("linux-g-on-os")'])
                     )

    add_launch_entry(title="Change Password",
                     launch_type=2,
                     command="change_password",
                     tags=set(['SERVEROK','_MENU_ROOT']),
                     tag_generators=set(['client_ok::IfPlatformIs("win")', 'client_ok::IfPlatformIs("mac")', 'client_ok::IfPlatformIs("linux-g-on-os")'])
                     )


def generate_demodata(environment, checkpoint_handler, job_cb = None):
    import lib.checkpoint
    
    progress = 0
    
    if job_cb is not None:
        job_cb.set_status('Clearing database', '', 0)
    SchemaFactory.delete_all_tables()
    
    progress += 20

    if job_cb is not None:
        job_cb.set_status('Creating launch data', '', progress)
    generate_launch_entries()
    if job_cb is not None and job_cb.is_canceling:
        return

    progress += 20

    if job_cb is not None:
        job_cb.set_status('Creating authorization data', '', progress)
    generate_demodata_auth(environment)
    if job_cb is not None and job_cb.is_canceling:
        return

    progress += 20

    if job_cb is not None:
        job_cb.set_status('Creating dialog data', '', progress)
    generate_demodata_dialog()
    if job_cb is not None and job_cb.is_canceling:
        return

    progress += 20

    if job_cb is not None:
        job_cb.set_status('Creating version data', '', progress)
    generate_demodata_version()
    if job_cb is not None and job_cb.is_canceling:
        return

def _add_sub_criteria(self, criteria, rule_entity_type=None, deactivated=False):
    a = auth_graph_model.RuleAssociation()
    
    a.parent_id = self.id
    a.parent_internal_type_name = self.internal_type_name

    a.child_id = criteria.id
    a.child_internal_type_name = criteria.internal_type_name

    a.rule_entity_type = rule_entity_type
    a.deactivated = deactivated
    
    return a


def reset_mgmt_access(checkpoint_handler):
    with Transaction() as dbt:
        access_def_created = dbt.select_first(auth_graph_model.RuleCriteria, 
                                              filter=and_(auth_graph_model.RuleCriteria.internal_type_name==RuleCriteria.TYPE_ADMIN_ACCESS,
                                                          auth_graph_model.RuleCriteria.title=="Administrator"))
        if not access_def_created:
            print "Unable to find Administrator access definition. Please start the management server and client before running this action."
            return
        
        assoc_exp = dbt.select_expression(auth_graph_model.RuleAssociation.child_id, auth_graph_model.RuleAssociation.parent_id == access_def_created.id)
        rules = dbt.select(auth_graph_model.RuleCriteria, in_(auth_graph_model.RuleCriteria.id, assoc_exp))
        assert len(rules)==1, "Internal Error: Number of rule definitions for access definition is not 1 but %d" % len(rules)
        access_def_rule = rules[0]
        rule_clause = dbt.add(auth_graph_model.RuleCriteria())
        rule_clause.internal_type_name = auth_graph_model.RuleCriteria.TYPE_RULE_CLAUSE
        rule_clause.aggregation_kind = auth_graph_model.RuleCriteria.AllOf
        rule_clause.title = access_def_created.title
        rule_clause.entity_type = access_def_rule.entity_type 
        dbt.flush()
        dbt.add(_add_sub_criteria(access_def_rule, rule_clause))
    
    
