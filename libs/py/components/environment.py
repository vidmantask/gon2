"""
Environment for holding global data common for all components,
eg. database connections and checkpoint handlers.
"""
from components.database.server_common.connection_factory import ConnectionFactory
from components.database.server_common.database_api import SchemaFactory

from lib.checkpoint import CheckPointLoggerWrapper

class Environment(object):
    def __init__(self, checkpoint_handler, db_connect_string=None, session_id='Unknown', db_encoding=None, num_of_threads=5, db_logging=False, management_message_session=None):
        import lib.checkpoint
        self.checkpoint_handler = checkpoint_handler
        self.connection_factory = ConnectionFactory(db_connect_string, db_encoding)

        self._management_message_session = management_message_session
        self._log_database_output = False
        self._database_logger = None
        self.set_log_database_output(db_logging)
        SchemaFactory.connect(self.get_default_database(num_of_threads))

    def get_default_database(self, num_of_threads=5):
        return self.connection_factory.get_default_connection(pool_size=num_of_threads*2, echo=self._log_database_output, logger=self._database_logger)
    
    def set_log_database_output(self, bool_value):
        if bool_value!=self._log_database_output:
            self._log_database_output = bool_value
            if bool_value:
                self._database_logger = CheckPointLoggerWrapper(self.checkpoint_handler, "database")
            else:
                self._database_logger = None 
    
    def get_management_message_session(self):
        return self._management_message_session