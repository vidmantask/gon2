"""
Holding functionality to handle download of gpm files
"""
import os
import os.path 

import lib.utility

import lib.gpm.gpm_builder
import lib.gpm.gpm_analyzer
import lib.gpm.gpm_env

class GPMDownloadManager(object):
    RC_OK = 0
    RC_GPM_NOT_FOUND = 1
    RC_DOWNLOAD_ID_NOT_FOUND = 2
    
    def __init__(self, gpms_root, gpmc_defs_root):
        self._gpms_root = gpms_root
        self._gpmc_defs_root = gpmc_defs_root
        self._downloads = {}
        self._downloads_next_id = 0
        self._repository = self._create_repository_instance()
    
    def _create_repository_instance(self):
        gpm_error_handler = lib.gpm.gpm_env.GpmErrorHandler()
        gpm_reporitory = lib.gpm.gpm_analyzer.GpmRepository()
        gpm_reporitory.init_using_cache(gpm_error_handler, self._gpms_root, self._gpmc_defs_root)
        return gpm_reporitory

    def _get_repository(self):
        if self._repository.has_cache_changed(self._gpms_root):
            self._repository = self._create_repository_instance()
        return self._repository
    
    def get_collection_ids(self):
        return self._get_repository().get_collection_ids()

    def get_gpm_collection_info(self, gpm_collection_id):
        return self._get_repository().get_gpm_collection_info(gpm_collection_id)
    
    def get_ids_for_collection(self, gpm_collection_id):
        return self._get_repository().get_ids_for_collection(gpm_collection_id)
    
    def get_info(self, gpm_id):
        return self._get_repository().get_info(gpm_id)

    def start_gpm_download(self, gpm_id):
        gpm_filename = os.path.abspath(os.path.join(self._gpms_root, '%s.gpm' % gpm_id))
        if not os.path.isfile(gpm_filename):
            return {'rc': GPMDownloadManager.RC_GPM_NOT_FOUND}
        
        download_id = self._downloads_next_id
        self._downloads_next_id += 1

        download_size = os.path.getsize(gpm_filename)
        download_checksum = lib.utility.calculate_checksum_for_file(gpm_filename)
        gpm_file = open(gpm_filename, 'rb')
        self._downloads[download_id] = (gpm_filename, gpm_file)
        return {'rc': GPMDownloadManager.RC_OK, 'download_id':download_id, 'download_size':download_size, 'download_checksum':download_checksum}
        
    
    def get_gpm_download_chunk(self, download_id, download_chunksize):
        if not self._downloads.has_key(download_id):
            return {'rc': GPMDownloadManager.RC_DOWNLOAD_ID_NOT_FOUND}
            
        (gpm_filename, gpm_file) = self._downloads[download_id]
        
        chunk_raw = gpm_file.read(download_chunksize)
        if len(chunk_raw) == 0:
            self.stop_gpm_download(download_id)
            return {'rc': GPMDownloadManager.RC_OK, 'more':False, 'download_chunk':''}

        return {'rc': GPMDownloadManager.RC_OK, 'more':True, 'download_chunk':chunk_raw}
        
            
    def stop_gpm_download(self, download_id):
        if self._downloads.has_key(download_id):
            (gpm_filename, gpm_file) = self._downloads[download_id]
            gpm_file.close() 
            del self._downloads[download_id]
