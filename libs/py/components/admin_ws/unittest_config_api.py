"""
Unittest of config api
"""
from __future__ import with_statement
import time
import os
import unittest

from lib import giri_unittest

import config_api
import components.auth.server_management.rule_api as rule_api
from components.plugin.server_common.config_api_lib import ConfigTemplateSpec
import components.auth.server_common.database_schema 
from components.database.server_common.connection_factory import ConnectionFactory
from components.database.server_common.database_api import ReadonlySession


class ActivityLogCreatorStub():

    def __init__(self):
        pass
        
    def save(self, **kwargs):
        pass
        
    def addValues(self, **kwargs):
        pass
    
    def copy(self):
        return self
    
    def copyUpdateAndAdd(self, dbt, **kwargs):
        pass

class PluginSocketConfigStub(object):

    connection = ConnectionFactory(None).create_connection('sqlite:///:memory:', False)
    components.auth.server_common.database_schema.connect_to_database(connection)

    def __init__(self):
        self.values = []
    
    def config_get_template(self, plugin_name, element_type, element_id, db_session):
        config_spec = ConfigTemplateSpec()
        config_spec.init(element_type, plugin_name, "")
        config_spec.add_field('id', '', ConfigTemplateSpec.VALUE_TYPE_INTEGER)
        field = config_spec.add_field('name', 'Name', ConfigTemplateSpec.VALUE_TYPE_STRING)
        if element_id:
            element = self.values[int(element_id)]
            config_spec.set_value("id", element_id)
            config_spec.set_value("name", element["name"])
        return config_spec.get_template()

    def config_create_row(self, plugin_name, element_type, template, transaction):
        config_spec = ConfigTemplateSpec(template)
        element_id = len(self.values)
        config_spec.set_value("id", element_id)
        element = dict()
        element.update(config_spec.get_field_value_dict())
        self.values.append(element)
        return "%s.%s" % (plugin_name, element_id)

    def config_update_row(self, plugin_name, element_type, template, transaction):
        config_spec = ConfigTemplateSpec(template)
        element_id = config_spec.get_value("id")
        element = self.values[element_id]
        element.update(config_spec.get_field_value_dict())

    def config_delete_row(self, plugin_name, element_type, element_id, dbt):
        element_id = int(element_id)
        self.values[element_id] = None

    def get_config_enabling(self, plugin_name, element_type):
        raise NotImplementedError()


class TemplateHandlerStub(object):
    
    def __init__(self):
        pass


class UserAdminException(Exception):
    pass

class UserAdminStub(object):
    
    def __init__(self, test_case):
        self.test_case = test_case
        self.user_values = [] 
        self.test_values = []
        
    def get_user(self, user_id):
        return self.user_values[int(user_id)]

    def get_test(self, test_id):
        return self.test_values[int(test_id)]
    
    def config_get_template(self, internal_element_type, element_id, custom_template, db_session):
        if internal_element_type == "user":
            config_spec = ConfigTemplateSpec()
            config_spec.init(internal_element_type, "User", "")
            config_spec.add_field('id', '', ConfigTemplateSpec.VALUE_TYPE_INTEGER)
            field = config_spec.add_field('name', 'Name', ConfigTemplateSpec.VALUE_TYPE_STRING)
            field.set_attribute_max_length(40)
            if element_id:
                module_name, dot, element_id = element_id.partition(".")
                user = self.user_values[int(element_id)]
                config_spec.set_value("id", element_id)
                config_spec.set_value("name", user["name"])
            return config_spec.get_template()
        elif internal_element_type == "test":
            config_spec = ConfigTemplateSpec()
            config_spec.init(internal_element_type, "User", "")
            config_spec.add_field('id', '', ConfigTemplateSpec.VALUE_TYPE_INTEGER)
            field = config_spec.add_field('test', 'Name', ConfigTemplateSpec.VALUE_TYPE_INTEGER)
            if element_id:
                module_name, dot, element_id = element_id.partition(".")
                test = self.test_values[int(element_id)]
                config_spec.set_value("id", element_id)
                config_spec.set_value("test", int(test["test"]))
            return config_spec.get_template()
            
        return None
    
    def config_update_row(self, internal_element_type, element_id, template, dbt):
        module_name, dot, element_id = element_id.partition(".")
        if internal_element_type == "user":
            element = self.get_user(element_id)
        elif internal_element_type == "test":
            element = self.get_test(element_id)
        else:
            raise UserAdminException()
        
        config_spec = ConfigTemplateSpec(template)
        element.update(config_spec.get_field_value_dict())
    
    def config_create_row(self, internal_element_type, template, dbt):
        if internal_element_type == "user":
            config_spec = ConfigTemplateSpec(template)
            user = config_spec.get_field_value_dict()
            user_id = len(self.user_values)
            user['id'] = user_id
            self.user_values.append(user)
            return "user.%s" % user_id
        elif internal_element_type == "test":
            config_spec = ConfigTemplateSpec(template)
            test = config_spec.get_field_value_dict()
            test_id = len(self.test_values)
            test['id'] = test_id
            self.test_values.append(test)
            return "test.%s" % test_id
            
        raise UserAdminException()
        
    def config_delete_row(self, internal_element_type, element_id, dbt):
        module_name, dot, element_id = element_id.partition(".")
        element_id = int(element_id)
        if internal_element_type == "user":
            self.user_values[element_id] =  None
        elif internal_element_type == "test":
            self.test_values[element_id] =  None
        else:
            raise UserAdminException()
    

class ConfigApiTest(unittest.TestCase):
    
    _test_element_types = { 

                        u"TestResult" : dict( title=u"Test", type_="rule", config_type=u"class", nfo=u"TestInfo"),
                        u"TestAction" : dict( title=u"Test1", type_=u"action", config_type=u"class",info=u"TestInfo1"),
                        u"TestResult2" : dict( title=u"Test2", type_="rule", config_type=u"class", info=u"TestInfo"),
                        
                        u"TestElement1" : dict( title=u"TestElement1", type_=u"module", config_type=u"module", 
                                                plugin=u"test_plugin1", internal_element_type = u"testelement1"),
                        u"TestElement2" : dict( title=u"TestElement2", type_=u"module",  config_type=u"module",
                                                plugin=u"test_plugin2", internal_element_type = u"testelement1"),
                        u"TestElement3" : dict( title=u"TestElement3", type_=u"module", config_type=u"module", 
                                                plugin=u"test_plugin3", internal_element_type = u"testelement3"),
                        u"TestElement4" : dict( title=u"TestElement4", type_=u"module", config_type=u"module", 
                                                plugin=u"test_plugin4", internal_element_type = u"testelement4"),

                        u"TestUser1" :    dict( title=u"TestUser1", type_=u"component", config_type=u"component", component_name=u"user",internal_element_type=u"user"),
                        u"TestUser3" :    dict( title=u"TestUser3", type_=u"component", config_type=u"component", component_name=u"user",internal_element_type=u"test"),
                        u"TestUser4" :    dict( title=u"TestUser4", type_=u"component", config_type=u"component", component_name=u"user",internal_element_type=u"notexist"),
                        
                     }
    
    rule_api.def_element_types.update(_test_element_types)
    rule_api._init_element_types()
    
    
    def test0(self):
        user_admin = UserAdminStub(self)
        template_handler = TemplateHandlerStub()
        plugin_socket_config = PluginSocketConfigStub()
        config_handler = config_api.PluginConfigAPI(plugin_socket_config, template_handler, user_admin, giri_unittest.get_checkpoint_handler(), ActivityLogCreatorStub())
        
        user_template = config_handler.get_config_element_template("TestUser1", None)
        config_spec = ConfigTemplateSpec(user_template)
        config_spec.set_value("name", "LaLa")
        
        user_id , new_template = config_handler.create_row_from_template("TestUser1", user_template)
        config_spec = ConfigTemplateSpec(new_template)
        self.assertEquals(config_spec.get_value("name"), "LaLa")
        
        config_spec.set_value("name", "Dipsy")
        config_handler.update_row_from_template("TestUser1", user_id, config_spec.get_template())
        
        dummy, dot, real_user_id = user_id.partition(".")
        self.assertEquals(user_admin.get_user(real_user_id)["name"], "Dipsy")
        
        no_template = config_handler.get_config_element_template("TestUser4", None)
        self.assertTrue(no_template is None)
        
        self.assertRaises(UserAdminException, config_handler.create_row_from_template, "TestUser4", user_template)
        self.assertRaises(UserAdminException, config_handler.update_row_from_template, "TestUser4", user_id, user_template)
        self.assertRaises(UserAdminException, config_handler.delete_row, "TestUser4", user_id)

        test_template = config_handler.get_config_element_template("TestUser3", None)
        config_spec = ConfigTemplateSpec(test_template)
        config_spec.set_value("test", 4)
        
        test_id , new_template = config_handler.create_row_from_template("TestUser3", test_template)
        config_spec = ConfigTemplateSpec(test_template)
        self.assertEquals(config_spec.get_value("test"), 4)
        
        config_handler.delete_row("TestUser3", test_id)
        dummy, dot, real_test_id = test_id.partition(".")
        self.assertTrue(user_admin.get_test(real_test_id) is None)

    def test1(self):
        user_admin = UserAdminStub(self)
        template_handler = TemplateHandlerStub()
        plugin_socket_config = PluginSocketConfigStub()
        config_handler = config_api.PluginConfigAPI(plugin_socket_config, template_handler, user_admin, giri_unittest.get_checkpoint_handler(), ActivityLogCreatorStub())

        user_template = config_handler.get_config_element_template("TestElement1", None)
        config_spec = ConfigTemplateSpec(user_template)
        config_spec.set_value("name", "Tinky Winky")
        
        user_id , new_template = config_handler.create_row_from_template("TestElement1", user_template)
        config_spec = ConfigTemplateSpec(new_template)
        self.assertEquals(config_spec.get_value("name"), "Tinky Winky")
        
        config_spec.set_value("name", "Po")
        config_handler.update_row_from_template("TestElement1", user_id, config_spec.get_template())
        
        dummy, dot, real_user_id = user_id.partition(".")
        self.assertEquals(plugin_socket_config.values[int(real_user_id)]["name"], "Po")

        config_handler.delete_row("TestElement1", user_id)
        self.assertTrue(plugin_socket_config.values[int(real_user_id)] is None)


    def test2(self):
        user_admin = UserAdminStub(self)
        template_handler = TemplateHandlerStub()
        plugin_socket_config = PluginSocketConfigStub()
        config_handler = config_api.PluginConfigAPI(plugin_socket_config, template_handler, user_admin, giri_unittest.get_checkpoint_handler(), ActivityLogCreatorStub())

        user_template = config_handler.get_config_element_template("TestResult", None)
        config_spec = ConfigTemplateSpec(user_template)
        config_spec.set_value("label", "NooNoo")
        
        user_id , new_template = config_handler.create_row_from_template("TestResult", user_template)
        config_spec = ConfigTemplateSpec(new_template)
        self.assertEquals(config_spec.get_value("label"), "NooNoo")
        
        config_spec.set_value("label", "Tubby Custard")
        config_handler.update_row_from_template("TestResult", user_id, config_spec.get_template())

        with ReadonlySession() as db_session:
            criteria = rule_api.get_class_criteria(db_session, user_id)
            self.assertEquals(criteria.title, "Tubby Custard")

        with ReadonlySession() as db_session:
            config_handler.delete_row("TestResult", user_id)
            self.assertTrue(rule_api.get_class_criteria(db_session, user_id) is None)


    def atest3a(self):
        user_admin = UserAdminStub(self)
        template_handler = TemplateHandlerStub()
        plugin_socket_config = PluginSocketConfigStub()
        config_handler = config_api.PluginConfigAPI(plugin_socket_config, template_handler, user_admin, giri_unittest.get_checkpoint_handler(), ActivityLogCreatorStub())

        user_template = config_handler.get_config_element_template("ProgramAccess", None)
        config_spec = ConfigTemplateSpec(user_template)
        config_spec.set_value("name", "Scooter")
        
        user_id , new_template = config_handler.create_row_from_template("ProgramAccess", user_template)
        config_spec = ConfigTemplateSpec(new_template)
        self.assertEquals(config_spec.get_value("label"), "NooNoo")
        
        config_spec.set_value("label", "Tubby Custard")
        config_handler.update_row_from_template("ProgramAccess", user_id, config_spec.get_template())

        with ReadonlySession() as db_session:
            criteria = rule_api.get_class_criteria(db_session, user_id)
            self.assertEquals(criteria.title, "Tubby Custard")

        with ReadonlySession() as db_session:
            config_handler.delete_row("ProgramAccess", user_id)
            self.assertTrue(rule_api.get_class_criteria(db_session, user_id) is None)

#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'pwl'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
