"""
This file contains the
"""
import datetime
import tempfile
import os.path
import shutil

import random
import sys

from lib import cryptfacility, commongon

import components.auth.server_management.rule_api as rule_api
import components.auth.server_management.new_rule_api as new_rule_api
import components.auth.server_common.rule_definitions as rule_definitions

from activity_log import ActivityLog, ActivityLogCreator

import lib.gpm.gpm_builder
import lib.gpm.gpm_env
import lib.hagi.gon_hagi_authentication
import lib.checkpoint
import lib.ws.ws_util as ws_util

from components.database.server_common import database_api
from components.auth.server_management import ElementType, RuleType, OperationNotAllowedException

import plugin_types.server_management.plugin_type_config
import plugin_types.common.plugin_type_token
from components.plugin.server_management import plugin_socket_config
from components.plugin.server_management import plugin_socket_report
from components.plugin.server_management import plugin_socket_element
from components.templates.server_common.template_api import TemplateHandler
from components.user.server_management.user_admin import UserAdmin
from components.dialog.server_management.dialog_api import DialogAdmin
from components.traffic.server_management.traffic_admin import TrafficAdmin
from components.plugin.server_common.config_api_lib import ConfigTemplateSpec
from components.endpoint.server_management.endpoint import EndpointAdmin
from components.auth.server_management.auth_admin import AuthAdmin, LoginException

from config_api import PluginConfigAPI


import admin_ws_download_manager

import portscan_api

import components.access_log.server_management.access_log
from components.traffic.server_management import traffic_admin
from components.rest.rest_api import RestApi
from components.rest.rest_api import Base64String


def empty_generator():
    for k in []:
        yield k

class NoAccessException(Exception):
    pass


class ElementFetcher(object):


    def __init__(self, admin_session, entity_type, search_filter, max_user_limit, max_group_limit):

        self.admin_session = admin_session
        self.entity_type = entity_type
        self.search_filter = search_filter
        self.result_truncated = False
        self.max_no_of_elements = dict()
        self.max_no_of_elements["User"] = max_user_limit
        self.max_no_of_elements["Group"] = max_group_limit
        self.admin_session.environment.checkpoint_handler.Checkpoint('ElementFetcher.__init__', "admin_ws_session", lib.checkpoint.DEBUG,
                                                                     entity_type=entity_type, search_filter=search_filter,
                                                                     max_user_limit=max_user_limit, max_group_limit=max_group_limit)

#        DO NOT DELETE : code will probably be enabled at some point
#        if rule_api.is_personal_token_type(entity_type):
#            self.personal_token_conditions = rule_api.get_all_personal_token_conditions()
#        else:
#            self.personal_token_conditions = None
        self.personal_token_conditions = None


    def get_search_filter(self):
        if self.search_filter:
            return "*%s*" % self.search_filter
        return None


    def element_generator(self):
        types = rule_api.get_basic_types(self.entity_type, self.admin_session.environment.checkpoint_handler)
        for sub_entity_type, type_dict in types:
            has_access = False
            try:
                self.admin_session._get_element_access(sub_entity_type)
            except NoAccessException:
                self.admin_session.environment.checkpoint_handler.Checkpoint('ElementFetcher.element_generator',
                                                                             "admin_ws_session", lib.checkpoint.INFO,
                                                                             msg="Denied access to element type '%s'" % sub_entity_type)
                continue

            max_no = self.max_no_of_elements.get(sub_entity_type)
            if max_no and max_no<0 and not self.search_filter:
                self.result_truncated = True
                continue
            if type_dict["config_type"] == "component":
                internal_element_type = type_dict["internal_element_type"]
                component = self.admin_session.plugin_config_api.get_component_handler(type_dict["component_name"])
                elements = component.get_elements(internal_element_type, search_filter=self.get_search_filter())
                if type_dict["component_name"]=="user" and internal_element_type == "user" and self.search_filter == "__REGISTERED_USERS__":
                    max_no = None

            elif type_dict["config_type"] == "module":
                plugin_providers = [(type_dict["plugin"], type_dict["internal_element_type"])]
                elements = self.admin_session.plugin_socket_element.get_module_elements(plugin_providers)
            else:
                elements = rule_api.get_criteria_elements(type_dict)
            if max_no:
                count = 0

                for e in elements:
                    count += 1
                    if count>abs(max_no):
                        self.result_truncated = True
                        break
                    yield self.admin_session._create_client_config_element(e, sub_entity_type, self.personal_token_conditions)
            else:
                for e in elements:
                    yield self.admin_session._create_client_config_element(e, sub_entity_type, self.personal_token_conditions)




class AdminWSSession(object):

    def __init__(self, environment, server_config, license_handler, session_id, service_management):
        self.environment = environment
        self.server_config = server_config
        self.session_id = session_id
        self.rule_handlers = dict()
        self.generators = dict()
        self.generator_count = 0
        self.report_activity()
        self._license_handler = license_handler
        self._activity_log_manager = ActivityLog(environment)
        self._activity_log_creator = ActivityLogCreator(self._activity_log_manager, session_id = session_id, severity=ActivityLog.INFO)
        self._user_admin = UserAdmin(environment, license_handler, self._activity_log_creator)
        self._endpoint_admin = EndpointAdmin(environment, server_config, license_handler, self._activity_log_creator)
        self.plugin_socket_config = plugin_socket_config.PluginSocket(self.environment.plugin_manager)
        self.plugin_socket_report = plugin_socket_report.PluginSocket(self.environment.plugin_manager)
        self.plugin_socket_element = plugin_socket_element.PluginSocket(self.environment.plugin_manager)
        self.template_handler = TemplateHandler(self.environment.checkpoint_handler)
        self._dialog_admin = DialogAdmin(environment, self.template_handler, self._activity_log_creator)
        self._traffic_admin = TrafficAdmin(environment, self.template_handler, self._dialog_admin, license_handler, self._user_admin, self._activity_log_creator)
        self._auth_admin = AuthAdmin(environment, self._user_admin, self.plugin_socket_report, license_handler, self._activity_log_creator)
        self.service_management = service_management
        self.plugin_config_api = PluginConfigAPI(self.plugin_socket_config,
                                                 self.template_handler,
                                                 self._user_admin,
                                                 self.environment.checkpoint_handler,
                                                 self._activity_log_creator)
        self._gpm_download_manager = admin_ws_download_manager.GPMDownloadManager(self.environment.gpms_root, self.environment.gpmc_defs_root)

        self.plugin_config_api.add_component_handler("dialog", self._dialog_admin)
        self.plugin_config_api.add_component_handler("traffic", self._traffic_admin)
        self.plugin_config_api.add_component_handler("endpoint", self._endpoint_admin)
        self.plugin_config_api.add_component_handler("auth", self._auth_admin)

        self._server_title_map = dict()

        self.max_no_of_specific_elements = 999

        self.rnd_menu = None

        new_rule_api.checkpoint_handler = self.environment.checkpoint_handler

        self.create_activity_log(summary = "Open Session",
                                 type="Session")


    def create_activity_log(self, **kwargs):
        activity_log = self._activity_log_creator.copy()
        activity_log.addValues(**kwargs)
        activity_log.save()


    def create_error_log(self, severity=ActivityLog.ERROR, **kwargs):
        self.create_activity_log(severity=severity, **kwargs)

    def create_log(self,**kwargs):
        self.create_activity_log(severity=ActivityLog.INFO, **kwargs)

    def raise_no_access_exception(self, element_type_name, element_type, access_type):
        msg = self._auth_admin.get_access_error_message(element_type_name, element_type, access_type)
        self.environment.checkpoint_handler.Checkpoint("no_access_exception", "admin_session", lib.checkpoint.WARNING, element_type_name=element_type_name, element_type=element_type, access_type=access_type)
        raise NoAccessException(msg)

    def report_activity(self):
        self.last_activity = datetime.datetime.now()

    def is_dead(self, timeout):
        return datetime.datetime.now() - self.last_activity > timeout

    def _update_client_config_element_data(self, element, e, entity_type=None):
        element.set_id(str(e.get_id()))

        if entity_type:
            element.set_entity_type(entity_type)
        else:
            element.set_entity_type(e.get_entity_type())

        element.set_info(e.get_info())
        element.set_label(e.get_label())
        element.set_element_type(e.get_element_type())

        if  element.get_id is None or element.get_entity_type is None or element.get_label is None:
            element_data = "id='%s', entity_type='%s', label='%s'" % (element.get_id, element.get_entity_type, element.get_label)
            raise Exception("Error in element data: %s" % element_data)


    def _create_client_config_element(self, e, entity_type=None, personal_token_conditions=None):
#        with self.environment.checkpoint_handler.CheckpointScope("_create_client_config_element", "admin_session", lib.checkpoint.DEBUG, element=e.get_id()):
        element = RestApi.Admin.ConfigElement()

        element.set_config_template(e.get_template())
        self._update_client_config_element_data(element, e, entity_type)

#        DO NOT DELETE : code will probably be enabled at some point
#        if personal_token_conditions and rule_api.is_personal_token_type(element._entity_type):
#            element_id_set = personal_token_conditions.get(element._entity_type, set())
#            is_personal_token_condition = element._id in element_id_set
#            print "is_personal_token_condition %s: %s" % (element._id, is_personal_token_condition)


        return element

    def _create_client_element(self, e, entity_type=None):
#        with self.environment.checkpoint_handler.CheckpointScope("_create_client_element", "admin_session", lib.checkpoint.DEBUG, element=e.get_id()):
        element = RestApi.Admin.Element()

        self._update_client_config_element_data(element, e, entity_type)
        return element

    def _create_client_rule(self, r):
        rule = RestApi.Admin.Rule()
        rule.set_id(r.get_id())
        rule.set_result_element(self._create_client_element(r.get_result_element()))
        rule.set_condition_elements([ self._create_client_element(e) for e in r.get_condition_elements() ])
        rule.set_active(r.is_active())
        if rule.get_id is None or not rule.get_result_element:
            rule_data = "rule id='%s'" % rule.get_id
            if rule.get_result_element:
                rule_data += ",result label=%s" % rule.get_result_element.get_label
            raise Exception("Error in rule data: %s" % rule_data)
        return rule


    def _create_client_string_list(self, element):
        lst = RestApi.Admin.StingList()
        if isinstance(element, basestring):
            lst.set_elements([element])
        else:
            lst.set_elements([e for e in element])
        return lst

    def get_rule_access(self, rule_type):
        rule_access = self._auth_admin.get_rule_access(rule_type)
        if not rule_access or not rule_access.read:
            self.raise_no_access_exception(rule_type, AuthAdmin.ACCESS_TYPE_RULE, "read")

        return rule_access

    def get_standard_access(self, standard_type, raise_if_not_read = True):
        standard_access = self._auth_admin.get_standard_access(standard_type)
        if raise_if_not_read and (not standard_access or not standard_access.read):
            self.raise_no_access_exception(standard_type, AuthAdmin.ACCESS_TYPE_STANDARD, "read")

        return standard_access

    def _get_element_access(self, element_type, raise_if_not_read_access=True):
        element_access = self._auth_admin.get_element_access(element_type)
        some_read_access = False

        for access in element_access:
            if access.read:
                some_read_access = True
                break

        if raise_if_not_read_access and not some_read_access:
            self.raise_no_access_exception(element_type, AuthAdmin.ACCESS_TYPE_ELEMENT, "read")

        return element_access

    def Login(self, request):

        username = request.get_username
        password = request.get_password
        error_code = 0
        error_msg = ""
        try:
            self._auth_admin.receive_login(username, password)
            self.create_log(type="Session", summary="Login",)
        except LoginException, e:
            error_msg = e.error_msg
            error_code = e.error_code
            self.create_error_log(type="Exception",
                                  summary="LoginException",
                                  details="Error code: %s, Error message: '%s'" % (error_code, error_msg))

        response = RestApi.Admin.LoginResponse()
        response.set_error_code(int(error_code))
        response.set_error_msg(error_msg)
        return response

    def InternalLogin(self, request):

        login_index = request.get_login_index
        login = request.get_login
        result = self.service_management.verify_internal_login(login_index, login)
        if result:
            self._auth_admin.give_access(AuthAdmin.ACCESS_DEF_ADMINISTRATOR)
            self._activity_log_creator.addValues(internal_user_login = True)
            self.create_log(type="Session", summary="InternalLogin",)

        response = RestApi.Admin.InternalLoginResponse()
        response.set_rc(result)
        return response


    def GetAccessDefintion(self, request):
        response = RestApi.Admin.GetAccessDefintionResponse()
        all_admin_access = self._auth_admin.get_all_admin_access()
        report_access_given = False
        for name, access_element in all_admin_access.items():
            if name.startswith(AuthAdmin.ACCESS_TYPE_REPORT):
                if report_access_given or not access_element or not access_element.read:
                    continue
                name = "Report"
                report_access_given = True
            access_def = RestApi.Admin.AccessDef()
            access_def.set_name(name)
            if access_element:
                access_def.set_read_access(access_element.read)
            else:
                access_def.set_read_access(False)
            response.get_access_defs.append(access_def)
        return response

    def GetModuleElements(self, request):
        element_type = request.get_element_type

        element_access_list = self._get_element_access(element_type)

        search_filter = request.get_search_filter

        response = RestApi.Admin.GetModuleElementsResponse()

        elements = self._get_module_elements(element_type, search_filter, element_access_list)
        for e in elements:
            response.get_elements.append(e)
        return response


    def GetSpecificElements(self, request):
        element_specs = request.get_element_specs
#        element_def_dict = dict()
#        for element_spec in element_specs:
#            element_id = element_spec._id
#            entity_type = element_spec._entity_type
#            elements = element_def_dict.get(entity_type, set())
#            elements.add(element_id)
#            element_def_dict[entity_type] = elements

        response = RestApi.Admin.GetSpecificElementsResponse()

        for element_spec in element_specs:
            entity_type = element_spec.get_entity_type
            try:
                element_access_list = self._get_element_access(entity_type)
            except NoAccessException:
                continue

            element_ids = element_spec.get_ids
            if len(element_ids) > self.max_no_of_specific_elements:
#                print "Get All " + entity_type
                # TODO: Check if extend works properly!
                response.get_elements.extend(self._get_module_elements(entity_type, None, element_access_list))
            else:
#                print "Get Some " + entity_type

                type_dict = rule_definitions.def_element_types.get(entity_type)
                assert type_dict is not None

                if type_dict["config_type"] == "component":
                    internal_element_type = type_dict["internal_element_type"]
                    component = self.plugin_config_api.get_component_handler(type_dict["component_name"])
                    elements = component.get_specific_elements(internal_element_type, element_ids)
                elif type_dict["config_type"] == "module":
                    plugin_element_ids = [element_id.partition(".")[2] for element_id in element_ids]
                    elements = self.plugin_socket_element.get_specific_elements(type_dict["plugin"], type_dict["internal_element_type"], plugin_element_ids)
                else:
                    elements = rule_api.get_specific_criteria_elements(element_ids)

                for e in elements:
                    response.get_elements.append(self._create_client_config_element(e, entity_type))


        return response


    def GetAuthRestrictionElements(self, request):
        element = self._create_server_element(request.get_element)
        restriction_type = request.get_restriction_type

        entity_type = element.get_entity_type()
        element_access_list = self._get_element_access(entity_type)

        restriction_elements = rule_api.get_restriction_elements(element, restriction_type)

        response = RestApi.Admin.GetAuthRestrictionElementsResponse()

        for restriction_element in restriction_elements:
            response.get_elements.append(self._create_client_element(restriction_element))

        return response


    def UpdateAuthRestrictionElements(self, request):
        element = self._create_server_element(request.get_element)
        restriction_type = request.get_restriction_type
        restriction_elements = [self._create_server_element(e) for e in request.get_restriction_elements]

        entity_type = element.get_entity_type()
        element_access_list = self._get_element_access(entity_type)
        if not element_access_list[0].update:
            self.raise_no_access_exception(entity_type, AuthAdmin.ACCESS_TYPE_ELEMENT, "update")

        rc = rule_api.update_restriction_elements(element, restriction_type, restriction_elements)

        lines = []
        lines.append("Menu Action name : %s" % element.get_label())
        lines.append("Menu Action id : %s\n" % element.get_id())
        lines.append("Zones:")
        for e in restriction_elements:
            lines.append("Zone '%s' (%s)" % (e.get_label(), e.get_id()))
        details = "\n".join(lines)

        self.create_log(type="Update",
                        summary="Update Menu Action Zone restrictions",
                        details=details )

        response = RestApi.Admin.UpdateAuthRestrictionElementsResponse()
        response.set_rc(rc)

        return response


    def RemoveUserFromLaunchTypeCategory(self, request):
        element = self._create_server_element(request.get_element)
        launch_type_category = request.get_launch_type_category

        rc = self._traffic_admin.delete_launch_type_launched_for_user(element.get_id(), launch_type_category)

        response = RestApi.Admin.RemoveUserFromLaunchTypeCategoryResponse()
        response.set_rc(rc)

        return response



    def GetRestrictedLaunchTypeCategories(self, request):
        launc_type_categories = []
        license = self._license_handler.get_license()
        for number, name in traffic_admin.launch_type_category_license_count_name.items():
            count = license.get_int(name, default=-1)
            if count >= 0:
                category_name = traffic_admin.launch_type_categories.get(number)
                if category_name:
                    launc_type_categories.append(category_name)

        response = RestApi.Admin.GetRestrictedLaunchTypeCategoriesResponse()
        response.set_launch_type_category(launc_type_categories)

        return response


    def _get_module_elements(self, entity_type, search_filter, element_access_list = None):
        types = rule_api.get_basic_types(entity_type, self.environment.checkpoint_handler)
        for sub_entity_type, type_dict in types:
            if type_dict["config_type"] == "component":
                internal_element_type = type_dict["internal_element_type"]
                component = self.plugin_config_api.get_component_handler(type_dict["component_name"])
                if search_filter:
                    elements = component.get_elements(internal_element_type, search_filter)
                else:
                    elements = component.get_elements(internal_element_type)
            elif type_dict["config_type"] == "module":
                plugin_providers = [(type_dict["plugin"], type_dict["internal_element_type"])]
                elements = self.plugin_socket_element.get_module_elements(plugin_providers)
            else:
                elements = rule_api.get_criteria_elements(type_dict)
            for e in elements:
                yield self._create_client_config_element(e, sub_entity_type)



    def _get_module_header_element(self, element_type):
        module_type = rule_api.get_element_type_dict(element_type)
        if not module_type:
            raise Exception("Unknown element type '%s'" % element_type)
        header = rule_api.TemplateElement(element_type, module_type)
        return header



    def get_next_elements(self, elements, start_index, count):
        with self.environment.checkpoint_handler.CheckpointScope('get_next_elements', "admin_ws_session", lib.checkpoint.DEBUG, start_index=start_index, count=count) as cps:
            if isinstance(elements, list):
                elements_to_return = elements[start_index:count + start_index]
                count_max = len(elements)
                cps.add_complete_attr(count_max=count_max)
            else:
                i = 0
                elements_to_return = []
                for e in elements:
                    elements_to_return.append(e)
                    i += 1
                    if i >= count:
                        break
                count_max = None
                cps.add_complete_attr(generator=True)
            cps.add_complete_attr(elements_to_return=len(elements_to_return))
            return elements_to_return, count_max

    def GetModuleHeaderElement(self, request):
        element_type = request.get_element_type
        element = self._get_module_header_element(element_type)

        response = RestApi.Admin.GetModuleHeaderElementResponse()
        response.set_element(self._create_client_element(element))

        return response

    def GetElementSeachCategories(self, request):
        element_type = request.get_element_type
        element = self._get_module_header_element(element_type)

        response = RestApi.Admin.GetElementSeachCategoriesResponse()
        if element_type == "User":
            categories = self._user_admin.get_search_categories()
            for (value, title) in categories:
                choice = RestApi.Admin.ConfigTemplate.ValueSelectionChoice()
                choice.set_value(value)
                choice.set_title(title)
                response.get_search_categories.append(choice)

        return response


    def GetModuleElementsFirst(self, request):
        element_type = request.get_element_type
        search_filter = request.get_search_filter
        count = request.get_count

        self._get_element_access(element_type)

        response = RestApi.Admin.GetModuleElementsFirstResponse()

#        elements = self._get_module_elements(element_type, search_filter, clear_cache)
        element_fetcher = ElementFetcher(self, element_type, search_filter, self.server_config.ws_user_limit, self.server_config.ws_group_limit)
        elements = element_fetcher.element_generator()
        element_count = -1
        index = 0


        elements_to_return, count_max = self.get_next_elements(elements, index, count)

        if not count_max and element_count >= 0:
            count_max = element_count

        response.set_result_truncated(element_fetcher.result_truncated)

        if len(elements_to_return) < count:
            response.set_count_max(len(elements_to_return))
        else:
            element_session_id = str(self.generator_count)
            self.generator_count +=1
            self.generators[element_session_id] = (count, elements, element_fetcher)
            response.set_next_session_id(element_session_id)
            response.set_count_max(count_max)

        for e in elements_to_return:
            response.get_elements.append(e)
        return response

    def GetModuleElementsNext(self, request):
        count = request.get_count
        element_session_id = request.get_next_session_id
        if not self.generators.has_key(element_session_id):
            raise Exception("Unknown next_session_id : '%s'" % element_session_id)
        index, elements, element_fetcher = self.generators[element_session_id]
        elements_to_return, count_max = self.get_next_elements(elements, index, count)

        response = RestApi.Admin.GetModuleElementsNextResponse()

        response.set_result_truncated(element_fetcher.result_truncated)

        if len(elements_to_return) < count:
            #response.set_count_max(len(elements_to_return))
            self.generators.pop(element_session_id)
        else:
            self.generators[element_session_id] = (count+index, elements, element_fetcher)
            response.set_next_session_id(element_session_id)
            #response.set_count_max(count_max)

        for e in elements_to_return:
            response.get_elements.append(e)

        #print elements_to_return
        return response


    def GetRulesFirst(self, request):
        rule_type = request.get_rule_type
        search_filter = request.get_search_filter
        count = request.get_count

        self.get_rule_access(rule_type)

        response = RestApi.Admin.GetRulesFirstResponse()

        rule_handler = self._get_rule_handler(rule_type, False)

        rules = rule_handler.get_rules()
        response.set_count_max(len(rules))

        index = 0
        with self.environment.checkpoint_handler.CheckpointScope("create_client_rules", "admin_ws_session", lib.checkpoint.DEBUG, n=len(rules)):
            while rules and index<count:
                r = rules.pop()
                rule = self._create_client_rule(r)
                response.get_rules.append(rule)
                index += 1;
            if rules:
                response.set_result_truncated(True)
                element_session_id = str(self.generator_count)
                self.generator_count +=1
                self.generators[element_session_id] = (index+1, rules)
                response.set_next_session_id(element_session_id)
            else:
                response.set_result_truncated(False)

#        elements = self._get_module_elements(element_type, search_filter, clear_cache)
        response.set_result_truncated(False)

        return response

    def GetRulesNext(self, request):
        count = request.get_count
        element_session_id = request.get_next_session_id
        if not self.generators.has_key(element_session_id):
            raise Exception("Unknown next_session_id : '%s'" % element_session_id)
        prev_fetched, rules = self.generators[element_session_id]

        response = RestApi.Admin.GetRulesNextResponse()

        index = 0
        with self.environment.checkpoint_handler.CheckpointScope("create_client_rules", "admin_ws_session", lib.checkpoint.DEBUG, n=len(rules)):
            while rules and index<count:
                r = rules.pop()
                rule = self._create_client_rule(r)
                response.get_rules.append(rule)
                index += 1;
            if rules:
                response.set_result_truncated(True)
                self.generators[element_session_id] = (prev_fetched + index + 1, rules)
                response.set_next_session_id(element_session_id)
            else:
                response.set_result_truncated(False)
                self.generators.pop(element_session_id)


        #print elements_to_return
        return response


    def AddMembersToFirstTimeEnrollers(self, request):
        element = request.get_element

        self._get_element_access("User")
        result_element_type = rule_api.get_gon_group_rule_type()
        self._get_element_access(result_element_type)
        rule_access = self.get_rule_access(result_element_type)
        if not rule_access.create:
            self.raise_no_access_exception(result_element_type, AuthAdmin.ACCESS_TYPE_RULE, "create")

        self._user_admin.add_members_to_one_time_enrollers(self._create_server_element(element))

        response = RestApi.Admin.AddMembersToFirstTimeEnrollersResponse()
        response.set_rc(True)
        return response

    def GetBasicEntityTypes(self, request):
        entity_type = request.get_entity_type
        basic_types = rule_api.get_basic_types(entity_type, self.environment.checkpoint_handler)

        response = RestApi.Admin.GetBasicEntityTypesResponse()
        for basic_type in basic_types:
            response.get_entity_types.append(basic_type[0])
            # Minor hack to ensure that menu actions are updated in menu structure mgmt:
            module_type_dict = basic_type[1]
            if module_type_dict.get("alias"):
                response.get_entity_types.append(module_type_dict.get("alias"))

        return response



    def _get_rule_handler(self, class_name, clear_cache=False):
        in_cache = self.rule_handlers.has_key(class_name)
        if in_cache and clear_cache:
            self.rule_handlers.pop(class_name)
            in_cache = False
        if not in_cache:
            activity_log_scope = self._activity_log_creator.copy()
            activity_log_scope.addValues(access_right="%s.%s" % (AuthAdmin.ACCESS_TYPE_RULE, class_name))
            rule_handler = rule_api.create_rule_handler(self.environment.checkpoint_handler, self._user_admin, class_name, activity_log_scope)
            self.rule_handlers[class_name] = rule_handler
        else:
            rule_handler = self.rule_handlers.get(class_name)
        return rule_handler


    def GetRules(self, request):
        class_name = request.get_class_name
        clear_cache = request.get_refresh_cache

        self.get_rule_access(class_name)

        rule_handler = self._get_rule_handler(class_name, clear_cache)

        response = RestApi.Admin.GetRulesResponse()

        rules = rule_handler.get_rules()

        with self.environment.checkpoint_handler.CheckpointScope("create_client_rules", "admin_ws_session", lib.checkpoint.DEBUG, n=len(rules)):
            for r in rules:
                rule = self._create_client_rule(r)
                response.get_rules.append(rule)

#        id = 42
#
#        response._content._result_element = ns0.ElementType_Def('result_element')
#        response._content._result_element._id = "a"
#        response._content._result_element._entity_type = "b"
#        response._content._result_element._info = "c"
#        response._content._result_element._label = "d"
#
#        response._content._condition_elements = []

        return response


    def _create_server_element(self, e):
        element = ServerElementType(e)
        return element

    def _create_server_rule(self, r):
        rule = ServerRuleType(r);
        return rule


    def CreateRule(self, request):
        client_rule = request.get_rule
        class_name = request.get_class_name

        rule_access = self.get_rule_access(class_name)
        if not rule_access.create:
            self.raise_no_access_exception(class_name, AuthAdmin.ACCESS_TYPE_RULE, "create")

        rule_handler = self._get_rule_handler(class_name)
        server_rule = self._create_server_rule(client_rule)

        new_rule = rule_handler.add_rule(server_rule)

        response = RestApi.Admin.CreateRuleResponse()
        response.set_rule(self._create_client_rule(new_rule))
        return response

    def UpdateRule(self, request):
        class_name = request.get_class_name
        new_rule = self._create_server_rule(request.get_new_rule)
        old_rule = self._create_server_rule(request.get_old_rule)

        rule_access = self.get_rule_access(class_name)
        if not rule_access.update:
            self.raise_no_access_exception(class_name, AuthAdmin.ACCESS_TYPE_RULE, "update")

        rule_handler = self._get_rule_handler(class_name)

        updated_rule = rule_handler.update_rule(old_rule, new_rule)
        updated_rule_for_client = self._create_client_rule(updated_rule)

        response = RestApi.Admin.UpdateRuleResponse()
        response.set_rule(updated_rule_for_client)
        return response

    def DeleteRule(self, request):
        class_name = request.get_class_name
        client_rule = request.get_rule

        rule_access = self.get_rule_access(class_name)
        if not rule_access.delete:
            self.raise_no_access_exception(class_name, AuthAdmin.ACCESS_TYPE_RULE, "delete")

        server_rule = self._create_server_rule(client_rule)
        rule_handler = self._get_rule_handler(class_name)

        rule_handler.delete_rule(server_rule)

        response = RestApi.Admin.DeleteRuleResponse()
        response.set_rc(True)
        return response

    def GetRuleSpec(self, request):
        class_name = request.get_class_name

        rule_access = self.get_rule_access(class_name)

        rule_handler = self._get_rule_handler(class_name)

        response = RestApi.Admin.GetRuleSpecResponse()

        client_spec = RestApi.Admin.RuleSpec()
        rule_spec = rule_handler.get_rule_spec()

        client_spec.set_rule_title(rule_spec.get_title())
        client_spec.set_rule_title_long(rule_spec.get_title_long())
        client_spec.set_unique_elements(rule_spec.get_unique_element_types())
        client_spec.set_mandatory_elements([ self._create_client_string_list(element) for element in
                                             rule_spec.get_mandatory_element_types()])
        client_spec.set_header_rule(self._create_client_rule(rule_spec.get_header_rule()))
        client_spec.set_template_rule(self._create_client_rule(rule_spec.get_template_rule()))
        client_spec.set_edit_enabled(rule_access.update)
        client_spec.set_create_enabled(rule_access.create)
        client_spec.set_delete_enabled(rule_access.delete)
        if rule_spec.get_type_map():
            entity_type_maps = []
            for key, values in rule_spec.get_type_map().items():
                entity_type_map = RestApi.Admin.StringMap()
                entity_type_map.set_key(key)
                entity_type_map.set_values(values)
                entity_type_maps.append(entity_type_map)
            client_spec.set_entity_type_maps(entity_type_maps)

        #response._content._rule_spec = client_spec
        response.set_rule_spec(client_spec)
        return response

    def _create_client_rule1(self, server_rule):
        rule = RestApi.Admin.RuleType1()
        rule.set_id(str(server_rule.rule_id))
        rule.set_result_id(server_rule.result)
        rule.set_entity_type(server_rule.entity_type)
        rule.set_rule_type(server_rule.rule_type)
        for c in server_rule.conditions:
            condition = RestApi.Admin.Condition()
            condition.set_condition_id(str(c.condition_id))
            condition.set_entity_type(c.entity_type)
            condition.set_rule_entity_type(c.rule_entity_type)
            rule.get_condition_elements.append(condition)
        rule.set_active(server_rule.active)
        return rule

    def _create_server_rule1(self, client_rule):
        rule = new_rule_api.Rule(client_rule._id, client_rule._result_id, client_rule._entity_type, client_rule._rule_type)
        for condition in client_rule._condition_elements:
            server_condition = new_rule_api.Condition(condition._condition_id,
                                                      condition._entity_type,
                                                      condition._rule_entity_type)
            rule.conditions.append(server_condition)
        return rule
    def _create_authorized_rule_list(self, rule_list):
        authorized_rule_list = []
        for rule in rule_list:
            try:
                self.get_rule_access(rule.entity_type)
            except NoAccessException:
                continue
            authorized_rule_list.append(rule)
        return authorized_rule_list


    def GetRulesForElement(self, request):
        element_id = request.get_element_id
        entity_type = request.get_entity_type
        known_rules = [self._create_server_rule1(client_rule) for client_rule in request.get_known_rules]

        new_rules, deleted_rules = new_rule_api.get_rules_for_element(element_id, entity_type, known_rules)
        new_rules = self._create_authorized_rule_list(new_rules)
        deleted_rules = self._create_authorized_rule_list(deleted_rules)

        response = RestApi.Admin.GetRulesForElementResponse()
        response.set_new_rules([self._create_client_rule1(rule) for rule in new_rules])
        response.set_deleted_rules([self._create_client_rule1(rule) for rule in deleted_rules])

        return response


    def GetReports(self, request):
        reports = self.plugin_socket_report.get_reports()
        report_access_dict = self._auth_admin.get_report_access()
        response = RestApi.Admin.GetReportsResponse()
        for report in reports:
            access = report_access_dict.get(report.report_id)
            if access and access.read:
                response_report = RestApi.Admin.ReportRef()
                response_report.set_module_name(report.module_name)
                response_report.set_report_id(report.report_id)
                response_report.set_report_title(report.report_title)
                response.get_reports.append(response_report)
        return response

    def GetReportSpec(self, request):
        module_name = request.get_module_name
        report_id = request.get_report_id
        specification_filename = request.get_specification_filename

        report_name = self._auth_admin.check_report_access(report_name=report_id, specification_filename=specification_filename)
        if report_name:
            self.raise_no_access_exception("%s.%s" % (report_name, specification_filename), AuthAdmin.ACCESS_TYPE_REPORT, "read")

        response = RestApi.Admin.GetReportSpecResponse()
        response.set_report_spec(self.plugin_socket_report.get_report_specification(module_name, report_id,
                                                                                    specification_filename))
        return response


    def GetReportData(self, request):
        module_name = request.get_module_name
        data_id = request.get_data_id

        report_name = self._auth_admin.check_report_access(data_id=data_id)
        if report_name:
            self.raise_no_access_exception("%s.%s" % (report_name, data_id), AuthAdmin.ACCESS_TYPE_REPORT, "read")

        args = self.report_data_build_args(request)

        response = RestApi.Admin.GetReportDataResponse()

        report_data_rows = self.plugin_socket_report.get_report_data(module_name, data_id, args)
#        print "GetReportData: %d rows" % len(report_data_rows)
        self.environment.checkpoint_handler.Checkpoint("GetReportData", "admin_session", lib.checkpoint.DEBUG, report_data_count=len(report_data_rows))
        for row in report_data_rows:
            response_row = RestApi.Admin.Values()
            for value in row:
                response_row.get_values.append(str(value))
            response.get_rows.append(response_row)
        return response


    def GetConfigSpec(self, request):
        entity_type = request.get_entity_type
        response = RestApi.Admin.GetConfigSpecResponse()

        element_access_list = self._get_element_access(entity_type)
        if element_access_list:
            create_enabled = False
            edit_enabled = True
            delete_enabled = True
            for element_access in element_access_list:
                if element_access.create:
                    create_enabled = True
                if not element_access.update:
                    edit_enabled = False
                if not element_access.delete:
                    delete_enabled = False
        else:
            create_enabled = False
            edit_enabled = False
            delete_enabled = False


#        For testing server auth on client
#        create_enabled = True
#        edit_enabled = True
#        delete_enabled = True

        if create_enabled:
            if entity_type == "Zone":
                license = self._license_handler.get_license()
                create_enabled = license.has('Feature', 'Zones')

        if create_enabled:
            response.set_config_templates(self.plugin_config_api.get_element_template_defs(entity_type))
        config_edit_enabled, config_delete_enabled  = self.plugin_config_api.get_crud_enabling(entity_type)
        response.set_error_templates(self.template_handler.get_error_template_names())
        response.set_edit_enabled(edit_enabled and config_edit_enabled)
        response.set_edit_possible(config_edit_enabled)
        response.set_delete_enabled(delete_enabled and config_delete_enabled)
        response.set_create_enabled(create_enabled and len(response.get_config_templates)>0)

        return response

    def GetConfigTemplate(self, request):
        entity_type = request.get_entity_type

#        element_access_list = self._get_element_access(entity_type)
#        assert len(element_access_list)==1
#        if not element_access_list[0].create:
#            llll

        template_name = request.get_template_name
        response = RestApi.Admin.GetConfigTemplateResponse()
        template = self.plugin_config_api.get_config_template(entity_type, template_name)
        if template:
            if not template.get_entity_type:
                template.set_entity_type(entity_type)
            response.set_config_template(template)
        return response

    def GetElementConfigTemplate(self, request):
        module_element = self._create_server_element(request.get_element)

        self._get_element_access(module_element.get_entity_type())

        bypass_custom_template = request.get_bypass_custom_template
        response = RestApi.Admin.GetElementConfigTemplateResponse()
        template = self.plugin_config_api.get_config_element_template(module_element.get_entity_type(), module_element.get_id(), bypass_custom_template=bypass_custom_template)
        if template:
            response.set_config_template(template)
        return response



    def CreateConfigTemplateRow(self, request):
        entity_type = request.get_entity_type

        if entity_type == "Zone":
            license = self._license_handler.get_license()
            if not license.has('Feature', 'Zones'):
                raise OperationNotAllowedException("Missing license feature 'Zones'")

        element_access_list = self._get_element_access(entity_type)
        assert len(element_access_list)==1
        if not element_access_list[0].create:
            self.raise_no_access_exception(entity_type, AuthAdmin.ACCESS_TYPE_ELEMENT, "create")

        template = request.get_template
        response = RestApi.Admin.CreateConfigTemplateRowResponse()
        element_id, template = self.plugin_config_api.create_row_from_template(entity_type, template)
        response.set_template(template)
        response.set_element_id(str(element_id))
        return response

    def UpdateConfigTemplateRow(self, request):
        entity_type = request.get_entity_type

        element_access_list = self._get_element_access(entity_type)
        assert len(element_access_list)==1
        if not element_access_list[0].update:
            self.raise_no_access_exception(entity_type, AuthAdmin.ACCESS_TYPE_ELEMENT, "update")

        element_id = request.get_element_id
        template = request.get_template
        response = RestApi.Admin.UpdateConfigTemplateRowResponse()
        result_template = self.plugin_config_api.update_row_from_template(entity_type, element_id, template)
        if result_template:
            response.set_template(result_template)
        else:
            response.set_template(template)
        return response

    def DeleteConfigElementRow(self, request):
        entity_type = request.get_entity_type

        element_access_list = self._get_element_access(entity_type)
        assert len(element_access_list)==1
        if not element_access_list[0].delete:
            self.raise_no_access_exception(entity_type, AuthAdmin.ACCESS_TYPE_ELEMENT, "delete")

        element_id = request.get_element_id
        response = RestApi.Admin.DeleteConfigElementRowResponse()
        response.set_rc(self.plugin_config_api.delete_row(entity_type, element_id))
        return response


    def GetTokenInfo(self, request):
        tokens = request.get_tokens
        self._get_element_access("Token")
        with database_api.ReadonlySession() as dbs:
            for token in tokens:
                plugin_name = token.get_token_type_id
                token_serial = token.get_token_serial
                db_token = plugin_types.common.plugin_type_token.lookup_token(dbs, plugin_name, token_serial)
                if db_token:
                    token.set_name(db_token.name)
                    token.set_casing(db_token.casing_number)
                    token.set_description(db_token.description)
                    token.set_enrollable(False)
                else:
                    token.set_enrollable(True)
        response = RestApi.Admin.GetTokenInfoResponse()
        response.set_tokens(ws_util.copy_tokens(tokens))
        return response

    def EnrollDevice(self, request):
        rc = True
        message = ""
        template = None
        unique_session_id = None

        try:
            template = request.get_device_template
            config_spec = ConfigTemplateSpec(template)
            group_name = request.get_group_name
            activate_rule = request.get_activate_rule
            unique_session_id = request.get_unique_session_id
            device_entity_type = template.get_entity_type

            element_access_list = self._get_element_access(device_entity_type)
            assert len(element_access_list)==1
            if not element_access_list[0].create:
                self.raise_no_access_exception(device_entity_type, AuthAdmin.ACCESS_TYPE_ELEMENT, "create")

            with database_api.Transaction() as transaction:
                device_id, template = self.plugin_config_api.create_row_from_template(device_entity_type, template, transaction)
                device_plugin, dot, device_id = device_id.partition(".")
                device_id, device_name, device_entity_type = self._endpoint_admin.create_endpoint(device_id, device_plugin, device_entity_type, unique_session_id, transaction)

                if group_name:

                    class_name = "DeviceGroupMembership"
                    rule_access = self.get_rule_access(class_name)
                    if not rule_access.create:
                        self.raise_no_access_exception(class_name, AuthAdmin.ACCESS_TYPE_RULE, "create")
                    self._get_element_access("Device")

                    rule_handler = self._get_rule_handler(class_name)
                    rc, message = rule_api.create_device_group_rule(rule_handler, group_name, device_id, device_name, activate_rule, transaction)

        except Exception, e:
            (etype, evalue, etrace) = sys.exc_info()
            self.environment.checkpoint_handler.CheckpointException("EnrollDevice", "admin_ws_session", lib.checkpoint.ERROR, etype, evalue, etrace)
            rc = False
            if unique_session_id and template:
                message = "Internal Error during Enrollment - details are logged on server"
            else:
                message = e

        response = RestApi.Admin.EnrollDeviceResponse()
        response.set_rc(rc)
        response.set_message(message)
        return response

    def EnrollDeviceToUser(self, request):
        rc = True
        message = ""
        template = None
        unique_session_id = None

        try:
            external_user_id = request.get_external_user_id
            plugin_name = request.get_user_plugin
            user_id = "%s.%s" % (plugin_name, external_user_id)
            template = request.get_device_template
            config_spec = ConfigTemplateSpec(template)
            device_name = config_spec.get_value("name")
            device_plugin = config_spec.get_value("plugin_type")
            enroll_as_endpoint = request.get_enroll_as_endpoint
            unique_session_id = request.get_unique_session_id
            device_entity_type = template.get_entity_type

            element_access_list = self._get_element_access(device_entity_type)
            assert len(element_access_list)==1
            if not element_access_list[0].create:
                self.raise_no_access_exception(device_entity_type, AuthAdmin.ACCESS_TYPE_ELEMENT, "create")

            with database_api.Transaction() as transaction:
                device_id, template = self.plugin_config_api.create_row_from_template(device_entity_type, template, transaction)
                if enroll_as_endpoint:
                    device_plugin, dot, device_id = device_id.partition(".")
                    device_id, device_name, device_entity_type = self._endpoint_admin.create_endpoint(device_id, device_plugin, device_entity_type, unique_session_id, transaction)
                else:
                    config_spec = ConfigTemplateSpec(template)
                    device_name = config_spec.get_value("name")
                if external_user_id:

                    class_name = rule_definitions.def_enrollment_map.get(device_entity_type)
                    if not class_name:
                        for key, value in rule_definitions.def_enrollment_map.items():
                            if rule_definitions.is_type_of(device_entity_type, key):
                                class_name = value
                                break
                    if not class_name:
                        raise Exception("Could not find rule type to create for device type %s" % device_entity_type)

                    rule_access = self.get_rule_access(class_name)
                    if not rule_access.create:
                        self.raise_no_access_exception(class_name, AuthAdmin.ACCESS_TYPE_RULE, "create")
                    self._get_element_access("User")

                    rule_handler = self._get_rule_handler(class_name)
                    rc, message = rule_api.create_personal_device_rule(rule_handler, user_id, device_id, device_name,
                                                                       device_entity_type, request.get_activate_rule,
                                                                       transaction)
                    rule_api.delete_gon_group_rule(self._get_rule_handler, user_id, transaction)
        except OperationNotAllowedException, e:
            raise e
        except Exception, e:
            (etype, evalue, etrace) = sys.exc_info()
            self.environment.checkpoint_handler.CheckpointException("EnrollDeviceToUser", "admin_ws_session", lib.checkpoint.ERROR, etype, evalue, etrace)
            rc = False
            if unique_session_id and template:
                message = "Internal Error during Enrollment - details are logged on server"
            else:
                message = unicode(e)

            if unique_session_id and template:
                self._endpoint_admin.log_failed_enrollment(template, unique_session_id, message)

        response = RestApi.Admin.EnrollDeviceToUserResponse()
        response.set_rc(rc)
        response.set_message(message)
        return response

    def GetEnrollmentRequests(self, request):
        rule_type = u"PersonalTokenAssignment"
        self.get_rule_access(rule_type)
        rule_handler = self._get_rule_handler(rule_type, False)
        rules = rule_handler.get_rules()

        response = RestApi.Admin.GetEnrollmentRequestsResponse()
        for rule in rules:
            if rule.is_active():
                continue
            enrollment_request = RestApi.Admin.EnrollmentRequest()
            enrollment_request.set_id(str(rule.get_id()))
            for condition in rule.get_condition_elements():
                if condition.get_info():
                    info = "%s: %s" % (condition.get_label(), condition.get_info())
                else:
                    info = condition.get_label()
                if condition.get_entity_type().startswith("User"):
                    enrollment_request.set_user_info(info)
                elif condition.get_entity_type().startswith("Token"):
                    enrollment_request.set_token_info(info)
            response.get_enrollment_requests.append(enrollment_request)

        return response




    def RespondToEnrollmentRequest(self, request):

        rule_id = request.get_enrollment_request_id
        accept_enrollment = request.get_accept
        with database_api.Transaction() as transaction:
            rule_criteria = rule_api.get_class_criteria(transaction, rule_id)
            if not rule_criteria:
                raise Exception("Rule not found")
            rule_access = self.get_rule_access(rule_criteria.entity_type)
            if not rule_access.update:
                self.raise_no_access_exception(rule_criteria.entity_type, AuthAdmin.ACCESS_TYPE_RULE, "update")

            if accept_enrollment:
                rule_api.set_rule_activation(rule_id, True, transaction)
            else:
                rule_api.set_rule_activation(rule_id, False, transaction)
#                rule_api.delete_rule(rule_id, transaction)
        print(request.get_enrollment_request_id)
        rc = True

        response = RestApi.Admin.RespondToEnrollmentRequestResponse()
        response.set_rc(rc)

        return response


    def DeployGetKnownSecretClient(self, request):
        response = RestApi.Admin.DeployGetKnownSecretClientResponse()
        response.set_knownsecret_client(self.environment.client_knownsecret)
        return response

    def DeployGetServers(self, request):
        response = RestApi.Admin.DeployGetServersResponse()
        response.set_servers(self.environment.get_servers())
        return response

    def DeployGenerateKeyPair(self, request):
        response = RestApi.Admin.DeployGenerateKeyPairResponse()

        key_pair = cryptfacility.pk_generate_keys()
        response.set_public_key(key_pair[0])
        response.set_private_key(key_pair[1])
        return response

    def GetGPMCollections(self, request):
        response = RestApi.Admin.GetGPMCollectionsResponse()
        response_collection_infos = []
        for gpm_collection_id in self._gpm_download_manager.get_collection_ids():
            response_collection_info = RestApi.Admin.Gpm.GPMCollectionInfo()

            collection_info = self._gpm_download_manager.get_gpm_collection_info(gpm_collection_id)
            response_collection_info.set_gpm_collection_id(gpm_collection_id)
            response_collection_info.set_gpm_collection_summary(collection_info['summary'])
            response_collection_info.set_gpm_collection_description(collection_info['description'])

            response_gpm_collection_gpms = []
            for gpm_id in self._gpm_download_manager.get_ids_for_collection(gpm_collection_id):
                info = self._gpm_download_manager.get_info(gpm_id)
                response_gpm_collection_gpm = RestApi.Admin.Gpm.GPMInfo()
                response_gpm_collection_gpm.set_gpm_id(gpm_id)
                response_gpm_collection_gpm.set_gpm_summary(info['summary'])
                response_gpm_collection_gpm.set_gpm_description(info['description'])
                response_gpm_collection_gpm.set_gpm_version(info['version'])
                response_gpm_collection_gpm.set_gpm_arch(info['arch'])
                response_gpm_collection_gpm.set_gpm_checksum(info['checksum'])
                response_gpm_collection_gpms.append(response_gpm_collection_gpm)

            response_collection_info.set_gpm_collection_gpms(response_gpm_collection_gpms)
            response_collection_infos.append(response_collection_info)

        response.set_gpm_collections(response_collection_infos)
        return response

    def DownloadGPMStart(self, request):
        gpm_id = request.get_gpm_id

        rc = self._gpm_download_manager.start_gpm_download(gpm_id)
        response = RestApi.Admin.DownloadGPMStartResponse()

        if rc['rc'] == admin_ws_download_manager.GPMDownloadManager.RC_OK:
            response.set_rc(True)
            response.set_download_id(rc['download_id'])
            response.set_download_size(rc['download_size'])
            response.set_download_checksum(rc['download_checksum'])
        else:
            response.set_rc(False)
            response.set_rc_message('gpm_id %s not found on admin server' % gpm_id)

        return response

    def DownloadGPMGetChunk(self, request):
        download_id = request.get_download_id
        download_chunk_size = request.get_download_chunk_size

        rc = self._gpm_download_manager.get_gpm_download_chunk(download_id, download_chunk_size)

        response = RestApi.Admin.DownloadGPMGetChunkResponse()

        if rc['rc'] == admin_ws_download_manager.GPMDownloadManager.RC_OK:
            response.set_rc(True)
            response.set_download_chunk(Base64String()(rc['download_chunk']))
            response.set_more(rc['more'])
        else:
            response.set_rc(False)
            response.set_rc_message('Invalid download_id %d' % download_id)
            response.set_more(False)
        return response

    def DownloadGPMStop(self, request):
        download_id = request.get_download_id

        self._gpm_download_manager.stop_gpm_download(download_id)
        response = RestApi.Admin.DownloadGPMStopResponse()
        response.set_rc(True)
        return response

    def GenerateGPMSignature(self, request):
        update_challenge = request.get_update_challenge
        gpm_ids = request.get_gpm_ids

        gpm_meta_temp_folder = tempfile.mkdtemp()
        gpm_error_handler = lib.gpm.gpm_env.GpmErrorHandler()
        gpm_ids_str = []
        for gpm_id in gpm_ids:
            gpm_filename = os.path.join(self.environment.gpms_root, '%s.gpm' % gpm_id)
            lib.gpm.gpm_builder.install_meta(gpm_meta_temp_folder, gpm_filename, gpm_error_handler)
            gpm_ids_str.append(str(gpm_id))
        gpm_meta_signature = lib.hagi.gon_hagi_authentication.generate_gpms_signature(gpm_meta_temp_folder,
                                                                                      gpm_ids_str,
                                                                                      str(update_challenge),
                                                                                      self.environment.server_knownsecret)
        shutil.rmtree(gpm_meta_temp_folder)

        response = RestApi.Admin.GenerateGPMSignatureResponse()
        response.set_gpm_signature(gpm_meta_signature)
        return response


    def PortScan(self, request):
        name = request.get_name
        port_numbers = request.get_port_numbers
        result_list = portscan_api.tcp_portscan(name, port_numbers)

        response = RestApi.Admin.PortScanResponse()
        _values = RestApi.Admin.ConfigTemplate.ValueSelection()
        for result in result_list:
            choice = RestApi.Admin.ConfigTemplate.ValueSelectionChoice()
            choice.set_value(result[0])
            choice.set_title(result[1])
            _values.get_selection_choice.append(choice)
        response.set_selection_values(_values)

        return response

    def GetEntityTypeForPluginType(self, request):
        plugin_name = request.get_plugin_name
        element_type = request.get_element_type
        result = rule_api.get_element_type_for_plugin_type(plugin_name, element_type)

        response = RestApi.Admin.GetEntityTypeForPluginTypeResponse()
        response.set_entity_type(result)

        return response

    def _create_client_menu_item(self, item):
        menu_item = RestApi.Admin.MenuItem()
        menu_item.set_id(item.get("id"))
        menu_item.set_label(item.get("label"))
        menu_type = item.get("menu_type")
        max_items = item.get("max_items", 0)
        if max_items:
            menu_item.set_max_items(max_items)

        if menu_type == "folder":
            menu_item.set_item_type("menu_folder")
        elif menu_type == "item":
            menu_item.set_item_type("menu_item")
        elif menu_type == "auto_folder":
            menu_item.set_item_type("auto_menu")
        elif menu_type == "auto_item":
            menu_item.set_item_type("auto_item")
        else:
            raise Exception("Unknown menu item type '%s'" % menu_type)
        return menu_item

    def GetMenuItems(self, request):
        response = RestApi.Admin.GetMenuItemsResponse()

        items = self._dialog_admin.get_menu_items()
        for item in items:
            menu_item = self._create_client_menu_item(item)
            response.get_items.append(menu_item)

        return response


    def _generate_random_menu1(self, root, items):

        n = len(items)

        children = random.randint(0, n)
        child_list = []
        for i in range(0, children):
            element_index = random.randint(0, len(items)-1)
            element = items.pop(element_index)
            child_list.append(element)

        for child in child_list:
            if child.get("menu_type")=="folder":
                self._generate_random_menu1(child, items)

        root["children"] = child_list

    def _generate_random_menu(self, items):


        for item in items:
            if item.get("id") == "-1":
                root = item
                break
        items.remove(root)

        self._generate_random_menu1(root, items)

        return root

    def create_menu_items(self, item):
        menu_items = []
        menu_item = RestApi.Admin.MenuItemRef()
        menu_items.append(menu_item)
        menu_item.set_id(item.get("id"))
        for child in item.get("children", []):
            child_items = self.create_menu_items(child)
            menu_item.get_children.append(child.get("id"))
            menu_items.extend(child_items)
        return menu_items

    def create_random_menu(self, root):
        menu_item_ref = RestApi.Admin.Menu()
        menu_item_ref.set_root_id(root.get("id"))
        menu_item_ref.set_menu_items(self.create_menu_items(root))
        return menu_item_ref

    def create_menu(self, root_id, menu):
        menu_item_ref = RestApi.Admin.Menu()
        menu_item_ref.set_root_id(root_id)
        menu_items = []
        for item in menu:
            menu_item = RestApi.Admin.MenuItemRef()
            menu_items.append(menu_item)
            menu_item.set_id(item.get("element_id"))
            for child in item.get("child_list", []):
                menu_item.get_children.append(child.get("element_id"))

        menu_item_ref.set_menu_items(menu_items)

        return menu_item_ref


    def GetMenu(self, request):
        self._dialog_admin.rebuild_menu()
        root_id, menu = self._dialog_admin.get_current_menu()

#        items = self._dialog_admin.get_menu_items()
#        root = self._generate_random_menu(items)

        response = RestApi.Admin.GetMenuResponse()

#        response._content._menu = self.create_random_menu(root)
#        self.rnd_menu = response._content._menu

        response.set_menu(self.create_menu(root_id, menu))

        return response

    def menu_item_to_dict(self, menu_item):
        item = dict()
        item["id"] = menu_item.get_id
        item["label"] = menu_item.get_label
        item_type = menu_item.get_item_type
        if item_type=="menu_folder":
            item["menu_type"] = "folder"
        elif item_type=="menu_item":
            item["menu_type"] = "item"
        elif item_type=="auto_menu":
            item["menu_type"] = "auto_folder"
        elif item_type=="auto_item":
            item["menu_type"] = "auto_item"

        return item

    def AddItemToMenu(self, request):
        item = self.menu_item_to_dict(request.get_item)
        menu_item = self.menu_item_to_dict(request.get_menu_item)

        root_id, menu = self._dialog_admin.add_item_to_tag(item, menu_item)

        response = RestApi.Admin.AddItemToMenuResponse()
        response.set_menu(self.create_menu(root_id, menu))

        return response

    def AddElementToMenu(self, request):
        element = self._create_server_element(request.get_element)
        to_item = self.menu_item_to_dict(request.get_menu_item)

        menu_item = self._dialog_admin.add_element_to_tag(element, to_item)
        new_item = self._create_client_menu_item(menu_item)
        root_id, menu = self._dialog_admin.get_current_menu()

        response = RestApi.Admin.AddElementToMenuResponse()
        response.set_menu(self.create_menu(root_id, menu))
        response.set_new_item(new_item)

        return response


    def MoveItemToMenu(self, request):
        item = self.menu_item_to_dict(request.get_item)
        to_item = self.menu_item_to_dict(request.get_to_menu_item)
        from_item = self.menu_item_to_dict(request.get_from_menu_item)

        root_id, menu = self._dialog_admin.move_item_to_tag(item, from_item, to_item)

        response = RestApi.Admin.MoveItemToMenuResponse()
        response.set_menu(self.create_menu(root_id, menu))

        return response

    def RemoveItemFromMenu(self, request):

        item = self.menu_item_to_dict(request.get_item)
        menu_item = self.menu_item_to_dict(request.get_menu_item)

        root_id, menu = self._dialog_admin.remove_item_from_tag(item, menu_item)

#        items = self._dialog_admin.get_menu_items()
#        root = self._generate_random_menu(items)
#        for menu_ref in self.rnd_menu._menu_items:
#            if menu_ref._id == menu_item._id:
#                menu_ref._children.remove(item._id)
#                break

        response = RestApi.Admin.RemoveItemFromMenuResponse()
        response.set_menu(self.create_menu(root_id, menu))
        return response

    def GOnServiceGetList(self, request):
        gateway_access = self.get_standard_access(self._auth_admin.STANDARD_ACCESS_GATEWAY_CONFIG)

        response = RestApi.Admin.GOnServiceGetListResponse()
        if self.service_management is None:
            return response

        service_infos = self.service_management.get_remote_service_infos()
        for service_info in service_infos:
            response_service = RestApi.Admin.GOnService()
            response_service.set_sid(service_info.sid)
            response_service.set_title(service_info.title if service_info.title else "N/A")
            response_service.set_ip(service_info.ip)
            response_service.set_port(service_info.port)
            response_service.set_status(service_info.get_status_as_string())
            response_service.set_session_count(components.access_log.server_management.access_log.get_number_of_sessions(service_info.sid))
            response.get_gon_services.append(response_service)

        response.set_update_enabled(gateway_access.update)
#        import time
#        time.sleep(3)

        return response

    def GOnServiceRestart(self, request):
        access_right = self.get_standard_access(self._auth_admin.STANDARD_ACCESS_GATEWAY_CONFIG)
        if not access_right.update:
            self.raise_no_access_exception(self._auth_admin.STANDARD_ACCESS_GATEWAY_CONFIG, AuthAdmin.ACCESS_TYPE_STANDARD, "update")

        sid = request.get_sid
        when_no_sessions = request.get_when_no_sessions
        rc = self.service_management.service_restart(sid, when_no_sessions)
        response = RestApi.Admin.GOnServiceRestartResponse()
        response.set_rc(rc)
        return response

    def GOnServiceStop(self, request):
        access_right = self.get_standard_access(self._auth_admin.STANDARD_ACCESS_GATEWAY_CONFIG)
        if not access_right.update:
            self.raise_no_access_exception(self._auth_admin.STANDARD_ACCESS_GATEWAY_CONFIG, AuthAdmin.ACCESS_TYPE_STANDARD, "update")

        sid = request.get_sid
        when_no_sessions = request.get_when_no_sessions
        rc = self.service_management.service_stop(sid, when_no_sessions)
        response = RestApi.Admin.GOnServiceStopResponse()
        response.set_rc(rc)
        return response

    def _create_client_session_info(self, session):
        response_session = RestApi.Admin.GOnSession()
        sid = session.get("server_sid")
        if not self._server_title_map.has_key(sid):
            return None
        title = self._server_title_map.get(sid)
        response_session.set_server_sid(sid)
        response_session.set_unique_session_id(session.get("unique_session_id"))
        response_session.set_server_title(title)
        response_session.set_user_id(session.get("user_id"))
        response_session.set_user_login(session.get("user_login"))
        response_session.set_user_name(session.get("user_name"))
        response_session.set_session_start(commongon.datetime2str(session.get("start_ts")))
        response_session.set_client_ip(session.get("client_ip"))
        return response_session

    def _update_gateway_title_map(self):
        service_infos = self.service_management.get_remote_service_infos()
        server_info_dict = dict([(service.sid, service.title) for service in service_infos if service.sid])
        self._server_title_map.update(server_info_dict)


    def GetGatewaySessions(self, request):
        gateway_access = self.get_standard_access(self._auth_admin.STANDARD_ACCESS_GATEWAY_CONFIG)

        search_filter = request.get_search_filter
        server_sid = request.get_server_sid
        sessions = components.access_log.server_management.access_log.get_online_user_sessions(server_sid)
        session_info = self._user_admin.get_user_info(sessions, search_filter)
        response = RestApi.Admin.GetGatewaySessionsResponse()

        self._update_gateway_title_map()

        index = 0
        count = 50
        while session_info and index<count:
            r = session_info.pop()
            response_session = self._create_client_session_info(r)
            if response_session:
                response.get_gon_sessions.append(response_session)
                index += 1;
        if session_info:
            element_session_id = str(self.generator_count)
            self.generator_count +=1
            self.generators[element_session_id] = (index+1, session_info)
            response.set_next_session_id(element_session_id)

#        import time
#        time.sleep(5)

        response.set_update_enabled(gateway_access.update)
        return response

    def GetGatewaySessionsNext(self, request):
        self.get_standard_access(self._auth_admin.STANDARD_ACCESS_GATEWAY_CONFIG)

        element_session_id = request.get_next_session_id
        if not self.generators.has_key(element_session_id):
            raise Exception("Unknown next_session_id : '%s'" % element_session_id)
        prev_fetched, session_info = self.generators[element_session_id]

        response = RestApi.Admin.GetGatewaySessionsNextResponse()

        index = 0
        count = 50
        while session_info and index<count:
            r = session_info.pop()
            response_session = self._create_client_session_info(r)
            if response_session:
                response.get_gon_sessions.append(response_session)
                index += 1;
        if session_info:
            self.generators[element_session_id] = (prev_fetched + index+1, session_info)
            response.set_next_session_id(element_session_id)
        else:
            response.set_next_session_id(None)
            self.generators.pop(element_session_id)


        #print elements_to_return
        return response

    def GatewayIsUserOnline(self, request):
        self.get_standard_access(self._auth_admin.STANDARD_ACCESS_GATEWAY_CONFIG)

        user_plugin = request.get_user_plugin
        external_user_id = request.get_external_user_id
        (rc, server_sid, unique_session_id) = components.access_log.server_management.access_log.is_user_online(user_plugin, external_user_id)
        response = RestApi.Admin.GatewayIsUserOnlineResponse()
        response.set_user_is_online(rc)
        if rc:
            response.set_sid(server_sid)
            response.set_unique_session_id(unique_session_id)
        return response

    def GatewaySessionClose(self, request):
        access_right = self.get_standard_access(self._auth_admin.STANDARD_ACCESS_GATEWAY_CONFIG)
        if not access_right.update:
            self.raise_no_access_exception(self._auth_admin.STANDARD_ACCESS_GATEWAY_CONFIG, AuthAdmin.ACCESS_TYPE_STANDARD, "update")

        sid = request.get_sid
        unique_session_id = request.get_unique_session_id
        rc = self.service_management.session_close(sid, unique_session_id)
        response = RestApi.Admin.GatewaySessionCloseResponse()
        response.set_rc(rc)
        return response

    def GatewaySessionRecalculateMenu(self, request):
        access_right = self.get_standard_access(self._auth_admin.STANDARD_ACCESS_GATEWAY_CONFIG)
        if not access_right.update:
            self.raise_no_access_exception(self._auth_admin.STANDARD_ACCESS_GATEWAY_CONFIG, AuthAdmin.ACCESS_TYPE_STANDARD, "update")

        sid = request.get_sid
        unique_session_id = request.get_unique_session_id
        rc = self.service_management.session_recalculate_menu(sid, unique_session_id)
        response = RestApi.Admin.GatewaySessionRecalculateMenuResponse()
        response.set_rc(rc)
        return response


    def GetElementsStop(self, request):
        element_session_id = request.get_next_session_id
        response = RestApi.Admin.GetElementsStopResponse()
        if not self.generators.has_key(element_session_id):
            response.set_rc(False)
        else:
            response.set_rc(True)
            self.generators.pop(element_session_id)

        return response


    def _create_license_count_item(self, item_name, licensed_items, actual_items):
        item = RestApi.Admin.LicenseCount()
        item.set_item_name(item_name)
        item.set_licensed_items(licensed_items)
        item.set_actual_items(actual_items)
        return item

    def _add_action_type_count_if(self, license, name, count_items):
        count = license.get_int(name, default=-1)
        if count >= 0:
            actual = self._traffic_admin.get_launch_type_launches(name)
            count_items.append(self._create_license_count_item(name, count, actual))


    def GetLicenseInfo(self):
        access_right = self.get_standard_access(self._auth_admin.STANDARD_ACCESS_GATEWAY_CONFIG, raise_if_not_read=False)
        edit_access = access_right.update if access_right else False

        response = RestApi.Admin.GetLicenseInfoResponse()
        license_info = None

        license = self._license_handler.get_license()
        if len(license.errors) > 0:
            self.environment.checkpoint_handler.CheckpointMultilineMessages("GetLicenseInf.error", "admin_ws_session", lib.checkpoint.ERROR, license.errors)
        if len(license.warnings) > 0:
            self.environment.checkpoint_handler.CheckpointMultilineMessages("GetLicenseInfo.warning", "admin_ws_session", lib.checkpoint.WARNING, license.warnings)

        if license.valid:
            license_info = RestApi.Admin.LicenseInfoTypeManagement()
            license_info.set_licensed_to(license.get_one('Licensed To'))
            license_info.set_license_number(license.get_one('License Number'))
            license_info.set_license_file_number(license.get_one('License File Number'))
            license_expires = license.get_date('License Expiration Date')
            if license_expires:
                license_info.set_license_expires(commongon.date2str(license_expires))
            maintenance_expires = license.get_date('Maintenance Expiration Date')
            if maintenance_expires:
                license_info.set_maintenance_expires(commongon.date2str(license_expires))

#            license_info.set_element_licensed_menu_items(license.get_int('Number of Menu Items'))
#            license_info.set_element_licensed_tokens(license.get_int('Number of Tokens'))
#            license_info.set_element_licensed_users(license.get_int('Number of Users'))
#
#            license_info.set_element_actual_menu_items(self._traffic_admin.get_number_of_menu_actions())
#            license_info.set_element_actual_tokens(plugin_types.common.plugin_type_token.get_number_of_registered_tokens())
#            license_info.set_element_actual_users(self._user_admin.get_number_of_registered_users())

            count_items = []
            count_items.append(self._create_license_count_item("Number of Users", license.get_int('Number of Users'), self._user_admin.get_number_of_registered_users()))
            count_items.append(self._create_license_count_item("Number of Tokens", license.get_int('Number of Tokens'), plugin_types.common.plugin_type_token.get_number_of_registered_tokens()))
            count_items.append(self._create_license_count_item("Number of Menu Actions", license.get_int('Number of Menu Items'), self._traffic_admin.get_number_of_menu_actions()))

            for name in traffic_admin.launch_type_category_license_count_name.values():
                self._add_action_type_count_if(license, name, count_items)

            license_info.set_count_type_license(count_items)

            status_ok = True
            for item in count_items:
                licensed = item.get_licensed_items
                actual = item.get_actual_items
                if licensed < actual:
                    status_ok = False
                    break
#
#            if (license_info.get_element_licensed_menu_items() < license_info.get_element_actual_menu_items() or
#                license_info.get_element_licensed_tokens() < license_info.get_element_actual_tokens() or
#                license_info.get_element_licensed_users() < license_info.get_element_actual_users()):
#                status_ok = False

            license_info.set_license_status_ok(status_ok)
            license_info.set_update_license_allowed(edit_access)

            license_info.set_license_file_content(license.to_string())

        response.set_license_info(license_info)
        return response

    def SetLicense(self, request):
        access_right = self.get_standard_access(self._auth_admin.STANDARD_ACCESS_GATEWAY_CONFIG)
        if not access_right.update:
            self.raise_no_access_exception(self._auth_admin.STANDARD_ACCESS_GATEWAY_CONFIG, AuthAdmin.ACCESS_TYPE_STANDARD, "update")

        license = request.get_license

        rc = self._license_handler.set_license(license)

        self.create_log( type="Update",
                         summary="Set License",
                         details=license,
                         access_right=self._auth_admin.STANDARD_ACCESS_GATEWAY_CONFIG)

        response = RestApi.Admin.SetLicenseResponse()
        response.set_rc(rc)
        return response

    def report_data_build_args(self, request_content):
        args = {}
        arg_int_01 = request_content.get_arg_int_01
        if arg_int_01 is not None:
            args['arg_int_01'] = int(arg_int_01)
        arg_int_02 = request_content.get_arg_int_02
        if arg_int_02 is not None:
            args['arg_int_02'] = int(arg_int_02)
        arg_int_03 = request_content.get_arg_int_03
        if arg_int_03 is not None:
            args['arg_int_03'] = int(arg_int_03)
        arg_int_04 = request_content.get_arg_int_04
        if arg_int_04 is not None:
            args['arg_int_04'] = int(arg_int_04)
        arg_int_05 = request_content.get_arg_int_05
        if arg_int_05 is not None:
            args['arg_int_05'] = int(arg_int_05)

        arg_string_01 = request_content.get_arg_string_01
        if arg_string_01 is not None:
            args['arg_string_01'] = arg_string_01
        arg_string_02 = request_content.get_arg_string_02
        if arg_string_02 is not None:
            args['arg_string_02'] = arg_string_02
        arg_string_03 = request_content.get_arg_string_03
        if arg_string_03 is not None:
            args['arg_string_03'] = arg_string_03
        arg_string_04 = request_content.get_arg_string_04
        if arg_string_04 is not None:
            args['arg_string_04'] = arg_string_04
        arg_string_05 = request_content.get_arg_string_05
        if arg_string_05 is not None:
            args['arg_string_05'] = arg_string_05

        arg_boolean_01 = request_content.get_arg_boolean_01
        if arg_boolean_01 is not None:
            args['arg_boolean_01'] = bool(arg_boolean_01)
        arg_boolean_02 = request_content.get_arg_boolean_02
        if arg_boolean_02 is not None:
            args['arg_boolean_02'] = bool(arg_boolean_02)
        arg_boolean_03 = request_content.get_arg_boolean_03
        if arg_boolean_03 is not None:
            args['arg_boolean_03'] = bool(arg_boolean_03)
        arg_boolean_04 = request_content.get_arg_boolean_04
        if arg_boolean_04 is not None:
            args['arg_boolean_04'] = bool(arg_boolean_04)
        arg_boolean_05 = request_content.get_arg_boolean_05
        if arg_boolean_05 is not None:
            args['arg_boolean_05'] = bool(arg_boolean_05)
        return args



def convert_config_column_type_to_ws(column_type):
    if column_type == plugin_types.server_management.plugin_type_config.Column.TYPE_STRING:
        return 'STRING'
    elif column_type == plugin_types.server_management.plugin_type_config.Column.TYPE_INTEGER:
        return 'INTEGER'
    elif column_type == plugin_types.server_management.plugin_type_config.Column.TYPE_BOOLEAN:
        return 'BOOLEAN'
    elif column_type == plugin_types.server_management.plugin_type_config.Column.TYPE_TEXT:
        return 'TEXT'
    return 'STRING'

# def convert_config_row_values_to_ws(row):
#     result = []
#     if row == None:
#         return result
#
#     for (name,value) in row.items():
#         ws_value = ns0.ConfigElementRowType_Def('values')
#         ws_value._value = value
#         ws_value._name = name
#         ws_value._is_null = value is None
#         result.append(ws_value)
#     return result

def convert_config_ws_to_row_values(ws_row):
    result = dict()
    for ws_value in ws_row:
        if ws_value._is_null:
            value=None
        elif ws_value._value is None:
            value = ""
        else:
            value = ws_value._value

        result[ws_value._name] = value
    return result


class ServerElementType(ElementType):

    def __init__(self, element_type):
        self.element_type = element_type
        self.entity_type = None

    def get_id(self):
        return self.element_type.get_id

    def get_entity_type(self):
        if self.entity_type:
            return self.entity_type
        return self.element_type.get_entity_type

    def get_info(self):
        return self.element_type.get_info

    def get_label(self):
        return self.element_type.get_label

    def get_element_type(self):
        return self.element_type.get_element_type


class ServerRuleType(RuleType):

    def __init__(self, rule_type):
        self.rule_type = rule_type
        self.result_element = None
        self.conditions = None

    def get_id(self):
        return self.rule_type.get_id

    def get_result_element(self):
        if not self.result_element:
            self.result_element = ServerElementType(self.rule_type.get_result_element)
        return self.result_element

    def get_condition_elements(self):
        if not self.conditions:
            self.conditions = []
            for e in self.rule_type.get_condition_elements:
                condition = ServerElementType(e)
                entity_type = e.get_entity_type
                if not "." in entity_type:
                    rule_entity_type = rule_api.get_rule_entity_type(entity_type, self.get_result_element().get_entity_type())
                    condition.entity_type = "%s.%s" % (rule_entity_type, entity_type)
                self.conditions.append(condition)
        return self.conditions

    def is_active(self):
        return self.rule_type.get_active
