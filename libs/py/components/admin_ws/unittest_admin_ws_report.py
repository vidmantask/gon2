"""
Unittest of admin web-service
"""
from __future__ import with_statement
import unittest
from lib import giri_unittest

import time
import base64
import hashlib
import httplib

import components.admin_ws.ws
import components.admin_ws.admin_ws_service


class AdminWSTest(unittest.TestCase):

    def setUp(self):
        self.dev_installation = giri_unittest.UnittestDevEnvInstallation.create()
        self.dev_installation.generate_gpms()
        self.dev_installation.generate_setupdata()
        self._admin_service_host = self.dev_installation.config_server_management.management_ws_ip
        self._admin_service_port = self.dev_installation.config_server_management.management_ws_port
        self._db_url = self.dev_installation.get_db_connect_url()

        self._folder_gpms = self.dev_installation.get_gpms_folder()
        self._folder_gpmcdefs = self.dev_installation.get_gpmcdefs_folder()
        plugin_folder = giri_unittest.PY_ROOT

        self._config_root = self.dev_installation.get_config_folder()
        self._admin_service = components.admin_ws.admin_ws_service.AdminWSService.run_default(giri_unittest.get_checkpoint_handler(), self._db_url, self._admin_service_host, self._admin_service_port, plugin_folder, self._config_root, None, config=self.dev_installation.config_server_management)

    def tearDown(self):
        time.sleep(1)
        self._admin_service.stop()
        self._admin_service.join()

    def _login(self, gon_server_management):
        request = components.admin_ws.ws.admin_ws_services.OpenSessionRequest()
        request_content = components.admin_ws.ws.admin_ws_services.ns0.OpenSessionRequestType_Def(None).pyclass()
        request.set_element_content(request_content)
        response = gon_server_management.OpenSession(request)
        return response.get_element_content().get_element_session_id()

    def _logout(self, gon_server_management, session_id):
        request = components.admin_ws.ws.admin_ws_services.CloseSessionRequest()
        request._content = components.admin_ws.ws.admin_ws_services.ns0.CloseSessionRequestType_Def(None)
        request._content._session_id = session_id
        response = gon_server_management.CloseSession(request)
        rc = response.get_element_content().get_element_rc()
        print rc
        self.assertTrue(rc)


    def test_get_report_data(self):
        time.sleep(5)
        url = "http://%s:%d" % (self._admin_service_host, self._admin_service_port)
        transdict = None
        tracefile = None

#        tracefile = open('/home/thwang/source/main2/tracefile', 'w')
#
#        url = "https://%s:%d" % (self._admin_service_host, self._admin_service_port)
#        transdict = {
#                     'key_file'  : '/home/thwang/source/main2/setup/key_store/https_server.key',
#                     'cert_file' : '/home/thwang/source/main2/setup/key_store/https_server.crt'
#                     }
        gon_server_management = components.admin_ws.ws.admin_ws_services.BindingSOAP(url, transdict=transdict, tracefile=tracefile)
        session_id = self._login(gon_server_management)

        request = components.admin_ws.ws.admin_ws_services.GetReportDataRequest()
        request_content = components.admin_ws.ws.admin_ws_services.ns0.GetReportDataRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request_content.set_element_module_name(u'al')
        request_content.set_element_data_id(210)
        """ request_content.set_element_arg_string_01("20100806T085044.375000_988c1c5e-9f0b-11df-8669-00221908b67b_0") """
        request.set_element_content(request_content)
        response = gon_server_management.GetReportData(request)

        for row in response.get_element_content().get_element_rows():
            print ", ".join(row.get_element_values())

        self._logout(gon_server_management, session_id)



#
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
