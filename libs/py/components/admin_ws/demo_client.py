from ZSI import ServiceProxy

import ws.admin_ws_services
import base64
import lib.utility

def main():
#    gon_server_management = ws.AdminWS_services.BindingSOAP("http://127.0.0.1:8080", tracefile=sys.stdout, )
    gon_server_management = ws.admin_ws_services.BindingSOAP("http://127.0.0.1:8080")

    request = ws.admin_ws_services.LoginRequest()
    request._content = ws.admin_ws_services.ns0.LoginRequestType_Def('content')
    request._content._username = "hej"
    request._content._password = "davs"
    response = gon_server_management.Login(request)
    session_id = response._content._session_id


#    request = ws.AdminWS_services.GetModuleElementsRequest()
#    request._content = ws.AdminWS_services.ns0.GetModuleElementsRequestType_Def('content')
#    request._content._session_id = session_id
#    request._content._element_type = 'key'
#    request._content._module_name  = 'key'
#    response = gon_server_management.GetModuleElements(request)
#    print "get_module_elements :"
#    for element in response._content._elements:
#        print "------------------"
#        print " id:", element._id
#
#    request = ws.AdminWS_services.GetRulesRequest()
#    request._content = ws.AdminWS_services.ns0.GetRulesRequestType_Def('content')
#    request._content._session_id = session_id
#    request._content._class_name = "hej_rule"
#    response = gon_server_management.GetRules(request)   
#    print response._content
#
#    request = ws.AdminWS_services.CreateRuleRequest()
#    request._content = ws.AdminWS_services.ns0.CreateRuleRequestType_Def('content')
#    request._content._session_id = session_id
#    request._content._rule = ws.AdminWS_services.ns0.RuleType_Def('rule')
#    request._content._rule._id = 43
#    request._content._rule._result_element = ws.AdminWS_services.ns0.ElementType_Def('result_element')
#    request._content._rule._result_element._id = "a"
#    request._content._rule._result_element._entity_type = "b"
#    request._content._rule._result_element._info = "c"
#    request._content._rule._result_element._label = "d"
#    request._content._rule._condition_elements = []
#    response = gon_server_management.CreateRule(request)
#    print response._content
#

#    request = ws.AdminWS_services.GetReportsRequest()
#    request._content = ws.AdminWS_services.ns0.GetReportsRequestType_Def('content')
#    request._content._session_id = session_id
#    response = gon_server_management.GetReports(request)
#    print response._content
#    
#
#    request = ws.AdminWS_services.GetReportSpecRequest()
#    request._content = ws.AdminWS_services.ns0.GetReportSpecRequestType_Def('content')
#    request._content._session_id = session_id
#    request._content._module_name = 'al'
#    request._content._report_id = '1'
#    response = gon_server_management.GetReportSpec(request)
#    print response._content
#
#
#    request = ws.AdminWS_services.GetReportDataRequest()
#    request._content = ws.AdminWS_services.ns0.GetReportDataRequestType_Def('content')
#    request._content._session_id = session_id
#    request._content._module_name = 'al'
#    request._content._data_id = '1'
#    response = gon_server_management.GetReportData(request)
#    print response._content

#
#
#    request = ws.AdminWS_services.GetConfigElementMetaRequest()
#    request._content = ws.AdminWS_services.ns0.GetConfigElementMetaRequestType_Def('content')
#    request._content._session_id = session_id
#    request._content._plugin_instance_name = 'soft_token'
#    request._content._config_element_name  = 'key_extra'
#    response = gon_server_management.GetConfigElementMeta(request)
#    print response
#    for col in response._content._columns:
#        print col._name, col._type, col._label
#
#
#
#    request = ws.AdminWS_services.CreateConfigElementRowRequest()
#    request._content = ws.AdminWS_services.ns0.CreateConfigElementRowRequestType_Def('content')
#    request._content._session_id = session_id
#    request._content._plugin_instance_name = 'soft_token'
#    request._content._config_element_name  = 'key_extra'
#    request._content._values = convert_config_row_values_to_ws([ u'42', u'00042', u'My public key for 42'])
#    
#    response = gon_server_management.CreateConfigElementRow(request)
#    for value in response._content._values:
#        print value._value
#                                                               
#
#    request = ws.AdminWS_services.UpdateConfigElementRowRequest()
#    request._content = ws.AdminWS_services.ns0.UpdateConfigElementRowRequestType_Def('content')
#    request._content._session_id = session_id
#    request._content._plugin_instance_name = 'soft_token'
#    request._content._config_element_name  = 'key_extra'
#    request._content._values = convert_config_row_values_to_ws([ u'42', u'00042', u'My public key for 42 now updated'])
#    
#    response = gon_server_management.UpdateConfigElementRow(request)
#    for value in response._content._values:
#        print value._value
#                                                               
#
#
#    print "---------------------------"
#
#
#    request = ws.AdminWS_services.GetConfigElementRowRequest()
#    request._content = ws.AdminWS_services.ns0.GetConfigElementRowRequestType_Def('content')
#    request._content._session_id = session_id
#    request._content._plugin_instance_name = 'soft_token'
#    request._content._config_element_name  = 'key_extra'
#    request._content._values = convert_config_row_values_to_ws([u'42'])
#    
#    response = gon_server_management.GetConfigElementRow(request)
#    for value in response._content._values:
#        print value._value
#
#
#
#    request = ws.AdminWS_services.DeleteConfigElementRowRequest()
#    request._content = ws.AdminWS_services.ns0.DeleteConfigElementRowRequestType_Def('content')
#    request._content._session_id = session_id
#    request._content._plugin_instance_name = 'soft_token'
#    request._content._config_element_name  = 'key_extra'
#    request._content._values = convert_config_row_values_to_ws([u'43'])
#    
#    response = gon_server_management.DeleteConfigElementRow(request)
#    print response._content._rc



    request = ws.admin_ws_services.DownloadGPMStartRequest()
    request._content = ws.admin_ws_services.ns0.DownloadGPMStartRequestType_Def(None).pyclass()
    request._content.set_element_session_id(session_id)
    request._content.set_element_gpm_id('a-1-1-win')
    request.set_element_content(request._content)
    response = gon_server_management.DownloadGPMStart(request)
    rc = response.get_element_content().get_element_rc()
    if rc:
        download_id = response.get_element_content().get_element_download_id()
        download_size = response.get_element_content().get_element_download_size()
        download_checksum = response.get_element_content().get_element_download_checksum()
        print download_id, download_size, download_checksum

        my_file = open('myfile', 'wb')
    else:
        print response.get_element_content().get_element_rc_message()

    more = rc
    while(more):
        request = ws.admin_ws_services.DownloadGPMGetChunkRequest()
        request._content = ws.admin_ws_services.ns0.DownloadGPMGetChunkRequestType_Def(None).pyclass()
        request._content.set_element_session_id(session_id)
        request._content.set_element_download_id(download_id)
        request._content.set_element_download_chunk_size(10000)
        request.set_element_content(request._content)
        response = gon_server_management.DownloadGPMGetChunk(request)
        rc = response.get_element_content().get_element_rc()
        if rc:
            download_chunk = response.get_element_content().get_element_download_chunk()
            my_file.write(base64.standard_b64decode(download_chunk))
            more = response.get_element_content().get_element_more()
            print more, len(download_chunk)
        else:
            print response.get_element_content().get_element_rc_message()
            more = False
        
    my_file.close()
    print lib.utility.calculate_checksum_for_file('myfile')



    request = ws.admin_ws_services.GetGPMCollectionsRequest()
    request._content = ws.admin_ws_services.ns0.GetGPMCollectionsRequestType_Def(None).pyclass()
    request._content.set_element_session_id(session_id)
    response = gon_server_management.GetGPMCollections(request)
    print len(response.get_element_content().get_element_gpm_collections())
    for collection in response.get_element_content().get_element_gpm_collections():
        print "Collection id: ", collection.get_element_gpm_collection_id()
        print "Collection summary: ", collection.get_element_gpm_collection_summary()
        




    request = ws.admin_ws_services.DownloadGPMStopRequest()
    request._content = ws.admin_ws_services.ns0.DownloadGPMStopRequestType_Def(None).pyclass()
    request._content.set_element_session_id(session_id)
    request._content.set_element_download_id(download_id)
    request.set_element_content(request._content)
    response = gon_server_management.DownloadGPMStop(request)
    print response.get_element_content().get_element_rc()

        

    request = ws.admin_ws_services.LogoutRequest()
    request._content = ws.admin_ws_services.ns0.LogoutRequestType_Def('content')
    request._content._session_id = session_id
    response = gon_server_management.Logout(request)
    print "logout rc :", response._content._rc
    

def convert_config_row_values_to_ws(row):
    result = []
    for value in row:
        ws_value = ws.admin_ws_services.ns0.ConfigElementRowType_Def('values')
        ws_value._value = value.__str__()
        result.append(ws_value)
    return result

def convert_config_ws_to_row_values(ws_row):
    result = []
    for ws_value in ws_row:
        result.append(ws_value._value)    
    return result


if __name__ == '__main__':
    main()
