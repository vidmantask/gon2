"""
Unittest of admin web-service
"""
from __future__ import with_statement
import unittest
from lib import giri_unittest

import time
import hashlib


import components.admin_ws.ws
import components.admin_ws.admin_ws_service
import tempfile


class AdminWSTest(unittest.TestCase):

    def setUp(self):
        self.dev_installation = giri_unittest.UnittestDevEnvInstallation.create()
        self.dev_installation.generate_gpms()
        self.dev_installation.generate_setupdata()

        self._admin_service_host = self.dev_installation.config_server_management.management_ws_ip
        self._admin_service_port = self.dev_installation.config_server_management.management_ws_port
        self._db_url = self.dev_installation.get_db_connect_url()
        self._folder_gpms = self.dev_installation.get_gpms_folder()
        self._folder_gpmcdefs = self.dev_installation.get_gpmcdefs_folder()
        plugin_folder = giri_unittest.PY_ROOT
        self._config_root = self.dev_installation.get_config_folder()
        self._admin_service = components.admin_ws.admin_ws_service.AdminWSService.run_default(giri_unittest.get_checkpoint_handler(), self._db_url, self._admin_service_host, self._admin_service_port, plugin_folder, self._config_root, None, config=self.dev_installation.config_server_management )


    def tearDown(self):
        time.sleep(2)
        self._admin_service.stop()

    def _login(self, gon_server_management):
        request = components.admin_ws.ws.admin_ws_services.OpenSessionRequest()
        request_content = components.admin_ws.ws.admin_ws_services.ns0.OpenSessionRequestType_Def(None).pyclass()
        request.set_element_content(request_content)
        response = gon_server_management.OpenSession(request)
        return response.get_element_content().get_element_session_id()

    def _logout(self, gon_server_management, session_id):
        request = components.admin_ws.ws.admin_ws_services.CloseSessionRequest()
        request._content = components.admin_ws.ws.admin_ws_services.ns0.CloseSessionRequestType_Def(None)
        request._content._session_id = session_id
        response = gon_server_management.CloseSession(request)
        rc = response.get_element_content().get_element_rc()
        self.assertTrue(rc)


    def test_download_gpm(self):
        """
        Testing the download of gpm-packages
        """
        time.sleep(1)
        gon_server_management = components.admin_ws.ws.admin_ws_services.BindingSOAP("http://%s:%d" % (self._admin_service_host, self._admin_service_port))
        session_id = self._login(gon_server_management)


        #
        # Testing successfull download
        #
        request = components.admin_ws.ws.admin_ws_services.DownloadGPMStartRequest()
        request_content = components.admin_ws.ws.admin_ws_services.ns0.DownloadGPMStartRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request_content.set_element_gpm_id('a-1-1-win')
        request.set_element_content(request_content)
        response = gon_server_management.DownloadGPMStart(request)
        rc = response.get_element_content().get_element_rc()
        self.assertTrue(rc)

        download_id = response.get_element_content().get_element_download_id()
        download_size = response.get_element_content().get_element_download_size()
        download_checksum = response.get_element_content().get_element_download_checksum()
        download_file = tempfile.TemporaryFile()

        more = rc
        while(more):
            request = components.admin_ws.ws.admin_ws_services.DownloadGPMGetChunkRequest()
            request_content = components.admin_ws.ws.admin_ws_services.ns0.DownloadGPMGetChunkRequestType_Def(None).pyclass()
            request_content.set_element_session_id(session_id)
            request_content.set_element_download_id(download_id)
            request_content.set_element_download_chunk_size(1000)
            request.set_element_content(request_content)
            response = gon_server_management.DownloadGPMGetChunk(request)
            rc = response.get_element_content().get_element_rc()
            if rc:
                download_chunk = response.get_element_content().get_element_download_chunk()
                download_file.write(download_chunk)
                more = response.get_element_content().get_element_more()
            else:
                more = False

        download_file.seek(0)
        download_file_checksum = hashlib.sha1(download_file.read()).hexdigest()
        download_file.close()
        self.assertEqual(download_file_checksum, download_checksum)


        #
        # Testing request for invalid gpm_id
        #
        request = components.admin_ws.ws.admin_ws_services.DownloadGPMStartRequest()
        request_content = components.admin_ws.ws.admin_ws_services.ns0.DownloadGPMStartRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request_content.set_element_gpm_id('a-1-1-win-xxxx')
        request.set_element_content(request_content)
        response = gon_server_management.DownloadGPMStart(request)
        rc = response.get_element_content().get_element_rc()
        self.assertFalse(rc)



        #
        # Testing cancel of download
        #
        request = components.admin_ws.ws.admin_ws_services.DownloadGPMStartRequest()
        request_content = components.admin_ws.ws.admin_ws_services.ns0.DownloadGPMStartRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request_content.set_element_gpm_id('a-1-1-win')
        request.set_element_content(request_content)
        response = gon_server_management.DownloadGPMStart(request)
        rc = response.get_element_content().get_element_rc()
        self.assertTrue(rc)
        download_id = response.get_element_content().get_element_download_id()
        download_size = response.get_element_content().get_element_download_size()
        download_checksum = response.get_element_content().get_element_download_checksum()

        request = components.admin_ws.ws.admin_ws_services.DownloadGPMGetChunkRequest()
        request_content = components.admin_ws.ws.admin_ws_services.ns0.DownloadGPMGetChunkRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request_content.set_element_download_id(download_id)
        request_content.set_element_download_chunk_size(100)
        request.set_element_content(request_content)
        response = gon_server_management.DownloadGPMGetChunk(request)
        rc = response.get_element_content().get_element_rc()
        self.assertTrue(rc)

        request = components.admin_ws.ws.admin_ws_services.DownloadGPMStopRequest()
        request_content = components.admin_ws.ws.admin_ws_services.ns0.DownloadGPMStopRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request_content.set_element_download_id(download_id)
        request.set_element_content(request_content)
        response = gon_server_management.DownloadGPMStop(request)
        rc = response.get_element_content().get_element_rc()
        self.assertTrue(rc)

        request = components.admin_ws.ws.admin_ws_services.DownloadGPMGetChunkRequest()
        request_content = components.admin_ws.ws.admin_ws_services.ns0.DownloadGPMGetChunkRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request_content.set_element_download_id(download_id)
        request_content.set_element_download_chunk_size(100)
        request.set_element_content(request_content)
        response = gon_server_management.DownloadGPMGetChunk(request)
        rc = response.get_element_content().get_element_rc()
        self.assertFalse(rc)

        self._logout(gon_server_management, session_id)



    def test_get_collections(self):
        time.sleep(1)
        gon_server_management = components.admin_ws.ws.admin_ws_services.BindingSOAP("http://%s:%d" % (self._admin_service_host, self._admin_service_port))
        session_id = self._login(gon_server_management)

        request = components.admin_ws.ws.admin_ws_services.GetGPMCollectionsRequest()
        request_content = components.admin_ws.ws.admin_ws_services.ns0.GetGPMCollectionsRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = gon_server_management.GetGPMCollections(request)
        self.assertTrue(len(response.get_element_content().get_element_gpm_collections()) > 0)
        for collection in response.get_element_content().get_element_gpm_collections():
            print "Collection id: ", collection.get_element_gpm_collection_id()
            print "Collection summary: ", collection.get_element_gpm_collection_summary()
            print "Collection description: ", collection.get_element_gpm_collection_description()

            for gpm in collection.get_element_gpm_collection_gpms():
                print "GPM id", gpm.get_element_gpm_id()
                print "GPM summary", gpm.get_element_gpm_summary()
                print "GPM description", gpm.get_element_gpm_description()
                print "GPM version", gpm.get_element_gpm_version()
                print "GPM arch", gpm.get_element_gpm_arch()
                print "GPM checksum", gpm.get_element_gpm_checksum()

        request = components.admin_ws.ws.admin_ws_services.GetGPMCollectionsRequest()
        request_content = components.admin_ws.ws.admin_ws_services.ns0.GetGPMCollectionsRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = gon_server_management.GetGPMCollections(request)
        self.assertTrue(len(response.get_element_content().get_element_gpm_collections()) > 0)
        for collection in response.get_element_content().get_element_gpm_collections():
            print "Collection id: ", collection.get_element_gpm_collection_id()
            print "Collection summary: ", collection.get_element_gpm_collection_summary()
            print "Collection description: ", collection.get_element_gpm_collection_description()
            for gpm in collection.get_element_gpm_collection_gpms():
                print "GPM id", gpm.get_element_gpm_id()
                print "GPM summary", gpm.get_element_gpm_summary()
                print "GPM description", gpm.get_element_gpm_description()
                print "GPM version", gpm.get_element_gpm_version()
                print "GPM arch", gpm.get_element_gpm_arch()
                print "GPM checksum", gpm.get_element_gpm_checksum()

        request = components.admin_ws.ws.admin_ws_services.GetGPMCollectionsRequest()
        request_content = components.admin_ws.ws.admin_ws_services.ns0.GetGPMCollectionsRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = gon_server_management.GetGPMCollections(request)
        self.assertTrue(len(response.get_element_content().get_element_gpm_collections()) > 0)
        for collection in response.get_element_content().get_element_gpm_collections():
            print "Collection id: ", collection.get_element_gpm_collection_id()
            print "Collection summary: ", collection.get_element_gpm_collection_summary()
            print "Collection description: ", collection.get_element_gpm_collection_description()
            for gpm in collection.get_element_gpm_collection_gpms():
                print "GPM id", gpm.get_element_gpm_id()
                print "GPM summary", gpm.get_element_gpm_summary()
                print "GPM description", gpm.get_element_gpm_description()
                print "GPM version", gpm.get_element_gpm_version()
                print "GPM arch", gpm.get_element_gpm_arch()
                print "GPM checksum", gpm.get_element_gpm_checksum()

        self._logout(gon_server_management, session_id)


    def test_get_client_knownsecret(self):
        time.sleep(1)
        gon_server_management = components.admin_ws.ws.admin_ws_services.BindingSOAP("http://%s:%d" % (self._admin_service_host, self._admin_service_port))
        session_id = self._login(gon_server_management)

        request = components.admin_ws.ws.admin_ws_services.DeployGetKnownSecretClientRequest()
        request_content = components.admin_ws.ws.admin_ws_services.ns0.DeployGetKnownSecretClientRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = gon_server_management.DeployGetKnownSecretClient(request)
        knownsecret_client = response.get_element_content().get_element_knownsecret_client()
        self.assertTrue(knownsecret_client != '')

        self._logout(gon_server_management, session_id)

    def test_get_servers(self):
        time.sleep(1)
        gon_server_management = components.admin_ws.ws.admin_ws_services.BindingSOAP("http://%s:%d" % (self._admin_service_host, self._admin_service_port))
        session_id = self._login(gon_server_management)

        request = components.admin_ws.ws.admin_ws_services.DeployGetServersRequest()
        request_content = components.admin_ws.ws.admin_ws_services.ns0.DeployGetServersRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = gon_server_management.DeployGetServers(request)
        servers = response.get_element_content().get_element_servers()
        self.assertTrue(servers != '')

        self._logout(gon_server_management, session_id)


    def test_ping(self):
        time.sleep(1)
        gon_server_management = components.admin_ws.ws.admin_ws_services.BindingSOAP("http://%s:%d" % (self._admin_service_host, self._admin_service_port))

        # Ping when login-ed
        session_id = self._login(gon_server_management)
        request = components.admin_ws.ws.admin_ws_services.PingRequest()
        request_content = components.admin_ws.ws.admin_ws_services.ns0.PingRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = gon_server_management.Ping(request)
        rc = response.get_element_content().get_element_rc()
        self.assertTrue(rc)
        self._logout(gon_server_management, session_id)

        # Ping when not login-ed
        request = components.admin_ws.ws.admin_ws_services.PingRequest()
        request_content = components.admin_ws.ws.admin_ws_services.ns0.PingRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = gon_server_management.Ping(request)
        rc = response.get_element_content().get_element_rc()
        self.assertFalse(rc)


#
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'
GIRI_UNITTEST_TIMEOUT = 60 * 4

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
