from __future__ import with_statement
from components.database.server_common import database_api
from components.database.server_common import schema_api

import lib.version
import datetime

dbapi = database_api.SchemaFactory.get_creator("server_management")


def connect_to_database(environment):
    dbapi.bind(environment.get_default_database())

def connect_to_database_using_connection(db_connection):
    dbapi.bind(db_connection)


table_activity_log = dbapi.create_table("activity_log",
                                   schema_api.Column('user_login', schema_api.String(100)),
                                   schema_api.Column('user_id', schema_api.String(1024)),
                                   schema_api.Column('user_plugin', schema_api.String(256)),
                                   schema_api.Column('user_name', schema_api.String(1024)),
                                   schema_api.Column('internal_user_login', schema_api.Boolean, default=False),
                                   schema_api.Column('access_right', schema_api.String(256)),
                                   schema_api.Column('session_id', schema_api.String(100)),
                                   schema_api.Column('timestamp', schema_api.DateTime, default=datetime.datetime.now(), index=True),
                                   schema_api.Column('type', schema_api.String(100), default="Unknown"),
                                   schema_api.Column('severity', schema_api.String(100)),
                                   schema_api.Column('summary', schema_api.Text, default=""),
                                   schema_api.Column('details', schema_api.Text, default=""),
                                   )

database_api.SchemaFactory.register_creator(dbapi)

class ActivityLog(database_api.PersistentObject):

    def before_commit(self):
        print "Yo - before commit called"

def create_mapper():
    database_api.mapper(ActivityLog, table_activity_log)


create_mapper()
