from ZSI.ServiceContainer import AsServer
from ws.admin_ws_services_server import *
from ws.admin_ws_services_types import *
from ws.admin_ws_services import * 
from ZSI import Fault



class Service(admin_wsService):

    def soap_Login(self, ps):
        self.request = ps.Parse(LoginRequest.typecode)
        print "Login called, request:", self.request._content._username, self.request._content._password 

        response =  LoginResponse()
        response._content = ns2.LoginResponseType_Def('content')
        response._content._session_id = "1"
        return response


        fault_response = ns0.LoginFaultElementType_Dec().pyclass()
        fault_response._error_session_id = "42"

        fault_response_fault = Fault(Fault.Server, "LoginFault", None, fault_response)
        raise fault_response_fault
    

    def soap_Logout(self, ps):
        self.request = ps.Parse(LogoutRequest.typecode)
        return LogoutResponse()

if __name__ == "__main__" :
    AsServer(8080, (Service(),) )
