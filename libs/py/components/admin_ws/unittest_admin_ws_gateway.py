"""
Unittest of admin web-service
"""
from __future__ import with_statement
import unittest
from lib import giri_unittest

import time
import base64
import hashlib
import httplib


import components.admin_ws.ws
import components.admin_ws.admin_ws_service
import tempfile



class AdminWSTest(unittest.TestCase):

    def setUp(self):
        self._admin_service_host = "127.0.0.1"
        self._admin_service_port = 18072
        
    def tearDown(self):
        time.sleep(1)

    def _login(self, gon_server_management):
        request = components.admin_ws.ws.admin_ws_services.OpenSessionRequest()
        request_content = components.admin_ws.ws.admin_ws_services.ns0.OpenSessionRequestType_Def(None).pyclass()
        request.set_element_content(request_content)
        response = gon_server_management.OpenSession(request)
        return response.get_element_content().get_element_session_id()

    def _logout(self, gon_server_management, session_id):
        request = components.admin_ws.ws.admin_ws_services.CloseSessionRequest()
        request._content = components.admin_ws.ws.admin_ws_services.ns0.CloseSessionRequestType_Def(None)
        request._content._session_id = session_id
        response = gon_server_management.CloseSession(request)
        rc = response.get_element_content().get_element_rc()
        self.assertTrue(rc)


    def test_is_user_online(self):
        time.sleep(1)
        gon_server_management = components.admin_ws.ws.admin_ws_services.BindingSOAP("https://%s:%d" % (self._admin_service_host, self._admin_service_port))
        session_id = self._login(gon_server_management)

        request = components.admin_ws.ws.admin_ws_services.GatewayIsUserOnlineRequest()
        request_content = components.admin_ws.ws.admin_ws_services.ns0.GatewayIsUserOnlineRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request_content.set_element_user_plugin('dev_env_user_user')
        request_content.set_element_external_user_id('3')
        request.set_element_content(request_content)
        response = gon_server_management.GatewayIsUserOnline(request)
        
        user_is_online = response.get_element_content().get_element_user_is_online()
        sid = response.get_element_content().get_element_sid()
        unique_session_id = response.get_element_content().get_element_unique_session_id()

        print user_is_online, sid, unique_session_id

        if user_is_online:
            request = components.admin_ws.ws.admin_ws_services.GatewaySessionRecalculateMenuRequest()
            request_content = components.admin_ws.ws.admin_ws_services.ns0.GatewaySessionRecalculateMenuRequestType_Def(None).pyclass()
            request_content.set_element_session_id(session_id)
            request_content.set_element_sid(sid)
            request_content.set_element_unique_session_id(unique_session_id)
            request.set_element_content(request_content)
            response = gon_server_management.GatewaySessionRecalculateMenu(request)
            rc = response.get_element_content().get_element_rc()
            print "GatewaySessionRecalculateMenu", rc

        time.sleep(5)

        if user_is_online:
            request = components.admin_ws.ws.admin_ws_services.GatewaySessionCloseRequest()
            request_content = components.admin_ws.ws.admin_ws_services.ns0.GatewaySessionCloseRequestType_Def(None).pyclass()
            request_content.set_element_session_id(session_id)
            request_content.set_element_sid(sid)
            request_content.set_element_unique_session_id(unique_session_id)
            request.set_element_content(request_content)
            response = gon_server_management.GatewaySessionClose(request)
            rc = response.get_element_content().get_element_rc()
            print "GatewaySessionClose", rc
        
        
        self._logout(gon_server_management, session_id)

#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'
GIRI_UNITTEST_IGNORE = True

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
