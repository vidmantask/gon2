import lib.portscan as portscan

ip_ranges = []
timeout = 0.1
parallel_connections=256
delay=None


def convert_ip_range(ip_range_str):
    result = []
    if ip_range_str:
        for ip_range in ip_range_str.split(","):
            ips = ip_range.split("-")
            if len(ips)==2:
                result.append((ips[0].strip(), ips[1].strip()))
    return result


def _get_portnumbers(ports):
    result = []
    for port in ports:
        try:
            result.append(int(port))
        except ValueError:
            raise Exception("Invalid port number '%s'" % port)
    return result
    
        
        

def tcp_portscan(name, ports):
    result = set()
    if name:
        result.add(portscan.dns_lookup(name))
    for addr in portscan.tcp_portscan(ip_ranges, _get_portnumbers(ports), parallel_connections, timeout, delay):
        result.add(portscan.dns_lookup(addr[0]))
    return result


def tcp_portscan1(ports):
    result = set()
    for addr in portscan.tcp_portscan(ip_ranges, _get_portnumbers(ports), parallel_connections, timeout, delay):
        (dns_name, title) = portscan.dns_lookup(addr[0])
        result.add((dns_name, addr[1]))
    return result
    
    
if __name__ == "__main__":
    lan = ('192.168.45.0', '192.168.45.255')
    devtest = ('172.16.0.0', '172.16.0.255')
    ip_ranges = [lan, devtest]
    print tcp_portscan1([80, 3389])
    
    