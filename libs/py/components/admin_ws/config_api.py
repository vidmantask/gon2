from __future__ import with_statement

import json
from components.rest.rest_api import RestApi
import components.templates.server_common.database_schema as template_schema
import lib.checkpoint
import lib.variable_expansion
import components.auth.server_management.rule_api as rule_api
from components.database.server_common import database_api
from components.plugin.server_common.config_api_lib import ConfigTemplateSpec

from components.auth.server_management import ElementType, OperationNotAllowedException

def template2logstring(template):
    try:
        config_spec = ConfigTemplateSpec(template)
        value_dict =  config_spec.get_field_value_dict()
        lines = []
        for name, value in sorted(value_dict.items()):
            lines.append("%s: '%s'" % (name, value))
        return "\n".join(lines)
    except Exception, e:
        return "Error getting element details:\n\n%s" % repr(e)



def expand_variables(field, parameter_fields):
    def get_replace_string(name, field_name):
        if name=="custom_template":
            for parameter_field in parameter_fields:
                if parameter_field.get_name == field_name:
                    return parameter_field.get_default_value
        return None
    if field.get_default_value:
        return lib.variable_expansion.expand(field.get_default_value, get_replace_string)
    else:
        return field.get_default_value


class ClassElementType(ElementType):

    def __init__(self, element_type, element_id, label):
        self.element_type = element_type
        self.element_id = element_id
        self.label = label

    def get_id(self):
        return self.element_id

    def get_entity_type(self):
        return self.element_type

    def get_info(self):
        return ""

    def get_label(self):
        return self.label


class ComponentConfigAPI(object):

    # API which should be implemented by components for config purposes

    # return list of elements of type ElementType
    def get_elements(self, element_type, search_filter=None):
        return []

    # return editEnabled, deleteEnabled
    def get_config_enabling(self, internal_element_type):
        raise NotImplementedError()

    # return templates for creating elements
    def get_create_templates(self, internal_element_type):
        raise NotImplementedError()

    def config_get_template(self, internal_element_type, element_id, custom_template, db_session):
        raise NotImplementedError()

    # update element
    def config_update_row(self, internal_element_type, element_id, template, dbt):
        raise NotImplementedError()

    # create element
    def config_create_row(self, internal_element_type, template, dbt):
        raise NotImplementedError()

    # delete element - return True if delete succeded, False otherwise
    def config_delete_row(self, internal_element_type, element_id, dbt):
        raise NotImplementedError()



class PluginConfigAPI(object):

    def __init__(self, plugin_socket_config, template_handler, user_admin, checkpoint_handler, activity_log_manager):
        self.plugin_socket_config = plugin_socket_config
        self.template_handler = template_handler
        self.checkpoint_handler = checkpoint_handler
        self.component_handlers = dict()
        self.add_component_handler("user", user_admin)
        self.activity_log_handler = activity_log_manager






    def add_component_handler(self, component_name, component):
        if self.component_handlers.has_key(component_name):
            self.checkpoint_handler.Checkpoint("add_component_handler", "config_api", lib.checkpoint.ERROR, message="Duplicate component handlers for '%s'" % component_name)
        self.component_handlers[component_name] = component

    def get_component_handler(self, component_name):
        return self.component_handlers.get(component_name)

    def get_config_spec(self, entity_type, response_object):
        with self.checkpoint_handler.CheckpointScope("get_config_spec", "config_api", lib.checkpoint.DEBUG, entity_type=entity_type):
            element_template_defs = self.get_element_template_defs(entity_type)
            response_object.set_config_templates(element_template_defs)
            response_object.get_edit_enabled, response_object.get_delete_enabled = self.get_crud_enabling(entity_type)
            response_object.set_create_enabled(len(element_template_defs)>0)

    def get_crud_enabling(self, entity_type):
        with self.checkpoint_handler.CheckpointScope("get_crud_enabling", "config_api", lib.checkpoint.DEBUG, entity_type=entity_type):
            element_type = entity_type
            edit_enabled = delete_enabled = True
            types = rule_api.get_basic_types(element_type, self.checkpoint_handler)
            for basic_entity_type, type_dict in types:
                if type_dict["config_type"]=="component":
                    internal_element_type = rule_api.get_plugin_element_type(basic_entity_type)
                    component = self.get_component_handler(type_dict["component_name"])
                    edit, delete = component.get_config_enabling(internal_element_type)
                elif type_dict["config_type"]=="class" or type_dict["config_type"]=="action":
                    crud_enabled = type_dict.get("crud_enabled", True)
                    edit = delete = crud_enabled
                elif type_dict["config_type"]=="module":
                    plugin_name = type_dict["plugin"]
                    internal_element_type = rule_api.get_plugin_element_type(basic_entity_type)
                    edit, delete = self.plugin_socket_config.get_config_enabling(plugin_name, internal_element_type)
                else:
                    edit = delete = False
                if not edit:
                    edit_enabled = False
                if not delete:
                    delete_enabled = False
                if not edit_enabled and not delete_enabled:
                    break

            return edit_enabled, delete_enabled



    def get_element_template_defs(self, entity_type):
        with self.checkpoint_handler.CheckpointScope("get_element_template_defs", "config_api", lib.checkpoint.DEBUG, entity_type=entity_type):
            element_type = entity_type
            template_refs = []
            types = rule_api.get_basic_types(element_type, self.checkpoint_handler)
            for basic_entity_type, type_dict in types:
                if type_dict["config_type"]=="component" or type_dict["config_type"] == "action":
                    internal_element_type = rule_api.get_plugin_element_type(basic_entity_type)
                    component = self.get_component_handler(type_dict["component_name"])
                    create_templates = component.get_create_templates(internal_element_type)
                else:
                    create_templates = [self.get_config_element_template(basic_entity_type, element_id=None, bypass_custom_template=True)]
                for create_template in create_templates:
                    if create_template:
                        template_ref = RestApi.Admin.TemplateRef()
                        template_ref.set_template_name(create_template.get_name)
                        template_ref.set_template_title(create_template.get_title)
                        template_ref.set_template_description(create_template.get_description)
                        template_ref.set_entity_type(basic_entity_type)
                        template_ref.set_template_type(-1)
                        template_refs.append(template_ref)

            return template_refs



    def get_element_template(self, element):
        with self.checkpoint_handler.CheckpointScope("get_element_template", "config_api", lib.checkpoint.DEBUG):
            element_id = element.get_id()
            return self.template_handler.get_element_template(element_id)

    def get_config_template(self, entity_type, template_name):
        with self.checkpoint_handler.CheckpointScope("get_config_template", "config_api", lib.checkpoint.DEBUG, entity_type=entity_type):
            if template_name:
                template_from_file = self.template_handler.get_template_copy(template_name)
                config_template = self.get_config_element_template(entity_type, None, custom_template=template_from_file, db_session=None)
                return config_template
    #            return ConfigTemplateSpec.merge_templates(config_template, template_from_file)
            else:
                return self.get_config_element_template(entity_type, None, db_session=None)


    def get_config_element_template(self, entity_type, element_id, bypass_custom_template=False, custom_template=None, db_session=None):
        with self.checkpoint_handler.CheckpointScope("get_config_element_template", "config_api", lib.checkpoint.DEBUG, entity_type=entity_type):
            with database_api.SessionWrapper(db_session) as db_session:
                element_type = entity_type
                element_type_def = rule_api.get_element_type_dict(element_type)
                if element_type_def["config_type"] == "module":
                    if element_id:
                        module_name, dot, element_id = element_id.partition(".")
                    else:
                        module_name = element_type_def["plugin"]
                    internal_element_type = rule_api.get_plugin_element_type(element_type)
                    return self.plugin_socket_config.config_get_template(module_name, internal_element_type, element_id, db_session)
                elif element_type_def["config_type"] == "component" or element_type_def["config_type"] == "action":
                    if element_type_def["config_type"] == "action" and not bypass_custom_template:
                        component = self.get_component_handler(element_type_def["component_name"])
                        internal_element_type = rule_api.get_plugin_element_type(element_type)
                        if element_id:
                            custom_template = self.template_handler.get_element_template(element_id)
                        config_template = component.config_get_template(internal_element_type, element_id, custom_template, db_session)
                        return ConfigTemplateSpec.merge_templates(config_template, custom_template)

                    else:
                        component = self.get_component_handler(element_type_def["component_name"])
                        internal_element_type = rule_api.get_plugin_element_type(element_type)
                        return component.config_get_template(internal_element_type, element_id, custom_template, db_session)
                elif element_type_def["config_type"] == "class":
                    return rule_api.config_get_template(element_type, element_id, db_session)
                else:
                    return None


    def _expand_template_variables(self, template, config_template):
        with self.checkpoint_handler.CheckpointScope("_expand_template_variables", "config_api", lib.checkpoint.DEBUG):
            parameter_fields = []
            other_fields = []
            for field in template.get_field:
                if field.get_field_type == "custom_template":
                    parameter_fields.append(field)
                else:
                    other_fields.append(field)
            for field in other_fields:
                value = expand_variables(field, parameter_fields)
                try:
                    config_template.set_value(field.get_name, value)
                except ConfigTemplateSpec.FieldNotFoundException, e:
                    print e

    def check_data_types(self, template):
        errors = ConfigTemplateSpec.check_data_types(template)
        if errors:
            error_str = "\n".join(["Error in field %s : %s" % (name, error_str) for (name, error_str) in errors])
            raise OperationNotAllowedException(error_str)

    def _get_updated_template(self, entity_type, template, dbt):
        with self.checkpoint_handler.CheckpointScope("_get_updated_template", "config_api", lib.checkpoint.DEBUG, entity_type=entity_type):
            # Return template with only the non custom fields from the template
            internal_element_type = rule_api.get_plugin_element_type(entity_type)
            config_template = self.get_config_element_template(entity_type, None, bypass_custom_template=True, custom_template=template, db_session=dbt)
            config_template_spec = ConfigTemplateSpec(config_template)
            update_template_spec = ConfigTemplateSpec()

            for field in template.get_element_field():
                if field.get_field_type != ConfigTemplateSpec.FIELD_TYPE_CUSTOM:
                    config_field = config_template_spec.get_field(field.get_element_name())
                    if config_field:
                        update_template_spec.add_field_def(config_field)

            self._expand_template_variables(template, update_template_spec)
            return update_template_spec.get_template()

    def _get_updated_template_for_create(self, entity_type, template, dbt):
        with self.checkpoint_handler.CheckpointScope("_get_updated_template_for_create", "config_api", lib.checkpoint.DEBUG, entity_type=entity_type):
            # Return template with all values set - those not set in template are filled in with default values
            internal_element_type = rule_api.get_plugin_element_type(entity_type)
            config_template = self.get_config_element_template(entity_type, None, bypass_custom_template=True, custom_template=template, db_session=dbt)
            config_template_spec = ConfigTemplateSpec(config_template)

            for field in template.get_element_field():
                if field.get_field_type != ConfigTemplateSpec.FIELD_TYPE_CUSTOM:
                    config_field = config_template_spec.get_field(field.get_element_name())
                    if config_field:
                        config_field.set_element_default_value(field.get_element_default_value())

            self._expand_template_variables(template, config_template_spec)
            return config_template_spec.get_template()


    def update_row_from_template(self, entity_type, element_id, template, transaction=None, skip_activity_log=False):
        with self.checkpoint_handler.CheckpointScope("update_row_from_template", "config_api", lib.checkpoint.DEBUG, entity_type=entity_type):
            with database_api.SessionWrapper(transaction, read_only=False) as dbt:
                if template.get_use_as_template:
                    old_template = self.get_config_element_template(entity_type, element_id, db_session=dbt)

                    config_template = self._get_updated_template(entity_type, template, dbt)

                    self.update_row_from_template(entity_type, element_id, config_template, dbt, skip_activity_log=True)
                    self.template_handler.save_template_values(dbt, template, element_id)

                    self.create_activity_log_entry(dbt, "Update", entity_type, template, element_id, old_template=old_template)

                else:
                    old_template = self.get_config_element_template(entity_type, element_id, db_session=dbt, bypass_custom_template=True)

                    self.check_data_types(template)
                    element_type_def = rule_api.get_element_type_dict(entity_type)
                    crud_enabled = element_type_def.get("crud_enabled", True)
                    if not crud_enabled:
                        raise OperationNotAllowedException("Elements of type '%s' can not be edited" % element_type_def.get("title", "<unknown>"))
                    if element_type_def["config_type"] == "class":
                        rule_api.config_update_row(entity_type, template, dbt)
                    elif element_type_def["config_type"] == "component" or element_type_def["config_type"] == "action":
                        internal_element_type = rule_api.get_plugin_element_type(entity_type)
                        component = self.get_component_handler(element_type_def["component_name"])
                        component.config_update_row(internal_element_type, element_id, template, dbt)
                    else:
                        module_name, dot, plugin_element_id = element_id.partition(".")
                        internal_element_type = rule_api.get_plugin_element_type(entity_type)
                        new_label = self.plugin_socket_config.config_update_row(module_name, internal_element_type, template, dbt)
                        if new_label:
                            rule_api.element_updated(entity_type, plugin_element_id, new_label, dbt)

                    if not skip_activity_log:
                        self.create_activity_log_entry(dbt, "Update", entity_type, template, element_id, old_template=old_template)

                return self.get_config_element_template(entity_type, element_id, db_session=dbt)


    def create_row_from_template(self, entity_type, template, transaction=None):
        with self.checkpoint_handler.CheckpointScope("create_row_from_template", "config_api", lib.checkpoint.DEBUG, entity_type=entity_type):
            with database_api.SessionWrapper(transaction, read_only=False) as dbt:
                if template.get_use_as_template:
                    config_template = self._get_updated_template_for_create(entity_type, template, dbt)
                    element_id, new_template = self.create_row_from_template(entity_type, config_template, dbt)
                    self.template_handler.save_template_values(dbt, template, element_id)
                else:
                    self.check_data_types(template)
                    element_type_def = rule_api.get_element_type_dict(entity_type)
                    crud_enabled = element_type_def.get("crud_enabled", True)
                    if not crud_enabled:
                        raise OperationNotAllowedException("Elements of type '%s' can not be created" % element_type_def.get("title", "<unknown>"))
                    if element_type_def["config_type"] == "component" or element_type_def["config_type"] == "action":
                        internal_element_type = rule_api.get_plugin_element_type(entity_type)
                        component = self.get_component_handler(element_type_def["component_name"])
                        element_id = component.config_create_row(internal_element_type, template, dbt)
                    else:
                        if element_type_def["config_type"] == "class":
                            element = rule_api.config_create_row(entity_type, template, dbt)
                            element_id = element.get_id()
                        else:
                            if element_type_def["config_type"] == "action":
                                module_name = "_action"
                            else:
                                module_name = element_type_def["plugin"]

                            if module_name:
                                internal_element_type = rule_api.get_plugin_element_type(entity_type)
                                element_id = self.plugin_socket_config.config_create_row(module_name, internal_element_type, template, dbt)

                new_template = self.get_config_element_template(entity_type, element_id, db_session=dbt)
                self.create_activity_log_entry(dbt, "Create", entity_type, new_template, element_id)
                return element_id, new_template


    def delete_row(self, entity_type, element_id):
        with self.checkpoint_handler.CheckpointScope("delete_row", "config_api", lib.checkpoint.DEBUG, entity_type=entity_type):

            element_type_def = rule_api.get_element_type_dict(entity_type)
            crud_enabled = element_type_def.get("crud_enabled", True)
            if not crud_enabled:
                raise OperationNotAllowedException("Elements of type '%s' can not be deleted" % element_type_def.get("title", "<unknown>"))
            with database_api.Transaction() as dbt:
                template = self.get_config_element_template(entity_type, element_id, db_session=dbt)
                self.create_activity_log_entry(dbt, "Delete", entity_type, template, element_id)

                if element_type_def["config_type"] == "class": #or element_type_def["type_"] == "action":
                    return rule_api.delete_criteria(element_id, dbt)
                else:
                    internal_element_type = rule_api.get_plugin_element_type(entity_type)


                    if element_type_def["config_type"] == "component" or element_type_def["config_type"] == "action":
                        rule_api.delete_criteria_for_element(entity_type, element_id, dbt)
                        component = self.get_component_handler(element_type_def["component_name"])
                        return component.config_delete_row(internal_element_type, element_id, dbt)
                    else:
                        module_name, dot, element_id = element_id.partition(".")
                        rule_api.delete_criteria_for_element(entity_type, element_id, dbt)
                        return self.plugin_socket_config.config_delete_row(module_name, internal_element_type, element_id, dbt)


    def create_activity_log_entry(self, transaction, action_type, entity_type, template, element_id, old_template=None):
        entity_type_title = rule_api.get_entity_type_title(entity_type)
        if old_template:
            config_spec = ConfigTemplateSpec(template)
            value_dict =  config_spec.get_field_value_dict()
            old_config_spec = ConfigTemplateSpec(old_template)
            old_value_dict =  old_config_spec.get_field_value_dict()
            lines = []
            for key in sorted(value_dict.keys()):
                value = value_dict.pop(key)
                try:
                    old_value = old_value_dict.pop(key)
                    if value!=old_value:
                        null_string_change = False
                        if not value and not old_value and (isinstance(value, basestring) or isinstance(old_value, basestring)):
                            null_string_change = True
                        if not null_string_change:
                            lines.append("'%s' changed from '%s' to '%s'" % (key, old_value, value))
                except KeyError:
                    lines.append("'%s' added with value '%s'" % (key, value))
            for key, value in sorted(old_value_dict.items()):
                lines.append("'%s' with value '%s' removed" % (key, value))

            if lines:
                template_string = "\n".join(lines)
            else:
                template_string = "No changes"

        else:
            template_string = template2logstring(template) if template else "id: '%s'" % element_id

        if template_string:
            details_string = "%s (%s):\n\n%s" % (entity_type_title, element_id, template_string)
        else:
            details_string = "%s: %s" % (entity_type_title, element_id)


        self.activity_log_handler.copyUpdateAndAdd(transaction,
                                                   type=action_type,
                                                 summary="%s %s" % (action_type, entity_type_title),
                                                  details = details_string,
                                                  access_right = "Entity.%s" % entity_type,
                                                  )

    def create_launch_specs_from_portscan(self):
        import components.admin_ws.portscan_api as portscan_api
        element_type = u"ProgramAccess"
        module_name = u"_action"
        entity_type = "%s.%s" % (element_type, module_name)

        element_templates = self.template_handler.get_templates()
        for template in element_templates:
            if template.get_entity_type==entity_type:
                server_host_field = None
                server_port_field = None
                name_field = None
                for field in template.get_field:
                    if field.get_name == "server_host":
                        server_host_field = field
                    elif field.get_name == "server_port":
                        if field.get_default_value:
                            server_port_field = field
                    elif field.get_name == "label":
                        name_field = field
                if server_host_field and server_port_field:
                    server_host_name = server_host_field.get_default_value
                    server_port = server_port_field.get_default_value
                    servers = portscan_api.tcp_portscan(server_host_name, [server_port])
                    index = 1
                    for server in servers:
                        server_name = server[0]
                        server_host_field.get_default_value = server_name
                        if name_field:
                            name_field.get_default_value = "%s %s" % (template.get_title, server_name)
                            index += 1
                        try:
                            self.create_row_from_template(entity_type, template)
                        except Exception, e:
                            print e




    def create_default_template_file(self, filename):
        try:
            template = RestApi.Admin.ConfigTemplate.ConfigurationTemplate()
            #print template.attribute_typecode_dict["name"]
            template.set_title("(default)")
            template.set_name("(default)")
            #TODO : Element type ???
            columns = self.plugin_socket_config.config_get_columns("_action", None)
            fields = []
            for col in columns:
                field = RestApi.Admin.ConfigTemplate.FieldElement()
                #print field.__dict__
                #print field.pyclass.__dict__
                #print help(field)
                field.set_name(col.column_name)
                field.set_title(col.column_label)
                field.set_field_type("edit")
                fields.append(field)
            #element_templates.append(self.plugin_socket_config.get_template_element(plugin_name))
            template.set_field(fields)
            template_str = json.dumps(template, default=lambda o: o.__dict__,
                                      sort_keys=False,
                                      indent=4)

            f = file(filename, "wt")
            f.write(template_str)
            f.close()
        except Exception, e:
                print e
                pass
