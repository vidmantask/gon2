"""
This file contains the implementation of the web-services offered by the
AdminWS web service interface.
"""
import lib.ws.zsi_hack
import lib.ws.ssl_hack
import lib.ws.zsi_extensions

import traceback

import ws.admin_ws_services_server

from components.auth.server_management import OperationNotAllowedException

import activity_log
from components.database.server_common import database_api

import os.path
import uuid
import sys
import threading
import datetime

import lib.checkpoint
import lib.utility.ip

import admin_ws_session
from components.rest.rest_api import RestApi
from components.rest.rest_service import RestService


admin_ws_module_id = "admin_ws"



class AdminWSService(threading.Thread, ws.admin_ws_services_server.admin_wsService):
    SERVER_GENERAL_ERROR = -1
    SERVER_SESSION_LOST = 0
    SERVER_ACCESS_DENIED = 1

    """
    All admin web-service calls is dispatched by this class,
    it handles logging and propper error reporting, but the real
    work is dispatched to the session.

    Notice that each time the soap_login method is called the janitor
    that clean-up dead sessions is activated.
    """
    def __init__(self, environment, server_config, license_handler):
        threading.Thread.__init__(self)
        ws.admin_ws_services_server.admin_wsService.__init__(self)
        self.environment = environment
        self._sessions = {}
        self._server_config = server_config
        self._license_handler = license_handler
        self._management_ws_ip_host = server_config.management_ws_ip
        self._management_ws_ip_port = server_config.management_ws_port
        self._session_timeout = datetime.timedelta(days=1, hours=0, minutes=0, seconds=0, microseconds=0)
        self._running = False
        self._started = False
        self.service_management = None
        self.checkpoint_handler = environment.checkpoint_handler
        self.activity_log = activity_log.ActivityLog(environment)

    @property
    def get_management_ws_ip_port(self):
        return self._management_ws_ip_port

    @property
    def get_management_ws_ip_host(self):
        return self._management_ws_ip_host

    @property
    def get_session_timeout(self):
        return self._session_timeout

    def get_session(self, session_id):
        try:
            return self._sessions[session_id]
        except KeyError:
            self.handle_unknown_session_exception()

    def run(self):
        try:
            self._running = True
            self._started = True
            self.checkpoint_handler.Checkpoint("run", admin_ws_module_id, lib.checkpoint.DEBUG, ip = self._management_ws_ip_host, port = self._management_ws_ip_port)
            services=(self,)
            address = (self._management_ws_ip_host, self._management_ws_ip_port)

            # TODO (vazarovic): Implement RestService as daemon (self._sc)
            RestService.start_admin_wsgi(self)

            # self.admin_ws_service_thread = threading.Thread(name="AdminWSGI", target=RestService.start_admin_wsgi,
            #                                                 args=(self._admin_ws_service,))
            # self.admin_ws_service_thread.setDaemon(True)
            # self.admin_ws_service_thread.start()

            # if self._server_config.management_ws_https_enabled:
            #     self._sc = lib.ws.zsi_extensions.ServiceContainerHTTPS.create(self.checkpoint_handler,
            #                                                                    admin_ws_module_id, address,
            #                                                                   services, self._server_config.management_ws_https_certificate_filename_abspath, self._server_config.management_ws_https_key_filename_abspath, self._server_config.management_ws_www_html_path)
            # else:
            #     self._sc = lib.ws.zsi_extensions.ServiceContainerHTTP.create(self.checkpoint_handler,
            #                                                                   admin_ws_module_id, address, services,
            #                                                                  self._server_config.management_ws_www_html_path)

            self._sc.serve_forever()

            self.checkpoint_handler.Checkpoint("run.terminate", admin_ws_module_id, lib.checkpoint.DEBUG)
            self._sc.server_close()
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("run.error", admin_ws_module_id, lib.checkpoint.CRITICAL, etype, evalue, etrace)
        self._running = False

    def stop(self):
        if self._running:
            self._sc.shutdown()

    def is_running(self):
        return self._running

    def is_started(self):
        return self._started

    def set_service_management(self, service_management):
        self.service_management = service_management

    def create_activity_log(self, **kwargs):
        with database_api.Transaction() as dbt:
            activity_log = dbt.add(database_schema.ActivityLog(**kwargs))

    def rest_open_session(self, request):
        self.janitor_remove_dead_sessions()
        with self.environment.checkpoint_handler.CheckpointScope("rest_open_session", admin_ws_module_id, lib.checkpoint.DEBUG) as cps:
            session_id = None
            try:
                old_session_id = request.get_old_session_id
                existing_session = self._sessions.get(old_session_id)
                if not existing_session:
                    session_id = self._generate_new_session_id()
                    if old_session_id:
                        self.activity_log.saveLogElement(None,
                                                         session_id = old_session_id,
                                                         severity=activity_log.ActivityLog.INFO,
                                                         type="Session",
                                                         summary = "Change session id",
                                                         details = "New session id: %s" % session_id)
                    session = admin_ws_session.AdminWSSession(self.environment, self._server_config, self._license_handler, session_id, self.service_management)
                    self._sessions[session_id] = session
                else:
                    session_id = old_session_id

                cps.add_complete_attr(session_id = session_id)
                response = RestApi.Admin.OpenSessionResponse()
                response.set_session_id(session_id)
                return response
            except:
                self.handle_unexpected_exception(session_id)

    def _generate_new_session_id(self):
        session_id = uuid.uuid4()
        if self._sessions.has_key(session_id):
            return self._generate_new_session_id()
        return str(session_id)

    def rest_close_session(self, request):
        session_id = request.get_session_id
        self.environment.checkpoint_handler.Checkpoint("CloseSession", admin_ws_module_id, lib.checkpoint.INFO, session_id = session_id)
        with self.environment.checkpoint_handler.CheckpointScope("rest_close_session", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            self._add_exception_log(session_id, "Session", "Close Session", activity_log.ActivityLog.INFO, "Session")
            try:
                del self._sessions[session_id]
                response = RestApi.Admin.CloseSessionResponse()
                response.set_rc(True)
                return response
            except KeyError:
                response = RestApi.Admin.CloseSessionResponse()
                response.set_rc(False)
                return response
            except:
                self.handle_unexpected_exception(session_id)

    def rest_login(self, request):
        session_id = request.get_session_id
        self.environment.checkpoint_handler.Checkpoint("login", admin_ws_module_id, lib.checkpoint.INFO, session_id = session_id)
        with self.environment.checkpoint_handler.CheckpointScope("rest_login", admin_ws_module_id,
                                                                 lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).Login(request)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_internal_login(self, request):
        session_id = request.get_session_id
        self.environment.checkpoint_handler.Checkpoint("login", admin_ws_module_id, lib.checkpoint.INFO, session_id = session_id)
        with self.environment.checkpoint_handler.CheckpointScope("rest_internal_login", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).InternalLogin(request)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_access_definition(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_access_definition", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetAccessDefintion(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_module_elements(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_module_elements", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetModuleElements(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_specific_elements(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_specific_elements", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetSpecificElements(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_auth_restriction_elements(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_auth_restriction_elements", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetAuthRestrictionElements(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_update_auth_restriction_elements(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_update_auth_restriction_elements", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).UpdateAuthRestrictionElements(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_remove_user_from_launch_type_category(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_remove_user_from_launch_type_category", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).RemoveUserFromLaunchTypeCategory(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_restricted_launch_type_categories(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_restricted_launch_type_categories", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetRestrictedLaunchTypeCategories(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_module_header_element(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_module_header_element", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetModuleHeaderElement(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_element_seach_categories(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_element_seach_categories", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetElementSeachCategories(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_module_elements_first(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_module_elements_first", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetModuleElementsFirst(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_module_elements_next(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_module_elements_next", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetModuleElementsNext(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_rules_first(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_rules_first", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetRulesFirst(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_rules_next(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_rules_next", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetRulesNext(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_add_members_to_first_time_enrollers(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_add_members_to_first_time_enrollers", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).AddMembersToFirstTimeEnrollers(request)
            except OperationNotAllowedException as msg:
                self.handle_exception_operation_not_allowed(msg, session_id)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_rules(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_rules", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetRules(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_create_rule(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_create_rule", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).CreateRule(request)
            except OperationNotAllowedException as msg:
                self.handle_exception_operation_not_allowed(msg, session_id)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_update_rule(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_update_rule", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).UpdateRule(request)
            except OperationNotAllowedException as msg:
                self.handle_exception_operation_not_allowed(msg, session_id)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_delete_rule(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_delete_rule", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).DeleteRule(request)
            except OperationNotAllowedException as msg:
                self.handle_exception_operation_not_allowed(msg, session_id)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_rule_spec(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_rule_spec", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetRuleSpec(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_rules_for_element(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_rules_for_element", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetRulesForElement(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_reports(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_reports", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetReports(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_report_data(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_report_data", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                if self._sessions.has_key(session_id):
                    session = self.get_session(session_id)
                else:
                    self.environment.checkpoint_handler.Checkpoint("rest_get_report_data.anonym", admin_ws_module_id, lib.checkpoint.WARNING, message='anonym user is reciving data')
                    session = admin_ws_session.AdminWSSession(self.environment, self._server_config, self._license_handler, 99999, 'anonym', 'anonym', self.service_management)
                return session.GetReportData(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)


    def rest_get_report_spec(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_report_spec", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetReportSpec(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_config_spec(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_config_spec", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetConfigSpec(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)


    def rest_get_config_template(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_config_template", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetConfigTemplate(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_element_config_template(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_element_config_template", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetElementConfigTemplate(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)



    def rest_create_config_template_row(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_create_config_template_row", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).CreateConfigTemplateRow(request)
            except OperationNotAllowedException as msg:
                self.handle_exception_operation_not_allowed(msg, session_id)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)


    def rest_update_config_template_row(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_update_config_template_row", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).UpdateConfigTemplateRow(request)
            except OperationNotAllowedException as msg:
                self.handle_exception_operation_not_allowed(msg, session_id)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)


    def rest_delete_config_element_row(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_delete_config_element_row", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).DeleteConfigElementRow(request)
            except OperationNotAllowedException as msg:
                self.handle_exception_operation_not_allowed(msg, session_id)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_enroll_device(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_enroll_device", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).EnrollDevice(request)
            except OperationNotAllowedException as msg:
                self.handle_exception_operation_not_allowed(msg, session_id)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_enroll_device_to_user(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_enroll_device_to_user", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).EnrollDeviceToUser(request)
            except OperationNotAllowedException as msg:
                self.handle_exception_operation_not_allowed(msg, session_id)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_enrollment_requests(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_enrollment_requests", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetEnrollmentRequests(request)
            except OperationNotAllowedException as msg:
                self.handle_exception_operation_not_allowed(msg, session_id)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_respond_to_enrollment_request(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_respond_to_enrollment_request", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).RespondToEnrollmentRequest(request)
            except OperationNotAllowedException as msg:
                self.handle_exception_operation_not_allowed(msg, session_id)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)



    def rest_get_token_info(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_token_info", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetTokenInfo(request)
            except OperationNotAllowedException as msg:
                self.handle_exception_operation_not_allowed(msg, session_id)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_deploy_get_known_secret_client(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_deploy_get_known_secret_client", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).DeployGetKnownSecretClient(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_deploy_get_servers(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_deploy_get_servers", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).DeployGetServers(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_deploy_generate_key_pair(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_deploy_generate_key_pair", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).DeployGenerateKeyPair(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_gpm_collections(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_gpm_collections", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetGPMCollections(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_download_gpm_start(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_download_gpm_start", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).DownloadGPMStart(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_download_gpm_get_chunk(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_download_gpm_get_chunk", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).DownloadGPMGetChunk(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_download_gpm_stop(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_download_gpm_stop", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).DownloadGPMStop(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_generate_gpm_signature(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_generate_gpm_signature", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GenerateGPMSignature(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_port_scan(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_port_scan", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).PortScan(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_entity_type_for_plugin_type(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_entity_type_for_plugin_type", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetEntityTypeForPluginType(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_basic_entity_types(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_basic_entity_types", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetBasicEntityTypes(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_menu_items(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_menu_items", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetMenuItems(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_menu(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_menu", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetMenu(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_add_item_to_menu(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_add_item_to_menu", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).AddItemToMenu(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_add_element_to_menu(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_add_element_to_menu", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).AddElementToMenu(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_move_item_to_menu(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_move_item_to_menu", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).MoveItemToMenu(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_remove_item_from_menu(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_remove_item_from_menu", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).RemoveItemFromMenu(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_g_on_service_get_list(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_g_on_service_get_list", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GOnServiceGetList(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_g_on_service_restart(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_g_on_service_restart", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GOnServiceRestart(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_g_on_service_stop(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_g_on_service_stop", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GOnServiceStop(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_gateway_sessions(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_gateway_sessions", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetGatewaySessions(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_gateway_sessions_next(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_gateway_sessions_next", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetGatewaySessionsNext(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)


    def rest_gateway_is_user_online(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_gateway_is_user_online", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GatewayIsUserOnline(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_gateway_session_close(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_gateway_session_close", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GatewaySessionClose(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_gateway_session_recalculate_menu(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_gateway_session_recalculate_menu", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GatewaySessionRecalculateMenu(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_elements_stop(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_elements_stop", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetElementsStop(request)
            except admin_ws_session.NoAccessException as msg:
                self.handle_exception_operation_not_authorized(msg, session_id)
            except:
                self.handle_unexpected_exception(session_id)

    def rest_get_license_info(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_get_license_info", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).GetLicenseInfo()
            except:
                self.handle_unexpected_exception(session_id)

    def rest_set_license(self, request):
        session_id = request.get_session_id
        with self.checkpoint_handler.CheckpointScope("rest_set_license", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = session_id):
            try:
                return self.get_session(session_id).SetLicense(request)
            except:
                self.handle_unexpected_exception(session_id)



    def rest_ping(self, request):
        with self.environment.checkpoint_handler.CheckpointScope("rest_ping", admin_ws_module_id, lib.checkpoint.DEBUG) as cps:
            session_id = request.get_session_id
            rc = True
            if session_id and not self._sessions.has_key(session_id):
                rc = False
            response = RestApi.Admin.PingResponse()
            response.set_rc(rc)
            return response

    def _create_fault_server_response(self, error_message, error_code=None):
        fault_response = RestApi.Admin.FaultServerElement()
        fault_response.set_error_message(error_message)
        if not error_code is None:
            fault_response.set_error_code(error_code)

        return fault_response



    def handle_unknown_session_exception(self):
        (etype, evalue, etrace) = sys.exc_info()
        self.environment.checkpoint_handler.CheckpointException("handle_unknown_session_exception", admin_ws_module_id, lib.checkpoint.WARNING, etype, evalue, etrace)

        error_message = 'Server sesssion lost or expired'
        fault_response = self._create_fault_server_response(error_message, self.SERVER_SESSION_LOST)
        raise fault_response


    def _add_exception_log(self, session_id, error_message, error_type, severity, log_type):
        if session_id and self._sessions.get(session_id):
            self.get_session(session_id).create_error_log(severity=severity, type=log_type, summary=error_type, details=error_message)
        else:
            self.activity_log.saveLogElement(None, severity=severity, type=log_type, summary=error_type, details=error_message)

    def handle_unexpected_exception(self, session_id=None):
        (etype, evalue, etrace) = sys.exc_info()
        if isinstance(evalue, RestApi.Admin.FaultServerElement):
            raise evalue
        self.environment.checkpoint_handler.CheckpointException("handle_unexpected_exception", admin_ws_module_id, lib.checkpoint.CRITICAL, etype, evalue, etrace)

        error_message = ''.join(traceback.format_exception_only(etype, evalue))
        self._add_exception_log(session_id, error_message, "Unexpected Exception", activity_log.ActivityLog.ERROR, "Exception")

        fault_response = self._create_fault_server_response(error_message)
        raise fault_response

    def handle_exception_operation_not_allowed(self, message, session_id=None):
        (etype, evalue, etrace) = sys.exc_info()
        self.environment.checkpoint_handler.CheckpointException("handle_exception_operation_not_allowed", admin_ws_module_id, lib.checkpoint.DEBUG, etype, evalue, etrace)

        error_message = ''.join(traceback.format_exception_only(etype, evalue))
#        self._add_exception_log(session_id, error_message, "Operation Not Allowed", activity_log.ActivityLog.INFO, "Exception")

        fault_response = RestApi.Admin.FaultServerOperationNotAllowedElement()
        fault_response.set_error_message(message.message)
        raise fault_response

    def handle_exception_operation_not_authorized(self, message, session_id=None):
        (etype, evalue, etrace) = sys.exc_info()
        error_message = ''.join(traceback.format_exception_only(etype, evalue))
        self.environment.checkpoint_handler.Checkpoint("handle_exception_operation_not_authorized", admin_ws_module_id, lib.checkpoint.DEBUG, message=error_message)

        error_message = ''.join(traceback.format_exception_only(etype, evalue))
        self._add_exception_log(session_id, error_message, "Operation Not Authorized", activity_log.ActivityLog.INFO, "Exception")

        fault_response = self._create_fault_server_response(error_message, self.SERVER_ACCESS_DENIED)
        raise fault_response

    def janitor_remove_dead_sessions(self):
        for session in self._sessions.values():
            if(session.is_dead(self._session_timeout)):
                self.environment.checkpoint_handler.Checkpoint("janitor_remove_dead_session", admin_ws_module_id, lib.checkpoint.DEBUG, session_id = "%r" % session.session_id)
                del self._sessions[session.session_id]

    def get_url(self):
        return "https://%s:%d" % (self._management_ws_ip_host, self._management_ws_ip_port)

    def get_connect_url(self):
        return "https://%s:%d" % (lib.utility.ip.build_connect_host_address(self._management_ws_ip_host), self._management_ws_ip_port)

    @classmethod
    def run_default(cls, checkpoint_handler, db_url, ip_host, ip_port, plugin_root, config_root, license_handler, config=None):

        import components.traffic.server_common.database_schema


        component_env = components.environment.Environment(checkpoint_handler, db_url)
        component_env.plugin_manager = components.plugin.server_management.manager.Manager(checkpoint_handler, component_env.get_default_database(), license_handler, plugin_root)
        component_env.gpms_root = os.path.join(config_root, 'gpm', 'gpms')
        component_env.gpmc_defs_root = os.path.join(config_root, 'gpm', 'gpmcdefs')
        component_env.client_knownsecret = "0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E000400D7997B3756260EEE8D25B9F98256115CDB487F2CA598FD94D7A361E04C01E90B42A6124E64CD4994C01C5108899F64839E46E34A2C5BF4BF753A6E3081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D2AD5BB92B2200E1AF062FA9F29CB43E9969AC7D1F190F7791E52F8D34F"
        component_env.server_knownsecret = "0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E000400872A7507237DEA288BCDEFC0BEF38A1A239CCF96E9FDC22F2F62336A3F013DD6A903639422F8F74E11CC4B725B72BF3FDB88E666690BFB7676B0F03081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D241304C4665A621272CE151248A2B3D2BBA7C8986219366F10790F01D2"
        component_env.get_servers = lambda : '127.0.0.1, 8042'

        components.database.server_common.schema_api.SchemaFactory.connect(component_env.get_default_database())

        class ServerConfigStub(object):
            def __init__(self):
                self.management_ws_ip = None
                self.management_ws_port = None
                self.management_ws_https_enabled = True
                self.management_ws_https_certificate_filename_abspath = os.path.join(config_root, 'certificates', 'gon_https_management_server.crt')
                self.management_ws_https_key_filename_abspath = os.path.join(config_root, 'certificates', 'gon_https_management_server.key.noencrypt.pem')
                self.management_ws_www_html_path = os.path.join(config_root, 'gon_server_management_service', 'win', 'www', 'html')
        server_config = ServerConfigStub()
        server_config.management_ws_ip = ip_host
        server_config.management_ws_port = ip_port
        if config is None:
            config = server_config
        service = AdminWSService(component_env, config, license_handler)
        service.start()
        return service



if __name__ == "__main__" :
    checkpoint_handler = lib.checkpoint.CheckpointHandler.get_cout_all()
    service = AdminWSService.run_default(checkpoint_handler, "sqlite:///:memory", '0.0.0.0', 8080, '../..', '../../../setup/dev_env/gon_installation/config', None)
    service.join()
