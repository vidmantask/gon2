'''
Created on 04/10/2011

@author: pwl
'''
import sys 

import lib.checkpoint
import datetime

import database_schema
from components.database.server_common import database_api
from components.plugin.server_management import plugin_socket_audit

class ActivityLog():
    
    INFO = "info"
    ERROR = "error"
    WARNING = "warning"
    
    USER_LOGIN = 'user_login'
    USER_ID = 'user_id'
    USER_PLUGIN = 'user_plugin'
    INTERNAL_USER_LOGIN = 'internal_user_login'
    ACCESS_RIGHT = 'access_right'
    SESSION_ID = 'session_id'
    TYPE = 'type'
    SEVERITY = 'severity'
    SUMMARY = 'summary'
    DETAILS = 'details'
    
    
    def __init__(self, environment):
        self.checkpoint_handler = environment.checkpoint_handler
        self.plugin_socket_audit = plugin_socket_audit.PluginSocketAudit(environment.plugin_manager)
        
    def saveLogElement(self, transaction, **kwargs):
        try:
            with database_api.SessionWrapper(transaction, read_only=False) as dbt:
                kwargs["timestamp"] = datetime.datetime.now()
                activityLog = database_schema.ActivityLog(**kwargs)
                dbt.add(activityLog)
            self.checkpoint_handler.Checkpoint("saveLogElement", "activity_log", lib.checkpoint.DEBUG, values=kwargs)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("saveLogElement", "activity_log", lib.checkpoint.ERROR, etype, evalue, etrace)
            self.checkpoint_handler.Checkpoint("saveLogElement", "activity_log", lib.checkpoint.ERROR, values=kwargs)
            
        kwargs["source"] = "management"
        self.plugin_socket_audit.save_entry(**kwargs)
        


class ActivityLogCreator():

    def __init__(self, activity_log, **default_values):
        self.activity_log = activity_log
        self.default_values = default_values
        
    def save(self, **kwargs):
        self.default_values.update(kwargs)
        self.activity_log.saveLogElement(None, **self.default_values)

    def add(self, transaction, **kwargs):
        self.default_values.update(kwargs)
        self.activity_log.saveLogElement(transaction, **self.default_values)
        
    def addValues(self, **kwargs):
        self.default_values.update(kwargs)
    
    def copy(self):
        return ActivityLogCreator(self.activity_log, **self.default_values)
    
    def copyAddAndSave(self, **kwargs):
        copy = self.copy()
        copy.addValues(**kwargs)
        copy.save()

    def copyUpdateAndAdd(self, transaction, **kwargs):
        copy = self.copy()
        copy.addValues(**kwargs)
        copy.add(transaction)
    
