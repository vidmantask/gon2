"""
Plugin socket for the Element plugin type
"""
from components.plugin.common import socket_base
from plugin_types.server_management import plugin_type_element

import lib.checkpoint
import sys

class PluginSocket(socket_base.PluginSocketBase):
    def __init__(self, plugin_manager):
        socket_base.PluginSocketBase.__init__(self, plugin_manager)
        self.plugins_filtered = socket_base.PluginSocketBase.plugin_type_filter(plugin_manager.plugins, plugin_type_element.PluginTypeElement)
        self.checkpoint_handler = plugin_manager.checkpoint_handler

    def get_module_element_count(self, plugin_providers):
        count = 0
        for (internal_module_name, internal_element_type) in plugin_providers:
            plugin = self.plugins_filtered.get(internal_module_name)
            if not plugin:
                self.checkpoint_handler.Checkpoint("plugins_filtered.get.error", "plugin_socket_element", lib.checkpoint.ERROR, msg="Unknown or missing plugin %s" % (internal_module_name))
                continue
            if not plugin.has_element_type(internal_element_type): 
                self.checkpoint_handler.Checkpoint("has_element_type.error", "plugin_socket_element", lib.checkpoint.ERROR, msg="Unknown element type '%s' for plugin '%s'" % (internal_element_type, internal_module_name))
                continue
            element_count = plugin.get_element_count(internal_element_type)
            if element_count < 0:
                return -1
            count += element_count
        return count

    def get_module_elements(self, plugin_providers, search_filter=None):
        count = -1
        for (internal_module_name, internal_element_type) in plugin_providers:
            plugin = self.plugins_filtered.get(internal_module_name)
            if not plugin:
                self.checkpoint_handler.Checkpoint("plugins_filtered.get.error", "plugin_socket_element", lib.checkpoint.ERROR, msg="Unknown or missing plugin %s" % (internal_module_name))
                continue
            count = plugin.get_element_count(internal_element_type)
            
            try:
                if search_filter:
                    plugin_elements = plugin.get_elements(internal_element_type, search_filter)
                else:
                    plugin_elements = plugin.get_elements(internal_element_type)
                if plugin_elements:
                    for element in plugin_elements:
                        yield element
            except GeneratorExit:
                return
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("get_elements", "PluginSocketElement", lib.checkpoint.ERROR, etype, evalue, etrace)
                continue
        
    def get_specific_elements(self, plugin_name, internal_element_type, element_ids):
        plugin = self.plugins_filtered.get(plugin_name)
        return plugin.get_specific_elements(internal_element_type, element_ids)
            
    

