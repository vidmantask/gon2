"""
Plugin socket for the Element plugin type
"""
from components.plugin.common import socket_base
from plugin_types.server_management.plugin_type_audit import PluginTypeAudit

import sys

import lib.checkpoint
import lib.version


class PluginSocketAudit(socket_base.PluginSocketBase):
    
    def __init__(self, plugin_manager):
        socket_base.PluginSocketBase.__init__(self, plugin_manager)
        self.plugins_filtered = socket_base.PluginSocketBase.plugin_type_filter(plugin_manager.plugins, PluginTypeAudit)
        self.checkpoint_handler = plugin_manager.checkpoint_handler
        version = lib.version.Version.create_current()
        version_string = "%s.%s.%s" % (version.get_version_major(), version.get_version_minor(), version.get_version_bugfix())
        for plugin in self.get_plugins():
            plugin.set_version_string(version_string)

    def get_plugins(self):
        return self.plugins_filtered.values()
    
    def get_plugin(self, name):
        return self.plugins_filtered.get(name, None)
    
    def save_entry(self, **kwargs):
        for plugin in self.get_plugins():
            try:
                plugin.save_entry(**kwargs)
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("save_entry", "PluginSocketAudit", lib.checkpoint.ERROR, etype, evalue, etrace)
                self.checkpoint_handler.Checkpoint("save_entry", "PluginSocketAudit", lib.checkpoint.ERROR, values=kwargs, plugin=plugin.plugin_name)
                    
