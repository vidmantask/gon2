"""
Plugin socket for the Element plugin type
"""
from components.plugin.common import socket_base
from plugin_types.server_management.plugin_type_endpoint import PluginTypeEndpoint

import lib.checkpoint
import sys

class PluginSocket(socket_base.PluginSocketBase):
    
    def __init__(self, plugin_manager):
        socket_base.PluginSocketBase.__init__(self, plugin_manager)
        self.plugins_filtered = socket_base.PluginSocketBase.plugin_type_filter(plugin_manager.plugins, PluginTypeEndpoint)
        self.checkpoint_handler = plugin_manager.checkpoint_handler
        self.endpoint_authentication_plugins = []
        self.devices_status_plugins = []
        for plugin in self.get_plugins():
            if plugin.supports_device_status():
                self.devices_status_plugins.append(plugin)
            if plugin.supports_endpoint_authentication():
                self.endpoint_authentication_plugins.append(plugin)

    def get_plugins(self):
        return self.plugins_filtered.values()
    
    def get_plugin(self, name):
        return self.plugins_filtered.get(name, None)
    
    def get_endpoint_authentication_plugins(self):
        return self.endpoint_authentication_plugins
        
    def get_devices_status_plugins(self):
        return self.devices_status_plugins
