"""
Plugin socket for the Element plugin type
"""
from components.plugin.common import socket_base
from plugin_types.server_management.plugin_type_user import PluginTypeUser
import components.user.server_management.user_admin

import lib.checkpoint
import sys

class PluginSocket(socket_base.PluginSocketBase):
    
    def __init__(self, plugin_manager):
        socket_base.PluginSocketBase.__init__(self, plugin_manager)
        self.plugins_filtered = socket_base.PluginSocketBase.plugin_type_filter(plugin_manager.plugins, PluginTypeUser)
        self.checkpoint_handler = plugin_manager.checkpoint_handler

    def get_plugins(self):
        return self.plugins_filtered.values()
    
    def get_plugin(self, name):
        return self.plugins_filtered.get(name, None)
        
