"""
Module manager for Management plugin modules
"""
from __future__ import with_statement
import os.path

import lib.checkpoint

from components.plugin.common import manager_base
import plugin_types.server_management


class Manager(manager_base.ManagerBase):
    def __init__(self, checkpoint_handler, database, license_handler, import_path):
        manager_base.ManagerBase.__init__(self, checkpoint_handler, import_path)
        
        self.plugin_classes = []
        self.plugins = {}
        with self.checkpoint_handler.CheckpointScope('init', manager_base.module_id, lib.checkpoint.DEBUG, plugin_root=self.plugin_root):
            if os.path.exists(self.plugin_root):
                self.plugin_classes = self.load_plugin_classes('server_management', plugin_types.server_management.PluginTypeManagementServer)
                self.plugins = self.create_plugin_instances(self.plugin_classes, checkpoint_handler=checkpoint_handler, database=database, license_handler=license_handler)
            else:
                self.checkpoint_handler.Checkpoint('init.error', manager_base.module_id, lib.checkpoint.ERROR, message='Plugin root not found')
        
        
if __name__ == '__main__':
    checkpoint_handler = lib.checkpoint.CheckpointHandler.get_cout_all()
    mm = Manager(checkpoint_handler, '/home/twa/source/hg_checkout/M2/py')
    