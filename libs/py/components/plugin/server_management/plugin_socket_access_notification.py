"""
Plugin socket for the Access Notification plugin type
"""
from components.plugin.common import socket_base
from plugin_types.server_management import plugin_type_access_notification


class PluginSocket(socket_base.PluginSocketBase):
    def __init__(self, plugin_manager):
        socket_base.PluginSocketBase.__init__(self, plugin_manager)
        self.plugins_filtered = socket_base.PluginSocketBase.plugin_type_filter(plugin_manager.plugins, plugin_type_access_notification.PluginTypeAccessNotification)
    
    def notify_login(self, login, user_sid, ip, timestamp):
        for plugin_name in self.plugins_filtered.keys():
            self.plugins_filtered[plugin_name].notify_login(login, user_sid, ip, timestamp)
