"""
Plugin socket for the Config plugin type
"""
from components.plugin.common import socket_base
from plugin_types.server_management import plugin_type_config

import components.dialog.server_common.dialog_api
import components.dialog.server_management.dialog_api


class PluginSocket(socket_base.PluginSocketBase):
    def __init__(self, plugin_manager):
        socket_base.PluginSocketBase.__init__(self, plugin_manager)
        self.plugins_filtered = socket_base.PluginSocketBase.plugin_type_filter(plugin_manager.plugins, plugin_type_config.PluginTypeConfig)
        
    def has_plugin(self, plugin_name):
        return self.plugins_filtered.has_key(plugin_name)
            
    def config_get_template(self, plugin_name, element_type, element_id, db_session):
        try:
            return self.plugins_filtered[plugin_name].config_get_template(element_type, element_id, db_session)
        except KeyError:
            raise Exception("Unknown plugin '%s' while getting element '%s' of type '%s'" % (plugin_name, element_id, element_type))

    def config_create_row(self, plugin_name, element_type, template, transaction):
        return self.plugins_filtered[plugin_name].config_create_row(element_type, template, transaction)

    def config_update_row(self, plugin_name, element_type, template, transaction):
        return self.plugins_filtered[plugin_name].config_update_row(element_type, template, transaction)

    def config_delete_row(self, plugin_name, element_type, element_id, dbt):
        return self.plugins_filtered[plugin_name].config_delete_row(element_type, element_id, dbt)

    def get_config_enabling(self, plugin_name, element_type):
        try:
            return self.plugins_filtered[plugin_name].get_config_enabling(element_type)
        except KeyError:
            return True, True

