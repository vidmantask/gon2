"""
Plugin socket for the Report plugin type
"""
from components.plugin.common import socket_base
from plugin_types.server_management import plugin_type_report

class PluginSocket(socket_base.PluginSocketBase):
    def __init__(self, plugin_manager):
        socket_base.PluginSocketBase.__init__(self, plugin_manager)
        self.plugins_filtered = socket_base.PluginSocketBase.plugin_type_filter(plugin_manager.plugins, plugin_type_report.PluginTypeReport)
            
    def get_reports(self):
        reports = []
        for plugin_name in self.plugins_filtered.keys():
            plugin_reports = self.plugins_filtered[plugin_name].get_reports()
            reports.extend(plugin_reports)
        return reports

    def get_report_data(self, plugin_name, data_id, args={}):
        return self.plugins_filtered[plugin_name].get_report_data(data_id, args)

    def get_report_specification(self, plugin_name, report_id, specification_filename):
        if plugin_name in self.plugins_filtered.keys():
            return self.plugins_filtered[plugin_name].get_report_specification(report_id)
        return self.find_report_specification_by_specification_filename(specification_filename)

    def find_report_specification_by_specification_filename(self, specification_filename):
        for plugin_name in self.plugins_filtered.keys():
            report_specification = self.plugins_filtered[plugin_name].find_report_specification_by_specification_filename(specification_filename)
            if report_specification is not None:
                return report_specification
        return None
    