"""
Unittest of plugin socket for Gateway Client env
"""
from __future__ import with_statement
import os
import os.path
import uuid

import sys
import shutil

import unittest
from lib import giri_unittest


import lib.gpm.gpm_env

import components.plugin.client_gateway.plugin_socket_client_runtime_env as plugin_socket_client_runtime_env
import components.plugin.client_gateway.manager
import plugin_types.client_gateway.plugin_type_client_runtime_env

import plugin_modules.soft_token.client_gateway
import plugin_modules.endpoint.client_gateway
import plugin_modules.endpoint.client_gateway_common



class RuntimeEnvSocketTest(unittest.TestCase):

    SERVERS_BASE = """<?xml version='1.0'?>
<servers>
  <connection_group title='Default' selection_delay_sec='1'>
   <connection title='Direct-Connection' host='127.0.0.1' port='13945' timeout_sec='10' type='direct' />
  </connection_group>
</servers>
"""
    SERVERS_CHANGE = """<?xml version='1.0'?>
<servers>
  <connection_group title='Default' selection_delay_sec='1'>
   <connection title='Direct-Connection' host='127.0.0.1' port='13945' timeout_sec='10' type='direct' />
   <connection title='Direct-Connection' host='127.0.0.1' port='13945' timeout_sec='10' type='direct' />
  </connection_group>
</servers>
"""


    def setUp(self):
        self.dev_installation = giri_unittest.UnittestDevEnvInstallation.create()
        self.dev_installation.generate_gpms()

        self.soft_token_root = giri_unittest.mkdtemp()
        self.runtime_env_soft_token = plugin_modules.soft_token.client_gateway.PluginClientRuntimeEnvInstance(giri_unittest.get_checkpoint_handler(), self.soft_token_root)
        self.runtime_env_soft_token.mark_as_unittest()  
        self.runtime_env_soft_token.set_knownsecret_and_servers("x", RuntimeEnvSocketTest.SERVERS_BASE, True)

        self.endpoint_token_root = giri_unittest.mkdtemp()
        self.runtime_env_endpoint_token = plugin_modules.endpoint.client_gateway.PluginClientRuntimeEnvInstance(giri_unittest.get_checkpoint_handler(), self.endpoint_token_root)
        self.runtime_env_endpoint_token.mark_as_unittest()  
        self.runtime_env_endpoint_token.set_knownsecret_and_servers("x", RuntimeEnvSocketTest.SERVERS_BASE, True)

        plugin_modules_root = '.'
        additional_device_roots = [self.soft_token_root, self.endpoint_token_root]
        self.runtime_plugin_manager = components.plugin.client_gateway.manager.Manager(giri_unittest.get_checkpoint_handler_null(), plugin_modules_root)
        self.runtime_plugins = self.runtime_plugin_manager.create_instances(async_service=None, checkpoint_handler=giri_unittest.get_checkpoint_handler_null(), user_interface=None, additional_device_roots=additional_device_roots)
        self.plugin_socket_client_runtime_env = plugin_socket_client_runtime_env.PluginSocket(None, self.runtime_plugins, giri_unittest.get_checkpoint_handler_null())

        self.runtime_env_unittest = []
        runtime_env_ids = self.plugin_socket_client_runtime_env.get_available_ids()
        for runtime_env_id in runtime_env_ids:
            runtime_env = self.plugin_socket_client_runtime_env.get_instance(runtime_env_id)
            if runtime_env.is_unittest():
                self.runtime_env_unittest.append(runtime_env)

    def tearDown(self):
        endpoint_token_deploy = plugin_modules.endpoint.client_gateway_common.EndpointToken(giri_unittest.get_checkpoint_handler(), plugin_modules.endpoint.client_gateway.PluginClientRuntimeEnv.PLUGIN_NAME)
        endpoint_token_deploy.remove_token(self.endpoint_token_root)

    def _restore_download_folder(self):
        download_folder = os.path.join(giri_unittest.mkdtemp(), 'download_cache')
        shutil.copytree(self.dev_installation.get_gpms_folder(), download_folder)
        return download_folder
    
    def test_connection_info(self):
        for runtime_env in self.runtime_env_unittest:
            if runtime_env.support_update_connection_info(giri_unittest.get_dictionary())['supported']:
                print "test connection info, ", runtime_env.get_info().get_id()
                self._do_test_connection_info(runtime_env)
                
    def _do_test_connection_info(self, runtime_env):
        error_handler = lib.gpm.gpm_env.GpmErrorHandler()
        knownsecret_base = uuid.uuid4().get_hex()
        servers_base = RuntimeEnvSocketTest.SERVERS_BASE
        runtime_env.update_connection_info(knownsecret_base, servers_base, error_handler)
        self.assertTrue(error_handler.ok())

        knownsecret_change = uuid.uuid4().get_hex()
        servers_change = RuntimeEnvSocketTest.SERVERS_CHANGE
        runtime_env.update_connection_info(knownsecret_change, servers_change, error_handler)
        self.assertTrue(error_handler.ok())

        knownsecret, servers = runtime_env.get_connection_info()
        self.assertEqual(knownsecret, knownsecret_change)
        self.assertEqual(servers, servers_change)
        self.assertNotEqual(knownsecret, knownsecret_base)
        self.assertNotEqual(servers, servers_base)
        

    def test_installation(self):
        for runtime_env in self.runtime_env_unittest:
            print "test installation, ", runtime_env.get_info().get_id()
            self._do_test_installation(runtime_env)

    def _do_test_installation(self, runtime_env):
        error_handler = lib.gpm.gpm_env.GpmErrorHandler()
        gpm_ids_install = ['c-1-1-noarch']

        class GPMInstallCallback(plugin_types.client_gateway.plugin_type_client_runtime_env.ClientRuntimeEnvInstallCB):
            pass
        
        gpm_installer_cb = GPMInstallCallback()

        knownsecret_base = uuid.uuid4().get_hex()
        servers_base = RuntimeEnvSocketTest.SERVERS_BASE
        generate_keypair = True
        runtime_env.set_download_root(self._restore_download_folder())
        runtime_env.install_gpms_clean(gpm_ids_install, gpm_installer_cb, error_handler, giri_unittest.get_dictionary(), knownsecret_base, servers_base, generate_keypair)
        if not error_handler.ok():
            error_handler.dump()
        self.assertTrue(error_handler.ok())

        gpm_error_handler = lib.gpm.gpm_env.GpmErrorHandler()
        gpm_reporitory = lib.gpm.gpm_analyzer.GpmRepository()
        gpm_reporitory.init_using_cache(gpm_error_handler, self.dev_installation.get_gpms_folder(), self.dev_installation.get_gpmcdefs_folder())

        gpm_ids_install = []
        gpm_ids_update = [('c-1-1-noarch', 'c-1-2-noarch')]
        gpm_ids_remove = ['c-1-1-noarch']
        gpms_meta = gpm_reporitory._meta_data
        install_state = None
        knownsecret_change = uuid.uuid4().get_hex()
        servers_change = RuntimeEnvSocketTest.SERVERS_CHANGE
        
        support_install = runtime_env.support_install_gpms(gpm_ids_install, gpm_ids_update, gpm_ids_remove, gpms_meta, install_state, giri_unittest.get_dictionary(), knownsecret_change, servers_change)
        if support_install['supported']:
            
            runtime_env.set_download_root(self._restore_download_folder())
            runtime_env.install_gpms(gpm_ids_install, gpm_ids_update, gpm_ids_remove, gpms_meta, gpm_installer_cb, install_state, error_handler, giri_unittest.get_dictionary(), None, None)
            if not error_handler.ok():
                error_handler.dump()
            self.assertTrue(error_handler.ok())

            knownsecret, servers = runtime_env.get_connection_info()
            self.assertNotEqual(knownsecret, knownsecret_change)
            self.assertNotEqual(servers, servers_change)
            self.assertEqual(knownsecret, knownsecret_base)
            self.assertEqual(servers, servers_base)

            runtime_env.set_download_root(self._restore_download_folder())
            runtime_env.install_gpms(gpm_ids_install, gpm_ids_update, gpm_ids_remove, gpms_meta, gpm_installer_cb, install_state, error_handler, giri_unittest.get_dictionary(), None, servers_change)
            knownsecret, servers = runtime_env.get_connection_info()
            self.assertEqual(servers, servers_change)
            self.assertNotEqual(servers, servers_base)
            self.assertEqual(knownsecret, knownsecret_base)
            self.assertNotEqual(knownsecret, knownsecret_change)
        
        
#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
