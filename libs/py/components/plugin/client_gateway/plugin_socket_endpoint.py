"""
Plugin socket for the endpoint plugin type
"""
from components.plugin.common import socket_base
from plugin_types.client_gateway import plugin_type_endpoint
import lib.checkpoint

class PluginSocketConnector(plugin_type_endpoint.IPluginTypeEndpointCallback):
    def __init__(self, plugin_name, plugin_socket):
        self.plugin_name = plugin_name
        self.plugin_socket = plugin_socket
        
    def report(self, endpoint_info):
        self.plugin_socket._report(self.plugin_name, endpoint_info)

    def report_some(self, endpoint_infos):
        self.plugin_socket._report_some(self.plugin_name, endpoint_infos)

    def done(self):
        self.plugin_socket._done(self.plugin_name)

    def done_hash(self):
        self.plugin_socket._done_hash(self.plugin_name)
    
    
class IPluginSocketCallback(object):
    def report(self, plugin_name, endpoint_info):
        """
        A plugin reports a single endpoint_info
        """
        raise NotImplementedError

    def report_some(self, plugin_name, endpoint_infos):
        """
        A plugin reports a list of endpoint_info's
        """
        raise NotImplementedError


    def all_done(self):
        """
        All plugin are done
        """
        raise NotImplementedError


class PluginSocket(socket_base.PluginSocketBase):
    def __init__(self, plugin_manager, plugins, callback):
        socket_base.PluginSocketBase.__init__(self, plugin_manager)
        self.callback = callback
        self.plugins_filtered = socket_base.PluginSocketBase.plugin_type_filter(plugins, plugin_type_endpoint.PluginTypeEndpoint)
        self.plugins_filtered_connectors = {}
        self.plugins_filtered_done = {}
        self.plugins_filtered_done_hash = dict([(name, False) for name in self.plugins_filtered.keys()])
        for plugin_name in self.plugins_filtered.keys():
            self.plugins_filtered_connectors[plugin_name] = PluginSocketConnector(plugin_name, self)
            self.plugins_filtered[plugin_name].set_callback(self.plugins_filtered_connectors[plugin_name])
            self.plugins_filtered_done[plugin_name] = False
        
    def run_all(self):
        """
        This method start all plugin as seperate threads
        """
        for plugin in self.plugins_filtered.values():
            plugin.start()
            
    def is_all_done(self):
        """
        This method returns true if all plugins has reported that they are done
        """
        for plugin_name in self.plugins_filtered.keys():
            if not self.plugins_filtered_done[plugin_name]:
                self.plugin_manager.checkpoint_handler.Checkpoint('is_all_done', "plugin_socket_endpoint", lib.checkpoint.DEBUG, plugin_not_done=plugin_name)
                return False
        self.plugin_manager.checkpoint_handler.Checkpoint('is_all_done', "plugin_socket_endpoint", lib.checkpoint.DEBUG, done=True)
        return True

    def is_all_done_hash(self):
        return False not in self.plugins_filtered_done_hash.values()

    def _report(self, plugin_name, endpoint_info):
        if self.callback is not None:
            self.callback.report(plugin_name, endpoint_info)

    def _report_some(self, plugin_name, endpoint_infos):
        if self.callback is not None:
            self.callback.report_some(plugin_name, endpoint_infos)

    def _done(self, plugin_name):
        self.plugins_filtered_done[plugin_name] = True
        self._done_hash(plugin_name)
        if self.is_all_done():
            self.callback.all_done()

    def _done_hash(self, plugin_name):
        try:
            self.plugins_filtered_done_hash[plugin_name] = True
        except KeyError:
            return
        if self.is_all_done_hash():
            self.callback.all_done_hash()

    