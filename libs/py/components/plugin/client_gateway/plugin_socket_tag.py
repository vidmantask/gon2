"""
Plugin socket for the Tag plugin type
"""
from components.communication import tunnel_endpoint_base 

from components.plugin.common import socket_base
from plugin_types.client_gateway import plugin_type_tag

class PluginSocket(socket_base.PluginSocketBase, tunnel_endpoint_base.TunnelendpointSession):
    def __init__(self, async_service, checkpoint_handler, tunnel_endpoint, plugin_manager, plugins):
        tunnel_endpoint_base.TunnelendpointSession.__init__(self, async_service, checkpoint_handler, tunnel_endpoint)
        socket_base.PluginSocketBase.__init__(self, plugin_manager)
        self.plugins_filtered = socket_base.PluginSocketBase.plugin_type_filter(plugins, plugin_type_tag.PluginTypeTag)
        self.checkpoint_handler = checkpoint_handler
    
    def session_start_and_connected(self):
        """
        This is called when tunnelendpoint_connected and session_start both has been called
        """ 
        child_id = 1
        for plugin_name in self.plugins_filtered.keys():
            self.plugins_filtered[plugin_name].set_tunnelendpoint(self.create_and_add_tunnelendpoint_tunnel(self.checkpoint_handler, child_id))
            self.tunnelendpoint_remote('remote_tag_plugin_created', plugin_name=plugin_name, child_id=child_id)
            child_id += 1
        self.tunnelendpoint_remote('remote_all_tag_plugins_created')

    def session_close(self):
        """
        Hard close of session. 
        Called from main session when communication is terminated
        """
        pass
