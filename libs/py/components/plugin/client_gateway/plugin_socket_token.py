"""
Plugin socket for the token plugin type
"""
from components.plugin.common import socket_base
from plugin_types.client_gateway import plugin_type_token


class PluginSocket(socket_base.PluginSocketBase):
    def __init__(self, plugin_manager, plugins):
        socket_base.PluginSocketBase.__init__(self, plugin_manager)
        self.plugins_filtered = socket_base.PluginSocketBase.plugin_type_filter(plugins, plugin_type_token.PluginTypeToken)
        
    def get_token_types(self):
        return self.plugins_filtered.keys()

    def initialize_token(self, plugin_name, token_id):
        try:
            self.plugins_filtered[plugin_name].initialize_token(token_id)
        except KeyError:
            raise plugin_type_token.Error("The token type(plugin_name) '%s' is not found" % plugin_name)
    
    def get_tokens(self, additional_device_roots=None):
        tokens = []
        for plugin in self.plugins_filtered.values():
            plugin_tokens = plugin.get_tokens(additional_device_roots)
            tokens.extend(plugin_tokens)
        return tokens

    def get_tokens_by_distance(self, client_runtime_env):
        tokens = self.get_tokens()
        plugin_type_token.Token.sort_by_distance(tokens, client_runtime_env)
        return tokens

    def deploy_token(self, plugin_name, token_id, client_knownsecret, servers):
        try:
            self.plugins_filtered[plugin_name].deploy_token(token_id, client_knownsecret, servers)
        except KeyError:
            raise plugin_type_token.Error("The token type(plugin_name) '%s' is not found" % plugin_name)

    def generate_keypair(self, plugin_name, token_id):
        try:
            self.plugins_filtered[plugin_name].generate_keypair(token_id)
        except KeyError:
            raise plugin_type_token.Error("The token type(plugin_name) '%s' is not found" % plugin_name)

    def set_serial(self, plugin_name, token_id, serial):
        try:
            return self.plugins_filtered[plugin_name].set_serial(token_id, serial)
        except KeyError:
            raise plugin_type_token.Error("The token type(plugin_name) '%s' is not found" % plugin_name)

    def get_serial(self, plugin_name, token_id):
        try:
            return self.plugins_filtered[plugin_name].get_serial(token_id)
        except KeyError:
            raise plugin_type_token.Error("The token type(plugin_name) '%s' is not found" % plugin_name)

    def get_public_key(self, plugin_name, token_id):
        try:
            return self.plugins_filtered[plugin_name].get_public_key(token_id)
        except KeyError:
            raise plugin_type_token.Error("The token type(plugin_name) '%s' is not found" % plugin_name)

    def set_enrolled(self, plugin_name, token_id):
        try:
            self.plugins_filtered[plugin_name].set_enrolled(token_id)
        except KeyError:
            raise plugin_type_token.Error("The token type(plugin_name) '%s' is not found" % plugin_name)

    def is_enrolled(self, plugin_name, token_id):
        try:
            self.plugins_filtered[plugin_name].is_enrolled(token_id)
        except KeyError:
            raise plugin_type_token.Error("The token type(plugin_name) '%s' is not found" % plugin_name)

    def reset_enrolled(self, plugin_name, token_id):
        try:
            self.plugins_filtered[plugin_name].reset_enrolled(token_id)
        except KeyError:
            raise plugin_type_token.Error("The token type(plugin_name) '%s' is not found" % plugin_name)

    def check_access(self, plugin_name, token_id):
        try:
            self.plugins_filtered[plugin_name].check_access(token_id)
        except KeyError:
            raise plugin_type_token.Error("The token type(plugin_name) '%s' is not found" % plugin_name)

    def create_token_endpoint(self, installation_root):
        plugin_id = u'endpoint_token'
        token = plugin_type_token.Token(plugin_id, installation_root, plugin_type_token.Token.TOKEN_STATUS_INITIALIZED, None, token_title="Computer User Token", token_plugin_module_name='endpoint')
        return token

    def get_knownsecret_and_servers(self, client_runtime_env):
        tokens = self.get_tokens_by_distance(client_runtime_env)
        for token in tokens:
            if token.token_distance in [plugin_type_token.Token.TOKEN_DISTANCE_CURRENT, plugin_type_token.Token.TOKEN_DISTANCE_CLOSE]:
                (knownsecret, servers) = self.plugins_filtered[token.token_type].get_knownsecret_and_servers(token.token_id)
                if knownsecret is not None and servers is not None:
                    return (knownsecret.encode('utf-8'), servers.encode('utf-8'))
        return (None, None)

    def set_servers(self, client_runtime_env, servers):
        tokens = self.get_tokens_by_distance(client_runtime_env)
        for token in tokens:
            if token.token_distance in [plugin_type_token.Token.TOKEN_DISTANCE_CURRENT]:
                return self.plugins_filtered[token.token_type].set_servers(token.token_id, servers)
        return 'Unable to locate current token'
        
