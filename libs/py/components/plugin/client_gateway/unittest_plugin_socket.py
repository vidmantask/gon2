"""
Unittest of plugin sockets for Gateway Client 
"""
from __future__ import with_statement
import os
import os.path

import sys
import shutil
import tempfile

import unittest
from lib import giri_unittest
from lib import checkpoint
from lib import cryptfacility


import lib.gpm.gpm_env

import components.plugin.client_gateway.manager
import plugin_types.client_gateway.plugin_type_client_runtime_env

import components.plugin.client_gateway.plugin_socket_client_runtime_env as plugin_socket_client_runtime_env
import components.plugin.client_gateway.plugin_socket_token as plugin_socket_token



class Socket_test(unittest.TestCase):

    def setUp(self):
        self.plugin_manager = components.plugin.client_gateway.manager.Manager(giri_unittest.get_checkpoint_handler(), giri_unittest.PY_ROOT)
        self.plugins = self.plugin_manager.create_instances(async_service=None, checkpoint_handler=giri_unittest.get_checkpoint_handler(), user_interface=None, additional_device_roots=[])
        
        self.plugin_socket_runtime_env = plugin_socket_client_runtime_env.PluginSocket(self.plugin_manager, self.plugins, giri_unittest.get_checkpoint_handler())
        self.plugin_socket_token = plugin_socket_token.PluginSocket(self.plugin_manager, self.plugins)

    def tearDown(self):
        pass

    def test_get_tokens_and_distance(self):
        runtime_env_ids = self.plugin_socket_runtime_env.get_available_ids()
        for runtime_env_id in runtime_env_ids:
            print runtime_env_id
            runtime_env = self.plugin_socket_runtime_env.get_instance(runtime_env_id)
            tokens = self.plugin_socket_token.get_tokens_by_distance(runtime_env)
            for token in tokens:
                print "  ", token.token_distance, token.token_type, token.token_id, token.runtime_env_id
            (knownsecret, servers) = self.plugin_socket_token.get_knownsecret_and_servers(runtime_env) 
            self.assertNotEqual(knownsecret, None)
            self.assertNotEqual(servers, None)
            

        
#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'
GIRI_UNITTEST_TIMEOUT = 60 * 4

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
