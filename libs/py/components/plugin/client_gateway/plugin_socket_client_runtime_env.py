"""
Plugin socket for the Gateway Client env plugin type
"""
import lib.checkpoint
import lib.appl.gon_os

from components.plugin.common import socket_base
from plugin_types.client_gateway import plugin_type_client_runtime_env

module_id = 'PluginSocketClientRuntimeEnv'

class PluginSocket(socket_base.PluginSocketBase):
    def __init__(self, plugin_manager, plugins, checkpoint_handler):
        socket_base.PluginSocketBase.__init__(self, plugin_manager)
        self.plugins_filtered = socket_base.PluginSocketBase.plugin_type_filter(plugins, plugin_type_client_runtime_env.PluginTypeClientRuntimeEnv)
        self.checkpoint_handler = checkpoint_handler
        
    def _encode_id(self, plugin_name, client_runtime_env_id):
        return u'%s::%s' % (plugin_name, client_runtime_env_id)
    
    def _decode_id(self, client_runtime_env_id):
        if client_runtime_env_id.find('::') != -1:
            (plugin_name, sep, client_runtime_env_id) = client_runtime_env_id.partition('::')
            return (plugin_name, client_runtime_env_id)
        return (None, None)
        
    def get_current_instance(self):
        for plugin_name in self.plugins_filtered.keys():
            plugin_client_runtime_env_ids = self.plugins_filtered[plugin_name].get_runtime_env_ids()
            for plugin_client_runtime_env_id in plugin_client_runtime_env_ids:
                runtime_env_instance = self.plugins_filtered[plugin_name].get_instance(plugin_client_runtime_env_id)
                if runtime_env_instance.is_current():
                    return (self._encode_id(plugin_name, plugin_client_runtime_env_id), runtime_env_instance)
        #
        # For gonos take the first token and force it to current
        #
        if lib.appl.gon_os.is_gon_os():
            for plugin_name in self.plugins_filtered.keys():
                plugin_client_runtime_env_ids = self.plugins_filtered[plugin_name].get_runtime_env_ids()
                for plugin_client_runtime_env_id in plugin_client_runtime_env_ids:
                    runtime_env_instance = self.plugins_filtered[plugin_name].get_instance(plugin_client_runtime_env_id)
                    runtime_env_instance.force_to_current();
                    return (self._encode_id(plugin_name, plugin_client_runtime_env_id), runtime_env_instance)
                
        return (None, None)

    def get_relocated_instance(self, installation_root):
        for plugin_name in self.plugins_filtered.keys():
            plugin_client_runtime_env_ids = self.plugins_filtered[plugin_name].get_runtime_env_ids()
            for plugin_client_runtime_env_id in plugin_client_runtime_env_ids:
                runtime_env_instance = self.plugins_filtered[plugin_name].get_instance(plugin_client_runtime_env_id)
                if runtime_env_instance.is_current_relocated(installation_root):
                    return (self._encode_id(plugin_name, plugin_client_runtime_env_id), runtime_env_instance)
        return (None, None)

    def get_instance(self, client_runtime_env_id):
        (plugin_name, client_runtime_env_id_plugin) = self._decode_id(client_runtime_env_id) 
        runtime_env_instance = None
        if self.plugins_filtered.has_key(plugin_name):
            runtime_env_instance = self.plugins_filtered[plugin_name].get_instance(client_runtime_env_id_plugin)
        if runtime_env_instance == None:
            self.checkpoint_handler.Checkpoint("get_instance.error", module_id, lib.checkpoint.ERROR, client_runtime_env_id=client_runtime_env_id, available_client_runtime_env_ids='%s'%self.get_available_ids())
        return runtime_env_instance

    def get_available_ids(self):
        available_runtime_env_ids = []
        for plugin_name in self.plugins_filtered.keys():
            runtime_env_ids = self.plugins_filtered[plugin_name].get_runtime_env_ids()
            for runtime_env_id in runtime_env_ids:
                available_runtime_env_ids.append(self._encode_id(plugin_name, runtime_env_id))
        return available_runtime_env_ids

    def get_info(self, client_runtime_env_id):
        instance = self.get_instance(client_runtime_env_id)
        if instance != None:
            info = instance.get_info()
            info.id = client_runtime_env_id
            return info
        return None

    def get_in_use_ids(self, timeout_sec=None):
        in_use_runtime_env_ids = []
        for runtime_env_id  in self.get_available_ids():
            runtime_env = self.get_instance(runtime_env_id)
            if runtime_env is not None:
                if runtime_env.is_in_use(timeout_sec):
                    in_use_runtime_env_ids.append(runtime_env_id)
        return in_use_runtime_env_ids

    def get_installed_gpm_meta_all(self, client_runtime_env_id, error_handler):
        instance = self.get_instance(client_runtime_env_id)
        if instance != None:
            return instance.get_installed_gpm_meta_all(error_handler)
        return None

    def install_gpms_clean(self, client_runtime_env_id, gpm_ids_install, cb, error_handler, dictionary, knownsecret, servers, generate_keypair):
        instance = self.get_instance(client_runtime_env_id)
        if instance != None:
            return instance.install_gpms_clean(gpm_ids_install, cb, error_handler, dictionary, knownsecret, servers, generate_keypair)
        return False

    def create_runtime_env_instance_endpoint(self, checkpoint_handler, root):
        plugin_id = u'endpoint_token_client_runtime_env'
        if  self.plugins_filtered.has_key(plugin_id):
            return self.plugins_filtered[plugin_id].create_instance(checkpoint_handler, root)
        return None
