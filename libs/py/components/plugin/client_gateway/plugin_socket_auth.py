"""
Plugin socket for the Auth plugin type
"""
from components.plugin.common import socket_base
from plugin_types.client_gateway import plugin_type_auth


class PluginSocket(socket_base.PluginSocketBase):
    def __init__(self, plugin_manager, plugins, current_runtime_env):
        socket_base.PluginSocketBase.__init__(self, plugin_manager)
        self.plugins_filtered = socket_base.PluginSocketBase.plugin_type_filter(plugins, plugin_type_auth.PluginTypeAuth)
        self._set_current_runtime_env(current_runtime_env)
        
    def set_tunnelendpoint(self, plugin_name, tunnel_endpoint):
        return self.plugins_filtered[plugin_name].set_tunnelendpoint(tunnel_endpoint)
    
    def _set_current_runtime_env(self, current_runtime_env):
        for plugin in self.plugins_filtered.values():
            plugin.set_current_runtime_env(current_runtime_env)
