"""
Module manager for client plugin modules
"""
from __future__ import with_statement
import os.path

import lib.checkpoint

from components.plugin.common import manager_base
import plugin_types.client_gateway


class Manager(manager_base.ManagerBase):
    def __init__(self, checkpoint_handler, import_path):
        manager_base.ManagerBase.__init__(self, checkpoint_handler, import_path)
        
        self.plugin_classes = []
        self.plugins = {}
        with self.checkpoint_handler.CheckpointScope('init', manager_base.module_id, lib.checkpoint.DEBUG, plugin_root=self.plugin_root):
            if os.path.exists(self.plugin_root):
                self.plugin_classes = self.load_plugin_classes('client_gateway', plugin_types.client_gateway.PluginTypeGatewayClient)
            else:
                self.checkpoint_handler.Checkpoint('init.error', manager_base.module_id, lib.checkpoint.ERROR, message='Plugin root not found')
        
    def create_instances(self, **arguments):
        self.plugins = self.create_plugin_instances(self.plugin_classes, **arguments)
        return self.plugins

        
if __name__ == '__main__':
    checkpoint_handler = lib.checkpoint.CheckpointHandler.get_cout_all()
    mm = Manager(checkpoint_handler, '/home/twa/source/hg_checkout/M2/py')
    mm.create_instances(user_interface=None)