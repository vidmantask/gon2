"""
Common functionality for all plugin sockets
"""

class PluginSocketBase(object):
    def __init__(self, plugin_manager):
        self.plugin_manager = plugin_manager
        
    @classmethod    
    def plugin_type_filter(self, plugins, plugin_type):
        """
        Filter module instances for modules of a given base class
        """
        result = {}
        for plugin_name in plugins.keys():
            if isinstance(plugins[plugin_name], plugin_type):
                result[plugin_name] = plugins[plugin_name]
        return result

