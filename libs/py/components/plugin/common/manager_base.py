"""
Base module manager
"""
from __future__ import with_statement

import sys
import types

import os
import os.path
import importlib

import lib.checkpoint

import plugin_types.common

module_id = 'plugin_manager'


class ManagerBase(object):
    def __init__(self, checkpoint_handler, import_path):
        self.checkpoint_handler = checkpoint_handler
        self.import_path = os.path.abspath(import_path)
        self.plugin_root = os.path.join(self.import_path, 'plugin_modules')
        if sys.platform in ['darwin', 'linux2']:
            sys.path.insert(0, import_path)

    def load_plugin_classes(self, giri_module_name, plugin_type):
        py_module_plugin_classes = []
        for module_name in os.listdir(self.plugin_root):
            plugin_module_folder_abs = os.path.join(self.plugin_root, module_name)
            if os.path.isdir(plugin_module_folder_abs):
                with self.checkpoint_handler.CheckpointScope('load_plugin_module', module_id, lib.checkpoint.DEBUG, module_name = module_name) as cpc:
                    py_module = self._load_plugin_module(cpc, module_name)
                    if py_module != None:
                        py_module_plugin_classes.extend(self._load_plugin_module_plugins(module_name, giri_module_name, plugin_type))
        return py_module_plugin_classes

    def load_plugin_clases_from_module(self, py_module, plugin_type):
        py_module_plugin_classes = []
        py_module_name = py_module.__name__
        with self.checkpoint_handler.CheckpointScope('load_plugin_clases_from_module', module_id, lib.checkpoint.DEBUG, py_module_name=py_module_name):
            try:
                for py_plugin_name in py_module.__dict__:
                    py_plugin = getattr(py_module, py_plugin_name)
                    if isinstance(py_plugin, types.TypeType) and issubclass(py_plugin, plugin_type):
                        self.checkpoint_handler.Checkpoint("load_plugin", module_id, lib.checkpoint.DEBUG, class_name=py_plugin.__name__)
                        py_module_plugin_classes.append(py_plugin)
            except :
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("load_plugin_module.error", module_id, lib.checkpoint.ERROR, etype, evalue, etrace)
        return py_module_plugin_classes


    def _load_plugin_module(self, cpc, module_name):
        py_module_name = 'plugin_modules.%s' % module_name
        with self.checkpoint_handler.CheckpointScope('load_plugin_module', module_id, lib.checkpoint.DEBUG, module_name = module_name, py_module_name=py_module_name) as cpc:
            try:
#                py_module = __import__(py_module_name, fromlist=[''], globals=globals(), locals=locals())
                py_module = importlib.import_module(py_module_name)
                plugin_module_description = getattr(py_module, 'plugin_module_description')
                plugin_module_author = getattr(py_module, 'plugin_module_author')
                plugin_module_version = getattr(py_module, 'plugin_module_version')
                cpc.add_complete_attr(py_module_name=py_module_name, plugin_module_description=plugin_module_description, plugin_module_author=plugin_module_author)
                return py_module
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.Checkpoint("load_plugin_module.error", module_id, lib.checkpoint.ERROR, py_module_name=py_module_name)
                self.checkpoint_handler.CheckpointException("load_plugin_module.error", module_id, lib.checkpoint.ERROR, etype, evalue, etrace)
            return None


#        with self.checkpoint_handler.CheckpointScope('load_plugin_module_plugins', module_id, lib.checkpoint.DEBUG, module_name=module_name, py_module_name=py_module_name) as cpc:

    def _load_plugin_module_plugins(self, module_name, giri_module_name, plugin_type):
        py_module_plugin_classes = []
        py_module_folder = os.path.join(self.plugin_root, '%s/%s' % (module_name, giri_module_name))
        if os.path.exists(py_module_folder):
            py_module_name = 'plugin_modules.%s.%s' % (module_name, giri_module_name)
            py_module_package = 'plugin_modules.%s' % (module_name)
            with self.checkpoint_handler.CheckpointScope('load_plugin_module_plugins', module_id, lib.checkpoint.DEBUG, module_name=module_name, py_module_name=py_module_name):
                try:
#                    self.checkpoint_handler.Checkpoint("load_plugin_module.error", module_id, lib.checkpoint.DEBUG, out=str(sys.modules))
#                    py_module = __import__(py_module_name)

                    py_module = importlib.import_module(py_module_name)
                    for py_plugin_name in py_module.__dict__:
                        py_plugin = getattr(py_module, py_plugin_name)
                        if isinstance(py_plugin, types.TypeType) and issubclass(py_plugin, plugin_type):
                            self.checkpoint_handler.Checkpoint("load_plugin", module_id, lib.checkpoint.DEBUG, class_name=py_plugin.__name__, module_file=py_module.__file__)
                            py_module_plugin_classes.append(py_plugin)
                except :
                    (etype, evalue, etrace) = sys.exc_info()
                    self.checkpoint_handler.CheckpointException("load_plugin_module.error", module_id, lib.checkpoint.ERROR, etype, evalue, etrace)
        return py_module_plugin_classes

    def create_plugin_instances(self, plugin_classes, **arguments):
        py_plugin_instances = {}
        for py_plugin_class in plugin_classes:
            try:
                if not py_plugin_class.__dict__.has_key('generator'):
                    py_plugin_instance = py_plugin_class(**arguments)
                    py_plugin_instance_name = py_plugin_instance.plugin_name
                    py_plugin_instances[py_plugin_instance_name] = py_plugin_instance
                    self.checkpoint_handler.Checkpoint("create_plugin_instance", module_id, lib.checkpoint.DEBUG, plugin_class=py_plugin_instance.__class__.__name__, plugin_name=py_plugin_instance_name)
                else:
                    instances = py_plugin_class.generator(**arguments)
                    for instance_id in instances.keys():
                        self.checkpoint_handler.Checkpoint("create_plugin_instance_by_generator", module_id, lib.checkpoint.DEBUG, plugin_class=py_plugin_class.__name__, plugin_name=instances[instance_id].plugin_name)
                    py_plugin_instances.update(instances)
            except :
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.Checkpoint("create_plugin_instance.error", module_id, lib.checkpoint.ERROR, plugin_module=py_plugin_class.__module__, plugin_class=py_plugin_class.__name__)
                self.checkpoint_handler.CheckpointException("create_plugin_instance.error", module_id, lib.checkpoint.ERROR, etype, evalue, etrace)
        return py_plugin_instances
