"""
Plugin socket for the Upgrade plugin type
"""
from components.plugin.common import socket_base
from plugin_types.server_config import plugin_type_upgrade

import sys
import lib.checkpoint

class PluginSocket(socket_base.PluginSocketBase):
    def __init__(self, plugin_manager):
        socket_base.PluginSocketBase.__init__(self, plugin_manager)
        self.plugins_filtered = socket_base.PluginSocketBase.plugin_type_filter(plugin_manager.plugins, plugin_type_upgrade.PluginTypeUpgrade)

    def upgrade(self, from_version, to_version, database_api, transaction, restore_path, checkpoint_handler):
        for plugin_name in self.plugins_filtered.keys():
            try:
                self.plugins_filtered[plugin_name].upgrade(from_version, to_version, database_api, transaction, restore_path)
            except:
                (etype, evalue, etrace) = sys.exc_info()
                checkpoint_handler.CheckpointException("PluginSocket.upgrade", plugin_name, lib.checkpoint.CRITICAL, etype, evalue, etrace)
                return False
        return True

    def upgrade_config(self, from_version, to_version, restore_path_config):
        for plugin_name in self.plugins_filtered.keys():
            self.plugins_filtered[plugin_name].upgrade_config(from_version, to_version, restore_path_config)

    def import_schema(self):
        for plugin_name in self.plugins_filtered.keys():
            self.plugins_filtered[plugin_name].import_schema()
        