"""
Plugin socket for the config specification plugin type
"""
import os
import os.path
import sys
import lib.checkpoint

from components.plugin.common import socket_base
from plugin_types.server_config import plugin_type_config_specification

class PluginSocket(socket_base.PluginSocketBase):
    PLUGIN_VERSION_FILENAME = 'version'
    
    def __init__(self, plugin_manager):
        socket_base.PluginSocketBase.__init__(self, plugin_manager)
        self.plugins_filtered = socket_base.PluginSocketBase.plugin_type_filter(plugin_manager.plugins, plugin_type_config_specification.PluginTypeConfigSpecification)

    def get_config_specifications(self):
        specifications = []
        for plugin_name in self.plugins_filtered.keys():
            specifications.append(self.plugins_filtered[plugin_name].get_config_specification())
        return specifications
    
    def get_plugins(self):
        plugins = []
        for plugin_name in self.plugins_filtered.keys():
            plugins.append(self.plugins_filtered[plugin_name])
        return plugins

    def get_plugins_sorted(self):
        plugins = self.get_plugins()
        return sorted(plugins, cmp=self.compare_plugins)
    
    def compare_plugins(self, plugin1, plugin2):
        plugin_type1, priority1 = plugin1.get_type_and_priority()
        plugin_type2, priority2 = plugin2.get_type_and_priority()
        if plugin_type1 > plugin_type2:
            return 1
        elif plugin_type1 < plugin_type2:
            return -1
        else:
            if priority1 > priority2:
                return 1
            elif priority1 < priority2:
                return -1
            else:
                return 0
    
    
    def generate_support_package(self, support_package_root):
        for plugin_name in self.plugins_filtered.keys():
            support_package_plugin_root = os.path.join(support_package_root, plugin_name)
            os.makedirs(support_package_plugin_root)
            self._generate_version_file(support_package_root, self.plugins_filtered[plugin_name])
            self.plugins_filtered[plugin_name].generate_support_package(support_package_plugin_root)

    def backup(self, backup_log, config_backup_root_abs, cb_config_backup, alternative_server_configuration_all):
        for plugin_name in self.plugins_filtered.keys():
            try:
                backup_package_plugin_root = os.path.join(config_backup_root_abs, plugin_name)
                os.makedirs(backup_package_plugin_root)
                self._generate_version_file(backup_package_plugin_root, self.plugins_filtered[plugin_name])
                self.plugins_filtered[plugin_name].backup(backup_log, backup_package_plugin_root, cb_config_backup, alternative_server_configuration_all)
            except:
                (etype, evalue, etrace) = sys.exc_info()
                backup_log.CheckpointException("PluginSocket.backup", plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
                cb_config_backup.cb_backup_error('')

    def restore(self, restore_log, restore_root_abs, cb_config_restore):
        for plugin_name in self.plugins_filtered.keys():
            try:
                restore_plugin_root = os.path.join(restore_root_abs, plugin_name)
                self.plugins_filtered[plugin_name].restore(restore_log, restore_plugin_root, cb_config_restore)
            except:
                (etype, evalue, etrace) = sys.exc_info()
                restore_log.CheckpointException("PluginSocket.restore", plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
                cb_config_restore.cb_restore_error('')

    def _generate_version_file(self, root, plugin):
        version_file_filename = os.path.join(root, PluginSocket.PLUGIN_VERSION_FILENAME)
        version_file_file = open(version_file_filename, 'wt')
        version_file_file.write(plugin.get_version().get_version_string_num())
        version_file_file.close()
