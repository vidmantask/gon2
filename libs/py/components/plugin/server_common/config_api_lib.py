from __future__ import with_statement
from components.rest.rest_api import RestApi
import lib.commongon as commongon


class ConfigTemplateSpec(object):

    FIELD_TYPE_EDIT = 'edit'
    FIELD_TYPE_CUSTOM = 'custom_template'
    FIELD_TYPE_HIDDEN = 'hidden'

    VALUE_TYPE_STRING = 'string_value_type'
    VALUE_TYPE_INTEGER = 'integer_value_type'
    VALUE_TYPE_BOOLEAN = 'boolean_value_type'
    VALUE_TYPE_TEXT = 'text_value_type'
    VALUE_TYPE_DATE = 'date_value_type'
    VALUE_TYPE_DATETIME = 'datetime_value_type'
    VALUE_TYPE_TIME = 'time_value_type'

    def __init__(self, config_specification=None):
        if not config_specification:
            self.config_specification = RestApi.Admin.ConfigTemplate.ConfigurationTemplate()
            self.config_specification.set_field([])
        else:
            self.config_specification = config_specification

        self.fields = dict()
        for field in self.config_specification.get_field:
            self.fields[field.get_name] = field


    def init(self, name, title, description, entity_type=None):
        self.config_specification = RestApi.Admin.ConfigTemplate.ConfigurationTemplate()
        self.config_specification.set_name(name)
        self.config_specification.set_title(title)
        self.config_specification.set_entity_type(entity_type)
        self.config_specification.set_description(description)
        self.config_specification.set_use_as_template(False)

    def set_title(self, title):
        self.config_specification.set_title(title)

    def set_read_only(self):
        self.config_specification.set_read_only(True)


    def get_template(self):
        return self.config_specification

    def set_use_as_template(self, use_as_template):
        self.config_specification.set_use_as_template(use_as_template)


    def _create_field(self, name, title, value_type, field_type, advanced, read_only):
        field = RestApi.Admin.ConfigTemplate.FieldElement()
        field.set_field_type(field_type)
        field.set_value_type(value_type)
        field.set_name(name)
        field.set_title(title)
        field.set_advanced(advanced)
        field.set_read_only(read_only)
        self.config_specification.get_field.append(field)
        self.fields[name] = field
        return field

    def add_field_def(self, field):
        self.config_specification.get_field.append(field)
        self.fields[field.get_name] = field

    def add_portscan_action(self, field, port_field_name, title=None):
        action = RestApi.Admin.ConfigTemplate.FieldAction()
        portscan = RestApi.Admin.ConfigTemplate.PortscanAction()
        portscan.set_port_field(port_field_name)
        if title:
            portscan.set_title(title)
        action.set_portscan(portscan)
        field.set_action(action)


    def add_test_config_action(self, field, module_id, sub_type=None, title=None, tooltip=None, test_type=None):
        action = RestApi.Admin.ConfigTemplate.FieldAction()
        test_action = RestApi.Admin.ConfigTemplate.TestConfigAction()
        test_action.set_module_id(module_id)
        test_action.set_sub_type(sub_type)
        if title:
            test_action.set_title(title)
        if tooltip:
            test_action.set_tooltip(tooltip)
        if test_type:
            test_action.set_test_type(test_type)
        action.set_test_config(test_action)
        field.set_action(action)

    def add_field(self, name, title, value_type, field_type=FIELD_TYPE_EDIT, value=None, advanced=False, mandatory=False, read_only=False, tooltip=None, section=None):
        if not self.fields.get(name):
            field = self._create_field(name, title, value_type, field_type, advanced, read_only)
            if not value is None:
                self.set_field_value(field, value)
            if mandatory:
                field.set_mandatory(True)
            if tooltip:
                field.set_tooltip(tooltip)
            if section:
                field.set_section(section)
            return field
        return None

    def add_fields(self, field_defs):
        fields_not_created = []
        for field_def in field_defs:
            name = field_def.get("name")
            title = field_def.get("title")
            value_type = field_def.get("value_type")
            field_type = field_def.get("field_type", ConfigTemplateSpec.FIELD_TYPE_EDIT)
            value = field_def.get("value", None)
            advanced = field_def.get("advanced", False)
            read_only = field_def.get("read_only", False)
            field = self.add_field(name, title, value_type, field_type, value, advanced, read_only)
            if not field:
                fields_not_created.append(field_def)
            else:
                selection = field_defs.get("selection", None)
                if selection:
                    for selection_title, selection_value in selection:
                        self.add_selection_choice(field, selection_title, selection_value)
                max_length = field_defs.get("max_length", None)
                if max_length:
                    field.set_max_length(max_length)
                read_only = field_defs.get("read_only", None)
                if read_only:
                    field.set_read_only(read_only)
        return fields_not_created


    def set_field_value(self, field, value):
        if value is None:
            str_value = None
        elif isinstance(value, basestring):
            str_value = value
        else:
            value_type = field.get_value_type
            if value_type==self.VALUE_TYPE_INTEGER:
                str_value = self.config_encode_INTEGER(value)
            elif value_type==self.VALUE_TYPE_BOOLEAN:
                str_value = self.config_encode_BOOLEAN(value)
            elif value_type==self.VALUE_TYPE_DATE:
                str_value = self.config_encode_DATE(value)
            elif value_type==self.VALUE_TYPE_DATETIME:
                str_value = self.config_encode_DATETIME(value)
            elif value_type==self.VALUE_TYPE_TIME:
                str_value = self.config_encode_TIME(value)
            else:
                str_value = self.config_encode_STRING(value)
        field.set_default_value(str_value)

    def set_value(self, field_name, value):
        field = self.fields.get(field_name)
        if field:
            self.set_field_value(field, value)
        else:
            raise self.FieldNotFoundException(field_name)

    def add_selection_choice(self, field, title, value):
        selection = field.get_selection
        if not selection:
            selection = RestApi.Admin.ConfigTemplate.ValueSelection()
            field.set_selection(selection)
        choice = RestApi.Admin.ConfigTemplate.ValueSelectionChoice()
        choice.set_title(title)
        choice.set_value(value)
        selection.get_selection_choice.append(choice)

    def set_section(self, field, section_name):
        field.set_section(section_name)

    def set_values_from_object(self, obj, attr_name_prefix=''):
        for field in self.fields.values():
            field_name = field.get_name
            if field_name.startswith(attr_name_prefix):
                attr_name = field.get_name.replace(attr_name_prefix, '')
                if hasattr(obj, attr_name):
                    self.set_field_value(field, getattr(obj, attr_name))

    def set_values_from_dict(self, obj, attr_name_prefix=''):
        for field in self.fields.values():
            field_name = field.get_name
            if field_name.startswith(attr_name_prefix):
                attr_name = field.get_name.replace(attr_name_prefix, '')
                if obj.has_key(attr_name):
                    self.set_field_value(field, obj.get(attr_name))

    def get_field_value(self, field):
        str_value = field.get_default_value
        value_type = field.get_value_type
        if value_type==self.VALUE_TYPE_INTEGER:
            value = self.config_decode_INTEGER(str_value)
        elif value_type==self.VALUE_TYPE_BOOLEAN:
            value = self.config_decode_BOOLEAN(str_value)
        elif value_type==self.VALUE_TYPE_DATE:
            value = self.config_decode_DATE(str_value)
        elif value_type==self.VALUE_TYPE_DATETIME:
            value = self.config_decode_DATETIME(str_value)
        elif value_type==self.VALUE_TYPE_TIME:
            value = self.config_decode_TIME(str_value)
        else:
            value = self.config_decode_STRING(str_value)
        return value

    def get_value(self, field_name):
        field = self.fields.get(field_name)
        if field:
            return self.get_field_value(field)
        raise self.FieldNotFoundException(field_name)

    def get_field_value_dict(self):
        field_dict = dict()
        for (name, field) in self.fields.items():
            field_dict[name] = self.get_field_value(field)
        return field_dict

    def has_field(self, field_name):
        return self.fields.get(field_name, None)

    def get_field(self, field_name):
        return self.fields.get(field_name, None)

    def update_obj_with_values(self, obj, exclude_fields=[], throw_error=True):
        errors = []
        row = self.get_field_value_dict()
        for name in exclude_fields:
            row.pop(name)
        for (name,value) in row.items():
            if not hasattr(obj, name):
                print name
                print obj.__dict__
            try:
                setattr(obj, name, value)
            except AttributeError, e:
                if throw_error:
                    raise Exception("Attribute '%s' not found" % name)
                errors.append(name)
        return errors

    def add_details(self, title, internal_name=None, details_type="wizard_type"):
        details = RestApi.Admin.ConfigTemplate.Details()
        self.config_specification.get_details.append(details)
        details.set_title(title)
        details.set_internal_name(internal_name)
        details.set_details_type(details_type)
        return details

    def create_config_template_spec(self, title, entity_type, template_name, sub_type=None, element_id=None):
        spec = RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec()
        spec.set_title(title)
        spec.set_entity_type(entity_type)
        spec.set_template_name(template_name)
        spec.set_sub_type(sub_type)
        spec.set_id(element_id)
        return spec

    def add_config_spec_to_details(self, details, title, entity_type, template_name, sub_type=None, element_id=None):
        spec = self.create_config_template_spec(title, entity_type, template_name, sub_type, element_id)
        if element_id:
            details.get_config_edit.append(spec)
        else:
            details.get_config_new.append(spec)

    class FieldNotFoundException(Exception):

        def __init__(self, field_name):
            Exception.__init__(self, "Field '%s' not found" % field_name)


    @classmethod
    def check_data_types(cls, template):
        errors = []
        for field in template.get_field:
            str_value = field.get_default_value
            if str_value:
                value_type = field.get_value_type
                if value_type==cls.VALUE_TYPE_INTEGER:
                    try:
                        int(str_value)
                    except ValueError, e:
                        errors.append((field.get_title, "'%s' is not a valid integer" % str_value))
        return errors

    @classmethod
    def _copy_attribute(cls, from_element, to_element, attr_name):
        getter = getattr(from_element, "get_%s" % attr_name)
        setter = getattr(to_element, "set_%s" % attr_name)
        setter(getter)

    @classmethod
    def _copy_element(cls, from_element, to_element, attr_name):
        getter = getattr(from_element, "get_%s" % attr_name)
        setter = getattr(to_element, "set_%s" % attr_name)
        setter(getter)


    @classmethod
    def copy_field(cls, field):
        copy = RestApi.Admin.ConfigTemplate.FieldElement()

        cls._copy_attribute(field, copy, "field_type")
        cls._copy_attribute(field, copy, "value_type")
        cls._copy_attribute(field, copy, "advanced")
        cls._copy_attribute(field, copy, "read_only")
        cls._copy_attribute(field, copy, "mandatory")

        cls._copy_element(field, copy, "name")
        cls._copy_element(field, copy, "title")
        cls._copy_element(field, copy, "tooltip")
        cls._copy_element(field, copy, "default_value")

        return copy

    @classmethod
    def copy_template(cls, config_template):
        '''NOTE : action and selection elements are NOT copied!!!
        '''
        copy = RestApi.Admin.ConfigTemplate.ConfigurationTemplate()

        cls._copy_attribute(config_template, copy, "name")
        cls._copy_attribute(config_template, copy, "title")
        cls._copy_attribute(config_template, copy, "entity_type")
        cls._copy_attribute(config_template, copy, "use_as_template")

        cls._copy_element(config_template, copy, "description")

        fields = []
        for field in config_template.get_field:
            fields.append(cls.copy_field(field))
        copy.set_field(fields)

        return copy

    @classmethod
    def merge_templates(cls, config_template, custom_template):
        if not custom_template:
            return config_template

        custom_template.set_use_as_template(True)
        if not custom_template.get_title:
            custom_template.set_title(config_template.get_title)

        config_fields = dict()
        for field in config_template.get_field:
            config_fields[field.get_name] = field
        for field in custom_template.get_field:
            if field.get_field_type != cls.FIELD_TYPE_CUSTOM:
                config_field = config_fields.get(field.get_name)
                if config_field:
                    # Custom overriding attributes
                    if not field.get_value_type:
                        field.set_value_type(config_field.get_value_type)

                    if not field.get_title:
                        field.set_title(config_field.get_title)

                    if not field.get_default_value:
                        field.set_default_value(config_field.get_default_value)

                    if not field.get_tooltip:
                        field.set_tooltip(config_field.get_tooltip)

                    if not field.get_action:
                        #if config_field.get_element_action():
                        field.set_action(config_field.get_action)

                    if not field.get_section:
                        field.set_section(config_field.get_section)

                    if not field.get_selection:
                        field.set_selection(config_field.get_selection)


                    # Config overriding attributes
                    if config_field.get_max_length:
                        field.set_max_length(config_field.get_max_length)

                    if config_field.get_read_only:
                        field.set_read_only(config_field.get_read_only)

        return custom_template


    @classmethod
    def config_encode(cls, value):
        if value is None:
            return value
        # bool is instance of int so this should be before int!
        if isinstance(value, bool):
            return '1' if value else '0'
        if isinstance(value, int):
            return '%s' % value
        if isinstance(value, long):
            return '%s' % value
        return ''

    @classmethod
    def config_encode_STRING(cls, value):
        try:
            return '%s' % value
        except ValueError:
            return ''
        except TypeError:
            return ''

    @classmethod
    def config_encode_INTEGER(cls, value):
        try:
            return '%d' % value
        except ValueError:
            return '0'
        except TypeError:
            return '0'

    @classmethod
    def config_encode_BOOLEAN(cls, value):
        try:
            if value:
                return '1'
            return '0'
        except ValueError:
            return '0'
        except TypeError:
            return '0'

    @classmethod
    def config_encode_TEXT(cls, value):
        try:
            return '%s' % value
        except ValueError:
            return ''
        except TypeError:
            return ''

    @classmethod
    def config_encode_DATE(cls, value):
        try:
            return commongon.date2str(value)
        except ValueError:
            return ''
        except TypeError:
            return ''

    @classmethod
    def config_encode_DATETIME(cls, value):
        try:
            return commongon.datetime2str(value)
        except ValueError:
            return ''
        except TypeError:
            return ''

    @classmethod
    def config_encode_TIME(cls, value):
        try:
            return commongon.time2str(value)
        except ValueError:
            return ''
        except TypeError:
            return ''

    @classmethod
    def config_decode_STRING(cls, value):
        return value

    @classmethod
    def config_decode_INTEGER(cls, value):
        try:
            return int(value)
        except ValueError:
            return 0
        except TypeError:
            return None

    @classmethod
    def config_decode_BOOLEAN(cls, value):
        if value is None:
            return None
        if value == '1':
            return True
        return False

    @classmethod
    def config_decode_TEXT(cls, value):
        return value

    @classmethod
    def config_decode_DATE(cls, value):
        try:
            return commongon.str2date(value)
        except ValueError:
            return None
        except TypeError:
            return None
    @classmethod
    def config_decode_DATETIME(cls, value):
        try:
            return commongon.str2datetime(value)
        except ValueError:
            return None
        except TypeError:
            return None

    @classmethod
    def config_decode_TIME(cls, value):
        try:
            return commongon.str2time(value)
        except ValueError:
            return None
        except TypeError:
            return None


