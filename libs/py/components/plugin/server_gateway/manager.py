"""
Plugin module manager for Gateway Server plugin modules
"""
from __future__ import with_statement
import os.path

import lib.checkpoint

from components.plugin.common import manager_base
import plugin_types.server_gateway
import components.cpm.server_gateway.plugins


class Manager(manager_base.ManagerBase):
    def __init__(self, checkpoint_handler, import_path):
        manager_base.ManagerBase.__init__(self, checkpoint_handler, import_path)
        
        self.plugin_classes = []
        with self.checkpoint_handler.CheckpointScope('init', manager_base.module_id, lib.checkpoint.DEBUG, plugin_root=self.plugin_root):
            if os.path.exists(self.plugin_root):
                self.plugin_classes = self.load_plugin_classes('server_gateway', plugin_types.server_gateway.PluginTypeGatewayServer)
                self.plugin_classes.extend(self.load_plugin_clases_from_module(components.cpm.server_gateway.plugins, plugin_types.server_gateway.PluginTypeGatewayServer))
            else:
                self.checkpoint_handler.Checkpoint('init.error', manager_base.module_id, lib.checkpoint.ERROR, message='Plugin root not found')

    def create_instances(self, **arguments ):
        return self.create_plugin_instances(self.plugin_classes, **arguments)
    

        
if __name__ == '__main__':
    checkpoint_handler = lib.checkpoint.CheckpointHandler.get_cout_all()
    mm = Manager(checkpoint_handler, '/home/twa/source/hg_checkout/M2/py')
    