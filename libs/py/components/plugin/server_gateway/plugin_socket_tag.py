"""
Plugin socket for the Tag plugin type
"""
from __future__ import with_statement
from components.communication import tunnel_endpoint_base 

from components.plugin.common import socket_base
from plugin_types.server_gateway import plugin_type_tag

import lib.checkpoint
import sys

module_id = 'plugin_socket_tag'

class PluginSocketCB(object):
    def cb_tag_changed(self):
        pass


class PluginSocket(socket_base.PluginSocketBase, tunnel_endpoint_base.TunnelendpointSession):
    def __init__(self, async_service, checkpoint_handler, tunnel_endpoint, plugin_manager, plugins, cpm_session):
        tunnel_endpoint_base.TunnelendpointSession.__init__(self, async_service, checkpoint_handler, tunnel_endpoint)
        socket_base.PluginSocketBase.__init__(self, plugin_manager)
        self.checkpoint_handler = checkpoint_handler
        self.plugins_filtered = socket_base.PluginSocketBase.plugin_type_filter(plugins, plugin_type_tag.PluginTypeTag)
        self._cb = None
        self.cpm_session = cpm_session
        
    def set_cb(self, cb):
        self._cb = cb
    
    def session_start_and_connected(self):
        """
        This is called when tunnelendpoint_connected and session_start both has been called
        """ 
        pass
    
    def session_close(self):
        """
        Hard close of session. Called from main session when communication is terminated
        """
        self._cb = None
        self.cpm_session = None
        self.reset_tunnelendpoint()
        for plugin_name in self.plugins_filtered.keys():
            self.plugins_filtered[plugin_name].reset()
            
    def remote_tag_plugin_created(self, plugin_name, child_id):
        with self.checkpoint_handler.CheckpointScope("PluginSocketTag::remote_tag_plugin_created", module_id, lib.checkpoint.DEBUG, plugin_name=plugin_name, child_id=child_id):
            if self.plugins_filtered.has_key(plugin_name):
                self.plugins_filtered[plugin_name].set_tunnelendpoint(self.create_and_add_tunnelendpoint_tunnel(self.checkpoint_handler, child_id))
                self.plugins_filtered[plugin_name].set_cb(self)
                self.plugins_filtered[plugin_name].client_connected()

    def remote_all_tag_plugins_created(self):
        pass
    
    def generate_tags(self, arg_tags_strings):
        arg_tags = plugin_type_tag.Tag.create_tag_list_from_string_set(arg_tags_strings)
        generated_tags = []
        
        for plugin_name in self.plugins_filtered.keys():
            self.plugins_filtered[plugin_name].set_cb(self)
            try:
                generated_tags.extend(self.plugins_filtered[plugin_name].generate_tags(arg_tags))
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("generate_tags", plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
                
        
        result = plugin_type_tag.Tag.create_string_set_from_tag_list(generated_tags)
        return result

    def tag_changed(self, plugin_name):
        with self.checkpoint_handler.CheckpointScope("PluginSocketTag::tag_changed", module_id, lib.checkpoint.DEBUG, plugin_name=plugin_name):
            self._cb.cb_tag_changed()

    def get_cpm_session(self):
        return self.cpm_session
