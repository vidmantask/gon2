"""
Plugin socket for the Traffic plugin type
"""

from components.plugin.common import socket_base
from plugin_types.server_gateway import plugin_type_traffic
import lib.variable_expansion

class PluginSocket(socket_base.PluginSocketBase):
    def __init__(self, plugin_manager, plugins):
        socket_base.PluginSocketBase.__init__(self, plugin_manager)
        self.plugins_filtered = socket_base.PluginSocketBase.plugin_type_filter(plugins, plugin_type_traffic.PluginTypeTraffic)
    
    def get_attribute(self, plugin_name, attribute_name):
        try:
            return self.plugins_filtered[plugin_name].get_attribute(attribute_name)
        except KeyError:
            return None

    def expand_variables(self, template_string):
        """
        Expand variables of form "%(foo.bar)" in template_string using plugin foo.get_attribute(bar).
        """
        return lib.variable_expansion.expand(template_string, self.get_attribute)
