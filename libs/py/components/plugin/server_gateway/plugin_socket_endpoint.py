"""
Plugin socket for the endpoint plugin type
"""

from components.plugin.common import socket_base
from plugin_types.server_gateway import plugin_type_endpoint


class PluginSocket(socket_base.PluginSocketBase):
    def __init__(self, plugin_manager, plugins):
        socket_base.PluginSocketBase.__init__(self, plugin_manager)
        self.plugins_filtered = socket_base.PluginSocketBase.plugin_type_filter(plugins, plugin_type_endpoint.PluginTypeEndpoint)
    
    def set_endpoint_session(self, endpoint_session):
        for plugin_name in self.plugins_filtered.keys():
            self.plugins_filtered[plugin_name].set_endpoint_session(endpoint_session)

    def reset_all(self):
        for plugin_name in self.plugins_filtered.keys():
            self.plugins_filtered[plugin_name].reset()
 
    def endpoint_info_report_some(self, endpoint_info_plugin_name, endpoint_info_attributes):
        for plugin_name in self.plugins_filtered.keys():
            self.plugins_filtered[plugin_name].endpoint_info_report_some(endpoint_info_plugin_name, endpoint_info_attributes)
        
    def endpoint_info_all_done(self):
        for plugin_name in self.plugins_filtered.keys():
            self.plugins_filtered[plugin_name].endpoint_info_all_done()
            
    def check(self, plugin_name, endpoint_id):
        return self.plugins_filtered[plugin_name].check(endpoint_id)
