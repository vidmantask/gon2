"""
Plugin socket for the User plugin type
"""

from components.plugin.common import socket_base
from plugin_types.server_gateway import plugin_type_user

class PluginSocket(socket_base.PluginSocketBase):
    def __init__(self, plugin_manager, plugins):
        socket_base.PluginSocketBase.__init__(self, plugin_manager)
        self.plugins_filtered = socket_base.PluginSocketBase.plugin_type_filter(plugins, plugin_type_user.PluginTypeUser)
    
    def get_current_user_id(self, plugin_name):
        return self.plugins_filtered[plugin_name].get_current_user_id()
    
    def get_plugin_names(self):
        return self.plugins_filtered.keys()

    def get_plugins(self):
        return self.plugins_filtered.values()

    def get_plugin(self, name):
        return self.plugins_filtered.get(name, None)

    def reset_user_session_callback_all(self):
        for plugin in self.get_plugins():
            plugin.reset_user_session_callback()