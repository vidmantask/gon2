"""
Plugin socket for the Auth plugin type
"""
from components.plugin.common import socket_base
from plugin_types.server_gateway import plugin_type_auth

class PluginSocket(socket_base.PluginSocketBase):
    def __init__(self, plugin_manager, plugins):
        socket_base.PluginSocketBase.__init__(self, plugin_manager)
        self.plugins_filtered = socket_base.PluginSocketBase.plugin_type_filter(plugins, plugin_type_auth.PluginTypeAuth)

    def is_plugin_loaded(self, plugin_name):
        return plugin_name in self.plugins_filtered.keys()
        
    def config_get_columns(self, plugin_name):
        return self.plugins_filtered[plugin_name].config_get_columns()

    def check_predicate(self, plugin_name, params, internal_type=None):
        return self.plugins_filtered[plugin_name].check_predicate(params, internal_type)

    def start(self, plugin_name):
        if not self.is_started(plugin_name):
            return self.plugins_filtered[plugin_name].start()

    def is_started(self, plugin_name):
        return self.plugins_filtered[plugin_name].is_started()

    def is_ready(self, plugin_name):
        return self.plugins_filtered[plugin_name].is_ready()

    def set_callback(self, plugin_name, callback):
        self.plugins_filtered[plugin_name].set_callback(callback)

    def set_callback_all(self, callback):
        for plugin_name in self.plugins_filtered.keys():
            self.plugins_filtered[plugin_name].set_callback(callback)
            
    def get_plugins(self):
        return self.plugins_filtered.values()

    def get_plugin_names(self):
        return self.plugins_filtered.keys()

    def reset_all(self):
        for plugin_name in self.plugins_filtered.keys():
            self.plugins_filtered[plugin_name].reset()

    def set_tunnelendpoint(self, plugin_name, tunnel_endpoint):
        self.plugins_filtered[plugin_name].set_tunnelendpoint(tunnel_endpoint)
