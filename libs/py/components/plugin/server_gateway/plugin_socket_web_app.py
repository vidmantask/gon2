"""
Plugin socket for the User plugin type
"""

from components.plugin.common import socket_base
from plugin_types.server_gateway import plugin_type_web_app

class PluginSocket(socket_base.PluginSocketBase):
    def __init__(self, plugin_manager, plugins):
        socket_base.PluginSocketBase.__init__(self, plugin_manager)
        self.plugins_filtered = socket_base.PluginSocketBase.plugin_type_filter(plugins, plugin_type_web_app.PluginTypeWebApp)
    
    def get_plugin_names(self):
        return self.plugins_filtered.keys()

    def get_plugins(self):
        return self.plugins_filtered.values()

    def get_plugin(self, name):
        return self.plugins_filtered.get(name, None)

