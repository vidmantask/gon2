from __future__ import with_statement
from operator import itemgetter
from lib.commongon import str2datetime
import datetime
import lib.checkpoint
import marshal

ROOT_TAG_NAME = "_MENU_ROOT"

built_in_tags = { "root" : {
                                'element_id' : "tag.-1",
                                'tag_id' : -1,
                                'id' : -1,
                                'type' : "folder",
                                'title' : "G/On menu",
                                'menu_sortitems' : "sort_title",
                                "tag_name" : ROOT_TAG_NAME,
                            }
                }

built_in_tag_names = [tag.get("tag_name") for tag in built_in_tags.values()]

class dialog_menu():
    """
    basic methods to build the menu structure from tags and launch ids.

    def build_menu_using_tags(self, menuitems, tag_properties = None):
    Builds a menu including all items (uses build_menu_structure

    def build_menu_structure(self, tag_properties, menu_tags):
    Builds an empty menu with the right structure.
    
    See: 
    - component/dialog/unittest_dialog_menu_lib for an example of usage
    - component/dialog/client/dialog_session.build_menu_using_tags for real world usage
    
    
    """

    default_sort_values = {'launch_count': 0, 'last_date' : datetime.datetime(1900,1,1), 'sort_title' : ''}
    sort_properties = [('launch_count', 'Most Used'), 
#                       ('last_date', 'Last Used'),
                       ('sort_title' , 'Alphabetically')]
    default_sort_property = 'sort_title'
    
    def __init__(self, checkpoint_handler, dictionary):
        self.checkpoint_handler = checkpoint_handler
        self.dictionary = dictionary

    def build_menu_using_tags(self, menuitems, tag_properties = None, admin=False):
        """ 
        @param admin: Used when building the menu for the management client.
        """
        with self.checkpoint_handler.CheckpointScope("build_menu_using_tags", "dialog_menu_lib", lib.checkpoint.DEBUG) as cp:

            parent_child_relations = []
            
            # Get root tag
            root = built_in_tags.get("root").copy()
            root["title"] = self.dictionary._("G/On menu")
            
            root_tag_name = root.get("tag_name")
            # root_menu_item = { root_tag_name : root }

            # Find tags that make a sub menu and initialize them
            menu_tags = [root_tag_name]
            menu_folders = { root_tag_name : root }
            for (oneTag, tag_property) in tag_properties.items():
                if tag_property['menu_show']:
                    # Add sub-folder 'menu_caption' to 'oneTag' folder 
                    menu_tags.append(oneTag)
                    menu_folder = tag_property.copy()
                    menu_folders[oneTag] = menu_folder
                    menu_folder["element_id"] = "tag.%s" % tag_property["tag_id"]
                    menu_folder["title"] = tag_property["menu_caption"]
                    menu_folder["icon_id"] = None
                    menu_folder["type"] = "folder"
                    menu_folder["auto_menu_all"] = tag_property.get("auto_menu_all", False)
                    menu_folder["sort_title"] = menu_folder.get("menu_caption", "").lower()
                                            
                    if tag_property["menu_parentmenutags"]:
                        for tag_name in tag_property["menu_parentmenutags"]:
                            relation = (tag_name, menu_folder)
                            if not relation in parent_child_relations:
                                parent_child_relations.append(relation)
                    else:
                        relation = (root_tag_name, menu_folder)
                        parent_child_relations.append(relation)
            
            # Find menu items and their menu relations
            for menuitem in menuitems:
                self.calculate_menu_item_properties_from_tags(menuitem, tag_properties) 
    
                element_id = "action.%s" % menuitem["launch_id"]
                menuitem["element_id"] = element_id
                menuitem["sort_title"] = menuitem["title"].lower() 
                menuitem['restricted'] = menuitem.get('disabled', False)
                menuitem['restriction_text'] = menuitem.get('tooltip', "")
                
                itemtags = menuitem['tags']
                tagsum = itemtags.intersection(menu_tags)
                
                for tag_name in tagsum:
                    addtoprops = menu_folders[tag_name]
                    if menuitem['show'] or (addtoprops.has_key('menu_overwrite_show')  and addtoprops['menu_overwrite_show']): # show or menu show all
                        relation = (tag_name, menuitem)
                        if not relation in parent_child_relations:
                            parent_child_relations.append(relation)
            
            # Build menu tree
            for (parent_tag, menu_item) in parent_child_relations:
                parent = menu_folders.get(parent_tag)
                if parent:
                    if admin and parent.get('auto_menu_all', False) and menu_item['type']=='item':
                        continue
                    child_list = parent.get("child_list", [])
                    child_list.append(menu_item)
                    parent["child_list"] = child_list
                
            # Sort and prune elements if specified:
            for menu_folder in menu_folders.values():
                if (menu_folder.get('menu_sortitems', None)):
                    sortfield = menu_folder['menu_sortitems'] # Find out how this folder should be sorted.
                    for menuitem in menu_folder.get("child_list", []):
                        if not menuitem.has_key(sortfield): # we can only add item if it has the key we want to sort on
                            menuitem[sortfield] = self.default_sort_values[sortfield] 
                    # Sort by title
                    if menu_folder['menu_sortitems'] == 'sort_title':
                        sort_list = [ (m[sortfield], m) for m in menu_folder.get("child_list", [])]
                        sort_list.sort()
                    # Sort by launch count
                    elif menu_folder['menu_sortitems'] == 'launch_count':
                        sort_list = [ (m['launch_count'], m) for m in menu_folder.get("child_list", [])]
                        sort_list = self.sort_by_most_used_recently(sort_list)
                    
                    child_list = []
                    folder_list = []
                    for m in sort_list:
                        menu_item = m[1]
                        if menu_item.get("type") == "item":
                            child_list.append(menu_item)
                        else:
                            folder_list.append(menu_item)
                    child_list.extend(folder_list)
                    menu_folder["child_list"] = child_list

                    if not admin:            
                        self._prune_menu(menu_folder)
                
            if admin:
                all_menu_items = []
                all_menu_items.extend(menu_folders.values())
                all_menu_items.extend(menuitems)
                return root['element_id'], all_menu_items
            else:
                self.remove_empty_folders(root)
                return self.create_menu_list(root, [])

    def sort_by_most_used_recently(self, sort_list):
        """ 
        Sort the menu elements from a list in the order of most used recently.
        
        This is done by calculating a delta value for each menu item.
        If a menu item has not been used enough to calculate the delta value,
        it given the largest + an amount calculated by the launch count.
        
        The delta value is calculated as a sum of the last x launch_dates divided 
        by the sum of launch dates. This should give some indication of the usage
        frequency.
        
        """
        sort_list.sort(reverse=True)    # Make sure that the menu items with most launches are first. 
        delta_max = 0                   # Use this to collect the max value for deltas.
        delta_count_max = 3             # The maximum number of deltas used for calculating the position.
        max_launch_count = 1.0
        
        try:
            # Calculate max launch count
            for menu_item in sort_list:
                if menu_item[1]['launch_count'] > max_launch_count:
                    max_launch_count = menu_item[1]['launch_count'] * 1.0
            # Sort menu items by most recently used
            for menu_item in sort_list:
                # Calculate a delta value for the items that has been launched at least delta_count_max times.
                if menu_item[1].has_key('launch_lastdate') and marshal.loads(menu_item[1]['launch_lastdate']) != [] and menu_item[1]['launch_count'] >= delta_count_max:
                    
                    delta_sum = 0
                    delta_count = 0.0
                    launches = marshal.loads(menu_item[1]['launch_lastdate'])
                    # Calculate delta value
                    for launch in launches:
                        delta_sum = delta_sum  + (datetime.datetime.now() - str2datetime(launch)).days
                        delta_count = delta_count + 1.0
                    menu_item[1]['delta'] = delta_sum/delta_count
                    
                    # Collect max delta value
                    if menu_item[1]['delta'] > delta_max:
                        delta_max = menu_item[1]['delta']
                # Set the delta value higher than delta max and lower depending on launch count.
                else:
                    menu_item[1]['delta'] = delta_max + (delta_count_max - (menu_item[1]['launch_count'] / max_launch_count))

            # Sort the list depending on the delta value - smallest first.        
            sort_list.sort(key=lambda s: s[1]['delta'])
            
            # Locate items that has never been launched.
            tmp_removal_list = []
            for menu_item in sort_list:
                if menu_item[1]['launch_count'] == 0:
                    tmp_removal_list.append(menu_item)
            # Remove items that has never been launched.        
            for tmp_item in tmp_removal_list:
                sort_list.remove(tmp_item) 
            
            return sort_list
        except:
            self.checkpoint_handler.CheckpointExceptionCurrent("sort_by_most_used_recently", "Dialog", lib.checkpoint.WARNING)
            # Return the empty list if something failed.
            # This happens f.ex. if the user has not logged in
            # which in turn makes the top x sub-menu pointless.
            return []
                
    def calculate_menu_item_properties_from_tags(self, menuitem, tag_properties):
        """
          takes any item properties modefieres from tags and 
          applies to the item properties
          
        """
        orderedtags = {}
        for tag in menuitem['tags']:
            if tag_properties.has_key(tag):
                orderedtags.update({tag : tag_properties[tag]['tag_priority']})
        orderedtags = sorted(orderedtags.iteritems(), key=itemgetter(1))
                
        for tag, _priority in orderedtags:
            if tag_properties.has_key(tag):
                props = tag_properties[tag]
                if props.has_key('item_show'):
                    menuitem['show'] = props['item_show'] 
                if props.has_key('item_enabled'):
                    menuitem['enabled'] = props['item_enabled'] 
    
    def update_tags_props(self, oneTag, tag_properties, menu_tags, last_item_id):
        if tag_properties[oneTag].has_key('menu_item_id'):
            item_count = tag_properties[oneTag]['item_count']
            tag_properties[oneTag]['item_count'] = item_count + 1
            return last_item_id 
        tag_properties[oneTag]['menu_item_id'] = last_item_id
        tag_properties[oneTag]['item_count'] = 0
        last_item_id = last_item_id + 1
        menu_tags.remove(oneTag)
        return last_item_id
                
    def _prune_menu(self, menu_folder, sorteditems=None):
        if not sorteditems:
            sorteditems = [item for item in menu_folder.get("child_list", []) if item.get("type")=="item"]
            
        menu_maxitems = int(menu_folder.get('menu_maxitems', 0))
        if (menu_maxitems):
            if int(menu_maxitems) < len(sorteditems):
                # lets delete something - probably directly in menu
                for item in sorteditems[menu_maxitems:]:
                    menu_folder["child_list"].remove(item)
                
    def remove_empty_folders(self, item):
        """ 
        Recursively remove empty folders in an existing menu structure
        """
        if item['type'] == 'folder':
            child_list = [ child for child in item.get("child_list", [])]
            for child in child_list:
                if  child['type'] == 'item':
                    continue
                self.remove_empty_folders(child)
                if not child.get("child_list", []):
                    item.get("child_list").remove(child)
                        
    def create_menu_list(self, item, menu_list):
        """ 
        Create list of all elements in the menu with parent identifier.
        """  
        for child in item.get("child_list", []):
            # Add menu item 'new_child' to 'item'
            new_child = child.copy()
            new_child['id'] = len(menu_list)+1
            new_child['parent_id'] = item['id']
            new_child['parent_tag_id'] = item['tag_id']
            menu_list.append(new_child)
            menu_list = self.create_menu_list(new_child, menu_list)
        return menu_list

    @classmethod
    def get_sortitem_title(cls, sort_option):
        """
        Used in management client
        """
        for (name, title) in cls.sort_properties:
            if name==sort_option:
                return title
        return "Unknown sort order"
