"""
This file contains the functionality related to a dialog session on the server
"""
from __future__ import with_statement

import base64
import sys 
import marshal

from lib import checkpoint 
from components.communication import tunnel_endpoint_base

from components.database.server_common.database_api import QuerySession
from components.database.server_common import schema_api
from components.database.server_common import database_api
from components.communication import message


from lib.commongon import  *

import components.dialog.server_common.database_schema as database_schema  
import components.dialog  
import components.dialog.server_gateway.dialog_informer
import components.management_message.server_gateway.session_send
from components.dialog.server_common.dialog_api import read_tag_properties_from_db

class IDialogCallback(object):
    """s
    API to dialog session
    """
    def dialog_update_ids(self, launch_ids):
        """
        Notification from the traffic session that access has changed 
        and the supplied argument should contain the list of launch_ids that now can be launched.  
        """
        raise NotImplementedError
    

class DialogSession(IDialogCallback,
                    tunnel_endpoint_base.TunnelendpointSession,
                    components.dialog.server_gateway.dialog_informer.IDialogInformerCalback):
    LAUNCH_TYPE_DIALOG  = 0
    LAUNCH_TYPE_TRAFFIC = 1
    
    def __init__(self, environment, async_service, checkpoint_handler, tunnel_endpoint_tunnel, user_session, server_config, cpm_session, session_info, dictionary, service_management_message_send, overwrite_user_id = -1):
        tunnel_endpoint_base.TunnelendpointSession.__init__(self, async_service, checkpoint_handler, tunnel_endpoint_tunnel)

        self.dictionary = dictionary
        self.checkpoint_handler = checkpoint_handler
        self.user_session = user_session
        self.server_config = server_config
        self.cpm_session = cpm_session
        self.unique_session_id = session_info.unique_session_id
        self.service_management_message_send = service_management_message_send
        self._current_menu = None
        self._current_menu_launch = None
        self.traffic_callback = None 
        self.current_launch_ids = []
        self.user_stat_keys = None
        self._management_message_sender = components.management_message.server_gateway.session_send.ManagementMessageSessionSender(components.dialog.component_id, self.service_management_message_send)

        dialog_informer_policy = components.dialog.server_gateway.dialog_informer.DialogInformerPolicy()
        dialog_informer_policy.welcome_message_enabled = server_config.dialog_welcome_message_enabled
        dialog_informer_policy.welcome_message_absfilename = server_config.dialog_welcome_message_filename
        dialog_informer_policy.welcome_message_close_on_cancel = server_config.dialog_welcome_message_close_on_cancel
        dialog_informer_policy.license_handler = server_config.get_license_handler()
        
        self._create_citrix_menu = server_config.dialog_create_citrix_menu
        
        self.dialog_informer_tunnel_endpoint = self.create_and_add_tunnelendpoint_tunnel(checkpoint_handler, 1)
        self.dialog_informer = components.dialog.server_gateway.dialog_informer.DialogInformer(self, self.async_service, checkpoint_handler, self.dialog_informer_tunnel_endpoint, dialog_informer_policy, self.dictionary)

        self.tag_properties = {}
        self.launch_elements = {}
        
        self.user_id = None
        self.default_tags = None 


    def session_start_and_connected(self):
        """
        This is called when tunnelendpoint_connected and session_start both has been called
        """ 
        self.read_tag_properties_from_db(-1)    


    def session_close(self):
        """
        Hard close of session. Called from main session when communication is terminated
        """
        with self.checkpoint_handler.CheckpointScope("session_close", "dialog_session", checkpoint.DEBUG):
            self.user_session = None
            self.cpm_session = None
            self.traffic_callback = None 
            self.reset_tunnelendpoint()
            self.dialog_informer.reset()

            self.launch_elements = None
            self.dictionary = None
            self.server_config = None
            self.service_management_message_send = None
            self._current_menu = None
            self._current_menu_launch = None
            self.current_launch_ids = []
            self.user_stat_keys = None
            self._management_message_sender = None
    
            self._create_citrix_menu = None
            self.tag_properties = {}
            self.launch_elements = {}
            
            self.user_id = None
            self.default_tags = None 

        
       
    def load_user_tags(self, launch_elements):
        """ 
          Loads launch_id from the database. 
          Dialog should only do this to load a users tags
          to suplement the tags passed on the dialog from L&T
        """
        user_id = self.get_current_user_id()
        if user_id:
        
            with QuerySession() as db_session:
                for launch_element in launch_elements:
                    tags = db_session.select(database_schema.DialogLaunchTags,
                                            database_schema.database_api.and_(database_schema.DialogLaunchTags.user_id==user_id,
                                                                              database_schema.DialogLaunchTags.launch_id==launch_element.launch_id))
                    tagset = set([tag.tag_name for tag in tags])
                    launch_element.tags.update(tagset)
                         

    
        
    def read_tag_properties_from_db(self, user_id):
        with self.checkpoint_handler.CheckpointScope("read_tag_properties_from_db", "dialog_session", checkpoint.DEBUG, user_id=user_id):
            self.tag_properties.update(read_tag_properties_from_db(QuerySession(), user_id))
    
    def create_citrix_menu(self):
        return self._create_citrix_menu        
    
    def split_menu_tags_from_other(self, tags):
        menu_tags = []
        other_tags = []
        for tag in tags:
            tag_properties = self.tag_properties.get(tag)
            if tag_properties:
                if tag_properties.get("menu_show"):
                    menu_tags.append(tag)
                else:
                    other_tags.append(tag)
        return menu_tags, other_tags
        
    def add_dynamic_menu_tags(self, tag_list):
        for tag_dict in tag_list:
            menu_tag_properties = dict()
            parent_list = tag_dict.get("parents")
            menu_tag_properties["menu_parentmenutags"] = set(parent_list)
            menu_tag_properties["menu_caption"] = tag_dict.get("caption")
            menu_tag_properties["tag_priority"] = 0
            menu_tag_properties["menu_show"] = True
            tag_name = tag_dict.get("name")
            menu_tag_properties["tag_id"] = tag_name
            existing = self.tag_properties.get(tag_name)
            if existing:
                menu_tag_properties["menu_parentmenutags"].update(existing.get("menu_parentmenutags"))
            self.tag_properties[tag_name] = menu_tag_properties

            
    def get_user_stat_keys(self):
        if not self.user_stat_keys:
            user_id = self.get_current_user_id()
            if user_id:
                self.user_stat_keys = {}
                self.user_stat_keys['user'] = user_id  
                self.user_stat_keys['os'] = 'all'
        return self.user_stat_keys

    def set_traffic_callback(self, traffic_callback):
        self.traffic_callback = traffic_callback
        
    # This is where the dialog session gets the list of launch_ids    
    def dialog_update_ids(self, launch_elements):
        with self.checkpoint_handler.CheckpointScope("dialog_update_ids", "dialog_session", checkpoint.DEBUG):
            self.load_user_tags(launch_elements)
            self.launch_elements.clear()
            self.launch_elements.update([(l.launch_id, l) for l in launch_elements])
            if len(launch_elements) > 0:
                self.dialog_informer.access_changed_before_start() # Assuming that dialog_informer_cb_access_changed_before_done is called
            
    def dialog_informer_cb_access_changed_before_done(self):
        self.update_current_menu(self.launch_elements.values(), self.get_user_stat_keys())
        self.tunnelendpoint_remote('server_cb_show_menu', menu=self._current_menu, tag_properties = self.tag_properties)


    # This is where the server collects menu launch item data for the client
    def update_current_menu(self, launch_elements, stat_keys = {}):
        menu_launch = {}
        
        #
        # Create static content
        #
        static_menu_id_actions  = 1
        static_menu_id_next = 2
        static_menu = []
        
        if stat_keys and stat_keys.has_key('user') and stat_keys.has_key('os'):
            launch_key = stat_keys['user']+'_'+stat_keys['os']
        else:
            launch_key = 'na_all'
        launch_key = unicode(launch_key)
        
        
        #
        # Append actions 
        #
        with QuerySession() as db_session:
            if self.default_tags is None:
                self.default_tags = [prop.tag_name for prop in db_session.select(database_schema.DialogTagProperties, database_schema.DialogTagProperties.auto_menu_all==True)]                 
            for launch_element in launch_elements:
                launch_id = launch_element.launch_id
                tags = set(self.default_tags)
                tags.add('_LAUNCH_ID_%s' % launch_id)
                tags = tags | launch_element.tags

                action_menu = { 'id': static_menu_id_next,
                                'type': 'item',
                                'launch_id': launch_id,
                                'parent_id': static_menu_id_actions,
                                'title': launch_element.title,
                                'icon_id': launch_element.icon_id,
                                'enabled': True,
                                'disabled': launch_element.deactivated,
                                'tooltip' : launch_element.tooltip,
                             }
                
                    
                # if there's dialog data, lets look for statistics
                dialog_statistics = db_session.select(database_schema.DialogStatistics, database_api.and_(database_schema.DialogStatistics.launch_key == launch_key, database_schema.DialogStatistics.launch_id == launch_id))
                for dialog_statistic in dialog_statistics:
                    
                    #
                    # Find the last X launch dates for this menu item.
                    #
                    dialog_launch_statistics = db_session.select(database_schema.DialogLaunchStatistics, database_api.and_(database_schema.DialogLaunchStatistics.launch_key == launch_key, database_schema.DialogLaunchStatistics.launch_id == launch_id))
                    
                    latest_dates = []
                    for dialog_launch_statistic in dialog_launch_statistics:
                        latest_dates.append(datetime2str(dialog_launch_statistic.launch_date))
                    
                    latest_dates.sort(reverse=True)
                    latest_dates = latest_dates[0:3]
                    
                    #
                    # Update the menu item with launch count and latest launch 
                    #
                    action_menu.update({'stat_id' : dialog_statistic.id,
                                'launch_count' : dialog_statistic.launch_count,
                                'launch_lastdate' : marshal.dumps(latest_dates)})

                # Lets add some tags    
                if ('CLIENTOK' in tags) and ('SERVEROK' in tags):
                    tags.add('SHOW')
                    tags.add('ENABLED') 
                            
                action_menu.update({'tags' : tags})
                
                # Lets set some properties
                
                action_menu.update({'show' : False,
                                    'enabled' : False,
                                    'strong' : False})
                for tag in tags:
                    if self.tag_properties.has_key(tag):
                        one_tag_properties = self.tag_properties[tag]
                        if one_tag_properties.has_key('item_show'):
                            action_menu.update({'show' : one_tag_properties['item_show']})
                        if one_tag_properties.has_key('item_enabled'):
                            action_menu.update({'enabled' : one_tag_properties['item_enabled']})
                        if one_tag_properties.has_key('strong'):
                            action_menu.update({'strong' : one_tag_properties['strong']})
                
                
                # lets append the menu data
                static_menu.append(action_menu)
                menu_launch[action_menu['launch_id']] = { 'launch_id': action_menu['launch_id'], 'launch_type':  DialogSession.LAUNCH_TYPE_TRAFFIC}
                static_menu_id_next += 1
                self.checkpoint_handler.Checkpoint("update_current_menu", "dialog_session", checkpoint.DEBUG, action_menu_id = action_menu['id'], action_menu_title = "%r" % action_menu['title'], tags = str(tags))

        self._current_menu_launch = menu_launch
        self._current_menu = static_menu
        
    def get_current_user_id(self):
        if self.user_session:
            self.user_id = self.user_session.get_user_id()
        return self.user_id

    def get_current_menu(self):
        menu = []
        for menu_item in self._current_menu:
            menu_item_copy = menu_item.copy()
            menu_item_copy['tags'] = list(menu_item_copy['tags'])
            menu.append(menu_item_copy)
        return menu
        
    def client_cb_menu_item_selected(self, launch_id, auto_launch=False):
        """
        Called when user selected a menu_item
        """
        with self.checkpoint_handler.CheckpointScope("client_cb_menu_item_selected", "dialog_session", checkpoint.DEBUG, launch_id=launch_id):
            try:
                launch_info = self._current_menu_launch[launch_id]
                if launch_info['launch_type'] == DialogSession.LAUNCH_TYPE_DIALOG:
                    self.launch_dialog(launch_id)
                elif launch_info['launch_type'] == DialogSession.LAUNCH_TYPE_TRAFFIC:
                    self.launch_traffic(launch_id)
                    
                user = self.get_current_user_id()
                if not user:
                    user = 'na' 
                launch_key = user + '_all'
                # add statistics code here...
                self.checkpoint_handler.Checkpoint("Sending menu item activation to management server", "dialog_sender", checkpoint.DEBUG, launch_key=launch_key, launch_id=launch_id)
                #print 'Server launch_info:'+str(launch_info)
                
                # We need to find the user here... if there is one.
                self._management_message_sender.message_remote('dialog_item_launched', unique_session_id=self.unique_session_id, launch_key=launch_key, launch_id=launch_id, auto_launch=auto_launch)


                # send a message to the Management Server to update usage statistics
                
            except KeyError:
                self.checkpoint_handler.Checkpoint("client_cb_menu_item_selected", "dialog_session",
                                                   checkpoint.DEBUG,
                                                   message="Not allowed to launch action")

    def client_user_add_tag_to_launch_id(self, launch_id, tag_name, overwriteuserid = None):
        """
        Called when user adds a tag to a launch_id
        """
        with self.checkpoint_handler.CheckpointScope("client_user_add_tag_to_launch_id", "dialog_session", checkpoint.DEBUG, launch_id=launch_id, tag_name=tag_name):
            try:
                if self.user_session == None: # mostly for the demo program, should normally have a value
                    user = overwriteuserid
                else:
                    user = self.user_session.get_user_id()

                if user is None:
                    user = -1

                self._management_message_sender.message_remote('dialog_user_add_tag_item', launch_id=launch_id, user_id=user, tag_name=tag_name)

                
            except KeyError:
                self.checkpoint_handler.Checkpoint("client_user_add_tag_to_launch_id", "dialog_session",
                                                   checkpoint.DEBUG,
                                                   message="Not allowed to launch action")


    def launch_dialog(self, launch_id):
        # To be  used for a launch inside the dialog component
        pass
    
    def launch_traffic(self, launch_id):
        try:
            if self.launch_elements.has_key(launch_id):
                tags = self.launch_elements.get(launch_id).tags
                package_id_missing = self.cpm_session.check_for_missing_package(tags)
                if package_id_missing is not None:
                    self.checkpoint_handler.Checkpoint("launch_traffic.package_missing", "dialog_session", checkpoint.DEBUG, package_id_missing=package_id_missing)
                    self.tunnelendpoint_remote('server_cb_launch_cancled_missing_package', package_id_missing=package_id_missing)
                    return
                
            if self.traffic_callback != None:
                self.traffic_callback.traffic_launch(launch_id)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("error", "dialog_session", checkpoint.ERROR, etype, evalue, etrace)
            



    def _image_id_to_filename(self, image_id, image_style, image_size_x, image_size_y, image_format):
        if image_id is None or image_id == "":
            return None

        image_size_str = '%ix%i' % (image_size_x, image_size_y)
        image_filename = "%s_%s_%s.%s" %(image_id.lower(), image_style.lower(), image_size_str, image_format.lower())
        image_filename_abs = os.path.join(self.server_config.image_abspath, image_filename)
        if os.path.exists(image_filename_abs):
            return image_filename_abs
        
        image_filename = "%s_%s_%s.%s" %(image_id.lower(), image_style.lower(), 'default', image_format.lower())
        image_filename_abs = os.path.join(self.server_config.image_abspath, image_filename)
        if os.path.exists(image_filename_abs):
            return image_filename_abs
        
        image_filename = "%s_%s_%s.%s" %(image_id.lower(), 'default', image_size_str, image_format.lower())
        image_filename_abs = os.path.join(self.server_config.image_abspath, image_filename)
        if os.path.exists(image_filename_abs):
            return image_filename_abs

        image_filename = "%s_%s_%s.%s" %(image_id.lower(), 'default', 'default', image_format.lower())
        image_filename_abs = os.path.join(self.server_config.image_abspath, image_filename)
        if os.path.exists(image_filename_abs):
            return image_filename_abs
        return None

    def remote_get_images(self, request_id, image_ids, image_style, image_size_x, image_size_y, image_format):
        self.checkpoint_handler.Checkpoint("remote_get_images", "dialog_session", checkpoint.DEBUG, request_id=request_id)
        for image_id in image_ids:
            image_filename = self._image_id_to_filename(image_id, image_style, image_size_x, image_size_y, image_format)
            if image_filename is not None:
                image_file = open(image_filename, 'rb')
                image_data = image_file.read()
                image_file.close()
                image_database64_encoded = base64.b64encode(image_data)
                self.tunnelendpoint_remote('remote_image_found', request_id=request_id, image_id=image_id, image_data=image_database64_encoded)
            else:
                self.tunnelendpoint_remote('remote_image_not_found', request_id=request_id, image_id=image_id)
                self.checkpoint_handler.Checkpoint("remote_get_images.image_not_found", "dialog_session", checkpoint.WARNING, request_id=request_id, image_id=image_id, image_style=image_style, image_size_x=image_size_x, image_size_y=image_size_y, image_format=image_format)
