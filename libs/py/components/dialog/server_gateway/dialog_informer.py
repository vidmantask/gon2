"""
This module contains functionality for handling information policies (Gateway Server side)
"""
from __future__ import with_statement
import os.path
import time
import codecs

import lib.checkpoint
from components.communication import tunnel_endpoint_base



class DialogInformerPolicy(object):
    def __init__(self):
        self.welcome_message_enabled = False
        self.welcome_message_absfilename = ''
        self.welcome_message_close_on_cancel = False
        self.license_handler = None
        

class IDialogInformerCalback(object):
    def dialog_informer_cb_access_changed_before_done(self):
        """
        This callback is called as a response to access_changed_before_start
        The response can both be syncon and asyncron.
        """
        raise NotImplementedError

    
class DialogInformer(tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch):
    def __init__(self, callback, async_service, checkpoint_handler, dialog_informer_tunnel_endpoint, informer_policy, dictionary):
        tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch.__init__(self, async_service, checkpoint_handler, dialog_informer_tunnel_endpoint)
        self.callback = callback
        self.checkpoint_handler = checkpoint_handler
        self.informer_policy = informer_policy
        if self.informer_policy is None:
            self.informer_policy = DialogInformerPolicy()
        self._access_changed_first_run = True
        self.access_changed_before_start_running = False
        self.license_display_timestamp = None
        self.license_display_time = 3
        self.dictionary = dictionary

        self.menu_updated_message = self.dictionary._("Your G/On Menu is ready") 

    def reset(self):
        self.callback = None
        self.reset_tunnelendpoint()
        
    def access_changed_before_start(self):
        """
        Notification from dialog component that access has changed, but the menu has not yet been shown to the user. 
        
        A call to the callback dialog_informer_cb_access_changed_done will be done as a response to this call. 
        The response can both be syncon and asyncron.
        """
        if self.access_changed_before_start_running:
            return
        self.access_changed_before_start_running = True
        
        if self._access_changed_first_run:
            self._access_changed_first_run = False
            self._access_changed_before_start_first_run()
            return
        self._access_changed_before_start()
        
    def _access_changed_before_start_first_run(self):
        self._menu_updated_handler()
        if self.informer_policy.welcome_message_enabled:
            if os.path.isfile(self.informer_policy.welcome_message_absfilename):
                message = codecs.open(self.informer_policy.welcome_message_absfilename, 'rb', 'utf-8').read() # Read as binary to keep line-feed encoding used in file
            else:
                self.checkpoint_handler.Checkpoint("DialogInformer::unable_to_find_message_file", "dialog_session", lib.checkpoint.ERROR, message_filename=self.informer_policy.welcome_message_absfilename)
                message = self.dictionary._("Error occurred while reading the message, please contact the administrator.") 
            self.tunnelendpoint_remote('remote_inform_on_first_access', message=message, close_on_cancel=self.informer_policy.welcome_message_close_on_cancel)
            return
        self._access_changed_before_done()

    def remote_welcome_message_response(self):
        """
        Called from client side indicating that the first_access message has been shown to the user
        """
        self._access_changed_before_done()

    def _access_changed_before_start(self):
        self._menu_updated_handler()
        self._access_changed_before_done()

    def _access_changed_before_done(self):
        self.access_changed_before_start_running = False
        self.callback.dialog_informer_cb_access_changed_before_done()

    def _menu_updated_handler(self):
        if self.license_display_timestamp is None: 
            self._menu_updated()
        else:
            if (time.time() - self.license_display_timestamp) > self.license_display_time:
                self._menu_updated()
        
    def _menu_updated(self):
        self.license_display_timestamp = time.time()
        license = self.informer_policy.license_handler.get_license()
        if not license.valid:
            self.tunnelendpoint_remote('remote_show_license_info', valid=False, header=self.dictionary._("G/On License invalid"), message=self.dictionary._("This software is not legally licensed"))
            return
        if license.license_expired:
            self.tunnelendpoint_remote('remote_show_license_info', valid=False, header=self.dictionary._("G/On License expired"), message=self.dictionary._("This software is not legally licensed"))
            return
        message_lines = license.get_all('Giritech Welcome Notification', default=[])
        message = "\n".join(message_lines)
        self.tunnelendpoint_remote('remote_show_license_info', valid=True, header=self.menu_updated_message, message=message)
        self.menu_updated_message = self.dictionary._("Your G/On menu has been updated")
