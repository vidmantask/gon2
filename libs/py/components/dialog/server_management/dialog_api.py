from __future__ import with_statement
from components.dialog.server_common.database_schema import DialogLaunchTags
from components.dialog.server_common.database_schema import DialogStatistics
from components.dialog.server_common.database_schema import DialogTagProperties
from components.dialog.server_common.dialog_api import read_tag_properties_from_db

from components.database.server_common import schema_api
from components.database.server_common import database_api
from components.dialog.server_common import database_schema

from components.auth.server_management import OperationNotAllowedException, ElementType

from components.admin_ws.ws.admin_ws_services_types import ns0 as ns0
from components.plugin.server_common.config_api_lib import ConfigTemplateSpec

import components.dialog.server_common.dialog_api as dialog_api1
import components.dialog.common.dialog_menu_lib as dialog_menu_lib 

import components.management_message.server_management.session_recieve

from components.user.server_common import sink_component_id as sink_component_id

import components.traffic.server_common.database_schema as traffic_schema


import lib.checkpoint
import lib.variable_expansion
import lib.dictionary


def delete_dialog_element_statistics(transaction, launch_id):
    elements = transaction.select(DialogStatistics, DialogStatistics.launch_id==launch_id)
    for e in elements:
        transaction.delete(e)

def delete_dialog_element_tags(transaction, launch_id):
    elements = transaction.select(DialogLaunchTags, DialogLaunchTags.launch_id==launch_id)
    for e in elements:
        transaction.delete(e)

def create_dialog_element(transaction, launch_id, label):
    pass
    
def delete_dialog_element(transaction, launch_id):
    delete_dialog_element_statistics(transaction, launch_id)
    delete_dialog_element_tags(transaction, launch_id) 

def update_dialog_element(transaction, launch_id, label):
    pass
        
    
def dialog_update_user_launch_item_tags(transaction, user_id, launch_id, tag_set):
    """
      Changes the user tags for a launch id to the supplied set
      
      In other words: If a user had a tag, but it isn't in this list
      they will NO LONGER HAVE IT, after this rutine has run...
      
      To read use py/components/dialog/server/dialog_api.py:get_tag_set
      
      
    """

    # environment.checkpoint_handler.Checkpoint("dialog_api_update_user_launch_item_tags", "dialog_sink", lib.checkpoint.DEBUG,  user_id=user_id, launch_id=launch_id, tag_set=str(tag_set))
   
    alltags = transaction.select(database_schema.DialogLaunchTags, 
                                 database_api.and_(
                                              database_schema.DialogLaunchTags.launch_id==launch_id,
                                              database_schema.DialogLaunchTags.user_id==user_id))

    for tag in alltags:
        transaction.delete(tag)
    for tag in tag_set:
        if tag:
            normalised_tag_name = check_tag_name(tag)
            create_default_tag_properties_if(transaction, normalised_tag_name)
            record = database_schema.DialogLaunchTags()
            record.launch_id = launch_id
            record.user_id= user_id
            record.tag_name = normalised_tag_name
            transaction.add(record)
        
def create_default_tag_properties_if(transaction, tag_name):
    if tag_name in dialog_menu_lib.built_in_tag_names:
        return
    props = transaction.select_first(DialogTagProperties, filter = DialogTagProperties.tag_name==tag_name)
    if not props:
        props = transaction.add(DialogTagProperties())
        props.tag_name = tag_name
        # create default caption
        upcase_title_parts = tag_name.split("_")
        title_parts = []
        for part in upcase_title_parts:
            if part:
                new_part = "%s%s" % (part[0], part[1:].lower())
                title_parts.append(new_part)
        props.menu_caption = " ".join(title_parts)
        props.user_id = -1
        
        

def check_tag_name(tag_name):
    if not tag_name:
        raise OperationNotAllowedException("Tag name cannot be empty")
    
    if not tag_name.replace("_", "").isalnum():
        raise OperationNotAllowedException("Tag '%s' : Only letters and '_' are allowed in tag names" % tag_name)
    
    normalised_tag_name = tag_name.upper()
    return normalised_tag_name

        
def update_tag_string(transaction, launch_id, dialog_tags):
    tag_set = set([tag.strip() for tag in dialog_tags.split(',')])
    dialog_update_user_launch_item_tags(transaction, -1, launch_id, tag_set)

       
def save_tag_values(transaction, tag, user_id, **kwargs): 
#                 menu_show=None,
#                 menu_caption=None, 
#                 menu_sortitems = None,
#                 menu_maxitems = None,
#                 menu_showinparent = None,
#                 item_enabled = None, 
#                 item_show = None,
#                 item_autolaunch_once = None
#                 ):
        
    """
      Saves a tag with data - overwrites if already exists
      
    """
     
    
    tagrec = transaction.select(database_schema.DialogTagProperties, 
                                database_schema.database_api.and_(
                                database_schema.DialogTagProperties.tag_name==tag,
                                database_schema.DialogTagProperties.user_id==user_id))
    if len(tagrec) == 0:
        rec = transaction.add(database_schema.DialogTagProperties())
    else:
        rec = tagrec[0]
        
    #basic
    rec.tag_name = tag
    rec.user_id = user_id
    
    for (name,value) in kwargs.items():
        setattr(rec, name, value)
    
        

    
    
class DialogAdmin(components.management_message.server_management.session_recieve.ManagementMessageSessionRecieverBase):
    
    def __init__(self, environment, template_handler, activity_log_manager):
        components.management_message.server_management.session_recieve.ManagementMessageSessionRecieverBase.__init__(self, environment.checkpoint_handler, sink_component_id)
        self.environment = environment
        self.menu = None
        self.root_id = None
        self.template_handler = template_handler
        self._traffic_admin = None
        self.activity_log_manager = activity_log_manager
        self.dictionary = lib.dictionary.Dictionary(environment.checkpoint_handler) 



    def set_traffic_admin(self, traffic_admin):
        self._traffic_admin = traffic_admin



    # Returns how the menu currently looks for a default user, in the same form as calculated on client
    # Result should be cached so that succeeding calls to the method doesn not result in a rebuild 
    # All items returned are assumed to be dict's containing:
    # id : unique id in menu
    # parent_id : reference to parent menu item (-1 = root)
    # type : "item", "folder" or "auto_folder"
    # element_id : launch_id for item, tag_id (tag_property_id) for tags
    # title : title
    def get_current_menu(self):
        if self.menu is None:
            self.rebuild_menu()
        return self.root_id, self.menu

    # Rebuild the menu structure and cache it 
    def rebuild_menu(self):
        with database_api.QuerySession() as transaction:
            tag_properties = read_tag_properties_from_db(transaction)
            actions = self._traffic_admin.get_elements("menu_action")
            menuitems = []
            default_tag_set = ["SHOW", "ENABLED"]
            for action in actions:
                dummy, dot, launch_id = action.get_id().partition(".")

                menuitem = dict()
                menuitem['show'] = True
                menuitem['launch_id'] = launch_id
                menuitem['title'] = action.get_label()
                menuitem['enabled'] = True
                menuitem['icon_id'] = None
                menuitem['type'] = 'item'
                menuitem['tags'] = dialog_api1.get_tag_set(transaction, launch_id, -1)
                menuitems.append(menuitem)
            menu_builder = dialog_menu_lib.dialog_menu(self.checkpoint_handler, self.dictionary)
            root_id, menu = menu_builder.build_menu_using_tags(menuitems, tag_properties, admin=True)
            
            self.root_id = root_id
            self.menu = menu
            
            for menu_item in self.menu:
                if menu_item['type'] == 'folder' and menu_item.get("auto_menu_all", False):
                    child_list = menu_item.get("child_list", [])
                    child_list.append(self._get_rep_for_all_menu_item())
                    menu_item["child_list"] = child_list
                    
            

    def _get_rep_for_all_menu_item(self):
        all_item = dict()
        all_item["id"] = "menu_action.-1"
        all_item["element_id"] = "menu_action.-1"
        all_item["label"] = "<All>"
        all_item["menu_type"] = "auto_item"
        return all_item
    
    # Adds a tag or launch spec to the (menu) tag item.
    # items are in the same form as returned from menu
    # All expectable errors (such as cycle introduction) should be handled by raising OperationNotAllowedException
    def add_item_to_tag(self, item, to_item):
        with database_api.Transaction() as dbt:
            if item.get("menu_type") == "item":
                self.add_launch_spec_to_tag(item.get("id"), to_item.get("id"), dbt)
            else:
                self.add_tag_to_tag(item.get("id"),to_item.get("id"), dbt)
        self.rebuild_menu()
        return self.get_current_menu()

    # Same as add_item_to_tag except that in stead of an item the id of a launch spec is given as input
    def add_element_to_tag(self, element, to_item):
        with database_api.Transaction() as dbt:
            new_menu_item = dict()
            new_menu_item["id"] = element.get_id()
            type_, dot, element_id = element.get_id().partition(".")
            if type_ == "menu_action": 
                self.add_launch_spec_to_tag(element.get_id(), to_item.get("id"), dbt)
                new_menu_item["label"] = element.get_label()
                new_menu_item["menu_type"] = "item"
            elif type_ == "tag": 
                self.add_tag_to_tag(element.get_id(),to_item.get("id"), dbt)
                new_menu_item["label"] = element.get_label()
                new_menu_item["menu_type"] = "folder"
            else: # Assume action
                action_id = traffic_schema.get_launch_id(dbt, element.get_id())
                self.add_launch_spec_to_tag(action_id, to_item.get("id"), dbt)
                new_menu_item["label"] = element.get_label()
                new_menu_item["menu_type"] = "item"
        self.rebuild_menu()
        return new_menu_item
        
    # Same as add_item_to_tag except that item should be removed as child of 'from_item'
    def move_item_to_tag(self, item, from_item, to_item):
        from_item_id = from_item.get("id")
        to_item_id = to_item.get("id")
        if from_item_id != to_item_id:
            with database_api.Transaction() as dbt:
                item_id = item.get("id")
                if item.get("menu_type") == "item":
                    self.add_launch_spec_to_tag(item_id, to_item_id, dbt)
                    self.remove_launch_spec_from_tag(item_id, from_item_id, dbt)
                else:
                    self.remove_tag_from_tag(item_id, from_item_id, dbt)
                    self.add_tag_to_tag(item_id, to_item_id, dbt)
            self.rebuild_menu()
        return self.get_current_menu()
    
    # Remove item as child of 'parent_item'
    def remove_item_from_tag(self, item, parent_item):
        with database_api.Transaction() as dbt:
            if item.get("menu_type") == "item":
                self.remove_launch_spec_from_tag(item.get("id"), parent_item.get("id"), dbt)
            else:
                self.remove_tag_from_tag(item.get("id"), parent_item.get("id"), dbt)
        self.rebuild_menu()
        return self.get_current_menu()

    def _get_internal_id(self, external_id):
        name, dot, internal_id = str(external_id).rpartition(".")
        return internal_id 

    def remove_tag_from_field(self, config_spec, field_name, tag_name):

        def get_replace_string(name, field_name):
            if name=="custom_template":
                return field_name
            return None
        
        tag_str = config_spec.get_value(field_name)
        tag_set = set([ tag.strip() for tag in tag_str.split(',') if tag])
        new_tag_list = []
        custom_fields = []
        for tag in tag_set:
            if tag.upper()!=tag_name:
                new_tag_list.append(tag)
                if tag.startswith("%"):
                    custom_field_name = lib.variable_expansion.expand(tag, get_replace_string)
                    if custom_field_name:
                        custom_fields.append(custom_field_name)
        
        config_spec.set_value(field_name, ",".join(new_tag_list))
        for custom_field_name in custom_fields:
            self.remove_tag_from_field(config_spec, custom_field_name, tag_name)


    def add_or_remove_tag_to_launch_template(self, launch_id, tag_name, add, dbt):
        element_id = traffic_schema.get_action_id(dbt, launch_id)
        template = self.template_handler.get_element_template(element_id)
        if template:
            config_spec = ConfigTemplateSpec(template)
            try:
                if add:
                    tag_str = config_spec.get_value("dialog_tags")
                    if tag_str:
                        tag_str = "%s,%s" % (tag_str, tag_name)
                    else:
                        tag_str = tag_name
                    config_spec.set_value("dialog_tags", tag_str)
                else:
                    self.remove_tag_from_field(config_spec, "dialog_tags", tag_name)
                            
                self.template_handler.save_template_values(dbt, template, element_id)
            except config_spec.FieldNotFoundException:
                pass
        

        summary = "Add tag to Menu Action" if add else "Remove tag from Menu Action"
        lines = []
        lines.append("tag_name : %s" % tag_name)
        lines.append("menu action id : %s" % element_id)
        details_string = "\n".join(lines)
        self.activity_log_manager.copyUpdateAndAdd(dbt,
                                                   type="Update",
                                                 access_right="ProgramAccess",
                                                 summary=summary,
                                                  details = details_string,
                                                )

        
    def add_launch_spec_to_tag(self, launch_id, tag_id, dbt):
        launch_id = self._get_internal_id(launch_id)
        tag_id = self._get_internal_id(tag_id)
        
        tag_name = None
        if tag_id=="-1":
            tag_name = dialog_menu_lib.ROOT_TAG_NAME
        else:
            tag = dbt.get(DialogTagProperties, tag_id)
            if tag:
                tag_name = tag.tag_name
        if tag_name:
            tag_set = dialog_api1.get_tag_set(dbt, launch_id, -1)
            tag_set.add(tag_name)
            dialog_update_user_launch_item_tags(dbt, -1, launch_id, tag_set)
            self.add_or_remove_tag_to_launch_template(launch_id, tag_name, True, dbt)
        
    def remove_launch_spec_from_tag(self, launch_id, tag_id, dbt):
        launch_id = self._get_internal_id(launch_id)
        tag_id = self._get_internal_id(tag_id)

        if tag_id=="-1":
            tag_name = dialog_menu_lib.ROOT_TAG_NAME
        else:
            tag = dbt.get(DialogTagProperties, tag_id)
            tag_name = tag.tag_name
        
        tag_set = dialog_api1.get_tag_set(dbt, launch_id, -1)
        try:
            tag_set.remove(tag_name)
        except KeyError:
            return
        dialog_update_user_launch_item_tags(dbt, -1, launch_id, tag_set)
        self.add_or_remove_tag_to_launch_template(launch_id, tag_name, False, dbt)
        

    def add_tag_to_tag(self, tag_id, to_tag_id, dbt):
        tag_id = self._get_internal_id(tag_id)
        to_tag_id = self._get_internal_id(to_tag_id)
        
        tag = dbt.get(DialogTagProperties, tag_id)
        tag.menu_show = True
        to_tag = dbt.get(DialogTagProperties, to_tag_id)
        if to_tag:
            to_tag_name = to_tag.tag_name
            tag.add_parent_menu_tag(to_tag_name)
        else:
            to_tag_name = dialog_menu_lib.ROOT_TAG_NAME
            
        summary = "Add tag as sub menu"
        lines = []
        lines.append("tag_name : %s" % tag.tag_name)
        lines.append("tag_id : %s" % tag.id)
        lines.append("parent menu tag name : %s" % to_tag_name)
        lines.append("parent menu tag id : %s" % to_tag_id)
        details_string = "\n".join(lines)

        self.activity_log_manager.copyUpdateAndAdd(dbt,
                                                   type="Update",
                                                 access_right="Tag",
                                                 summary=summary,
                                                  details = details_string,
                                                )
        
        

    def remove_tag_from_tag(self, tag_id, from_tag_id, dbt):
        tag_id = self._get_internal_id(tag_id)
        from_tag_id = self._get_internal_id(from_tag_id)
        
        tag = dbt.get(DialogTagProperties, tag_id)
        parent_tag_set = tag.get_parent_menu_tag_set()

        if parent_tag_set:
            from_tag = dbt.get(DialogTagProperties, from_tag_id)
            if from_tag:
                from_tag_name = from_tag.tag_name
                try:
                    parent_tag_set.remove(from_tag_name)
                except KeyError:
                    pass
                tag.remove_parent_menu_tag(from_tag_name, dbt)

                # activity log
                summary = "Remove tag from menu tag"
                lines = []
                lines.append("tag_name : %s" % tag.tag_name)
                lines.append("tag_id : %s" % tag.id)
                lines.append("parent menu tag name : %s" % from_tag_name)
                lines.append("parent menu tag id : %s" % from_tag_id)
                details_string = "\n".join(lines)
                
                self.activity_log_manager.copyUpdateAndAdd(dbt,
                                                           type="Update",
                                                         access_right="Tag",
                                                         summary=summary,
                                                          details = details_string,
                                                        )
                
            
        if from_tag_id < 0:
            # activity log - remove tag from root
            summary = "Remove tag from menu tag"
            lines = []
            lines.append("tag_name : %s" % tag.tag_name)
            lines.append("tag_id : %s" % tag.id)
            lines.append("parent menu tag name : %s" % dialog_menu_lib.ROOT_TAG_NAME)
            lines.append("parent menu tag id : %s" % from_tag_id)
            details_string = "\n".join(lines)
            
            self.activity_log_manager.copyUpdateAndAdd(dbt,
                                                     type="Update",
                                                     access_right="ProgramAccess",
                                                     summary=summary,
                                                      details = details_string,
                                                    )
            
             
#                try:
#                    parent_tag_set.remove(from_tag_name)
#                except KeyError:
#                    return
#                tag.menu_parentmenutags = ','.join(parent_tag_set)
        if not parent_tag_set:
            # root menu
            tag.menu_show = False
            
            
    
    def get_menu_items(self):
        tags = self.get_elements("tag")
        actions = self._traffic_admin.get_elements("menu_action")

        root_item = dict()
        root_item["id"] = "tag.-1"
        root_item["label"] = "G/On Menu"
        root_item["menu_type"] = "folder"
        
        items = [root_item]
        for tag in tags:
            menu_item = dict()
            menu_item["id"] = tag.get_id()
            menu_item["label"] = tag.get_label()
            menu_item["menu_type"] = tag.get_menu_type()
            menu_item["max_items"] = tag.get_max_items()
            items.append(menu_item)

        items.append(self._get_rep_for_all_menu_item())

        for action in actions:
            menu_item = dict()
            dummy, dot, launch_id = action.get_id().partition(".")
            menu_item["id"] = "action.%s" % launch_id
            menu_item["label"] = action.get_label()
            menu_item["menu_type"] = "item"
            items.append(menu_item)
            
        return items
    
    def get_elements(self, element_type, search_filter=None):
        with database_api.ReadonlySession() as dbs:
            if element_type == "tag":
                tags = dbs.select(database_schema.DialogTagProperties, 
                                  database_schema.DialogTagProperties.user_id == -1, 
                                  order_by=database_schema.DialogTagProperties.tag_priority)
                return [TagElement(tag) for tag in tags]
        return []
        
    def get_specific_elements(self, element_type, element_ids):
        with database_api.ReadonlySession() as dbs:
            if element_type == "tag":
                tag_ids = [self._get_internal_id(tag_id) for tag_id in element_ids]
                tags = dbs.select(database_schema.DialogTagProperties, 
                                  database_api.and_(database_api.in_(database_schema.DialogTagProperties.id, tag_ids),
                                                    database_schema.DialogTagProperties.user_id == -1) 
                                  )
                return [TagElement(tag) for tag in tags]
        return []
        
    # return editEnabled, deleteEnabled
    def get_config_enabling(self, element_type):
        if element_type == "tag":
            return True, True
            
        
    # return templates for creating elements    
    def get_create_templates(self, element_type):
        if element_type == "tag":
            with database_api.ReadonlySession() as db_session:
                return [self.config_get_template(element_type, None, None, db_session)]
#            template_ref = ns0.TemplateRefType_Def("TemplateRefType").pyclass()
#            template_ref._template_name = "tag_template"
#            template_ref._template_title = "Tag"
#            template_ref._entity_type = "Tag"
#            template_ref._template_type = -1
#            return [template_ref]
    
    def config_get_template(self, element_type, element_id, custom_template, db_session):
        if element_type == "tag":
            return self.get_tag_template(element_id, db_session)
    
    # update element     
    def config_update_row(self, element_type, element_id, template, dbt):
        if element_type=="tag":
            config_spec = ConfigTemplateSpec(template)
            self._is_tag_data_valid(config_spec)
            tag = dbt.get(DialogTagProperties, config_spec.get_value("id"))
            #self.set_parent_menu_tags_from_string(tag, config_spec.get_value("menu_parentmenutags"), dbt)
            if tag:
                errors = config_spec.update_obj_with_values(tag, exclude_fields=["id", "menu_parentmenutags"], throw_error=False)
                if errors:
                    for name in errors:
                        self.checkpoint_handler.CheckPoint("config_update_row", "dialog_api", lib.checkpoint.WARNING, message="Columh '%s' does not exist in database" % name)
           
        else:
            raise NotImplementedError()
        
    def _is_tag_data_valid(self, config_spec):
        tag_name = config_spec.get_value("tag_name")
        normalised_tag_name = check_tag_name(tag_name)
        if normalised_tag_name in dialog_menu_lib.built_in_tag_names:
            raise OperationNotAllowedException("This tag name is reserved for internal use") 
        config_spec.set_value("tag_name", normalised_tag_name)
            
    # TODO : add cycle check to this..
    def set_parent_menu_tags_from_string(self, tag, parent_tag_string, dbt):
        tag_set = set([ check_tag_name(tag.strip()) for tag in parent_tag_string.split(',') if tag])
        tag.set_parent_menu_tags(tag_set, dbt)
        
        
        
    # create element
    def config_create_row(self, element_type, template, dbt):
        if element_type=="tag":
            config_spec = ConfigTemplateSpec(template)
            self._is_tag_data_valid(config_spec)
            tag = dbt.add(DialogTagProperties())
            tag.user_id = -1
#            self.set_parent_menu_tags_from_string(tag, config_spec.get_value("menu_parentmenutags"), dbt)
            errors = config_spec.update_obj_with_values(tag, exclude_fields=["menu_parentmenutags"], throw_error=False)
            if errors:
                for name in errors:
                    self.checkpoint_handler.CheckPoint("config_update_row", "dialog_api", lib.checkpoint.WARNING, message="Columh '%s' does not exist in database" % name)
            dbt.flush()
            return tag.id
           
        else:
            raise NotImplementedError()
        
    # delete element
    def config_delete_row(self, element_type, element_id, dbt):
        if element_type=="tag":
            element_id = self._get_internal_id(element_id)
            tag = dbt.get(DialogTagProperties, element_id)
            tag.delete(dbt)
            return True
        else:
            raise NotImplementedError()
        
    def get_tag_template(self, element_id, db_session):
        config_spec = ConfigTemplateSpec()
        config_spec.init("TagTemplate", "Edit Tag", "")

        field = config_spec.add_field("tag_name", 'Name', ConfigTemplateSpec.VALUE_TYPE_STRING)
        field.set_attribute_max_length(256)
#        if element_id:
#            # Can't change tag name once created(?)
#            field.set_attribute_read_only(True)
        field = config_spec.add_field("menu_caption", 'Caption', ConfigTemplateSpec.VALUE_TYPE_STRING)
        field.set_attribute_max_length(256)
            
        if not element_id:
            config_spec.add_field("menu_show", 'Show in menu', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, value=True)
        else:
            config_spec.add_field("id", 'Id', ConfigTemplateSpec.VALUE_TYPE_STRING, field_type=ConfigTemplateSpec.FIELD_TYPE_HIDDEN)
            config_spec.add_field("menu_show", 'Show in menu', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN)

        field = config_spec.add_field("menu_parentmenutags", 'Parent tags', ConfigTemplateSpec.VALUE_TYPE_STRING)
        field.set_attribute_read_only(True)
        config_spec.add_field("menu_maxitems", 'Max items to show (0=All)', ConfigTemplateSpec.VALUE_TYPE_INTEGER)
        
        sort_options = dialog_menu_lib.dialog_menu.sort_properties
        if sort_options:
            field = config_spec.add_field("menu_sortitems", 'Sort option', ConfigTemplateSpec.VALUE_TYPE_STRING)
            for (value, title) in sort_options:
                config_spec.add_selection_choice(field, title, value)
        
#        config_spec.add_field("tag_priority", 'Priority', ConfigTemplateSpec.VALUE_TYPE_INTEGER)
#        config_spec.add_field("menu_overwrite_enabled", 'Override item enabling', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, value=False)
        config_spec.add_field("menu_overwrite_show", 'Override item show', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, value=False)
        config_spec.add_field("auto_menu_all", 'Automatically add to all items', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, value=False)
            
        if element_id:
            element_id = self._get_internal_id(element_id)            
            tag = db_session.get(DialogTagProperties, element_id)
            if tag:
                config_spec.set_values_from_object(tag)
                config_spec.set_value("menu_parentmenutags", ",".join(tag.get_parent_menu_tag_set()))
        else:
            config_spec.set_value("menu_sortitems", dialog_menu_lib.dialog_menu.default_sort_property)
            

#        schema_api.Column('menu_sortitems', schema_api.String(20)),
#        schema_api.Column('menu_maxitems', schema_api.Integer),
#        schema_api.Column('menu_showinparent', schema_api.Integer),
#        schema_api.Column('menu_removefromparent', schema_api.Integer),
#        schema_api.Column('menu_overwrite_enabled', schema_api.Integer),
#        schema_api.Column('menu_overwrite_show', schema_api.Integer),
#        schema_api.Column('item_enabled', schema_api.Integer),
#        schema_api.Column('item_show', schema_api.Integer)

        
        return config_spec.get_template()
    

class TagElement(ElementType):
    
    def __init__(self, dialog_tag_property):
        self.dialog_tag_property = dialog_tag_property
        
    def get_id(self):
        return str("tag.%s" % self.dialog_tag_property.id)
    
    def get_entity_type(self):
        return "Dialog"

    def get_label(self):
#        if self.dialog_tag_property.menu_caption:
#            return "%s (%s)" % (self.dialog_tag_property.menu_caption, self.dialog_tag_property.tag_name)
#        else:  
#            return "(%s)" % (self.dialog_tag_property.tag_name,)
        base = self.dialog_tag_property.menu_caption if self.dialog_tag_property.menu_caption else ''
        annotations = [self.dialog_tag_property.tag_name]
        if self.dialog_tag_property.menu_maxitems:
            annotations.append("Top %s" % self.dialog_tag_property.menu_maxitems)
        if self.dialog_tag_property.menu_sortitems: # and self.dialog_tag_property.menu_sortitems!=dialog_menu_lib.dialog_menu.default_sort_property: 
            sort_order_name = dialog_menu_lib.dialog_menu.get_sortitem_title(self.dialog_tag_property.menu_sortitems)
            annotations.append(sort_order_name)
            
        annotation_str = ", ".join(annotations)
        return "%s (%s)" % (base, annotation_str)

    def get_menu_type(self):
        if self.dialog_tag_property.auto_menu_all:
            return "auto_folder"
        else:
            return "folder"
        
    def get_max_items(self):
        return self.dialog_tag_property.menu_maxitems
    

