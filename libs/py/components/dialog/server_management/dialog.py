"""
Module for dialog reporting used by the Management Server.
"""
from __future__ import with_statement
import lib.checkpoint
import datetime

from components.database.server_common import schema_api
from components.database.server_common import database_api

import components.dialog

from components.dialog.server_common import database_schema
import components.plugin.server_management.plugin_socket_access_notification

import components.management_message.server_management.session_recieve



class DialogAdminReceiver(components.management_message.server_management.session_recieve.ManagementMessageSessionRecieverBase):
    def __init__(self, environment):
        components.management_message.server_management.session_recieve.ManagementMessageSessionRecieverBase.__init__(self, environment.checkpoint_handler, components.dialog.component_id)
        self.environment = environment

    def _get_session(self, t, unique_session_id):
        sessions = t.select(database_schema.Session, database_schema.Session.unique_session_id==unique_session_id)
        if(len(sessions) == 0):
            return None
        else:
            return sessions[0]

    def _get_traffic_proxy(self, t, unique_session_id, proxy_id):
        traffic_proxys = t.select(database_schema.TrafficProxy, database_api.and_(database_schema.TrafficProxy.unique_session_id==unique_session_id, database_schema.TrafficProxy.proxy_id==proxy_id))
        if(len(traffic_proxys) == 0):
            return None
        else:
            return traffic_proxys[0]
        
            
    def dialog_item_launched(self, unique_session_id, server_sid, launch_key, launch_id, auto_launch=False):
        if not auto_launch:
            launch_key = unicode(launch_key)
            with database_api.Transaction() as t:
    
                self.environment.checkpoint_handler.Checkpoint("MenuItem activation Sink", "dialog_sink", lib.checkpoint.DEBUG,  launch_key=launch_key, launch_id=launch_id)
                
                stat_records = t.select(database_schema.DialogStatistics, 
                                        database_api.and_(
                                                          database_schema.DialogStatistics.launch_id==launch_id,
                                                          database_schema.DialogStatistics.launch_key==launch_key))
                                                          
                if len(stat_records)==0:
                    record = database_schema.DialogStatistics()
                    record.launch_id = launch_id
                    record.last_date = datetime.datetime.now()
                    record.launch_key = unicode(launch_key)
                    record.launch_count = 1
                    t.add(record)
                else: 
                    for record  in stat_records:
                        record.launch_count = record.launch_count +1
                        record.last_date = datetime.datetime.now() 

                #
                # Add a row in the launch statistics table. This is used for calculating a
                # top x most frequently used menu order.
                #
                launch_stat_records = t.select(database_schema.DialogLaunchStatistics, 
                                               database_api.and_(
                                                                 database_schema.DialogLaunchStatistics.launch_id==launch_id,
                                                                 database_schema.DialogLaunchStatistics.launch_key==launch_key))

                # If there is more than x records we delete the oldest until we reach the limit.
                _LIMIT = 3
                if len(launch_stat_records) > _LIMIT:
                    _recordremovallist = []
                    # Figure out which records to remove.
                    for record in launch_stat_records:
                        _recordremovallist.append(record.launch_date)         
                    _recordremovallist.sort(reverse=True)
                    _recordremovallist = _recordremovallist[_LIMIT:]
                    # Remove the selected records.
                    for record in launch_stat_records:
                        if record.launch_date in _recordremovallist:
                            t.delete(record)

                # Add the new record to launch statistics.
                record = database_schema.DialogLaunchStatistics()
                record.launch_id = launch_id
                record.launch_key = unicode(launch_key)
                record.launch_date = datetime.datetime.now()
                t.add(record)
    