# Add dialog tag reader here...

from __future__ import with_statement
import lib.checkpoint
import datetime

from components.database.server_common import schema_api
from components.database.server_common import database_api

import components.dialog

from components.dialog.server_common import database_schema
from components.database.server_common.database_api import QuerySession
        
def get_tag_set(dbs, launch_id, user_id=-1):
    """
      Reads a tag set for an action (and user)
           
    """
    alltags = dbs.select(database_schema.DialogLaunchTags, 
                                 database_api.and_(
                                              database_schema.DialogLaunchTags.launch_id==launch_id,
                                              database_schema.DialogLaunchTags.user_id==user_id))
    tagset = set({})
    for tag in alltags:
        tagset.add(tag.tag_name)

    return tagset


def get_all_tags_by_launch_id(dbs, user_id=-1):
    """
      Reads a tag set for an action (and user)
           
    """
    alltags = dbs.select(database_schema.DialogLaunchTags, database_schema.DialogLaunchTags.user_id==user_id)
    tag_dict = dict()
    for tag in alltags:
        launch_id = tag.launch_id
        tagset = tag_dict.get(launch_id, set())
        tagset.add(tag.tag_name)
        tag_dict[launch_id] = tagset

    return tag_dict

def get_tag_string(dbs, launch_id , user_id=-1):
    return ','.join(get_tag_set(dbs, launch_id, user_id))



   
def tag_record_to_tag_dict(tag):
    """ 
      Copies a database record into at dictionary
      
      The important thing here is that we do not copy the None/null value
      We do not want dictionary values for Null values (as that would
      overwrite already existing values in the tag evaluation)
       
    
    """
    properties = {}
                
                
    properties['tag_id'] = tag.id            
    # menu properties
    if tag.tag_priority:
        properties['tag_priority'] = tag.tag_priority
    else:
        properties['tag_priority'] = 0 
    
    properties['menu_show'] = tag.menu_show

    if (tag.menu_caption):
        properties['menu_caption'] = tag.menu_caption
    else:
        properties['menu_caption'] = ''

    query_session = QuerySession()
    parent_tags = query_session.select(database_schema.dbapi_dialog_table_parentmenutag, database_schema.dbapi_dialog_table_parentmenutag.c.tag_property_id==tag.id)
    properties['menu_parentmenutags'] = set([parent.tag_name for parent in parent_tags])

    if tag.menu_sortitems:
        properties['menu_sortitems'] = tag.menu_sortitems

    if tag.menu_maxitems:
        properties['menu_maxitems'] = tag.menu_maxitems

    if tag.menu_showinparent != None:
        properties['menu_showinparent'] = tag.menu_showinparent

    if tag.menu_removefromparent != None:
        properties['menu_removefromparent'] = tag.menu_removefromparent
        
    if tag.menu_overwrite_enabled != None:
        properties['menu_overwrite_enabled'] = tag.menu_overwrite_enabled
        
    if tag.menu_overwrite_show != None:
        properties['menu_overwrite_show'] = tag.menu_overwrite_show

    if tag.item_enabled != None:
        properties['item_enabled'] = bool(tag.item_enabled)
    
    if tag.item_show != None:
        properties['item_show'] = bool(tag.item_show)
        
    properties['auto_launch'] = tag.item_autolaunch_once
    properties['auto_launch_first'] = tag.item_autolaunch_first_start

    properties['auto_menu_all'] = tag.auto_menu_all
    
    return properties

    
def read_tag_properties_from_db(transaction, user_id = -1):
    
    """ 
    returns a series of tag dictionaries with all  tags for a user
    
    user_id = -1 gives for the no user scenario
    """
    
    tbl_tag_properties = transaction.select(database_schema.DialogTagProperties, 
                                           database_schema.DialogTagProperties.user_id==user_id, 
                                           order_by=database_schema.DialogTagProperties.tag_priority)
    tag_props = {}
    for tag in tbl_tag_properties:
        # item properties
        properties = tag_record_to_tag_dict(tag) 
        tag_props.update({tag.tag_name : properties})
        
    return tag_props