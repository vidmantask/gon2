"""
This file contains the database schema for the dialog component
"""
from __future__ import with_statement
from components.database.server_common import schema_api
from components.database.server_common import database_api


dbapi_dialog_ro = schema_api.SchemaFactory.get_creator("dialog_component_ro")


def connect_to_database(environment):
    dbapi_dialog_ro.bind(environment.get_default_database())


dbapi_dialog_table_dialog_statistics = dbapi_dialog_ro.create_table("dialog_statistics",
                                                                    schema_api.Column('launch_id', schema_api.String(256)),
                                                                    schema_api.Column('launch_key', schema_api.String(256)),
                                                                    schema_api.Column('launch_count', schema_api.Integer),
                                                                    schema_api.Column('last_date', schema_api.DateTime)
                                                                    )

dbapi_dialog_table_dialog_launch_statistics = dbapi_dialog_ro.create_table("dialog_launch_statistics",
                                                                    schema_api.Column('launch_id', schema_api.String(256)),
                                                                    schema_api.Column('launch_key', schema_api.String(256)),
                                                                    schema_api.Column('launch_date', schema_api.DateTime)
                                                                    )

dbapi_dialog_table_dialog_tag_properties = dbapi_dialog_ro.create_table("dialog_tag_properties",
                                                                    schema_api.Column('user_id', schema_api.String(1024)),
                                                                    schema_api.Column('tag_name', schema_api.String(256)),
                                                                    schema_api.Column('tag_priority', schema_api.Integer), # sort, tag priority for sorting out contradictions 
                                                                    schema_api.Column('menu_show', schema_api.Boolean, default=False), # Boolean
                                                                    schema_api.Column('menu_caption', schema_api.String(256), default=''), # title
                                                                    schema_api.Column('menu_sortitems', schema_api.String(20)), # launch item property name for sorting. enum. "Sort order" 
                                                                    schema_api.Column('menu_maxitems', schema_api.Integer, default=0), # Max no of launch elements in menu. 0 == All
                                                                    schema_api.Column('menu_showinparent', schema_api.Integer), # Not implemented. Show in parent menu
                                                                    schema_api.Column('menu_removefromparent', schema_api.Integer), # Not implemented.
                                                                    schema_api.Column('menu_overwrite_enabled', schema_api.Boolean, default=False), # BOOL, Overwrie enabled on menu items
                                                                    schema_api.Column('menu_overwrite_show', schema_api.Boolean, default=False), # BOOL,Overwrie show on menu items
                                                                    schema_api.Column('item_enabled', schema_api.Boolean), # BOOL, ENABLED tag
                                                                    schema_api.Column('item_show', schema_api.Boolean), # Bool
                                                                    schema_api.Column('item_autolaunch_once', schema_api.Boolean, default=False), # Bool
                                                                    schema_api.Column('item_autolaunch_first_start', schema_api.Boolean, default=False), # Bool
                                                                    schema_api.Column('auto_menu_all', schema_api.Boolean, default=False), # Bool : if true, tag is automatically placed on all launch items
                                                                    )

dbapi_dialog_table_parentmenutag = dbapi_dialog_ro.create_table("parentmenutag",
                                                                schema_api.Column('tag_property_id', schema_api.Integer, nullable=False),
                                                                schema_api.Column('tag_name', schema_api.String(256), nullable=False),
                                                                )


dbapi_dialog_table_dialog_launch_tags = dbapi_dialog_ro.create_table("dialog_launch_tags",
                                                                    schema_api.Column('launch_id', schema_api.String(256), nullable=False),
                                                                    schema_api.Column('user_id', schema_api.String(1024)),
                                                                    schema_api.Column('tag_name', schema_api.String(256))
                                                                    )

dbapi_dialog_table_dialog_launch_tag_generators = dbapi_dialog_ro.create_table("dialog_launch_tag_generators",
                                                                               schema_api.Column('launch_id', schema_api.Integer),
                                                                               schema_api.Column('tag_generator', schema_api.String(1000))
                                                                               )

dbapi_dialog_ro.add_foreign_key_constraint("parentmenutag", "tag_property_id", "dialog_tag_properties")
                                                                               

schema_api.SchemaFactory.register_creator(dbapi_dialog_ro)


class DialogStatistics(database_api.PersistentObject):
    pass
    
    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls, dbapi_dialog_table_dialog_statistics)

class DialogLaunchStatistics(database_api.PersistentObject):
    
    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls, dbapi_dialog_table_dialog_launch_statistics)

class ParentMenuTag(object):

    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls, dbapi_dialog_table_parentmenutag)

class DialogTagProperties(database_api.PersistentObject):

    def _add_parent_tag(self, tag):
        parent_menu_tag = ParentMenuTag()
        parent_menu_tag.tag_name = tag
        self.parent_tags.append(parent_menu_tag)


    def get_parent_menu_tag_set(self):
        return set([parent.tag_name for parent in self.parent_tags])
    
        
    def set_parent_menu_tags(self, tags, transacton):
        for current_tag in self.parent_tags:
            if tags:
                tag_name = tags.pop()
                current_tag.tag_name = tag_name
            else:
                transacton.delete(current_tag)
        for tag_name in tags:
            self._add_parent_tag(tag_name)
            
            

    def add_parent_menu_tag(self, tag):
        current_tags = self.get_parent_menu_tag_set()
        if not tag in current_tags:
            self._add_parent_tag(tag)

    def remove_parent_menu_tag(self, tag, transaction):
        for tag_obj in self.parent_tags:
            if tag_obj.tag_name == tag:
                transaction.delete(tag_obj)
        
    def delete(self, transaction):
        for tag_obj in self.parent_tags:
            transaction.delete(tag_obj)
        transaction.delete(self)
        
        
        
    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls, dbapi_dialog_table_dialog_tag_properties, [schema_api.Relation(ParentMenuTag, "parent_tags")])

class DialogLaunchTags(database_api.PersistentObject):
    
    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls, dbapi_dialog_table_dialog_launch_tags)

class DialogLaunchTagGenerators(database_api.PersistentObject):
    
    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls, dbapi_dialog_table_dialog_launch_tag_generators)

ParentMenuTag.map_to_table()
DialogStatistics.map_to_table()
DialogLaunchStatistics.map_to_table()
DialogTagProperties.map_to_table()
DialogLaunchTags.map_to_table()
DialogLaunchTagGenerators.map_to_table()
