"""
Unittest of dialog admimessage sink component
"""
from __future__ import with_statement
import time
import os
import unittest

from lib import giri_unittest
import components.database
import components.database.server_common.connection_factory
import lib.checkpoint

import components.environment

import components.dialog.common.dialog_menu_lib as dialog_menu_lib


class dialog_menu_lib_test(unittest.TestCase):


    def atest(self):
        environment = components.environment.Environment(giri_unittest.get_checkpoint_handler(), 'sqlite:///:memory:', 'dialog_menu_lib_unittest')

        with giri_unittest.get_checkpoint_handler().CheckpointScope("unittest_dialog_menu_lib", "dialog_menu_lib", lib.checkpoint.DEBUG) as cp:

            menuitems = [{'show': True, 'icon_id': None, 'tags': set(['ALL', 'SHOW', 'WINDOWS', 'ENABLED', '_LAUNCH_ID_1', 'BROWSER']), 'enabled': False, 'parent_id': 1, 'title': u'Windows browse www.giritech.com', 'strong': False, 'type': 'item', 'id': 2},
             {'show': True, 'icon_id': None, 'tags': set(['ALL', 'SHOW', 'WINDOWS', 'ENABLED', '_LAUNCH_ID_2', 'BROWSER', 'DEVELOPER']), 'enabled': False, 'parent_id': 1, 'title': u'Windows browse Mantis', 'strong': False, 'type': 'item', 'id': 3},
             {'tags': set(['ALL', 'SHOW', 'ENABLED', 'OUTLOOK', '_LAUNCH_ID_18', 'CLIENTOK', 'LINUX', 'MAIL', 'SERVEROK', 'BROWSER']), 'icon_id': None, 'title': u'Linux OWA', 'enabled': False, 'launch_lastdate': '2008-09-10T12:59:23', 'parent_id': 1, 'launch_count': 1, 'show': True, 'launch_id': 1, 'strong': False, 'type': 'item', 'id': 4},
             {'show': True, 'icon_id': None, 'tags': set(['ALL', 'RDP', 'SHOW', 'ENABLED', 'LINUX', '_LAUNCH_ID_20']), 'enabled': False, 'parent_id': 1, 'title': u'Linux RD to My PC', 'strong': False, 'type': 'item', 'id': 5},
             {'show': False, 'icon_id': None, 'tags': set(['ALL', 'RDP', 'NOSHOW', 'ENABLED', 'EMPTY', 'LINUX', '_LAUNCH_ID_21']), 'enabled': False, 'parent_id': 1, 'title': u'No Show', 'strong': False, 'type': 'item', 'id': 6}
             ]

            tags = {
                 'ALL': {'tag_id' : 1, 'menu_show': True, 'menu_caption': 'All Programs', 'showinparent': False, 'itemoverwrite_show': True, 'tag_priority' : 1, 'menu_sortitems' : 'launch_count', 'menu_maxitems' : 3},
                 'ENABLE': {'tag_id' : 2,'menu_show': False, 'enabled': True, 'tag_priority' : 1},
                 'SHOW': {'tag_id' : 3,'menu_show': False, 'show': True, 'tag_priority' : 1, 'menu_caption':'Show menu', 'menu_sortitems' : 'last_date'},
                 'WINDOWS': {'tag_id' : 4,'menu_show': False, 'menu_caption': None, 'tag_priority' : 1},
                 'LINUX': {'tag_id' : 5,'menu_show': False, 'menu_caption': None, 'tag_priority' : 1},
                 'BROWSER': {'tag_id' : 6,'menu_show': True, 'menu_caption': 'Browser Apps', 'parentmenutags': set(['ALL']), 'tag_priority' : 1},
                 'EMPTY': {'tag_id' : 7,'menu_show': True, 'menu_caption': 'Empty: Should not show', 'tag_priority' : 1}
                 }

            for tag_dict in tags.values():
                if not tag_dict.get("menu_parentmenutags", None):
                    tag_dict["menu_parentmenutags"] = []
            for menuitem in menuitems:
                menuitem['launch_id'] = menuitem['id']

            menu  = dialog_menu_lib.dialog_menu(giri_unittest.get_checkpoint_handler(), giri_unittest.get_dictionary())
            for i in range(0,10):
                finalmenu =  menu.build_menu_using_tags(menuitems, tags)

            #menu.print_menu(menu)

            isAll = False
            isId8 = False
            countinAll = 0
            isEmpty = False
            for item in finalmenu:
                if item['title'] =='All Programs':
                    isAll = True
                if item['id'] == 8:
                    isId8 = True
                if item['parent_tag_id'] == 1:
                    countinAll = countinAll + 1
                if item['title'] =='Empty: Should not show':
                    isEmpty = True

            self.assertTrue(isAll)
            self.assertTrue(isId8)
            self.assertEqual(countinAll, 3)
            self.assertFalse(isEmpty) # There should not be an empty menu

        # run time example:
        # 20090115T101646.019802      unittest_dialog_menu_lib             duration=000000.172630 scope_end=0

    def _find_folder(self, name, items):
        for item in items:
            if item['type']=='folder' and item['tag_id'] == name:
                return item
        return None

    def _find_folders(self, name, items):
        folders = []
        for item in items:
            if item['type']=='folder' and item['tag_id'] == name:
                folders.append(item)
        return folders

    def _find_launch_item(self, items, **kwargs):
        for item in items:
            if item['type']=='item':
                found = True
                for key, value in kwargs.items():
                    if item[key] != value:
                        found = False
                        break
                if found:
                    return item
        return None

    def _find_launch_items(self, items, **kwargs):
        folders = []
        for item in items:
            if item['type']=='item':
                found = True
                for key, value in kwargs.items():
                    if item[key] != value:
                        found = False
                        break
                if found:
                    folders.append(item)
        return folders


    def test1(self):
        environment = components.environment.Environment(giri_unittest.get_checkpoint_handler(), 'sqlite:///:memory:', 'dialog_menu_lib_unittest')

        with giri_unittest.get_checkpoint_handler().CheckpointScope("unittest_dialog_menu_lib", "dialog_menu_lib", lib.checkpoint.DEBUG) as cp:

            menuitems = [
             #0
             {
              'show': True,
              'tags': set(['ALL', 'SHOW', 'WINDOWS', 'ENABLED', '_LAUNCH_ID_1', 'BROWSER']),
              'enabled': False,
              'title': u'Windows browse www.giritech.com',
             },
             #1
             {
               'show': True,
               'tags': set(['ALL', 'SHOW', 'WINDOWS', 'ENABLED', '_LAUNCH_ID_2', 'BROWSER', 'DEVELOPER']),
               'enabled': False,
               'title': u'Windows browse Mantis',
               'launch_count': 2,
             },
             #2
             {
               'show': True,
               'tags': set(['ALL', 'SHOW', 'ENABLED', 'OUTLOOK', '_LAUNCH_ID_18', 'CLIENTOK', 'LINUX', 'MAIL', 'SERVEROK', 'BROWSER']),
               'title' : u'Linux OWA',
               'enabled': False,
               'launch_lastdate': '2008-09-10T12:59:23',
               'launch_count': 1,
             },
             #3
             {
                'show': True,
                'tags': set(['ALL', 'RDP', 'SHOW', 'ENABLED', 'LINUX', '_LAUNCH_ID_20']),
                'enabled': False,
                'title': u'Linux RD to My PC',
             },
             #4
             {
                'show': False,
                'tags': set(['ALL', 'RDP', 'NOSHOW', 'ENABLED', 'EMPTY', 'LINUX', '_LAUNCH_ID_21']),
                'enabled': False,
                'title': u'No Show',
            },
           ]

            tags = {
                 'TOP3': {
                          'menu_show': True,
                          'menu_caption': 'Max3',
                          'menu_sortitems' : 'launch_count',
                          'menu_maxitems' : 3,
                          'auto_menu_all' : True,
                        },
                 'ALL': {
                          'menu_show': True,
                          'menu_caption': 'All',
                          'menu_sortitems' : 'launch_count',
                          'auto_menu_all' : True,
                        },
                 'ENABLE': {
                          'menu_show': False,
                          'enabled': True,
                         },
                 'SHOW': {
                           'menu_show': False,
                           'show': True,
                           'menu_caption':'Show menu',
                           'menu_sortitems' : 'last_date'
                         },
                 'WINDOWS': {
                             'menu_show': False,
                             'menu_caption': None,
                            },
                 'LINUX': {
                            'menu_show': False,
                            'menu_caption': None,
                          },
                 'BROWSER': {
                             'menu_show': True,
                             'menu_caption': 'Browser Apps',
                             'menu_parentmenutags': set(['TOP3']),
                             },
                 'EMPTY': {
                           'menu_show': True,
                           'menu_caption': 'Empty: Should not show',
                          },
                 }

            count = 0
            auto_tags = []
            for name, tag_dict in tags.items():
                if not tag_dict.get("menu_parentmenutags", None):
                    tag_dict["menu_parentmenutags"] = []
                tag_dict['tag_id'] = name
                tag_dict['tag_priority'] = 0
                if tag_dict.get("auto_menu_all"):
                    auto_tags.append(name)
                count += 1

#            for name, tag_dict in tags.items():
#                print name, ' ', tag_dict['tag_id']
            count = 0
            for menuitem in menuitems:
                menuitem['launch_id'] = count
                menuitem['icon_id'] = None
                menuitem['type'] = 'item'
                for tag in auto_tags:
                    menuitem['tags'].add(tag)
                count += 1

            menu  = dialog_menu_lib.dialog_menu(giri_unittest.get_checkpoint_handler(), giri_unittest.get_dictionary())
            finalmenu =  menu.build_menu_using_tags(menuitems, tags)

#            menu_dict = dict([(item['element_id'], item) for item in finalmenu])


            self.assertTrue(self._find_folder("EMPTY", finalmenu) is None)
            self.assertEquals(len(self._find_folders("BROWSER", finalmenu)), 1)
            self.assertEquals(len(self._find_folders("TOP3", finalmenu)), 1)
            self.assertEquals(len(self._find_folders("ALL", finalmenu)), 1)

            top3_folder = self._find_folder("TOP3", finalmenu)
            top3_folder_id = top3_folder.get("id")

            top3_items = self._find_launch_items(finalmenu, parent_id=top3_folder_id)
            self.assertEquals(len(top3_items), 3)

            top3_item1 = self._find_launch_item(top3_items, launch_count=2)
            top3_item2 = self._find_launch_item(top3_items, launch_count=1)
            top3_items.remove(top3_item1)
            top3_items.remove(top3_item2)
            top3_item3 = top3_items.pop()
            self.assertEquals(len(top3_items), 0)
            self.assertTrue(finalmenu.index(top3_item1) <  finalmenu.index(top3_item2))
            self.assertTrue(finalmenu.index(top3_item2) <  finalmenu.index(top3_item3))

            self.assertTrue(len(self._find_launch_items(finalmenu, launch_id=0)) in range(2,4))
            self.assertEquals(len(self._find_launch_items(finalmenu, launch_id=1)), 3)
            self.assertEquals(len(self._find_launch_items(finalmenu, launch_id=2)), 3)
            self.assertTrue(len(self._find_launch_items(finalmenu, launch_id=3)) in range(2,4))
            self.assertEquals(len(self._find_launch_items(finalmenu, launch_id=4)), 0)

            # Admin test
            root_id, finalmenu =  menu.build_menu_using_tags(menuitems, tags, admin=True)

            self.assertFalse(self._find_folder("EMPTY", finalmenu) is None)

            top3_menu = self._find_folder("TOP3", finalmenu)
            self.assertEquals(len(top3_menu.get("child_list")), 1)
            self.assertEquals(top3_menu.get("child_list")[0].get("tag_id"), "BROWSER")


            # Multiple parents and empty folder in levels

            tags["BROWSER"]["menu_parentmenutags"].add("ALL")

            new_empty = tags["EMPTY"].copy()
            new_empty["menu_parentmenutags"] = set(["EMPTY"])
            new_empty["tag_id"] = "EMPTY1"
            tags["EMPTY1"] = new_empty

            new_empty = tags["EMPTY1"].copy()
            new_empty["tag_id"] = "EMPTY2"
            tags["EMPTY2"] = new_empty

            new_empty = tags["EMPTY"].copy()
            new_empty["tag_id"] = "EMPTY3"
            new_empty["menu_parentmenutags"] = set(["EMPTY1"])
            tags["EMPTY3"] = new_empty

            finalmenu =  menu.build_menu_using_tags(menuitems, tags)

            self.assertTrue(self._find_folder("EMPTY", finalmenu) is None)
            self.assertTrue(self._find_folder("EMPTY1", finalmenu) is None)
            self.assertTrue(self._find_folder("EMPTY2", finalmenu) is None)
            self.assertTrue(self._find_folder("EMPTY3", finalmenu) is None)
            self.assertEquals(len(self._find_folders("BROWSER", finalmenu)), 2)
            self.assertEquals(len(self._find_folders("TOP3", finalmenu)), 1)
            self.assertEquals(len(self._find_folders("ALL", finalmenu)), 1)

            self.assertTrue(len(self._find_launch_items(finalmenu, launch_id=0)) in range(3,5))
            self.assertEquals(len(self._find_launch_items(finalmenu, launch_id=1)), 4)
            self.assertEquals(len(self._find_launch_items(finalmenu, launch_id=2)), 4)
            self.assertTrue(len(self._find_launch_items(finalmenu, launch_id=3)) in range(2,4))
            self.assertEquals(len(self._find_launch_items(finalmenu, launch_id=4)), 0)


            # Admin test
            root_id, finalmenu =  menu.build_menu_using_tags(menuitems, tags, admin=True)
            self.assertFalse(self._find_folder("EMPTY", finalmenu) is None)
            self.assertFalse(self._find_folder("EMPTY1", finalmenu) is None)
            self.assertFalse(self._find_folder("EMPTY2", finalmenu) is None)
            self.assertFalse(self._find_folder("EMPTY3", finalmenu) is None)

            self.assertEquals(len(self._find_folders("BROWSER", finalmenu)), 1)
            self.assertEquals(len(self._find_folders("TOP3", finalmenu)), 1)
            self.assertEquals(len(self._find_folders("ALL", finalmenu)), 1)


#
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'pwl'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
