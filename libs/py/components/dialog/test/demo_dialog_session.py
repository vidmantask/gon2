"""
Demo of dialog session
"""
import time
import unittest
import sys

from lib import giri_unittest

import components.communication.async_service
async_servcice_handler = components.communication.async_service.APIAsyncSession_create(5)

from components.communication import sim
import components.presentation.user_interface as gui

import components.environment
import components.dialog.server_gateway.dialog_session
import components.dialog.client_gateway.dialog_session

import components.traffic.server_gateway.traffic_session as traffic_session_server 
from components.communication.sim import sim_tunnel_endpoint

        
import components.communication.async_service
async_servcice_handler = components.communication.async_service.APIAsyncSession_create(5)

import lib.checkpoint
checkpoint_handler_null = lib.checkpoint.CheckpointHandler.get_NULL()
checkpoint_handler_show = lib.checkpoint.CheckpointHandler.get_cout_all()
component_env = components.environment.Environment(checkpoint_handler_show, 'sqlite:///../../../py/setup/demo/gon_server_db.sqlite'  , 'demo_dialog_session')


import components.dialog.server_common.database_schema
components.dialog.server_common.database_schema.connect_to_database(component_env)


class MyTrafficSession(traffic_session_server.ITrafficSessionCallback):
    def traffic_update_ids(self, action_ids, deactivated_actions):
        print "MyTrafficSession:traffic_update_ids, action_ids:", action_ids

    def traffic_launch(self, launch_id):
        print "MyTrafficSession:traffic_launch, launch_id:", launch_id

def test_menu_click(user_interface, llist):
    
    
    for id in llist:
        time.sleep(1)
        user_interface.menu.add_to_launch_list(id)


class DialogSessionTest(unittest.TestCase):



    def test_gui_session(self):
        sim_tunnel_endpoint_connector = sim_tunnel_endpoint.SimThread(checkpoint_handler_null, async_servcice_handler)
        sim_tunnel_endpoint_connector.start_and_wait_until_ready()
            
        tunnel_endpoint_client = sim_tunnel_endpoint_connector.get_tunnelendpoint_client()
        tunnel_endpoint_server = sim_tunnel_endpoint_connector.get_tunnelendpoint_server()
        
        dialog_session_server = components.dialog.server_gateway.dialog_session.DialogSession(component_env, checkpoint_handler_show, tunnel_endpoint_server, None, None, None, overwrite_user_id = 5) 
        dialog_session_server.set_traffic_callback(MyTrafficSession())

        user_interface = gui.UserInterface(view_type=gui.UserInterface.VIEW_TYPE_SIMULATOR)
        dialog_session_client = components.dialog.client_gateway.dialog_session.DialogSession(component_env, tunnel_endpoint_client, user_interface, None)

        #demo_data_stat_data = {'user' : 'TC', 'os':'UBUNTU'}

        action_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44]
        
        # Example structure:
        #launch_id_tags = {}
        #launch_id_tags.update({ 1 : set(['ALL','WINDOWS', 'BROWSER', 'SHOW', 'ENABLED'])})
        #launch_id_tags.update({ 2 : set(['ALL','WINDOWS', 'BROWSER', 'DEVELOPER', 'ENABLED'])})
        #launch_id_tags.update({25 : set(['ALL','LINUX', 'OUTLOOK', 'MAIL','BROWSER', 'CLIENTOK', 'SERVEROK'])})
        #launch_id_tags.update({26 : set(['ALL','LINUX', 'RDP', 'SHOW', 'ENABLED'])})
        
        
        
        #dialog_session_server.dialog_update_ids(action_ids, stat_keys = demo_data_stat_data)
        dialog_session_server.load_tags_for_launch_items(action_ids, -1)
        for k in dialog_session_server.launch_id_tags:
            dialog_session_server.launch_id_tags[k].add('CLIENTOK')
        dialog_session_server.dialog_update_ids(action_ids) 
        user_interface.start()

        action_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44]
        dialog_session_server.load_tags_for_launch_items(action_ids, -1)

        for k in dialog_session_server.launch_id_tags:
            dialog_session_server.launch_id_tags[k].add('CLIENTOK')
            dialog_session_server.launch_id_tags[k].add('TOPX')
        dialog_session_server.dialog_update_ids(action_ids) 

        test_menu_click(user_interface, action_ids)

        sim_tunnel_endpoint_connector.stop()
        sim_tunnel_endpoint_connector.join()


if __name__ == '__main__':
    gut_py = giri_unittest.CMakeUnittest()
    
    if gut_py.do_run():
        unittest.main()
