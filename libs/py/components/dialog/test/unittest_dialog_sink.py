"""
Unittest of dialog admimessage sink component
"""
from __future__ import with_statement
import os
import unittest

from lib import giri_unittest
import components.database
import components.database.server_common.connection_factory

import lib.checkpoint

import components.management_message.server_common.database_schema

import components.environment

import components.dialog.server_common.database_schema
import components.dialog.server_management.dialog
import components.dialog.server_gateway.dialog

import components.dialog.server_common.dialog_api  
import components.dialog.server_management.dialog_api

from components.database.server_common import schema_api
from components.database.server_common import database_api

import components.plugin.server_management.manager



class DialogTagUpdateTest(unittest.TestCase):
   
    def setUp(self):
        self.temp_folder = giri_unittest.mkdtemp()
        self.db_filename = os.path.join(self.temp_folder, 'temp_sqllite.db')

    def test(self):
        environment = components.environment.Environment(giri_unittest.get_checkpoint_handler(), 'sqlite:///%s' % self.db_filename, 'management_message_unittest')
        components.management_message.server_common.database_schema.connect_to_database(environment)
        components.dialog.server_common.database_schema.connect_to_database(environment)

        license_handler = None
        environment.plugin_manager = components.plugin.server_management.manager.Manager(giri_unittest.get_checkpoint_handler(), environment.get_default_database(), license_handler, giri_unittest.PY_ROOT)

        #
        # Clearing database
        #
        tagstr = 'a,b, c, test'
        with database_api.Transaction() as t:
            components.dialog.server_management.dialog_api.dialog_update_user_launch_item_tags(t, 5, 1, ('TEST5_1a','TEST5_1c'))
            components.dialog.server_management.dialog_api.dialog_update_user_launch_item_tags(t, 5, 2, ('TEST5_2a','TEST5_2C', 'TEST5_2F','AUTOLAUNCH'))
            components.dialog.server_management.dialog_api.dialog_update_user_launch_item_tags(t, 5, 3, ('TEST5_3a','TEST5_3c'))

            components.dialog.server_management.dialog_api.update_tag_string(t, -5, tagstr)

            components.dialog.server_management.dialog_api.dialog_update_user_launch_item_tags(t, 5, 9, ('TEST5_3234a','TEST5_31234c'))
            components.dialog.server_management.dialog_api.delete_dialog_element(t, 9)
            
        
            tagset = set(tagstr.replace(' ', '').upper().split(','))
            self.assertEqual(components.dialog.server_common.dialog_api.get_tag_set(t, -5), tagset)

        
        with database_api.Transaction() as t:
            # test tag property save/load
            
            # save some tag data
            ## All
            components.dialog.server_management.dialog_api.save_tag_values(t, 'ALL', -1, menu_show=True, menu_caption='All Programs')
            components.dialog.server_management.dialog_api.save_tag_values(t, 'TEST', -1, menu_show=False)
            components.dialog.server_management.dialog_api.save_tag_values(t, 'AUTOLAUNCH', -1, menu_show=False, item_autolaunch_once=True)
            ## User 2
            components.dialog.server_management.dialog_api.save_tag_values(t, 'TEST', 2, menu_show=True, menu_caption='Show test tag menu')

            # load tag data
            tag_props = components.dialog.server_common.dialog_api.read_tag_properties_from_db(t)
            self.assertEqual(tag_props['ALL']['menu_caption'], 'All Programs')
            self.assertEqual(tag_props['TEST']['menu_show'], False)
            
            # Add user tag values
            tag_props.update(components.dialog.server_common.dialog_api.read_tag_properties_from_db(t, 2))

            self.assertEqual(tag_props['ALL']['menu_caption'], 'All Programs')
            self.assertEqual(tag_props['TEST']['menu_show'], True)
            
            #print str(tag_props) 
        

#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'cso'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
