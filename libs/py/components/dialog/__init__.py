"""
One of the famous 3.
"Model" and "controller" for dialogue with user, on Gateway Server showing which launch_ids are available, lets user choose launch_id, lets traffic_launch effectuate. 
The actual visual GUI "view" is in presentation component.

Includes
- presentation customization
- favourites
- about
- help
- alerts
TODO: Management part for this module
"""

component_id = 'component_dialog'