"""
This module contains functionality for handling information policies (Gateway Client side)
"""
from __future__ import with_statement
from components.communication import tunnel_endpoint_base


class DialogInformer(tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch):
    def __init__(self, async_service, checkpoint_handler, dialog_informer_tunnel_endpoint, user_interface, cb_close):
        tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch.__init__(self, async_service, checkpoint_handler, dialog_informer_tunnel_endpoint)
        self.user_interface = user_interface
        self.dictionary = self.user_interface.dictionary
        self.cb_close = cb_close
        self._waiting_for_gui = False
        self._close_on_cancel = False
        self.first_access_finish = False
    
    def is_first_access_finish(self):
        return self.first_access_finish
    
    def remote_inform_on_first_access(self, message, close_on_cancel):
        self._close_on_cancel = close_on_cancel
        self.user_interface.modalmessage.subscribe(self.gui_on_update_callback)
        self.user_interface.modalmessage.set_cancel_label()
        self.user_interface.modalmessage.set_next_label()
        self.user_interface.modalmessage.set_text(message)
        self.user_interface.modalmessage.set_next_allowed(True)
        if self._close_on_cancel:
            self.user_interface.modalmessage.set_cancel_allowed(True)
        else:
            self.user_interface.modalmessage.set_cancel_allowed(False)
        self._waiting_for_gui = True
        self.user_interface.modalmessage.set_size(self.user_interface.modalmessage.ID_LARGE_SIZE)
        self.user_interface.modalmessage.view.display()
        
        # An issue on windows where text not is shown if set_text() is called before display() 
        self.user_interface.modalmessage.set_text(message)

    def remote_show_license_info(self, valid, header, message):
        self.first_access_finish = True
        self.user_interface.message.set_message(header, message)
        self.user_interface.message.display()
        
    def remote_show_menu_updated(self):
        self.first_access_finish = True
        self.user_interface.message.set_message(self.dictionary._("G/On Client"), self.dictionary._("Your menu has been updated"))
        self.user_interface.message.display()
        
    def gui_on_update_callback(self):
        if self._waiting_for_gui:
            if self.user_interface.modalmessage.get_next_clicked():
                self._waiting_for_gui = False
                self.user_interface.modalmessage.set_next_clicked(False)
                self.tunnelendpoint_remote('remote_welcome_message_response')
                self.user_interface.modalmessage.unsubscribe(self.gui_on_update_callback)
                self.user_interface.modalmessage.view.hide()
            elif self.user_interface.modalmessage.get_cancel_clicked():
                self._waiting_for_gui = False
                self.user_interface.modalmessage.set_cancel_clicked(False)
                self.user_interface.modalmessage.unsubscribe(self.gui_on_update_callback)
                self.user_interface.modalmessage.view.hide()
                if self._close_on_cancel:
                    self.cb_close()
                else:
                    self.tunnelendpoint_remote('remote_welcome_message_response')
        