"""
This file constains the client-side of a dialog_session
"""
from __future__ import with_statement

from lib import checkpoint 
from components.communication import tunnel_endpoint_base
from operator import itemgetter

import datetime

import components.dialog.common.dialog_menu_lib as dialog_menu_lib

from lib.commongon import  *

import components.dialog.client_gateway.dialog_informer
            
    

class DialogSession(tunnel_endpoint_base.TunnelendpointSession):
    """
    tbd
    """

    latest_menu_from_server = {}
    topxCount = 3
    topxToRoot = True
    
    last_tag_properties = {}
    already_launched_ids = []

    
    def __init__(self, async_service, checkpoint_handler, tunnel_endpoint_tunnel, user_interface, is_first_start, cb_close):
        tunnel_endpoint_base.TunnelendpointSession.__init__(self, async_service, checkpoint_handler, tunnel_endpoint_tunnel)
        self.user_interface = user_interface
        self.dictionary = self.user_interface.dictionary
        self.user_interface.menu.subscribe(self.on_menu_item_selected)
        self.is_first_start = is_first_start
        self.cb_close = cb_close

        self.dialog_informer_tunnel_endpoint = self.create_and_add_tunnelendpoint_tunnel(self.checkpoint_handler, 1)
        self.dialog_informer = components.dialog.client_gateway.dialog_informer.DialogInformer(async_service, self.checkpoint_handler, self.dialog_informer_tunnel_endpoint, user_interface, self.cb_close)
    

    def session_start_and_connected(self):
        """
        This is called when tunnelendpoint_connected and session_start both has been called
        """ 
        pass

    def session_close(self):
        """Hard close of session. Called from main session when communication is terminated"""
        with self.checkpoint_handler.CheckpointScope("session_close", "dialog_session", checkpoint.DEBUG):
            pass

    def is_first_access_finish(self):
        """
        Detect if the first_start run of the client is over 
        """
        return self.dialog_informer.is_first_access_finish()

    def update_gui_menu(self, menu):
        with self.checkpoint_handler.CheckpointScope("server_cb_update_gui_menu", "dialogs_session", checkpoint.DEBUG):
            
            self.user_interface.menu.clear()
            for element in menu:
                #with self.checkpoint_handler.CheckpointScope("server_cb_update_gui_menu", "Menu Item="+element['title'], checkpoint.DEBUG):
                #    pass
    
                id, parentid = element['id'], element['parent_id']
                title, icon = element['title'], element['icon_id']
                type = element['type']
                
                if type == 'folder':
                    self.user_interface.menu.add_folder(parentid, id, title, icon)
                elif type == 'item':
                    enabled = element['enabled']
                    launch_id = element['launch_id']
                    restricted = element['disabled']
                    restriction_text = element['tooltip']
                    self.user_interface.menu.add_item(parentid, id, title, icon, enabled, launch_id, restricted, restriction_text)
    
    def update_client_menu(self, id):
        if self.latest_menu_from_server != {}:
            # update the used menu item
            for element in self.latest_menu_from_server:
                if element['id'] == id:
                    with self.checkpoint_handler.CheckpointScope("Updating menu item count/date", "dialog_session", checkpoint.DEBUG, id=id):
                        if element.has_key('launch_count'):
                            element['launch_count'] = element['launch_count'] + 1
                        else:
                            element['launch_count'] = 1
                        element['launch_date'] = datetime2str(datetime.now())
            
            menu = self.latest_menu_from_server[:]
            
            menu = self.build_menu_using_tags(menu)
            #self.find_and_add_most_used_to_menu(menu, self.topxCount, self.topxToRoot)
            #update the
            self.update_gui_menu(menu)
        
    # old function that isn't really relevant any more. use the tag system
    def find_and_add_most_used_to_menu(self, menu, count = 3, toRoot = True):
        
        root = None
        
       
        self.checkpoint_handler.Checkpoint("Adding top X menu items", "dialog_session", checkpoint.DEBUG, count=count, toRoot=toRoot)

        max_id = 0   
        # ensure that there aren't any empty launch_counts (sorted fails)
        for element in menu:
            if not element.has_key('launch_count'):
                element['launch_count'] = 0
            if element['id']>max_id:
                max_id = element['id'] + 1
        
        sortedmenu = sorted(menu, key=itemgetter('launch_count'), reverse = True)
        
        parent_id = -1
        
        # if not "to root" we want to add a root menu item that can be parent
        if not toRoot:
            newe = {}
            newe['id']= max_id
            parent_id = max_id
            max_id = max_id + 1
            newe['parent_id'] = -1
            newe['type'] = 'folder'
            newe['title'] = 'Top 3 menu actions'
            newe['icon_id'] = -1
            menu.append(newe)
             
        
        added = 0
        hasdevider = False
        
        for element in sortedmenu:
            if element['launch_count'] > 0:
                if not hasdevider and parent_id==-1: # add divider if we are in root
                    newe = {}
                    newe['id']=-1
                    newe['parent_id'] = -1
                    newe['type'] = 'divider'
                    newe['title'] = '------------'
                    newe['icon_id'] = -1
                    menu.append(newe)
                    hasdevider = True
                newe = element.copy()
                newe['launch_count'] = 0 # ensure that it isn't copied again
                newe['parent_id'] = parent_id
                menu.append(newe)
            else:
                break
            added = added + 1
            if added>=count:
                break
    

    def build_menu_using_tags(self, menuitems, tag_properties = None):
        
        
        if tag_properties == None:
            if self.last_tag_properties != {}:
                tag_properties = self.last_tag_properties.copy()
        else:
            self.last_tag_properties = tag_properties.copy()
        
        menu  = dialog_menu_lib.dialog_menu(self.checkpoint_handler, self.dictionary)
        
        return menu.build_menu_using_tags(menuitems, tag_properties)
        
        


    def on_menu_item_selected(self):
        """ When the user has selected a menu item this should be run. """
        launchlist = self.user_interface.menu.get_launch_list()
        if launchlist != []:
            with self.checkpoint_handler.CheckpointScope("on_menu_item_selected", "dialog_session", checkpoint.DEBUG, launchlist=str(launchlist)):
                for launch_id in launchlist:
                    #launch_id = self.menuid2launchid[id]
                    self.tunnelendpoint_remote('client_cb_menu_item_selected', launch_id=launch_id)
                    self.user_interface.menu.remove_from_launch_list(launch_id)
                    #self.update_client_menu(id=int(id))

    def autolaunch_menu_items(self, menu, tag_properties):
        """ Launch items tagged with an auto launch tag.
        
        """
        for menuitem in menu:
            launch_id = menuitem['launch_id']
            if not launch_id in self.already_launched_ids:
                auto_launch = False
                enabled = False
                for tag in menuitem['tags']:
                    tagvalues = tag_properties.get(tag, dict())
                    if tagvalues.get('auto_launch', False):
                        auto_launch = True
                        if enabled:
                            break
                    if self.is_first_start and tagvalues.get('auto_launch_first', False):
                        auto_launch = True
                        if enabled:
                            break
                    if tagvalues.get('item_enabled', False):
                        enabled = True
                        if auto_launch:
                            break
                if auto_launch and enabled:
                    self.tunnelendpoint_remote('client_cb_menu_item_selected', launch_id=launch_id, auto_launch=True)
                self.already_launched_ids.append(launch_id)

    def server_cb_show_menu(self, menu, tag_properties):
        """ Updates the menu that is displayed on the client side. 

            Menu elements are updated id they already exist
            in the menu. Added if don't already exist. Removed
            if they no longer exist in the new menu.
            
            Note:
            For now it is not possible to move a menu element
            to a new position. For instance from one folder to
            another.
        """
        with self.checkpoint_handler.CheckpointScope("server_cb_show_menu", "dialog_session", checkpoint.DEBUG) as cp:

            self.latest_menu_from_server = menu[:]
            
            if (tag_properties == None) or (tag_properties == {}):
                self.find_and_add_most_used_to_menu(menu, self.topxCount, self.topxToRoot)
            else:
                # we look for auto launch first, as we also want to launch items that will not get shown
                self.autolaunch_menu_items(menu, tag_properties)
                menu = self.build_menu_using_tags(menu, tag_properties)
            self.update_gui_menu(menu)
           
    
    def server_cb_launch_cancled_missing_package(self, package_id_missing):
        """
        Callback from server that notifiy that a launch has been canceled because the package was missing
        """
        self.user_interface.message.set_message(self.dictionary._("G/On Client"), self.dictionary._("The launch has been cancelled because the package '%s' is not installed.") % package_id_missing)
        self.user_interface.message.display()


    def remote_image_found(self, request_id, image_id, image_data):
        """
        Callback from server as a response to remote_get_images indicating that an image has been found
        """
        pass
    
    def remote_image_not_found(self, request_id, image_id):
        """
        Callback from server as a response to remote_get_images indicating that no image is found matching request
        """
        pass
    