# -*- coding: iso-8859-1 -*-
"""
Plugin for Management Server
"""
from __future__ import with_statement

import sys
import os.path
import ConfigParser

import smtplib
import socket
import email.mime.text

import lib.checkpoint

from components.database.server_common import schema_api
from components.database.server_common import database_api

from plugin_modules import local_users
from plugin_modules.local_users.server_common import database_schema

from plugin_types.server_management import plugin_type_element
from plugin_types.server_management import plugin_type_config
from plugin_types.server_management import plugin_type_access_notification
from plugin_types.server_management import plugin_type_user 


class UserElement(plugin_type_user.User):
    
    def __init__(self, user):
        plugin_type_user.User.__init__(self, local_users.plugin_name)
        self.id = user.id
        self.login_name = user.short_name
        self.fullname = user.long_name
        
        

class GroupElement(plugin_type_user.Group):
    
    def __init__(self, group):
        plugin_type_user.Group.__init__(self, local_users.plugin_name)
        self.id = group.id
        self.name = self.group.short_name
        self.long_name = group.long_name
        
        


class PluginAuthentication(plugin_type_user.PluginTypeUser):
    
    def __init__(self, checkpoint_handler, license_handler, database):
        plugin_type_user.PluginTypeUser.__init__(self, checkpoint_handler, database, license_handler, local_users.plugin_name)
        self._users = None
        self._groups = None

        self.create_test_users()


    def fetch_users(self):
        if self._users is None:
            self._users = []
            with database_api.ReadonlySession() as db_session:
                users = db_session.select(database_schema.User)
                for user in users:
                    self._users.append(UserElement(user))
                
        return self._users


    def fetch_groups(self):
        if self._groups is None:
            self._groups = []
            with database_api.ReadonlySession() as db_session:
                groups = db_session.select(database_schema.Group)
                for group in groups:
                    self._groups.append(GroupElement(group))
                
        return self._groups

    def get_edit_columns(self, element_type):
        columns = []
        columns.append(plugin_type_config.Column(self.plugin_name, 'user_login',   plugin_type_config.Column.TYPE_STRING, 'Login'))
        columns.append(plugin_type_config.Column(self.plugin_name, 'user_fullname',   plugin_type_config.Column.TYPE_STRING, 'Full name'))
        columns.append(plugin_type_config.Column(self.plugin_name, 'my_pc_1',    plugin_type_config.Column.TYPE_STRING, 'My-PC 1'))
        columns.append(plugin_type_config.Column(self.plugin_name, 'my_pc_2',    plugin_type_config.Column.TYPE_STRING, 'My-PC 2'))
        return columns


    def add_config_values(self, element, value_dict):
        with database_api.ReadonlySession() as dbs:
            user = self._lookup_user(dbs, element.get_id())
            if user != None:
                value_dict["id"] = user.id
                value_dict["user_login"] = user.short_name
                value_dict["user_fullname"] = user.long_name
                value_dict["my_pc_1"] = user.my_pc_1
                value_dict["my_pc_2"] = user.my_pc_2
    
    def update_row(self, element_type, value_dict):
        with database_api.Transaction() as dbt:
            user = self._lookup_user(dbt, value_dict.get("id"))
            if user != None:
                user.short_name = value_dict["user_login"]
                user.long_name =value_dict["user_fullname"]
                user.my_pc_1 = value_dict["my_pc_1"]
                user.my_pc_2 = value_dict["my_pc_2"]

    def create_row(self, element_type, value_dict):
        pass
    
    def delete_row(self, element):
        with database_api.Transaction() as dbs:
            user = self._lookup_user(dbs, element.get_id())
            if user:
                dbs.delete(user)
        
    


    ''' 

    
    
    def config_create_row(self, row):
        with database_api.Transaction() as dbt:
            user = self._lookup_user(dbt, row[0])
            if user == None:
                dbt.add(database_schema.User(user_sid=row[0], my_pc_1=row[1], my_pc_2=row[2]))
        return row
    
    def config_update_row(self, row):
        with database_api.Transaction() as dbt:
            user = self._lookup_user(dbt, row[0])
            if user != None:
                user.my_pc_1 = row[1]
                user.my_pc_2 = row[2]
                return row
            return None
    
    def config_delete_row(self, row):
        with database_api.Transaction() as dbt:
            user = self._lookup_user(dbt, row[0])
            if user != None:
                dbt.delete(user)
                return True
            return False

    '''
    def _lookup_user(self, dbs, user_id):
        return dbs.get(database_schema.User, user_id)
        
    
    test_users = 0

    def create_test_users(self):
        with database_api.Transaction() as dbt:
            count = dbt.select_count(database_schema.User)
            if count < self.test_users:
                users_to_create = self.test_users - count
                first_index = count
                for i in range(users_to_create):
                    user = dbt.add(database_schema.User())
                    user.short_name = user.long_name = "TestUser%s" % (count+i)
            else:
                for i in range(self.test_users, count):
                    user = dbt.select_first(database_schema.User, filter=database_schema.User.short_name=="TestUser%s" % (i))
                    if user:
                        dbt.delete(user)
                        
        
    
    