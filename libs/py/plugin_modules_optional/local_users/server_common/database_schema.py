"""
This file contains the database schema for the local_users plugin
"""
from __future__ import with_statement
from components.database.server_common.database_api import *

dbapi = SchemaFactory.get_creator("plugin_local_users")


def connect_to_database_using_connection(db_connection):
    dbapi.bind(db_connection)


table_member_object = dbapi.create_table("member_object", 
                                         Column('short_name', String(10), nullable=False),
                                         Column('long_name', String(256), default=""),
                                         Column('type', String(30), nullable=False)
                                        )

table_user = dbapi.create_table("user", 
                             Column('password', String(256)),
                             Column('email', String(512)),
                             Column('my_pc_1', String(1000)),
                             Column('my_pc_2', String(1000)),
                             inherit_from = table_member_object
                            )

table_group = dbapi.create_table("group", 
                             inherit_from = table_member_object
                             )


table_group_membership = dbapi.create_table("group_membership", 
                     Column('parent_id', Integer),
                     Column('child_id', Integer),
                    )

table_object_detail = dbapi.create_table("object_detail", 
                     Column('object_id', Integer),
                     Column('property_name', String(64), nullable=False),
                     Column('property_value', String(1024)),
                    )

dbapi.add_foreign_key_constraint(table_group_membership, 'parent_id', table_group)
dbapi.add_foreign_key_constraint(table_group_membership, 'child_id', table_member_object)
dbapi.add_foreign_key_constraint(table_object_detail, 'object_id', table_member_object)

#SchemaFactory.register_creator(dbapi)


class MemberObject(PersistentObject):
    
    def __getattr__(self, arg):
        if self.details:
            for detail in self.details:
                if detail.property_name==arg:
                    return detail.property_value
        raise AttributeError("Object has no attribute '%s'" % arg)
    

class User(MemberObject):
    pass

class Group(MemberObject):

    def add_object(self, obj):
        self.members.append(obj)
        
    def remove_object(self, obj):
        try:
            self.members.remove(obj)
        except ValueError:
            pass

class ObjectDetail(object):
    pass

mapper(ObjectDetail, table_object_detail)

mapper(MemberObject, 
             table_member_object, 
             polymorphic_on = 
             table_member_object.c.type, 
             polymorphic_identity = 'Object',
             relations=[Relation(ObjectDetail, "details")]
             )

mapper(Group,
             table_group, 
             inherits=MemberObject,
             polymorphic_identity = 'Group', 
             relations = [Relation(MemberObject,'members', intermediate_table=table_group_membership, backref_property_name="memberof")]
             )

mapper(User, 
             table_user, 
             inherits=MemberObject,
             polymorphic_identity = 'User'
             )


def _get_group_memberships(group, groups):
    groups.append(group.id)
    for g in group.memberof:
        if not g.id in groups:
            _get_group_memberships(g, groups)
    

def get_group_memberships(obj):
    groups = []
    for group in obj.memberof:
        _get_group_memberships(group, groups)
    

def find_user(name, db_session):
    return db_session.select_first(User, filter=(User.short_name==name))
    
    
    

