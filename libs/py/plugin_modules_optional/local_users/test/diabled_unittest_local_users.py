"""
Unittest of local_users plugin module
"""
from __future__ import with_statement
import os
import unittest
from lib import giri_unittest
from lib import checkpoint

if False:
    from plugin_modules.local_users.server_common import database_schema
 
from components.database.server_common.connection_factory import ConnectionFactory

GIRITECH_UNITTEST_IGNORE = True


class Database(unittest.TestCase):

    connection = ConnectionFactory(None).create_connection('sqlite:///:memory:', True)

    def setUp(self):
        pass
        
    def test_schema(self):
        database_schema.connect_to_database_using_connection(self.connection)



        
if __name__ == '__main__':
    gut_py = giri_unittest.CMakeUnittest()
   
    if gut_py.do_run():
        unittest.main()    
