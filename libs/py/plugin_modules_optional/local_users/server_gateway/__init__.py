"""
Local users plugin for Gateway Server
"""
from __future__ import with_statement
import sys
import os.path
import ConfigParser

import lib.checkpoint


from components.communication import message

from components.database.server_common import schema_api
from components.database.server_common import database_api

from plugin_modules import local_users
from plugin_modules.local_users.server_common import database_schema
from plugin_types.server_gateway import plugin_type_auth
from plugin_types.server_gateway import plugin_type_traffic
from plugin_types.server_gateway import plugin_type_user





class PluginAuthentication(plugin_type_auth.PluginTypeAuth, plugin_type_traffic.PluginTypeTraffic, plugin_type_user.PluginTypeUser):
    """
    Server part of local_users module, plugging into server
    """
    def __init__(self, checkpoint_handler, database, management_message_session, access_log_server_session):
        plugin_type_auth.PluginTypeAuth.__init__(self, checkpoint_handler, local_users.plugin_name, database, management_message_session, access_log_server_session)
        plugin_type_traffic.PluginTypeTraffic.__init__(self, checkpoint_handler, local_users.plugin_name)
        plugin_type_user.PluginTypeUser.__init__(self, checkpoint_handler, local_users.plugin_name)
        self.user = None
        self.group_ids = None
        self.email = None

    def start(self):
        """
        Initiates the login process
        """
        with self.checkpoint_handler.CheckpointScope("PluginAuthentication::start", self.plugin_name, lib.checkpoint.DEBUG) as cps:
            self.request_login()
            self.set_started()


    def is_logged_in_name(self, login):
        """
        True if the user is logged in as login
        """
        user_id = login[0]
        user_name = login[1]
        if self.authenticated:
            if user_id:
                return self.user.id == int(user_id)
        return False

    def is_logged_in_member(self, group):
        """
        True if the user is logged in as member of group
        """
        group_id = group[0]
        if self.authenticated:
            if group_id:
                if self.group_ids is None:
                    self.group_ids = database_schema.get_group_memberships(user)
                return int(group_id) in self.group_ids 
        return False

    def is_authenticated(self):
        return self.authenticated
    
    def login_cancelled(self):
        self.authenticated = False
        self.set_ready()


    def receive_login(self, login, password):
        """
        Recieves result from login and password prompt and continues the authentication proces
        """
        with self.checkpoint_handler.CheckpointScope("PluginAuthentication::receive_login", self.plugin_name, lib.checkpoint.DEBUG, login=login):
            with database_api.ReadonlySession() as dbs:
                db_user = database_schema.find_user(login, dbs)
                if db_user:
                    #self.checkpoint_handler.Checkpoint("PluginAuthentication::receive_login.entered", self.plugin_name, lib.checkpoint.DEBUG, login=login, user_sid=self.user_SID, user_email=self.email)
                    self.authenticated = True
                    self.user = db_user
        self.set_ready()

    def get_current_user_id(self):
        if self.authenticated and self.user:
            return str(self.user.id)
        else:
            return None
                
    def get_attribute(self, attribute_name):
        if self.authenticated and self.user:
            if attribute_name == 'login':
                return self.user.shortname
            elif attribute_name == 'password':
                return self.user.password
            elif attribute_name == 'my_pc_1':
                return self.user.my_pc_1
            elif attribute_name == 'my_pc_2':
                return self.user.my_pc_2
        return None
    
    def _get_my_info(self):
        with database_api.ReadonlySession() as dbs:
            user = self._lookup_user(dbs, self.user_SID)
            if user != None:
                self.my_pc_1 = user.my_pc_1
                self.my_pc_2 = user.my_pc_2
    
    def _lookup_user(self, dbs, user_sid):
        user = dbs.select(database_schema.User, database_api.and_(database_schema.User.user_sid==user_sid))
        if len(user) == 0:
            return None
        return user[0]
