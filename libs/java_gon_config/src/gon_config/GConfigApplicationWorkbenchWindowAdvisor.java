package gon_config;

import gon_config.model.GConfigModelFactory;

import org.eclipse.jface.preference.PreferenceManager;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;
import org.eclipse.ui.intro.IIntroManager;

public class GConfigApplicationWorkbenchWindowAdvisor extends WorkbenchWindowAdvisor {

	public GConfigApplicationWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
		super(configurer);
	}

	public ActionBarAdvisor createActionBarAdvisor(
			IActionBarConfigurer configurer) {
		return new GConfigApplicationActionBarAdvisor(configurer);
	}

	public void preWindowOpen() {
		IWorkbenchWindowConfigurer configurer = getWindowConfigurer();
		configurer.setInitialSize(new Point(750, 650));
		configurer.setShowCoolBar(false);
		configurer.setShowStatusLine(true);
		configurer.setShowProgressIndicator(true);
		
	}

	@Override
	public void openIntro() {
		try {
			boolean configured = GConfigModelFactory.getConfigModel().getConfiguredStatus();
			if (! configured) {
				final IIntroManager mgr = PlatformUI.getWorkbench().getIntroManager();
				mgr.showIntro(getWindowConfigurer().getWindow(), false);
			}
		}
		catch(Throwable t) {
			t.printStackTrace();
			
		}
	}

	@Override
	public void postWindowOpen(){ 
		// Hide spurious search menu item	
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		page.hideActionSet("org.eclipse.search.searchActionSet");
	  
		// Hide spurious preference items	
		PreferenceManager pm = PlatformUI.getWorkbench( ).getPreferenceManager();
		pm.remove("org.eclipse.ui.preferencePages.Workbench");
		pm.remove("org.eclipse.birt.report.designer.ui.preferences");
		pm.remove("org.eclipse.jdt.ui.preferences.JavaBasePreferencePage");
		pm.remove("org.eclipse.team.ui.TeamPreferences");
		pm.remove("org.eclipse.debug.ui.DebugPreferencePage");
	  
//		IPreferenceNode[] arr = pm.getRootSubNodes();
//		        
//		for(IPreferenceNode pn:arr){
//		    System.out.println("Label:" + pn.getLabelText() + " ID:" + pn.getId());
//		}		
	  
	}	

}
