package gon_config;

import java.util.ArrayList;

import gon_client_management.ext.CommonUtils;
import gon_client_management.ext.GILogger;

import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class GConfigActivator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "gon_config";

	// The shared instance
	private static GConfigActivator plugin;

	public interface GIStopCallback {
		public void stop();
	}
	
	private ArrayList<GIStopCallback> stopCallbacks = new ArrayList<GIStopCallback>(); 
	
	/**
	 * The constructor
	 */
	public GConfigActivator() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		super.stop(context);
		plugin = null;
		for (GIStopCallback callback : stopCallbacks) {
			callback.stop();
		}
		
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static GConfigActivator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}

	public static ILog getClientLog() {
		plugin.getBundle().getSymbolicName();
		return plugin.getLog();
	}
	
	public static String getPluginId() {
		return plugin.getBundle().getSymbolicName();
	}
	
	private static GLogger logger = null;

	public static GILogger getLogger() {
		if (logger==null) {
			logger = new GLogger();
		}
		return logger;
	}
	
	private static class GLogger implements GILogger {

		/* (non-Javadoc)
		 * @see gon_client_management.GILogger#log(int, java.lang.String)
		 */
		public void log(int severity, String message) {
			Status msg = new Status(severity, getPluginId(), message);
			plugin.getLog().log(msg);
		}
		
		/* (non-Javadoc)
		 * @see gon_client_management.GILogger#logError(java.lang.String)
		 */
		public void logError(String message) {
			log(IStatus.ERROR, message);
		}
	
	
		/* (non-Javadoc)
		 * @see gon_client_management.GILogger#logInfo(java.lang.String)
		 */
		public void logInfo(String message) {
			log(IStatus.INFO, message);
		}
		
		/* (non-Javadoc)
		 * @see gon_client_management.GILogger#logException(java.lang.Throwable)
		 */
		public void logException(Throwable t) {
			logError(CommonUtils.getStackTrace(t));
		}
	}

	public void addStopCallback(GIStopCallback stopCallback) {
		stopCallbacks.add(stopCallback);
	}

	
}
