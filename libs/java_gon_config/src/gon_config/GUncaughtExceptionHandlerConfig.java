package gon_config;

import java.lang.Thread.UncaughtExceptionHandler;

import org.eclipse.jface.dialogs.MessageDialog;

public class GUncaughtExceptionHandlerConfig implements UncaughtExceptionHandler {

	
	public GUncaughtExceptionHandlerConfig(String pluginId) {
	}
	
	public static void handleException(Throwable e) {
		GConfigActivator.getLogger().logException(e);
		String localizedMessage = e.getLocalizedMessage();
		if (localizedMessage==null)
			localizedMessage = e.toString();
		MessageDialog.openError(null, "Error", localizedMessage);	
	}

	public void uncaughtException(Thread t, Throwable e) {
		handleException(e);
	}

}
