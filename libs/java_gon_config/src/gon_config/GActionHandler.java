package gon_config;

import gon_client_management.view.ext.GJobHandler;
import gon_config.install_wizard.GChangeWizard;
import gon_config.install_wizard.GCreateGPMWizardPage;
import gon_config.install_wizard.GSinglePageWizard;
import gon_config.install_wizard.InstallWizardDialog;
import gon_config.model.GConfigModelFactory;
import gon_config.service.GLocalServiceJob;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.IHandlerListener;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;

public class GActionHandler implements IHandler {

	@Override
	public void addHandlerListener(IHandlerListener handlerListener) {
	}

	@Override
	public void dispose() {
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		String commandId = event.getCommand().getId();

		if (commandId.equals("SUPPORT_PACK_GON_CONFIG")) {
			final Shell shell = HandlerUtil.getActiveWorkbenchWindow(event).getShell();
			final GLocalServiceJob localServiceJob =  GConfigModelFactory.getConfigModel().getGenerateSupportPackageJob();
			final GJobHandler job = new GJobHandler("Generating Support Package", "2", shell.getDisplay(), localServiceJob, GConfigActivator.getLogger());
			job.setUser(true);
			job.schedule();
		}
		else if (commandId.equals("GENERATE_GPMS_GON_CONFIG")) {
			final Shell shell = HandlerUtil.getActiveWorkbenchWindow(event).getShell();
			WizardDialog dialog = new WizardDialog(shell, new GSinglePageWizard(new GCreateGPMWizardPage()));
			dialog.open();
		}
		else if (commandId.equals("CHANGE_GON_CONFIG")) {
			final Shell shell = HandlerUtil.getActiveWorkbenchWindow(event).getShell();
			WizardDialog dialog = new InstallWizardDialog(shell, new GChangeWizard());
			dialog.open();
		}
		
		return null;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public boolean isHandled() {
		return true;
	}

	@Override
	public void removeHandlerListener(IHandlerListener handlerListener) {
	}

}
