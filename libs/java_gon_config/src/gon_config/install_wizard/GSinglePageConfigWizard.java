package gon_config.install_wizard;

import gon_client_management.model.ext.GIConfigPage;
import gon_client_management.view.ext.ConfigWizardPage;

import org.eclipse.jface.wizard.IWizardContainer;

public class GSinglePageConfigWizard extends GSinglePageWizard {

	private GIConfigPage configPage;
	private ConfigWizardPage page;

	public GSinglePageConfigWizard(ConfigWizardPage page, GIConfigPage configPage) {
		super(page);
		this.page = page;
		this.configPage = configPage;
	}

	@Override
	public boolean performFinish() {
		IWizardContainer container = getContainer();
		if (container!=null) {
			page.setValues();
			String errorMsg = configPage.savePage();
			if (errorMsg!=null) {
				page.setErrorMessage(errorMsg);
				return false;
			}
			
		}
		return true;
	}
	
	

}
