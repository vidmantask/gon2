package gon_config.install_wizard;


abstract class GFinalisePage extends GJobWizardPage {

	public GFinalisePage(String pageName) {
		super(pageName);
	}
	
	
	@Override
	public boolean canFlipToNextPage() {
		return false;
	}
	

}
