package gon_config.install_wizard;

import gon_client_management.model.ext.GIJobInfo;
import gon_config.model.GConfigModelFactory;
import gon_config.service.GLocalServiceJob;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.giritech.admin_deploy_ws.types_job.Job_status_type0;

public class GCreateSetupDataPage extends GJobWizardPage {

	private boolean canCallDemoData = false;
	Button button = null;
	
	protected GCreateSetupDataPage() {
		super("CreateSetupData");
		setTitle("Generate Additional Setup Data");
	}

	@Override
	public void createControl(Composite parent) {
		FormToolkit toolkit = new FormToolkit(parent.getDisplay());
		Form dialogForm = toolkit.createForm(parent);
		
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		dialogForm.getBody().setLayout(layout);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		dialogForm.getBody().setLayoutData(gridData);
		
		Label finishlabel = toolkit.createLabel(dialogForm.getBody(), "", SWT.WRAP);
		finishlabel.setText("To complement the setup a set of software packages is generated for the users of the\n" +
				"G/On Server. Also a number of rules are created for demonstrating the servers rule base\n" +
				"functionality. The demonstration rules can be seen in the Administration client.\n\n" +
				"Click the 'Generate' button to generate packages and demonstration data.\n\n");
		finishlabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		
		button = toolkit.createButton(dialogForm.getBody(), "Generate", SWT.NONE);
		GridData buttongridData = new GridData();
		buttongridData.horizontalAlignment = SWT.END;
		button.setLayoutData(buttongridData);
		
		
		final GLocalServiceJob localServiceJob =  GConfigModelFactory.getConfigModel().getCreateGPMSJob();
		final GLocalServiceJob localServiceJob1 =  GConfigModelFactory.getConfigModel().getCreateDemoDataJob();
		
		button.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				performAction(localServiceJob, "Generate packages", "1");
				if (canCallDemoData)
					performAction(localServiceJob1, "Generate demo data", "1");
				
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				performAction(localServiceJob, "Generate packages", "1");
				if (canCallDemoData)
					performAction(localServiceJob1, "Generate demo data", "1");
				
			}
			
		});
		
		setControl(dialogForm);

		
	}

	@Override
	public void jobFinished(String jobId, GIJobInfo lastProgress) {
		super.jobFinished(jobId, lastProgress);
		if (lastProgress.getJobStatus()==Job_status_type0.done_ok_job_status) {
			//GInstallWizard wizard = (GInstallWizard) getWizard();
			//wizard.setCanFinish(true);
			button.setEnabled(false);
			canCallDemoData  = true;
		}
		else {
			setErrorMessage(lastProgress.getJobHeader() + ": " + lastProgress.getJobInfo());
		}
		
		
	}
	
}
