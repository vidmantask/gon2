package gon_config.install_wizard;

import gon_config.GConfigActivator;

import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;

public class GSinglePageWizard extends Wizard {

	private boolean needsProgress;

	public GSinglePageWizard(WizardPage page) {
		this.needsProgress = page instanceof GJobWizardPage;
		this.addPage(page);
		setDialogSettings(GConfigActivator.getDefault().getDialogSettings());
		
	}

	@Override
	public boolean performFinish() {
		return true;
	}

	@Override
	public boolean needsProgressMonitor() {
		return needsProgress;
	}
	
}
