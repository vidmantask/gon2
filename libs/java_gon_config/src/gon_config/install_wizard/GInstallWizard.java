package gon_config.install_wizard;

import gon_client_management.model.ext.GIConfigPanePage;
import gon_client_management.view.ext.ConfigWizardPage;
import gon_client_management.model.ext.GIConfigPage;
import gon_config.GConfigActivator;
import gon_config.model.GConfigModelFactory;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;

public class GInstallWizard extends Wizard {
	

	private boolean canFinish = false;

	private WizardState currentState;
	private ArrayList<IWizardPage> beforeConfigPages;
	private ArrayList<IWizardPage> afterConfigPages;

	private HashMap<IWizardPage, GIConfigPage> configPageModelMap;

	private HashMap<IWizardPage, IWizardPage> nextPageMap;

	
	enum WizardState {
		BEFORE_CONFIG,
		CONFIG,
		AFTER_CONFIG
	}

	public GInstallWizard() {
		currentState = WizardState.BEFORE_CONFIG;
		
		beforeConfigPages = new ArrayList<IWizardPage>();
		afterConfigPages = new ArrayList<IWizardPage>();
		
		configPageModelMap = new HashMap<IWizardPage, GIConfigPage>();
		
		nextPageMap = new HashMap<IWizardPage, IWizardPage>();
		
		setForcePreviousAndNextButtons(true);
		
		setDialogSettings(GConfigActivator.getDefault().getDialogSettings());
	}
	
	
	@Override
	public void addPages() {
		GInstallStartPage startPage = new GInstallStartPage();
		addBeforeConfigPage(startPage);
		
		GFinaliseWizardPage endPage = new GFinaliseWizardPage();
		addAfterConfigPage(endPage);
		
	}

	public void addBeforeConfigPage(IWizardPage page) {
		if (beforeConfigPages.size()==0)
			super.addPage(page);
		beforeConfigPages.add(page);
	}

	public void addAfterConfigPage(IWizardPage page) {
		afterConfigPages.add(page);
	}
	
	
	@Override
	public void addPage(IWizardPage page) {
//		super.addPage(page);
		page.setWizard(this);
	}

	@Override
	public IWizardPage getNextPage(IWizardPage page) {
		IWizardPage nextPage = nextPageMap.get(page);
		if (nextPage==null) {
			return page;
		}
		return nextPage;
		
		
	}
	

	public void backPressed(IWizardPage page) {
		final IWizardPage previousPage = page.getPreviousPage();
		if (beforeConfigPages.contains(previousPage)) {
			currentState = WizardState.BEFORE_CONFIG;
		}
		else if (!afterConfigPages.contains(previousPage)) {
			currentState = WizardState.CONFIG;
			
		}
		
		clearNextMap(previousPage);
//		findNextPage(previousPage);
	}


	private void clearNextMap(IWizardPage page) {
		if (page!=null) {
			IWizardPage nextPage = nextPageMap.remove(page);
			clearNextMap(nextPage);
		}
	}


	private IWizardPage findNextPage(IWizardPage page) {
		IWizardPage nextPage = nextPageMap.get(page);
		if (nextPage!=null)
			return nextPage;
		boolean firstOfLastPages = false;
		if (currentState==WizardState.BEFORE_CONFIG) {
			int index = beforeConfigPages.indexOf(page);
			if (index<0) {
				GConfigActivator.getLogger().logError("Unable to find page '" + page.getName() + "' in install wizard pages");
				throw new RuntimeException("Error in page order");
			}
			index++;
			if (index<beforeConfigPages.size()) {
				nextPage = beforeConfigPages.get(index);
				addPage(nextPage);
				return nextPage;
			}
			else {
				GIConfigPage firstConfigPage = GConfigModelFactory.getConfigModel().getFirstConfigPage();
				if (firstConfigPage!=null) {
					currentState = WizardState.CONFIG;
					ConfigWizardPage configWizardPage = addConfigPage(firstConfigPage);
					return configWizardPage;
				}
				else {
					currentState = WizardState.AFTER_CONFIG;
					firstOfLastPages = true;
				}
			}
		}
		if (currentState==WizardState.CONFIG) {
			if (page instanceof ConfigWizardPage) {
				ConfigWizardPage currentConfigWizardPage = (ConfigWizardPage) page;
				GIConfigPage currentConfigPage = configPageModelMap.get(currentConfigWizardPage);
				if (currentConfigPage==null) {
					GConfigActivator.getLogger().logError("Unable to find model object for page '" + page.getName() + "' in install wizard pages");
					throw new RuntimeException("Error in wizard order");
				}
				if (currentConfigPage.isFinished()) {
					GIConfigPage nextConfigPage = GConfigModelFactory.getConfigModel().getNextConfigPage();
					if (nextConfigPage==null) {
						currentState = WizardState.AFTER_CONFIG;
						firstOfLastPages = true;
					}
					else {
						ConfigWizardPage configWizardPage = addConfigPage(nextConfigPage);
						return configWizardPage;
					}
				}
				else {
					return page;
				}
			}
			else {
				return page;
			}
			
		}
		if (currentState==WizardState.AFTER_CONFIG) {
			if (afterConfigPages.size()>0) {
				if (firstOfLastPages) {
					nextPage = afterConfigPages.get(0);
					addPage(nextPage);
					return nextPage;
				}
				else {
					int index = afterConfigPages.indexOf(page);
					if (index<0) {
						GConfigActivator.getLogger().logError("Unable to find page '" + page.getName() + "' in install wizard pages");
						throw new RuntimeException("Error in page order");
					}
					index++;
					if (index<afterConfigPages.size()) {
						nextPage = afterConfigPages.get(index);
						addPage(nextPage);
						return nextPage;
					}
					else {
						return null;
					}
					
				}
			}
			else {
				return null;
			}
			
		}
		GConfigActivator.getLogger().logError("Illegal state '" + currentState + "' in install wizard");
		return null;
	}


	private ConfigWizardPage addConfigPage(GIConfigPage firstConfigPage) {
		GIConfigPanePage pageModel = firstConfigPage.getPage();
		ConfigWizardPage configWizardPage;
		if (firstConfigPage.getType().equals("User")) {
			configWizardPage = new UserConfigWizardPage(pageModel, getDialogSettings(), false);
		}
		else {
			configWizardPage = new ConfigWizardPage(pageModel, GConfigActivator.getLogger(), getDialogSettings(), false);
		}
		this.configPageModelMap.put(configWizardPage, firstConfigPage);
		addPage(configWizardPage);
		return configWizardPage;
	}
	
	


	@Override
	public boolean performFinish() {
		
		return true;
	}

	@Override
	public boolean needsProgressMonitor() {
		return true;
	}


	public boolean finishPage(IWizardPage page) {
		if (page instanceof ConfigWizardPage && currentState==WizardState.CONFIG) {
			ConfigWizardPage configPage = (ConfigWizardPage) page;
			configPage.setValues();
			GIConfigPage currentConfigPage = configPageModelMap.get(configPage);			

			final String errorMsg = currentConfigPage.savePage();
			if (errorMsg!=null) {
				configPage.setErrorMessage(errorMsg);
				return false;
			}
		}
		IWizardPage nextPage = findNextPage(page);
		nextPageMap.put(page, nextPage);
		return true;
	}
	
	

	public void setCanFinish(boolean b) {
		canFinish = b;
		
	}



	@Override
	public boolean canFinish() {
		return canFinish;
	}
	
}
