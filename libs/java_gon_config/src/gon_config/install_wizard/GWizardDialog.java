package gon_config.install_wizard;

import gon_config.GConfigActivator;

import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;

public class GWizardDialog extends WizardDialog {

	public GWizardDialog(Shell parentShell, IWizard newWizard) {
		super(parentShell, newWizard);
	}
	
	@Override
	protected IDialogSettings getDialogBoundsSettings() {
		return GConfigActivator.getDefault().getDialogSettings();
	}
	

}
