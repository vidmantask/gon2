package gon_config.install_wizard;

import gon_config.GConfigActivator;

import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;

public class InstallWizardDialog extends GWizardDialog {

	public InstallWizardDialog(Shell parentShell, IWizard newWizard) {
		super(parentShell, newWizard);
	}


	@Override
	protected void nextPressed() {
		IWizardPage page = getCurrentPage();
		GInstallWizard wizard = (GInstallWizard) getWizard();
		boolean ok = wizard.finishPage(page);
		if (ok)
			super.nextPressed();
	}

	@Override
	protected void backPressed() {
		IWizardPage page = getCurrentPage();
		GInstallWizard wizard = (GInstallWizard) getWizard();
		wizard.backPressed(page);
		super.backPressed();
	}
	
	
	
	

}
