package gon_config.install_wizard;

import gon_client_management.model.ext.GIJobInfo;
import gon_config.model.GConfigModelFactory;
import gon_config.service.GLocalServiceJob;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;

import com.giritech.admin_deploy_ws.types_job.Job_status_type0;

public class GInstallStartPage extends GJobWizardPage {

	private Button button;

	protected GInstallStartPage() {
		super("Start");

		setTitle("Initialize Configuration");
	}

	@Override
	public void createControl(Composite parent) {

		/* Background color for the entire area - including progress. */
		parent.getParent().setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		/* Background color for the composite. */
		parent.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));

		FormToolkit toolkit = new FormToolkit(parent.getDisplay());
		ScrolledForm dialogForm = toolkit.createScrolledForm(parent);

		/* Background color for the main content area. */
		//dialogForm.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE)); // TEST

		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		dialogForm.getBody().setLayout(layout);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		dialogForm.getBody().setLayoutData(gridData);

		Label welcomelabel = toolkit.createLabel(dialogForm.getBody(), "", SWT.WRAP);
		welcomelabel.setText("Welcome to the G/On Server Configuration wizard.\n\n");
		welcomelabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));

		Label stepslabel = toolkit.createLabel(dialogForm.getBody(), "", SWT.WRAP);
		stepslabel.setText("For each step in the configuration process there will be two sets of parameters that\n" +
				"can be specified. One set of parameters requires your attention while the second, more\n" +
				"advanced set of parameters will be predetermined by this wizard. At each step you may\n" +
				"click the Advanced button if you want to review the default settings.\n\n");

		Label generatelabel = toolkit.createLabel(dialogForm.getBody(), "", SWT.WRAP);
		generatelabel.setText("Click Prepare Configuration to create the necessary settings for this wizard.\n");
		button = toolkit.createButton(dialogForm.getBody(), "Prepare Configuration", 0);
		GridData buttongridData = new GridData();
		buttongridData.horizontalAlignment = SWT.END;
		button.setLayoutData(buttongridData);

		final GLocalServiceJob localServiceJob =  GConfigModelFactory.getConfigModel().getCreatePrepareInstallationJob();

		button.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				performAction(localServiceJob, "Prepare Installation", "1");

			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				performAction(localServiceJob, "Prepare Installation", "1");

			}

		});

		setControl(dialogForm);
	}



	@Override
	public void jobFinished(String jobId, GIJobInfo lastProgress) {
		//System.out.println("GInstallStartPage.jobFinished() called: " + lastProgress.getJobStatus() );

		if (lastProgress.getJobStatus() != Job_status_type0.done_ok_job_status) {
			//System.out.println("Setting button to enabled");
			button.setEnabled(true);
			button.update();
		}
		else {
			//System.out.println("Setting button to disabled");
			button.setEnabled(false);
			button.update();
		}
		super.jobFinished(jobId, lastProgress);
	}


}
