package gon_config.install_wizard;

import gon_client_management.model.ext.GIJobInfo;
import gon_config.model.GConfigModelFactory;
import gon_config.service.GLocalServiceJob;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;

import com.giritech.admin_deploy_ws.types_job.Job_status_type0;

public class GChangeStartPage extends GJobWizardPage {

	Button button = null;

	protected GChangeStartPage() {
		super("Start");

		setTitle("Change Configuration");
	}

	@Override
	public void createControl(Composite parent) {

		/* Background color for the entire area - including progress. */
		parent.getParent().setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		/* Background color for the composite. */
		parent.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));

		FormToolkit toolkit = new FormToolkit(parent.getDisplay());
		ScrolledForm dialogForm = toolkit.createScrolledForm(parent);

		/* Background color for the main content area. */
		//dialogForm.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE)); // TEST

		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		dialogForm.getBody().setLayout(layout);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		dialogForm.getBody().setLayoutData(gridData);

		Label welcomelabel = toolkit.createLabel(dialogForm.getBody(), "", SWT.WRAP);
		welcomelabel.setText("Welcome to the G/On Server Configuration Change wizard.\n\n");
		welcomelabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));


		Label generatelabel = toolkit.createLabel(dialogForm.getBody(), "", SWT.WRAP);
		generatelabel.setText("Prepare the system for update.\n\n" );
		button = toolkit.createButton(dialogForm.getBody(), "Prepare system change", 0);
		GridData buttongridData = new GridData();
		buttongridData.horizontalAlignment = SWT.END;
		button.setLayoutData(buttongridData);

		final GLocalServiceJob localServiceJob =  GConfigModelFactory.getConfigModel().getCreatePrepareChangeJob();

		button.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				performAction(localServiceJob, "Prepare system change", "1");

			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				performAction(localServiceJob, "Prepare system change", "1");

			}

		});

		setControl(dialogForm);
	}



	@Override
	public void jobFinished(String jobId, GIJobInfo lastProgress) {
		//System.out.println("GInstallStartPage.jobFinished() called: " + lastProgress.getJobStatus() );

		if (lastProgress.getJobStatus() != Job_status_type0.done_ok_job_status) {
			//System.out.println("Setting button to enabled");
			button.setEnabled(true);
			button.update();
		}
		else {
			//System.out.println("Setting button to disabled");
			button.setEnabled(false);
			button.update();
		}
		super.jobFinished(jobId, lastProgress);
	}


}
