package gon_config.install_wizard;

public class GChangeWizard extends GInstallWizard {
	
	

	public GChangeWizard() {
		super();
	}

	@Override
	public void addPages() {
		GChangeStartPage startPage = new GChangeStartPage();
		addBeforeConfigPage(startPage);
		
		GFinaliseChangePage endPage = new GFinaliseChangePage(); 
		addAfterConfigPage(endPage);
		
	}
	
}
