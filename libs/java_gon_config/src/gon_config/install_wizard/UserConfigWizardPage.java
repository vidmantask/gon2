package gon_config.install_wizard;

import gon_client_management.model.ext.GIConfigColumn;
import gon_client_management.model.ext.GIConfigPage;
import gon_client_management.model.ext.GIConfigPanePage;
import gon_client_management.model.ext.GIConfigRowValue;
import gon_client_management.model.ext.GIDetails;
import gon_client_management.view.ext.ConfigWizardPage;
import gon_client_management.view.ext.GEditorFieldCreator;
import gon_client_management.view.ext.GEnablingControl;
import gon_config.GConfigActivator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

import com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec;

public class UserConfigWizardPage extends ConfigWizardPage {
	
	List<GIConfigColumn> generalColumns = new ArrayList<GIConfigColumn>();
	Map<GIConfigColumn, Integer> mapColumnIndex = new HashMap<GIConfigColumn, Integer>();
	Map<String, UserDirConfig> mapUserDirConfig = new LinkedHashMap<String, UserDirConfig>();
	List<DetailsViewer> detailsViewers = new ArrayList<DetailsViewer>();
	public IDialogSettings dialogSettings;

	public UserConfigWizardPage(GIConfigPanePage configPanePage,IDialogSettings dialogSettings, boolean readOnly) {
		super(configPanePage, GConfigActivator.getLogger(), dialogSettings, readOnly);
		this.dialogSettings = dialogSettings;
		List<GIDetails> details = configPanePage.getDetails();
		for (GIDetails detail : details) {
			String userDirName = detail.getName();
			UserDirConfig userDirConfig = getUserDirConfig(userDirName);
			userDirConfig.detail = detail;
			
		}
		
		for(int i=0; i<configPanePage.getColumnCount(); i++) {
			GIConfigColumn column = configPanePage.getColumn(i);
			
			mapColumnIndex.put(column, i);
			if (column.getName().contains(".")) {
				int indexOf = column.getName().indexOf(".");
				String userDirName = column.getName().substring(0, indexOf);
				UserDirConfig userDirConfig = mapUserDirConfig.get(userDirName);
				String fieldName = column.getName().substring(indexOf+1);
				if (userDirConfig != null) {
					if (fieldName.equals("enabled")) {
						userDirConfig.enablerField = column;
					}
					else {
						userDirConfig.columns.add(column);
					}
				}
				else {
					generalColumns.add(column);
				}
			}
			else {
				generalColumns.add(column);
			}
		}
	}
	
	
	private UserDirConfig getUserDirConfig(String name) {
		UserDirConfig userDirConfig = mapUserDirConfig.get(name);
		if (userDirConfig==null) {
			userDirConfig = new UserDirConfig();
			mapUserDirConfig.put(name, userDirConfig);
		}
		return userDirConfig;
		
	}
	
	public void createControl(Composite parent) {
		/**
		 * Section for all content parts.
		 */
		FormToolkit toolkit = new FormToolkit(parent.getDisplay());
		ScrolledForm dialogForm = toolkit.createScrolledForm(parent);
		

		/* Setup layout for the main form. */
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		dialogForm.getBody().setLayout(layout);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		dialogForm.getBody().setLayoutData(gridData);

		if (generalColumns.size()>0) {
			final Composite master = configTemplateUtil.createFieldSection(dialogForm.getBody(), toolkit, myConfigPane.getTitle(), false);
			GIConfigPanePage generalConfigPane = new GSimpleConfigPanePage(generalColumns);
			configTemplateUtil.populateFieldSection(master, toolkit, generalConfigPane );
		}
		
		for (final UserDirConfig userDirConfig : mapUserDirConfig.values()) {
			DetailsViewer detailsViewer = new DetailsViewer(userDirConfig);
			detailsViewer.create(dialogForm.getBody(), toolkit);
			detailsViewers.add(detailsViewer);
			
		}
		
		setControl(dialogForm);
	}
	
	@Override
	public void setValues() {
		configTemplateUtil.setValues();
		for (DetailsViewer detailsViewer : detailsViewers) {
			detailsViewer.setValues();
		}
	}
	
	
	class DetailsViewer {
		
		UserDirConfig userDirConfig;
		private TableViewer tableViewer;
		private Button createButton;
		private Button deleteButton;
		private Button editButton;
		private Widget enablerWidget;
		
		DetailsViewer(UserDirConfig userDirConfig) {
			this.userDirConfig = userDirConfig;
		}
		
		public void setValues() {
			if (userDirConfig.enablerField != null) {
				GEditorFieldCreator.getWidgetValue(enablerWidget, myConfigPane.getValue(mapColumnIndex.get(userDirConfig.enablerField)));
				
			}
			
				
		}

		void create(Composite parent, FormToolkit toolkit) {
			
			Section masterSection = toolkit.createSection(parent, Section.TITLE_BAR | Section.TWISTIE ); 		
			masterSection.setText(userDirConfig.detail.getTitle());
			masterSection.setLayout(new GridLayout());
			//GridData masterSectionGridData = new GridData(GridData.FILL_BOTH);
			GridData masterSectionGridData = new GridData(GridData.FILL_HORIZONTAL);
			masterSection.setLayoutData(masterSectionGridData);

			/* Create a container for parts in this section. */
			final Composite section = toolkit.createComposite(masterSection);
			masterSection.setClient(section);
			
//			final Composite section = configTemplateUtil.createFieldSection(parent, toolkit, userDirConfig.detail.getTitle(), true);
			GIConfigPanePage userDirConfigPane = new GSimpleConfigPanePage(userDirConfig.columns);
			configTemplateUtil.populateFieldSection(section, toolkit, userDirConfigPane );


			GEnablingControl enablerControl = null;
			if (userDirConfig.enablerField != null) {
				GIConfigRowValue value = myConfigPane.getValue(mapColumnIndex.get(userDirConfig.enablerField));
				masterSection.setExpanded(value.getValueBoolean());
				GEditorFieldCreator editorFieldCreator = new GEditorFieldCreator(UserConfigWizardPage.this);
				
				enablerWidget = editorFieldCreator.createLabelAndEditor(userDirConfig.enablerField, toolkit, section, null);
				if (enablerWidget instanceof Button) {
					Button enablerButton = (Button) enablerWidget;
					final GEnablingControl control = new GEnablingControl(section, enablerButton);
					enablerButton.addSelectionListener(new SelectionAdapter() {
						public void widgetSelected(SelectionEvent e) {
							setEnabling(control);
						}
						
					});
					enablerControl = control;
					
				}
				GEditorFieldCreator.setWidgetValue(enablerWidget, value);
			}
			
			List<Control> controls = new ArrayList<Control>();
			if (!userDirConfig.detail.isSingleton()) {
			
				tableViewer = new TableViewer(section, SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION);
				TableViewerColumn elementColumn = new TableViewerColumn(tableViewer, SWT.NONE);
				elementColumn.getColumn().setWidth(500);
				elementColumn.getColumn().setResizable(true);
				elementColumn.getColumn().setText("Configured Directories");
				
				DetailsContentProvider provider = new DetailsContentProvider();
				tableViewer.setContentProvider(provider);
				tableViewer.setLabelProvider(provider);
	//			elementColumn.setLabelProvider(provider);
				tableViewer.setInput(section);
	
				final Table table = tableViewer.getTable();
				table.setHeaderVisible(true);
				table.setLinesVisible(true);
				table.setLayout(new GridLayout());
				GridData gridDataTable = new GridData(GridData.FILL_BOTH);
				gridDataTable.horizontalSpan = 2;
				gridDataTable.grabExcessHorizontalSpace = true;
				gridDataTable.grabExcessVerticalSpace = true;
				
				table.setLayoutData(gridDataTable);
				
				controls.add(table);
				controls.add(tableViewer.getControl());
			}

			int buttonCount = 0;
			if (userDirConfig.detail.isCreateEnabled())
				buttonCount += userDirConfig.detail.getCreateSpecs().length;
			if (userDirConfig.detail.isUpdateEnabled())
				buttonCount++;
			if (userDirConfig.detail.isDeleteEnabled())
				buttonCount++;
			
			Composite composite =  new Composite(section, SWT.NULL);
//			{
				GridLayout gridLayout = new GridLayout(buttonCount, false);
				composite.setLayout(gridLayout);
				GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
				gridData.horizontalSpan = 2;
				composite.setLayoutData(gridData);
//			}

			if (userDirConfig.detail.isUpdateEnabled()) {
				editButton = toolkit.createButton(composite, "Edit", SWT.PUSH);
	//			{
	//				TableEditor editor = new TableEditor(table);
	//				editor.grabHorizontal = true;
	//				editor.setEditor(editButton, tableItem, 1);
	//			}
				editButton.addSelectionListener(new SelectionAdapter() {
	
					@Override
					public void widgetSelected(SelectionEvent e) {
						if (!userDirConfig.detail.isSingleton()) {
							int selectionIndex = tableViewer.getTable().getSelectionIndex();
							if (selectionIndex>=0)
								openWizard(selectionIndex);
						}
						else {
							openWizard(0);
							
						}
						
					}
	
					
				});
				controls.add(editButton);
			}
			if (userDirConfig.detail.isDeleteEnabled()) {
				deleteButton = toolkit.createButton(composite, "Delete", SWT.PUSH);
				deleteButton.addSelectionListener(new SelectionAdapter() {
	
					@Override
					public void widgetSelected(SelectionEvent e) {
						Boolean delete = MessageDialog.openConfirm(null, "Confirm", "Are you sure you want to delete?");
						if (delete) {
							int selectionIndex = tableViewer.getTable().getSelectionIndex();
							if (selectionIndex>=0)
								userDirConfig.detail.delete(selectionIndex);
							tableViewer.refresh();
						}
						
					}
	
				});
				controls.add(deleteButton);
			}
//			{
//				TableEditor editor = new TableEditor(table);
//				editor.grabHorizontal = true;
//				editor.setEditor(deleteButton, tableItem, 2);
//			}
			
			boolean createEnabled = userDirConfig.detail.isCreateEnabled();
			if (createEnabled) {
				ConfigurationTemplateSpec[] createSpecs = userDirConfig.detail.getCreateSpecs();
				int count = 0;
				for (ConfigurationTemplateSpec configurationTemplateSpec : createSpecs) {
					createButton = toolkit.createButton(composite, "Add " + configurationTemplateSpec.getTitle() , SWT.PUSH);
					GridData gridData2 = new GridData();
	//				gridData2.horizontalSpan = 2;
					createButton.setLayoutData(gridData2);
					
					count++;
					final int wizardIndex = -count;
					createButton.addSelectionListener(new SelectionAdapter() {
						
						@Override
						public void widgetSelected(SelectionEvent e) {
							openWizard(wizardIndex);
							
						}
		
					});
					
					controls.add(createButton);
				}
				
			}
			
			if (!userDirConfig.detail.isSingleton()) {
				tableViewer.addSelectionChangedListener(new ISelectionChangedListener() {
					
					@Override
					public void selectionChanged(SelectionChangedEvent event) {
						updateActionEnabling();
						
					}
				});
				updateActionEnabling();
			}
			
			
//			final Composite otherFieldsSection = new Composite(section, SWT.NULL);
//			{
//				GridLayout gridLayout = new GridLayout(2, false);
//				otherFieldsSection.setLayout(gridLayout);
//				GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
//				gridData.horizontalSpan = 2;
//				otherFieldsSection.setLayoutData(gridData);
//			}
// 
//			GIConfigPanePage userDirConfigPane = new GSimpleConfigPanePage(userDirConfig.columns);
//			configTemplateUtil.populateFieldSection(otherFieldsSection, toolkit, userDirConfigPane );
//			Control[] children = section.getChildren();
			
			if (enablerControl!=null) {
				for (Control control : controls) {
					enablerControl.addChild(control);
				} 
				setEnabling(enablerControl);
			}
			
		}
		
		protected void updateActionEnabling() {
			if (!userDirConfig.detail.isSingleton()) {
			
				int index = tableViewer.getTable().getSelectionIndex();
				if (index<0) {
					setButtonEnabling(false);
				}
				else {
					setButtonEnabling(true);
					
				}
			}

			
		}

		private void setButtonEnabling(boolean b) {
			if (editButton!=null)
				editButton.setEnabled(b);
			if (deleteButton!=null)
				deleteButton.setEnabled(b);
			
		}

		private void openWizard(int index) {
			GIConfigPage configPage = userDirConfig.detail.getConfigPage(index);
			ConfigWizardPage configWizardPage = new ConfigWizardPage(configPage.getPage(), GConfigActivator.getLogger(), dialogSettings, false);
			GSinglePageWizard wizard = new GSinglePageConfigWizard(configWizardPage, configPage);
			WizardDialog dialog = new GWizardDialog(getShell(), wizard);
			dialog.open();
			if (tableViewer!=null)
				tableViewer.refresh();
		}

		private void setEnabling(final GEnablingControl control) {
			control.setEnabling();
			if (control.getEnabling()) {
				updateActionEnabling();
			}
		}

		class DetailsContentProvider extends LabelProvider implements IStructuredContentProvider, ITableLabelProvider {

			@Override
			public Object[] getElements(Object inputElement) {
				return userDirConfig.detail.getConfigSpecs();
			}

			@Override
			public void dispose() {
				
			}

			@Override
			public void inputChanged(Viewer viewer, Object oldInput,Object newInput) {
				
			}



			@Override
			public Image getColumnImage(Object element, int columnIndex) {
				return null;
			}

			@Override
			public String getColumnText(Object element, int columnIndex) {
				if (element instanceof ConfigurationTemplateSpec) {
					return ((ConfigurationTemplateSpec) element).getTitle();
				}
				return super.getText(element);
			}

			
		}
		
	}
	
	
	class UserDirConfig {
		public GIConfigColumn enablerField;
		List<GIConfigColumn> columns = new ArrayList<GIConfigColumn>();
		GIDetails detail = null;
		
		
	}
	
	class GSimpleConfigPanePage implements GIConfigPanePage  {

		List<GIConfigColumn> columns;
		
		GSimpleConfigPanePage(List<GIConfigColumn> columns) {
			this.columns = columns;
		}
		
		@Override
		public GIConfigPanePage getAdvancedPane() { return null; }

		@Override
		public GIConfigColumn getColumn(int index) {
			return columns.get(index);
		}

		@Override
		public int getColumnCount() {
			return columns.size();
		}

		@Override
		public List<GIDetails> getDetails() {
			return null;
		}

		@Override
		public String getTitle() {
			return myConfigPane.getTitle();
		}

		@Override
		public GIConfigRowValue getValue(int index) {
			GIConfigColumn column = columns.get(index);
			Integer realIndex = mapColumnIndex.get(column);
			return myConfigPane.getValue(realIndex);
		}
		
	}
	
	

}
