package gon_config.install_wizard;

public class GUpgradeWizard extends GInstallWizard {
	
	
	private String systemName;

	public GUpgradeWizard(String systemName) {
		super();
		this.systemName = systemName;
	}

	@Override
	public void addPages() {
		GUpgradeStartPage startPage = new GUpgradeStartPage(systemName);
		addBeforeConfigPage(startPage);
		
		GFinaliseUpgradePage endPage = new GFinaliseUpgradePage(); 
		addAfterConfigPage(endPage);
		
	}
	
}
