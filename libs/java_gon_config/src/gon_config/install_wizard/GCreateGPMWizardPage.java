package gon_config.install_wizard;

import gon_config.model.GConfigModelFactory;
import gon_config.service.GLocalServiceJob;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;

public class GCreateGPMWizardPage extends GJobWizardPage {


	public GCreateGPMWizardPage() {
		super("CreateGPM");
		setTitle("Generate Packages");
	}

	@Override
	public void createControl(Composite parent) {
		FormToolkit toolkit = new FormToolkit(parent.getDisplay());
		Form dialogForm = toolkit.createForm(parent);
		
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		dialogForm.getBody().setLayout(layout);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		dialogForm.getBody().setLayoutData(gridData);
		
		Label finishlabel = toolkit.createLabel(dialogForm.getBody(), "", SWT.WRAP);
		finishlabel.setText("Click the 'Generate' button to generate packages.\n\n");
		finishlabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		
		Button button = toolkit.createButton(dialogForm.getBody(), "Generate", SWT.NONE);
		GridData buttongridData = new GridData();
		buttongridData.horizontalAlignment = SWT.CENTER;
		button.setLayoutData(buttongridData);
		
		final GLocalServiceJob localServiceJob =  GConfigModelFactory.getConfigModel().getCreateGPMSJob();
		
		button.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				performAction(localServiceJob, "Generate packages", "1");
				
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				performAction(localServiceJob, "Generate packages", "1");
				
			}
			
		});
		
		setControl(dialogForm);

		
	}

	
}
