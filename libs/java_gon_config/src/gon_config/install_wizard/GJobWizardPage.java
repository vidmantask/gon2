package gon_config.install_wizard;

import gon_client_management.model.ext.GIJobInfo;
import gon_client_management.view.ext.GJobHandler;
import gon_client_management.view.ext.GJobHandler.JobUpdateHandler;
import gon_config.GConfigActivator;
import gon_config.service.GLocalServiceJob;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.WizardPage;

import com.giritech.admin_deploy_ws.types_job.Job_status_type0;

public abstract class GJobWizardPage extends WizardPage {

	protected boolean complete = false;

	protected GJobWizardPage(String pageName) {
		super(pageName);
	}

	public GJobWizardPage(String pageName, String title,
			ImageDescriptor titleImage) {
		super(pageName, title, titleImage);
		//getControl().getParent().getParent().setBackground(getControl().getDisplay().getSystemColor(SWT.COLOR_BLUE));
	}
	
	@Override
	public boolean isPageComplete() {
		return complete;
	}

	protected void performAction(GLocalServiceJob localServiceJob, String title, String jobId) {
		final GJobHandler job = new GJobHandler(title, jobId, getShell().getDisplay(), localServiceJob, GConfigActivator.getLogger());
		MyJobUpdateHandler myJobUpdateHandler = new MyJobUpdateHandler();
		job.setJobUpdateHandler(myJobUpdateHandler);
		final IRunnableWithProgress runnable = new IRunnableWithProgress() {

			public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
				job.run(monitor);
			}
		};


		
		try {
			setMessage("");
			setErrorMessage(null);
			getContainer().run(true, true, runnable);
		} catch (InvocationTargetException e) {
			setErrorMessage(e.getCause().getMessage());
			return;
		} catch (InterruptedException e) {
			setErrorMessage(e.getMessage());
			return;
		}
		myJobUpdateHandler.finished(jobId, job.getProgress());
		
	}
	private class MyJobUpdateHandler implements JobUpdateHandler {

		@Override
		public void finished(String jobId, GIJobInfo lastProgress) {
			jobFinished(jobId, lastProgress);
			
		}

		@Override
		public void update(String jobId, GIJobInfo status) {
			setMessage(status.getJobHeader());
		}
	}
	
	public void jobFinished(String jobId, GIJobInfo lastProgress) {
		if (lastProgress.getJobStatus() != Job_status_type0.done_ok_job_status) {
			complete = false;
			setPageComplete(complete);
			setErrorMessage(lastProgress.getJobInfo());
		}
		else {
			complete = true;
			setPageComplete(complete);
			setMessage(lastProgress.getJobInfo());
		}
	}
}
