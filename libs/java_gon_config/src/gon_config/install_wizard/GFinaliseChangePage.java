package gon_config.install_wizard;

import gon_client_management.model.ext.GIJobInfo;
import gon_config.model.GConfigModelFactory;
import gon_config.service.GLocalServiceJob;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.giritech.admin_deploy_ws.types_job.Job_status_type0;

public class GFinaliseChangePage extends GFinalisePage {

	Button button = null;
	
	public GFinaliseChangePage() {
		super("Finalise");
		setTitle("Finalize Change");
	}
	


	@Override
	public void createControl(Composite parent) {
		FormToolkit toolkit = new FormToolkit(parent.getDisplay());
		Form dialogForm = toolkit.createForm(parent);
		
		
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		dialogForm.getBody().setLayout(layout);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		dialogForm.getBody().setLayoutData(gridData);
		
		Label finishlabel = toolkit.createLabel(dialogForm.getBody(), "", SWT.WRAP);
		finishlabel.setText("The configuration wizard is now ready to apply your changes.\n" +
				"Click the 'Update' button to generate and write the configuration data to the server. \n\n " +
				"Notice that services will have to be restarted for the changes to take effect. Use the\n" +
				"Microsoft Windows Services tool to restart the services.\n" +
				"Restarting services means that any connected users will be disconnected.\n\n");
		finishlabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		
		//toolkit.createText(dialogForm.getBody(), "Click button to finalise installation");	
		button = toolkit.createButton(dialogForm.getBody(), "Update", 0);
		GridData buttongridData = new GridData();
		buttongridData.horizontalAlignment = SWT.END;
		button.setLayoutData(buttongridData);
		
		
		final GLocalServiceJob localServiceJob =  GConfigModelFactory.getConfigModel().getCreateFinaliseChangeJob();
		
		button.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				performAction(localServiceJob, "Finalizing", "Finalizing");
				
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				performAction(localServiceJob, "Finalizing", "Finalizing");
				
			}
			
		});
		
		setControl(dialogForm);

		
		
	}

	



	@Override
	public void jobFinished(String jobId, GIJobInfo lastProgress) {
		super.jobFinished(jobId, lastProgress);
		if (lastProgress.getJobStatus()==Job_status_type0.done_ok_job_status) {
			GInstallWizard wizard = (GInstallWizard) getWizard();
			wizard.setCanFinish(true);
			button.setEnabled(false);
			setPreviousPage(null);
		}
		else {
			setErrorMessage(lastProgress.getJobHeader() + ": " + lastProgress.getJobInfo());
		}
	}

	
}
