package gon_config.install_wizard;

import gon_client_management.model.ext.GIJobInfo;
import gon_config.model.GConfigModelFactory;
import gon_config.service.GLocalServiceJob;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.giritech.admin_deploy_ws.types_job.Job_status_type0;

public class GFinaliseWizardPage extends GFinalisePage {

	boolean canCallFinish = false;
	Button button = null;
	
	public GFinaliseWizardPage() {
		super("Finalise");
		setTitle("Finalize Installation");
	}
	


	@Override
	public void createControl(Composite parent) {
		FormToolkit toolkit = new FormToolkit(parent.getDisplay());
		Form dialogForm = toolkit.createForm(parent);
		
		
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		dialogForm.getBody().setLayout(layout);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		dialogForm.getBody().setLayoutData(gridData);
		
		Label finishlabel = toolkit.createLabel(dialogForm.getBody(), "", SWT.WRAP);
		finishlabel.setText("The G/On Server Configuration wizard has now completed the collection of data\n" +
				"required to configure and install Services and to generate server configuration data.\n" +
				"This may take a few minutes.\n\n" +
				"Click the Configure button to configure Server and Services.");
		finishlabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		
		//toolkit.createText(dialogForm.getBody(), "Click button to finalise installation");	
		button = toolkit.createButton(dialogForm.getBody(), "Configure", 0);
		GridData buttongridData = new GridData();
		buttongridData.horizontalAlignment = SWT.END;
		button.setLayoutData(buttongridData);
		
		Label serviceslabel = toolkit.createLabel(dialogForm.getBody(), "", SWT.WRAP | SWT.BOLD);
		serviceslabel.setText("\n\nPlease Notice:\nThe Install Services button will install the services but they will not be started. Use the\n" +
				"Microsoft Windows Services tool to start the services and specify properties for each\n" +
				"service. When you have installed the services click Finish to close this wizard and\n" +
				"return to the G/On Server status view.");
		serviceslabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		
		final GLocalServiceJob localServiceJob =  GConfigModelFactory.getConfigModel().getCreateFinaliseInstallationJob();
		
		button.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				performAction(localServiceJob, "Finalizing", "Finalizing");
				
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				performAction(localServiceJob, "Finalizing", "Finalizing");
				
			}
			
		});
		
		setControl(dialogForm);

		
		
	}

	@Override
	public void jobFinished(String jobId, GIJobInfo lastProgress) {
		super.jobFinished(jobId, lastProgress);
		if (lastProgress.getJobStatus()==Job_status_type0.done_ok_job_status) {
			GInstallWizard wizard = (GInstallWizard) getWizard();
			wizard.setCanFinish(true);
			button.setEnabled(false);
			canCallFinish = true;
		}
		else {
			setErrorMessage(lastProgress.getJobHeader() + ": " + lastProgress.getJobInfo());
		}
	}

	
}
