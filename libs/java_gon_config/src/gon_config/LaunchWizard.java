package gon_config;

import gon_config.install_wizard.GInstallWizard;
import gon_config.install_wizard.GUpgradeWizard;
import gon_config.install_wizard.InstallWizardDialog;
import gon_config.model.GConfigModelFactory;

import java.util.Properties;

import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.intro.IIntroManager;
import org.eclipse.ui.intro.IIntroSite;
import org.eclipse.ui.intro.config.IIntroAction;

public class LaunchWizard implements IIntroAction {	
	
	public void startWizard(final IWorkbenchWindow workbenchWindow, String systemName) {
		final IWizard wizard;
		if (systemName==null) {
			wizard = new GInstallWizard();
		}
		else {
			wizard = new GUpgradeWizard(systemName);
		}

		workbenchWindow.getWorkbench().getDisplay().asyncExec(new Runnable() {

			@Override
			public void run() {
				WizardDialog dialog = new InstallWizardDialog(workbenchWindow.getShell(), wizard);
				dialog.open();
				update();
			}
		});
	}
	
	/**
	 * Standard run method for the IIntroAction.
	 */
	@Override
	public void run(IIntroSite site, Properties params) {
		String systemName = params.getProperty("systemname");
		startWizard(site.getWorkbenchWindow(), systemName);
	}

	/**
	 * Do stuff after the wizard has run. 
	 * (I know i am using a deprecated method here. Will fix it later)
	 */
	public void update() {
		boolean configured = GConfigModelFactory.getConfigModel().getConfiguredStatus();
		final IIntroManager mgr = PlatformUI.getWorkbench().getIntroManager();
		if (configured) 
			mgr.closeIntro(mgr.getIntro());
		else 
			mgr.showIntro(PlatformUI.getWorkbench().getWorkbenchWindows()[0], false);
	}
	
}
