package gon_config;

import gon_client_management.ext.CommonUtils;
import gon_config.model.GConfigModelFactory;

import java.io.PrintWriter;
import java.util.Calendar;
import java.util.List;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.intro.config.IIntroContentProviderSite;
import org.eclipse.ui.intro.config.IIntroXHTMLContentProvider;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.giritech.server_config_ws.DateType;
import com.giritech.server_config_ws.GOnSystemType;
import com.giritech.server_config_ws.LicenseInfoType;

public class DynamicIntroContent implements IIntroXHTMLContentProvider {

	private static Boolean configured = null;
	
	
	private boolean isConfigured() {
		if (configured==null) {
			configured = Boolean.valueOf(GConfigModelFactory.getConfigModel().getConfiguredStatus());
		}
		return configured.booleanValue();
	}

	@Override
	public void createContent(String id, Element parent) {
		Document dom = parent.getOwnerDocument();
		
		if (id.equals("should_i_run_the_wizard_1")) {
			
//			<h2>License info</h2>
			{
				Element h2 = dom.createElement("h2");
				parent.appendChild(h2);
				h2.setTextContent("License info");
			}
			LicenseInfoType licenseStatus = GConfigModelFactory.getConfigModel().getLicenseStatus();

			// Check license exists
			if (licenseStatus==null) {
				{
					Element div = dom.createElement("div");
					div.appendChild(dom.createTextNode("Invalid license. System not operational."));
					div.setAttribute("class", "error");
					parent.appendChild(div);
					
					writeLicenseFileChoice(parent, dom, null);
				}

				return;
				
			}
			// Write license info
			{
				Element table = dom.createElement("table");
				table.setAttribute("border", "0");
				table.setAttribute("width", "100%");
				parent.appendChild(table);
				
				addLicenseRow(dom, table, "License Number:", licenseStatus.getLicense_number() + "#" + licenseStatus.getLicense_file_number());
				if (licenseStatus.getLicense_expires()!=null)
				{
					String formattedDate = convertDataToString(licenseStatus.getLicense_expires());
					addLicenseRow(dom, table, "License Expiration Date:", formattedDate);
					
				}
				if (licenseStatus.getMaintenance_expires()!=null)
				{
					String formattedDate = convertDataToString(licenseStatus.getMaintenance_expires());
					addLicenseRow(dom, table, "Maintenance Expiration Date", formattedDate);
					
				}
				addLicenseRow(dom, table, "Licensed To:", licenseStatus.getLicensed_to());

				String[] loginText = licenseStatus.getLogin_text();
				if (loginText!=null && loginText.length>0) {
					Element p = dom.createElement("div");
					p.setAttribute("style", "font-weight:bold");
					for (int i=0; i<loginText.length; i++) {
						addLicenseRow(dom, table, "", loginText[i], false, true);
						Element p1 = dom.createElement("div");
						p.appendChild(p1);
						p1.setTextContent(loginText[i]);
						
					}
				}
				
			}

			List<GOnSystemType> upgradeSystems = GConfigModelFactory.getConfigModel().getSystemStatusForUpdate();
			
			// If default license - make user aware
//			if (licenseStatus.getIs_default_license()) {
				writeLicenseFileChoice(parent, dom, upgradeSystems);

//			}
			
			{  // run_upgrade_wizard
				if (upgradeSystems.size()>0) {
					Element parent_td = createBorderSection(dom, parent);
					
					createSectionHeader(dom, parent_td, "Upgrade of existing G/On Server installations:");
					
	
					Element table = dom.createElement("table");
					table.setAttribute("border", "0");
					table.setAttribute("cellpadding", "5");
					table.setAttribute("width", "100%");
					parent_td.appendChild(table);
					Element header_tr = dom.createElement("tr");
					header_tr.setAttribute("align", "left");
					table.appendChild(header_tr);
					
					Element th1 = dom.createElement("th");
					header_tr.appendChild(th1);
					th1.setTextContent("Name");
	
					Element th2 = dom.createElement("th");
					header_tr.appendChild(th2);
					th2.setTextContent("Version");
	
					
					
					Element th4 = dom.createElement("th");
					header_tr.appendChild(th4);
					th4.setTextContent("");
					
					for (GOnSystemType gOnSystemType : upgradeSystems) {
						Element tr = dom.createElement("tr");
						table.appendChild(tr);
						
						Element td1 = dom.createElement("td");
						tr.appendChild(td1);
						td1.setTextContent(gOnSystemType.getTitle());
	
						Element td2 = dom.createElement("td");
						tr.appendChild(td2);
						td2.setTextContent(gOnSystemType.getVersion());
	
	
						Element td4 = dom.createElement("td");
						td4.setAttribute("align", "right");
						tr.appendChild(td4);
						Element button = dom.createElement("input");
						button.setAttribute("type", "button");
						button.setAttribute("value", "Upgrade Wizard");
						button.setAttribute("name", gOnSystemType.getName());
						button.setAttribute("style", "width:110pt");
						button.setAttribute("onclick", "return click_upgrade_button(this)");
	//					button.setAttribute("onclick", "return click_upgrade_button('" + gOnSystemType.getName() + "')");
	//					button.setAttribute("onclick", "return button_link_onclick()");
						
						td4.appendChild(button);
						
						
					}
				}
			}			
//			<h2>G/On Configuration Wizard</h2>
			{
				Element parent_td = createBorderSection(dom, parent);
				
				createSectionHeader(dom, parent_td, "Configuration of a new G/On Server installation:");
				//Installation part 
				if (!isConfigured()) {
					Element div = dom.createElement("div");
					div.appendChild(dom.createTextNode("The server has not been configured!"));
					parent_td.appendChild(div);
				}
				else {
				
				
					Element bold = dom.createElement("b");
					parent_td.appendChild(bold);
					bold.appendChild(dom.createTextNode("WARNING: This wizard is used for the initial configuration " +
														"of the G/On Server or for a complete re-configuration. A new set " +
														"of encryption keys will be created and any existing Authentication Rules, " +
														"Authorization Rules, and Menu Action Items will be deleted!"));
					
				}

				// Wrap button in table for alignment with upgrade buttons
				Element parentTable = dom.createElement("table");
				parentTable.setAttribute("cellpadding", "5");
				parentTable.setAttribute("width", "100%");
				parent_td.appendChild(parentTable);
				
				Element parent_tr = dom.createElement("tr");
				parentTable.appendChild(parent_tr);
				
				parent_td = dom.createElement("td");
				parent_tr.appendChild(parent_td);
				
				Element div = dom.createElement("div");
				parent_td.appendChild(div);
				div.setAttribute("align", "right");
//						div.setAttribute("class", "buttons");
	
				Element installButton = dom.createElement("input");
				installButton.setAttribute("id", "launchWiz");
				installButton.setAttribute("type", "button");
				installButton.setAttribute("value", "Installation Wizard");
				installButton.setAttribute("onclick", "return button_link_onclick()");
				installButton.setAttribute("style", "width:110pt");
				div.appendChild(installButton);
			}

		}
	}

	private void writeLicenseFileChoice(Element parent, Document dom,
			List<GOnSystemType> upgradeSystems) {
		{
			Element div = dom.createElement("div");
			if (upgradeSystems!=null && upgradeSystems.size()>0) {
				div.appendChild(dom.createTextNode("If you are upgrading an existing system the licence will be taken automatically from that system. Otherwise, "));
			}
			writeLicenceInstallChoice(dom, div);
			
			div.setAttribute("class", "highlight");
			parent.appendChild(div);
		}
	}

	private void writeLicenceInstallChoice(Document dom, Element div) {
		div.appendChild(dom.createTextNode("If you wish to install a new G/On license file, please click "
										  ));
		Element a = dom.createElement("a");
		div.appendChild(a);
//					a.setAttribute("href", "http://www.w3schools.com");
		a.setAttribute("href", "");
		a.setAttribute("onclick", "return refresh_clicked()");
		a.setTextContent("here");
	}

	public static String convertDataToString(DateType licenseExpires) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(licenseExpires.getYyyy().intValue(), licenseExpires.getMm().intValue()-1, licenseExpires.getDd().intValue());
		return CommonUtils.DateTimeConverter.datetime2local(calendar.getTime());
	}

	private void addLicenseRow(Document dom, Element table, String caption, String value) {
		addLicenseRow(dom, table, caption, value, false, false);
	}

	private void addLicenseRow(Document dom, 
							   Element table, 
							   String caption,
							   String value, 
							   boolean captionBold,
							   boolean valueBold) {
		
		Element tr = dom.createElement("tr");
		table.appendChild(tr);
		
		Element td1 = dom.createElement("td");
		if (captionBold)
			td1.setAttribute("style", "font-weight:bold");
		tr.appendChild(td1);
		td1.setTextContent(caption);

		Element td2 = dom.createElement("td");
		if (valueBold)
			td2.setAttribute("style", "font-weight:bold");
		tr.appendChild(td2);
		td2.setTextContent(value);
	}
	
	private Element createBorderSection(Document dom, Element parent) {
		parent.appendChild(dom.createElement("br"));
		Element parentTable = dom.createElement("table");
		parentTable.setAttribute("border", "1");
		parentTable.setAttribute("cellpadding", "5");
		parentTable.setAttribute("width", "100%");
		parent.appendChild(parentTable);
		
		Element parent_tr = dom.createElement("tr");
		parentTable.appendChild(parent_tr);
		
		Element parent_td = dom.createElement("td");
		parent_tr.appendChild(parent_td);
		return parent_td;
	}

	private void createSectionHeader(Document dom, Element parent_td, String text) {
		{
//			Element h2 = dom.createElement("div");
//			h2.setAttribute("style", "font-size:11pt;font-weight:bold; ");
//			parent_td.appendChild(h2);
//			h2.setTextContent(text);
//			parent_td.appendChild(dom.createElement("br"));
			
			Element h2 = dom.createElement("h4");
			parent_td.appendChild(h2);
			h2.setTextContent(text);
			
		}
	}

	@Override
	public void createContent(String id, PrintWriter out) { /* Unused. */}

	@Override
	public void createContent(String id, Composite parent, FormToolkit toolkit) { /* Unused. */	}

	@Override
	public void dispose() { /* Unused (for now). */ 
		configured = null;
	}

	@Override
	public void init(IIntroContentProviderSite site) { /* Unused (for now). */}
}
