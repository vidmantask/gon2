package gon_config;

import java.util.Properties;

import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.intro.IIntroManager;
import org.eclipse.ui.intro.IIntroPart;
import org.eclipse.ui.intro.IIntroSite;
import org.eclipse.ui.intro.config.IIntroAction;

public class RestartIntro implements IIntroAction {

	@Override
	public void run(IIntroSite site, Properties params) {
		final IIntroManager mgr = PlatformUI.getWorkbench().getIntroManager();
//		mgr.closeIntro(mgr.getIntro());
		IIntroPart showIntro = null;
		int count = 0;
		while (showIntro==null && count<200) {
			showIntro = mgr.showIntro(null, false);
			count++;
		} 
		
	}

}
