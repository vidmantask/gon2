package gon_config;

import gon_client_management.ext.CommonUtils;
import gon_config.service.GLocalServiceFactory;

import java.io.IOException;
import java.util.Properties;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.intro.IIntroManager;
import org.eclipse.ui.intro.IIntroPart;
import org.eclipse.ui.intro.IIntroSite;
import org.eclipse.ui.intro.config.IIntroAction;

public class ChooseLicenseFile implements IIntroAction {

	String [] extensions = {"gon_license.lic", "*.lic", "*.*"}; 
	
	@Override
	public void run(IIntroSite site, Properties params) {
		Shell shell = site.getShell();
		boolean ok = runInShell(shell);
		if (ok)
			restartIntro(site, params);

	}

	public boolean runInShell(Shell shell) {
		FileDialog fileChooser = new FileDialog(shell, SWT.OPEN);
		fileChooser.setFileName("gon_license.lic");
		fileChooser.setFilterExtensions(extensions);
		String filePath = fileChooser.open();
		if (filePath==null)
			return false;
		String content;
		try {
			content = CommonUtils.convertFileToString(filePath, null, "UTF-8");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return GLocalServiceFactory.getLocalService().setLicense(content);
	}
	
	public void restartIntro(IIntroSite site, Properties params) {
		final IIntroManager mgr = PlatformUI.getWorkbench().getIntroManager();
		mgr.closeIntro(mgr.getIntro());
		IIntroPart showIntro = null;
		int count = 0;
		while (showIntro==null && count<200) {
			try {
				showIntro = mgr.showIntro(null, false);
			} 
			catch(Throwable t) {
				GConfigActivator.getLogger().logException(t);
			}
			count++;
		}
		if (showIntro==null)
			throw new RuntimeException("Unable to refresh view.\nPlease close and reload the Welcome View manually");
		
	}
	

}
