package gon_config;

import gon_client_management.ext.CommonUtils;
import gon_config.service.GLocalService;
import gon_config.service.GLocalServiceFactory;

import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;

/**
 * This class controls all aspects of the application's execution
 */
public class GConfigApplication implements IApplication {

	/* (non-Javadoc)
	 * @see org.eclipse.equinox.app.IApplication#start(org.eclipse.equinox.app.IApplicationContext)
	 */
	public Object start(IApplicationContext context) {
		CommonUtils.setSSLProperties("gon_config.cacerts");
		Display display = PlatformUI.createDisplay();
		String[] args = (String[]) context.getArguments().get(IApplicationContext.APPLICATION_ARGS);
		GLocalService.setArgs(args);
		
		boolean serverConnected = false;
		while(!serverConnected) {
			try {
				serverConnected = GLocalServiceFactory.getLocalService().ping();
			}
			catch (Throwable t) {
				GConfigLaunchFailureDialog lfDialog = new GConfigLaunchFailureDialog(null);
				int rc = lfDialog.open();
				if (rc == Window.CANCEL) {
					System.exit(0);
				}
			}
		}
		
		try {
			int returnCode = PlatformUI.createAndRunWorkbench(display, new GConfigApplicationWorkbenchAdvisor());
			if (returnCode == PlatformUI.RETURN_RESTART) {
				return IApplication.EXIT_RESTART;
			}
			return IApplication.EXIT_OK;
		} finally {
			display.dispose();
		}
	}


	/* (non-Javadoc)
	 * @see org.eclipse.equinox.app.IApplication#stop()
	 */
	public void stop() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		if (workbench == null)
			return;
		final Display display = workbench.getDisplay();
		display.syncExec(new Runnable() {
			public void run() {
				if (!display.isDisposed())
					workbench.close();
			}
		});
	}
}
