package gon_config.preferences;

import gon_config.GConfigActivator;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;


/**
 * Class used to initialize default preference values.
 */
public class GConfigPreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	public void initializeDefaultPreferences() {
		IPreferenceStore store = GConfigActivator.getDefault().getPreferenceStore();
		
		store.setDefault(GConfigPreferenceConstants.G_CONFIG_SERVER_EXECUTABLE_PATH, "../../gon_config_service/win/gon_config_service.exe");
		store.setDefault(GConfigPreferenceConstants.G_CONFIG_SERVER_HOST, "127.0.0.1");
		store.setDefault(GConfigPreferenceConstants.G_CONFIG_SERVER_PORT, "8071");
		store.setDefault(GConfigPreferenceConstants.G_CONFIG_SERVER_TIMEOUT, 5);
	}

}
