package gon_config.preferences;

/**
 * Constant definitions for plug-in preferences
 * 
 * EXAMPLES:
 * public static final String P_PATH = "pathPreference";
 *	public static final String P_BOOLEAN = "booleanPreference";
 *	public static final String P_CHOICE = "choicePreference";
 *	public static final String P_STRING = "stringPreference";
 *
 */
public class GConfigPreferenceConstants {
	
	public static final String G_CONFIG_SERVER_EXECUTABLE_PATH = "stringPreferenceServerPath";
	public static final String G_CONFIG_SERVER_HOST = "stringPreferenceServerHost";
	public static final String G_CONFIG_SERVER_PORT = "stringPreferenceServerPort";
	public static final String G_CONFIG_SERVER_TIMEOUT = "stringPreferenceServerTimeout";
}
