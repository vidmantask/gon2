package gon_config.preferences;

import gon_config.GConfigActivator;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

/**
 * This class represents a preference page that
 * is contributed to the Preferences dialog. By 
 * subclassing <samp>FieldEditorPreferencePage</samp>, we
 * can use the field support built into JFace that allows
 * us to create a page that is small and knows how to 
 * save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They
 * are stored in the preference store that belongs to
 * the main plug-in class. That way, preferences can
 * be accessed directly via the preference store.
 */

public class GConfigPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	
	public GConfigPreferencePage() {
		super(GRID);
		setPreferenceStore(GConfigActivator.getDefault().getPreferenceStore());
		setDescription("Setup for the G/On Configuration Service connection\nA restart is required for changes to take effect");
	}


	/**
	 * Creates the field editors. Field editors are abstractions of
	 * the common GUI blocks needed to manipulate various types
	 * of preferences. Each field editor knows how to save and
	 * restore itself.
	 * 
	 * EXAMPLES: 
	 *	addField(new DirectoryFieldEditor(PreferenceConstants.P_PATH, "&Directory preference:", getFieldEditorParent()));
	 * addField(new BooleanFieldEditor(PreferenceConstants.P_BOOLEAN, "&An example of a boolean preference", getFieldEditorParent()));
	 * 	addField(new RadioGroupFieldEditor(PreferenceConstants.P_CHOICE, "An example of a multiple-choice preference", 1, new String[][] { { "&Choice 1", "choice1" }, {"C&hoice 2", "choice2" }}, getFieldEditorParent()));
	 *	addField(new StringFieldEditor(PreferenceConstants.G_SERVER_URL, "G/On Administration Server:", getFieldEditorParent()));
	 * 
	 */
	public void createFieldEditors() {
		StringFieldEditor gServerUrlField = new StringFieldEditor(GConfigPreferenceConstants.G_CONFIG_SERVER_EXECUTABLE_PATH, "G/On Configuration Service executable:", getFieldEditorParent());
		addField(gServerUrlField);

		StringFieldEditor gAdminServiceUrlField = new StringFieldEditor(GConfigPreferenceConstants.G_CONFIG_SERVER_HOST, "Service Host:", getFieldEditorParent());
		addField(gAdminServiceUrlField);

		StringFieldEditor gAdminServiceLocationField = new StringFieldEditor(GConfigPreferenceConstants.G_CONFIG_SERVER_PORT, "Service Port:", getFieldEditorParent());
		addField(gAdminServiceLocationField);

		IntegerFieldEditor gAdminServiceTimeoutField = new IntegerFieldEditor(GConfigPreferenceConstants.G_CONFIG_SERVER_TIMEOUT, "Service Timeout (minutes):", getFieldEditorParent());
		addField(gAdminServiceTimeoutField);
	
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
	}
}
