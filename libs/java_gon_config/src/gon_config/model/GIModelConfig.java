package gon_config.model;

import gon_client_management.model.ext.GIConfigPage;
import gon_config.service.GLocalServiceJob;

import java.util.List;

import com.giritech.server_config_ws.GOnSystemType;
import com.giritech.server_config_ws.LicenseInfoType;
import com.giritech.server_config_ws.ServiceType;

public interface GIModelConfig {
	public GLocalServiceJob getCreateGPMSJob();  
	public GLocalServiceJob getGenerateKnownSecretsJob();
	
	public boolean getConfiguredStatus();
	
	public GIConfigPage getFirstConfigPage();
	public GIConfigPage getNextConfigPage();
	public GLocalServiceJob getCreateFinaliseInstallationJob();
	public GLocalServiceJob getCreateDemoDataJob();
	public GLocalServiceJob getCreateInstallServicesJob();
	public GLocalServiceJob getGenerateSupportPackageJob();
	public ServiceType[] getServiceStatus();
	public Object[] getSystemStatus();
	public List<GOnSystemType> getSystemStatusForUpdate();
	GLocalServiceJob getCreateFinaliseUpgradeJob();
	GLocalServiceJob getCreateFinaliseChangeJob();
	GLocalServiceJob getCreatePrepareInstallationJob();
	GLocalServiceJob getCreatePrepareUpgradeJob(String systemName);
	GLocalServiceJob getCreatePrepareChangeJob();
	LicenseInfoType getLicenseStatus();
	public GIManagementServiceStatus getManagementServiceStatus();
	public boolean managementServiceCommand(String actionType);

	public static String STATE_RUNNING = "running";
	public static String STATE_NOT_RUNNING = "not_running";
	public static String STATE_SERVICE_NOT_FOUND = "service_not_found";
	public static String STATE_STOPPING = "stopping";
	public static String STATE_RESTARTING = "restarting";
	
	public static String ACTION_START = "start";
	public static String ACTION_RESTART = "restart";
	public static String ACTION_STOP = "stop";
	
	

}
