package gon_config.model;

public interface GIManagementServiceStatus {
	
	public String getId();
	
	public String getName();
	
	public String getStatus();

}
