package gon_config.model;

import gon_client_management.model.ext.GConfigTemplateUtil;
import gon_client_management.model.ext.GIConfigPage;
import gon_client_management.model.ext.GIConfigPanePage;
import gon_client_management.model.ext.GIFieldAction;
import gon_client_management.model.ext.GIDetails;
import gon_config.service.GLocalServiceFactory;
import gon_config.service.GLocalServiceJob;

import java.util.ArrayList;
import java.util.List;

import server_config_ws.TestConfigSpecificationResponse;

import com.giritech.admin_ws.types_config_template.ConfigurationTemplate;
import com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec;
import com.giritech.admin_ws.types_config_template.Details;
import com.giritech.admin_ws.types_config_template.ErrorLocationType;
import com.giritech.admin_ws.types_config_template.Test_config_action;
import com.giritech.admin_ws.types_config_template.Value_selection;
import com.giritech.server_config_ws.GOnSystemType;
import com.giritech.server_config_ws.LicenseInfoType;
import com.giritech.server_config_ws.SaveConfigTemplateResponseType;
import com.giritech.server_config_ws.ServiceType;

public class GConfigModel implements GIModelConfig {
	
	public GLocalServiceJob getCreateGPMSJob() {  
		return new GLocalServiceJob(new GLocalServiceJob.GIJobCaller() {
			public int callJob() {
				return GLocalServiceFactory.getLocalService().genereateGPMS();
			}
		});
		
	}

	@Override
	public GLocalServiceJob getCreateDemoDataJob() {
		return new GLocalServiceJob(new GLocalServiceJob.GIJobCaller() {
			public int callJob() {
				return GLocalServiceFactory.getLocalService().generateDemoData();
			}
		});
	}
	
	public GLocalServiceJob getGenerateKnownSecretsJob() {  
		return new GLocalServiceJob(new GLocalServiceJob.GIJobCaller() {
			public int callJob() {
				return GLocalServiceFactory.getLocalService().genereateKnownSecrets();
			}
		});
		
	}

	@Override
	public boolean getConfiguredStatus() {
		return GLocalServiceFactory.getLocalService().getConfiguredStatus();
	}
	
	@Override
	public LicenseInfoType getLicenseStatus() {
		return GLocalServiceFactory.getLocalService().getLicenseStatus();
	}
	

	@Override
	public ServiceType[] getServiceStatus() {
		return GLocalServiceFactory.getLocalService().getServiceStatus();
	}

	@Override
	public Object[] getSystemStatus() {
		GOnSystemType[] systemStatus = GLocalServiceFactory.getLocalService().getSystemStatus();
		ArrayList<GOnSystemType> systems = new ArrayList<GOnSystemType>();
		for (GOnSystemType gonSystem : systemStatus) {
			if (gonSystem.getIs_installation()) {
				systems.add(gonSystem);
			}
		}
		return systems.toArray();
		
	}
	
	@Override
	public List<GOnSystemType> getSystemStatusForUpdate() {
		GOnSystemType[] systemStatus = GLocalServiceFactory.getLocalService().getSystemStatus();
		ArrayList<GOnSystemType> upgradeSystems = new ArrayList<GOnSystemType>(); 
		for (GOnSystemType gonSystem : systemStatus) {
			if (gonSystem.getCan_be_used_for_upgrade()) {
				upgradeSystems.add(gonSystem);
			}
		}
		return upgradeSystems;
	}
	
	
	public class GDetails implements GIDetails {
		
		ConfigurationTemplateSpec[] editSpecs;
		ConfigurationTemplateSpec[] newSpec;
		private String title;
		private String internalName;
		private boolean editEnabled;
		private boolean deleteEnabled;
		
		
		GDetails(Details details) {
			editSpecs = details.getConfig_edit();
			if (editSpecs==null) 
				editSpecs = new ConfigurationTemplateSpec[0]; 
			newSpec = details.getConfig_new();
			internalName = details.getInternal_name();
			title = details.getTitle();
			editEnabled = details.getEdit_enabled();
			deleteEnabled = details.getDelete_enabled();
			
		}

		@Override
		public ConfigurationTemplateSpec[] getConfigSpecs() {
			return editSpecs;
		}

		@Override
		public ConfigurationTemplateSpec[] getCreateSpecs() {
			return newSpec;
		}
		
		@Override
		public boolean isCreateEnabled() {
			return newSpec!=null;
		}

		@Override
		public String getName() {
			return internalName;
		}

		@Override
		public String getTitle() {
			return title;
		}

		@Override
		public boolean isDeleteEnabled() {
			return deleteEnabled;
		}

		@Override
		public boolean isUpdateEnabled() {
			return editEnabled;
		}
		
		@Override
		public boolean isSingleton() {
			return !isCreateEnabled() && !isDeleteEnabled() && editSpecs.length==1;
		}
		


		@Override
		public void delete(int index) {
			if (index >= 0 && index < editSpecs.length) {
				
				boolean ok = GLocalServiceFactory.getLocalService().deleteConfigSpec(editSpecs[index]);
				if (ok) {
					synchronized (editSpecs) {
						if (editSpecs.length > 0) {
							ConfigurationTemplateSpec[] tmpSpec = new ConfigurationTemplateSpec[editSpecs.length-1];
							System.arraycopy(editSpecs, 0, tmpSpec, 0, index);
							System.arraycopy(editSpecs, index+1, tmpSpec, index, tmpSpec.length-index);
							editSpecs = tmpSpec;
						}
					}
				}
			}
			
		}


		public ConfigurationTemplateSpec getConfigSpec(int i) {
			if (i<0) {
				int newIndex = Math.abs(i)-1;
				return newSpec[newIndex];
			}
			else  {
				return editSpecs[i];
			}
		}

		
		@Override
		public GIConfigPage getConfigPage(int index) {
			ConfigurationTemplateSpec configSpec = getConfigSpec(index);
			return new GConfigTemplatePage(configSpec, this, index);
			
		}

		public void update(int index, ConfigurationTemplateSpec configSpec) {
			synchronized (editSpecs) {
				if (index<0) {
					ConfigurationTemplateSpec[] tmp = new ConfigurationTemplateSpec[editSpecs.length+1];
					System.arraycopy(editSpecs, 0, tmp, 0, editSpecs.length);
					tmp[editSpecs.length] = configSpec;
					editSpecs = tmp;
				}
				else {
					editSpecs[index] = configSpec;
				}
			}
			
		}

	}
	
	private class GConfigPage implements GIConfigPage {
		protected final GConfigTemplateUtil configTemplateUtil;
		boolean finished = false;
		protected ConfigurationTemplate configSpec;

		private GConfigPage(ConfigurationTemplate configSpec) {
			this.configTemplateUtil =  new GConfigTemplateUtil(configSpec, new GFieldAction(), createDetails(configSpec));
			this.configSpec = configSpec;
		}

		private List<GIDetails> createDetails(ConfigurationTemplate template) {
			 ArrayList<GIDetails> details = new ArrayList<GIDetails>();
			 if (template.getDetails()!=null) { 
				 for (Details detail : template.getDetails()) {
					GDetails gDetail = new GDetails(detail);
					details.add(gDetail);
				}
			 }
			return details;
		}


		@Override
		public GIConfigPanePage getPage() {
			return configTemplateUtil.getConfigPanePage();
		}

		@Override
		public String savePage() {
			configTemplateUtil.updateTemplateValues();
			final ErrorLocationType errorLocationType;
			try {
				errorLocationType = save();
			}
			catch(RuntimeException e) {
				return e.getMessage();
			}
			if (errorLocationType!=null) {
				return errorLocationType.getMessage();
			}
			else {
				finished = true;
				return null;
			}
		}


		protected ErrorLocationType save() {
			return GLocalServiceFactory.getLocalService().saveConfigSpec(configTemplateUtil.template);
		}

		@Override
		public boolean isFinished() {
			return finished;
		}
		
		@Override
		public String getType() {
			String entityType = configSpec.getEntity_type();
			if (entityType!=null)
				return entityType;
			return "";
		}
		
	}


	private final class GConfigTemplatePage extends GConfigPage {

		private ConfigurationTemplateSpec configTemplateSpec;
		private GDetails details;
		private int detailIndex;

		private GConfigTemplatePage(ConfigurationTemplateSpec configTemplateSpec, GDetails gDetails, int index) {
			super(GLocalServiceFactory.getLocalService().getConfigSpec(configTemplateSpec));
			this.configTemplateSpec = configTemplateSpec;
			this.details = gDetails;
			this.detailIndex = index;
		}
		
		protected ErrorLocationType save() {
			SaveConfigTemplateResponseType saveConfigResponse = GLocalServiceFactory.getLocalService().saveConfigSpec(configSpec, configTemplateSpec);
			if (saveConfigResponse.getRc()) {
				
				details.update(detailIndex, saveConfigResponse.getConfig_spec());
				return null;
			}
			else {
				return saveConfigResponse.getError_location();
			}
		}
		
		


	}

	private class GFieldAction implements GIFieldAction {

		@Override
		public Value_selection portScan(String name, List<String> portNumbers) {
			throw new RuntimeException("Not implemented");
		}

		@Override
		public TestConfigSpecificationResponse testConfigSpec(ConfigurationTemplate template, Test_config_action action) {
			return GLocalServiceFactory.getLocalService().testConfigSpec(template, action);
		}
		
		
	}
	
	
	@Override
	public GIConfigPage getFirstConfigPage() {
		final ConfigurationTemplate configSpecFirst = GLocalServiceFactory.getLocalService().getConfigSpecFirst();
		if (configSpecFirst==null)
			return null;
		return new GConfigPage(configSpecFirst);	
	}

	@Override
	public GIConfigPage getNextConfigPage() {
		ConfigurationTemplate configSpecNext = GLocalServiceFactory.getLocalService().getConfigSpecNext();
		if (configSpecNext!=null) {
			return new GConfigPage(configSpecNext);
		}
		return null;
	}

	@Override
	public GLocalServiceJob getCreatePrepareInstallationJob() {
		return new GLocalServiceJob(new GLocalServiceJob.GIJobCaller() {
			public int callJob() {
				return GLocalServiceFactory.getLocalService().prepareInstallation();
			}
		});
	}

	
	@Override
	public GLocalServiceJob getCreatePrepareUpgradeJob(final String systemName) {
		return new GLocalServiceJob(new GLocalServiceJob.GIJobCaller() {
			public int callJob() {
				return GLocalServiceFactory.getLocalService().prepareUpgrade(systemName);
			}
		});
	}

	@Override
	public GLocalServiceJob getCreatePrepareChangeJob() {
		return new GLocalServiceJob(new GLocalServiceJob.GIJobCaller() {
			public int callJob() {
				return GLocalServiceFactory.getLocalService().prepareChange();
			}
		});
	}
	
	@Override
	public GLocalServiceJob getCreateFinaliseInstallationJob() {
		return new GLocalServiceJob(new GLocalServiceJob.GIJobCaller() {
			public int callJob() {
				return GLocalServiceFactory.getLocalService().finaliseInstallation();
			}
		});
	}

	
	@Override
	public GLocalServiceJob getCreateFinaliseUpgradeJob() {
		return new GLocalServiceJob(new GLocalServiceJob.GIJobCaller() {
			public int callJob() {
				return GLocalServiceFactory.getLocalService().finaliseUpgrade();
			}
		});
	}

	@Override
	public GLocalServiceJob getCreateFinaliseChangeJob() {
		return new GLocalServiceJob(new GLocalServiceJob.GIJobCaller() {
			public int callJob() {
				return GLocalServiceFactory.getLocalService().finaliseChange();
			}
		});
	}
	
	@Override
	public GLocalServiceJob getCreateInstallServicesJob() {
		return new GLocalServiceJob(new GLocalServiceJob.GIJobCaller() {
			public int callJob() {
				return GLocalServiceFactory.getLocalService().installServices();
			}
		});
	}

	@Override
	public GLocalServiceJob getGenerateSupportPackageJob() {
		return new GLocalServiceJob(new GLocalServiceJob.GIJobCaller() {
			public int callJob() {
				return GLocalServiceFactory.getLocalService().generateSupportPackage();
			}
		});
	}

	

	@Override
	public GIManagementServiceStatus getManagementServiceStatus() {
		return GLocalServiceFactory.getLocalService().getManagementServiceStatus();
	}
	
	

	@Override
	public boolean managementServiceCommand(String actionType) {
		return GLocalServiceFactory.getLocalService().managementServiceCommand(actionType);
	}


	
	
}
