package gon_config;

import gon_client_management.ext.LaunchFailureDialog;
import gon_config.preferences.GConfigPreferenceConstants;

import org.eclipse.swt.widgets.Shell;

public class GConfigLaunchFailureDialog extends LaunchFailureDialog {

	public GConfigLaunchFailureDialog(Shell shell) {
		super(shell, "G/On Server Configuration");
	}

	@Override
	protected String getServerHost() {
		return GConfigActivator.getDefault().getPreferenceStore().getString(GConfigPreferenceConstants.G_CONFIG_SERVER_HOST);
		
	}

	@Override
	protected String getServerPort() {
		return GConfigActivator.getDefault().getPreferenceStore().getString(GConfigPreferenceConstants.G_CONFIG_SERVER_PORT);
	}

	@Override
	protected void setServerHost(String host) {
		GConfigActivator.getDefault().getPreferenceStore().setValue(GConfigPreferenceConstants.G_CONFIG_SERVER_HOST, host);

	}

	@Override
	protected void setServerPort(String port) {
		GConfigActivator.getDefault().getPreferenceStore().setValue(GConfigPreferenceConstants.G_CONFIG_SERVER_PORT, port);

	}

}
