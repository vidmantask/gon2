package gon_config.service;


public class GLocalServiceFactory {

	private static GILocalService service = null;
	
	public static GILocalService getLocalService() {
		try {
			if (service==null) {
				service = GLocalService.getLocalService();
			}
			return service;
		}
		catch (Throwable t) {
			throw new RuntimeException("Unable to connect to local service");
		}
	}

	/**
	 * @param server the server to set
	 */
	public static void setService(GILocalService service) {
		GLocalServiceFactory.service = service;
	}

	
}
