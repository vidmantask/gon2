package gon_config.service;


import gon_client_management.model.ext.GIJob;
import gon_client_management.model.ext.GIJobInfo;
import gon_client_management.model.ext.GJobInfoTypeWrapper;

public class GLocalServiceJob implements GIJob {

	public interface GIJobCaller {
		
		public int callJob();
		
	}
	
	
	private Integer jobId = null;
	private GIJobCaller caller;
	
	public GLocalServiceJob(GIJobCaller caller) {
		this.caller = caller;
	}
	
	public boolean cancel() {
		return GLocalServiceFactory.getLocalService().cancelJob(jobId);
	}

	public GIJobInfo getStatus() {
		return new GJobInfoTypeWrapper(GLocalServiceFactory.getLocalService().getJobInfo(jobId));
	}

	public void start() {
		jobId = caller.callJob();
	}

	public int getTotalWork() {
		return 100;
	}

}
