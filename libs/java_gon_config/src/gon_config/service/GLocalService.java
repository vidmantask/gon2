package gon_config.service;




import gon_client_management.ext.CommonUtils.ProcessHandler;
import gon_client_management.model.ext.GServiceTimeoutException;
import gon_config.GConfigActivator;
import gon_config.GConfigActivator.GIStopCallback;
import gon_config.model.GIManagementServiceStatus;
import gon_config.preferences.GConfigPreferenceConstants;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.SocketTimeoutException;
import java.rmi.RemoteException;
import java.util.ArrayList;

import org.apache.axiom.soap.SOAPFaultDetail;
import org.apache.axis2.AxisFault;

import server_config_ws.CancelJob;
import server_config_ws.CancelJobResponse;
import server_config_ws.DeleteConfigTemplate;
import server_config_ws.DeleteConfigTemplateResponse;
import server_config_ws.FaultServer;
import server_config_ws.GOnManagementServiceControl;
import server_config_ws.GOnManagementServiceControlResponse;
import server_config_ws.GOnManagementServiceGetStatus;
import server_config_ws.GOnManagementServiceGetStatusResponse;
import server_config_ws.GetConfigTemplate;
import server_config_ws.GetConfigTemplateResponse;
import server_config_ws.GetFirstConfigSpecification;
import server_config_ws.GetFirstConfigSpecificationResponse;
import server_config_ws.GetGOnSystems;
import server_config_ws.GetGOnSystemsResponse;
import server_config_ws.GetJobInfo;
import server_config_ws.GetJobInfoResponse;
import server_config_ws.GetLicenseInfo;
import server_config_ws.GetLicenseInfoResponse;
import server_config_ws.GetNextConfigSpecification;
import server_config_ws.GetNextConfigSpecificationResponse;
import server_config_ws.GetServices;
import server_config_ws.GetServicesResponse;
import server_config_ws.GetStatus;
import server_config_ws.GetStatusResponse;
import server_config_ws.Login;
import server_config_ws.LoginResponse;
import server_config_ws.Ping;
import server_config_ws.PingResponse;
import server_config_ws.ReloadConfigSpecification;
import server_config_ws.ReloadConfigSpecificationResponse;
import server_config_ws.SaveConfigSpecification;
import server_config_ws.SaveConfigSpecificationResponse;
import server_config_ws.SaveConfigTemplate;
import server_config_ws.SaveConfigTemplateResponse;
import server_config_ws.Server_config_wsService;
import server_config_ws.Server_config_wsServiceStub;
import server_config_ws.SetLicense;
import server_config_ws.SetLicenseResponse;
import server_config_ws.StartJobFinalizeChange;
import server_config_ws.StartJobFinalizeChangeResponse;
import server_config_ws.StartJobFinalizeInstallation;
import server_config_ws.StartJobFinalizeInstallationResponse;
import server_config_ws.StartJobFinalizeUpgrade;
import server_config_ws.StartJobFinalizeUpgradeResponse;
import server_config_ws.StartJobGenerateDemodata;
import server_config_ws.StartJobGenerateDemodataResponse;
import server_config_ws.StartJobGenerateGPMS;
import server_config_ws.StartJobGenerateGPMSResponse;
import server_config_ws.StartJobGenerateKnownsecrets;
import server_config_ws.StartJobGenerateKnownsecretsResponse;
import server_config_ws.StartJobGenerateSupportPackage;
import server_config_ws.StartJobGenerateSupportPackageResponse;
import server_config_ws.StartJobInstallServices;
import server_config_ws.StartJobInstallServicesResponse;
import server_config_ws.StartJobPrepareChange;
import server_config_ws.StartJobPrepareChangeResponse;
import server_config_ws.StartJobPrepareInstallation;
import server_config_ws.StartJobPrepareInstallationResponse;
import server_config_ws.StartJobPrepareUpgrade;
import server_config_ws.StartJobPrepareUpgradeResponse;
import server_config_ws.TestConfigSpecification;
import server_config_ws.TestConfigSpecificationResponse;

import com.giritech.admin_deploy_ws.types_job.JobInfoType;
import com.giritech.admin_ws.types_config_template.ConfigurationTemplate;
import com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec;
import com.giritech.admin_ws.types_config_template.ErrorLocationType;
import com.giritech.admin_ws.types_config_template.Test_config_action;
import com.giritech.server_config_ws.CancelJobRequestType;
import com.giritech.server_config_ws.DeleteConfigTemplateRequestType;
import com.giritech.server_config_ws.FaultServerElementType;
import com.giritech.server_config_ws.GOnManagementServiceControlRequestType;
import com.giritech.server_config_ws.GOnManagementServiceGetStatusRequestType;
import com.giritech.server_config_ws.GOnManagementServiceGetStatusResponseType;
import com.giritech.server_config_ws.GOnSystemType;
import com.giritech.server_config_ws.GetConfigTemplateRequestType;
import com.giritech.server_config_ws.GetFirstConfigSpecificationRequestType;
import com.giritech.server_config_ws.GetGOnSystemsRequestType;
import com.giritech.server_config_ws.GetJobInfoRequestType;
import com.giritech.server_config_ws.GetLicenseInfoRequestType;
import com.giritech.server_config_ws.GetNextConfigSpecificationRequestType;
import com.giritech.server_config_ws.GetServicesRequestType;
import com.giritech.server_config_ws.GetStatusRequestType;
import com.giritech.server_config_ws.LicenseInfoType;
import com.giritech.server_config_ws.LoginRequestType;
import com.giritech.server_config_ws.PingRequestType;
import com.giritech.server_config_ws.ReloadConfigSpecificationRequestType;
import com.giritech.server_config_ws.SaveConfigSpecificationRequestType;
import com.giritech.server_config_ws.SaveConfigTemplateRequestType;
import com.giritech.server_config_ws.SaveConfigTemplateResponseType;
import com.giritech.server_config_ws.ServiceType;
import com.giritech.server_config_ws.SetLicenseRequestType;
import com.giritech.server_config_ws.StartJobFinalizeChangeRequestType;
import com.giritech.server_config_ws.StartJobFinalizeInstallationRequestType;
import com.giritech.server_config_ws.StartJobFinalizeUpgradeRequestType;
import com.giritech.server_config_ws.StartJobGenerateDemodataRequestType;
import com.giritech.server_config_ws.StartJobGenerateGPMSRequestType;
import com.giritech.server_config_ws.StartJobGenerateKnownsecretsRequestType;
import com.giritech.server_config_ws.StartJobGenerateSupportPackageRequestType;
import com.giritech.server_config_ws.StartJobInstallServicesRequestType;
import com.giritech.server_config_ws.StartJobPrepareChangeRequestType;
import com.giritech.server_config_ws.StartJobPrepareInstallationRequestType;
import com.giritech.server_config_ws.StartJobPrepareUpgradeRequestType;
import com.giritech.server_config_ws.TestConfigSpecificationRequestType;


public class GLocalService implements GILocalService, GIStopCallback {


	private static final String ARG_CONFIG_SERVICE_PATH = "-gon_config_service_path";
	private static final String ARG_CONFIG_SERVICE_WORKING_FOLDER = "-gon_config_service_working_folder";

	private static String serviceWorkingFolder;
	private static String servicePath; 
	
	private String admin_deploy_ws_url;
	private long ws_timeout_min;
	String sessionID;
	Server_config_wsService service;

	private GLocalService(String admin_deploy_ws_url, long ws_timeout_min) {
		this.admin_deploy_ws_url = admin_deploy_ws_url;
		this.ws_timeout_min = ws_timeout_min;
		
		GConfigActivator.getDefault().addStopCallback(this);
		
		connect();
	}
	
	@SuppressWarnings("serial") class ErrorConnectionException extends RuntimeException {
		
		ErrorConnectionException(RemoteException e) {
			super("Error connecting to local service - please check local service settings in preferences are correct and restart the client");
			if (e instanceof AxisFault) {
				AxisFault e1 = (AxisFault) e;
				SOAPFaultDetail faultDetailElement = e1.getFaultDetailElement();
				if (faultDetailElement!=null) {
					String text3 = faultDetailElement.toString();
					GConfigActivator.getLogger().logError(text3);
				}
			}
			GConfigActivator.getLogger().logException(e);
		}
	}
	
	private RuntimeException handleRemoteException(RemoteException e) {
		
		if (e.getCause() instanceof SocketTimeoutException) {
			GConfigActivator.getLogger().logException(e);
			synchronized(this) {
				try {
					this.wait(this.ws_timeout_min * 60 * 1000);
				} catch (InterruptedException e1) {
					// ignore
				}
			}
			return new GServiceTimeoutException(e);
		}
		else {
			return new ErrorConnectionException(e);
		}
	}
	
	private void connect() {
		
    	Server_config_wsServiceStub admin_deploy_ws_service = null;
		try {
			admin_deploy_ws_service = new Server_config_wsServiceStub(admin_deploy_ws_url);
		} catch (AxisFault e) {
			throw new RuntimeException(e);
		}

        /*
         * This is needed because the py-soap server need content length of requests,
         * and this is enabled if CHUNKED is disabled.
         */
        admin_deploy_ws_service._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, "false");
        
        /*
         * Increase timeout
         * 
         */
        long timeout_ms = ws_timeout_min * 60 * 1000; 
        admin_deploy_ws_service._getServiceClient().getOptions().setTimeOutInMilliSeconds(timeout_ms);
        
        
        Login login_request = new Login();
        LoginRequestType login_request_content = new LoginRequestType();
        login_request_content.setPassword("Hej");
        login_request_content.setUsername("Davsxxx");
        login_request.setContent(login_request_content);
        

        LoginResponse login_response;
		try {
			login_response = admin_deploy_ws_service.Login(login_request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
        this.sessionID = login_response.getContent().getSession_id();
        this.service = admin_deploy_ws_service;
	}
	
	
	void handleFaultServer(FaultServer e) {
		GConfigActivator.getLogger().logException(e);
		final FaultServerElementType faultMessage = e.getFaultMessage();
		if (faultMessage!=null)
			throw new RuntimeException(faultMessage.getFaultServerElementType());
		else 
			throw new RuntimeException(e);
	}
	

	private static GILocalService server_instance = null;
	private static ProcessHandler processHandler = null;
	/**
	 * The default server string is "https://127.0.0.1:8080"
	 * 
	 * @return A new GIServer instance
	 */
	public synchronized static GILocalService getLocalService() {
		String serverUrl = "https://" +
			GConfigActivator.getDefault().getPreferenceStore().getString(GConfigPreferenceConstants.G_CONFIG_SERVER_HOST) + 
			":" + 
			GConfigActivator.getDefault().getPreferenceStore().getString(GConfigPreferenceConstants.G_CONFIG_SERVER_PORT);
		
		final int timeout_minutes = GConfigActivator.getDefault().getPreferenceStore().getInt(GConfigPreferenceConstants.G_CONFIG_SERVER_TIMEOUT);
		if (server_instance == null) {
			try {
				server_instance = new GLocalService(serverUrl, timeout_minutes);
			}
			catch(RuntimeException t) {
				// ignore
			}
		}
		if (server_instance != null) {
			if (!((GLocalService) server_instance).ping())
				server_instance = null;
		}
		if (server_instance == null) {
			startLocalService();
			//String serverUrl = "https://127.0.0.1:8082";
			int connect_attempts = 0;
			RuntimeException error = null;
			while(connect_attempts < 2) {
				myWait(100*(connect_attempts+1));
				try {
					server_instance = new GLocalService(serverUrl, 5);
					if (((GLocalService) server_instance).ping())
						return server_instance;
				}
				catch(RuntimeException t) {
					error = t;
				}
				connect_attempts++;
			}
			if (error!=null)
				throw error;
			else
				throw new RuntimeException("Could not contact server");
		}
		return server_instance;
	}
	
	private synchronized static void myWait(int timeout) {
		try {
			Thread.sleep(timeout);
		} catch (InterruptedException e) {
		}
		
	}

	@Override
	public boolean ping() {
		Ping request = new Ping();
		PingRequestType requestContent = new PingRequestType();
		requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
        PingResponse response;
        try {
			response = this.service.Ping(request);
		} catch (RemoteException e) {
			GConfigActivator.getLogger().logException(e);
			return false;
		} catch (FaultServer e) {
			GConfigActivator.getLogger().logException(e);
			return false;
		}
		return response.getContent().getRc();
		
	}
	
	
	@SuppressWarnings("unused")
	private static void printChildOutput(Process child) {
		InputStream out = child.getInputStream();
		InputStreamReader gcdReader = new InputStreamReader(out);
		BufferedReader in = new BufferedReader(gcdReader);
		String line;
		try {
			while ((line = in.readLine()) != null) {
				System.out.println(line);
			}
		} catch (IOException e) {
		}
	}

	public synchronized static void stopLocalService()  {
		if (processHandler!=null) {
			processHandler.stop();
		}
	}
	
	
	public static void setArgs(String[] args) {
		int servicePathIndex = getArgumentIndex(ARG_CONFIG_SERVICE_PATH, args);
		if (servicePathIndex < 0) {
			servicePath = GConfigActivator.getDefault().getPreferenceStore().getString(GConfigPreferenceConstants.G_CONFIG_SERVER_EXECUTABLE_PATH);
		}
		else {
			servicePath = args[servicePathIndex];
//			File f = new File(servicePath);
//			if (!f.exists())
//				throw new RuntimeException("Specified config service executable '"  +  servicePath + "' does not exist");
		}
		int serviceWorkingFolderIndex = getArgumentIndex(ARG_CONFIG_SERVICE_WORKING_FOLDER, args);
		
		if (serviceWorkingFolderIndex < 0) {
			File f = new File(servicePath);
			serviceWorkingFolder = f.getParent();
		}
		else {
			serviceWorkingFolder = args[serviceWorkingFolderIndex];
		}
		File d = new File(serviceWorkingFolder);
		serviceWorkingFolder = d.getAbsolutePath();
	}

	private static int getArgumentIndex(String argName, String[] args) {
		for (int i=0; i<args.length; i++) {
			String arg = args[i];
			if (arg.equals(argName) && (i+1) < args.length) {
				return i+1;
			}
		}
		return -1;
	}
	
	private static void startLocalService() {
		stopLocalService();
		//String localServiceCmd = "../gon_config_service/win/gon_config_service.exe --serve --ip 127.0.0.1 --port 8082";
		if (servicePath==null) 
			setArgs(new String[0]);
		
		ArrayList<String> cmdList = new ArrayList<String>();
		cmdList.add(servicePath);
		cmdList.add("--serve");
		cmdList.add("--ip");
		cmdList.add(GConfigActivator.getDefault().getPreferenceStore().getString(GConfigPreferenceConstants.G_CONFIG_SERVER_HOST));
		cmdList.add("--port");
		cmdList.add(GConfigActivator.getDefault().getPreferenceStore().getString(GConfigPreferenceConstants.G_CONFIG_SERVER_PORT));
		cmdList.add("--instance_run_root");
		cmdList.add(serviceWorkingFolder);
		
		String [] cmdArray = new String[cmdList.size()];
		for(int i=0; i<cmdList.size(); i++) {
			cmdArray[i] = cmdList.get(i);
		}
		
		String localServiceCmd = "\"" + servicePath + "\""+
			" --serve" +
			" --ip " + GConfigActivator.getDefault().getPreferenceStore().getString(GConfigPreferenceConstants.G_CONFIG_SERVER_HOST) + 
			" --port " + GConfigActivator.getDefault().getPreferenceStore().getString(GConfigPreferenceConstants.G_CONFIG_SERVER_PORT) +
			" --instance_run_root " + "\"" + serviceWorkingFolder + "\"";
		
		if (processHandler==null) {
			processHandler = new ProcessHandler(cmdArray, null, true, GConfigActivator.getLogger());
		}
		else {
			processHandler.setCommand(localServiceCmd);
		}
		processHandler.start();
	}
	
	public int genereateKnownSecrets() {
		StartJobGenerateKnownsecrets request = new StartJobGenerateKnownsecrets();
		StartJobGenerateKnownsecretsRequestType requestContent = new StartJobGenerateKnownsecretsRequestType();
        requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
        StartJobGenerateKnownsecretsResponse response;
        try {
			response = this.service.StartJobGenerateKnownsecrets(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getJob_id().intValue();
		
	}

	public int genereateGPMS() {
		StartJobGenerateGPMS request = new StartJobGenerateGPMS();
		StartJobGenerateGPMSRequestType requestContent = new StartJobGenerateGPMSRequestType();
        requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
        StartJobGenerateGPMSResponse response;
        try {
			response = this.service.StartJobGenerateGPMS(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getJob_id().intValue();
		
	}

	public int generateDemoData() {
		StartJobGenerateDemodata request = new StartJobGenerateDemodata();
		StartJobGenerateDemodataRequestType requestContent = new StartJobGenerateDemodataRequestType();
        requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
        StartJobGenerateDemodataResponse response;
        try {
			response = this.service.StartJobGenerateDemodata(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getJob_id().intValue();
	}

	public int generateSupportPackage() {
		StartJobGenerateSupportPackage request = new StartJobGenerateSupportPackage();
		StartJobGenerateSupportPackageRequestType requestContent = new StartJobGenerateSupportPackageRequestType();
        requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
        StartJobGenerateSupportPackageResponse response;
        try {
			response = this.service.StartJobGenerateSupportPackage(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getJob_id().intValue();
		
	}
	
	public int installServices() {
		StartJobInstallServices request = new StartJobInstallServices();
		StartJobInstallServicesRequestType requestContent = new StartJobInstallServicesRequestType();
        requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
        StartJobInstallServicesResponse response;
        try {
			response = this.service.StartJobInstallServices(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getJob_id().intValue();
		
	}

	public int prepareInstallation() {
		StartJobPrepareInstallation request = new StartJobPrepareInstallation();
		StartJobPrepareInstallationRequestType requestContent = new StartJobPrepareInstallationRequestType();
        requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
        StartJobPrepareInstallationResponse response;
        try {
			response = this.service.StartJobPrepareInstallation(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getJob_id().intValue();
		
	}
	
	
	public int finaliseInstallation() {
		StartJobFinalizeInstallation request = new StartJobFinalizeInstallation();
		StartJobFinalizeInstallationRequestType requestContent = new StartJobFinalizeInstallationRequestType();
        requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
        StartJobFinalizeInstallationResponse response;
        try {
			response = this.service.StartJobFinalizeInstallation(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getJob_id().intValue();
		
	}

	public int prepareUpgrade(String systemName) {
		StartJobPrepareUpgrade request = new StartJobPrepareUpgrade();
		StartJobPrepareUpgradeRequestType requestContent = new StartJobPrepareUpgradeRequestType();
        requestContent.setSession_id(this.sessionID);
        requestContent.setSystem_name(systemName);
        request.setContent(requestContent);
        StartJobPrepareUpgradeResponse response;
        try {
			response = this.service.StartJobPrepareUpgrade(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getJob_id().intValue();
		
	}
	
	public int finaliseUpgrade() {
		StartJobFinalizeUpgrade request = new StartJobFinalizeUpgrade();
		StartJobFinalizeUpgradeRequestType requestContent = new StartJobFinalizeUpgradeRequestType();
        requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
        StartJobFinalizeUpgradeResponse response;
        try {
			response = this.service.StartJobFinalizeUpgrade(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getJob_id().intValue();
		
	}

	public int prepareChange() {
		StartJobPrepareChange request = new StartJobPrepareChange();
		StartJobPrepareChangeRequestType requestContent = new StartJobPrepareChangeRequestType();
        requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
        StartJobPrepareChangeResponse response;
        try {
			response = this.service.StartJobPrepareChange(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getJob_id().intValue();
		
	}
	
	public int finaliseChange() {
		StartJobFinalizeChange request = new StartJobFinalizeChange();
		StartJobFinalizeChangeRequestType requestContent = new StartJobFinalizeChangeRequestType();
        requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
        StartJobFinalizeChangeResponse response;
        try {
			response = this.service.StartJobFinalizeChange(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getJob_id().intValue();
		
	}
	
	public JobInfoType getJobInfo(int jobId) {
		GetJobInfo request = new GetJobInfo();
		GetJobInfoRequestType requestContent = new GetJobInfoRequestType();
        requestContent.setSession_id(this.sessionID);
        requestContent.setJob_id(BigInteger.valueOf(jobId));
        request.setContent(requestContent);
        GetJobInfoResponse response;
        try {
			response = this.service.GetJobInfo(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getJob_info();
		
	}
	
	public boolean cancelJob(int jobId) {
		CancelJob request = new CancelJob();
		CancelJobRequestType requestContent = new CancelJobRequestType();
        requestContent.setSession_id(this.sessionID);
        requestContent.setJob_id(BigInteger.valueOf(jobId));
        request.setContent(requestContent);
        CancelJobResponse response;
        try {
			response = this.service.CancelJob(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getRc();
		
	}
	
	public boolean getConfiguredStatus() {
		GetStatus request = new GetStatus();
		GetStatusRequestType requestContent = new GetStatusRequestType();
        requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
        GetStatusResponse response;
        try {
			response = this.service.GetStatus(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getConfigured();
		
	}
	
	public LicenseInfoType getLicenseStatus() {
		GetLicenseInfo request = new GetLicenseInfo();
		GetLicenseInfoRequestType requestContent = new GetLicenseInfoRequestType();
        requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
        GetLicenseInfoResponse response;
        try {
			response = this.service.GetLicenseInfo(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getLicense_info();
		
	}
	
	public boolean setLicense(String content) {
		SetLicense request = new SetLicense();
		SetLicenseRequestType requestContent = new SetLicenseRequestType();
        requestContent.setSession_id(this.sessionID);
        requestContent.setLicense(content);
        request.setContent(requestContent);
        SetLicenseResponse response;
        try {
			response = this.service.SetLicense(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getRc();
		
	}
	

	public ServiceType[] getServiceStatus() {
		GetServices request = new GetServices();
		GetServicesRequestType requestContent = new GetServicesRequestType();
        requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
        GetServicesResponse response;
        try {
			response = this.service.GetServices(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		ServiceType[] services = response.getContent().getServices();
		if (services==null)
			services = new ServiceType[0];
		return services;
		
	}

	public GOnSystemType[] getSystemStatus() {
		GetGOnSystems request = new GetGOnSystems();
		GetGOnSystemsRequestType requestContent = new GetGOnSystemsRequestType();
        requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
        GetGOnSystemsResponse response;
        try {
			response = this.service.GetGOnSystems(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		GOnSystemType[] gonSystems = response.getContent().getGon_systems();
		if (gonSystems==null)
			gonSystems = new GOnSystemType[0];
		return gonSystems;
		
	}

	public ConfigurationTemplate getConfigSpecFirst() {
		GetFirstConfigSpecification request = new GetFirstConfigSpecification();
		GetFirstConfigSpecificationRequestType requestContent = new GetFirstConfigSpecificationRequestType();
        requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
        GetFirstConfigSpecificationResponse response;
        try {
			response = this.service.GetFirstConfigSpecification(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getConfig_specification();
		
	}
	
	public ConfigurationTemplate getConfigSpecNext() {
		GetNextConfigSpecification request = new GetNextConfigSpecification();
		GetNextConfigSpecificationRequestType requestContent = new GetNextConfigSpecificationRequestType();
        requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
        GetNextConfigSpecificationResponse response;
        try {
			response = this.service.GetNextConfigSpecification(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getConfig_specification();
		
	}

	@Override
	public ConfigurationTemplate reloadConfigSpec() {
		ReloadConfigSpecification request = new ReloadConfigSpecification();
		ReloadConfigSpecificationRequestType requestContent = new ReloadConfigSpecificationRequestType();
        requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
        ReloadConfigSpecificationResponse response;
        try {
			response = this.service.ReloadConfigSpecification(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getConfig_specification();
		
	}
	
	@Override
	public ErrorLocationType saveConfigSpec(ConfigurationTemplate template) {
		SaveConfigSpecification request = new SaveConfigSpecification();
		SaveConfigSpecificationRequestType requestContent = new SaveConfigSpecificationRequestType();
        requestContent.setSession_id(this.sessionID);
        requestContent.setConfig_specification(template);
        request.setContent(requestContent);
        SaveConfigSpecificationResponse response;
        try {
			response = this.service.SaveConfigSpecification(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		boolean ok = response.getContent().getRc();
		if (!ok) {
			return response.getContent().getError_location();
		}
		else
			return null;
		
	}
	
	@Override
	public ConfigurationTemplate getConfigSpec(ConfigurationTemplateSpec spec) {
		GetConfigTemplate request = new GetConfigTemplate();
		GetConfigTemplateRequestType requestContent = new GetConfigTemplateRequestType();
        requestContent.setSession_id(this.sessionID);
        requestContent.setConfig_template_spec(spec);
        request.setContent(requestContent);
        GetConfigTemplateResponse response;
        try {
			response = this.service.GetConfigTemplate(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getConfig_template();
		
	}
	

	@Override
	public SaveConfigTemplateResponseType saveConfigSpec(ConfigurationTemplate template, ConfigurationTemplateSpec spec) {
		SaveConfigTemplate request = new SaveConfigTemplate();
		SaveConfigTemplateRequestType requestContent = new SaveConfigTemplateRequestType();
        requestContent.setSession_id(this.sessionID);
        requestContent.setConfig_template(template);
        requestContent.setEntity_type(spec.getEntity_type());
        requestContent.setElement_id(spec.getId());
        requestContent.setSub_type(spec.getSub_type());
        request.setContent(requestContent);
        SaveConfigTemplateResponse response;
        try {
			response = this.service.SaveConfigTemplate(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent();
		
	}

	@Override
	public boolean deleteConfigSpec(ConfigurationTemplateSpec spec) {
		DeleteConfigTemplate request = new DeleteConfigTemplate();
		DeleteConfigTemplateRequestType requestContent = new DeleteConfigTemplateRequestType();
        requestContent.setSession_id(this.sessionID);
        requestContent.setConfig_spec(spec);
        request.setContent(requestContent);
        DeleteConfigTemplateResponse response;
        try {
			response = this.service.DeleteConfigTemplate(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getRc();
		
	}
	
	public TestConfigSpecificationResponse testConfigSpec(ConfigurationTemplate template, Test_config_action action) {
		TestConfigSpecification request = new TestConfigSpecification();
		TestConfigSpecificationRequestType requestContent = new TestConfigSpecificationRequestType();
        requestContent.setSession_id(this.sessionID);
        requestContent.setConfig_specification(template);
        requestContent.setModule_id(action.getModule_id());
        requestContent.setSub_type(action.getSub_type());
        requestContent.setTest_type(action.getTest_type());
        request.setContent(requestContent);
        try {
			return this.service.TestConfigSpecification(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public GIManagementServiceStatus getManagementServiceStatus() {
		GOnManagementServiceGetStatus request = new GOnManagementServiceGetStatus();
		GOnManagementServiceGetStatusRequestType requestContent = new GOnManagementServiceGetStatusRequestType();
        requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
        GOnManagementServiceGetStatusResponse response;
        try {
			response = this.service.GOnManagementServiceGetStatus(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		final GOnManagementServiceGetStatusResponseType responsContent = response.getContent();
		return new GIManagementServiceStatus() {
			
			@Override
			public String getStatus() {
				return responsContent.getStatus();
			}
			
			@Override
			public String getName() {
				return responsContent.getName();
			}
			
			@Override
			public String getId() {
				return responsContent.getId();
			}
		};
		
	}
	
	@Override
	public boolean managementServiceCommand(String actionType) {
		GOnManagementServiceControl request = new GOnManagementServiceControl();
		GOnManagementServiceControlRequestType requestContent = new GOnManagementServiceControlRequestType();
        requestContent.setSession_id(this.sessionID);
        requestContent.setCommand(actionType);
        request.setContent(requestContent);
        GOnManagementServiceControlResponse response;
        try {
			response = this.service.GOnManagementServiceControl(request);
		} catch (RemoteException e) {
			throw handleRemoteException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getRc();
	}


	@Override
	public void stop() {
		stopLocalService();
		
	}
	
	
}
