package gon_config.service;

import gon_config.model.GIManagementServiceStatus;
import server_config_ws.TestConfigSpecificationResponse;

import com.giritech.admin_deploy_ws.types_job.JobInfoType;
import com.giritech.admin_ws.types_config_template.ConfigurationTemplate;
import com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec;
import com.giritech.admin_ws.types_config_template.ErrorLocationType;
import com.giritech.admin_ws.types_config_template.Test_config_action;
import com.giritech.server_config_ws.GOnSystemType;
import com.giritech.server_config_ws.LicenseInfoType;
import com.giritech.server_config_ws.SaveConfigTemplateResponseType;
import com.giritech.server_config_ws.ServiceType;

public interface GILocalService {

	public int genereateKnownSecrets();
	
	public int genereateGPMS();
	
	public int generateDemoData();	
	
	public int generateSupportPackage();
	
	public JobInfoType getJobInfo(int jobId);
	
	public boolean cancelJob(int jobId);

	
	public boolean getConfiguredStatus();

	public LicenseInfoType getLicenseStatus();
	
	public ServiceType[] getServiceStatus();
	
	public GOnSystemType[] getSystemStatus();	
	
	public ConfigurationTemplate getConfigSpecNext();
	
	public ConfigurationTemplate getConfigSpecFirst();
	
	public ErrorLocationType saveConfigSpec(ConfigurationTemplate template);

	public TestConfigSpecificationResponse testConfigSpec(ConfigurationTemplate template, Test_config_action action);
	
	public int prepareInstallation();
	public int finaliseInstallation();

	public int prepareUpgrade(String systemName);
	public int finaliseUpgrade();

	public int prepareChange();
	public int finaliseChange();
	
	public int installServices();

	public boolean setLicense(String content);

	boolean ping();

	GIManagementServiceStatus getManagementServiceStatus();

	boolean managementServiceCommand(String actionType);

	ConfigurationTemplate getConfigSpec(ConfigurationTemplateSpec spec);

	SaveConfigTemplateResponseType saveConfigSpec(ConfigurationTemplate template, ConfigurationTemplateSpec spec);

	ConfigurationTemplate reloadConfigSpec();

	boolean deleteConfigSpec(ConfigurationTemplateSpec spec);	
}
