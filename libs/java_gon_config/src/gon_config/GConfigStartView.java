package gon_config;

import gon_client_management.model.ext.GIJobInfo;
import gon_client_management.view.ext.GJobHandler;
import gon_client_management.view.ext.GSafeView;
import gon_config.install_wizard.GChangeWizard;
import gon_config.install_wizard.GCreateGPMWizardPage;
import gon_config.install_wizard.GSinglePageWizard;
import gon_config.install_wizard.InstallWizardDialog;
import gon_config.model.GConfigModelFactory;
import gon_config.model.GIManagementServiceStatus;
import gon_config.model.GIModelConfig;
import gon_config.service.GLocalServiceJob;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import com.giritech.admin_deploy_ws.types_job.JobInfoType;
import com.giritech.admin_deploy_ws.types_job.Job_status_type0;
import com.giritech.server_config_ws.LicenseInfoType;

public class GConfigStartView extends GSafeView {

	private GIModelConfig modelApi;

	public GConfigStartView() {
		super(GConfigActivator.getLogger(), null);
		modelApi = GConfigModelFactory.getConfigModel();

		/** Running State **/
		JFaceResources.getImageRegistry().put("G_RUNNING_STATE_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("java_gon_config", "icons/g_running_state_man_icon_16x16.png").createImage());
		/** Stop State **/
		JFaceResources.getImageRegistry().put("G_STOP_STATE_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("java_gon_config", "icons/g_stop_state_man_icon_16x16.png").createImage());
		/** Restart State **/
		JFaceResources.getImageRegistry().put("G_RESTART_STATE_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("java_gon_config", "icons/g_restart_state_man_icon_16x16.png").createImage());
		/** Unknown State **/
		JFaceResources.getImageRegistry().put("G_UNKNOWN_STATE_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("java_gon_config", "icons/g_unknown_state_man_icon_16x16.png").createImage());

		/** Stop Action **/
		JFaceResources.getImageRegistry().put("G_STOP_ACTION_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("java_gon_config", "icons/g_stop_action_man_icon_16x16.png").createImage());
		/** Restart Action **/
		JFaceResources.getImageRegistry().put("G_RESTART_ACTION_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("java_gon_config", "icons/g_restart_action_man_icon_16x16.png").createImage());
		/** Refresh Action **/
		JFaceResources.getImageRegistry().put("G_REFRESH_ACTION_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("java_gon_config", "icons/g_refresh_action_man_icon_16x16.png").createImage());

	}

	public static final String ID = "gon_config.view";
	private Form statusForm;
	protected boolean complete = false;
	private Label licenseNumberText;
	private Label licenseExpirationText;
	private Label licenseOwnerText;
	private Action refreshAction;
	private ChooseLicenseFile chooseLicenseFileAction;
	private Composite licensesContainer;
	private Label managementServiceName;
	private Label managementStatusImage;
	private Label managementStatusText;
	private Button startButton;
	private Button restartButton;
	private Button stopButton;
	private ArrayList<Button> configuredDependentButtons = new ArrayList<Button>();
	private Composite infoContainer;

	public Image getBoolImage(boolean b) {
		if (!b) {
			return PlatformUI.getWorkbench().getSharedImages().getImage(
					ISharedImages.IMG_OBJS_ERROR_TSK);
		}
		else {
			if( JFaceResources.getImageRegistry().getDescriptor("G_SERVICE_ACTIVE_ICON") == null ) {
				JFaceResources.getImageRegistry().put("G_SERVICE_ACTIVE_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("java_gon_config", "icons/g_service_active.png").createImage());
			}
			return JFaceResources.getImageRegistry().get("G_SERVICE_ACTIVE_ICON");
		}
	}

	class ServiceJob extends Job {

		private String endState;
		private final String actionType;
		private String state;
		private int totalJobTime;

		public ServiceJob(String actionType) {
			super(actionType);
			this.actionType = actionType;
			setUser(true);
			if (actionType.equals(GIModelConfig.ACTION_RESTART)) {
				endState = GIModelConfig.STATE_RUNNING;
				setName("Restarting service");
				totalJobTime = 20;
			}
			else if (actionType.equals(GIModelConfig.ACTION_START)) {
				endState = GIModelConfig.STATE_RUNNING;
				setName("Starting service");
				totalJobTime = 10;
			}
			else if (actionType.equals(GIModelConfig.ACTION_STOP)) {
				endState = GIModelConfig.STATE_NOT_RUNNING;
				setName("Stopping service");
				totalJobTime = 6;
			}
		}

		@Override
		protected IStatus run(IProgressMonitor monitor) {
			final String returnStatus;
			boolean ok = modelApi.managementServiceCommand(actionType);
			if (ok) {
				monitor.beginTask(getName(), totalJobTime);
				state = null;
				int waitingSeconds = 0;
				int interval = 2;
				do {
					try {
						synchronized(this) {
							wait(interval*1000);
						}
					} catch (InterruptedException e) {
					}
					waitingSeconds += interval;
					monitor.worked(interval);
					state = getServiceStatus();
					getSite().getShell().getDisplay().asyncExec(new Runnable() {

						@Override
						public void run() {
							updateServiceStatus(state);
						}

					});

				} while (!endState.equals(state) && waitingSeconds < totalJobTime);
				monitor.done();
				if (endState.equals(state))
					returnStatus = null;
				else
					returnStatus = "Timeout waiting for service update";
			}
			else {
				returnStatus = "Error updating service - check server log for details";
			}
			getSite().getShell().getDisplay().asyncExec(new Runnable() {

				@Override
				public void run() {
					if (returnStatus!=null)
						MessageDialog.openWarning(getSite().getShell(), getName(), returnStatus);
					refresh();
				}

			});
			return Status.OK_STATUS;
		}

	}

	private void createStatusView(final Composite parent) {

		/** Create a nice full width header. */
		FormToolkit toolkit = new FormToolkit(parent.getDisplay());
		statusForm = toolkit.createForm(parent);
		statusForm.setText("Configuration Status");
		statusForm.updateToolBar();
		toolkit.decorateFormHeading(statusForm);

		/** Create layout for the main form. */
		GridLayout layout = new GridLayout();
		statusForm.getBody().setLayout(layout);


		/** Create a section for the services table. */
		Section servicesSection = toolkit.createSection(statusForm.getBody(), Section.TITLE_BAR);
		servicesSection.setText("G/On Management Service");

		/** Create a layout for this section. */
		servicesSection.setLayout(new GridLayout());
		servicesSection.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		Composite servicesContainer = toolkit.createComposite(servicesSection);
		servicesSection.setClient(servicesContainer);
		servicesContainer.setLayout(new GridLayout());

		infoContainer = toolkit.createComposite(servicesContainer);
		infoContainer.setLayout(new GridLayout(1, false));
		infoContainer.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));


		final Table table = new Table(infoContainer, SWT.BORDER);
		table.setLinesVisible(false);
		table.setHeaderVisible(true);
		TableItem tableItem = new TableItem(table, SWT.NONE);
//		table.addListener(SWT.MeasureItem, new Listener() {
//			   public void handleEvent(Event event) {
//			      event.height = 30;
//			   }
//		});

		String[] titles = { "Service name", "State", "Start", "Restart", "Stop", "Update"};
		int[] widths = { 200, 120, 80, 80, 80, 120};
		for (int i = 0; i < titles.length; i++) {
		      TableColumn column = new TableColumn(table, SWT.CENTER);
		      column.setWidth(widths[i]);
		      column.setText(titles[i]);
		 }

		managementServiceName = toolkit.createLabel(table, "Service Name");
		{
			TableEditor editor = new TableEditor(table);
			editor.grabHorizontal = true;
			editor.setEditor(managementServiceName, tableItem, 0);
		}

		Composite statusComposite = toolkit.createComposite(table);
		GridLayout gridLayout = new GridLayout(2, false);
//		gridLayout.horizontalSpacing = 0;
		gridLayout.verticalSpacing = 0;
		gridLayout.marginHeight = 0;
		gridLayout.marginBottom = 0;
//		gridLayout.marginLeft = 0;
//		gridLayout.marginRight = 0;
//		gridLayout.marginWidth = 0;
		statusComposite.setLayout(gridLayout);

		managementStatusImage = toolkit.createLabel(statusComposite, "");
		managementStatusImage.setImage(JFaceResources.getImageRegistry().get("G_UNKNOWN_STATE_ICON"));
		managementStatusText = toolkit.createLabel(statusComposite, "Status");
		{
			TableEditor editor = new TableEditor(table);
			editor.grabHorizontal = true;
		    editor.horizontalAlignment = SWT.LEFT;
			editor.setEditor(statusComposite, tableItem, 1);
		}

		startButton = toolkit.createButton(table, "Start", SWT.PUSH);
		startButton.setImage(JFaceResources.getImageRegistry().get("G_RUNNING_STATE_ICON"));
		{
			TableEditor editor = new TableEditor(table);
			editor.grabHorizontal = true;
		    editor.horizontalAlignment = SWT.LEFT;
		    editor.setEditor(startButton, tableItem, 2);
		}
		startButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				Job job = new ServiceJob(GIModelConfig.ACTION_START);
				setButtons("", true);
				job.schedule();
			}

		});
		restartButton = toolkit.createButton(table, "Restart", SWT.NONE);
		restartButton.setImage(JFaceResources.getImageRegistry().get("G_RESTART_ACTION_ICON"));
		{
			TableEditor editor = new TableEditor(table);
			editor.grabHorizontal = true;
			editor.setEditor(restartButton, tableItem, 3);
		}
		restartButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				Job job = new ServiceJob(GIModelConfig.ACTION_RESTART);
				setButtons("", true);
				job.schedule();

			}

		});

		stopButton = toolkit.createButton(table, "Stop", SWT.NONE);
		stopButton.setImage(JFaceResources.getImageRegistry().get("G_STOP_ACTION_ICON"));
		{
			TableEditor editor = new TableEditor(table);
			editor.grabHorizontal = true;
			editor.setEditor(stopButton, tableItem, 4);
		}
		stopButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				Job job = new ServiceJob(GIModelConfig.ACTION_STOP);
				setButtons("", true);
				job.schedule();
			}

		});


		Button tButton = toolkit.createButton(table, "Run Change Wizard", SWT.NONE);
		{
			TableEditor editor = new TableEditor(table);
			editor.grabHorizontal = true;
			editor.setEditor(tButton, tableItem, 5);
		}
		tButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetDefaultSelected(e);
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				WizardDialog dialog = new InstallWizardDialog(getSite().getShell(), new GChangeWizard());
				dialog.open();
				refresh();
			}
		});
		configuredDependentButtons.add(tButton);

		/** Create a section for the licenses table. */
		Section licensesSection = toolkit.createSection(statusForm.getBody(), Section.TITLE_BAR);
		licensesSection.setText("License Information");

		/** Create a layout for this section. */
		licensesSection.setLayout(new GridLayout());
		licensesSection.setLayoutData(new GridData(GridData.FILL_BOTH));

		licensesContainer = toolkit.createComposite(licensesSection);
		licensesSection.setClient(licensesContainer);
		licensesContainer.setLayout(new GridLayout(2, false));

		toolkit.createLabel(licensesContainer, "License Number");
		licenseNumberText = toolkit.createLabel(licensesContainer, "");

		toolkit.createLabel(licensesContainer, "License Expiration Date");
		licenseExpirationText = toolkit.createLabel(licensesContainer, "");

		toolkit.createLabel(licensesContainer, "Licensed To");
		licenseOwnerText = toolkit.createLabel(licensesContainer, "");

		toolkit.createLabel(licensesContainer, "Update License File");
		Button updateLicenseButton = toolkit.createButton(licensesContainer, "Choose File", SWT.PUSH);
		updateLicenseButton.setBounds(0, 0, 100, 100);
		chooseLicenseFileAction = new ChooseLicenseFile();

		updateLicenseButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				boolean ok = chooseLicenseFileAction.runInShell(getSite().getShell());
				if (ok)
					refresh();
			}


		});


		/** Create a section for the package re-creation. */
		Section generatePackagesSection = toolkit.createSection(statusForm.getBody(), Section.TITLE_BAR);
		generatePackagesSection.setText("Software Package (GPM) Generation");
		generatePackagesSection.setLayout(new GridLayout());
		generatePackagesSection.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		Composite packageContainer = toolkit.createComposite(generatePackagesSection);
		generatePackagesSection.setClient(packageContainer);
		packageContainer.setLayout(new GridLayout());

		/** Add some descriptive text for this section. */
		Label label = toolkit.createLabel(packageContainer, "", SWT.WRAP);
		label.setText("A number of software packages for end user clients are created by the configuration " +
				"wizard during setup. If you have made changes to the package setup files you can " +
				"regenerate the packages by clicking the 'generate' button below.");
		label.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		/** Add a button for re-generating the GPMs. */
		Button gButton = toolkit.createButton(packageContainer, "Generate", SWT.NONE);
		GridData buttongridData = new GridData();
		buttongridData.horizontalAlignment = SWT.END;
		gButton.setLayoutData(buttongridData);

		gButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				WizardDialog dialog = new WizardDialog(getSite().getShell(), new GSinglePageWizard(new GCreateGPMWizardPage()));
				dialog.open();
			}
		});
		configuredDependentButtons.add(gButton);

		/** Create a section for the support package creation. */
		Section generateSupportPackSection = toolkit.createSection(statusForm.getBody(), Section.TITLE_BAR);
		generateSupportPackSection.setText("Support Package Generation");
		generateSupportPackSection.setLayout(new GridLayout());
		generateSupportPackSection.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		Composite supportPackContainer = toolkit.createComposite(generateSupportPackSection);
		generateSupportPackSection.setClient(supportPackContainer);
		supportPackContainer.setLayout(new GridLayout());

		/** Add some descriptive text for this section. */
		Label label2 = toolkit.createLabel(supportPackContainer, "", SWT.WRAP);
		label2.setText("A support package can be generated that will allow the Soliton Systems developers to investigate any problems " +
				"that may have occurred in the G/On Server system. You would probably only want to run this if asked to by " +
				"your Soliton Systems support person.");
		label2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		/** Add a button for re-generating the GPMs. */
		Button sButton = toolkit.createButton(supportPackContainer, "Generate", SWT.NONE);
		sButton.setLayoutData(buttongridData);

		sButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				final GLocalServiceJob localServiceJob =  modelApi.getGenerateSupportPackageJob();
				final GJobHandler job = new GJobHandler("Generating Support Package", "2", getSite().getShell().getDisplay(), localServiceJob, GConfigActivator.getLogger());
				job.setJobUpdateHandler(new GJobHandler.JobUpdateHandler() {

					@Override
					public void update(String jobId, GIJobInfo status) {


					}

					@Override
					public void finished(String jobId, GIJobInfo lastProgress) {
						JobInfoType jobInfoType = lastProgress.getJobInfoType();
						if (lastProgress.getJobStatus() == Job_status_type0.done_ok_job_status) {
							String fileName = jobInfoType.getJob_done_arg_01() + ".zip";
							File f = new File(fileName);
							if (f.exists()) {
								FileDialog fileChooser = new FileDialog(getSite().getShell(), SWT.SAVE);
								fileChooser.setFileName(fileName);
								fileChooser.setOverwrite(true);
								String copyFileName = fileChooser.open();
								if (copyFileName!=null)
									if (!copyFileName.equalsIgnoreCase(fileName)) {
										try {
											FileUtils.copyFile(f, new File(copyFileName));
										} catch (IOException e) {
											throw new RuntimeException(e);
										}
									}
							}


						}
						else if (lastProgress.getJobStatus() == Job_status_type0.done_error_job_status) {
							String errorMsg = lastProgress.getJobInfo();
							MessageDialog.openError(getSite().getShell(), "Error", errorMsg);

						}
					}
				});
				job.setUser(true);
				job.schedule();
			}
		});

		createActions();
		createToolbar();
		createContextMenu();

	}

	public void refresh() {
		boolean configuredStatus = modelApi.getConfiguredStatus();
		GIManagementServiceStatus managementServiceStatus = modelApi.getManagementServiceStatus();
		managementServiceName.setText(managementServiceStatus.getName());
		String status = managementServiceStatus.getStatus();
		setServiceStatus(status, configuredStatus);
		setConfiguredDependentButtonStatus(configuredStatus);


		managementStatusText.pack();
		managementStatusImage.pack();
		infoContainer.layout();

		LicenseInfoType licenseStatus = modelApi.getLicenseStatus();
		if (licenseStatus == null) {
			licenseNumberText.setText("Invalid");
			licenseExpirationText.setText("N/A");
			licenseOwnerText.setText("N/A");
		}
		else {
			licenseNumberText.setText(licenseStatus.getLicense_number());
			String dateString = DynamicIntroContent.convertDataToString(licenseStatus.getLicense_expires());
			licenseExpirationText.setText(dateString);
			licenseOwnerText.setText(licenseStatus.getLicensed_to());

		}
		licensesContainer.layout();

	}

	private void setConfiguredDependentButtonStatus(boolean configuredStatus) {
		for (Button b : configuredDependentButtons) {
			b.setEnabled(configuredStatus);
		}

	}

	private String getServiceStatus() {
		GIManagementServiceStatus managementServiceStatus = modelApi.getManagementServiceStatus();
		String status = managementServiceStatus.getStatus();
		return status;
	}

	private void setButtons(String status, boolean configuredStatus) {
		if (status.equalsIgnoreCase(GIModelConfig.STATE_NOT_RUNNING)) {
			stopButton.setEnabled(false);
			startButton.setEnabled(configuredStatus);
			restartButton.setEnabled(false);
		}
		else if (status.equalsIgnoreCase(GIModelConfig.STATE_RUNNING)) {
			stopButton.setEnabled(true);
			startButton.setEnabled(false);
			restartButton.setEnabled(true);
		}
		else {
			stopButton.setEnabled(false);
			startButton.setEnabled(false);
			restartButton.setEnabled(false);
		}
	}

	private void updateServiceStatus(String status) {
		if (status.equalsIgnoreCase(GIModelConfig.STATE_NOT_RUNNING)) {
			managementStatusImage.setImage(JFaceResources.getImageRegistry().get("G_STOP_STATE_ICON"));
			managementStatusText.setText("Stopped");
		}
		else if (status.equalsIgnoreCase(GIModelConfig.STATE_RUNNING)) {
			managementStatusImage.setImage(JFaceResources.getImageRegistry().get("G_RUNNING_STATE_ICON"));
			managementStatusText.setText("Running");

		}
		else if (status.equalsIgnoreCase(GIModelConfig.STATE_RESTARTING)) {
			managementStatusImage.setImage(JFaceResources.getImageRegistry().get("G_RESTART_STATE_ICON"));
			managementStatusText.setText("Restarting");
		}
		else if (status.equalsIgnoreCase(GIModelConfig.STATE_STOPPING)) {
			managementStatusImage.setImage(JFaceResources.getImageRegistry().get("G_STOP_STATE_ICON"));
			managementStatusText.setText("Stopping");
		}
		else {
			managementStatusImage.setImage(JFaceResources.getImageRegistry().get("G_UNKNOWN_STATE_ICON"));
			managementStatusText.setText("Service not found");
		}
	}


	private void setServiceStatus(String status, boolean configuredStatus) {
		setButtons(status, configuredStatus);
		updateServiceStatus(status);
	}

	@Override
	public void createView(Composite parent) {
		createStatusView(parent);

	}

	public void createActions() {

		refreshAction = new Action("Refresh") {
			public void run() {
				refresh();
			}
		};
		refreshAction.setImageDescriptor(ImageDescriptor.createFromImage(JFaceResources.getImageRegistry().get("G_REFRESH_ACTION_ICON")));


	}

	private void createToolbar() {
		statusForm.getToolBarManager().add(refreshAction);
		statusForm.updateToolBar();
	}

	/**
	 * Create context menu.
	 */
	private void createContextMenu() {
		// Create menu manager.
		MenuManager menuMgr = new MenuManager();
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager mgr) {
				fillContextMenu(mgr);
			}
		});

		// Create menu.
//		Menu menu = menuMgr.createContextMenu(servicesViewer.getControl());
//		servicesViewer.getControl().setMenu(menu);
//
//		// Register menu for extension.
//		getSite().registerContextMenu(menuMgr, servicesViewer);
	}

	private void fillContextMenu(IMenuManager mgr) {
		mgr.add(refreshAction);
	}

	@Override
	public void setViewFocus() {
		refresh();
	}
}
