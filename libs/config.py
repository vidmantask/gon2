'''
**********************************************************************************************************
G/On Develop Environement utility.
**********************************************************************************************************

Initialization of Develop environment

  Initialize for normal development:
  $ config.py --cmd_init normal

  Initialize for normal development with build of C++:
  $ config.py --cmd_init normal --enable_cpp_build


Functionality for handling G/On Services

  Complete restore of environment used when running gon_services and gon_client. This will restore
  the folder ./setup/dev_env/gon_installation from ./setup/dev_env/gon_installation_template.
  $ python config.py --cmd_gon_services init

  Start all G/On Services (Gateway Server, Management Server, Mangement Client Service and Config Service):
  $ python config.py --cmd_gon_services start

  Stop all G/On Services:
  $ python config.py --cmd_gon_services stop


Functionality for running G/On Client

  Generate registry entries for running client from development environment as a ComputerUserToken:
  $ python config.py --cmd_gon_client first_start

  Run client:
  $ python config.py --cmd_gon_client run


Functionality for running G/On Client Stealth

  Run client:
  $ python config.py --cmd_gon_client_stealth run


Functionality for source code generation(incl. py/java generation from wsdl):

  Generate py and java proxy for admin_ws web-service interface:
  $ python config.py --cmd_source_gen admin_ws


Functionality for handling devbotnet web server:

  Start devbotnet web server:
  $ python config.py --cmd_web serve

  Clear ALL information about previous runned jobs:
  $ python config.py --cmd_web clean


Functionality for starting devbotnet client (for use of devbotnet web server):

  Start devbotnet slave:
  $ python config.py --cmd_devbotnet start

  Stop devbotnet slave:
  $ python config.py --cmd_devbotnet stop

  Stop devbotnet slave:
  $ python config.py --cmd_devbotnet restart

'''

import sys
import os
import os.path
import optparse
import xmlrpclib
import SimpleXMLRPCServer
import ConfigParser
import datetime
import threading
import tempfile
import shutil
import time
import pickle
import copy
import subprocess
import string
import re
import traceback
import tarfile
import difflib
import glob
import hashlib

HG_ROOT = os.path.normpath(os.path.abspath(os.path.dirname(__file__)))
ROOT_PY = os.path.join(HG_ROOT, 'py')
sys.path.append(ROOT_PY)
import lib.dev_env.path_setup


ROOT_GON_BUILD = os.path.join(HG_ROOT, '..', 'gon_build')
ROOT_BUILD = os.path.join(ROOT_GON_BUILD, 'build')
ROOT_INSTALL = os.path.join(ROOT_GON_BUILD, 'dist')

TARGET_PLATFORM_MAC    = "mac_64"
TARGET_PLATFORM_LINUX  = "linux_64"
TARGET_PLATFORM_WIN    = "win_32"
def detect_platform():
    if sys.platform == "win32":
        return TARGET_PLATFORM_WIN
    elif sys.platform in [ "linux2", "linux" ]:
        return TARGET_PLATFORM_LINUX
    elif sys.platform == "darwin":
        return TARGET_PLATFORM_MAC
    return "unknown"


import paramiko

import lib.utility
import lib.version
import lib.dev_env
import lib.dev_env.tools
import lib.dev_env.devbotnet
import lib.dev_env.devbotnet_jobs
import lib.dev_env.devbotnet_jobs_no_db
import lib.dev_env.source_gen




DEV_BUILD_HOST = '192.168.42.118'
DEV_BUILD_HOST_USERNAME = 'gbuilder'
DEV_BUILD_HOST_PASSWORD = 'GiriGiri007'



DEV_PACKAGE_LIST = { 'client_and_server': [
                               'appl.gon_client',
                               'appl.gon_client_uninstaller',
                               'appl.gon_client_launch',
                               'appl.gon_client_management',
                               'appl.gon_config_service',
                               'appl.gon_server_gateway',
                               'appl.gon_server_management',
                               'appl.gon_server_installer'
                               ],
                     'client':[
                               'appl.gon_client',
                               'appl.gon_client_uninstaller',
                               'appl.gon_client_launch',
                               ],

                     'server':[
                               'appl.gon_config_service',
                               'appl.gon_server_gateway_service',
                               'appl.gon_server_management_service',
                               'appl.gon_server_installer'
                               ]
                    }



def print_header(info):
    print "=" * 80
    print info
    print "=" * 80

def print_sub_header(info):
    print "-" * 80
    print info
    print "-" * 80

def hg_get_revision(git_root=HG_ROOT, shell_required=False, check_modification=True):
    git_file = '.'
    if os.path.isfile(git_root):
        git_root, git_file = os.path.split(git_root)
    command = ["git", 'log', '-n', '1', '--pretty=format:%H', '--', git_file]
    process = subprocess.Popen(command, cwd=git_root, stdout=subprocess.PIPE, shell=shell_required)
    process_result = process.communicate()
    process.wait()
    result = str(process_result[0].strip())
    digest = result[2:len(result)-1]

    if not check_modification:
        return digest

    command = ["git", '--no-pager', 'diff', git_file]
    process = subprocess.Popen(command, cwd=git_root, stdout=subprocess.PIPE, shell=shell_required)
    process_result = process.communicate()
    process.wait()
    result = str(process_result[0].strip())
    result_diff = result[2:len(result)-1]
    modified = ""
    if len(result_diff) > 0:
#        modified = "(modified " + hashlib.md5(result_diff.encode('utf-8')).hexdigest() +")"
        modified = "(modified)"
    result = digest + modified
    return result

def get_changeset_local(changeset='tip'):
    if changeset.endswith('+'):
        changeset = changeset[:len(changeset)-1]

    command = ["hg", "id", "-n", '-r', changeset]
    process = subprocess.Popen(command, cwd=hg_root, stdout=subprocess.PIPE)
    process.wait()
    process_result = process.communicate()
    return process_result[0].strip()

def hg_uncommited_files(hg_root=HG_ROOT):
    command = ["hg", "st", "-mar"]
    process = subprocess.Popen(command, cwd=hg_root, stdout=subprocess.PIPE)
    process.wait()
    process_result = process.communicate()
    hg_status_output = process_result[0].rstrip()
    return hg_status_output.splitlines()

def hg_pull_with_update(hg_root=HG_ROOT):
    command = ["hg", "pull", "-u"]
    process = subprocess.Popen(command, cwd=hg_root, stdout=subprocess.PIPE)
    process.wait()
    process_result = process.communicate()
    return process_result[0].rstrip()

def hg_check_for_news(hg_root=HG_ROOT):
    """
    Check remote repository for news, and return true if news are available
    """
    command = ["hg", "incoming"]
    process = subprocess.Popen(command, cwd=hg_root, stdout=subprocess.PIPE)
    process.wait()
    return process.returncode == 0


IOS_SDK_IPHONE = '/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/usr'
def get_tool_lipo():
    return os.path.join(IOS_SDK_IPHONE, 'bin', 'lipo')


def exec_command(command, env=None, check_rc=True, shell=True, cwd=None, ignore_error=False):
    if shell:
        command = " ".join(command)
    print command
    rc =  subprocess.call(command, env=env, cwd=cwd, shell=shell)
    if check_rc and rc and not ignore_error:
        print "ERROR " * 15
        print "ERROR " * 15
        print 'Execution of command failed %r' % command
        print "ERROR " * 15
        print "ERROR " * 15
        response = raw_input("Do you want to proceed (YES/NO) ? ")
        if response != "YES":
            exit(1)


# ========================================================================================================================
class CommandlineOptions(object):
    def __init__(self):
        self._parser = optparse.OptionParser()
        self._parser.add_option('--cmd_init', type='string', default=None,  help='Initialize develop environment [normal, release]')
        self._parser.add_option('--cmd_gon_services', type='string', default=None,  help='Handle G/On Services runned from dev environment [init, start, stop, build_gpms, update_license]')
        self._parser.add_option('--cmd_gon_client', type='string', default=None,  help='Handle G/On Client runned from dev environment [first_start, run]')
        self._parser.add_option('--cmd_gon_client_stealth', type='string', default=None,  help='Handle G/On Client Stealth runned from dev environment [run]')
        self._parser.add_option('--cmd_release', type='string', default=None,  help='[clean, build, pack, gen_installers, gen_gpms, gen_gpm_for_gon_os, all, distribute')

        self._parser.add_option('--cmd_web', type='string', default=None, help='Expose the development environment as web-server, for browse of unittest and static analysis results. [serve, clear]')
        self._parser.add_option('--cmd_devbotnet', type='string', default=None, help='Handle devbotnet functionality [start, stop, restart]')

        self._parser.add_option('--cmd_source_gen', type='string', default=None, help='Execute one of the following source code generators [admin_ws, admin_deploy_ws, server_config_ws]')
        self._parser.add_option('--cmd_dictionary', type='string', default=None, help='Dictionary handling [generate]')

        self._parser.add_option('--enable_cpp_build', action='store_true', default=False, help='Optional option for --cmd_init that enables build of C++ code.')

        self._parser.add_option('--auto_yes', action='store_true', default=False, help='Optional option for answer yes to all questions.')

        self._parser.add_option('--ifilter', type='string', default=None, help='Include filter')
        self._parser.add_option('--compare_to_file', action='store_true', default=False, help='Optional option for --cmd_release that enables dumps diff to file.')

        self._parser.add_option('--build_id', type='string', default='0', help='Build id')
        self._parser.add_option('--build_store', type='string', default=None, help='')
        self._parser.add_option('--skip_sign',  action='store_true', default=False, help='Skip signing')

        self._parser.add_option('--cmd_unittest',  type='string', default=None, help='Accepted arguments [py, cpp]')
        self._parser.add_option('--unittest_efilter',  type='string', default=None, help='Unittest include filter, default none')
        self._parser.add_option('--unittest_ifilter',  type='string', default=None, help='Unittest include filter, default all')

        (self._options, self._args) = self._parser.parse_args()
        self._config_version = self._read_config_version_ini()
        self._config = self._read_config_ini()

    def _read_config_ini(self):
        config_filename = os.path.join(os.path.dirname(__file__), 'config_local.ini')
        config = ConfigParser.ConfigParser()
        if not os.path.exists(config_filename):
            tools_setup = lib.dev_env.tools.ToolsSetup.create_from_config(config)
            dev_env = lib.dev_env.DevEnv(tools_setup, HG_ROOT, self.get_version())
            devbotnet_slave_info = lib.dev_env.devbotnet.DevbotnetSlaveInfo(dev_env)
            devbotnet_slave_info.update_config_ini(config)

            config_file = open(config_filename, 'w')
            config.write(config_file)
            config_file.close()
        else:
            config.read(config_filename)
        return config

    def update_config_ini(self, type, cpp_build):
        config_filename = os.path.join(os.path.dirname(__file__), 'config_local.ini')
        config = ConfigParser.ConfigParser()
        config.read(config_filename)

        if not config.has_section('dev_env_do_not_change'):
            config.add_section('dev_env_do_not_change')
        config.set('dev_env_do_not_change', 'type', type)
        config.set('dev_env_do_not_change', 'cpp_build', cpp_build)

        if not config.has_section('release'):
            config.add_section('release')
            config.set('release', 'package_list', 'client_and_server')
            config.set('release', 'generate_installers', False)

        config_file = open(config_filename, 'w')
        config.write(config_file)
        config_file.close()

    def get_config_ini_type(self):
        return self._config.get('dev_env_do_not_change', 'type')

    def get_config_ini_cpp_build(self):
        if self._config.has_option('dev_env_do_not_change', 'cpp_build'):
            return self._config.getboolean('dev_env_do_not_change', 'cpp_build')
        return False

    def get_devbotnet_slave_info(self, dev_env):
        return lib.dev_env.devbotnet.DevbotnetSlaveInfo.create_from_config_ini(dev_env, self._config)

    def _read_config_version_ini(self):
        config_version_filename = os.path.join(os.path.dirname(__file__), '..', 'config_version.ini')
        config_version = ConfigParser.ConfigParser()
        config_version.read(config_version_filename)
        return config_version

    def cmd_init(self):
        return self._options.cmd_init

    def cmd_unittest(self):
        return self._options.cmd_unittest

    def unittest_ifilter(self):
        return self._options.unittest_ifilter

    def unittest_efilter(self):
        return self._options.unittest_efilter

    def cmd_gon_services(self):
        return self._options.cmd_gon_services

    def cmd_gon_client(self):
        return self._options.cmd_gon_client

    def cmd_gon_client_stealth(self):
        return self._options.cmd_gon_client_stealth

    def cmd_release(self):
        return self._options.cmd_release

    def cmd_unittest(self):
        return self._options.cmd_unittest

    def cmd_static_analysis(self):
        return self._options.cmd_static_analysis

    def option_enable_cpp_build(self):
        return self._options.enable_cpp_build

    def option_ifilter(self):
        return self._options.ifilter

    def cmd_source_gen(self):
        return self._options.cmd_source_gen

    def cmd_dictionary(self):
        return self._options.cmd_dictionary

    def cmd_web(self):
        return self._options.cmd_web

    def cmd_devbotnet(self):
        return self._options.cmd_devbotnet

    def print_help(self):
        self._parser.print_help()

    def get_version(self):
        major = self._config_version.getint('version', 'major')
        minor = self._config_version.getint('version', 'minor')
        bugfix = self._config_version.getint('version', 'bugfix')
        if self._options.build_id is not None:
            build_id = int(self._options.build_id)
        else:
            build_id = self._config_version.getint('version', 'build_id')
        version_depot = hg_get_revision()
        build_date = datetime.datetime.now()
        return lib.version.Version(major, minor, bugfix, build_id, version_depot, build_date)

    def get_tools_setup(self):
        return lib.dev_env.tools.ToolsSetup.create_from_config(self._config)

    def get_compare_to_file(self):
        return self._options.compare_to_file

    def get_auto_yes(self):
        return self._options.auto_yes

    def get_release_generate_installers(self):
        return False
#        if self._config.has_option('release', 'generate_installers'):
#            return self._config.getboolean('release', 'generate_installers')
#        return False

    def get_release_generate_gpms(self):
        return True
#        if self._config.has_option('release', 'generate_gpms'):
#            return self._config.getboolean('release', 'generate_gpms')
#        return False

    def get_release_generate_gpm_for_gon_os(self):
        return True
#        if self._config.has_option('release', 'generate_gpm_for_gon_os'):
#            return self._config.getboolean('release', 'generate_gpm_for_gon_os')
#        return False

    def get_release_package_list(self):
        if self._config.has_section('release'):
            return self._config.get('release', 'package_list')
        return 'client_and_server'

    def get_build_folder(self):
        platform = detect_platform()
        return os.path.join(ROOT_BUILD, platform, 'libs')

    def build_store(self):
        if self._options.build_store is not None:
            return self._options.build_store
        return os.path.join(ROOT_GON_BUILD, 'build_store')

    def skip_sign(self):
        return self._options.skip_sign



# ========================================================================================================================
class DevEnvRelease(object):
    RELEASE_COMMAND_clean = 'clean'
    RELEASE_COMMAND_build = 'build'
    RELEASE_COMMAND_pack = 'pack'
    RELEASE_COMMAND_distribute = 'distribute'
    RELEASE_COMMAND_gen_installers = 'gen_installers'
    RELEASE_COMMAND_gen_gpms = 'gen_gpms'
    RELEASE_COMMAND_gen_gpm_for_gon_os = 'gen_gpm_for_gon_os'
    RELEASE_COMMAND_all = 'all'

    def __init__(self, dev_env, commandline_options, devbotnet_slave_info):
        self.commandline_options = commandline_options
        self.dev_env = dev_env
        self.devbotnet_slave_info = devbotnet_slave_info
        self.type = commandline_options.get_config_ini_type()
        self.build_cpp = commandline_options.get_config_ini_cpp_build()
        self.compare_to_file = commandline_options.get_compare_to_file()
        self.auto_yes = commandline_options.get_auto_yes()

        self.compare_to_file_filename = None
        if self.compare_to_file:
            self.init_compare_file()

    def init_compare_file(self):
        self.compare_to_file_filename = os.path.join(HG_ROOT, 'setup', 'dev_env', 'release_diffs', 'diff_%s' % self.dev_env.version.get_version_string_num().strip())
        self.compare_to_file_folder = os.path.dirname(self.compare_to_file_filename)
        if not os.path.exists(self.compare_to_file_folder):
            os.makedirs(self.compare_to_file_folder)
        compare_to_file = open(self.compare_to_file_filename, 'a')
        compare_to_file.write("=" * 80 + "\n")
        compare_to_file.write("=" * 80 + "\n")
        compare_to_file.write("%s(%s) %s\n" % (self.dev_env.version.get_version_string(), self.dev_env.version.get_build_date_string(), self.dev_env.version.get_version_depot()))
        compare_to_file.write("-" * 80 + "\n")
        compare_to_file.close()

    def info(self):
        header_info = ''
        if self.type == self.dev_env.SETUP_COMMAND_release:
            header_info =  "RELEASE BUILD - RELEASE BUILD - RELEASE BUILD - RELEASE BUILD - RELEASE BUILD\n"
        header_info += "Release info : %s(%s) %s" % (self.dev_env.version.get_version_string(), self.dev_env.version.get_build_date_string(), self.dev_env.version.get_version_depot())
        print print_header(header_info)

    def _mark_version_in_file(self, filename):
        afile = open(filename, 'r')
        afile_content = afile.read()
        afile.close()
        afile_content = afile_content.replace(self.dev_env.version.get_version_string_num(), '${gon_version}')
        afile_content = afile_content.replace(self.dev_env.get_build_pack_root(), '${gon_build_pack_root}')
        afile_content = re.sub(r'(\s/var/tmp/.*)(\t|\s|\n)', r'${gon_temp_filename}\2', afile_content)

        afile = open(filename, 'w')
        afile.write(afile_content)
        afile.close()

    def _compare_files(self, filename):
        try:
            accept_filename = os.path.join(HG_ROOT, 'setup', 'dev_env', 'accepted_files', os.path.basename(filename))
            accept_file = open(accept_filename, 'r')
            accepted_content = accept_file.readlines()
            accept_file.close()
        except:
            accepted_content = []

        self._mark_version_in_file(filename)
        datafile = open(filename, 'r')
        data_content = datafile.readlines()
        datafile.close()

        d = difflib.Differ()
        result = difflib.unified_diff(accepted_content, data_content, filename)
        sys.stdout.writelines(result)

        if self.compare_to_file:
            compare_to_file = open(self.compare_to_file_filename, 'a')
            compare_to_file.writelines(result)
            compare_to_file.close()
            return False
        else:
            if data_content != accepted_content:
                if self.auto_yes:
                    print("WARNING, output differs, auto ignore")
                    do_proceed = 'IGNORE'
                else:
                    do_proceed = raw_input("WARNING, output differs. Do you want to accept new output ? YES,NO,[IGNORE] : ")
                if do_proceed.upper() == "YES":
                    shutil.copy(filename, accept_filename)
                    return False
                if do_proceed.upper() == "NO":
                    return True
                return False
        return False

    def clean(self):
        print_sub_header('clean')
        if os.path.isdir(self.dev_env.pack_root):
            shutil.rmtree(self.dev_env.pack_root)
        if os.path.isdir(self.dev_env.build_root):
            shutil.rmtree(self.dev_env.build_root)
        if self.build_cpp:
            self._clean_cpp()
        if self.devbotnet_slave_info.arch_target in [lib.dev_env.devbotnet.ARCH_TARGET_LINUX, lib.dev_env.devbotnet.ARCH_TARGET_LINUX_64]:
            self._clean_cxfreeze()
        if self.devbotnet_slave_info.arch_target in [lib.dev_env.devbotnet.ARCH_TARGET_MAC]:
            self._clean_py2app()
        if self.devbotnet_slave_info.arch_target in [lib.dev_env.devbotnet.ARCH_TARGET_WIN, lib.dev_env.devbotnet.ARCH_TARGET_WIN_64]:
            self._clean_py2exe()
        if self.devbotnet_slave_info.arch_target in [lib.dev_env.devbotnet.ARCH_TARGET_IPHONE]:
            self._clean_iphone()

    def _clean_cpp(self):
        command = ['cmake', HG_ROOT]
        result = subprocess.call(command, cwd=self.commandline_options.get_build_folder())

        command = [self.dev_env.tools_setup.get_make(), 'clean']
        result = subprocess.call(command, cwd=self.commandline_options.get_build_folder())

    def _clean_cxfreeze(self):
        setup_files = self.dev_env.select_files_with_suffix(ROOT_PY, '.cxfreeze.py')
        for setup_file in setup_files:
            dist_folder = os.path.join(os.path.dirname(setup_file), 'dist')
            if os.path.isdir(dist_folder):
                shutil.rmtree(dist_folder)

    def _clean_py2app(self):
        setup_files = self.dev_env.select_files_with_suffix(ROOT_PY, '.py2app.setup.py')
        for setup_file in setup_files:
            dist_folder = os.path.join(os.path.dirname(setup_file), 'build')
            if os.path.isdir(dist_folder):
                shutil.rmtree(dist_folder)

    def _clean_py2exe(self):
        setup_files = self.dev_env.select_files_with_suffix(ROOT_PY, '.py2exe.setup.py')
        for setup_file in setup_files:
            dist_folder = os.path.join(os.path.dirname(setup_file), 'build')
            if os.path.isdir(dist_folder):
                shutil.rmtree(dist_folder)

    def _clean_iphone(self):
        command = [self.dev_env.tools_setup.get_make(), 'clean', 'CLEAN_IOS']
        result = subprocess.call(command, cwd=HG_ROOT)
        if result != 0:
            print "ERROR building"
            return 1
        return 0

    def build(self):
        print_sub_header('build')
        if self.build_cpp:
            if self.devbotnet_slave_info.arch_target in [lib.dev_env.devbotnet.ARCH_TARGET_IPHONE]:
                return self._build_cpp_iphone()
            else:
                return self._build_cpp()
        else:
            print "Building of C++ disabled, using precompiled version of Giritech C++ libs"
        return 0

    def _build_cpp(self):
        std_out_filename = os.path.join(self.dev_env.temp_root, 'build_%s.stdout' % self.devbotnet_slave_info.arch_target)
        std_err_filename = os.path.join(self.dev_env.temp_root, 'build_%s.stderr' % self.devbotnet_slave_info.arch_target)
        std_out_file = open(std_out_filename, 'w')
        std_err_file = open(std_err_filename, 'w')

        command = [self.dev_env.tools_setup.get_make(), 'DEPLOY_CPP_ALL']
        print command
        print self.commandline_options.get_build_folder()
        result = subprocess.call(command, cwd=self.commandline_options.get_build_folder(), stdout=std_out_file, stderr=std_err_file)
        std_out_file.close()
        std_err_file.close()
        if result != 0:
            print open(std_err_filename, 'r').read()
            return result

        if self._compare_files(std_out_filename) or self._compare_files(std_err_filename):
            print "ERROR building"
            return 1
        return 0

    def _build_cpp_iphone_copy_lib_files(self, folder_src, arch):
        folder_dest = os.path.join(folder_src, 'lib', arch)
        if not os.path.exists(folder_dest):
            os.makedirs(folder_dest)
        for filename in glob.glob(os.path.join(folder_src, '*.a')):
            shutil.copy(filename, folder_dest)


    def _build_cpp_iphone_create_fat(self, folder_src):
        dest_folder = os.path.join(folder_src, 'lib')
        src_folder_1 = os.path.join(folder_src, 'lib', 'armv7')
        src_folder_2 = os.path.join(folder_src, 'lib', 'armv7s')
        src_folder_3 = os.path.join(folder_src, 'lib', 'sim')
        filenames = set()
        for filename in glob.glob(os.path.join(src_folder_1, '*.a')):
            filenames.add(os.path.basename(filename))
        for filename in glob.glob(os.path.join(src_folder_2, '*.a')):
            filenames.add(os.path.basename(filename))
        for filename in glob.glob(os.path.join(src_folder_3, '*.a')):
            filenames.add(os.path.basename(filename))

        for filename in filenames:
            src_filename_1 = os.path.join(src_folder_1, filename)
            src_filename_2 = os.path.join(src_folder_2, filename)
            src_filename_3 = os.path.join(src_folder_3, filename)
            dest_filename = os.path.join(dest_folder, filename)
            command = [get_tool_lipo(), src_filename_1, src_filename_2, src_filename_3, '-output', dest_filename, '-create']
            print "CREATE FAT LIB %s" % dest_filename
            exec_command(command, cwd=HG_ROOT, shell=False)

            command = ['strip', '-S', dest_filename]
            print "STRIP LIB %s" % dest_filename
            exec_command(command, cwd=HG_ROOT, shell=False)

    def _build_cpp_iphone(self):
        toolchain_filename = os.path.join(HG_ROOT, 'setup', 'init', 'toolchains', 'current.toolchain')
        toolchain_armv7_filename = os.path.join(HG_ROOT, 'setup', 'init', 'toolchains', 'iphone-armv7-6.0.toolchain')
        toolchain_armv7s_filename = os.path.join(HG_ROOT, 'setup', 'init', 'toolchains', 'iphone-armv7s-6.0.toolchain')
        toolchain_sim_filename = os.path.join(HG_ROOT, 'setup', 'init', 'toolchains', 'iphone-sim-6.0.toolchain')

        shutil.copy(toolchain_armv7_filename, toolchain_filename)
        command = ['cmake -C setup/init/cmake_init_iphone -C setup/init/cmake_init_release -G "Unix Makefiles" .']
        exec_command(command, cwd=HG_ROOT, shell=True)

        command = [self.dev_env.tools_setup.get_make(), 'clean', 'BUILD_IOS_LIBS']
        exec_command(command, cwd=HG_ROOT, shell=False, env=os.environ)
        self._build_cpp_iphone_copy_lib_files(os.path.join(HG_ROOT, 'gon_build', 'out', 'build', 'bin', 'iphone'), 'armv7')

        shutil.copy(toolchain_armv7s_filename, toolchain_filename)
        command = [self.dev_env.tools_setup.get_make(), 'clean', 'BUILD_IOS_LIBS']
        exec_command(command, cwd=HG_ROOT, shell=False)
        self._build_cpp_iphone_copy_lib_files(os.path.join(HG_ROOT, 'gon_build', 'out', 'build', 'bin', 'iphone'), 'armv7s')

        shutil.copy(toolchain_sim_filename, toolchain_filename)
        command = [self.dev_env.tools_setup.get_make(), 'clean', 'BUILD_IOS_LIBS']
        exec_command(command, cwd=HG_ROOT, shell=False)
        self._build_cpp_iphone_copy_lib_files(os.path.join(HG_ROOT, 'gon_build', 'out', 'build', 'bin', 'iphone'), 'sim')

        self._build_cpp_iphone_create_fat(os.path.join(HG_ROOT, 'gon_build', 'out', 'build', 'bin', 'iphone'))

        com_lib_root = os.path.join(HG_ROOT, 'gon_build', 'out', 'pack', 'GOnSecureCommunicationLib')
        if os.path.exists(com_lib_root):
            shutil.rmtree(com_lib_root)

        command = 'xcodebuild -project cpp/component/appl/gon_secure_communication_lib_ios/GOnSecureCommunicationLib/GOnSecureCommunicationLib.xcodeproj -target  GOnSecureCommunicationLib -sdk iphoneos6.0'
        exec_command(command, cwd=HG_ROOT, shell=True)

        command = 'xcodebuild -project cpp/component/appl/gon_secure_communication_lib_ios/GOnSecureCommunicationLib/GOnSecureCommunicationLib.xcodeproj -target  GOnSecureCommunicationLib -sdk iphonesimulator6.0 -arch i386'
        exec_command(command, cwd=HG_ROOT, shell=True)

        com_lib_root_a = os.path.join(com_lib_root, 'lib', 'Release')
        if not os.path.exists(com_lib_root_a):
            os.makedirs(com_lib_root_a)
        command = [get_tool_lipo(), 'gon_build/out/pack/GOnSecureCommunicationLib/lib/armv7_armv7s/Release-iphoneos/libGOnSecureCommunicationLib.a', 'gin_build/out/pack/GOnSecureCommunicationLib/lib/i386/Release-iphoneos/libGOnSecureCommunicationLib.a', '-output', 'gon_build/out/pack/GOnSecureCommunicationLib/lib/Release/libGOnSecureCommunicationLib.a', '-create']
        exec_command(command, cwd=HG_ROOT, shell=False)

    def pack(self, ifilter=None):
        print_sub_header('pack')
        if self.devbotnet_slave_info.arch_target in [lib.dev_env.devbotnet.ARCH_TARGET_LINUX, lib.dev_env.devbotnet.ARCH_TARGET_LINUX_64]:
            return self._pack_cxfreeze(ifilter)
        print (self.devbotnet_slave_info.arch_target, lib.dev_env.devbotnet.ARCH_TARGET_MAC)
        if self.devbotnet_slave_info.arch_target in [lib.dev_env.devbotnet.ARCH_TARGET_MAC]:
            return self._pack_py2app(ifilter)
        if self.devbotnet_slave_info.arch_target in [lib.dev_env.devbotnet.ARCH_TARGET_WIN, lib.dev_env.devbotnet.ARCH_TARGET_WIN_64]:
            return self._pack_pyinstall(ifilter)
        return 0

    def _is_in_package(self, module_name, package_names):
        for package_name in package_names:
            if module_name.startswith(package_name):
                return True
        return False

    def _filter_by_modules(self, setup_files):
        result = []
        pacakge_list = DEV_PACKAGE_LIST[self.commandline_options.get_release_package_list()]

        for setup_file in setup_files:
            if self._is_in_package(self.dev_env.py_filename_to_modulename(setup_file), pacakge_list):
                result.append(setup_file)
        return result


    def _pack_cxfreeze(self, ifilters):
        setup_files = self.dev_env.select_files_with_suffix(ROOT_PY, '.cxfreeze.py', ifilters=ifilters)

        if ifilters is None:
            setup_files = self._filter_by_modules(setup_files)

        for setup_file in setup_files:
            print os.path.basename(setup_file)
            std_out_filename = os.path.join(self.dev_env.temp_root, '%s.pack.stdout' % os.path.basename(setup_file))
            std_err_filename = os.path.join(self.dev_env.temp_root, '%s.pack.stderr' % os.path.basename(setup_file))
            std_out_file = open(std_out_filename, 'w')
            std_err_file = open(std_err_filename, 'w')

            env = os.environ
            env['PYTHONPATH'] = lib.dev_env.path_setup.generate_path()
            command = [self.dev_env.tools_setup.get_python(), setup_file, 'build_exe']
            result = subprocess.call(command, cwd=os.path.dirname(setup_file), env=env, stdout=std_out_file, stderr=std_err_file)

            std_out_file.close()
            std_err_file.close()
            if self._compare_files(std_out_filename) or self._compare_files(std_err_filename):
                print "ERROR packing",  setup_file
                return 1
        return 0

    def _pack_py2app(self, ifilters):
        print ("_pack_py2app")
        setup_files = self.dev_env.select_files_with_suffix(ROOT_PY, '.py2app.setup.py', ifilters=ifilters)
        if ifilters is None:
            setup_files = self._filter_by_modules(setup_files)
        for setup_file in setup_files:
            print os.path.basename(setup_file)
            std_out_filename = os.path.join(self.dev_env.temp_root, '%s.pack.stdout' % os.path.basename(setup_file))
            std_err_filename = os.path.join(self.dev_env.temp_root, '%s.pack.stderr' % os.path.basename(setup_file))
            std_out_file = open(std_out_filename, 'w')
            std_err_file = open(std_err_filename, 'w')

            env = os.environ
            env['PYTHONPATH'] = lib.dev_env.path_setup.generate_path()
            command = [self.dev_env.tools_setup.get_python(), setup_file, 'py2app']
            result = subprocess.call(command, cwd=os.path.dirname(setup_file), env=env, stdout=std_out_file, stderr=std_err_file)

            std_out_file.close()
            std_err_file.close()
            if self._compare_files(std_out_filename) or self._compare_files(std_err_filename):
                print "ERROR packing",  setup_file
                return 1
        return 0

    def _pack_py2exe(self, ifilters):
        setup_files = self.dev_env.select_files_with_suffix(ROOT_PY, '.py2exe.setup.py',ifilters=ifilters)
        if ifilters is None:
            setup_files = self._filter_by_modules(setup_files)
        for setup_file in setup_files:
            print os.path.basename(setup_file)
            std_out_filename = os.path.join(self.dev_env.temp_root, '%s.pack.stdout' % os.path.basename(setup_file))
            std_err_filename = os.path.join(self.dev_env.temp_root, '%s.pack.stderr' % os.path.basename(setup_file))
            std_out_file = open(std_out_filename, 'w')
            std_err_file = open(std_err_filename, 'w')

            env = os.environ
            env['PYTHONPATH'] = lib.dev_env.path_setup.generate_path()
            command = [self.dev_env.tools_setup.get_python(), setup_file, 'py2exe']
            result = subprocess.call(command, cwd=os.path.dirname(setup_file), env=env, stdout=std_out_file, stderr=std_err_file)

            std_out_file.close()
            std_err_file.close()
            if self._compare_files(std_out_filename) or self._compare_files(std_err_filename):
                print "ERROR packing",  setup_file
                return 1
        return 0

    def _pack_pyinstall(self, ifilters):
        setup_files = self.dev_env.select_files_with_suffix(ROOT_PY, '.pyinstaller.spec',ifilters=ifilters)
        if ifilters is None:
            setup_files = self._filter_by_modules(setup_files)
        for setup_file in setup_files:
            app_name = os.path.basename(os.path.dirname(setup_file))
            print os.path.basename(setup_file), app_name
            std_out_filename = os.path.join(self.dev_env.temp_root, '%s.pack.stdout' % os.path.basename(setup_file))
            std_err_filename = os.path.join(self.dev_env.temp_root, '%s.pack.stderr' % os.path.basename(setup_file))
            std_out_file = open(std_out_filename, 'w')
            std_err_file = open(std_err_filename, 'w')

            env = os.environ
            env['PYTHONPATH'] = lib.dev_env.path_setup.generate_path()
            dist_path =  self.dev_env.generate_pack_destination(app_name)
            build_path =  self.dev_env.generate_build_destination(app_name)

            if os.path.exists(dist_path):
                shutil.rmtree(dist_path)
            if os.path.exists(build_path):
                shutil.rmtree(build_path)

            command = ['pyinstaller', '--distpath', dist_path, '-y', '--workpath', build_path, setup_file]

            if app_name in ['gon_server_gateway_service', 'gon_server_management_service', 'gon_config_service']:
                command.append('--uac-admin')

            print(command, os.path.dirname(setup_file))
            result = subprocess.call(command, cwd=os.path.dirname(setup_file), env=env, stdout=std_out_file, stderr=std_err_file)

            std_out_file.close()
            std_err_file.close()
            if self._compare_files(std_out_filename) or self._compare_files(std_err_filename):
                print "ERROR packing",  setup_file
                return 1
        return 0

    def gen_installers(self):
        print_sub_header('generate installers')
        if self.devbotnet_slave_info.arch_target in [lib.dev_env.devbotnet.ARCH_TARGET_LINUX, lib.dev_env.devbotnet.ARCH_TARGET_LINUX_64]:
            return self._gen_installers_linux()
        return 0

    def gen_gpms(self):
        print_sub_header('generate gpms')
        if self.devbotnet_slave_info.arch_target in [lib.dev_env.devbotnet.ARCH_TARGET_LINUX, lib.dev_env.devbotnet.ARCH_TARGET_LINUX_64]:
            return self._gen_gpms_linux()
        return 0

    def gen_gpm_for_gon_os(self):
        print_sub_header('generate gpm_for_gon_os')
        if self.devbotnet_slave_info.arch_target in [lib.dev_env.devbotnet.ARCH_TARGET_LINUX, lib.dev_env.devbotnet.ARCH_TARGET_LINUX_64]:
            rc = self._gen_tar_for_gon_os_linux()
            if rc != 0:
                return rc
# twa, disabled generation of gpm-package for gon_os_gon_client because the client is now build into gon_os.
#            return self._gen_gpm_for_gon_os_linux()
        return 0

    def _check_folders_for_rpmbuild(self, rpm_build_root):
        if os.path.exists(rpm_build_root):
            shutil.rmtree(rpm_build_root)

        rpm_folders = ['SOURCES', 'SPECS', 'BUILD', 'BUILDROOT', 'RPMS', 'SRPMS']
        for rpm_folder in rpm_folders:
            try:
                os.makedirs(os.path.join(rpm_build_root, rpm_folder))
            except:
                pass

    def _add_files_to_tarfile(self, include_filelist_filename, source_path, tar_dest, tarfile):
        include_filelist_file = open(include_filelist_filename, 'r')
        include_filelist = include_filelist_file.read().splitlines()
        for include_filelist_line in include_filelist:
            include_filelist_line = include_filelist_line.strip()
            if include_filelist_line.startswith('#'):
                continue
            for filename in glob.glob(os.path.join(source_path, include_filelist_line)):
                filename_abs = os.path.normpath(filename)
                filename_rel = os.path.relpath(filename_abs, source_path)
                tarfile.add(filename_abs, os.path.join(tar_dest, filename_rel))

    def _gen_installers_linux(self):
        src_folder_with_arch_java = 'linux.gtk.x86'
        if self.devbotnet_slave_info.arch_target == lib.dev_env.devbotnet.ARCH_TARGET_LINUX_64:
            src_folder_with_arch_java = 'linux.gtk.x86_64'

        dest_folder_with_arch = self.devbotnet_slave_info.arch_target

        rpm_build_root_src = os.path.join(HG_ROOT, 'setup', 'release', 'linux', 'rpmbuild')
        rpm_build_root = os.path.join(HG_ROOT, 'gon_build', 'out', 'build', 'rpmbuild')
        self._check_folders_for_rpmbuild(rpm_build_root)

        gon_server_foldername = 'gon_server_%d.%d.%d-%d' % (self.dev_env.version.get_version_major(), self.dev_env.version.get_version_minor(), self.dev_env.version.get_version_bugfix(), self.dev_env.version.get_version_build_id())
        dest_gon_server_base = os.path.join(gon_server_foldername, 'opt', 'giritech', 'soft', gon_server_foldername)

        tarfilename = os.path.join(rpm_build_root, 'SOURCES', gon_server_foldername+'.tar.bz2')
        mytarfile = tarfile.open(tarfilename, mode='w:bz2')
        mytarfile.add(os.path.join(self.dev_env.get_build_pack_root(), 'gon_server_gateway_service', 'linux'), os.path.join(dest_gon_server_base, 'gon_server_gateway_service', dest_folder_with_arch))
        mytarfile.add(os.path.join(self.dev_env.get_build_pack_root(), 'gon_server_management_service', 'linux'), os.path.join(dest_gon_server_base, 'gon_server_management_service', dest_folder_with_arch))
        mytarfile.add(os.path.join(self.dev_env.get_build_pack_root(), 'gon_server_installer', 'linux'), os.path.join(dest_gon_server_base, 'gon_server_installer', dest_folder_with_arch))
        mytarfile.add(os.path.join(self.dev_env.get_build_pack_root(), src_folder_with_arch_java, 'gon_config'), os.path.join(dest_gon_server_base, 'gon_config', dest_folder_with_arch))
        mytarfile.add(os.path.join(self.dev_env.get_build_pack_root(), 'gon_config_service', 'linux'), os.path.join(dest_gon_server_base, 'gon_config_service', dest_folder_with_arch))
        mytarfile.add(os.path.join(self.dev_env.get_build_pack_root(), src_folder_with_arch_java, 'gon_client_management'), os.path.join(dest_gon_server_base, 'gon_client_management', dest_folder_with_arch))

        self._add_files_to_tarfile(os.path.join(HG_ROOT, 'setup', 'release', 'config_include_linux.filelist'), os.path.join(HG_ROOT, 'setup', 'release', 'config'), os.path.join(dest_gon_server_base, 'config'), mytarfile)
        mytarfile.add(os.path.join(HG_ROOT, 'setup', 'release', 'linux', 'instance_templates'), os.path.join(dest_gon_server_base, 'instance_templates'))
        mytarfile.add(os.path.join(HG_ROOT, 'setup', 'key_store', 'gon_config.cacerts'), os.path.join(dest_gon_server_base, 'gon_config', dest_folder_with_arch, 'gon_config.cacerts'))
        mytarfile.add(os.path.join(HG_ROOT, 'setup', 'key_store', 'gon_management_client.cacerts'), os.path.join(dest_gon_server_base, 'gon_client_management', dest_folder_with_arch, 'gon_management_client.cacerts'))

        mytarfile.add(os.path.join(HG_ROOT, 'web_app', 'www_root', 'dme'), os.path.join(dest_gon_server_base, 'config', 'www_root', 'dme'))
        mytarfile.add(os.path.join(HG_ROOT, 'web_app', 'www_root', 'web_app_demo', 'vacation_app', 'build'), os.path.join(dest_gon_server_base, 'config', 'www_root',  'web_app_demo', 'vacation_app', 'build'))
        mytarfile.close()

        file_list = []
        mytarfile = tarfile.open(tarfilename, mode='r:bz2')
        mytarfileitem = mytarfile.next()
        while mytarfileitem:
            if mytarfileitem.isfile():
                itemname = mytarfileitem.name[len(gon_server_foldername):]
                file_list.append(itemname)
            mytarfileitem = mytarfile.next()

        dictionary = {}
        dictionary['GON_VERSION'] = self.dev_env.version.get_version_string_num().strip()
        dictionary['GON_VERSION_MAJOR'] = "%d" % self.dev_env.version.get_version_major()
        dictionary['GON_VERSION_MINOR'] = "%d" % self.dev_env.version.get_version_minor()
        dictionary['GON_VERSION_BUGFIX'] = "%d" % self.dev_env.version.get_version_bugfix()
        dictionary['GON_VERSION_BUILD_ID'] = "%d" % self.dev_env.version.get_version_build_id()
        dictionary['GON_FILELIST'] = '"' + '"\n"'.join(file_list) + '"'
        dictionary['GON_RPM_TARGET'] = self.devbotnet_slave_info.rpm_target
        dictionary['GON_DEST_FOLDER_WITH_ARCH'] = dest_folder_with_arch


        rpm_spec_base_filename = os.path.join(rpm_build_root_src, 'SPECS', 'gon_server.spec.template')
        rpm_spec_filename = os.path.join(rpm_build_root, 'SPECS', 'gon_server.spec')
        self.dev_env.expand_in_file(rpm_spec_base_filename, rpm_spec_filename, dictionary)

        std_out_filename = os.path.join(self.dev_env.temp_root, 'gen_installers.stdout')
        std_err_filename = os.path.join(self.dev_env.temp_root, 'gen_installers.stderr')
        std_out_file = open(std_out_filename, 'w')
        std_err_file = open(std_err_filename, 'w')

        env = os.environ
        command = ['rpmbuild', '--define', "_topdir %s" % rpm_build_root, '--bb', rpm_spec_filename]
        result = subprocess.call(command, env=env, stdout=std_out_file, stderr=std_err_file)
        std_out_file.close()
        std_err_file.close()

        std_out_file.close()
        std_err_file.close()
        if self._compare_files(std_out_filename) or self._compare_files(std_err_filename):
            print "ERROR gen_installers"
            return 1

        pack_root_src = os.path.join(rpm_build_root, 'RPMS')
        pack_root_dest = os.path.join(self.dev_env.get_pack_root(), 'rpms')
        try:
            shutil.rmtree(pack_root_dest)
        except:
            pass
        shutil.copytree(pack_root_src, pack_root_dest)


    def _gen_gpms_linux(self):
        client_file_packages_root = os.path.normpath(os.path.join(HG_ROOT, '..', 'gon_client_file_packages'))

        out_gpms_root = os.path.normpath(os.path.join(HG_ROOT, 'gon_build', 'out', 'gpm'))
        if os.path.isdir(out_gpms_root):
            shutil.rmtree(out_gpms_root)

        setup_release_root = os.path.normpath(os.path.join(HG_ROOT, 'setup', 'dev_env', 'gen_gpms'))

        # Generate client_packages
        env = os.environ
        env['PYTHONPATH'] = lib.dev_env.path_setup.generate_path()
        command = [self.dev_env.tools_setup.get_python(), os.path.join(self.dev_env.py_root, 'appl', 'gon_config_service', 'gon_config_service.py') , '--generate_gpms']
        print command
        print setup_release_root
        rc = subprocess.call(command, cwd=setup_release_root, env=env)

        out_pack_gpms = os.path.join(self.dev_env.get_pack_root(), 'gpm', 'gpms')
        if os.path.exists(out_pack_gpms):
            shutil.rmtree(out_pack_gpms)
        os.makedirs(out_pack_gpms)

        for filename in glob.glob(os.path.join(out_gpms_root, 'gpms','*.gpm') ):
            shutil.copy(filename, out_pack_gpms)

        # copy client_packages pre-generated gpms
        for filename in glob.glob(os.path.join(client_file_packages_root, 'gpm', 'gpms','*.gpm') ):
            shutil.copy(filename, out_pack_gpms)

        out_pack_gpmdefs = os.path.join(self.dev_env.get_pack_root(), 'gpm', 'gpmdefs', 'prebuild')
        if os.path.exists(out_pack_gpmdefs):
            shutil.rmtree(out_pack_gpmdefs)
        os.makedirs(out_pack_gpmdefs)

        for filename in glob.glob(os.path.join(client_file_packages_root, 'gpm', 'gpmdefs','*.gpmdef.xml') ):
            shutil.copy(filename, out_pack_gpmdefs)


    def _move_program_for_arch(self, platforms, programs):
        for platform_from, platform_to in platforms:
            for program in programs:
                gon_config_src = os.path.join(self.dev_env.get_pack_root(), platform_from, program)
                gon_config_dest = os.path.join(self.dev_env.get_pack_root(), program, platform_to)
                print(gon_config_src, gon_config_dest)
                if os.path.exists(gon_config_src):
                    print "Moving %s to %s" % (gon_config_src, gon_config_dest)
                    if os.path.exists(gon_config_dest):
                        shutil.rmtree(gon_config_dest)
                    shutil.move(gon_config_src, gon_config_dest)

    def _gen_tar_for_gon_os_linux(self):
        pack_gpms_root = os.path.join(self.dev_env.get_pack_root(), 'tgzs')
        if not os.path.exists(pack_gpms_root):
            os.makedirs(pack_gpms_root)
        gon_client_root = os.path.join(self.dev_env.get_pack_root(), 'gon_client')

        tar_filename = os.path.join(pack_gpms_root, 'gon_client_gonos.tgz')
        if os.path.exists(tar_filename):
            os.remove(tar_filename)

        tar = tarfile.open(tar_filename, "w:gz")
        tar.add(os.path.join(self.dev_env.get_pack_root(), 'gon_client'), arcname=os.path.join('/', 'opt', 'giritech', 'gon_client'))
        tar.close()

        linux_root = os.path.join(self.dev_env.get_pack_root(), 'gon_client')
        if os.path.exists(linux_root):
            shutil.rmtree(linux_root)

        return 0

    def _gen_gpm_for_gon_os_linux(self):
        build_root = os.path.join(HG_ROOT, 'gon_build', 'out', 'build', "gpm_for_gon_os")
        if os.path.exists(build_root):
            shutil.rmtree(build_root)

        setup_src = os.path.join(HG_ROOT, 'setup', 'release', 'gon_os')
        shutil.copytree(setup_src, build_root)

        os.makedirs(os.path.join(build_root, 'root'))
        shutil.copytree(os.path.join(HG_ROOT, 'setup', 'release', 'client_shortcuts'), os.path.join(build_root, 'root', 'client_shortcuts'))
        shutil.copytree( os.path.join(self.dev_env.get_pack_root(), 'gon_client'), os.path.join(build_root, 'root', 'gon_client'))

        # Generate client_packages
        env = os.environ
        env['PYTHONPATH'] = lib.dev_env.path_setup.generate_path()
        command = [self.dev_env.tools_setup.get_python(), os.path.join(self.dev_env.py_root, 'appl', 'gon_config_service', 'gon_config_service.py') , '--generate_gpms']
        rc = subprocess.call(command, cwd=build_root, env=env)

        pack_gpms_root = os.path.join(self.dev_env.get_pack_root(), 'gpms')
        if not os.path.exists(pack_gpms_root):
            os.makedirs(pack_gpms_root)

        for filename in glob.glob(os.path.join(build_root, 'gpm', 'gpms','*.gpm') ):
            shutil.copy(filename, pack_gpms_root)

        linux_root = os.path.join(self.dev_env.get_pack_root(), 'gon_client')
        if os.path.exists(linux_root):
            shutil.rmtree(linux_root)

    def distribute(self, version):
        print_sub_header('distribute')
        platform = detect_platform()

        folders_in_pack_to_remove = ['build']
        for folder_to_remove in folders_in_pack_to_remove:
            folder = os.path.join(self.dev_env.get_pack_root(), folder_to_remove)
            if os.path.exists(folder):
                shutil.rmtree(folder)

#        platforms = [('win32.win32.x86', 'win'), ('win32.win32.x86_64', 'win_64')]
#        programs = ['gon_config', 'gon_client_management']
#        self._move_program_for_arch(platforms, programs)

        from_path = self.dev_env.get_pack_root()
        to_path =  os.path.join(HG_ROOT,self.commandline_options.build_store(), version.get_version_string().strip())
        if not os.path.exists(to_path):
            os.makedirs(to_path)


        # Sign packages if Mac App
        if not self.commandline_options.skip_sign():
            if platform == TARGET_PLATFORM_MAC:
                app_path = os.path.join(from_path,'gon_client','mac','gon_client.app')

                print('unlock keychain')
                home = os.path.expanduser("~")
                command = ['/usr/bin/security', 'unlock-keychain', '-p', 'GiriGiri007', os.path.join(home, 'Library', 'Keychains', 'login.keychain')]
                rc = subprocess.call(command, cwd=from_path)
                if rc != 0:
                    return False

                print('codesign app')
                command = ['/usr/bin/codesign', '--deep', '--timestamp=none', '-sDeveloper ID Application: Soliton Systems Development Center Europe A/S (VM5Y62B2KP)', app_path]
                rc = subprocess.call(command, cwd=from_path)
                if rc != 0:
                    return False

                print('verify app')
                command = ['/usr/bin/codesign', '--deep', '--timestamp=none', '-v', app_path]
                rc = subprocess.call(command, cwd=from_path)
                if rc != 0:
                    return False

                print('Notarize app')
                app_archive_path = os.path.join(from_path,'gon_client','mac','gon_client.app.zip')
                command = ['/usr/bin/ditto', '-c', '-k', '--keepParent', app_path, app_archive_path]
                rc = subprocess.call(command, cwd=from_path)
                if rc != 0:
                    print('Notarize app.failed.1')
                    return False

                command = ['/usr/bin/ditto', '-c', '-k', '--keepParent', app_path, app_archive_path]
                rc = subprocess.call(command, cwd=from_path)
                if rc != 0:
                    print('Notarize app.failed.2')
                    return False

                command = ['/usr/bin/xcrun', 'altool', '--notarize-app', '--primary-bundle-id', "com.giritech.gonclient", '--username', "bsh@solitonsystems.com", '--password', "@keychain:AC_PASSWORD", '--file', app_archive_path]
                rc = subprocess.call(command, cwd=from_path)
                if rc != 0:
                    print('Notarize app.failed.3')
                    return False

                if os.path.exists(app_archive_path):
                    os.remove(app_archive_path)
                print('Notarize app.done')


        tar_filename =  os.path.join(to_path, 'distribute.%s.tgz' % detect_platform())
        print "FROM :", from_path
        print "TO   :", to_path
        print
        print "DO YOU WANT DISTRIBUTE THIS (YES/NO)?"
        if not self.auto_yes:
            response = sys.stdin.read(3)
        else:
            response = 'YES'
        if response == 'YES':
            if self.auto_yes:
                print "AUTO YES"

            # Build tar file to distribute
            if os.path.exists(tar_filename):
                os.remove(tar_filename)

            tar = tarfile.open(tar_filename, "w:gz")
            tar.add(from_path, arcname='')
            tar.close()

#            transport = paramiko.Transport((DEV_BUILD_HOST, 22))
#            transport.connect(username=DEV_BUILD_HOST_USERNAME, password=DEV_BUILD_HOST_PASSWORD)
#            sftp = paramiko.SFTPClient.from_transport(transport)

#            sftp.put(tar_filename, to_path)

#            sftp.close()
#            transport.close()

            print "DISTRIBUTE DONE"
            return True
        else:
            print "DISTRIBUTE ABORTED"
            return False



# ========================================================================================================================
WEB_COMMAND_clear = 'clear'
WEB_COMMAND_serve = 'serve'


def do_web_service(dev_env, devbotnet_slave_info, cmd):
    pid_filename = "development.pid"
    log_filename = "development.log"
    ini_filename = "development.ini"
    db_filename = "development.db"

    monitor_root = os.path.join(devbotnet_slave_info.dev_env.py_root, 'setup','tools', 'monitor')

    if cmd == WEB_COMMAND_clear:
        print_header('Web - clear')
        db_filename_abs = os.path.join(monitor_root, db_filename)
        if os.path.exists(db_filename_abs):
            os.unlink(db_filename_abs)
            print "Databasefile has been removed"
        log_filename_abs = os.path.join(monitor_root, log_filename)
        if os.path.exists(log_filename_abs):
            os.unlink(log_filename_abs)
    else:
        if cmd == WEB_COMMAND_serve:
            print_header('Web - serve')
            try:
                service_env = os.environ
                service_env['PYTHONPATH'] = lib.dev_env.path_setup.generate_path()
                command = [dev_env.tools_setup.get_paster(), 'serve', '--reload', 'development.ini']
                command_cwd = os.path.join(devbotnet_slave_info.dev_env.py_root, 'setup', 'tools', 'monitor')
                print command
                subprocess.call(command, cwd=command_cwd, env=service_env)
            except:
                traceback.print_exc()
                time.sleep(4)

# ========================================================================================================================
class JobDoRelease(lib.dev_env.devbotnet_jobs.Job):
    def __init__(self, dev_env, commandline_options, devbotnet_slave_info=None):
        lib.dev_env.devbotnet_jobs.Job.__init__(self, dev_env.temp_root)
        self.dev_env = dev_env
        self.commandline_options = commandline_options
        self.job_info = lib.dev_env.devbotnet_jobs.JobInfo()
        self.job_info.type = lib.dev_env.devbotnet_jobs.JobInfo.JOB_TYPE_RELEASE
        if devbotnet_slave_info is not None:
            self.job_info.slave_name = devbotnet_slave_info.name
            self.job_info.slave_arch = devbotnet_slave_info.arch
            self.job_info.slave_arch_target = devbotnet_slave_info.arch_target

    def do_run(self):
        command = [self.dev_env.tools_setup.get_python(), 'config.py', '--cmd_release', 'all']
        result = subprocess.call(command, cwd=HG_ROOT, stdout=self.stdout_file, stderr=self.stderr_file)
        if result != 0:
            return lib.dev_env.devbotnet_jobs.Job.JOB_STATUS_DONE_WITH_ERRORS
        return lib.dev_env.devbotnet_jobs.Job.JOB_STATUS_DONE

    def get_job_info(self, job_info=None):
        return lib.dev_env.devbotnet_jobs.Job.get_job_info(self, self.job_info)



# ========================================================================================================================
GON_SERVICE_COMMAND_start = 'start'
GON_SERVICE_COMMAND_stop = 'stop'
GON_SERVICE_COMMAND_init = 'init'
GON_SERVICE_COMMAND_build_gpms = 'build_gpms'
GON_SERVICE_COMMAND_update_license = 'update_license'


GLOBAL_PRINT_LOCK = threading.Lock()

class ServiceJob(threading.Thread):
    def __init__(self, dev_env, name, command, cwd, use_stop_file=False):
        threading.Thread.__init__(self, name=name)
        self.dev_env = dev_env
        self.name = name
        self.command = command
        self.cwd = cwd
        self.use_stop_file = use_stop_file
        self.setDaemon(True)

    @classmethod
    def print_thread_safe(cls, message):
        GLOBAL_PRINT_LOCK.acquire()
        print message
        GLOBAL_PRINT_LOCK.release()

    def run(self):
        std_out_file = open(os.path.join(self.dev_env.temp_root, '%s.stdout' % self.name), 'w')
        std_err_file = open(os.path.join(self.dev_env.temp_root, '%s.stdout' % self.name), 'w')
        run_filename = os.path.join(self.dev_env.temp_root, '%s.run' % self.name)
        service_env = os.environ
        service_env['PYTHONPATH'] = lib.dev_env.path_setup.generate_path()

        if self.use_stop_file:
            self.command.append('--foreground_with_stop_file')
            self.command.append(run_filename)
            run_file = open(run_filename, 'w')
            run_file.write("x")
            run_file.close()

        print "starting", self.command
        process = subprocess.Popen(self.command, cwd=self.cwd, env=service_env, stdout=std_out_file, stderr=std_err_file)
        print "waiting",  self.command
        time.sleep(10)
        rc = process.poll()
        print rc
        if(rc is None):
            pid_file = open(os.path.join(self.dev_env.temp_root,'%s.pid' % self.name), 'w')
            pid_file.write(str(process.pid))
            pid_file.close()
            ServiceJob.print_thread_safe("%30s running" % self.name)
        else:
            ServiceJob.print_thread_safe("%30s failed to start" % self.name)
            try:
                os.remove(run_filename)
            except:
                pass
        process.wait()

    def terminate_by_pid(self):
        run_filename = os.path.join(self.dev_env.temp_root, '%s.run' % self.name)
        pid_filename = os.path.join(self.dev_env.temp_root,'%s.pid' % self.name)
        if os.path.exists(run_filename):
            os.remove(run_filename)
            time.sleep(4)
            if not os.path.exists(pid_filename):
                return False

        if not os.path.exists(pid_filename):
            ServiceJob.print_thread_safe("%30s failed to stop, pid not found." % self.name)
            return False

        pid_file = open(pid_filename, 'r')
        pid = int(pid_file.read())
        pid_file.close()
        os.remove(pid_filename)

        if self.use_stop_file:
            time.sleep(5)

        ok = False
        if sys.platform == 'win32':
            try:
                import win32api, win32con
                handle = win32api.OpenProcess(win32con.PROCESS_TERMINATE, 0, pid)
                win32api.TerminateProcess(handle, 0)
                win32api.CloseHandle(handle)
                ok = True
            except:
                if self.use_stop_file:
                    ok = True
                else:
                    (etype, evalue, etrace) = sys.exc_info()
                    ServiceJob.print_thread_safe("%30s failed to stop %s" % (self.name, evalue))
                    return

        elif sys.platform == 'linux2':
            try:
                import signal
                os.kill(pid, signal.SIGTERM)
                ok = True
            except OSError:
                if self.use_stop_file:
                    ok = True
                else:
                    (etype, evalue, etrace) = sys.exc_info()
                    ServiceJob.print_thread_safe("%30s failed to stop %s" % (self.name, evalue))
                    return
        if ok:
            ServiceJob.print_thread_safe("%30s stopped" % (self.name))
        else:
            ServiceJob.print_thread_safe("%30s failed to stop" % (self.name))


def do_gon_services(dev_env, dev_env_installation, command):
    if command in [GON_SERVICE_COMMAND_start, GON_SERVICE_COMMAND_stop]:
        service_job_gateway_command = [dev_env.tools_setup.get_python(), os.path.join(dev_env.py_root, 'appl', 'gon_server_gateway_service', 'gon_server_gateway_service.py')]
        service_job_gateway = ServiceJob(dev_env, 'gon_gateway_service', service_job_gateway_command, dev_env_installation.gon_server_gateway_service_root, use_stop_file=True)

        service_job_management_command = [dev_env.tools_setup.get_python(), os.path.join(dev_env.py_root, 'appl', 'gon_server_management_service', 'gon_server_management_service.py')]
        service_job_management = ServiceJob(dev_env, 'gon_management_service', service_job_management_command, dev_env_installation.gon_server_management_service_root, use_stop_file=True)

        service_job_client_management_command = [dev_env.tools_setup.get_python(), os.path.join(dev_env.py_root, 'appl', 'gon_client_management_service', 'gon_client_management_service.py'), '--serve']
        service_job_client_management = ServiceJob(dev_env, 'gon_client_management_service', service_job_client_management_command, dev_env_installation.gon_client_management_service_root)

        service_job_config_command = [dev_env.tools_setup.get_python(), os.path.join(dev_env.py_root, 'appl', 'gon_config_service', 'gon_config_service.py'), '--serve']
        service_job_config = ServiceJob(dev_env, 'gon_config_service', service_job_config_command, dev_env_installation.gon_config_service_root)

        if command == GON_SERVICE_COMMAND_start:
            print_header("G/On Services - Starting services")
            dev_env_installation.restore_plugins(restore_complete=False)
            service_job_gateway.start()
            time.sleep(3)
            service_job_management.start()
            time.sleep(3)
            service_job_client_management.start()
            time.sleep(3)
            service_job_config.start()
            time.sleep(15)

        elif command == GON_SERVICE_COMMAND_stop:
            print_header("G/On Services - Stopping services")
            service_job_gateway.terminate_by_pid()
            service_job_management.terminate_by_pid()
            service_job_client_management.terminate_by_pid()
            service_job_config.terminate_by_pid()

    if command in [GON_SERVICE_COMMAND_init]:
        print_header("G/On Services - Initiaizing")
        dev_env_installation.restore_installation_root()

    if command in [GON_SERVICE_COMMAND_build_gpms]:
        print_header("G/On Services - Build gpms")
        dev_env_installation.generate_gpms()

    if command in [GON_SERVICE_COMMAND_update_license]:
        print_header("G/On Services - Update license")
        dev_env_installation.update_license()


# ========================================================================================================================
#
DEVBOTNET_COMMAND_start = 'start'
DEVBOTNET_COMMAND_stop = 'stop'
DEVBOTNET_COMMAND_restart = 'restart'

def do_devbotnet_service(dev_env, dev_env_installation, command):
    service_devbotnet_command = [dev_env.tools_setup.get_python(), os.path.join(dev_env.py_root, 'lib', 'dev_env', 'devbotnet_slave_main.py')]
    service_devbotnet = ServiceJob(dev_env, 'devbotnet_service', service_devbotnet_command, dev_env.py_root, use_stop_file=True)

    if command == DEVBOTNET_COMMAND_start:
        print_header("devbotnet - Starting services")
        service_devbotnet.start()
        time.sleep(2)

    elif command == DEVBOTNET_COMMAND_stop:
        print_header("devbotnet - Stopping services")
        service_devbotnet.terminate_by_pid()

    elif command == DEVBOTNET_COMMAND_restart:
        print_header("devbotnet - Stopping services")
        time.sleep(2)
        service_devbotnet.terminate_by_pid()
        time.sleep(2)
        print_header("devbotnet - Starting services")
        service_devbotnet.start()
        time.sleep(2)


# ========================================================================================================================
#

class SourceGen(object):
    SOURCE_GEN_WS_SETUP = { 'admin_ws'        : {'wsdl_filename': os.path.join('common', 'admin_ws', 'binding.wsdl'),         'py_out_folder': os.path.join('py', 'components', 'admin_ws', 'ws')},
                            'admin_deploy_ws' : {'wsdl_filename': os.path.join('common', 'admin_deploy_ws', 'binding.wsdl'),  'py_out_folder': os.path.join('py', 'components', 'admin_deploy_ws', 'ws')},
                            'server_config_ws': {'wsdl_filename': os.path.join('common', 'server_config_ws', 'binding.wsdl'), 'py_out_folder': os.path.join('py', 'components', 'server_config_ws', 'ws')}
                           }

    def __init__(self, dev_env):
        self.dev_env = dev_env

    def generate_ws(self, ws_id):
        print_header('SourceGenerator - WebService')

        java_component_id = ws_id
        wsdl_filename = os.path.join(self.dev_env.hg_root, SourceGen.SOURCE_GEN_WS_SETUP[ws_id]['wsdl_filename'])
        py_out_folder = os.path.join(self.dev_env.hg_root, SourceGen.SOURCE_GEN_WS_SETUP[ws_id]['py_out_folder'])

        print_sub_header('Generating py-files')
        rc = lib.dev_env.source_gen.source_gen_wsdl_to_py(self.dev_env.tools_setup, wsdl_filename, py_out_folder)
        if not rc:
            print "Generating py-files failed"
            return 1

        print_sub_header('Generating java-files')
        rc = lib.dev_env.source_gen.source_gen_wsdl_to_java(self.dev_env.tools_setup, wsdl_filename, java_component_id)
        if not rc:
            print "Generating java-files failed"
            return 1


        return 0



# ========================================================================================================================
#
class CMDDictionary(object):
    GEN_COMMAND = 'gen'

    def __init__(self, dev_env):
        self.dev_env = dev_env


    def run(self, command):
        if command == CMDDictionary.GEN_COMMAND:
            return self.run_gen()
        print "Invalid command"
        return 1

    def run_gen(self):
        print_header('Dictionary - generate from source')

        service_env = os.environ
        service_env['PYTHONPATH'] = lib.dev_env.path_setup.generate_path()
        command = ['python', os.path.join('py', 'setup', 'tools', 'dictionary.py'), "--generate_en_source"]
        print command
        rc = subprocess.call(command, env=service_env)
        if rc != 0:
            print "ERROR generating dictionary"
            return 1

        return 0


# ========================================================================================================================
#
def do_init(commandline_options):
    platform = detect_platform()

    if platform == TARGET_PLATFORM_LINUX:
        command = ['cmake', '-C', os.path.join(HG_ROOT, 'setup/init/cmake_init_linux64'), '-C', os.path.join(HG_ROOT, 'setup/init/cmake_init_release'), '-G', '"Unix Makefiles"', HG_ROOT]
    elif platform == TARGET_PLATFORM_MAC:
        command = ['cmake', '-C', os.path.join(HG_ROOT, 'setup/init/cmake_init_mac'), '-C', os.path.join(HG_ROOT, 'setup/init/cmake_init_release'), '-G', '"Unix Makefiles"', HG_ROOT]
    elif platform == TARGET_PLATFORM_WIN:
        command = ['cmake', '-C', os.path.join(HG_ROOT, 'setup/init/cmake_init_win'), '-C', os.path.join(HG_ROOT, 'setup/init/cmake_init_release'), '-G', '"NMake Makefiles"', HG_ROOT]

    if not os.path.exists(commandline_options.get_build_folder()):
        os.makedirs(commandline_options.get_build_folder())

    exec_command(command, cwd=commandline_options.get_build_folder(), shell=True)

# ========================================================================================================================
#
def do_unittest_cpp(commandline_options):
    pass

def do_unittest_py(commandline_options):
    version = lib.version.Version.create_current()
    dev_env = lib.dev_env.DevEnv(lib.dev_env.tools.ToolsSetup.create_default(), HG_ROOT, version)
    job = lib.dev_env.devbotnet_jobs_no_db.JobDoCoverage(dev_env, ifilter=commandline_options.unittest_ifilter(), efilter=commandline_options.unittest_efilter(), coverage_out_folder=os.path.join(HG_ROOT, '..', 'gon_build', 'coverage', 'py'))
    job.run()
    if job.get_status() == lib.dev_env.devbotnet_jobs_no_db.Job.JOB_STATUS_DONE_OK:
        return 0
    return 1

def do_unittest(commandline_options):
    if commandline_options.cmd_unittest() == 'py':
        return do_unittest_py(commandline_options)
    elif commandline_options.cmd_unittest() == 'cpp':
        return do_unittest_cpp(commandline_options)
    return 1

# ========================================================================================================================
def main():
    commandline_options = CommandlineOptions()

    if commandline_options.cmd_init() is not None:
        if not commandline_options.cmd_init() in [lib.dev_env.DevEnv.SETUP_COMMAND_normal, lib.dev_env.DevEnv.SETUP_COMMAND_release]:
            print "Invalid command '%s' for --cmd_init" %  commandline_options.cmd_init()
            return 1
        dev_env = lib.dev_env.DevEnv(commandline_options.get_tools_setup(), HG_ROOT, commandline_options.get_version())
        dev_env.setup(commandline_options.cmd_init())
        commandline_options.update_config_ini(commandline_options.cmd_init(), commandline_options.option_enable_cpp_build())
        do_init(commandline_options)
        return 0

    if commandline_options.cmd_gon_services() is not None:
        if not commandline_options.cmd_gon_services() in [GON_SERVICE_COMMAND_init, GON_SERVICE_COMMAND_start, GON_SERVICE_COMMAND_stop, GON_SERVICE_COMMAND_build_gpms, GON_SERVICE_COMMAND_update_license]:
            print "Invalid command '%s' for --cmd_gon_services" %  commandline_options.cmd_gon_services()
            return 1
        dev_env = lib.dev_env.DevEnv(commandline_options.get_tools_setup(), HG_ROOT, commandline_options.get_version())
        dev_env_installation = lib.dev_env.DevEnvInstallation(dev_env)
        do_gon_services(dev_env, dev_env_installation, commandline_options.cmd_gon_services())
        return 0

    if commandline_options.cmd_gon_client() is not None:
        if not commandline_options.cmd_gon_client() in ['run', 'first_start']:
            print "Invalid command '%s' for --cmd_gon_client" %  commandline_options.cmd_gon_client()
            return 1
        dev_env = lib.dev_env.DevEnv(commandline_options.get_tools_setup(), HG_ROOT, commandline_options.get_version())
        dev_env_installation = lib.dev_env.DevEnvInstallation(dev_env)
        if commandline_options.cmd_gon_client() == 'run':
            dev_env_installation.do_gon_client_run()
        elif commandline_options.cmd_gon_client() == 'first_start':
            dev_env_installation.do_gon_client_first_start()
        return 0

    if commandline_options.cmd_gon_client_stealth() is not None:
        if not commandline_options.cmd_gon_client_stealth() in ['run']:
            print "Invalid command '%s' for --cmd_gon_client_stealth" %  commandline_options.cmd_gon_client()
            return 1
        dev_env = lib.dev_env.DevEnv(commandline_options.get_tools_setup(), HG_ROOT, commandline_options.get_version())
        dev_env_installation = lib.dev_env.DevEnvInstallation(dev_env)
        if commandline_options.cmd_gon_client_stealth() == 'run':
            dev_env_installation.do_gon_client_stealth_run()
        return 0

    if commandline_options.cmd_release() is not None:
        if not commandline_options.cmd_release() in [DevEnvRelease.RELEASE_COMMAND_clean, DevEnvRelease.RELEASE_COMMAND_build, DevEnvRelease.RELEASE_COMMAND_pack, DevEnvRelease.RELEASE_COMMAND_gen_installers, DevEnvRelease.RELEASE_COMMAND_distribute, DevEnvRelease.RELEASE_COMMAND_gen_gpms, DevEnvRelease.RELEASE_COMMAND_gen_gpm_for_gon_os, DevEnvRelease.RELEASE_COMMAND_all]:
            print "Invalid command '%s' for --cmd_release" %  commandline_options.cmd_release()
            return 1
        dev_env = lib.dev_env.DevEnv(commandline_options.get_tools_setup(), HG_ROOT, commandline_options.get_version())
        devbotnet_slave_info = commandline_options.get_devbotnet_slave_info(dev_env)
        dev_env_release = DevEnvRelease(dev_env, commandline_options, devbotnet_slave_info)
        filters = None
        if commandline_options.option_ifilter() in DEV_PACKAGE_LIST:
            filters = []
            for filter in DEV_PACKAGE_LIST[commandline_options.option_ifilter()]:
                 filters.append(filter.split('.')[1] + '.')

        dev_env_release.info()
#        if commandline_options.cmd_release() in [DevEnvRelease.RELEASE_COMMAND_clean, DevEnvRelease.RELEASE_COMMAND_all]:
#            dev_env.setup(lib.dev_env.DevEnv.SETUP_COMMAND_release)
#            dev_env_release.clean()
        if commandline_options.cmd_release() in [DevEnvRelease.RELEASE_COMMAND_build, DevEnvRelease.RELEASE_COMMAND_all]:
            rc = dev_env_release.build()
            if rc != 0:
                return rc
        if commandline_options.cmd_release() in [DevEnvRelease.RELEASE_COMMAND_pack, DevEnvRelease.RELEASE_COMMAND_all]:
            dev_env_release.pack(filters)
        if commandline_options.cmd_release() in [DevEnvRelease.RELEASE_COMMAND_gen_installers, DevEnvRelease.RELEASE_COMMAND_distribute]:
            if commandline_options.get_release_generate_installers():
                dev_env_release.gen_installers()
            else:
                print "Generation of installers disabled in config_local.ini"
        if commandline_options.cmd_release() in [DevEnvRelease.RELEASE_COMMAND_gen_gpms]:
            dev_env_release.gen_gpms()
        if commandline_options.cmd_release() in [DevEnvRelease.RELEASE_COMMAND_gen_gpm_for_gon_os, DevEnvRelease.RELEASE_COMMAND_distribute]:
            if commandline_options.get_release_generate_gpm_for_gon_os():
                dev_env_release.gen_gpm_for_gon_os()
            else:
                print "Generation of gpm_for_gon_os disabled in config_local.ini"
        if commandline_options.cmd_release() in [DevEnvRelease.RELEASE_COMMAND_distribute]:
            ok = dev_env_release.distribute(commandline_options.get_version())
            if not ok:
                print "Failed to distribute"
                return 1
        return 0

    if commandline_options.cmd_web() is not None:
        if not commandline_options.cmd_web() in [WEB_COMMAND_clear, WEB_COMMAND_serve]:
            print "Invalid command '%s' for --cmd_web" %  commandline_options.cmd_web()
            return 1
        dev_env = lib.dev_env.DevEnv(commandline_options.get_tools_setup(), HG_ROOT, commandline_options.get_version())
        devbotnet_slave_info = commandline_options.get_devbotnet_slave_info(dev_env)
        do_web_service(dev_env, devbotnet_slave_info, commandline_options.cmd_web())
        return 0

    if commandline_options.cmd_source_gen():
        if not commandline_options.cmd_source_gen() in SourceGen.SOURCE_GEN_WS_SETUP.keys():
            print "Invalid command '%s' for --cmd_source_gen" %  commandline_options.cmd_source_gen()
            return 1
        dev_env = lib.dev_env.DevEnv(commandline_options.get_tools_setup(), HG_ROOT, commandline_options.get_version())
        source_generator = SourceGen(dev_env)
        return source_generator.generate_ws(commandline_options.cmd_source_gen())

    if commandline_options.cmd_dictionary():
        if not commandline_options.cmd_dictionary() in [CMDDictionary.GEN_COMMAND]:
            print "Invalid command '%s' for --cmd_dictionary" %  commandline_options.cmd_dictionary()
            return 1
        dev_env = lib.dev_env.DevEnv(commandline_options.get_tools_setup(), HG_ROOT, commandline_options.get_version())
        cmd_dictionary = CMDDictionary(dev_env)
        return cmd_dictionary.run(commandline_options.cmd_dictionary())

    if commandline_options.cmd_devbotnet() is not None:
        if not commandline_options.cmd_devbotnet() in [DEVBOTNET_COMMAND_start, DEVBOTNET_COMMAND_stop, DEVBOTNET_COMMAND_restart]:
            print "Invalid command '%s' for --cmd_devbotnet" %  commandline_options.cmd_devbotnet()
            return 1
        dev_env = lib.dev_env.DevEnv(commandline_options.get_tools_setup(), HG_ROOT, commandline_options.get_version())
        dev_env_installation = lib.dev_env.DevEnvInstallation(dev_env)
        do_devbotnet_service(dev_env, dev_env_installation, commandline_options.cmd_devbotnet())
        return 0

    if commandline_options.cmd_unittest() is not None:
        return do_unittest(commandline_options)

    commandline_options.print_help()
    print __doc__
    return 0

if __name__ == '__main__':
    rc = main()
    exit(rc)
