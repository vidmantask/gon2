#!/usr/bin/env python
"""
Build py2exe files, and package in zip.
"""

NAME = 'launchtest'

import py2exe as py2exe_unused # just to make sure it is available for distutils

import sys

if len(sys.argv) == 1:
    sys.argv.extend(["py2exe", "-q"]) # we default to quiet build

import os
target_dir = r'\\bishop\dev\tmp\%s' % NAME

os.system("hg id > hg-id.tmp")
hg_id = file('hg-id.tmp').read().split(' ')[0]
os.remove('hg-id.tmp')

try:
    version_seq = int(file('version.txt').read().strip())
except IOError:
    version_seq = 0

from distutils.core import setup

common = dict(version="0.0.%s" % version_seq,
              company_name="Giritech",
              copyright="Copyright 2009")

options={"py2exe": {
            "dll_excludes": ["w9xpopen.exe"], # we don't need these
            "ascii": 1, # do not automatically include encodings and codecs
            "compressed": 1, # compress the library archive
            "bundle_files": 1, # create singlefile exe
            "dist_dir" : ".", # directory to put final built distributions in
            # Defaults:
            "optimize": 0, # optimization level -O0
            "includes": ['encodings.base64_codec'], # list of modules to include
            "excludes": [], # list of modules to exclude
            "packages": [], # list of packages to include
            },
            }

setup(console=[dict(script="launchtest.py",
                    dest_base="launchtest",
                    #description="launchtest",
                    name="launchtest",
                    **common)],
      #data_files=matplotlib.get_py2exe_datafiles(),
      options=options,
      zipfile=None, # append zip-archive to the executable.
      )

print "Done"
