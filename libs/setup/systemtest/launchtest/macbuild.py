#!/usr/bin/env python
"""
Build py2exe files, and package in zip.
"""

import sys

if len(sys.argv) == 1:
    sys.argv.extend(["py2app"]) # we default to quiet build

import os

try:
    version_seq = int(file('version.txt').read().strip())
except IOError:
    version_seq = 0

from setuptools import setup

common = dict(version="0.0.%s" % version_seq,
              company_name="Giritech",
              copyright="Copyright 2008")

options={"py2app": {
            'argv_emulation': True,
            "dist_dir" : ".", # directory to put final built distributions in
            "optimize": 0, # optimization level -O0
            "includes": [], # list of modules to include
            "excludes": [], # list of modules to exclude
            "packages": [], # list of packages to include
            },
            }

setup(app=[dict(script="launchtest.py",
                    dest_base="launchtest",
                    #description="Gauge client",
                    #name="Gauge client",
                    **common)],
      options=options,
      )

print "Done"
