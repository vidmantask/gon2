
Test of port forward launch features


G/On setup

Run ./generate.sh to generate templates and copy them to the admin server.
Instantiate the templates and give access to them - login is not needed.


Launchtest.py

A little GUI application suitable for testing launches has been created. It
can be controlled with command line options so that it can exercise the
different launch options.

Some kind of description is shown in the title. In the main window is a status
message or response, a text input field, and Ok and Cancel buttoms.

Usually, when Ok is pressed then a TCP connection is made (over the port
forward) and the text is sent to an echo server which sends the same text
back, and the received text is shown.

In another mode it displays the content of a specific file, reloading it every
time Ok is pressed.

In some modes the GUI runs in a subprocess and the main program will terminate
immediately or after some (often 10) seconds.


Server side

The echo server is a standard Unix service - it is often included in xinetd.
In /etc/xinetd.d/echo-stream set "disable = no" and "service xinetd start".
The templates use airbus.dev as server.


Testing

Make sure some kind of launchtest.py is available. The command for starting it
must be set in the environment variable LAUNCHTEST before starting G/On, and
the lock-to-process name in LAUNCHLOCK.

FIXME: LAUNCHLOCK do currently not work because there is no variable expansion
of lock_to_process_name! ltn-ok must thus be edited on the server.

Just run each menu item on each platform, read the description in the window
title, do what it says and press Ok a couple of times.

cp README.txt launchtest.py *.xml  /run/media/$USER/G-ON/
unix2dos /run/media/$USER/G-ON/*.{txt,xml,py}
mkdir /run/media/$USER/G-ON/win
cp launchtest.exe /run/media/$USER/G-ON/win/


Windows

build.py can be used to build launchtest.exe.
(Note: There seems to be some problems when the exe is placed in \ ...)

set LAUNCHTEST=e:\win\launchtest.exe
set LAUNCHLOCK=launchtest.exe
"G-On Windows.exe"
or
set LAUNCHTEST=python g:\launchtest.py
set LAUNCHLOCK=python.exe


Linux

Install wxPython mesa-dri-drivers mesa-libGL wxBase wxGTK wxGTK-gl wxGTK-media wxPython mesa-libGLU
and run launchtest.py directly.

export LAUNCHTEST="python /run/media/$USER/G-ON/launchtest.py"
export LAUNCHLOCK=/usr/bin/python2.7
export LAUNCHTESTRUNNER="python"
/run/media/$USER/G-ON/gon_client/linux/gon_client.com &


Mac

wxPython is already available and can be used directly. (macbuild.py can be
used to build a .app, but that doesn't work properly ...)

10.4 do however have Python 2.3 without subprocess, so fetch the tar ball from
http://www.lysator.liu.se/~astrand/popen5/ and run "python setup.py install".

Python 2.3 also requires that pythonw is used for GUI applications.

10.6 confuses itself with its 64-bit stuff, so it must be disabled before
starting G/On.

export VERSIONER_PYTHON_PREFER_32_BIT=yes
export LAUNCHTEST="pythonw /Volumes/G-ON/launchtest.py"
export LAUNCHLOCK=pythonw
/Volumes/G-ON/G-On\ Mac/Contents/MacOS/gon_client  

