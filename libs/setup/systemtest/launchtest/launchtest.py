#!/usr/bin/env python

import sys, os, socket, wx, optparse, time, signal, subprocess

shell = sys.platform == 'win32'
if os.getenv('LAUNCHTESTRUNNER'):
    launchtestrunner = [os.getenv('LAUNCHTESTRUNNER')] + sys.argv
else:
    launchtestrunner = sys.argv

def main():
    parser = optparse.OptionParser()
    parser.add_option('--sub', action='store_true', help='fork and run in subprocess')
    parser.add_option('--other', action='store_true', help='fork non-sub-process')
    parser.add_option('--sleep', type='int', help='main process sleeps this long and then terminates')
    parser.add_option('--wait', action='store_true', help='wait for subprocess (not other)')
    #parser.add_option('--setsid', action='store_true', help='setsid in child process')
    parser.add_option('--title', type='string', default='Simple TCP client', help='show this title')
    parser.add_option('--watch', type='string', default=None, help='watch out for file')
    parser.add_option('--host', type='string', default='127.0.0.1', help='tcp host')
    parser.add_option('--port', type='int', default=7, help='tcp port')
    parser.add_option('--request', type='string', default='ping', help='send this request')
    parser.add_option('--step2', action='store_true', help='perform step 2')
    parser.add_option('--step3', action='store_true', help='perform step 3')
    options, args = parser.parse_args()

    if options.sub or options.other:
        if not options.step2:
            # parent of step2
            subprocess.Popen(launchtestrunner + ['--step2'], shell=shell)
            if options.sleep or options.wait:
                s = options.sleep or 10
                print 'sleeping', s, 'seconds'
                time.sleep(s)
            print 'orphaning child'
            sys.exit()
        # child
        if options.other:
            # create new child and let intermediate process die to make it orphan
            if not options.step3:
                # parent of step3
                subprocess.Popen(launchtestrunner + ['--step3'], shell=shell)
                print 'orphaning other'
                sys.exit()

    app = wx.PySimpleApp()

    if options.watch:

        while True:
            try:
                txt = file(options.watch).read()
            except Exception, e:
                txt = str(e)
            dlg = wx.TextEntryDialog(None, options.watch + ':\n' + txt, options.title, 'Press enter to reload')
            if dlg.ShowModal() == wx.ID_CANCEL:
                sys.exit()

    else:
        request = options.request

        response = 'No request yet - please enter request:'
        while True:
            dlg = wx.TextEntryDialog(None, response, options.title, request)
            if dlg.ShowModal() == wx.ID_CANCEL:
                sys.exit()
            request = dlg.GetValue()
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            try:
                s.connect((options.host, options.port))
            except Exception, e:
                response = 'Connect error: %r' % e
                continue
            time.sleep(0.1) # immediate send and shutdown seems to cause something to get lost
            try:
                s.sendall(request)
            except socket.error, e:
                response = 'Send error: %r' % e
                continue
            try:
                s.shutdown(socket.SHUT_WR)
            except socket.error, e:
                response = 'Shutdown error: %s' % e
                continue
            try:
                response = s.recv(1<<16)
                response = 'Response: %r' % response
            except socket.error, e:
                response = 'Receive error: %r'
                continue


if __name__ == '__main__':
    main()
