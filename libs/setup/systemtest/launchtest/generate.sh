#!/bin/bash

#airbus.dev:
server=192.168.42.41

i=0
while read name ltpp sp ltpn koc cwp pft pfn pfl cmd; do
	case "$name" in
	\#*)
		;;
	?*)
		i=$((i+1))
		j=$(printf '%02i' $i)
		ltpn=${ltpn/#-/}
		pft=${pft/#-/}
		pfn=${pfn/#-/}
		title=${cmd/#*--title?\"/}
		title=${title/%\"/}
		echo "$j $name	$ltpp	$sp	$ltpn	$koc	$cwp	$pft	$pfn	$pfl	$cmd"
		cat << EOF > launchtest-$name.xml
<?xml version="1.0" encoding="utf-8" ?>
<ConfigurationTemplate name="launchtest-$name" title="Launch test $j $name" xmlns="http://giritech.com/admin_ws">
    <description>$name: $title</description>

    <field field_type="edit">
      <name>label</name>
      <default_value>Launch test $j $name: $title</default_value>
    </field>

    <field field_type="hidden">
      <name>launch_type</name>
      <default_value>0</default_value>
    </field>

    <field field_type="edit">
      <title>server address 0</title>
      <name>portforward.0.server_host</name>
      <default_value>$server</default_value>
    </field>

    <field field_type="edit" advanced="true">
      <title>server port 0</title>
      <name>portforward.0.server_port</name>
      <default_value>777</default_value>
    </field>

    <field field_type="edit">
      <title>server address 1</title>
      <name>portforward.1.server_host</name>
      <default_value>$server</default_value>
    </field>

    <field field_type="edit" advanced="true">
      <title>server port 1</title>
      <name>portforward.1.server_port</name>
      <default_value>7</default_value>
    </field>

    <field field_type="edit" advanced="true">
      <name>portforward.1.client_host</name>
      <default_value>127.0.0.1</default_value>
    </field>

    <field field_type="edit">
      <name>command</name>
      <default_value>$cmd</default_value>
    </field>

    <field field_type="edit">
      <name>portforward.1.lock_to_process_name</name>
      <default_value>$ltpn</default_value>
    </field>

    <field field_type="edit">
      <name>portforward.1.lock_to_process_pid</name>
      <default_value>$ltpp</default_value>
    </field>

    <field field_type="edit">
      <name>portforward.1.sub_processes</name>
      <default_value>$sp</default_value>
    </field>

    <field field_type="edit">
      <name>close_with_process</name>
      <default_value>$cwp</default_value>
    </field>

    <field field_type="edit">
      <name>kill_on_close</name>
      <default_value>$koc</default_value>
    </field>

    <field field_type="edit">
      <name>param_file_template</name>
      <default_value><![CDATA[$pft]]></default_value>
    </field>

    <field field_type="edit">
      <name>param_file_name</name>
      <default_value>$pfn</default_value>
    </field>

    <field field_type="edit">
      <name>param_file_lifetime</name>
      <default_value>$pfl</default_value>
    </field>

    <field field_type="edit" advanced="true">
      <name>dialog_tags</name>
      <default_value>CLIENTOK,SERVEROK</default_value>
    </field>

    <field field_type="edit" advanced="true">
      <name>dialog_tag_generators</name>
      <default_value><![CDATA[]]></default_value>
    </field>

</ConfigurationTemplate>
EOF
		;;
	esac
done < $(dirname $0)/table.txt

