<?xml version="1.0" encoding="utf-8" ?>
<ConfigurationTemplate name="DME_demo_app" title="DME AppBox Demo App" xmlns="http://giritech.com/admin_ws">
	<description>Built-in demo web app for the DME secure browser, through a G/On http proxy connection</description>

	<field field_type="hidden">
      <name>launch_type</name>
      <default_value>11</default_value>
    </field>

    <field field_type="edit">
      <name>label</name>
      <tooltip>Internal name, only visible in the Management Client</tooltip>
      <default_value>(DME) Demo App Vacation Registration</default_value>
    </field>

    <field field_type="edit">
      <name>menu_title</name>
      <tooltip>Name presented to the user</tooltip>
      <default_value>Vacation</default_value>
    </field>

    <field field_type="edit">
      <name>image_id</name>
      <default_value>vacation</default_value>
      <tooltip>Icon presented to the user, taken from the folder: config/images, on the server</tooltip>
    </field>

    <field field_type="custom_template" advanced="true">
      <title>Web Server Address</title>
	  <name>web_server_address</name>
      <tooltip>IP or DNS name - default is the Gateway server itself</tooltip>
      <default_value>127.0.0.1</default_value>
    </field>

     <field field_type="custom_template" advanced="true">
      <title>Web Server Port</title>
      <name>web_server_port</name>
      <tooltip>OBS: Currently no support for https (port 443)</tooltip>
      <default_value>8075</default_value>
    </field>

    <field field_type="custom_template" advanced="true">
	  <name>folder</name>
      <title>Web Server Start Folder or File</title>
      <default_value>web_app_demo/vacation_app/build/production/index.html</default_value>
    </field>

    <field field_type="custom_template">
      <name>applicable_os</name>
      <title>Applicable OS</title>
      <default_value>DME-mobile</default_value>
      <selection empty_allowed="0">
        <selection_choice title="All (iOS and Android)" value="DME-mobile" />
        <selection_choice title="iOS, only" value="DME-iOS" />
        <selection_choice title="Android, only" value="DME-android" />
      </selection>
    </field>
	
    <field field_type="custom_template" advanced="true">
      <name>applicable_screen_size</name>
      <title>Applicable Screen Size</title>
      <default_value></default_value>
      <selection empty_allowed="0">
        <selection_choice title="All Sizes" value="" />
        <selection_choice title="Small (2-3 inch)" value="-small" />
        <selection_choice title="SmartPhone Size (around 4 inch, iPhone size)" value="-normal" />
        <selection_choice title="Small Tablet Size (5-7 inch)" value="-large" />
        <selection_choice title="Large Tablet Size (7-10 inch, iPad size)" value="-xlarge" />
      </selection>
    </field>
	
    <field field_type="custom_template" advanced="true">
      <name>browser_overlay_back</name>
      <title>Overlay Launch Pad Button</title>
      <tooltip>Usefull for apps without a built-in button to go to the AppBox launch pad</tooltip>
      <default_value>off</default_value>
      <selection empty_allowed="0">
        <selection_choice title="Disabled" value="off" />
        <selection_choice title="Enabled" value="on" />
      </selection>
    </field>
	
    <field field_type="custom_template" advanced="true">
      <name>progress_bar</name>
      <title>Progress Bar</title>
      <tooltip>Show progress bar during loading of Javascript and HTML from the web server</tooltip>
      <default_value>on</default_value>
      <selection empty_allowed="0">
        <selection_choice title="Disabled" value="off" />
        <selection_choice title="Enabled" value="on" />
      </selection>
    </field>

    <field field_type="custom_template" advanced="true">
      <name>orientation_lock_small</name>
      <title>Orientation on Small Screens</title>
      <tooltip>Orientation on small screens (around 2-3 inch)</tooltip>
      <default_value></default_value>
      <selection empty_allowed="0">
        <selection_choice title="Free" value="" />
        <selection_choice title="Locked in landscape" value="small_landscape" />
        <selection_choice title="Locked in portrait" value="small_portrait" />
        <selection_choice title="Locked in current orientation" value="small_landscape small_portrait" />
      </selection>
    </field>
	
    <field field_type="custom_template" advanced="true">
      <name>orientation_lock_normal</name>
      <title>-- on Smart Phone Sized Screens</title>
      <tooltip>Orientation on smart phone sized screens (around 4 inch, iPhone size)</tooltip>

      <default_value></default_value>
      <selection empty_allowed="0">
        <selection_choice title="Free" value="" />
        <selection_choice title="Locked in landscape" value="normal_landscape" />
        <selection_choice title="Locked in portrait" value="normal_portrait" />
        <selection_choice title="Locked in current orientation" value="normal_landscape normal_portrait" />
      </selection>
    </field>
	
    <field field_type="custom_template" advanced="true">
      <name>orientation_lock_large</name>
      <title>-- on Small Tablet Sized Screens</title>
      <tooltip>Orientation on small tablet sized screens (around 5-7 inch)</tooltip>

      <default_value></default_value>
      <selection empty_allowed="0">
        <selection_choice title="Free" value="" />
        <selection_choice title="Locked in landscape" value="large_landscape" />
        <selection_choice title="Locked in portrait" value="large_portrait" />
        <selection_choice title="Locked in current orientation" value="large_landscape large_portrait" />
      </selection>
    </field>
	
    <field field_type="custom_template" advanced="true">
      <name>orientation_lock_xlarge</name>
      <title>-- on Large Tablet Sized Screens</title>
      <tooltip>Orientation on large tablet sized screens (around 7-10 inch, iPad size)</tooltip>

      <default_value></default_value>
      <selection empty_allowed="0">
        <selection_choice title="Free" value="" />
        <selection_choice title="Locked in landscape" value="xlarge_landscape" />
        <selection_choice title="Locked in portrait" value="xlarge_portrait" />
        <selection_choice title="Locked in current orientation" value="xlarge_landscape xlarge_portrait" />
      </selection>
    </field>
	
    <field field_type="custom_template" advanced="true">
      <name>browser_cache</name>
      <title>Browser Cache</title>
      <default_value>on</default_value>
      <selection empty_allowed="0">
        <selection_choice title="Disabled" value="off" />
        <selection_choice title="Enabled" value="on" />
        <selection_choice title="Enabled and cleared at each launch" value="clear_on_launch" />
      </selection>
    </field>
	
    <field field_type="edit" advanced="true">
      <name>log_tcp_connections</name>
      <title>TCP Connection Logging</title>
      <tooltip>Server side logging - see results in the Management Client, Reporting perspective</tooltip>
    </field>

    <field field_type="edit" advanced="true">
      <name>log_http_connections</name>
      <title>HTTP Proxy Logging</title>
      <tooltip>Server side logging - see the result in the Management Client, Reporting perspective</tooltip>
    </field>

    <field field_type="custom_template" advanced="true">
      <name>client_side_browser_logging</name>
      <title>Browser Logging</title>
      <tooltip>Client side logging, automatically transfered to the server folder: gon_server_gateway_service/win/client_logs</tooltip>
      <default_value></default_value>
      <selection empty_allowed="0">
        <selection_choice title="Disabled" value="" />
        <selection_choice title="Enabled" value="LOG_TO_SERVER_ENABLED" />
      </selection>
    </field>

	<!-- The field called 'citrix_command' is for this launch type used to control client side browser logging -->
    <field field_type="hidden">
	  <name>citrix_command</name>
      <default_value>%(custom_template.client_side_browser_logging)</default_value>
    </field>
	
    <field field_type="custom_template" advanced="true">
      <name>https_handling</name>
      <title>HTTPS Interception</title>
      <tooltip>Client side interception of HTTPS traffic originating from the DME client</tooltip>
      <default_value>do_not_handle</default_value>
      <selection empty_allowed="0">
        <selection_choice title="No interception" value="do_not_handle" />
        <selection_choice title="Intercept, block and log (for debugging)" value="block" />
      </selection>
    </field>
	
    <field field_type="edit" advanced="true">
      <name>command</name>
      <default_value>secure_browser --url=http://%(custom_template.web_server_address):%(custom_template.web_server_port)/%(custom_template.folder) --skin=app_fullscreen --skin_back_button=%(custom_template.browser_overlay_back) --cache=%(custom_template.browser_cache) --handle_scheme_https=%(custom_template.https_handling) --skin_progress_bar=%(custom_template.progress_bar) --orientation_lock %(custom_template.orientation_lock_small),%(custom_template.orientation_lock_normal),%(custom_template.orientation_lock_large),%(custom_template.orientation_lock_xlarge)</default_value>
    </field>

    <field field_type="edit" advanced="true">
      <name>dialog_tags</name>
      <default_value>DME,WEB_APP,SERVEROK</default_value>
    </field>

    <field field_type="edit" advanced="true">
      <name>dialog_tag_generators</name>
      <default_value><![CDATA[client_ok::IfPlatformIs("%(custom_template.applicable_os)%(custom_template.applicable_screen_size)")
]]></default_value>
    </field>

    <field field_type="edit" advanced="true">
      <name>sso_login</name>
      <tooltip>Note: this is only used, if an #auth directive is added to permitted server address(es)</tooltip>
	  <default_value>%(user.login)@%(user.domain)</default_value>
    </field>

    <field field_type="edit" advanced="true">
      <name>sso_password</name>
      <tooltip>Note: this is only used, if an #auth directive is added to permitted server address(es)</tooltip>
	  <default_value>%(user.password)</default_value>
    </field>

    <field field_type="hidden">
      <name>portforward.0.client_host</name>
	  <!-- Must be defined, even though it is not used in this launch type -->
      <default_value>127.0.0.1</default_value>
    </field>

   <field field_type="edit" advanced="true">
      <title>1. Permitted Server Address</title>
      <name>portforward.1.server_host</name>
      <tooltip>IP or subnet or DNS name - add #auth or #auth=basic or #auth=ntlm to enable SSO</tooltip>
      <default_value>%(custom_template.web_server_address) #auth=basic</default_value>
    </field>

    <field field_type="edit" advanced="true">
      <title>-------------------- Port</title>
      <name>portforward.1.server_port</name>
      <default_value>%(custom_template.web_server_port)</default_value>
    </field>

    <field field_type="edit" advanced="true">
      <title>2. Permitted Server Address</title>
      <name>portforward.2.server_host</name>
      <tooltip>IP or subnet or DNS name - add #auth or #auth=basic or #auth=ntlm to enable SSO</tooltip>
      <default_value>yui.yahooapis.com</default_value>
    </field>

    <field field_type="edit" advanced="true">
      <title>-------------------- Port</title>
      <name>portforward.2.server_port</name>
      <default_value>80</default_value>
    </field>

    <field field_type="edit" advanced="true">
      <title>3. Permitted Server Address</title>
      <name>portforward.3.server_host</name>
      <tooltip>IP or subnet or DNS name - add #auth or #auth=basic or #auth=ntlm to enable SSO</tooltip>
      <default_value></default_value>
    </field>

    <field field_type="edit" advanced="true">
      <title>-------------------- Port</title>
      <name>portforward.3.server_port</name>
      <default_value></default_value>
    </field>

    <field field_type="edit" advanced="true">
      <title>4. Permitted Server Address</title>
      <name>portforward.4.server_host</name>
      <tooltip>IP or subnet or DNS name - add #auth or #auth=basic or #auth=ntlm to enable SSO</tooltip>
      <default_value></default_value>
    </field>

    <field field_type="edit" advanced="true">
      <title>-------------------- Port</title>
      <name>portforward.4.server_port</name>
      <default_value></default_value>
    </field>

    <field field_type="edit" advanced="true">
      <title>5. Permitted Server Address</title>
      <name>portforward.5.server_host</name>
      <tooltip>IP or subnet or DNS name - add #auth or #auth=basic or #auth=ntlm to enable SSO</tooltip>
      <default_value></default_value>
    </field>

    <field field_type="edit" advanced="true">
      <title>-------------------- Port</title>
      <name>portforward.5.server_port</name>
      <default_value></default_value>
    </field>

    <field field_type="edit" advanced="true">
      <title>6. Permitted Server Address</title>
      <name>portforward.6.server_host</name>
      <tooltip>IP or subnet or DNS name - add #auth or #auth=basic or #auth=ntlm to enable SSO</tooltip>
      <default_value></default_value>
    </field>

    <field field_type="edit" advanced="true">
      <title>-------------------- Port</title>
      <name>portforward.6.server_port</name>
      <default_value></default_value>
    </field>

    <field field_type="edit" advanced="true">
      <title>7. Permitted Server Address</title>
      <name>portforward.7.server_host</name>
      <tooltip>IP or subnet or DNS name - add #auth or #auth=basic or #auth=ntlm to enable SSO</tooltip>
      <default_value></default_value>
    </field>

    <field field_type="edit" advanced="true">
      <title>-------------------- Port</title>
      <name>portforward.7.server_port</name>
      <default_value></default_value>
    </field>

    <field field_type="edit" advanced="true">
      <title>8. Permitted Server Address</title>
      <name>portforward.8.server_host</name>
      <tooltip>IP or subnet or DNS name - add #auth or #auth=basic or #auth=ntlm to enable SSO</tooltip>
      <default_value></default_value>
    </field>

    <field field_type="edit" advanced="true">
      <title>-------------------- Port</title>
      <name>portforward.8.server_port</name>
      <default_value></default_value>
    </field>

    <field field_type="edit" advanced="true">
      <title>9. Permitted Server Address</title>
      <name>portforward.9.server_host</name>
      <tooltip>IP or subnet or DNS name - add #auth or #auth=basic or #auth=ntlm to enable SSO</tooltip>
      <default_value></default_value>
    </field>

    <field field_type="edit" advanced="true">
      <title>-------------------- Port</title>
      <name>portforward.9.server_port</name>
      <default_value></default_value>
    </field>

    <field field_type="edit" advanced="true">
      <title>10. Permitted Server Address</title>
      <name>portforward.10.server_host</name>
      <tooltip>IP or subnet or DNS name - add #auth or #auth=basic or #auth=ntlm to enable SSO</tooltip>
      <default_value></default_value>
    </field>

    <field field_type="edit" advanced="true">
      <title>-------------------- Port</title>
      <name>portforward.10.server_port</name>
      <default_value></default_value>
    </field>

</ConfigurationTemplate>
