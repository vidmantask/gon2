!include "FileFunc.nsh"

!define GON_INSTALLER_DESTINATION_ROOT ".."
!define GON_INSTALLATION_ROOT "..\..\..\.."
!define GON_GPMS_ROOT "gpms"

!define GON_ICON_FILENAME   "gon_installer.ico"

;
; NSIS Configuration
;
Name "G/On Client Stealth Runnner"
Icon "${GON_ICON_FILENAME}"
OutFile "${GON_INSTALLER_DESTINATION_ROOT}\G-On Client Stealth.exe"
InstallDir "$TEMP\gon_client_stealth_runner"
RequestExecutionLevel user
SilentInstall silent
BrandingText " "


;
; Sections
;
Section
  SetOutPath "$INSTDIR"
  File /r "${GON_INSTALLATION_ROOT}\gon_client_stealth\win\*"

  SetOutPath "$INSTDIR\gpms"
  File "${GON_GPMS_ROOT}\*"

  ${GetParent} "$EXEPATH" $R0
  ExecWait '"$INSTDIR\gon_client_stealth.exe"' $0
  SetErrorLevel $0
  RMDir /r "$INSTDIR"
SectionEnd
