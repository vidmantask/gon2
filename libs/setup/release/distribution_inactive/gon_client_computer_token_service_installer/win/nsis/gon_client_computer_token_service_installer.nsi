!include "FileFunc.nsh"

!define GON_INSTALLER_DESTINATION_ROOT ".."
!define GON_INSTALLATION_ROOT "..\..\..\.."
!define GON_ICON_FILENAME   "gon_installer.ico"
!include "gon_5_include.nsh"

;
; NSIS Configuration
;
Name "G/On Client Computer Token Service Installer"
Icon "${GON_ICON_FILENAME}"
OutFile "${GON_INSTALLER_DESTINATION_ROOT}\G-On Client Computer Token Service Installer.exe"

InstallDir $PROGRAMFILES\Giritech\gon_client_computer_token_service
RequestExecutionLevel admin
SilentInstall silent
BrandingText " "
LogSet on


;
; Sections
;
Section "Install Section" InstallSection
  SetOutPath "$INSTDIR"
  File /r "${GON_INSTALLATION_ROOT}\gon_client_computer_token_service\win\*"

  ; Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Client Computer Token Service ${GIRI_VERSION}" "DisplayName" "G-On Client Computer Token Service ${GIRI_VERSION}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Client Computer Token Service ${GIRI_VERSION}" "UninstallString" "$\"$INSTDIR\uninstall.exe$\""
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Client Computer Token Service ${GIRI_VERSION}" "Publisher" "Giritech"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Client Computer Token Service ${GIRI_VERSION}" "DisplayVersion" "${GIRI_VERSION}"
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Client Computer Token Service ${GIRI_VERSION}" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Client Computer Token Service ${GIRI_VERSION}" "NoRepair" 1

  nsExec::ExecToStack '"$INSTDIR\gon_client_computer_token_service.exe" --startup auto install'
  Sleep 1000
  nsExec::ExecToStack '"$INSTDIR\gon_client_computer_token_service.exe" start'
  Sleep 1000
SectionEnd

Section "Uninstall"
  nsExec::ExecToStack '"$INSTDIR\gon_client_computer_token_service.exe" stop'
  Sleep 1000
  nsExec::ExecToStack '"$INSTDIR\gon_client_computer_token_service.exe" remove'
  Sleep 1000

  DetailPrint "Deleting Files"
  Delete "$INSTDIR\Uninstall.exe"
  RMDir /r "$INSTDIR"

  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Client Computer Token Service ${GIRI_VERSION}"
SectionEnd
