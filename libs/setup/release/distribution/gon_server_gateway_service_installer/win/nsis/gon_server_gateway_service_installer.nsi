!include "MUI2.nsh"
;
; Include generated definitions
;
!define GON_INSTALLER_DESTINATION_ROOT ".."
!define GON_INSTALLATION_ROOT "..\..\..\.."
;!define GIRI_VERSION '5.0.0-1'
!include "gon_5_include.nsh"

;
; Giritech variabel definitions
;
!define GIRI_TITLE 'G/On Gateway Server'
!define GIRI_IMAGE_INSTALLER_SPLASH gon_5_install_splash.bmp
!define GIRI_IMAGE_INSTALLER_V gon_5_v.bmp
!define GIRI_IMAGE_INSTALLER_H gon_5_installer_h.bmp
!define GIRI_IMAGE_UNINSTALLER_SPLASH gon_5_install_splash.bmp
!define GIRI_IMAGE_UNINSTALLER_V gon_5_uninstaller_v.bmp
!define GIRI_IMAGE_UNINSTALLER_H gon_5_uninstaller_h.bmp


;
; Misc variabel definitions
;
!define MUI_WELCOMEPAGE_TITLE "Welcome to the G/On Gateway Server Installation"
!define MUI_WELCOMEPAGE_TEXT "You are about to install ${GIRI_TITLE} ${GIRI_VERSION}$\r$\n$\r$\nClick Next to install the G/On Gateway Server or Cancel to exit."

!define MUI_WELCOMEFINISHPAGE_BITMAP_NOSTRETCH
!define MUI_WELCOMEFINISHPAGE_BITMAP "${GIRI_IMAGE_INSTALLER_H}"

!define MUI_HEADERIMAGE
!define MUI_HEADERIMAGE_BITMAP "${GIRI_IMAGE_INSTALLER_V}"

!define MUI_COMPONENTSPAGE_SMALLDESC

!define MUI_FINISHPAGE_TITLE "G/On 5 Gateway Server Successfully Installed"
!define MUI_FINISHPAGE_TEXT "You are now ready to start the G/On Gateway Server Service.$\r$\n$\r$\nClick Finish to exit."

!define MUI_FINISHPAGE_LINK "Visit www.solitonsystems.com for the latest news."
!define MUI_FINISHPAGE_LINK_LOCATION "https://solitonsystems.com/it-security/products/g-on"

!define MUI_UNTEXT_WELCOME_INFO_TITLE "Welcome to the G/On Gateway Server Uninstaller"
!define MUI_UNTEXT_WELCOME_INFO_TEXT "WARNING: This will uninstall G/On Gateway Server. If you are completely sure you want to continue, click Next.$\r$\n$\r$\nClick Cancel to cancel this uninstallation."

!define MUI_UNWELCOMEFINISHPAGE_BITMAP_NOSTRETCH
!define MUI_UNWELCOMEFINISHPAGE_BITMAP "${GIRI_IMAGE_UNINSTALLER_H}"


BrandingText " "


;
; Function definitions
;
Function .onInit
        InitPluginsDir
        File /oname=$PLUGINSDIR\splash.bmp "${GIRI_IMAGE_INSTALLER_SPLASH}"
        advsplash::show 1500 600 400 -1 $PLUGINSDIR\splash
        Delete $PLUGINSDIR\splash.bmp
FunctionEnd


;
;
;
!include LogicLib.nsh
Var ReturnCode



;
; NSIS Configuration
;
OutFile ${GON_INSTALLER_DESTINATION_ROOT}\gon_server_gateway_service_installer.exe

InstallDir $PROGRAMFILES\Giritech\gon_gateway_${GIRI_VERSION}
RequestExecutionLevel admin

Name "${GIRI_TITLE}"
Caption "${GIRI_TITLE} Installer"

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "${GON_INSTALLATION_ROOT}\LICENSE_EULA.rtf"
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_WELCOME
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH

!insertmacro MUI_LANGUAGE "English"

Icon "gon_installer.ico"
UninstallIcon "gon_uninstaller.ico"



Section "Install Section" InstallSection
  SetOutPath "$INSTDIR\gon_server_gateway_service"
  File /r /x *.log "${GON_INSTALLATION_ROOT}\gon_server_gateway_service\*"

  SetOutPath "$INSTDIR\gon_server_gateway_service\win"
  File /oname=gon_server_gateway_local.ini "gon_server_gateway_local.ini"

  SetOutPath "$INSTDIR\gon_config_service\win"
  File /r /x *.log /x events /x backup "${GON_INSTALLATION_ROOT}\gon_config_service\win\*"

  SetOutPath "$INSTDIR\gon_config_service\win"
  File /oname=gon_config_service_local.ini "gon_config_service_local.ini"

  SetOutPath "$INSTDIR"
  File /oname=LICENSE.txt "${GON_INSTALLATION_ROOT}\LICENSE.txt"
  File /oname=LICENSE_EULA.rtf "${GON_INSTALLATION_ROOT}\LICENSE_EULA.rtf"

  SetOutPath "$INSTDIR\config\deployed"
  File /oname=gon_service_management_client.ks "${GON_INSTALLATION_ROOT}\config\deployed\gon_service_management_client.ks"
  File /oname=default.lic "${GON_INSTALLATION_ROOT}\config\deployed\default.lic"
  File /oname=gon_client.ks "${GON_INSTALLATION_ROOT}\config\deployed\gon_client.ks"
  File /oname=gon_server.ks "${GON_INSTALLATION_ROOT}\config\deployed\gon_server.ks"
  File /oname=gon_client.servers "${GON_INSTALLATION_ROOT}\config\deployed\gon_client.servers"
  File /oname=gon_scramble_salt "${GON_INSTALLATION_ROOT}\config\deployed\gon_scramble_salt"

  DetailPrint "Installing G/On Gateway Server Service"
  nsExec::ExecToStack '"$INSTDIR\gon_server_gateway_service\win\gon_server_gateway_service.exe" install'
  Pop $ReturnCode
  ${If} $ReturnCode != 0
  	MessageBox MB_OK "Unable to install G/On Gateway Server Service"
  ${EndIf}

  ; Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Gateway Server ${GIRI_VERSION}" "DisplayName" "G-On Gateway Server ${GIRI_VERSION}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Gateway Server ${GIRI_VERSION}" "UninstallString" "$\"$INSTDIR\uninstall.exe$\""
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Gateway Server ${GIRI_VERSION}" "Publisher" "Soliton"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Gateway Server ${GIRI_VERSION}" "DisplayVersion" "${GIRI_VERSION}"
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Gateway Server ${GIRI_VERSION}" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Gateway Server ${GIRI_VERSION}" "NoRepair" 1
SectionEnd


Section "Uninstall"
  DetailPrint "Removing G/On Gateway Server Service"
  nsExec::ExecToStack '"$INSTDIR\gon_server_gateway_service\win\gon_server_gateway_service.exe" remove'
  Pop $ReturnCode
  ${If} $ReturnCode != 0
  	MessageBox MB_OK "Unable to remove G/On Gateway Server Service"
  ${EndIf}

  DetailPrint "Deleting Files..."
  Delete "$INSTDIR\Uninstall.exe"
  RMDir /r "$INSTDIR"

  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Gateway Server ${GIRI_VERSION}"
SectionEnd
