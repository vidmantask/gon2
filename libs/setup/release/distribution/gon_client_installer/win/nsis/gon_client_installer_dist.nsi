!include "FileFunc.nsh"

!define GON_INSTALLER_DESTINATION_ROOT ".."
!define GON_INSTALLATION_ROOT "..\..\..\.."
!define GON_GPMS_ROOT "gpms"

!define GON_ICON_FILENAME   "gon_installer.ico"
!define GON_SPLASH_FILENAME "gon_client_installer_splash.bmp"

;
; NSIS Configuration
;
Name "G/On Client Installer"
Icon "${GON_ICON_FILENAME}"
OutFile "${GON_INSTALLER_DESTINATION_ROOT}\G-On Client Installer.exe"
InstallDir "$TEMP\gon_client_installer_dist"
RequestExecutionLevel user
SilentInstall silent
BrandingText " "


;
; Function definitions
;
Function .onInit
  InitPluginsDir
  File /oname=$PLUGINSDIR\splash.bmp "${GON_SPLASH_FILENAME}"
  advsplash::show 1500 600 400 -1 $PLUGINSDIR\splash
  Delete $PLUGINSDIR\splash.bmp
FunctionEnd

;
; Sections
;
Section
  SetOutPath "$INSTDIR"
  File /r "${GON_INSTALLATION_ROOT}\gon_client_installer\win\*"

  SetOutPath "$INSTDIR\gpms"
  File "${GON_GPMS_ROOT}\*"

  ${GetParent} "$EXEPATH" $R0
  ExecWait '"$INSTDIR\gon_client_installer.exe"'
  RMDir /r "$INSTDIR"
SectionEnd
