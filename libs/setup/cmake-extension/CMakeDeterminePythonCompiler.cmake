
# determine the compiler to use for Python programs
# NOTE, a generator may set CMAKE_Python_COMPILER before
# loading this file to force a compiler.

IF(NOT CMAKE_Python_COMPILER)
  FIND_PROGRAM(CMAKE_Python_COMPILER
    NAMES python2.5 python2.4 python2.3 python2.2 python2.1 python2.0 python1.6 python1.5 python
    PATHS
    [HKEY_LOCAL_MACHINE\\SOFTWARE\\Python\\PythonCore\\2.5\\InstallPath]
    [HKEY_LOCAL_MACHINE\\SOFTWARE\\Python\\PythonCore\\2.4\\InstallPath]
    [HKEY_LOCAL_MACHINE\\SOFTWARE\\Python\\PythonCore\\2.3\\InstallPath]
    [HKEY_LOCAL_MACHINE\\SOFTWARE\\Python\\PythonCore\\2.2\\InstallPath]
    [HKEY_LOCAL_MACHINE\\SOFTWARE\\Python\\PythonCore\\2.1\\InstallPath]
    [HKEY_LOCAL_MACHINE\\SOFTWARE\\Python\\PythonCore\\2.0\\InstallPath]
    [HKEY_LOCAL_MACHINE\\SOFTWARE\\Python\\PythonCore\\1.6\\InstallPath]
    [HKEY_LOCAL_MACHINE\\SOFTWARE\\Python\\PythonCore\\1.5\\InstallPath]
  )
ENDIF(NOT CMAKE_Python_COMPILER)

# configure variables set in this file for fast reload later on
CONFIGURE_FILE(${CMAKE_ROOT}/Modules/CMakePythonCompiler.cmake.in 
  ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/CMakePythonCompiler.cmake IMMEDIATE @ONLY)
SET(CMAKE_Python_COMPILER_ENV_VAR "PYTHON_COMPILER")
SET(CMAKE_Python_COMPILER "PYTHON_COMPILER")
