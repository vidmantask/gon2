[db]
#encoding = utf8
#type = sqlite
#host = 
#port = 
#database = ../../config/gon_server_db.sqlite
#username = 
#<-- Potential sensitive information deleted -->
#query = 
#log_enabled = False
#<-- Potential sensitive information deleted -->

[plugin_modules]
#path = .

[deployment]
#path = ../../config/deployed

[cpm]
#gpms_path = ../../config/gpm/gpms
#concurent_downloads = 2

[license]
#filename = ../../config/deployed/gon_license.lic

[dictionary]
#dictionary_path = ../../config/dictionary

[images]
#path = ../../config/images

[security]
#dos_attack_keyexchange_phase_one_high = 20
#dos_attack_keyexchange_phase_one_low = 5
#dos_attack_security_closed_high = 4
#dos_attack_security_closed_low = 2
#dos_attack_ban_attacker_ips = True

[login]
#security_package = NTLM
#<-- Potential sensitive information deleted -->
#target_spn = 
#enable_sso = False
#local_gateway_sso = False
#show_last_user_directory = False
#show_last_login = False

[web_server]
#enabled = True
#host = 127.0.0.1
#port = 8075
#www_root = ../../config/www_root

[session]
#offline_credential_timeout_min = 60
#close_when_entering_background = False
#timeout_min = 120
#keep_alive_ping_interval_sec = 29
#keyexchange_timeout_sec = 60

[log]
#in_folder_path = cp_logs
#rotate = True
#verbose = 1
#session_enabled = False
#client_path = ./client_logs
#enabled = True
#enabled_traffic_logs = 
#in_folder_enabled = False
#file = gon_server_gateway.log
#session_path = ./session_logs
#type = text
#session_enabled_by_remote = False

[service]
#ip = 0.0.0.0
#client_connect_ports = 443
#num_of_threads = 5
#port = 443
#client_connect_addresses = 127.0.0.1
client_connect_ports = 3945, 443
port = 13945
client_connect_addresses = gon04.demo.giritech.com,80.160.92.43

[authorization]
#timeout_sec = 150
#always_allow_access = False
#require_full_login = False
#use_tri_state_logic = True
#access_rule = 
timeout_sec = 60

[service_http]
#log_2_enabled = False
#ip = 0.0.0.0
#log_3_enabled = False
#enabled = False
#client_connect_ports = 80
#port = 80
enabled = True
port = 10080

[service_management]
#connect_port = 8074
#filedist_enabled = False
#connect_ip = 127.0.0.1
#filedist_path = ../../config
#message_queue_offline_filename = ./message_queue_offline.dat
connect_port = 8174

[dialog]
#welcome_message_filename = ../../config/gon_welcome_message.txt
#create_citrix_menu = True
#welcome_message_close_on_cancel = True
#welcome_message_enabled = False
welcome_message_filename = ./gon_welcome_message.txt

