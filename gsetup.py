"""
"""
import sys
import os
import os.path
import ConfigParser
import optparse
import subprocess
import shutil
import tempfile
import tarfile
import threading

ROOT = os.path.abspath(os.path.dirname(__file__))

TARGET_PLATFORM_MAC    = "mac_64"
TARGET_PLATFORM_LINUX  = "linux_64"
TARGET_PLATFORM_WIN    = "win_32"
def detect_platform():
    if sys.platform == "win32":
        return TARGET_PLATFORM_WIN
    elif sys.platform in [ "linux2", "linux" ]:
        return TARGET_PLATFORM_LINUX
    elif sys.platform == "darwin":
        return TARGET_PLATFORM_MAC
    return "unknown"

pips_common = [
    'altgraph==0.16.1',
    'asn1crypto==0.24.0',
    'bcrypt==3.1.4',
    'cffi==1.11.5',
    'comtypes==1.1.7',
    'cryptography==2.3.1',
    'dis3==0.1.2',
    'enum34==1.1.6',
    'future==0.16.0',
    'idna==2.7',
    'ipaddress==1.0.22',
    'IPy==0.83',
    'macholib==1.11',
    'paramiko==2.4.1',
    'pefile==2018.8.8',
    'pyasn1==0.4.4',
    'pycparser==2.18',
    'pycrypto==2.6.1',
    'pydot==1.2.4',
    'PyInstaller==3.3.1',
    'PyNaCl==1.2.1',
    # 'pyodbc==4.0.24',
    'pyparsing==2.2.0',
    'python-graph-core==1.8.2',
    'python-graph-dot==1.8.2',
    'setuptools==40.2.0',
    'six==1.11.0',
    'SQLAlchemy==1.2.11',
    'wheel==0.31.1',
    'ZSI==2.0rc3',
    'coverage==4.5.3',
]

pips_win = [
    'pywin32==223',
    'pypiwin32==223',
]

pips_mac = [
    'pyobjc-framework-Cocoa==5.2',
    'pyobjc-framework-WebKit==5.2',
    'pyobjc-framework-UserNotifications==5.2',
    'py2app==0.19'
]

pips_linux = [
]


class CommandlineOptions(object):
    def __init__(self):
        self._parser = optparse.OptionParser()
        self._parser.add_option('--cmd_install_py_modules',  action='store_true', help='Install all python modules for current platform')
        self._parser.add_option('--cmd_build_cpp',  action='store_true', help='Build C++ py-extenseions')
        self._parser.add_option('--cmd_build',      action='store_true', help='Build')
        self._parser.add_option('--cmd_build_release',  action='store_true', help='Build Release')
        self._parser.add_option('--cmd_build_prebuild_client_file_packages',  action='store_true', help='Build prebuild client file packages from extern repo')

        self._parser.add_option('--build_store', type='string', default=None, help='')
        self._parser.add_option('--release_store', type='string', default=None, help='')
        self._parser.add_option('--build_id',  type='string', default=None, help='')
        self._parser.add_option('--skip_clean',  action='store_true', help='Skip remove of gon_build folder')
        self._parser.add_option('--skip_sign',  action='store_true', default=False, help='Skip signing')
        self._parser.add_option('--create_git_tag',  action='store_true', help='Create git-tag if cmd_release command is ok')

        self._parser.add_option('--cmd_devbotnet',  type='string', default=None, help='Accepted arbuments are [slave, master]')
        self._parser.add_option('--devbotnet_slave_mac',    type='string', default=None, help='host:port of devbotnet slave for building mac')
        self._parser.add_option('--devbotnet_slave_linux',  type='string', default=None, help='host:port of devbotnet slave for building linux')

        self._parser.add_option('--cmd_unittest',  type='string', default=None, help='Accepted arguments [py, cpp]')
        self._parser.add_option('--unittest_efilter',  type='string', default=None, help='Unittest include filter, default none')
        self._parser.add_option('--unittest_ifilter',  type='string', default=None, help='Unittest include filter, default all')

        (self._options, self._args) = self._parser.parse_args()

    def cmd_install_py_modules(self):
        return self._options.cmd_install_py_modules

    def cmd_build_cpp(self):
        return self._options.cmd_build_cpp

    def cmd_build(self):
        return self._options.cmd_build

    def cmd_build_release(self):
        return self._options.cmd_build_release

    def cmd_build_prebuild_client_file_packages(self):
        return self._options.cmd_build_prebuild_client_file_packages

    def build_store(self):
        if self._options.build_store is not None:
            return self._options.build_store
        return os.path.join(ROOT, 'gon_build', 'build_store')

    def release_store(self):
        if self._options.release_store is not None:
            return self._options.release_store
        return os.path.join(ROOT, 'gon_build', 'release_store')

    def build_id(self):
        if self._options.build_id is not None:
            return self._options.build_id

        config_version_filename = os.path.join(ROOT, 'config_version.ini')
        config_version = ConfigParser.ConfigParser()
        config_version.read(config_version_filename)
        build_id = config_version.getint('version', 'build_id')
        return "%d" % build_id

    def skip_clean(self):
        return self._options.skip_clean

    def skip_sign(self):
        return self._options.skip_sign

    def create_git_tag(self):
        return self._options.create_git_tag

    def cmd_devbotnet(self):
        return self._options.cmd_devbotnet

    def devbotnet_slave_mac(self):
        return self._options.devbotnet_slave_mac

    def devbotnet_slave_linux(self):
        return self._options.devbotnet_slave_linux

    def cmd_unittest(self):
        return self._options.cmd_unittest

    def unittest_ifilter(self):
        return self._options.unittest_ifilter

    def unittest_efilter(self):
        return self._options.unittest_efilter


    def get_version(self):
        config_version_filename = os.path.join(ROOT, 'config_version.ini')
        config_version = ConfigParser.ConfigParser()
        config_version.read(config_version_filename)

        major = config_version.getint('version', 'major')
        minor = config_version.getint('version', 'minor')
        bugfix = config_version.getint('version', 'bugfix')
        if self._options.build_id is not None:
            build_id = int(self._options.build_id)
        else:
            build_id = config_version.getint('version', 'build_id')
        return "%d.%d.%d-%d" % (major, minor, bugfix, build_id)


def copytree(src, dest, ignore=None):
    if os.path.isdir(src):
        if not os.path.isdir(dest):
            os.makedirs(dest)
        files = os.listdir(src)
        if ignore is not None:
            ignored = ignore(src, files)
        else:
            ignored = set()
        for f in files:
            if f not in ignored:
                copytree(os.path.join(src, f), os.path.join(dest, f), ignore)
    else:
        shutil.copyfile(src, dest)

def exec_command(command, env=None, check_rc=True, shell=True, cwd=None):
    if shell:
        command = " ".join(command)
    rc =  subprocess.call(command, env=env, cwd=cwd, shell=shell)
    print "RC:", rc
    if check_rc and rc:
        print "ERROR " * 15
        print "ERROR " * 15
        print 'Execution of command failed %r' % command
        print "ERROR " * 15
        print "ERROR " * 15
        exit(1)
    return 0

def create_git_tag(git_root, tag_name):
    command = ['git', 'tag' , '-a', tag_name, '-m', 'Tag created from succesfully gsetup.py --cmd_build_release command']
    if(subprocess.call(command, env=None, cwd=git_root, shell=False)) != 0:
      print("Tagging failed for repo {0}".format(git_root))
      return 1

    command = ['git', 'push' , 'origin', tag_name]
    print command
    if(subprocess.call(command, env=None, cwd=git_root, shell=False)) != 0:
      print("Tagging push failed for repo {0}".format(git_root))
      return 1
    return 0


def copy_files_to_build_dist_folder(options):
    print "Copy to build_store ", options.build_store()
    src = os.path.join(ROOT, 'gon_build','pack')
    dst = os.path.join(options.build_store())
    copytree(src, dst)
    print "Copy to build_store done"


def cmd_build_cpp(options) :
    print "cmd_build_cpp"
    command = ['python', 'setup.py', '--build']
    rc = exec_command(command, shell=True, cwd=os.path.join(ROOT, 'libs_external'))
    if rc != 0:
        return rc
    return 0

def cmd_build(options) :
    print "clear out build folder"
    if os.path.exists(os.path.join(options.build_store())):
        shutil.rmtree(os.path.join(options.build_store()), ignore_errors=True)
        if os.path.exists(os.path.join(options.build_store())):
            shutil.rmtree(os.path.join(options.build_store()), ignore_errors=True)
    if not os.path.exists(os.path.join(options.build_store())):
        os.makedirs(os.path.join(options.build_store()))

    pack_folder = os.path.join(ROOT, 'gon_build','pack')
    if not options.skip_clean():
        if os.path.exists(pack_folder):
            shutil.rmtree(pack_folder)
        os.makedirs(pack_folder)

    print "cmd_build"
    command = ['python', 'config.py', '--cmd_init', 'release', '--enable_cpp_build', '--build_id', options.build_id(), '--build_store', options.build_store()]
    rc = exec_command(command, shell=True, cwd=os.path.join(ROOT, 'libs'))
    if rc != 0:
        return rc

    command = ['python', 'config.py', '--cmd_release', 'all', '--compare_to_file', '--auto_yes', '--build_id', options.build_id(), '--build_store', options.build_store()]
    rc = exec_command(command, shell=True, cwd=os.path.join(ROOT, 'libs'))
    if rc != 0:
        return rc

    command = ['python', 'config.py', '--cmd_release', 'distribute', '--ifilter', 'client', '--compare_to_file', '--auto_yes', '--build_id', options.build_id(), '--build_store', options.build_store()]
    rc = exec_command(command, shell=True, cwd=os.path.join(ROOT, 'libs'))
    if rc != 0:
        return rc
    return 0

def cmd_build_release(options) :
    print "cmd_build_release"

    rc = 0
    command = ['python', 'gon_release_manager.py', '--build_release_prepare', '--not_beta', '--build_id', options.build_id(), '--build_store', options.build_store(), '--release_store', options.release_store()]
    exec_command(command, shell=True, cwd=os.path.join(ROOT, 'libs', 'py', 'appl', 'gon_release_manager'))

    command = ['python', 'gon_release_manager.py', '--build_release', '--not_beta', '--build_id', options.build_id(), '--build_store', options.build_store(), '--release_store', options.release_store()]
    rc = exec_command(command, shell=True, cwd=os.path.join(ROOT, 'libs', 'py', 'appl', 'gon_release_manager'))
    if rc != 0:
        return rc

    if options.create_git_tag():
        rc = create_git_tag(ROOT, 'gon_release_%s' % options.get_version())
        if rc != 0:
            return rc

    return 0


def cmd_install_py_modules(options):

    def install_py_modules(folder):
        for filename in os.listdir(folder):
            if filename.endswith('.tar.gz'):
                print "Installing src %s" % filename
                temp_folder = tempfile.mkdtemp(dir=os.path.join(ROOT, 'gon_build', 'temp'))
                tar = tarfile.open(os.path.join(folder, filename))
                tar.extractall(temp_folder)
                tar.close()
                command = ['python', 'setup.py', 'build']
                filname_name_elements = filename.split('.tar.gz')
                exec_command(command, shell=True, check_rc=True, cwd=os.path.join(temp_folder, filname_name_elements[0]))
                command = ['python', 'setup.py', 'install']
                exec_command(command, shell=True, check_rc=True, cwd=os.path.join(temp_folder, filname_name_elements[0]))

            elif filename.endswith('.whl'):
                print "Installing bin %s" % filename
                command = ['pip', 'install', os.path.join(folder, filename)]
                exec_command(command, shell=True, check_rc=True)

    def install_py_modules_pip(pips):
        for pip in pips:
            print "Install pip %s" % pip
            command = ['pip', 'install', pip]
            exec_command(command, shell=True, check_rc=True)

    temp_folder = os.path.join(ROOT, 'gon_build', 'temp')
    if not os.path.exists(temp_folder):
        os.makedirs(temp_folder)

    root_external_modules = os.path.join(ROOT, 'libs', 'py', 'setup', 'py_external_modules')
    install_py_modules(os.path.join(root_external_modules, 'common'))
    install_py_modules_pip(pips_common)

    if detect_platform() == TARGET_PLATFORM_WIN:
        install_py_modules(os.path.join(root_external_modules, 'win'))
        install_py_modules_pip(pips_win)

    if detect_platform() == TARGET_PLATFORM_LINUX:
        install_py_modules(os.path.join(root_external_modules, 'linux'))
        install_py_modules_pip(pips_linux)

    if detect_platform() == TARGET_PLATFORM_MAC:
        install_py_modules(os.path.join(root_external_modules, 'mac'))
        install_py_modules_pip(pips_mac)
    return 0
#
#
#
DEVBOTNET = ["master", "slave"]

def run_cmd_devbotnet_master(options):
    command = ['python', 'master.py']
    if options.skip_sign():
        command.append("--skip_sign")
    if options.skip_clean():
        command.append("--skip_clean")
    if options.devbotnet_slave_linux() is not None:
        command.extend(['--slave_linux', options.devbotnet_slave_linux()])
    if options.devbotnet_slave_mac() is not None:
        command.extend(['--slave_mac', options.devbotnet_slave_mac()])
    command.extend(['--slave_win', "127.0.0.1:6090"])

    command = " ".join(command)
    cwd=os.path.join(ROOT, 'libs', 'py', 'setup', 'tools','devbotnet')
    master_proc = subprocess.Popen(command, shell=True, cwd=cwd, bufsize=1)
    rc = run_cmd_devbotnet_slave(options)
    master_proc.terminate()

def run_cmd_devbotnet_slave(options):
    try:
        command = ['python', 'slave.py']
        rc = exec_command(command, shell=True, cwd=os.path.join(ROOT, 'libs', 'py', 'setup', 'tools','devbotnet'))
        if rc != 0:
            return rc
        return 0
    except KeyboardInterrupt:
        return 0


def run_cmd_devbotnet(options):
    if options.cmd_devbotnet() not in DEVBOTNET:
        print("ERROR, Invalid argumet for --cmd_devbotnet, allowed values are %s" % DEVBOTNET)
        return 1

    if options.cmd_devbotnet() == "master":
#        if detect_platform() != TARGET_PLATFORM_WIN:
#            print("ERROR, Platform for running --cmd_devbotnet master, can only be runned on a windows platform")
#            return 1

        if options.devbotnet_slave_mac() is None:
            print("WARNING, Missing argument --devbotnet_slave_mac")

        if options.devbotnet_slave_linux() is None:
            print("WARNING, Missing argument --devbotnet_slave_linux")
        return run_cmd_devbotnet_master(options)

    else:
        if detect_platform() == TARGET_PLATFORM_WIN:
            print("ERROR, Platform for running --cmd_devbotnet slave, can not be runned on a windows platform")
            return 1
        return run_cmd_devbotnet_slave(options)

def cmd_build_prebuild_client_file_packages(options):
    print "cmd_build_prebuild_client_file_packages"
    client_file_package_source_root = os.path.join(ROOT, "..", "gon_client_file_packages")
    if not os.path.exists(client_file_package_source_root):
        print("ERROR, Client File Package folder not found. Expected to find it here '%s'" % client_file_package_source_root)
        return 1
    command = ['python', 'py/appl/gon_config_service/gon_config_service.py', '--config', "./setup/dev_env/gen_gpms/gon_config_service.ini", "--generate_gpms"]
    env = {}
    env["PYTHONPATH"] = os.path.join(ROOT, "libs", 'py')
    rc = exec_command(command, shell=True, env=env, cwd=os.path.join(ROOT, 'libs'))
    if rc != 0:
        return rc
    return 0

#PYTHONPATH=/home/twa/source/gon/libs/py python py/appl/gon_config_service/gon_config_service.py --config ./setup/dev_env/gen_gpms/gon_config_service.ini --generate_gpms



def run_cmd_unittest(options):
    print "run_cmd_unittest"
    command = ['python', 'config.py', '--cmd_unittest', options.cmd_unittest()]
    if options.unittest_ifilter() is not None:
        command.extend(["--unittest_ifilter", options.unittest_ifilter()])
    if options.unittest_efilter() is not None:
        command.extend(["--unittest_efilter", options.unittest_efilter()])
    rc = exec_command(command, shell=True, cwd=os.path.join(ROOT, 'libs'))
    if rc != 0:
        return rc
    return 0


def main():
    options = CommandlineOptions()
    if options.cmd_install_py_modules():
        return cmd_install_py_modules(options)

    if options.cmd_build_cpp():
        if not options.skip_clean():
            if os.path.exists(os.path.join(ROOT, 'gon_build')):
                shutil.rmtree(os.path.join(ROOT, 'gon_build'), ignore_errors=True)
        return cmd_build_cpp(options)

    if options.cmd_build():
        return cmd_build(options)

    if options.cmd_build_release():
        return cmd_build_release(options)

    if options.cmd_build_prebuild_client_file_packages():
        return cmd_build_prebuild_client_file_packages(options)

    if options.cmd_devbotnet() is not None:
        return run_cmd_devbotnet(options)

    if options.cmd_unittest() is not None:
        return run_cmd_unittest(options)




    return 0

if __name__ == '__main__':
    rc = main()
    if rc != 0:
        exit(rc)
